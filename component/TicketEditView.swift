//
//  ShowTextMessage.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class TicketEditView: UIView {
    
    typealias TicketEditViewCompletion = (String?, String?) -> Void
    var completion: TicketEditViewCompletion? = nil
    
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var viewCenter: NSLayoutConstraint!
    @IBOutlet weak var titleLab: UILabel!
    
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var ticketField: UITextField!
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        viewCenter.constant = 0
        self.endEditing(true)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        nameField.layer.borderWidth = 1.0
        nameField.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        ticketField.layer.borderWidth = 1.0
        ticketField.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        nameField.addPadding()
        ticketField.addPadding()
        
        sendBtn.isEnabled = true
        sendBtn.alpha = 1.0
    }
    
    @IBAction func cancelAction(_ sender: Any?) {
        self.removeFromSuperview()
    }
    
    @IBAction func sendAction(_ sender: Any) {
        nameField.resignFirstResponder()
        ticketField.resignFirstResponder()
        
        
        if let completion = completion {
            completion(nameField.text, ticketField.text)
        }
        
        cancelAction(nil)
    }
}

extension TicketEditView {
    class func showReruen(name: String?, ticket: String?, completion: @escaping TicketEditViewCompletion) {
        guard let vw = Bundle.main.loadNibNamed("TicketEditView", owner: self, options: nil)?.first as? TicketEditView else {
            return
        }
        vw.nameField.text = name
        vw.ticketField.text = ticket
        vw.completion = completion
        vw.frame = UIScreen.main.bounds
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.addSubview(vw)
    }
}

extension TicketEditView: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        viewCenter.constant = -(self.frame.height/6)
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        viewCenter.constant = 0
        textField.resignFirstResponder()
        return true
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == ticketField {
            if string == "" {
                return true
            }
            
            if (textField.text?.count ?? 0) >= 8 {
                return false
            }
            
            let aSet = CharacterSet(charactersIn: "0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            
            return string == numberFiltered
        }
        return true
    }
}
