//
//  CustomGMSMarker.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit
import GoogleMaps

class CustomGMSMarker: GMSMarker {
    var index: Int = 0
    var distance: Double = 0
    var isNotFound = false
    public override init() {
        super.init()
    }
}
