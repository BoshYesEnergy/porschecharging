//
//  RouteStationInfoListManager.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class RouteStationInfoListManager: NSObject,NSCoding {
    var stationInfoArray: [RouteStationInfoObjClass]?
    
    init(stationInfoArray: [RouteStationInfoObjClass]) {
        self.stationInfoArray = stationInfoArray
        
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(stationInfoArray, forKey: "StationInfoArray")
    }
    
    // 解碼
    required init?(coder aDecoder: NSCoder) {
        stationInfoArray = aDecoder.decodeObject(forKey: "StationInfoArray") as? Array
    }
}
