//
//  RoutePlanObjClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class RoutePlanObjClass: NSObject, NSCoding {
    var routeName: String?
    var createTime: String?
    var stationCount: String?
    var dataName: String?
    
    init(routeName: String? , createTime: String? , stationCount: String? , dataName: String?) {
        self.routeName = routeName
        self.createTime = createTime
        self.stationCount = stationCount
        self.dataName = dataName
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(routeName, forKey: "RouteName")
        aCoder.encode(createTime, forKey: "CreateTime")
        aCoder.encode(stationCount, forKey: "StationCount")
        aCoder.encode(dataName, forKey: "DataName")

    }
    
    // 解碼
    required init?(coder aDecoder: NSCoder) {
        routeName = aDecoder.decodeObject(forKey: "RouteName") as? String
        createTime = aDecoder.decodeObject(forKey: "CreateTime") as? String
        stationCount = aDecoder.decodeObject(forKey: "StationCount") as? String
        dataName = aDecoder.decodeObject(forKey: "DataName") as? String
    }
}
