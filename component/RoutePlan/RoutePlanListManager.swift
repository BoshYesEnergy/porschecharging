//
//  RoutePlanListManager.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class RoutePlanListManager: NSObject,NSCoding {
    var planListArray: [RoutePlanObjClass]?
    
    init(planListArray: [RoutePlanObjClass]) {
        self.planListArray = planListArray
        
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(planListArray, forKey: "PlanListArray")
    }
    
    // 解碼
    required init?(coder aDecoder: NSCoder) {
        planListArray = aDecoder.decodeObject(forKey: "PlanListArray") as? Array
    }
}
