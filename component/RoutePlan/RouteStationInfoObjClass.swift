//
//  RouteStationInfoObjClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class RouteStationInfoObjClass: NSObject,NSCoding {
    var stationName: String?
    var distance: String?
    var stationId: String?
    var latitude: String?
    var longitude: String?
    
    var address: String?
    var stationCategory: String?
    var hasDC : Bool?
    var hasAC : Bool?
    var connection: Bool?
    
    init(stationName: String? , distance: String? , stationId: String? , latitude: String? , longitude: String?, address: String?, stationCategory: String?, hasDC: Bool?, hasAC: Bool?, connection: Bool?) {
        self.stationName = stationName
        self.distance  = distance
        self.stationId = stationId
        self.latitude  = latitude
        self.longitude = longitude
        
        self.address   = address
        self.stationCategory = stationCategory
        self.hasDC = hasDC
        self.hasAC = hasAC
        self.connection = connection
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(stationName, forKey: "StationName")
        aCoder.encode(distance, forKey: "StationDistance")
        aCoder.encode(stationId, forKey: "StationId")
        aCoder.encode(latitude, forKey: "RouteStationLatitude")
        aCoder.encode(longitude, forKey: "RouteStationLongitude")
        
        aCoder.encode(address, forKey: "RouteStationaddress")
        aCoder.encode(stationCategory, forKey: "RouteStationstationCategory")
        aCoder.encode(hasDC, forKey: "RouteStationstationhasDC")
        aCoder.encode(hasAC, forKey: "RouteStationstationhasAC")
        aCoder.encode(connection, forKey: "RouteStationstationconnection")
        
    }
    
    // 解碼
    required init?(coder aDecoder: NSCoder) {
        stationName = aDecoder.decodeObject(forKey: "StationName") as? String
        distance = aDecoder.decodeObject(forKey: "StationDistance") as? String
        stationId = aDecoder.decodeObject(forKey: "StationId") as? String
        latitude = aDecoder.decodeObject(forKey: "RouteStationLatitude") as? String
        longitude = aDecoder.decodeObject(forKey: "RouteStationLongitude") as? String
        
        address = aDecoder.decodeObject(forKey: "RouteStationaddress") as? String
        stationCategory = aDecoder.decodeObject(forKey: "RouteStationstationCategory") as? String
        hasDC = aDecoder.decodeObject(forKey: "RouteStationstationhasDC") as? Bool
        hasAC = aDecoder.decodeObject(forKey: "RouteStationstationhasAC") as? Bool
        connection = aDecoder.decodeObject(forKey: "RouteStationstationconnection") as? Bool
    }
}
