//
//  NormalCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class NormalCell: UITableViewCell {
 
    @IBOutlet weak var stationICON: UIImageView!
    @IBOutlet weak var titleCell: UILabel!
    @IBOutlet weak var stationAddress: UILabel!
    
    var list: StationSearchModel.Data? {
        didSet {
            guard let list      = list else { return }
            titleCell.text      = list.position ?? ""
            stationAddress.text = list.address ?? ""
            let type = list.type ?? ""
            switch type.lowercased() {
            case "" : stationICON.image = UIImage(named: "icon_search_location_default")
            case "m": stationICON.image = UIImage(named: "icon_search_recent_default")
            case "h": stationICON.image = UIImage(named: "icon_search_station_default")
            default : break
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
