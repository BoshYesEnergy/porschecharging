//
//  ShowImageMessage.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/5/18.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class ShowImageMessage: UIView {
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var titleLabel: UILabel! 
    @IBOutlet weak var checkBtn: UIButton!
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var checkIcon: UIImageView!
    
    typealias SelectCompletion = (Bool) -> Void
    var completion: SelectCompletion? = nil
    var tapFlameout = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUI()
    }
}


//MARK:- Set UI
extension ShowImageMessage {
    func setUI() {
        bottomView.layer.cornerRadius = 5 
        checkBtn.imageView?.contentMode = .scaleToFill
        checkIcon.image = UIImage(named: "checkbox_black_0")
        confirmBtn.setTitle("確定", for: .normal)
    }
}


//MARK:- Action
extension ShowImageMessage {
    @IBAction func checkAction(_ sender: UIButton) {
        if sender.isSelected == true {
            checkIcon.image = UIImage(named: "checkbox_black_0")
            sender.isSelected = !sender.isSelected
            tapFlameout = false
            UserDefaults.standard.set(tapFlameout, forKey: "Flameout")
        } else {
            checkIcon.image = UIImage(named: "checkbox_black_1")
            sender.isSelected = !sender.isSelected
            tapFlameout = true
            UserDefaults.standard.set(tapFlameout, forKey: "Flameout")
        }
    }
    
    @IBAction func confirm(_ sender: UIButton) {
        if let completion = completion {
            completion(tapFlameout)
        }
        self.removeFromSuperview()
    } 
}


//MARK:- Custom
extension ShowImageMessage {
    class func customView(title: String , confirm: String, completion: @escaping SelectCompletion) {
        guard let vw = Bundle.main.loadNibNamed("ShowImageMessage", owner: self, options: nil)?.first as? ShowImageMessage else {
            return
        }
        vw.titleLabel.text = title
        vw.confirmBtn.setTitle(confirm, for: .normal)
        vw.completion = completion
        vw.frame = UIScreen.main.bounds
        vw.layoutIfNeeded()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.addSubview(vw)
    }
}
