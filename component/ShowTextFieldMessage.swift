//
//  ShowTextFieldMessage.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

protocol ShowTextFieldMessageDelegate: class {
    func showTextFieldMessageWith(text: String)
}

class ShowTextFieldMessage: UIView {

    // MARK: - IBOutlet
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var viewCenterConstraint: NSLayoutConstraint!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var clearBtn: UIButton!
    
    // MARK: - Property
    typealias SelectCompletion = (String) -> Void
    var completion: SelectCompletion? = nil

    // MARK: - 生命週期
    override func awakeFromNib() {
        super.awakeFromNib()
        
        messageView.layer.masksToBounds = true
        messageView.layer.cornerRadius = 8.0
        
        titleLabel.text = ""
        messageLabel.text = ""
        
        self.lineView.isHidden = true
        
        textField.delegate = self
        /* 一定要讓 textField becomeFirstResponder
           不然 textField 設 delegate 之後會無法打字 By Scott 2018.11.14 */
        textField.becomeFirstResponder()
    }
    
    // MARK: - IBAction
    @IBAction func cancelAction(_ sender: UIButton) {
        self.removeFromSuperview()
    }
    
    @IBAction func sendAction(_ sender: UIButton) {
        if (textField.text?.count == 0) {
            ShowMessageView.showMessage(title: nil, message: "請輸入路線名稱", completion: { (isOK) in
                
            })
        } else {
            textField.resignFirstResponder()
            if let text = textField.text {
                if let completion = completion {
                    completion(text)
                }
            }
            self.removeFromSuperview()
        }
    }
    
    
    @IBAction func clearAction(_ sender: UIButton) {
        self.textField.text = ""
    }
    
    // MARK: - 自訂 Func
    // default
    class func showMessage(title: String?, message: String , completion: @escaping SelectCompletion) {
        guard let vw = Bundle.main.loadNibNamed("ShowTextFieldMessage", owner: self, options: nil)?.first as? ShowTextFieldMessage else {
            return
        }
        if let title = title, title.count > 0 {
            vw.titleLabel.text = title
        }
        
        vw.messageLabel.text = message
        vw.completion = completion
        vw.frame = UIScreen.main.bounds
        vw.layoutIfNeeded()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.addSubview(vw)
    }
    
    // 自訂按鈕名稱
    class func showMessageCustomeBtnWith(title: String?, message: String, cancelBtnTitle: String, sureBtnTitle: String, completion: @escaping SelectCompletion) {
        guard let vw = Bundle.main.loadNibNamed("ShowTextFieldMessage", owner: self, options: nil)?.first as? ShowTextFieldMessage else {
            return
        }
        
        vw.cancelBtn.setTitle(cancelBtnTitle, for: .normal)
        vw.sendBtn.setTitle(sureBtnTitle, for: .normal)
        
        if let title = title, title.count > 0 {
            vw.titleLabel.text = title
        }
        
        vw.messageLabel.text = message
        vw.completion = completion
        vw.frame = UIScreen.main.bounds
        vw.layoutIfNeeded()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.addSubview(vw)
    }
}

extension ShowTextFieldMessage: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        viewCenterConstraint.constant = -(self.frame.height / 6)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        viewCenterConstraint.constant = -(self.frame.height / 12)
        return true
    }
}


