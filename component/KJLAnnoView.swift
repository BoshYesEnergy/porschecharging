//
//  ReataurantAnnoView.swift
//  EatWaht
//
//  Created by s5346 on 2018/2/22.
//  Copyright © 2018年 s5346. All rights reserved.
//

import UIKit
import MapKit

class KJLAnnoView: MKAnnotationView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    var imageName: String? = nil
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier);
        
        /* Your actual init code */
        if let imageName = imageName {
            self.image = UIImage(named: imageName)
        }
        
        self.canShowCallout = false
        
//        numberLab = UILabel(frame: self.bounds)
//        numberLab?.font = UIFont.boldSystemFont(ofSize: 14)
//        numberLab?.textColor = UIColor.white
//        numberLab?.textAlignment = .center
//        numberLab?.backgroundColor = UIColor.clear
//        self.addSubview(numberLab!)
    }
    
    convenience init(frame: CGRect) {
        self.init(annotation: nil, reuseIdentifier: nil);
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let hitView = super.hitTest(point, with: event)
        
        if hitView == nil {
            self.superview?.bringSubview(toFront: self)
        }
        
        return hitView;
    }
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let rect = self.bounds
        var isInside = rect.contains(point)
        if(!isInside)
        {
            for view in self.subviews {
                isInside = view.frame.contains(point);
                if isInside {
                    break
                }
            }
        }
        return isInside;
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        if selected {
            
        }else {
            
        }
    }
}
