//
//  ShowMessageView.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit
var isShowKey: UInt8 = 0
class ShowMessageView: UIView {

    typealias SelectCompletion = (Bool) -> Void
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var messageLab: UILabel!
    var completion: SelectCompletion? = nil
    @IBOutlet weak var titleLab: UILabel!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var sureBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layoutIfNeeded()
//        messageView.layer.masksToBounds = true
//        messageView.layer.cornerRadius = 8.0
//        messageView.layer.borderColor = #colorLiteral(red: 0.8196078431, green: 0.8196078431, blue: 0.8196078431, alpha: 1)
//        messageView.layer.borderWidth = 1.5
        
        titleLab.text = ""
        lineView.isHidden = true
        sureBtn.setTitle(LocalizeUtils.localized(key: Language.Confirm.rawValue), for: .normal)
    }
    
    // 確定
    @IBAction func sureAction(_ sender: Any) {
        if let completion = completion {
            completion(true)
        }
       self.removeView()
    }
    
    // 取消
    @IBAction func cancelAction(_ sender: Any) {
        self.removeView()
    }
    
    func removeView() {
        ShowMessageView.setIsShowed(false)
        self.removeFromSuperview()
    }
}

extension ShowMessageView {
    class func showMessage(title: String?, message: String) {
        if self.isShowed().boolValue == true {
            return
        }
        
        self.setIsShowed(true)
        guard let vw = Bundle.main.loadNibNamed("ShowMessageView", owner: self, options: nil)?.first as? ShowMessageView else {
            return
        }
        if let title = title, title.count > 0 {
            vw.titleLab.text = "\n" + title + "\n"
            vw.lineView.isHidden = true
        }
        
        vw.messageLab.text = message
        vw.frame = UIScreen.main.bounds
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.addSubview(vw)
    }
    
    // 回傳確認
    class func showMessage(title: String?, message: String, completion: @escaping SelectCompletion) {
        if self.isShowed().boolValue == true {
            return
        }
        
        self.setIsShowed(true)
        guard let vw = Bundle.main.loadNibNamed("ShowMessageView", owner: self, options: nil)?.first as? ShowMessageView else {
            return
        }
        if let title = title, title.count > 0 {
            vw.titleLab.text = "\n" + title + "\n"
            vw.lineView.isHidden = true
        }
        
        vw.messageLab.text = message
        vw.completion = completion
        vw.frame = UIScreen.main.bounds
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.addSubview(vw)
    }
    
    
    class func showSingleMessage(title: String?, message: String, sureBtnTitle: String , completion: @escaping SelectCompletion) {
        if self.isShowed().boolValue == true {
            return
        }
        
        self.setIsShowed(true)
        guard let vw = Bundle.main.loadNibNamed("ShowMessageView", owner: self, options: nil)?.first as? ShowMessageView else {
            return
        }
        vw.sureBtn.setTitle(sureBtnTitle, for: .normal)
        if let title = title, title.count > 0 {
            vw.titleLab.text = "\n" + title + "\n"
            vw.lineView.isHidden = true
        }
        
        vw.messageLab.text = message
        vw.completion = completion
        vw.frame = UIScreen.main.bounds
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.addSubview(vw)
    }
    
    class func showQuestion(title: String?, message: String, completion: @escaping SelectCompletion) {
        if self.isShowed().boolValue == true {
            return
        }
        
        self.setIsShowed(true)
        guard let vw = Bundle.main.loadNibNamed("ShowMessageView", owner: self, options: nil)?.last as? ShowMessageView else {
            return
        }
        
        if let title = title, title.count > 0 {
            vw.titleLab.text = "\n" + title + "\n"
            vw.lineView.isHidden = true
        }
        
        vw.messageLab.text = message
        vw.completion = completion
        vw.frame = UIScreen.main.bounds
        vw.layoutIfNeeded()
       // UIApplication.shared.keyWindow?.addSubview(vw)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.addSubview(vw)
    }
    
    // 設定
    class func showQuestionSetting(title: String?, message: String, completion: @escaping SelectCompletion) {
        if self.isShowed().boolValue == true {
            return
        }
        
        self.setIsShowed(true)
        guard let vw = Bundle.main.loadNibNamed("ShowMessageView", owner: self, options: nil)?.last as? ShowMessageView else {
            return
        }
        
        vw.sureBtn.setTitle("設定", for: .normal)
        
        if let title = title, title.count > 0 {
            vw.titleLab.text = "\n" + title + "\n"
            vw.lineView.isHidden = true
        }
        
        vw.messageLab.text = message
        vw.completion = completion
        vw.frame = UIScreen.main.bounds
        vw.layoutIfNeeded()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.addSubview(vw)
    }
    
    // 自訂按鈕文字
    class func showQuestionWithCustomButton(messageTitle: String?, message: String, sureBtnTitle: String , cancelBtnTitle: String , completion: @escaping SelectCompletion) {
        if self.isShowed().boolValue == true {
            return
        }
        
        self.setIsShowed(true)
        guard let vw = Bundle.main.loadNibNamed("ShowMessageView", owner: self, options: nil)?.last as? ShowMessageView else {
            return
        }
        
        vw.sureBtn.setTitle(sureBtnTitle, for: .normal)
        vw.cancelBtn.setTitle(cancelBtnTitle, for: .normal)
        
        if let title = messageTitle, title.count > 0 {
            vw.titleLab.text = "\n" + title + "\n"
            vw.lineView.isHidden = true
        }
        
        vw.messageLab.text = message
        vw.completion = completion
        vw.frame = UIScreen.main.bounds
        vw.layoutIfNeeded()
       let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.addSubview(vw)
    }
    
    class func setIsShowed(_ isShowed: Bool) {
        objc_setAssociatedObject(self, &isShowKey, NSNumber(value: isShowed), .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
    
    class func isShowed() -> NSNumber {
        if (objc_getAssociatedObject(self, &isShowKey) == nil) {
            objc_setAssociatedObject(self, &isShowKey, NSNumber.init(value: false), .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
        return objc_getAssociatedObject(self, &isShowKey) as! NSNumber
    }
}
