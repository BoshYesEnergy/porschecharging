//
//  PhotoView.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit
import Kingfisher

class PhotoView: UIView {

    @IBOutlet weak var kcollection: UICollectionView!
    var picArray: [StationDetailClass.Data.PicArray] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        kcollection.register(UINib(nibName: "PhotoCell", bundle: nil), forCellWithReuseIdentifier: "PhotoCell")
    }

    @IBAction func closeAction(_ sender: UITapGestureRecognizer) {
        self.removeFromSuperview()
    }
    
    func changeData(arr: [StationDetailClass.Data.PicArray]?, index: Int) {
        if let arr = arr {
            picArray = arr
            kcollection.reloadData()
            
            self.setNeedsLayout()
            self.layoutIfNeeded()
            kcollection.contentOffset = CGPoint(x: self.frame.width * CGFloat(index), y: 0)
        }
    }
}

extension PhotoView: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return picArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath)
        
        if let cell = cell as? PhotoCell {
            let data = picArray[indexPath.row]
            
            let tempUrl = URL(string: data.picPath ?? "")
            cell.imageCell.kf.setImage(with: tempUrl, placeholder: nil, options: nil, progressBlock: nil) { (img, error, type, url) in
                cell.imageCell.image = img
            }
        }
        
        return cell
    }
    
    
}

extension PhotoView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.frame.width, height: self.frame.height)
    }
}

