//
//  KJLRequest.swift
//  EatWaht
//
//  Created by s5346 on 2018/2/18.
//  Copyright © 2018年 s5346. All rights reserved.
//

import Foundation
import Alamofire
import SVProgressHUD

protocol KJLRequestDelegate: class {
    func apiFailResultWith(resCode: String , message: String)
}

class KJLRequest {
    private static var kInstance : KJLRequest?
    static func shareInstance() -> KJLRequest {
        if kInstance == nil {
            kInstance = KJLRequest()
        }
        return kInstance!
    }
    
    let device = "ios"
    var token = ""
    var profile: ProfileClass?
    var language: String?
    
    weak var kJLRequestDelegate: KJLRequestDelegate? = nil
    var loginMethod : RequestLoginType = .none
    static let isLineOpen = true
    static var domain = "https://porscheapi.yes-energy.com.tw/api/"           // 正式機
    static var pushDomain = "https://appserver.yulon-energy.com:8079/api/"    // 正式機
//        static var domain = "http://18.182.99.126:8089/api/"                  // 測試機
//        static var pushDomain = "http://18.182.99.126:8079/api/"              // 測試機
    static let majiDomain = "https://third-party-api.autopass.xyz"            // 車麻吉
    static var googleKey = "AIzaSyA1DzPupNDnFBWu0fNLaFJJr3Cy86LdoW8"          // google test GCP key
    
    typealias RequestCompletion = (Codable?) -> Void
    
    // 電話跟信箱判斷流程
    enum RequestIdentityType {
        case none
        case registApp
        case registerFB
        case registGoogle
        case registApple
        case validPhone
        case validPhoneForChangePhone
        case validPhoneForChangeAppEmail
        case validPhoneForChangeFbEmail
        case validPhoneForChangeGoogleEmail
        case validPhoneForChangePassword
        case forgotPassword
    }
    
    // 登入類型
    enum RequestLoginType: String {
        case none = ""
        case app = "app"
        case fb = "fb"
        case google = "google"
        case forgot = "forgot"
        case business = "business"
        case apple = "apple" 
    }
    
    // 忘記密碼：ForgotPW, 驗證Email：CheckEmail
    enum RequestCheckEmail: String {
        case ForgotPW = "ForgotPW"
        case CheckEmail = "CheckEmail"
    }
    
    // queryType: Station：充電樁列表, Favourite：我的最愛
    enum RequestQueryType: String {
        case Station    = "station"
        case Favourite  = "favourite"
        case None       = ""
    }
    
    // reportType: OK：充電成功, Fail：充電失敗, Charging：充電中, Other：其他
    enum RequestReportType: String {
        case OK = "OK"
        case Fail = "Fail"
        case Charging = "Charging"
        case Other = "Other"
    }
    
    // currentTypes: 充電樁條件,用半形逗號區隔, DC：直流 AC：交流 DC,AC：直流+交流
    enum RequestCurrentTypes: String {
        case NONE = ""
        case DC = "DC"
        case AC = "AC"
        case DCAC = "DC,AC"
    }
    
    // mapDetail: 地圖設定（詳細資訊）, 交通狀況（路況）：TrafficLayer
    enum RequestMapDetail: String {
        case NONE = ""
        case TrafficLayer = "TrafficLayer"
    }
    
    // chargeTypes: 是否需支付充電費用,用半形逗號區隔, 免費：Free, 付費：Paid, 付費+免費：Paid,Free
    enum RequestChargeTypes: String {
        case NONE = ""
        case Free = "Free"
        case Paid = "Paid"
        case FreePaid = "Paid,Free"
    }
    
    /// stationOpenType：充電站開放類型 (若多選以半形逗號區隔) , Open：對外開放 , Condition：有條件開放 , OpenCondition：對外開放、有條件開放
    enum RequestStationOpenType: String {
        case NONE = ""
        case Open = "Open"
        case Condition = "Condition"
        case OpenCondition = "Open,Condition"
    }
    
    /// parkingCoupon：停車優惠 (若多選以半形逗號區隔) , Coupon：有優惠 , Price：無優惠 , CouponPrice：有優惠、無優惠
    enum RequestParkingCouponType: String {
        case NONE = ""
        case Coupon = "Coupon"
        case Price = "Price"
        case CouponPrice = "Coupon,Price"
    }
    
    /// chargePayType：充電費支付方式 (若多選以半形逗號區隔) , App：YES Charge APP , Other：其他 , AppOther： YES Charge APP、其他
    enum RequestChargePayType: String {
        case NONE = ""
        case App = "App"
        case Other = "Other"
        case AppOther = "App,Other"
    }
    
    // cardType: VISA / MasterCard /JCB
    enum RequestCardType: String {
        case VISA = "VISA"
        case MasterCard = "MasterCard"
        case JCB = "JCB"
    }
    
    enum RequestPayment: String {
        case none = ""
        case All = "all"
        case CreditCard = "CreditCard"
        case UUCard = "UUCard"
        case ThirdParty = "ThirdParty"
    }
    
    enum RequestBOOL: String {
        case TRUE = "true"
        case FALSE = "false"
    }
    
    enum RequestChangePW: String {
        case none = ""
        case change = "change"
        case verify = "verify"
        case forgot = "forgot"
    }
    
    enum ReservePayment: String {
        case none = ""
        case All = "all"
        case CreditCard = "creditcard"
        case UUCard = "uucard"
        case ThirdParty = "thirdparty"
    }
    
    enum RequestAPI: String {
        // [001] 版本確認
        case apiForVersionCheck = "VersionCheck"
        // [002] 系統參數
        case apiForSystemVerb = "SystemVerb"
        // [003] 裝置資訊上傳
        case apiForDeviceUpload = "DeviceIdUpload"
        // [101] App會員註冊
        case apiForAppMemberRegister = "AppMemberRegister"
        // [102] App會員登入
        case apiForAppLogin = "AppLogin"
        //  企業會員登入
        case AppBusinessLogin = "AppBusinessLogin"
        // [103] Facebook登入
        case apiForFbLogin = "FbLogin"
        // [104] Google登入
        case apiForGoogleLogin = "GoogleLogin"
        // [105] 登出
        case apiForLogout = "Logout"
        // [106] Apple log in
        case apiForAppleLogin = "AppleLogin"
        // [111] 發送電話驗證
        case apiForCheckPhone = "CheckPhone"
        // [112] 確認電話驗證碼
        case apiForVerifyPhone = "VerifyPhone"
        // [121] 驗證Email
        case apiForCheckEmail = "CheckEmail"
        // [201] 查詢基本資料
        case apiForProfile = "Profile"
        // [202] 變更基本資料
        case apiForEditProfile = "EditProfile"
        // [203] 變更密碼
        case apiForChangePW = "ChangePW"
        // [204] 上傳大頭照
        case apiForUploadMemberPic = "UploadMemberPic"
        // [205] 變更手機
        case apiForChangePhone = "ChangePhone"
        // [206] 變更信箱
        case apiForChangeEmail = "ChangeEmail"
        // [207] 設為預設信箱
        case apiForSetDefaultEmail = "SetDefaultEmail"
        // [301] 站點查詢
        case apiForStationList = "StationList"
        // [302] 站點明細
        case apiForStationDetail = "StationDetail"
        // [901]车麻吉站點明細
        case apiForMajiStationDetail = "/v1/pois/query_by_ids"
        // [303] 充電回報
        case apiForChargingReport = "ChargingReport"
        // [304] 加入我的最愛
        case apiForAddFavourite = "AddFavourite"
        // [305] 分類篩選條件
        case apiForSetPreferences = "SetPreferences"
        // [306] 充電樁列表
        case apiForSocketList = "SocketList"
        // [307] 查詢分類篩選
        case apiForShowPreferences = "ShowPreferences"
        // [308] 查詢充電回報
        case apiForChargingReportList = "ChargingReportList"
        // [309] 充電站新建
        case apiForStationCreate = "StationCreate"
        // [310] 近七天充電次數統計
        case apiForSevenDaysCharge = "SevenDaysCharge"
        // [311] 充電費率列表
        case apiForChargeFeeList = "ChargeFeeList"
        // [312] 充電樁規格列表
        case apiForSpotFormatList = "SpotFormatList"
        // [401] 配對/準備充電 Step1
        case apiForChargerPair = "ChargerPair"
        // [401-?] 配對/準備充電 Step1
        case apiForChargerPairing = "ChargerPairing"
        // [402] 開始充電 Step2
        case apiForBeginCharging = "BeginCharging"
        // [403] 充電狀態查詢 Step3
        case apiForChargingStatus = "ChargingStatus"
        // [5403] 充電狀態查詢 Step3
        case apiForCarChargingStatus = "CarChargingStatus"
        // [404] 充電結果 Step4
//        case apiForChargingResult = "ChargingResult"
        // [405] 停止充電
        case apiForStopCharging = "StopCharging"
        // [406] 充電紀錄列表
        case apiForChargeList = "ChargeList"
        // [407] 充電紀錄統計
        case apiForChargeStatistics = "ChargeStatistics"
        // [408] 查詢充電中列表
        case apiForChargingListData = "ChargingListData"
        // [409] 取消配對 Step1
        case apiForCancelPair = "CancelPair"
        // [410] 解除地鎖
        case apiForParkingLockDown = "ParkingLockDown_v2"
        
        // [501] 查詢信用卡
        case apiForCardList = "CardList"
        // new[501]
        case apiForCardListData = "CardListData"
        // [502] 新增信用卡
        case apiForAddCard = "AddCard"
        // [503] 編輯信用卡
        case apiForEditCard = "EditCard"
        // [504] 刪除信用卡
        case apiForDeleteCard = "DeleteCard"
        // [511] 查詢統一編號
        case apiForTaxIDList = "TaxIDList"
        // [512] 編輯統一編號
        case apiForEditTaxID = "EditTaxID"
        // [521] 查詢手機條碼
        case apiForMbCodeList = "MbCodeList"
        // [522] 新增手機條碼
        case apiForAddMbCode = "AddMbCode"
        // [523] 編輯手機條碼
        case apiForEditMbCode = "EditMbCode"
        // [524] 刪除手機條碼
        case apiForDelMbCode = "DelMbCode"
        // [541] 補扣款
        case apiForRepay = "Repay"
        // [601] 查詢消息列表
        case apiForNewsList = "NewsList"
        // [602] 查詢消息明細
        case apiForNewsDetail = "NewsDetail"
        // [603] 編輯消息訂閱
        case apiForSetSubscribe = "SetSubscribe"
        // [604] 查詢消息訂閱
        case apiForShowSubscribe = "ShowSubscribe"
        // [605] 訊息刪除
        case apiNewsBatchReadDelete = "NewsBatchReadDelete"
        // [606] 分類未讀數及推播數
        case apiGetNewsBadge = "NewsTypeCount"
        // [701] 申請建樁
        case apiForApplyStation = "ApplyStation"
        // [702] 查詢常見問題列表
        case apiForQuestionList = "QuestionList"
        // [703] 查詢常見問題明細
        case apiForQuestionDetail = "QuestionDetail"
        // [704] Google Map Route -- use Direction key
        case apiForGoogleMapRoute = "GoogleMapRoute"
        /// [801] 預約準備
        case apiForReservePrepare = "ReservePrepare"
        /// [804] 預約列表
        case apiForReserveList = "ReserveList"
        /// [805] 預約明細
        case apiForReserveDetail = "ReserveDetail"
        /// [806]
        
        /// 口罩
        case apiForMaskMap = "MaskMap"
        /// 綁定/解綁定車牌
        case apiForBindLicensePlate = "BindLicensePlate"
        /// 查詢綁定車牌
        case apiForQueryLicensePlate = "QueryLicensePlate"
        /// 綁定車牌備註
        case apiForAddRemarkLicensePlate = "AddRemarkLicensePlate"
        /// [552]新增AddLineRegKey
        case apiForAddLineRegKey = "AddLineRegKey"
        case apiForChargingOvertimeStatus = "ChargingOvertimeStatus"
        case apiForAddLineReserve = "LineReserve"
        case apiForAddLineConfirm = "LineConfirm"
        case apiForAddEasyCardReserve = "EasyCardRegister"
        case apiForAddEasyCardConfirm = "EasyCardConfirm"
        case apiCheckCarLockStatus = "CarLockStatus"
        case apiChargingParkingLockUp = "ParkingLockUp"
        case apiCarChargingResult = "CarChargingResult"
        case apiUpdateFcmToken = "PushTokenRegister"
        case apiPushLogOut = "PushLogOut"
        
        
        /// 2020/10/22
        case apiGenerateQrCode      = "GenerateQrCode"
        case apiScanQrCode          = "ScanQrCode"
        case apiGetVoucher          = "GetVoucher"
        case apiCheckPartner        = "CheckPartner"
        case apiSubscriptionCar     = "SubscriptionCar"
        case apiPorscheCarList      = "PorscheCarList"
        case apiPartner             = "Partner"
        case apiDeletePartner       = "DeletePartner"
        case apiInvoiceSettingList  = "InvoiceSettingList"
        case apiEditInvoiceSetting  = "EditInvoiceSetting"
        case apiSetInvoiceSettings  = "SetInvoiceSettings"
        case apiConfirmPartner      = "ConfirmPartner"
        case apiSetStationHistory   = "SetStationHistory"
        case apiChargingHistory     = "ChargingHistory"
        case apiPorscheChargingResult      = "ChargingResult"
        case apiViewSubscription    = "ViewSubscription"
        // search Keyword for map
        case apiStationSearch       = "StationSearch"
        // language
        case apiChangeLanguage      = "ChangeLanguage"
        case apiPorscheLogin        = "PorscheLogin"
        // Reserve
        case apiForReserveStart  = "ReserveStart"
        case apiForReserveCancel = "ReserveCancel"
        case apiForReserveCheck  = "ReserveCheck"
        
        case apiCheckTerms       = "CheckTerms"
        
        case apiPushGetList = "PushGetList"
        case apiPushSetRead = "PushSetRead"
        case apiPushDelete  = "PushDelete"
        case apiChargingData = "ChargingData"
        
        func requestURL(_ param: String?) -> String{
            switch self {
            case .AppBusinessLogin:
            return domain + RequestAPI.AppBusinessLogin.rawValue
            case .apiPushSetRead:
            return pushDomain + RequestAPI.apiPushSetRead.rawValue
            case .apiPushGetList:
            return pushDomain + RequestAPI.apiPushGetList.rawValue
            case .apiPushLogOut:
            return pushDomain + RequestAPI.apiPushLogOut.rawValue
            case .apiUpdateFcmToken:
            return pushDomain + RequestAPI.apiUpdateFcmToken.rawValue
            case .apiCarChargingResult:
            return domain + RequestAPI.apiCarChargingResult.rawValue
            case .apiChargingParkingLockUp:
                return domain + RequestAPI.apiChargingParkingLockUp.rawValue
            case .apiForCarChargingStatus:
            return domain + RequestAPI.apiForCarChargingStatus.rawValue
            case .apiCheckCarLockStatus:
            return domain + RequestAPI.apiCheckCarLockStatus.rawValue
            case .apiForAddLineReserve:
                return domain + RequestAPI.apiForAddLineReserve.rawValue
            case .apiForAddLineConfirm:
                return domain + RequestAPI.apiForAddLineConfirm.rawValue
            case .apiForAddLineRegKey:
                return domain + RequestAPI.apiForAddLineRegKey.rawValue
            case .apiForAddEasyCardReserve:
                return domain + RequestAPI.apiForAddEasyCardReserve.rawValue
            case .apiForAddEasyCardConfirm:
                return domain + RequestAPI.apiForAddEasyCardConfirm.rawValue
            case .apiForVersionCheck:
                return domain + RequestAPI.apiForVersionCheck.rawValue
            case .apiForSystemVerb:
                return domain + RequestAPI.apiForSystemVerb.rawValue
            case .apiForAppMemberRegister:
                return domain + RequestAPI.apiForAppMemberRegister.rawValue
            case .apiForAppLogin:
                return domain + RequestAPI.apiForAppLogin.rawValue
            case .apiForFbLogin:
                return domain + RequestAPI.apiForFbLogin.rawValue
            case .apiForGoogleLogin:
                return domain + RequestAPI.apiForGoogleLogin.rawValue
            case .apiForLogout:
                return domain + RequestAPI.apiForLogout.rawValue
            case . apiForAppleLogin:
                return domain + RequestAPI.apiForAppleLogin.rawValue
            case .apiForCheckPhone:
                return domain + RequestAPI.apiForCheckPhone.rawValue
            case .apiForVerifyPhone:
                return domain + RequestAPI.apiForVerifyPhone.rawValue
            case .apiForCheckEmail:
                return domain + RequestAPI.apiForCheckEmail.rawValue
            case .apiForProfile:
                return domain + RequestAPI.apiForProfile.rawValue
            case .apiForChargeFeeList:
                return domain + RequestAPI.apiForChargeFeeList.rawValue
            case .apiForSpotFormatList:
                return domain + RequestAPI.apiForSpotFormatList.rawValue
            case .apiForEditProfile:
                return domain + RequestAPI.apiForEditProfile.rawValue
            case .apiForChangePW:
                return domain + RequestAPI.apiForChangePW.rawValue
            case .apiForStationList:
                return domain + RequestAPI.apiForStationList.rawValue
            case .apiForStationDetail:
                return domain + RequestAPI.apiForStationDetail.rawValue
            case .apiForMajiStationDetail:
                return majiDomain + RequestAPI.apiForMajiStationDetail.rawValue
            case .apiForChargingReport:
                return domain + RequestAPI.apiForChargingReport.rawValue
            case .apiForAddFavourite:
                return domain + RequestAPI.apiForAddFavourite.rawValue
            case .apiForStationCreate:
                return domain + RequestAPI.apiForStationCreate.rawValue
            case .apiForSetPreferences:
                return domain + RequestAPI.apiForSetPreferences.rawValue
            case .apiForSocketList:
                return domain + RequestAPI.apiForSocketList.rawValue
            case .apiForChargerPair:
                return domain + RequestAPI.apiForChargerPair.rawValue
            case .apiForChargerPairing:
                return domain + RequestAPI.apiForChargerPairing.rawValue
            case .apiForBeginCharging:
                return domain + RequestAPI.apiForBeginCharging.rawValue
            case .apiForChargingStatus:
                return domain + RequestAPI.apiForChargingStatus.rawValue
//            case .apiForChargingResult:
//                return domain + RequestAPI.apiForChargingResult.rawValue
            case .apiForStopCharging:
                return domain + RequestAPI.apiForStopCharging.rawValue
            case .apiForChargeList:
                return domain + RequestAPI.apiForChargeList.rawValue
            case .apiForChargeStatistics:
                return domain + RequestAPI.apiForChargeStatistics.rawValue
            case .apiForChargingListData:
                return domain + RequestAPI.apiForChargingListData.rawValue
            case .apiForCardList:
                return domain + RequestAPI.apiForCardList.rawValue
            case .apiForCardListData:
                return domain + RequestAPI.apiForCardListData.rawValue
            case .apiForAddCard:
                return domain + RequestAPI.apiForAddCard.rawValue
            case .apiForEditCard:
                return domain + RequestAPI.apiForEditCard.rawValue
            case .apiForDeleteCard:
                return domain + RequestAPI.apiForDeleteCard.rawValue
            case .apiForTaxIDList:
                return domain + RequestAPI.apiForTaxIDList.rawValue
            case .apiForEditTaxID:
                return domain + RequestAPI.apiForEditTaxID.rawValue
            case .apiForMbCodeList:
                return domain + RequestAPI.apiForMbCodeList.rawValue
            case .apiForAddMbCode:
                return domain + RequestAPI.apiForAddMbCode.rawValue
            case .apiForEditMbCode:
                return domain + RequestAPI.apiForEditMbCode.rawValue
            case .apiForDelMbCode:
                return domain + RequestAPI.apiForDelMbCode.rawValue
            case .apiForApplyStation:
                return domain + RequestAPI.apiForApplyStation.rawValue
            case .apiForQuestionList:
                return domain + RequestAPI.apiForQuestionList.rawValue
            case .apiForQuestionDetail:
                return domain + RequestAPI.apiForQuestionDetail.rawValue
            case .apiForGoogleMapRoute:
                return domain + RequestAPI.apiForGoogleMapRoute.rawValue
            case .apiForCancelPair:
                return domain + RequestAPI.apiForCancelPair.rawValue
            case .apiForChangeEmail:
                return domain + RequestAPI.apiForChangeEmail.rawValue
            case .apiForChangePhone:
                return domain + RequestAPI.apiForChangePhone.rawValue
            case .apiForNewsList:
                return domain + RequestAPI.apiForNewsList.rawValue
            case .apiForNewsDetail:
                return domain + RequestAPI.apiForNewsDetail.rawValue
            case .apiForSetSubscribe:
                return domain + RequestAPI.apiForSetSubscribe.rawValue
            case .apiForShowSubscribe:
                return domain + RequestAPI.apiForShowSubscribe.rawValue
                //            case .apiForSetSubscribe:
            //                return domain + RequestAPI.apiForShowSubscribe.rawValue
            case .apiForShowPreferences:
                return domain + RequestAPI.apiForShowPreferences.rawValue
            case .apiForDeviceUpload:
                return domain + RequestAPI.apiForDeviceUpload.rawValue
            case .apiForUploadMemberPic:
                return domain + RequestAPI.apiForUploadMemberPic.rawValue
            case .apiForChargingReportList:
                return domain + RequestAPI.apiForChargingReportList.rawValue
            case .apiForSevenDaysCharge:
                return domain + RequestAPI.apiForSevenDaysCharge.rawValue
            case .apiForParkingLockDown:
                return domain + RequestAPI.apiForParkingLockDown.rawValue
            case .apiForRepay:
                return domain + RequestAPI.apiForRepay.rawValue
            case .apiForReservePrepare:
                return domain + RequestAPI.apiForReservePrepare.rawValue
            case .apiForReserveCancel:
                return domain + RequestAPI.apiForReserveCancel.rawValue
            case .apiForReserveStart:
                return domain + RequestAPI.apiForReserveStart.rawValue
            case .apiForReserveList:
                return domain + RequestAPI.apiForReserveList.rawValue
            case .apiForReserveDetail:
                return domain + RequestAPI.apiForReserveDetail.rawValue
            case .apiForSetDefaultEmail:
                return domain + RequestAPI.apiForSetDefaultEmail.rawValue
            case .apiForReserveCheck:
                return domain + RequestAPI.apiForReserveCheck.rawValue
            case .apiForChargingOvertimeStatus:
                return domain + RequestAPI.apiForChargingOvertimeStatus.rawValue
            case .apiNewsBatchReadDelete:
                return domain + RequestAPI.apiNewsBatchReadDelete.rawValue
            case .apiGetNewsBadge:
                return domain + RequestAPI.apiGetNewsBadge.rawValue
            case .apiForMaskMap:
                return domain + RequestAPI.apiForMaskMap.rawValue
            case .apiForBindLicensePlate:
                return domain + RequestAPI.apiForBindLicensePlate.rawValue
            case .apiForQueryLicensePlate:
                return domain + RequestAPI.apiForQueryLicensePlate.rawValue
            case .apiForAddRemarkLicensePlate:
                return domain + RequestAPI.apiForAddRemarkLicensePlate.rawValue
            case .apiGenerateQrCode:
                return domain + RequestAPI.apiGenerateQrCode.rawValue
            case .apiScanQrCode:
                return domain + RequestAPI.apiScanQrCode.rawValue
            case .apiCheckPartner:
                return domain + RequestAPI.apiCheckPartner.rawValue
            case .apiGetVoucher:
                return domain + RequestAPI.apiGetVoucher.rawValue
            case .apiSubscriptionCar:
                return domain + RequestAPI.apiSubscriptionCar.rawValue
            case .apiPorscheCarList:
                return domain + RequestAPI.apiPorscheCarList.rawValue
            case .apiPartner:
                return domain + RequestAPI.apiPartner.rawValue
            case .apiDeletePartner:
                return domain + RequestAPI.apiDeletePartner.rawValue
            case .apiInvoiceSettingList:
                return domain + RequestAPI.apiInvoiceSettingList.rawValue
            case .apiEditInvoiceSetting:
                return domain + RequestAPI.apiEditInvoiceSetting.rawValue
            case .apiSetInvoiceSettings:
                return domain + RequestAPI.apiSetInvoiceSettings.rawValue
            case .apiConfirmPartner:
                return domain + RequestAPI.apiConfirmPartner.rawValue
            case .apiSetStationHistory:
                return domain + RequestAPI.apiSetStationHistory.rawValue
            case .apiChargingHistory:
                return domain + RequestAPI.apiChargingHistory.rawValue
            case .apiPorscheChargingResult:
                return domain + RequestAPI.apiPorscheChargingResult.rawValue
            case .apiViewSubscription:
                return domain + RequestAPI.apiViewSubscription.rawValue
            case .apiStationSearch:
                return domain + RequestAPI.apiStationSearch.rawValue
            case .apiChangeLanguage:
                return domain + RequestAPI.apiChangeLanguage.rawValue
            case .apiPorscheLogin:
                return domain + RequestAPI.apiPorscheLogin.rawValue
            case .apiCheckTerms:
                return domain + RequestAPI.apiCheckTerms.rawValue
            case .apiPushDelete:
                return pushDomain + RequestAPI.apiPushDelete.rawValue
            case .apiChargingData:
                return domain + RequestAPI.apiChargingData.rawValue
            }
        }
    }
    
    func requestForServer(request: DataRequest, apiType: RequestAPI ,completion: @escaping RequestCompletion) {
        SVProgressHUD.setDefaultMaskType(.clear)
        switch apiType {
//        case RequestAPI.apiForChargingResult    : SVProgressHUD.show(withStatus: "交易結果確認中，請稍候...")
        case RequestAPI.apiCarChargingResult    : SVProgressHUD.show(withStatus: "交易結果確認中，請稍候...")
        case RequestAPI.apiForStationList       : SVProgressHUD.dismiss()
        case RequestAPI.apiGetNewsBadge         : SVProgressHUD.dismiss()
        case RequestAPI.apiForChargingListData  : SVProgressHUD.dismiss()
        case RequestAPI.apiForAddRemarkLicensePlate: SVProgressHUD.dismiss()
        case RequestAPI.apiForChargeList        : SVProgressHUD.dismiss()
        case RequestAPI.apiForQueryLicensePlate : SVProgressHUD.dismiss()
        case RequestAPI.apiForBindLicensePlate  : SVProgressHUD.dismiss()
        
        case RequestAPI.apiForDeviceUpload      : SVProgressHUD.dismiss()
        case RequestAPI.apiForShowPreferences   : SVProgressHUD.dismiss()
        case RequestAPI.apiForSystemVerb        : SVProgressHUD.dismiss()
        case RequestAPI.apiForVersionCheck      : SVProgressHUD.dismiss()
        case RequestAPI.apiUpdateFcmToken       : SVProgressHUD.dismiss()
        case RequestAPI.apiForProfile           : SVProgressHUD.dismiss()
        case RequestAPI.apiForAddFavourite      : SVProgressHUD.dismiss()
        case RequestAPI.apiForSetPreferences    : SVProgressHUD.dismiss()
        
        case RequestAPI.apiGenerateQrCode       : SVProgressHUD.dismiss()
        case RequestAPI.apiCheckPartner         : SVProgressHUD.dismiss()
        case RequestAPI.apiPartner              : SVProgressHUD.dismiss()
        case RequestAPI.apiChargingHistory      : SVProgressHUD.dismiss()
        case RequestAPI.apiStationSearch        : SVProgressHUD.dismiss()
        case RequestAPI.apiSetStationHistory    : SVProgressHUD.dismiss()
        case RequestAPI.apiChangeLanguage       : SVProgressHUD.dismiss()
        case RequestAPI.apiCheckTerms           : SVProgressHUD.dismiss()
        case RequestAPI.apiForReserveCancel     : SVProgressHUD.dismiss()
        case RequestAPI.apiChargingData         : SVProgressHUD.dismiss()
        case RequestAPI.apiInvoiceSettingList   : SVProgressHUD.dismiss()
        case RequestAPI.apiForReserveCheck      : SVProgressHUD.dismiss()
        case RequestAPI.apiForCardListData      : SVProgressHUD.dismiss()
        case RequestAPI.apiEditInvoiceSetting   : SVProgressHUD.dismiss()
        default                                 : SVProgressHUD.show()
        }
        
        
        printLog("======Start Request url: \(request.request?.url ?? URL(fileURLWithPath: "")) ======")
        printLog("======Http Body: \(String(data: request.request?.httpBody ?? Data(), encoding: String.Encoding.utf8)) ======")
        
        printLog("======Http Url: \(request.request?.url?.absoluteString) ======")
        
        request.responseData { (responseData) in
            Alamofire.Session.default.session.getAllTasks { (tasks) in
                if tasks.count > 0 {
                    
                } else {
                    SVProgressHUD.dismiss()
                }
                //            tasks.forEach({$0.cancel()})
            }
            
            printLog("======End Request: \(request.request?.url ?? URL(fileURLWithPath: ""))======")
            printLog("======Http Url: \(request.request?.url?.absoluteString) ====== \(String(data: responseData.data ?? Data(), encoding: String.Encoding.utf8))")
            
            
            switch apiType {
            case .apiForMajiStationDetail:
                printLog("")
            case .apiForCarChargingStatus, .apiPushGetList:
                printLog("")
            case .apiPorscheCarList, .apiViewSubscription:
                /// Subscripation
                printLog("")
            case .apiForBeginCharging, .apiForChargingStatus, .apiForStopCharging, .apiForChargingOvertimeStatus, .apiChargingData:
                /// charging 
                printLog("")
            case .apiScanQrCode, .apiPartner, .apiConfirmPartner, .apiGenerateQrCode, .apiCheckPartner, .apiDeletePartner:
                /// scan partner
                printLog("")
            case .apiForAddCard, .apiInvoiceSettingList, .apiEditInvoiceSetting:
                /// payment
                printLog("")
            case .apiGetVoucher:
                /// Voucher
                printLog("")
            case .apiChargingHistory, .apiPorscheChargingResult:
                /// Charging History
                printLog("")
            case .apiSetStationHistory,  .apiStationSearch:
                /// Main get user infomation  /// Main - serch bar
                printLog("")
            case .apiForReserveCheck, .apiForReserveCancel:
                // reserve
                printLog("")
            case .apiSetInvoiceSettings:
                // InvoiceSettings
                printLog("")
            case .apiChangeLanguage:
                // Main Language
                printLog("")
            default:
                print("responseData.error?1234 = \(responseData.error)")
                
                if self.isErrorCode(data: responseData.data, localDescription: responseData.error?.localizedDescription, apiType: apiType) == false {
                    completion(nil)
                    return
                }
            }
            
            
            if let data = responseData.data {
                let newStr = String(data: data, encoding: String.Encoding.utf8)
                let decoder = JSONDecoder()
                switch apiType {
                case .apiForVersionCheck:
                    if let response = try? decoder.decode(VersionCheckClass.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiForSystemVerb:
                    if let response = try? decoder.decode(SystemVerbClass.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiForProfile:
                    if let response = try? decoder.decode(ProfileClass.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiForChargeFeeList:
                    if let response = try? decoder.decode(ChargeFeeListClass.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiForSpotFormatList:
                    if let response = try? decoder.decode(SpotFormatListClass.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiForStationList:
                    if let response = try? decoder.decode(StationListClass.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiForStationDetail:
                    if let response = try? decoder.decode(StationDetailClass.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                    
                case .apiForMajiStationDetail:
                    do{
                        try decoder.decode(MajiStationDetailClass.self, from: data)
                    }
                    catch {
                        print(error)
                    }
                    if let response = try? decoder.decode(MajiStationDetailClass.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiForChargeList:
                    if let response = try? decoder.decode(ChargeListClass.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiForChargeStatistics:
                    if let response = try? decoder.decode(ChargeStatisticsClass.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiForCardList:
                    if let response = try? decoder.decode(CardListClass.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiForCardListData:
                    if let response = try? decoder.decode(CardListDataModel.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiForSocketList:
                    if let response = try? decoder.decode(SocketListClass.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiForChargerPair:
                    if let response = try? decoder.decode(ChargerPairClass.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiForChargerPairing:
                    if let response = try? decoder.decode(ChargerPairingModel.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiForChargingListData:
                    if let response = try? decoder.decode(ChargingListClass.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiForBeginCharging:
                    if let response = try? decoder.decode(BeginChargingModel.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiForChargingStatus:
                    if let response = try? decoder.decode(ChargingStatusClass.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiForCarChargingStatus:
                    if let response = try? decoder.decode([ChargingStatusClass].self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
//                case .apiForChargingResult,.apiCarChargingResult:
//                    if let response = try? decoder.decode(ChargingResultClass.self, from: data) {
//                        completion(response)
//                    }else {
//                        completion(nil)
//                    }
                case .apiForTaxIDList:
                    if let response = try? decoder.decode(TaxIDListClass.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiForMbCodeList:
                    if let response = try? decoder.decode(MbCodeListClass.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiForQuestionList:
                    if let response = try? decoder.decode(QuestionListClass.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiForQuestionDetail:
                    if let response = try? decoder.decode(QuestionDetailClass.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiForChangePW:
                    if let response = try? decoder.decode(ChangePWClass.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiPushGetList,.apiForNewsList:
                    if let response = try? decoder.decode(NewsListClass.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiForNewsDetail:
                    if let response = try? decoder.decode(NewsDetailClass.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiForShowSubscribe:
                    if let response = try? decoder.decode(ShowSubscribeClass.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiForShowPreferences:
                    if let response = try? decoder.decode(ShowPreferencesClass.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiForChargingReportList:
                    if let response = try? decoder.decode(ChargingReportListClass.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiForSevenDaysCharge:
                    if let response = try? decoder.decode(SevenDaysChargeClass.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiForParkingLockDown,.apiCheckCarLockStatus,.apiChargingParkingLockUp:
                    if let response = try? decoder.decode(ParkingLockDownModel.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiForCheckPhone:
                    if let response = try? decoder.decode(CheckPhoneClass.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiForAppLogin,.AppBusinessLogin:
                    if let response = try? decoder.decode(AppLoginClass.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiForReservePrepare:
                    if let response = try? decoder.decode(ReservePrepareClass.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiForReserveList:
                    if let response = try? decoder.decode(ReserveListClass.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiForReserveDetail:
                    if let response = try? decoder.decode(ReserveDetailClass.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiForChargingOvertimeStatus:
                    if let response = try? decoder.decode(ReserveDetailClass.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiGetNewsBadge:
                    if let response = try? decoder.decode(NewTypeCountModel.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                case .apiForGoogleMapRoute:
                    if let response = try? decoder.decode(GoogleMapRouteModel.self, from: data) {
                        completion(response)
                    } else {
                        completion(nil)
                    }
                case .apiForMaskMap:
                    if let response = try? decoder.decode(MaskDetailModel.self, from: data) {
                        completion(response)
                    } else {
                        completion(nil)
                    }
                case .apiForAddEasyCardReserve:
                    if let response = try? decoder.decode(EsayCardModel.self, from: data) {
                        completion(response)
                    } else {
                        completion(nil)
                    }
                case .apiForQueryLicensePlate:
                    if let response = try? decoder.decode(QueryLicensePlateModel.self, from: data) {
                        completion(response)
                    } else {
                        completion(nil)
                    }
                    break
                case .apiGenerateQrCode:
                    if let response = try? decoder.decode(GenerateQrCodeModel.self, from: data) {
                        completion(response)
                    } else {
                        completion(nil)
                    }
                    break
                case .apiGetVoucher:
                    if let response = try? decoder.decode(GetVoucherModel.self, from: data) {
                        completion(response)
                    } else {
                        completion(nil)
                    }
                    break
                case .apiSubscriptionCar:
                    if let response = try? decoder.decode(SubscriptionCarModel.self, from: data) {
                        completion(response)
                    } else {
                        completion(nil)
                    }
                    break
                case .apiPorscheCarList:
                    if let response = try? decoder.decode(PorscheCarListModel.self, from: data) {
                        completion(response)
                    } else {
                        completion(nil)
                    }
                    break
                case .apiPartner:
                    if let response = try? decoder.decode(PartnerModel.self, from: data) {
                        completion(response)
                    } else {
                        completion(nil)
                    }
                    break
                case .apiInvoiceSettingList:
                    if let response = try? decoder.decode(InvoiceSettingListModel.self, from: data) {
                        completion(response)
                    } else {
                        completion(nil)
                    }
                    break
                case .apiCheckPartner:
                    if let response = try? decoder.decode(CheckPartnerModel.self, from: data) {
                        completion(response)
                    } else {
                        completion(nil)
                    }
                    break
                case .apiChargingHistory:
                    if let response = try? decoder.decode(ChargingHistoryModel.self, from: data) {
                        completion(response)
                    } else {
                        completion(nil)
                    }
                    break
                case .apiPorscheChargingResult:
                    if let response = try? decoder.decode(PorscheChargingResultModel.self, from: data) {
                        completion(response)
                    } else {
                        completion(nil)
                    }
                    break
                case .apiViewSubscription:
                    if let response = try? decoder.decode(SubscriptionCarModel.self, from: data) {
                        completion(response)
                    } else {
                        completion(nil)
                    }
                    break
                case .apiStationSearch:
                    if let response = try? decoder.decode(StationSearchModel.self, from: data) {
                        completion(response)
                    } else {
                        completion(nil)
                    }
                    break
                case .apiPorscheLogin:
                    if let response = try? decoder.decode(PorscheLoginModel.self, from: data) {
                        completion(response)
                    } else {
                        completion(nil)
                    }
                    break
                case .apiForReserveStart:
                    if let response = try? decoder.decode(ReserveStartModel.self, from: data) {
                        completion(response)
                    } else {
                        completion(nil)
                    }
                    break
                case .apiChargingData:
                    if let response = try? decoder.decode(ChargingDataModel.self, from: data) {
                        completion(response)
                    } else {
                        completion(nil)
                    }
                    break
                case .apiForReserveCheck:
                    if let response = try? decoder.decode(ReserveCheckModel.self, from: data) {
                        completion(response)
                    } else {
                        completion(nil)
                    }
                    break
                case .apiForFbLogin,
                     .apiForCheckEmail,
                     .apiForLogout,
                     .apiForAddFavourite,
                     .apiForStationCreate,
                     .apiForChargingReport,
                     .apiForEditCard,
                     .apiForApplyStation,
                     .apiForSetPreferences,
                     .apiForStopCharging,
                     .apiForAddCard,
                     .apiForEditTaxID,
                     .apiForDeleteCard,
                     .apiForEditMbCode,
                     .apiForDelMbCode,
                     .apiForAddMbCode,
                     .apiForVerifyPhone,
                     .apiForAppMemberRegister,
                     .apiForEditProfile,
                     .apiForCancelPair,
                     .apiForChangePhone,
                     .apiForGoogleLogin,
                     .apiForChangeEmail,
                     .apiForSetDefaultEmail,
                     .apiForUploadMemberPic,
                     .apiForRepay,
                     .apiForReserveCancel,
                     .apiForAddLineRegKey,
                     .apiForSetSubscribe,
                     .apiForAddLineReserve,
                     .apiUpdateFcmToken,
                     .apiPushLogOut,
                     .apiPushSetRead,
                     .apiForAppleLogin,
                     .apiForAddLineConfirm,
                     .apiNewsBatchReadDelete,
                     .apiForBindLicensePlate,
                     .apiForAddRemarkLicensePlate,
                     .apiScanQrCode,
                     .apiDeletePartner,
                     .apiEditInvoiceSetting,
                     .apiConfirmPartner,
                     .apiSetStationHistory,
                     .apiChangeLanguage,
                     .apiCheckTerms,
                     .apiPushDelete,
                     .apiForAddEasyCardConfirm:
                    if let response = try? decoder.decode(NormalClass.self, from: data) {
                        completion(response)
                    }else {
                        completion(nil)
                    }
                default:
                    completion(nil)
                }
            }else {
                completion(nil)
            }
        }
    }
    
    func isErrorCode(data: Data?, localDescription: String?, apiType: RequestAPI) -> Bool {
        if apiType == .apiForDeviceUpload {
            return false
        }
        
        print("localDescription: \(localDescription)")
        guard let data = data else {
            //data如為nil則顯示"Error"
//            ShowMessageView.showMessage(title: nil, message: "Error")
            return false
        }
        guard let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
            if let localDescription = localDescription {
                if localDescription.contains("無法完成作業。軟體導致連線失敗") {
                    ShowMessageView.showMessage(title: nil, message: "app連線失敗，還請重新操作一次。")
                } else {
                    ShowMessageView.showMessage(title: nil, message: localDescription)
                }
            } else {
                //localDescription為nil會顯示"Unknow Error"
//                ShowMessageView.showMessage(title: nil, message: "Unknow Error")
            }
            return false
        }
        guard var resCode = json?["resCode"] as? String else {
            //            ShowMessageView.showMessage(title: nil, message: "Unknow")
            var statusStr = ""
            if let status = json?["status"] as? Int {
                statusStr = "\(status)"
            }else {
                statusStr = ""
            }
            let message = json?["error"] as? String ?? "Unknow Error"
//            ShowMessageView.showMessage(title: statusStr, message: message)
            return false
        }
        
        if apiType == .apiForParkingLockDown  || apiType == .apiChargingParkingLockUp {
            if resCode == "0119" {
                resCode = "0000"
            }
        }
        if resCode != "0000" {
            let msg = json?["resMsg"] as? String ?? ""
            if msg.count > 0 {
                // 登出
                if resCode == "0109" {
                    if KJLRequest.shareInstance().token == "" {
                        return false
                    }
                    UIViewController.pushLogOut()
                    KJLRequest.logout()
                    ShowMessageView.showMessage(title: nil, message: msg, completion: { (isOK) in
                        if TabBar != nil {
                            let viewController = TabBar!
                            if viewController.presentedViewController != nil { viewController.presentedViewController?.dismiss(animated: false, completion: nil)
                            }
                            let indexView:UINavigationController = viewController.viewControllers![TabBar!.selectedIndex] as! UINavigationController
                            let lastViewController = indexView.viewControllers.last
                            if lastViewController?.presentedViewController != nil { lastViewController!.presentedViewController?.dismiss(animated: false, completion: nil)
                            }
                           
                            if let nav = lastViewController?.navigationController {
                                nav.popViewController(animated: false)
                            }
                            
                            let subcontrol = indexView.viewControllers.first!
                            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SelectLoginViewController") as! SelectLoginViewController
                            let nav:UINavigationController = UINavigationController.init(rootViewController: vc)
                            nav.isNavigationBarHidden = true
                            nav.modalPresentationStyle = .overFullScreen
                            nav.modalTransitionStyle   = .crossDissolve
                            nav.hidesBottomBarWhenPushed = true
                            subcontrol.present(nav, animated: true, completion: nil)
                        }
                    })
                    return false
                } else if resCode == "0409" {
                    return false
                }
                
                switch apiType {
                case .apiForVerifyPhone:
                    ShowMessageView.showMessage(title: nil, message: "驗證碼錯誤，請再重新輸入或重新發送驗證碼")
                case .apiForGoogleLogin, .apiForFbLogin, .apiForAppleLogin:
                    return true
                case .apiForReservePrepare:
                    if (resCode == "0801") {
                        self.kJLRequestDelegate?.apiFailResultWith(resCode: resCode , message: msg)
                    } else {
                        ShowMessageView.showMessage(title: nil, message: msg)
                    }
                    break
                case .apiForChargerPair:
                    self.kJLRequestDelegate?.apiFailResultWith(resCode: resCode , message: msg)
                    break
                case .apiForChargerPairing:
                    self.kJLRequestDelegate?.apiFailResultWith(resCode: resCode , message: msg)
                    break
                case .apiForReserveCheck:
                    self.kJLRequestDelegate?.apiFailResultWith(resCode: resCode , message: msg)
                    break
                case .apiForBeginCharging:
                    if msg.contains("沒有此筆") ||  msg.contains("已逾時"){
                        NotificationCenter.default.post(name: .startChargError, object: nil, userInfo: nil)
                        ShowMessageView.showMessage(title: nil, message: msg)
                    } else {
                        ShowMessageView.showMessage(title: nil, message: msg)
                    }
                    break
                case .apiSubscriptionCar:
                    if resCode == "9000" {
                        return true
                    } else {
                        self.kJLRequestDelegate?.apiFailResultWith(resCode: resCode , message: msg)
                    }
                case .apiForAddCard:
                    if resCode == "9000" {
                        break
                    } else {
                        self.kJLRequestDelegate?.apiFailResultWith(resCode: resCode , message: msg)
                    }
                case .apiPorscheCarList:
                    if resCode == "9000" {
                        break
                    } else {
                        self.kJLRequestDelegate?.apiFailResultWith(resCode: resCode , message: msg)
                    } 
                default:
                    if msg != "DeviceId Error" {
                        ShowMessageView.showMessage(title: nil, message: msg)
                    }else{
                        UserDefaults.standard.removeObject(forKey: "deviceId")
                        UserDefaults.standard.synchronize()
                    }
                } 
            } else {
                ShowMessageView.showMessage(title: nil, message: "Error but no resMsg")
            }
            return false
        }
        
        return true
    }
    
    class func getHeader() -> HTTPHeaders {
        printLog("===token===:\(KJLRequest.shareInstance().token)")
        let headers : HTTPHeaders = ["Content-Type": "application/json;charset=UTF-8",
                                     "X-Auth_Token": KJLRequest.shareInstance().token]
        return headers
    }
}

extension KJLRequest {
    
    class func requestForPushSetRead(detailId:String,memberNo:String,taskId:String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["detailId":detailId,"memberNo":memberNo,"appId":"9","taskId":taskId]
        let request = AF.request(KJLRequest.RequestAPI.apiPushSetRead.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())

        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiPushSetRead, completion: completion)
    }
    
    class func requestForPushDelete(detailId:String,memberNo:String,taskId:String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["detailId":detailId,"memberNo":memberNo,"appId":"9","taskId":taskId]
        let request = AF.request(KJLRequest.RequestAPI.apiPushDelete.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())

        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiPushDelete, completion: completion)
    }
    
    class func requestForPushGetList(deviceId:String,memberNo:String,pageNum:Int,rowsPerPage:Int, completion: @escaping RequestCompletion) {
        let params : Parameters = ["memberNo":memberNo,
                                   "deviceId":deviceId,
                                   "appId":"9",
                                   "pageNum":pageNum,
                                   "rowsPerPage":rowsPerPage]
        let request = AF.request(KJLRequest.RequestAPI.apiPushGetList.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())

        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiPushGetList, completion: completion)
    }
    
    // 登出 token
    class func requestForPushLogOut(deviceId:String,memberNo:String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["memberNo":memberNo,"deviceId":deviceId,"appId":"9"]

        let request = AF.request(KJLRequest.RequestAPI.apiPushLogOut.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())

        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiPushLogOut, completion: completion)
    }
    
    // 更新 token
    class func requestForUpdateFcmToken(deviceId:String, fcmToken: String ,memberNo:String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["memberNo":memberNo,"fcmToken":fcmToken,"deviceId":deviceId,"appId":"9","osVersion": Bundle.main.infoDictionary?["CFBundleShortVersionString"] ?? "", "platform": "ios"]
        // ios_iphone_ver
        let request = AF.request(KJLRequest.RequestAPI.apiUpdateFcmToken.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())

        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiUpdateFcmToken, completion: completion)
    }
    
    // [001] 版本確認
    class func requestForVersionCheck(completion: @escaping RequestCompletion) {
        let params : Parameters = ["version": Bundle.main.infoDictionary?["CFBundleShortVersionString"] ?? "", "platform": KJLRequest.shareInstance().device]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForVersionCheck.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForVersionCheck, completion: completion)
    }
    
    // [002] 系統參數
    class func requestForSystemVerb(completion: @escaping RequestCompletion) {
        let params : Parameters = ["typeListVer": "v0", "locListVer": "v0"]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForSystemVerb.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForSystemVerb, completion: completion)
    }
    
    // [003] 裝置資訊上傳
    class func requestForDeviceUpload(devicdId:String, completion: @escaping RequestCompletion) {
        let model = "\(KJLRequest.shareInstance().device)_\(UIDevice.current.type.rawValue)_ \(UIDevice.current.systemVersion)"
        let params : Parameters = ["platform": KJLRequest.shareInstance().device,
                                   "deviceType": model,
                                   "deviceid": devicdId]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForDeviceUpload.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForDeviceUpload, completion: completion)
    }
    
    // [102] App企業登入
    class func requestForEnterpiseLogin(account: String, passWord: String, passWordWeb: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["account": account,
                                   "passWord": passWord,
                                   "passWordWeb": passWordWeb]
        let request = AF.request(KJLRequest.RequestAPI.AppBusinessLogin.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.AppBusinessLogin, completion: completion)
    }
    
    // [101] App會員註冊
    class func requestForAppMemberRegister(phoneNumber: String, email: String, nickName: String, sex: String, passWord: String , completion: @escaping RequestCompletion) {
        let params : Parameters = ["phoneNumber": phoneNumber,
                                   "email": email,
                                   "nickName": nickName,
                                   "sex": sex,
                                   "passWord": passWord] // 採SHA256的hash密文
        
        let request = AF.request(KJLRequest.RequestAPI.apiForAppMemberRegister.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForAppMemberRegister, completion: completion)
    }
    
    // [102] App會員登入
    class func requestForAppLogin(phoneNumber: String, passWord: String, passWordWeb: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["phoneNumber": phoneNumber,
                                   "passWord": passWord,
                                   "passWordWeb": passWordWeb]
        // password:採SHA256的hash密文 , passWordWeb: 採SHA512的hash密文
        //        let params : Parameters = ["phoneNumber": "0900123456",
        //                                   "passWord": "test01"] // 採SHA256的hash密文
        
        let request = AF.request(KJLRequest.RequestAPI.apiForAppLogin.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForAppLogin, completion: completion)
    }
    
    // [103] Facebook登入
    class func requestForFbLogin(fbToken: String, fbId: String, email: String, name: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["fbToken": fbToken,
                                   "fbId": fbId,
                                   "fbEmail": email,
                                   "fbNickname": name,]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForFbLogin.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForFbLogin, completion: completion)
    }
    
    // [104] Google登入
    class func requestForGoogleLogin(gToken: String, gmail: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["gtoken": gToken,
                                   "gmail": gmail]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForGoogleLogin.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForGoogleLogin, completion: completion)
    }
    
    // [105] 登出
    class func requestForLogout(completion: @escaping RequestCompletion) {
        let request = AF.request(KJLRequest.RequestAPI.apiForLogout.requestURL(nil), method: .post, parameters: nil, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForLogout, completion: completion)
    }
    
    // [106] Apple sign in
    class func requestForAppleLogin(appleID: String, appleNickname: String, appleEmail: String? = nil , completion: @escaping RequestCompletion) {
        let params : Parameters = ["appleId": appleID,
                                   "appleNickname": appleNickname,
                                   "appleEmail": appleEmail]
        let request = AF.request(KJLRequest.RequestAPI.apiForAppleLogin.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForAppleLogin, completion: completion)
    }
    
    // [111] 發送電話驗證
    class func requestForCheckPhone(regType: String, regCode: String, phoneNumber: String, email: String = "", completion: @escaping RequestCompletion) {
        let params : Parameters = ["regType": regType,
                                   "regCode": regCode,
                                   "phoneNumber": phoneNumber,
                                   "fbEmail": email, ]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForCheckPhone.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForCheckPhone, completion: completion)
    }
    
    // [112] 確認電話驗證碼
    class func requestForVerifyPhone(phoneNumber: String, verifyCode: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["phoneNumber": phoneNumber,
                                   "verifyCode": verifyCode]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForVerifyPhone.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForVerifyPhone, completion: completion)
    }
    
    // [121] 驗證Email
    class func requestForCheckEmail(type: KJLRequest.RequestLoginType, email: String, phoneNumber: String,  completion: @escaping RequestCompletion) {
        let params : Parameters = ["type": type.rawValue,
                                   "email": email,
                                   "phoneNumber": phoneNumber,
                                   "bmwId": "",
                                   "emovingId":"",]
        
        // 忘記密碼：ForgotPW,驗證Email：CheckEmail
        //        params["type"] = type.rawValue
        
        let request = AF.request(KJLRequest.RequestAPI.apiForCheckEmail.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForCheckEmail, completion: completion)
    }
    
    // [201] 查詢基本資料
    class func requestForProfile(completion: @escaping RequestCompletion) {
        let request = AF.request(KJLRequest.RequestAPI.apiForProfile.requestURL(nil), method: .post, parameters: nil, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForProfile, completion: completion)
    }
    
    // [202] 變更基本資料
    class func requestForEditProfile(nickName: String, sex: String, carNo: String?, backupEmail: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["nickName": nickName ?? "",
                                   "backupEmail" : backupEmail,
                                   "sex": sex ?? "",
                                   "carNo": carNo ?? ""]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForEditProfile.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForEditProfile, completion: completion)
    }
    
    // [203] 變更密碼
    class func requestForChangePW(type: KJLRequest.RequestChangePW, passWord: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["type": type.rawValue,
                                   "passWord": passWord]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForChangePW.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForChangePW, completion: completion)
    }
    
    // [204] 上傳大頭照
    class func requestForUploadMemberPic(picBytes: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["picBytes": picBytes]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForUploadMemberPic.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForUploadMemberPic, completion: completion)
    }
    
    // [205] 變更手機
    class func requestForChangePhone(phoneOld: String, phoneNew: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["phoneOld": phoneOld,
                                   "phoneNew": phoneNew]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForChangePhone.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForChangePhone, completion: completion)
    }
    
    // [206] 變更信箱
    class func requestForChangeEmail(emailType: KJLRequest.RequestLoginType, emailOld: String, emailNew: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["emailType": emailType.rawValue,
                                   "emailOld": emailOld,
                                   "emailNew": emailNew]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForChangeEmail.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForChangeEmail, completion: completion)
    }
    
    // [207] 設為預設信箱
    class func requestForSetDefaultEmail(email: String , emailType: KJLRequest.RequestLoginType , completion: @escaping RequestCompletion) {
        let params : Parameters = ["email": email,
                                   "emailType": emailType.rawValue]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForSetDefaultEmail.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForSetDefaultEmail, completion: completion)
    }
    
    /// [208] 查詢綁定車牌
    class func requestForQueryLicensePlate(completion: @escaping RequestCompletion) {
        let request = AF.request(KJLRequest.RequestAPI.apiForQueryLicensePlate.requestURL(nil), method: .post, parameters: nil, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())

        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForQueryLicensePlate, completion: completion)
    }
    
    /// [209] 綁定/解綁定車牌
    class func requestForBindLicensePlate(_ phone: String, _ plate: String, _ percentage: Int, bind: Bool,completion: @escaping RequestCompletion) {
        let params : Parameters = ["registerPhone": phone,
                                   "licensePlate": plate,
                                   "chargeLimit": percentage,
                                   "bind": bind]
        let request = AF.request(KJLRequest.RequestAPI.apiForBindLicensePlate.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())

        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForBindLicensePlate, completion: completion)
    }
    
    /// [210] 綁定車牌備註
    class func requestForAddRemarkLicensePlate(_ plate: String,_ phone: String , _ remark: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["registerPhone": phone,
                                   "licensePlate": plate,
                                   "remark": remark, ]
        let request = AF.request(KJLRequest.RequestAPI.apiForAddRemarkLicensePlate.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())

        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForAddRemarkLicensePlate, completion: completion)
    }
    
    // [301] 站點查詢
    class func requestForStationList(queryType: RequestQueryType, queryString: String?, type: String ,completion: @escaping RequestCompletion) {
        // Station：充電樁列表, Favourite：我的最愛
        let params : Parameters = ["queryType": queryType.rawValue,
                                   "queryString": queryString ?? "",
                                   "type": type]
        
        var headers = KJLRequest.getHeader()
        var token = headers["X-Auth_Token"] ?? ""
        if token.count <= 0 {
            token = "none"
            headers["X-Auth_Token"] = token
        }
        
        let request = AF.request(KJLRequest.RequestAPI.apiForStationList.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers)
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForStationList, completion: completion)
    }
    
    // [302] 站點明細
    class func requestForStationDetail(stationId: String, completion: @escaping RequestCompletion) {
        let lang = UserDefaults.standard.string(forKey: "UserLanguage")
        let language = lang == "en" ? "en" : "zh"
        let params : Parameters = ["stationId": stationId, 
                                   "lang" : language,]
        
        var headers = KJLRequest.getHeader()
        var token = headers["X-Auth_Token"] ?? ""
        if token.count <= 0 {
            token = "none"
            headers["X-Auth_Token"] = token
        } 
        
        let request = AF.request(KJLRequest.RequestAPI.apiForStationDetail.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers)
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForStationDetail, completion: completion)
    }
    
    // 车麻吉站點明細
    class func requestForStationDetailByMaji(majiId: String, majiToken: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["ids": majiId]
        
        let headers : HTTPHeaders = [
            //            "Content-Type": "application/json;charset=UTF-8",
            "X-Autopass-Thirdparty-Client-Id": "1234567890",
            "Authorization": "Bearer " + majiToken
        ]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForMajiStationDetail.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers)
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForMajiStationDetail, completion: completion)
    }
    
    // [303] 充電回報
    class func requestForChargingReport(stationId: String, reportType: RequestReportType, comment: String, completion: @escaping RequestCompletion) {
        // OK：充電成功, Fail：充電失敗, Charging：充電中, Other：其他
        let params : Parameters = ["stationId": stationId,
                                   "reportType": reportType.rawValue,
                                   "comment": comment]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForChargingReport.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForChargingReport, completion: completion)
    }
    
    // [304] 加入我的最愛
    class func requestForAddFavourite(stationId: String, add: RequestBOOL, completion: @escaping RequestCompletion) {
        let params : Parameters = ["stationId": stationId,
                                   "add": add.rawValue]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForAddFavourite.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForAddFavourite, completion: completion)
    }
    
    // [305] 分類篩選條件
    // 手機可內存，有更動時呼叫更新
    class func requestForSetPreferences(useDefault: KJLRequest.RequestBOOL, showFavourite: RequestBOOL, mapDetail: RequestMapDetail, currentTypes: RequestCurrentTypes, chargeTypes: RequestChargeTypes, interfaceTypesAC: String, interfaceTypesDC: String, stationStatus: Bool , stationOpenType: RequestStationOpenType , stationReserve: Bool , businessTime: Bool, parkingCoupon: RequestParkingCouponType , chargePayType: RequestChargePayType , socketBrand: String, type: String , completion: @escaping RequestCompletion) {
        var params : Parameters = [:]
//        if useDefault == .TRUE {
//            params = ["useDefault": useDefault.rawValue , "type": type]
//
//        } else {
            params = ["useDefault": useDefault.rawValue , 
                      "showFavourite": showFavourite.rawValue,
                      "mapDetail": mapDetail.rawValue,
                      "currentTypes": currentTypes.rawValue,
                      "chargeTypes": chargeTypes.rawValue,
                      "interfaceTypesAC": interfaceTypesAC,
                      "interfaceTypesDC": interfaceTypesDC,
                      "stationStatus": stationStatus,
                      "stationOpenType": stationOpenType.rawValue,
                      "stationReserve": stationReserve,
                      "businessTime" : businessTime,
                      "parkingCoupon": parkingCoupon.rawValue,
                      "chargePayType": chargePayType.rawValue,
                      "socketBrand": socketBrand,
                      "type": type] // 充電樁類型,用半形逗號區隔
//        }
        print("params: \(params)")
        let request = AF.request(KJLRequest.RequestAPI.apiForSetPreferences.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForSetPreferences, completion: completion)
    }
    
    // [306] 充電樁列表
    class func requestForSocketList(stationId: String, stationCategory: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["stationId": stationId,
                                   "stationCategory": stationCategory]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForSocketList.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForSocketList, completion: completion)
    }
    
    // [307] 查詢分類篩選
    class func requestForShowPreferences(type: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["type": type]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForShowPreferences.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForShowPreferences, completion: completion)
    }
    
    // [308] 查詢充電回報
    class func requestForChargingReportList(stationId: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["stationId": stationId]
        
        var headers = KJLRequest.getHeader()
        var token = headers["X-Auth_Token"] ?? ""
        if token.count <= 0 {
            token = "none"
            headers["X-Auth_Token"] = token
        }
        
        let request = AF.request(KJLRequest.RequestAPI.apiForChargingReportList.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers)
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForChargingReportList, completion: completion)
    }
    
    // [309] 充電站新建
    class func requestForStationCreate(inputDicArray: [String: String], stationPic: [String] , completion: @escaping RequestCompletion) {
        
        let params : Parameters = ["chargeFee": inputDicArray["chargeFee"] ?? "",
                                   "note": inputDicArray["note"] ?? "",
                                   "spotFloor": inputDicArray["spotFloor"] ?? "",
                                   "spotFormat": inputDicArray["spotFormat"] ?? "",
                                   "spotSpace": inputDicArray["spotSpace"] ?? "",
                                   "spotVendor": inputDicArray["spotVendor"] ?? "",
                                   "stationAddress": inputDicArray["stationAddress"] ?? "",
                                   "stationLatitude": inputDicArray["stationLatitude"] ?? "",
                                   "stationLongitude": inputDicArray["stationLongitude"] ?? "",
                                   "stationName": inputDicArray["stationName"] ?? "",
                                   "stationPhone": inputDicArray["stationPhone"] ?? "",
                                   "stationPic": stationPic ]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForStationCreate.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForStationCreate, completion: completion)
    }
    
    // [310] 近七天充電次數統計
    class func requestForSevenDaysCharge(stationId: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["stationId": stationId]
        
        var headers = KJLRequest.getHeader()
        var token = headers["X-Auth_Token"] ?? ""
        if token.count <= 0 {
            token = "none"
            headers["X-Auth_Token"] = token
        }
        
        let request = AF.request(KJLRequest.RequestAPI.apiForSevenDaysCharge.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers)
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForSevenDaysCharge, completion: completion)
    }
    
    
    // [311] 充電費率列表 :
    class func requestForChargeFeeList(completion: @escaping RequestCompletion) {
        let request = AF.request(KJLRequest.RequestAPI.apiForChargeFeeList.requestURL(nil), method: .post, parameters: nil, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForChargeFeeList, completion: completion)
    }
    
    // [312] 充電樁規格列表
    class func requestForSpotFormatList(completion: @escaping RequestCompletion) {
        let request = AF.request(KJLRequest.RequestAPI.apiForSpotFormatList.requestURL(nil), method: .post, parameters: nil, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForSpotFormatList, completion: completion)
    }
    
    // [401] 配對/準備充電 Step1
    // 配對充電樁，成功後進入準備充電畫面
    class func requestForChargerPair(qrNo: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["qrNo": qrNo] // 充電槍 QR CODE NO
        
        let request = AF.request(KJLRequest.RequestAPI.apiForChargerPair.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForChargerPair, completion: completion)
    }
    
    // [401-?] 配對/準備充電 Step1
    // 配對充電樁，成功後進入準備充電畫面
    class func requestForChargerPairing(qrNo: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["qrNo": qrNo] // 充電槍 QR CODE NO
        
        let request = AF.request(KJLRequest.RequestAPI.apiForChargerPairing.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForChargerPairing, completion: completion)
    }
    
    // [402] 開始充電 Step2
    // 成功條件：
    // 信用卡預授權成功
    // OCPP API回應開始充電成功
    class func requestForBeginCharging(qrNo: String, cardNo: String , mifareCard: Bool = false, completion: @escaping RequestCompletion) {
        let params : Parameters = ["qrNo": qrNo,
                                   "cardNo": cardNo,
                                   "mifareCard": mifareCard]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForBeginCharging.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForBeginCharging, completion: completion)
    }
    
    // [403] 充電狀態查詢 Step3
    // status為Finish或Interrupt則接著查詢404進入結果畫面
    class func requestForChargingStatus(tranNo: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["tranNo": tranNo]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForChargingStatus.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForChargingStatus, completion: completion)
    }
    
    // [5403] 充電狀態查詢 Step3
    // status為Finish或Interrupt則接著查詢404進入結果畫面
    class func requestForCarChargingStatus(tranNo: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["tranNo": tranNo]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForCarChargingStatus.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForCarChargingStatus, completion: completion)
    }
    
    // ChargingOvertimeStatu
    class func checkChargingOvertimeStatus(tranNo: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["tranNo": tranNo]
        let request = AF.request(KJLRequest.RequestAPI.apiForChargingOvertimeStatus.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForChargingOvertimeStatus, completion: completion)
    }
    
    // ChargingOvertimeStatu
    class func checkChargingParkingLockUp(tranNo: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["tranNo": tranNo]
        let request = AF.request(KJLRequest.RequestAPI.apiChargingParkingLockUp.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiChargingParkingLockUp, completion: completion)
    }
    
    // [552] AddLineRegKey
    class func requestForAddLineRegKey(params: Parameters, completion: @escaping RequestCompletion) {
        
        let request = AF.request(KJLRequest.RequestAPI.apiForAddLineRegKey.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForAddLineRegKey, completion: completion)
    }
    
    // [404] 停止充電
//    class func requestForChargingResult(tranNo: String, delay: Bool, completion: @escaping RequestCompletion) {
//        let params : Parameters = ["tranNo": tranNo,
//                                   "delay" : delay ]
//
//        let request = AF.request(KJLRequest.RequestAPI.apiForChargingResult.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
//
//        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForChargingResult, completion: completion)
//    }
    
    // [5405] 充電結果
    class func requestForCarChargingResult(tranNo: String, delay: Bool, completion: @escaping RequestCompletion) {
        let params : Parameters = ["tranNo": tranNo,
                                   "delay" : delay ]
        
        let request = AF.request(KJLRequest.RequestAPI.apiCarChargingResult.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiCarChargingResult, completion: completion)
    }
    
    // [405] 充電結果 Step4
    class func requestForStopCharging(tranNo: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["tranNo": tranNo]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForStopCharging.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForStopCharging, completion: completion)
    }
    
    // [406] 充電紀錄列表
    class func requestForChargeList(startTime: String, endTime: String, payment: RequestPayment, completion: @escaping RequestCompletion) {
        let params : Parameters = ["startTime": startTime,
                                   "endTime": endTime,
                                   "payment": payment.rawValue]
        
        
        let request = AF.request(KJLRequest.RequestAPI.apiForChargeList.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForChargeList, completion: completion)
    }
    
    // [407] 充電紀錄統計
    class func requestForChargeStatistics(startTime: String, endTime: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["startTime": startTime,
                                   "endTime": endTime]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForChargeStatistics.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForChargeStatistics, completion: completion)
    }
    
    // [408] 查詢充電中列表
    class func requestForChargingListData(completion: @escaping RequestCompletion) {
        let request = AF.request(KJLRequest.RequestAPI.apiForChargingListData.requestURL(nil), method: .post, parameters: nil, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForChargingListData, completion: completion)
    }
    
    // [409] 取消配對 Step1 --- 後端有自動刪單功能 暫時不使用
    class func requestForCancelPair(tranNo: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["tranNo": tranNo]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForCancelPair.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForCancelPair, completion: completion)
    }
    
    // [410] 取消配對 ParkingLockDown
    class func requestForParkingLockDown(qrNo: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["qrNo": qrNo ]
        
        var headers = KJLRequest.getHeader()
        var token = headers["X-Auth_Token"] ?? ""
        if token.count <= 0 {
            token = "none"
            headers["X-Auth_Token"] = token
        }
        
        let request = AF.request(KJLRequest.RequestAPI.apiForParkingLockDown.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers)
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForParkingLockDown, completion: completion)
    }
    
    // [5413] 查詢狀態
    class func checkCarLockStatus(qrNo: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["qrNo": qrNo]
        
        var headers = KJLRequest.getHeader()
        var token = headers["X-Auth_Token"] ?? ""
        if token.count <= 0 {
            token = "none"
            headers["X-Auth_Token"] = token
        }
        
        let request = AF.request(KJLRequest.RequestAPI.apiCheckCarLockStatus.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers)
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiCheckCarLockStatus, completion: completion)
    }

    // [501] 查詢信用卡
    class func requestForCardList(completion: @escaping RequestCompletion) {
        let request = AF.request(KJLRequest.RequestAPI.apiForCardList.requestURL(nil), method: .post, parameters: nil, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForCardList, completion: completion)
    }
    
    // new[501] 查詢信用卡
       class func requestForCardListData(completion: @escaping RequestCompletion) {
           let request = AF.request(KJLRequest.RequestAPI.apiForCardListData.requestURL(nil), method: .post, parameters: nil, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
           
           KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForCardListData, completion: completion)
       }
    
    // [502] 新增信用卡
    class func requestForAddCard(cardNum: String, expiry: String, checkCode: String, completion: @escaping RequestCompletion) {
        // cardNum=9527-1130-2384-9487, expiry=202505, checkCode=398
        let params : Parameters = ["cardNum": cardNum,
                                   "expiry": expiry,
                                   "checkCode": checkCode]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForAddCard.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForAddCard, completion: completion)
    }
    
    // [503] 編輯信用卡
    class func requestForEditCard(cardNo: String, cardName: String, defaultStr: RequestBOOL, completion: @escaping RequestCompletion) {
        let params : Parameters = ["cardNo": cardNo,
                                   "cardName": cardName,
                                   "default": defaultStr.rawValue]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForEditCard.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForEditCard, completion: completion)
    }
    
    // [504] 刪除信用卡
    class func requestForDeleteCard(cardNo: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["cardNo": cardNo]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForDeleteCard.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForDeleteCard, completion: completion)
    }
    
    // [511] 查詢統一編號
    // 最多五組，按照taxIdNo（客戶統編序號 0~4）填入，若無該編號則為空白資料。
    class func requestForTaxIDList(completion: @escaping RequestCompletion) {
        let request = AF.request(KJLRequest.RequestAPI.apiForTaxIDList.requestURL(nil), method: .post, parameters: nil, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForTaxIDList, completion: completion)
    }
    
    // [512] 編輯統一編號
    // 最多五組
    class func requestForEditTaxID(taxIds: [[String: Any]], completion: @escaping RequestCompletion) {
        let params : Parameters = ["taxIds": taxIds]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForEditTaxID.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForEditTaxID, completion: completion)
    }
    
    // [513] Linepay 註冊
    class func requestLinePayRegister(completion: @escaping RequestCompletion) {
        
        let params : Parameters = ["confirmURL": "yescharge://LinePay?status=true",
                                   "cancelURL": "yescharge://LinePay?status=false"]
        let request = AF.request(KJLRequest.RequestAPI.apiForAddLineReserve.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForAddLineReserve, completion: completion)
    }
    
    // [514] Linepay 請求
    class func requestLinePayConfirm(transactionId:String,completion: @escaping RequestCompletion) {
        let params : Parameters = ["transactionId": transactionId]
        let request = AF.request(KJLRequest.RequestAPI.apiForAddLineConfirm.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForAddLineConfirm, completion: completion)
    }
    
    // [515]
    class func requestEasyCardRegister(completion: @escaping RequestCompletion) { 
        let params : Parameters = ["confirmURL": "yescharge://EasyPay", ]
        let request = AF.request(KJLRequest.RequestAPI.apiForAddEasyCardReserve.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForAddEasyCardReserve, completion: completion)
    }
    
    // [516]
    class func requestEasyCardConfirm(transactionId:String,completion: @escaping RequestCompletion) {
        let params : Parameters = ["intentId": transactionId]
        let request = AF.request(KJLRequest.RequestAPI.apiForAddEasyCardConfirm.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForAddEasyCardConfirm, completion: completion)
    }
    
    // [521] 查詢手機條碼
    class func requestForMbCodeList(completion: @escaping RequestCompletion) {
        let request = AF.request(KJLRequest.RequestAPI.apiForMbCodeList.requestURL(nil), method: .post, parameters: nil, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForMbCodeList, completion: completion)
    }
    
    // [522] 新增手機條碼
    class func requestForAddMbCode(name: String, code: String, defaultStr: RequestBOOL?, completion: @escaping RequestCompletion) {
        let params : Parameters = ["name": name,
                                   "code": code,
                                   "default": defaultStr?.rawValue ?? ""]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForAddMbCode.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForAddMbCode, completion: completion)
    }
    
    // [523] 編輯手機條碼
    class func requestForEditMbCode(no: String, name: String?, code: String?, defaultStr: RequestBOOL, completion: @escaping RequestCompletion) {
        let params : Parameters = ["no": no,
                                   "name": name ?? "",
                                   "code": code ?? "",
                                   "default": defaultStr.rawValue]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForEditMbCode.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForEditMbCode, completion: completion)
    }
    
    // [524] 刪除手機條碼
    class func requestForDelMbCode(no: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["no": no]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForDelMbCode.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForDelMbCode, completion: completion)
    } 
    
    // [541] 補扣款
    class func requestForpRepay(tranNo: Int , cardNo: Int , completion: @escaping RequestCompletion) {
        let params : Parameters = ["tranNo": tranNo ,
                                   "cardNo" : cardNo ]
        var headers = KJLRequest.getHeader()
        var token = headers["X-Auth_Token"] ?? ""
        if token.count <= 0 {
            token = "none"
            headers["X-Auth_Token"] = token
        }
        
        let request = AF.request(KJLRequest.RequestAPI.apiForRepay.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForRepay, completion: completion)
    }
    
    // [601] 查詢消息列表
    class func requestForNewsList(newsType:String,isInclude:Int, completion: @escaping RequestCompletion) {
        var headers = KJLRequest.getHeader()
        var token = headers["X-Auth_Token"] ?? ""
        if token.count <= 0 {
            token = "none"
            headers["X-Auth_Token"] = token
        }
        let parameters: Parameters = ["newsType":newsType,"isInclude":isInclude]
        let request = AF.request(KJLRequest.RequestAPI.apiForNewsList.requestURL(nil), method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForNewsList, completion: completion)
    }
    
    // [602] 查詢消息明細
    class func requestForNewsDetail(newsNo: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["newsNo": newsNo]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForNewsDetail.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForNewsDetail, completion: completion)
    }
    
    // [603] 編輯消息訂閱
    class func requestForSetSubscribe(useDefault: KJLRequest.RequestBOOL, type: String, loc: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["useDefault": useDefault.rawValue,
                                   "type": type,
                                   "loc": loc]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForSetSubscribe.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForSetSubscribe, completion: completion)
    }
    
    // [604] 查詢消息訂閱
    class func requestForShowSubscribe(completion: @escaping RequestCompletion) {
        let request = AF.request(KJLRequest.RequestAPI.apiForShowSubscribe.requestURL(nil), method: .post, parameters: nil, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForShowSubscribe, completion: completion)
    }
    
    // [605] 訊息刪除
    class func requestForNewsBatchReadDelete(delNews: Bool, jsonNewNo: [Dictionary<String, String>], completion: @escaping RequestCompletion) {
        let params : Parameters = ["list": jsonNewNo,
                               "delNews": delNews]
        let request = AF.request(KJLRequest.RequestAPI.apiNewsBatchReadDelete.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiNewsBatchReadDelete, completion: completion)
    }
    
    // [606] 分類未讀訊息數及推播數
    class func requestForGetNewsBadge(completion: @escaping RequestCompletion) {
        let request = AF.request(KJLRequest.RequestAPI.apiGetNewsBadge.requestURL(nil), method: .post, parameters: nil, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiGetNewsBadge, completion: completion)
    }
    
    // [701] 申請建樁
    class func requestForApplyStation(name: String, phone: String, loc: String, carType: String?, completion: @escaping RequestCompletion) {
        let params : Parameters = ["name": name,
                                   "phone": phone,
                                   "loc": loc, // 地區（由API-002取得）
            "carType": carType ?? ""] // 車型（由API-002取得）
        
        let request = AF.request(KJLRequest.RequestAPI.apiForApplyStation.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForApplyStation, completion: completion)
    }
    
    // [702] 查詢常見問題列表
    class func requestForQuestionList(completion: @escaping RequestCompletion) {
        let request = AF.request(KJLRequest.RequestAPI.apiForQuestionList.requestURL(nil), method: .post, parameters: nil, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForQuestionList, completion: completion)
    }
    
    // [703] 查詢常見問題明細
    class func requestForQuestionDetail(questionNo: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["questionNo": questionNo]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForQuestionDetail.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForQuestionDetail, completion: completion)
    }
    
    // [704] Google Map Route -- use Direction key
    class func requestForGoogleMapRoute(_ fromLatitude: String, _ fromLongitude: String, _ toLatitude: String, _ toLongitude: String, avoid: String, completion: @escaping RequestCompletion) {
        /// avoid = "" : "tolls" 收費站 : ""highways"" 高速公路 : "tolls%7Chighways" 避開二種
        let params : Parameters = ["fromLatitude": fromLatitude,
                                   "fromLongitude" : fromLongitude,
                                   "toLatitude" : toLatitude,
                                   "toLongitude" : toLongitude,
                                   "avoid" : avoid,
                                   "platform" : "iOS",
                                    ]
        
        let request = AF.request(KJLRequest.RequestAPI.apiForGoogleMapRoute.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForGoogleMapRoute, completion: completion)
    }
    
    /// [801] 預約準備
    class func requestForReservePrepare(stationId: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["stationId": stationId]
        
        var headers = KJLRequest.getHeader()
        var token = headers["X-Auth_Token"] ?? ""
        if token.count <= 0 {
            token = "none"
            headers["X-Auth_Token"] = token
        }
        
        let request = AF.request(KJLRequest.RequestAPI.apiForReservePrepare.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers)
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForReservePrepare, completion: completion)
    }
     
    /// [804] 預約列表
    class func requestForReserveList(dateEnd: String , dateStart: String , payment: String , completion: @escaping RequestCompletion) {
        let params : Parameters = ["dateEnd": dateEnd,
                                   "dateStart": dateStart,
                                   "payment": payment]
        
        var headers = KJLRequest.getHeader()
        var token = headers["X-Auth_Token"] ?? ""
        if token.count <= 0 {
            token = "none"
            headers["X-Auth_Token"] = token
        }
        
        let request = AF.request(KJLRequest.RequestAPI.apiForReserveList.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers)
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForReserveList, completion: completion)
    }
    
    /// [805] 預約明細
    class func requestForReserveDetail(reserveNo: Int , completion: @escaping RequestCompletion) {
        let params : Parameters = ["reserveNo": reserveNo]
        
        var headers = KJLRequest.getHeader()
        var token = headers["X-Auth_Token"] ?? ""
        if token.count <= 0 {
            token = "none"
            headers["X-Auth_Token"] = token
        }
        
        let request = AF.request(KJLRequest.RequestAPI.apiForReserveDetail.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers)
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForReserveDetail, completion: completion)
    }
    
    /// 口罩
    class func requestForMaskDetail(completion: @escaping RequestCompletion) {
        var headers = KJLRequest.getHeader()
        var token = ""
        if (KJLRequest.isLogin() == true) {
            token = headers["X-Auth_Token"] ?? ""
            if token.count <= 0 {
                token = "none"
                headers["X-Auth_Token"] = token
            }
        }
        let request = AF.request(KJLRequest.RequestAPI.apiForMaskMap.requestURL(nil), method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForMaskMap, completion: completion)
    }
    
    ///
    //MARK: GenerateQrCode -- create 2020/10/22
    ///
    class func requestForGenerateQrCode(completion: @escaping RequestCompletion) {
        let params : Parameters = [:]
        
        let request = AF.request(KJLRequest.RequestAPI.apiGenerateQrCode.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiGenerateQrCode, completion: completion)
    }
    
    ///
    //MARK: ScanQrCode -- create 2020/10/22
    ///
    class func requestForScanQrCode(qrcode: String ,completion: @escaping RequestCompletion) {
        let params : Parameters = ["qrcode" : qrcode]
        
        let request = AF.request(KJLRequest.RequestAPI.apiScanQrCode.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiScanQrCode, completion: completion)
    }
    
    ///
    //MARK: Check Partner -- create 2020/10/22
    ///
    class func requestForCheckPartner(qrcode: String, subscription: Bool ,completion: @escaping RequestCompletion) {
        let params : Parameters = ["qrcode" : qrcode,
                                   "subscription" : subscription]
        
        let request = AF.request(KJLRequest.RequestAPI.apiCheckPartner.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiCheckPartner, completion: completion)
    }
    
    ///
    //MARK: GetVoucher -- create 2020/10/22
    ///
    class func requestForGetVoucher(email: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["email" : email]
        
        let request = AF.request(KJLRequest.RequestAPI.apiGetVoucher.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiGetVoucher, completion: completion)
    }
    
    ///
    //MARK: SubscriptionCar -- create 2020/10/22
    ///
    class func requestForSubscriptionCar(email: String, cardNum: String, bind: Bool, completion: @escaping RequestCompletion) {
        let params : Parameters = ["vin" : email,
                                   "cardNum" : cardNum,
                                   "bind" : bind]
        
        let request = AF.request(KJLRequest.RequestAPI.apiSubscriptionCar.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiSubscriptionCar, completion: completion)
    }
    
    ///
    //MARK: Partner -- create 2020/10/26
    ///
    class func requestForPartner(isSubscription: Bool ,completion: @escaping RequestCompletion) {
        let params : Parameters = ["subscription": isSubscription]
        
        let request = AF.request(KJLRequest.RequestAPI.apiPartner.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiPartner, completion: completion)
    }
    
    ///
    //MARK: Delete Partner -- create 2020/10/26
    ///
    class func requestForDeletePartner(isSubscription: Bool, partnerMemberNo: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["subscription": isSubscription,
                                   "partnerMemberNo" : partnerMemberNo]
        
        let request = AF.request(KJLRequest.RequestAPI.apiDeletePartner.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiDeletePartner, completion: completion)
    }
    
    ///
    //MARK: Porsche Car List -- create 2020/10/22
    ///
    class func requestForPorscheCarList(completion: @escaping RequestCompletion) {
        let params : Parameters = [:]
        
        let request = AF.request(KJLRequest.RequestAPI.apiPorscheCarList.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiPorscheCarList, completion: completion)
    }
    
    ///
    //MARK: Invoice Setting List -- create 2020/10/28
    ///
    ///  useBackup  = true -> backupEmail ,
    ///               false = accountEmail 
    ///  invoiceType = 01 = Personal e-Invoice area
    ///                02 = Business e-Invoice area
    ///  vehicleType = 01 = email ,
    ///                02 = vehicleNum
    class func requestForInvoiceSettingList(completion: @escaping RequestCompletion) {
        let params : Parameters = [:]
        
        let request = AF.request(KJLRequest.RequestAPI.apiInvoiceSettingList.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiInvoiceSettingList, completion: completion)
    }
    
    ///
    //MARK: Edit Invoice Setting -- create 2020/10/28
    ///
    ///  useBackup  = true -> backupEmail ,
    ///               false = accountEmail
    ///  invoiceType = 01 = Personal e-Invoice area
    ///                02 = Business e-Invoice area
    ///  vehicleType = 01 = email ,
    ///                02 = vehicleNum
    class func requestForEditInvoiceSetting(_ useBackup: Bool, _ invoiceType: String, _ vehicleType: String  ,completion: @escaping RequestCompletion) {
        let params : Parameters = ["useBackup"   : useBackup,
                                   "invoiceType" : invoiceType,
                                   "vehicleType" : vehicleType ]
        print("select params: \(params)")
        let request = AF.request(KJLRequest.RequestAPI.apiEditInvoiceSetting.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiEditInvoiceSetting, completion: completion)
    }

    //MARK: Set Invoice Settings -- create 2020/10/28
    class func requestForSetInvoiceSettings(_ backupEmail: String, _ vehicleNum: String, _ taxidName: String  ,_ taxidNum: String,completion: @escaping RequestCompletion) {
        let params : Parameters = ["backupEmail": backupEmail,
                                   "vehicleNum" : vehicleNum,
                                   "taxidName"  : taxidName,
                                   "taxidNum"   : taxidNum]
        print("##params: \(params)")
        let request = AF.request(KJLRequest.RequestAPI.apiSetInvoiceSettings.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiSetInvoiceSettings, completion: completion)
    }
    
    //MARK: Set Invoice Settings -- create 2020/11/01
    class func requestForConfirmPartner(_ qrcode: String, delete: Bool, completion: @escaping RequestCompletion) {
        let params : Parameters = ["qrcode": qrcode,
                                   "del"   : delete ]
        
        let request = AF.request(KJLRequest.RequestAPI.apiConfirmPartner.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiConfirmPartner, completion: completion)
    }
    
    //
    //MARK: Set Station History -- create 2020/11/10
    // get user gusetTap for documental
    //
    class func requestForSetStationHistory(_ stationId: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["stationId": stationId]
        
        let request = AF.request(KJLRequest.RequestAPI.apiSetStationHistory.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiSetStationHistory, completion: completion)
    }
    
    
    //
    //MARK: Set Charging History -- create 2020/11/12
    //
    class func requestForSetChargingHistory(_ date: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["dateRange": date]
        
        let request = AF.request(KJLRequest.RequestAPI.apiChargingHistory.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiChargingHistory, completion: completion)
    }
     
    //
    //MARK: Set Charging Result -- create 2020/11/12
    //
    class func requestForChargingResult(_ tranNo: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["tranNo": tranNo]
        
        let request = AF.request(KJLRequest.RequestAPI.apiPorscheChargingResult.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiPorscheChargingResult, completion: completion)
    }
    
    //
    //MARK: Set Charging Result -- create 2020/11/13
    //
    class func requestForViewSubscription(_ email: String, cardNum: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["vin" : email, "cardNum" : cardNum]
        
        let request = AF.request(KJLRequest.RequestAPI.apiViewSubscription.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiViewSubscription, completion: completion)
    }
    
    //
    //MARK: Set Station Search -- create 2020/11/16
    //
    class func requestForStationSearch(_ queryString: String, completion: @escaping RequestCompletion) {
        let params : Parameters = ["queryString" : queryString]
        
        let request = AF.request(KJLRequest.RequestAPI.apiStationSearch.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiStationSearch, completion: completion)
    }
    
    //
    //MARK: Set Change Language -- create 2020/11/19
    //
    class func requestForChangeLanguage(_ language: String, completion: @escaping RequestCompletion) {
        /// 英文 = "en"  中文 = "zh"
        let params : Parameters = ["lang" : language]
        let request = AF.request(KJLRequest.RequestAPI.apiChangeLanguage.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiChangeLanguage, completion: completion)
    }
    
    //
    //MARK: Set Porsche Login -- create 2020/11/19
    //
    class func requestForPorscheLogin(_ porscheCode: String, _ language: String, completion: @escaping RequestCompletion) {
        /// 英文 = "en"  中文 = "zh"
        let params : Parameters = ["porscheCode" : porscheCode,
                                   "lang" : language]
        let request = AF.request(KJLRequest.RequestAPI.apiPorscheLogin.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiPorscheLogin, completion: completion)
    }
    
    //
    //MARK: Set Reserve Start -- create 2020/11/20
    //
    class func requestForReserveStart(stationId: String , parkingSpace: Int, completion: @escaping RequestCompletion) {
        let params : Parameters = ["stationId"   : stationId,
                                   "parkingSpace": parkingSpace]
        let request = AF.request(KJLRequest.RequestAPI.apiForReserveStart.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForReserveStart, completion: completion)
    }
    
    //
    //MARK: Set Reserve Cancel -- create 2020/11/20
    //
    class func requestForReserveCancel(completion: @escaping RequestCompletion) {
        let params : Parameters = [:]
        let request = AF.request(KJLRequest.RequestAPI.apiForReserveCancel.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForReserveCancel, completion: completion)
    }
    
    //
    //MARK: Set Reserve Check -- create 2020/11/20
    //
    class func requestForReserveCheck(completion: @escaping RequestCompletion) {
        let params : Parameters = [:]
        let request = AF.request(KJLRequest.RequestAPI.apiForReserveCheck.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiForReserveCheck, completion: completion)
    }
    
    //
    //MARK: Set Check Terms -- create 2020/11/20
    //
    class func requestForCheckTerms(completion: @escaping RequestCompletion) {
        let params : Parameters = [:]
        let request = AF.request(KJLRequest.RequestAPI.apiCheckTerms.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiCheckTerms, completion: completion)
    }
    
    //
    //MARK: Set Check Terms -- create 2020/11/22
    //
    class func requestForChargingData(completion: @escaping RequestCompletion) {
        let params : Parameters = [:]
        let request = AF.request(KJLRequest.RequestAPI.apiChargingData.requestURL(nil), method: .post, parameters: params, encoding: JSONEncoding.default, headers: KJLRequest.getHeader())
        KJLRequest.shareInstance().requestForServer(request: request, apiType:RequestAPI.apiChargingData, completion: completion)
    }
     
    // 登出
    class func logout() {
        KJLRequest.shareInstance().token = ""
        KJLRequest.saveToekn(token: "")
        //        KJLRequest.shareInstance().name = ""
        //        KJLRequest.shareInstance().picUrl = ""
        KJLRequest.shareInstance().profile = nil
        UserDefaults.standard.removeObject(forKey: "EnterpiseLogin")
        UserDefaults.standard.removeObject(forKey: "setUserLanguage")
        UserDefaults.standard.removeObject(forKey: "UserLanguage")
        UserDefaults.standard.synchronize()
        
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    // 判斷是否登入
    class func isLogin() -> Bool {
        if KJLRequest.shareInstance().token.count <= 0 {
            return false
        } else {
            return true
        }
    }
    
    // 儲存toekn
    class func saveToekn(token: String?) {
        KJLRequest.shareInstance().token = token ?? ""
        KJLCommon.saveData(name: "token", data: KJLRequest.shareInstance().token)
        
        if KJLRequest.shareInstance().token.count > 0 {

        }
    }
    
    // 取得token
    class func gotToken() {
        KJLRequest.shareInstance().token = KJLCommon.getSaveString(name: "token")
        if KJLRequest.isLogin() == true {
            KJLRequest.requestForProfile { (response) in
                guard let response = response as? ProfileClass else {
                    return
                }
                KJLRequest.shareInstance().profile = response
            }
            
            if KJLRequest.shareInstance().token.count > 0 {

            }
        }
    }
    
    class func cancelAll() {
        //        Alamofire.SessionManager.default.session.getAllTasks { (tasks) in
        //            tasks.forEach({$0.cancel()})
        //        }
        //        Alamofire.SessionManager.default.session.invalidateAndCancel()
    }
    
}

