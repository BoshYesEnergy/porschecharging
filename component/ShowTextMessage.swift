//
//  ShowTextMessage.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class ShowTextMessage: UIView {

    enum ShowTextMessageType: String {
        case ChargingSuccess = "充電成功"
        case ChargingFaile = "充電失敗"
        case Charging = "充電中"
        case Other = "其他"
        case Nothing = ""
    }
    
    typealias SelectCompletion = (Bool) -> Void
    var completion: SelectCompletion? = nil
    
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var viewCenter: NSLayoutConstraint!
    @IBOutlet weak var titleLab: UILabel!
    @IBOutlet weak var limitNumberLab: UILabel!

    @IBOutlet weak var messageText: UITextView!
    @IBOutlet weak var tipLab: UILabel!
    var stationNo: String?
    var type: ShowTextMessageType = .Nothing
    
    override func awakeFromNib() {
        super.awakeFromNib()
        messageText.layer.borderWidth = 1.0
        messageText.layer.borderColor = #colorLiteral(red: 0.8195181489, green: 0.8196596503, blue: 0.8195091486, alpha: 1)
        
        sendBtn.isEnabled = true
        sendBtn.alpha = 1.0
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    @IBAction func sendAction(_ sender: Any) {
        messageText.resignFirstResponder()
        
        let userMsg = messageText.text ?? ""
//        guard userMsg.count > 0 else {
//            KJLCommon.showAlert(title: nil, message: "請輸入心得")
//            return
//        }
        
        if type == .ChargingSuccess || type == .ChargingFaile || type == .Charging || type == .Other {
            var serverType: KJLRequest.RequestReportType = .OK
            var msg = ""
            switch type {
            case .ChargingSuccess:
                serverType = .OK
                msg = "謝謝您的充電體驗分享"
            case .ChargingFaile:
                serverType = .Fail
                if userMsg.count > 0 {
                    msg = "謝謝您的充電體驗分享"
                }else {
                    msg = "請分享您的充電體驗，幫助充電環境更佳友善"
                }
            case .Charging:
                serverType = .Charging
                msg = "謝謝您的充電體驗分享"
            case .Other:
                serverType = .Other
                if userMsg.count > 0 {
                    msg = "謝謝您的充電體驗分享"
                }else {
                    msg = "請分享您的充電體驗，幫助充電環境更佳友善"
                }
            case .Nothing:
                break
            }
            
            KJLRequest.requestForChargingReport(stationId: stationNo ?? "", reportType: serverType, comment: userMsg) { (response) in
                guard (response as? NormalClass) != nil else {
                    return
                }
                ShowMessageView.showMessage(title: nil, message: msg, completion: { (isOK) in
                    if let completion = self.completion {
                        if self.type == .ChargingFaile || self.type == .Other {
                            self.messageText.becomeFirstResponder()
                            if userMsg.count > 0 {
                                completion(true)
                                self.removeFromSuperview()
                            }else {
                                completion(false)
                            }
                        }else {
                            self.removeFromSuperview()
                            completion(true)
                        }
                    }
                })
            }
        }
    }
}

extension ShowTextMessage {
    class func show(type: ShowTextMessageType, stationNo: String?) {
        guard let vw = Bundle.main.loadNibNamed("ShowTextMessage", owner: self, options: nil)?.first as? ShowTextMessage else {
            return
        }
        vw.titleLab.text = type.rawValue
        vw.type = type
        vw.stationNo = stationNo
        vw.frame = UIScreen.main.bounds
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.addSubview(vw)
    }
    
    class func showReruen(type: ShowTextMessageType, stationNo: String?, completion: @escaping SelectCompletion) {
        guard let vw = Bundle.main.loadNibNamed("ShowTextMessage", owner: self, options: nil)?.first as? ShowTextMessage else {
            return
        }
        vw.titleLab.text = type.rawValue
        vw.type = type
        vw.stationNo = stationNo
        vw.completion = completion
        vw.frame = UIScreen.main.bounds
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.addSubview(vw)
    }
}

extension ShowTextMessage: UITextViewDelegate {
    public func textViewDidBeginEditing(_ textView: UITextView) {
        viewCenter.constant = -(self.frame.height/6)
        tipLab.isHidden = true
    }
    
    public func textViewDidEndEditing(_ textView: UITextView) {
        viewCenter.constant = 0
        if textView.text.count <= 0 {
            tipLab.isHidden = true
        }else {
            tipLab.isHidden = false
        }
    }
    
    public func textViewDidChange(_ textView: UITextView) {
        print("textViewDidChangetextViewDidChange")
//        if textView.text.count > 0 {
//            sendBtn.isEnabled = true
//            sendBtn.alpha = 1.0
//        }else {
//            sendBtn.isEnabled = false
//            sendBtn.alpha = 0.5
//        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentNumber = textView.text.count + (text.count - range.length)
        let limitNumber = 24
        if currentNumber <= limitNumber {
            self.limitNumberLab.text = "\(currentNumber)/\(limitNumber)"
        }else{
            
        }
        return currentNumber <= limitNumber
    }
}
