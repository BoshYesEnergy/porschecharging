//
//  CustomPin.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit
import MapKit

class CustomPin: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    var data: StationListClass.Data? = nil
    @objc var pk: String = ""
    @objc var imageName: String = "'"
    @objc var num: String = ""
    
    @objc init(coordinate: CLLocationCoordinate2D) {
        self.coordinate = coordinate
        self.title = ""
        self.subtitle = ""
        self.pk = "0"
        self.imageName = "ic_btn_radio"
    }
}
