//
//  KJLDatePickerView.swift
//  ListenAPP
//
//  Created by s5346 on 2017/5/4.
//  Copyright © 2017年 s5346. All rights reserved.
//

import UIKit

class KJLDatePickerView: UIView {

    typealias KJLDatePickerCompletion = (Date?) -> Void
    var pickerCompletion: KJLDatePickerCompletion?
    
    @IBOutlet weak var wrapView: UIView!
    @IBOutlet weak var warpViewBottom: NSLayoutConstraint!
    @IBOutlet weak var kPickerView: UIDatePicker!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        kPickerView.locale = Locale(identifier: "zh")
        kPickerView.minimumDate = Date(timeIntervalSince1970: 0)
        kPickerView.maximumDate = Date(timeIntervalSinceNow: 0)
    }
    
    deinit {
        print(self)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        pickerCompletion?(nil)
        close()
    }
    
    @IBAction func sureAction(_ sender: Any) {
        pickerCompletion?(kPickerView.date)
        close()
    }
    
    func show() {
//        let format = DateFormatter()
//        format.dateFormat = "yyyy/MM/dd"
//        let date = format.date(from: "1990/06/15")
//        if let date = date {
//            kPickerView.date = date
//        }

        frame = UIScreen.main.bounds
        backgroundColor = UIColor(white: 0.0, alpha: 0.0)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.addSubview(self)
        
        warpViewBottom.constant = 0.0
        
        UIView.animate(withDuration: 0.3) {
            self.backgroundColor = UIColor(white: 0.0, alpha: 0.2)
            self.layoutIfNeeded()
        }
    }
    
    func close() {
        frame = UIScreen.main.bounds
        
        warpViewBottom.constant = -260
        
        UIView.animate(withDuration: 0.3, animations: {
            self.backgroundColor = UIColor(white: 0.0, alpha: 0.0)
            self.layoutIfNeeded()
        }) { (completion) in
            self.removeFromSuperview()
        }
    }

}
