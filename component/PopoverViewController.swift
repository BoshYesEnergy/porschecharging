//
//  PopoverViewController.swift
//  NissanCar
//
//  Created by Scott.Tsai on 2018/10/30.
//  Copyright © 2018年 s5346. All rights reserved.
//

import UIKit

class PopoverViewController: KJLNavView {

    
    @IBOutlet weak var messageLabel: UILabel!
    
    private var messageString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.messageLabel.text = self.messageString
    }
    
    func setupPopoverMessage(message: String) {
        self.messageString = message
    }
}
