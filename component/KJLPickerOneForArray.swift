//
//  KJLPickerOne.swift
//  ListenAPP
//
//  Created by s5346 on 2017/5/3.
//  Copyright © 2017年 s5346. All rights reserved.
//

import UIKit

class KJLPickerOneForArray: UIView {

    typealias KJLPickerOneCompletion = (String?) -> Void
    var pickerCompletion: KJLPickerOneCompletion?
    
    @IBOutlet weak var wrapView: UIView!
    @IBOutlet weak var warpViewBottom: NSLayoutConstraint!
    @IBOutlet weak var kPickerView: UIPickerView!
    
    var data: [String] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
//        data = KJLCommon.getSaveSystemVerbClass()?.datas?.typeData?.typeList ?? []
//        kPickerView.reloadAllComponents()
    }
    
    func changeData(t: [String]) {
        data = t
        kPickerView.reloadAllComponents()
    }
    
    deinit {
        print(self)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        pickerCompletion?("")
        close()
    }
    
    @IBAction func sureAction(_ sender: Any) {
        pickerCompletion?(data[kPickerView.selectedRow(inComponent: 0)])
        close()
    }
    
    func show() {
        frame = UIScreen.main.bounds
        backgroundColor = UIColor(white: 0.0, alpha: 0.0)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.addSubview(self)
        
        warpViewBottom.constant = 0.0
        
        UIView.animate(withDuration: 0.3) {
            self.backgroundColor = UIColor(white: 0.0, alpha: 0.2)
            self.layoutIfNeeded()
        }
    }
    
    func close() {
        frame = UIScreen.main.bounds
        
        warpViewBottom.constant = -260
        
        UIView.animate(withDuration: 0.3, animations: { 
            self.backgroundColor = UIColor(white: 0.0, alpha: 0.0)
            self.layoutIfNeeded()
        }) { (completion) in
            self.removeFromSuperview()
        }
    }

}

extension KJLPickerOneForArray: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return data[row]
    }
}

extension KJLPickerOneForArray: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return data.count
    }
}
