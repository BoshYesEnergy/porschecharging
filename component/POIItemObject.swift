//
//  POIItemObject.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/2/10.
//  Copyright © 2020 s5346. All rights reserved.
//


class POIItem: NSObject, GMUClusterItem {
    var position: CLLocationCoordinate2D
    var name: String!
    var index: Int = 0
    var distance: Double = 0
    var isNotFound = false
    var iconImage:UIImage? = nil
    var iconView: UIView? = nil
    init(position: CLLocationCoordinate2D, name: String) {
        self.position = position
        self.name = name
    }
} 
