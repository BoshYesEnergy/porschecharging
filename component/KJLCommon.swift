//
//  KJLCommon.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import Foundation
import MapKit
import AVFoundation
import Photos

func printLog<T>(_ message: T,
                 logError: Bool = false,
                 file: String = #file,
                 method: String = #function,
                 line: Int = #line)
{
    if logError {
        print("\((file as NSString).lastPathComponent)[\(line)], \(method): \(message)")
    } else {
        #if DEBUG
            print("\((file as NSString).lastPathComponent)[\(line)], \(method): \(message)")
        #endif
    }
}

class KJLCommon {
    typealias Completion = (Bool) -> Void
    
    // MARK: - alert
    class func showAlertTwo(title: String?, message: String, cancelButton: String, sureButton: String, completion: @escaping Completion) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: cancelButton, style: .default) { (action) in
            completion(false)
        }
        let sure = UIAlertAction(title: sureButton, style: .default) { (action) in
            completion(true)
        }
        
        alert.addAction(cancel)
        alert.addAction(sure)
        
        if let currentVC = KJLCommon.currentController() {
            currentVC.present(alert, animated: true, completion: nil)
        }
    }
    
    class func showAlert(title: String?, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let sure = UIAlertAction(title: "OK", style: .default) { (action) in
        }
        
        alert.addAction(sure)
        
        if let currentVC = KJLCommon.currentController() {
            currentVC.present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: - 取得目前controller
    class func currentController() -> UIViewController? {
        var tempVC = UIApplication.shared.keyWindow?.rootViewController
        
        if let a = tempVC as? UINavigationController {
            if let b = a.viewControllers.last {
                tempVC = b
            }
        }
        
        var currentVC : UIViewController?
        repeat{
            currentVC = tempVC!
            tempVC = tempVC?.presentedViewController
        }while tempVC != nil
        
        return currentVC!
    }
    
    // MARK: - 判斷location權限
    class func authorizeForLocation() -> Bool {
        // 1. 還沒有詢問過用戶以獲得權限
        if CLLocationManager.authorizationStatus() == .notDetermined {
            //            CLLocationManager.requestWhenInUseAuthorization(<#T##CLLocationManager#>)
            return true
        }
            // 2. 用戶不同意
        else if CLLocationManager.authorizationStatus() == .denied {
            KJLCommon.requestAuthorize(desc: "需開啟定位")
            return false
        }
            // 3. 用戶已經同意
        else if CLLocationManager.authorizationStatus() == .authorizedAlways {
            return true
        }
            
        else if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            return true
        }
        
        return false
    }
    
    // MARK: - 請求權限
    class func requestAuthorize(desc: String) {
        ShowMessageView.showQuestionSetting(title: nil, message: desc, completion: { (isOK) in
            if isOK == true {
                let a = URL(string: UIApplicationOpenSettingsURLString)
                if UIApplication.shared.canOpenURL(a!) {
                    UIApplication.shared.open(a!, options: [:], completionHandler: nil)
                }
                /*
                if #available(iOS 10.0, *) {
                    let a = URL(string: UIApplicationOpenSettingsURLString)
                    if UIApplication.shared.canOpenURL(a!) {
                        UIApplication.shared.open(a!, options: [:], completionHandler: nil)
                    }
                }else {
                    if let bundleID = Bundle.main.bundleIdentifier {
                        
                        let a = URL(string: "prefs:root=\(bundleID)")
                        if UIApplication.shared.canOpenURL(a!) {
                            UIApplication.shared.openURL(a!)
                        }
                    }
                }*/
            }
        })
    }
    
    // MARK: - 判斷相機權限
    class func authorizeForCamera() -> Bool {
        let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        guard status != .denied, status != .restricted else {
            KJLCommon.requestAuthorize(desc: "拍照或掃QRCode")
            return false
        }
        
        return true
    }
    
    // MARK: - 驗證email格式
    class func isValidateEmail(email: String) -> Bool {
        //        let strRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{1,5}"
        let strRegex = "^\\w+((-\\w+)|(\\.\\w+))*\\@[A-Za-z0-9]+((\\.|-)[A-Za-z0-9]+)*\\.[A-Za-z0-9]+$"
        let predicate = NSPredicate(format: "SELF MATCHES %@", strRegex)
        return predicate.evaluate(with: email)
    }
    
    // 限制英數
    class func isEnglishNumber(str: String) -> Bool {
        let strRegex = "^[A-Za-z0-9]+$"
        let predicate = NSPredicate(format: "SELF MATCHES %@", strRegex)
        return predicate.evaluate(with: str)
    }
    
    class func onlyNumber(str: String) -> Bool {
        let strRegex = "^[0-9]+$"
        let predicate = NSPredicate(format: "SELF MATCHES %@", strRegex)
        return predicate.evaluate(with: str)
    }
    
    class func onlyEnglish(str: String) -> Bool {
        let strRegex = "^[A-Za-z]+$"
        let predicate = NSPredicate(format: "SELF MATCHES %@", strRegex)
        return predicate.evaluate(with: str)
    }
    
    // 驗證台灣
    class func is886Phone(str: String) -> Bool {
        let strRegex = "[0][9][0-9]{8}"
        let predicate = NSPredicate(format: "SELF MATCHES %@", strRegex)
        return predicate.evaluate(with: str)
    }
    
    // 驗證手機條碼
    class func isPhoneTicket(str: String) -> Bool {
        //let strRegex = "^\\/{1}[0-9A-Z]{7}$"
        // 財政部查到之標準
//        let strRegex = "^\\/{1}[^<>&:\"\']{7}$" //(原始的 Code)
        /* 開頭第 1 碼為/  其餘 7 碼為 A~Z 或 0~9 或 + 或 - 或 . (修改 By Scott 2018-11-02) */
        let strRegex = "^\\/{1}[A-Z0-9+-.]{7}$"
        let predicate = NSPredicate(format: "SELF MATCHES %@", strRegex)
        return predicate.evaluate(with: str)
    }
    
    class func getCountry() -> [String] {
        return ["台灣","美國"]
    }
    class func getCountryCode(country: String) -> String {
        let dic: [String: String] = ["台灣": "886",
                                     "美國": "1"]
        return dic[country] ?? ""
    }
    
    // 存資料
    class func saveData(name: String, data: Any) {
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let dataPath = path + "/" + name
        print(dataPath)
        let saveData = NSKeyedArchiver.archivedData(withRootObject: data)
        try? saveData.write(to: URL(fileURLWithPath: dataPath))
    }
    
    // 取資料 string
    class func getSaveString(name: String) -> String {
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let dataPath = path + "/" + name
        print(dataPath)
        if let saveData = NSKeyedUnarchiver.unarchiveObject(withFile: dataPath) as? String {
            return saveData
        }
        return ""
    }
    
    // 存資料 SystemVerbClass
    class func saveSystemVerbClass(data: SystemVerbClass) {
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let dataPath = path + "/" + "SystemVerbClass"
        print(dataPath)
        
        if let encodedData = try? JSONEncoder().encode(data) {
            NSKeyedArchiver.archiveRootObject(encodedData, toFile: dataPath)
        }
    }
    
    // 取資料 SystemVerbClass
    class func getSaveSystemVerbClass() -> SystemVerbClass? {
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let dataPath = path + "/" + "SystemVerbClass"
        print(dataPath)
        if let saveData = NSKeyedUnarchiver.unarchiveObject(withFile: dataPath) as? Data {
            if let object = try? JSONDecoder().decode(SystemVerbClass.self, from: saveData) {
                return object
            }
            return nil
        }
        return nil
    }
    
    // 導航
    class func openNavMap(lat: Double?, long: Double?, subControl: UIViewController) {
        let control = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let apple = UIAlertAction(title: "Apple Map", style: .default, handler: { (alertAction) in
            KJLCommon.openNavMapApple(lat: lat ?? 0.0, long: long ?? 0.0)
        })
        
        let google = UIAlertAction(title: "Google Map", style: .default, handler: { (alertAction) in
            
            LocationManagers.updateLocation {(status, location) in
                if  status == .Success {
                    KJLCommon.openNavMapGoogle(saddr: "\(location!.coordinate.latitude),\(location!.coordinate.longitude)", lat: lat ?? 0.0, long: long ?? 0.0)
                }else{
                    
                    KJLCommon.openNavMapGoogle(saddr: "My+Location", lat: lat ?? 0.0, long: long ?? 0.0)
                }
            }
        })
        
        let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        
        control.addAction(apple)
        control.addAction(google)
        control.addAction(cancel)
        
        subControl.present(control, animated: true, completion: nil)
    }
    
    class func openStringNavMapWith(lat: Double?, long: Double? , startPlace: String , endPlace: String, subControl: UIViewController) {
        let control = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let apple = UIAlertAction(title: "Apple Map", style: .default, handler: { (alertAction) in
            KJLCommon.openNavMapApple(lat: lat ?? 0.0, long: long ?? 0.0)
        })
        
        let google = UIAlertAction(title: "Google Map", style: .default, handler: { (alertAction) in
            KJLCommon.openStringNavMapGoogleWith(startPlace: startPlace, endPlace: endPlace)
        })
        
        let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        
        control.addAction(apple)
        control.addAction(google)
        control.addAction(cancel)
        
        subControl.present(control, animated: true, completion: nil)
    }
    
    class func openNavMapApple(lat: Double?, long: Double?) {
        let location = "\(lat ?? 0.0),\(long ?? 0.0)&z=15"
        let directionsURL = "http://maps.apple.com/maps?saddr=Current%20Location&daddr=\(location)"
        guard let url = URL(string: directionsURL) else {
            return
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    class func openNavMapGoogle(saddr:String,lat: Double?, long: Double?) {
        if UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!) {
            let typeString = UserDefaults.standard.string(forKey: "filterCarType") ?? "1"
            let travelmode = (typeString == "1") ? "driving" : "two-wheeler"
                //點選車子狀態顯示
            let urlStr = "https://www.google.com/maps/dir/?api=1&travelmode=\(travelmode)&dir_action=navigate&origin=\(saddr)&destination=\(lat ?? 0.0),\(long ?? 0.0)"   //q=\(myData?.name ?? "")&
//            print("urlStr")
//            urlStr = urlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            //urlStr = "comgooglemaps://?" + urlStr
        
            if let url = URL(string: urlStr) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }                
            }
        } else {
            ShowMessageView.showMessage(title: nil, message: "請先下載google map")
        }

    }
    
    class func openMultiStationMapGoogle(dataArray: [CLLocationCoordinate2D]) {
        if UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!) {
            
            var urlStr = ""
            for coodinate in dataArray {
                urlStr += "/\(coodinate.latitude),\(coodinate.longitude)"
            }
            
            urlStr = urlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            urlStr = "comgooglemapsurl://www.google.com/maps/dir" + urlStr
            if let url = URL(string: urlStr) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        } else {
            ShowMessageView.showMessage(title: nil, message: "請先下載google map")
        }
    }
    
    class func openStringNavMapApple(startPlace: String , endPlace: String) {
//        let location = "\(lat ?? 0.0),\(long ?? 0.0)&z=15"
        let directionsURL = "http://maps.apple.com/maps?saddr=\(startPlace)&daddr=\(endPlace)"
        guard let url = URL(string: directionsURL) else {
            return
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    // 導航 - Google 新增停靠站
    class func openMultiStationNavMap(lat: Double?, long: Double? , dataArray: [CLLocationCoordinate2D], subControl: UIViewController) {
        let control = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let apple = UIAlertAction(title: "Apple Map", style: .default, handler: { (alertAction) in
            // Apple Map 無新增停靠站的功能 - 僅導航 起始到終點
            KJLCommon.openNavMapApple(lat: lat ?? 0.0, long: long ?? 0.0)
        })
        
        let google = UIAlertAction(title: "Google Map", style: .default, handler: { (alertAction) in
            KJLCommon.openMultiStationMapGoogle(dataArray: dataArray)
        })
        
        let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        
        control.addAction(apple)
        control.addAction(google)
        control.addAction(cancel)
        
        subControl.present(control, animated: true, completion: nil)
    }

    
    class func openStringNavMapGoogleWith(startPlace: String , endPlace: String) {
        if UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!) {
            var urlStr = "saddr=\(startPlace)&daddr=\(endPlace)&zoom=15&views=traffic"//q=\(myData?.name ?? "")&
            urlStr = urlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            urlStr = "comgooglemaps://?" + urlStr
            
            if let url = URL(string: urlStr) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }else {
            ShowMessageView.showMessage(title: nil, message: "請先下載google map")
        }
        
    } 
    
    class func isX() -> Bool {
        if #available(iOS 11.0, *) {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            return (appDelegate.window?.safeAreaInsets.bottom)! > CGFloat(0.0)
        } else {
            return false
        }
    }
    
    class func isIphoneSE() -> Bool {
        return UIScreen.main.bounds.height == 568 ? true : false
    }
    
    class func showLogin(subcontrol: UIViewController) {
        ShowMessageView.showQuestion(title: "請登入帳號", message: "此功能需登入帳號後才能使用", completion: { (isOK) in
            if isOK == true {
                if let vc = subcontrol.storyboard?.instantiateViewController(withIdentifier: "SelectLoginViewController") as? SelectLoginViewController {
                    vc.hidesBottomBarWhenPushed = true
                    subcontrol.navigationController?.pushViewController(vc, animated: true)
                }
            }
        })
    }
    
    // default image
    class func defaultImage() -> UIImage? {
        return UIImage(named: "charge_img_defult")
    }
    class func defaultImage2() -> UIImage? {
        return UIImage(named: "photo_headshot_none")
    }
    
    // 手機格式判斷
    class func validPhone(phone: String) -> Bool {
        guard phone.count >= 9 else {
//            ShowMessageView.showMessage(title: nil, message: "格式錯誤，請再重新輸入")
            return false
        }
        return true
    }
    
    class func validPhone10(phone: String) -> Bool {
        guard phone.count == 10 else {
            //            ShowMessageView.showMessage(title: nil, message: "格式錯誤，請再重新輸入")
            return false
        }
        return true
    }
    
    // Convert String to base64
    class func convertImageToBase64(image: UIImage) -> String {
        let imageData = UIImagePNGRepresentation(image)!
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    // Convert base64 to String
    class func convertBase64ToImage(imageString: String) -> UIImage {
        let imageData = Data(base64Encoded: imageString, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)!
        return UIImage(data: imageData)!
    }
    
}

extension UITextField {
    func setBottomBorder() {
        self.layer.backgroundColor = #colorLiteral(red: 0.9647058824, green: 0.9764705882, blue: 0.9882352941, alpha: 1) //UIColor.white.cgColor
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = #colorLiteral(red: 0.8431372549, green: 0.8392156863, blue: 0.8392156863, alpha: 1)
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
    
    func setupGreenBottomBorder() {
        self.layer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) //UIColor.white.cgColor
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = #colorLiteral(red: 0.2392156863, green: 0.7607843137, blue: 0.7843137255, alpha: 1)
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
        
    }
    
    func setCorner() {
        self.layer.cornerRadius = 5.0
        self.layer.masksToBounds = false
    }
    
    func addPadding() {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 14, height: self.frame.height))
        self.leftView = paddingView
        self.leftViewMode = UITextFieldViewMode.always
    }
}

extension UIButton {
    func setCorner() {
        self.layer.cornerRadius = 5.0
        self.layer.masksToBounds = false
    }
    
    func setLikeButton() {
        self.layer.cornerRadius = 5.0
        self.layer.masksToBounds = false
        self.layer.borderWidth = 1
        self.layer.borderColor = #colorLiteral(red: 0.1497560143, green: 0.7302395701, blue: 0.7572383881, alpha: 1)
    }
    
    func setLikeButtonGray() {
        self.layer.cornerRadius = 5.0
        self.layer.masksToBounds = false
        self.layer.borderWidth = 1
        self.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    }
    
    func setWhiteBackgroundBtn() {
        self.layer.cornerRadius = self.bounds.size.height / 2
        self.layer.masksToBounds = false
        self.layer.borderWidth = 1
        self.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    func setupBackgroundImageWith(normaImageString: String , highLightImageString: String) {
        self.setBackgroundImage(UIImage(named: normaImageString), for: .normal)
        self.setBackgroundImage(UIImage(named: highLightImageString), for: .highlighted)
    }
    
    func setEmptyWhiteShortImage() {
        self.setupBackgroundImageWith(normaImageString: "home_btn_white_short_normal", highLightImageString: "home_btn_white_short_pressed")
    }
    
    func setEmptyWhiteLongImage() {
        self.setupBackgroundImageWith(normaImageString: "home_btn_white_long_normal", highLightImageString: "home_btn_white_long_pressed")
    }
    
    func setEmptyWhiteSpecialLongImage() {
        self.setupBackgroundImageWith(normaImageString: "plan_btn_list_normal", highLightImageString: "plan_btn_list_pressed")
    }
    
    func setFullCyanShortImage() {
        self.setupBackgroundImageWith(normaImageString: "home_btn_cyan_short_normal", highLightImageString: "home_btn_cyan_short_pressed")
    }
    
    func setFullCyanLongImage() {
        self.setBackgroundImage(UIImage(named: "sign_in_btn_confirm"), for: .normal)
    }
    
    func setEpmtyGrayShortImage() {
        self.setBackgroundImage(UIImage(named: "home_btn_white_short_disable"), for: .normal)
    }
    
    func setFullGrayShortImage() {
        self.setBackgroundImage(UIImage(named: "home_btn_cyan_short_disable"), for: .normal)
    }
    
    func setEpmtyGrayLongImage() {
        self.setBackgroundImage(UIImage(named: "home_btn_white_long_disable"), for: .normal)
    }
    
    func setFullGrayLongImage() {
        self.setBackgroundImage(UIImage(named: "home_btn_white_long_disable_full"), for: .normal)
    }
}

extension String {
//    func sha256() -> String? {
//        guard
//            let data = self.data(using: String.Encoding.utf8),
//            let shaData = sha256(data)
//            else { return nil }
//        let rc = shaData.base64EncodedString(options: [])
//        return rc
//    }
//
//    func sha256(_ data: Data) -> Data? {
//        guard let res = NSMutableData(length: Int(CC_SHA256_DIGEST_LENGTH)) else { return nil }
//        CC_SHA256((data as NSData).bytes, CC_LONG(data.count), res.mutableBytes.assumingMemoryBound(to: UInt8.self))
//        return res as Data
//    }
//    func sha256() -> String? {
//        guard let stringData = self.data(using: String.Encoding.utf8) else { return nil }
//        return digest(input: stringData as NSData).base64EncodedString(options: [])
//    }
//
//    private func digest(input : NSData) -> NSData {
//        let digestLength = Int(CC_SHA256_DIGEST_LENGTH)
//        var hash = [UInt8](repeating: 0, count: digestLength)
//        CC_SHA256(input.bytes, UInt32(input.length), &hash)
//        return NSData(bytes: hash, length: digestLength)
//    }
    
    var data: Data {
        return Data(utf8)
    }
    
    var sha256: String {
        return HMAC.hash(inp: self, algo: HMACAlgo.SHA256)
    }
    
    var sha512: String {
        return HMAC.hash(inp: self, algo: HMACAlgo.SHA512)
    }
    
    public struct HMAC {
        
        static func hash(inp: String, algo: HMACAlgo) -> String {
            if let stringData = inp.data(using: String.Encoding.utf8, allowLossyConversion: false) {
                return hexStringFromData(input: digest(input: stringData as NSData, algo: algo))
            }
            return ""
        }
        
        private static func digest(input : NSData, algo: HMACAlgo) -> NSData {
            let digestLength = algo.digestLength()
            var hash = [UInt8](repeating: 0, count: digestLength)
            switch algo {
            case .MD5:
                CC_MD5(input.bytes, UInt32(input.length), &hash)
                break
            case .SHA1:
                CC_SHA1(input.bytes, UInt32(input.length), &hash)
                break
            case .SHA224:
                CC_SHA224(input.bytes, UInt32(input.length), &hash)
                break
            case .SHA256:
                CC_SHA256(input.bytes, UInt32(input.length), &hash)
                break
            case .SHA384:
                CC_SHA384(input.bytes, UInt32(input.length), &hash)
                break
            case .SHA512:
                CC_SHA512(input.bytes, UInt32(input.length), &hash)
                break
            }
            return NSData(bytes: hash, length: digestLength)
        }
        
        private static func hexStringFromData(input: NSData) -> String {
            var bytes = [UInt8](repeating: 0, count: input.length)
            input.getBytes(&bytes, length: input.length)
            
            var hexString = ""
            for byte in bytes {
                hexString += String(format:"%02x", UInt8(byte))
            }
            
            return hexString
        }
    }
    
    enum HMACAlgo {
        case MD5, SHA1, SHA224, SHA256, SHA384, SHA512
        
        func digestLength() -> Int {
            var result: CInt = 0
            switch self {
            case .MD5:
                result = CC_MD5_DIGEST_LENGTH
            case .SHA1:
                result = CC_SHA1_DIGEST_LENGTH
            case .SHA224:
                result = CC_SHA224_DIGEST_LENGTH
            case .SHA256:
                result = CC_SHA256_DIGEST_LENGTH
            case .SHA384:
                result = CC_SHA384_DIGEST_LENGTH
            case .SHA512:
                result = CC_SHA512_DIGEST_LENGTH
            }
            return Int(result)
        }
    }
    
    //使用正则表达式替换
    func pregReplace(pattern: String, with: String,
                     options: NSRegularExpression.Options = []) -> String {
        let regex = try! NSRegularExpression(pattern: pattern, options: options)
        return regex.stringByReplacingMatches(in: self, options: [],
                                              range: NSMakeRange(0, self.count),
                                              withTemplate: with)
    }
}

extension UIView {
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.masksToBounds = true
            layer.cornerRadius = newValue
        }
    }
    
    func shadowsType(kcolor: UIColor) {
        layer.masksToBounds = false
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shadowColor = kcolor.cgColor
        layer.shadowOpacity = 0.1
        layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        
    }
    
    func setupShadowWith(color: UIColor , opacity: Float , radius: CGFloat, CGSizeWidth: CGFloat , CGSizeHeight: CGFloat) {
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowRadius = radius
        layer.shadowOffset = CGSize(width: CGSizeWidth, height: CGSizeHeight)
        layer.masksToBounds = false
    }
}

extension UserDefaults {
    func saveRegType(regType: String) {
        self.set(regType, forKey: "regType")
    }
    func getRegType() -> String {
        return self.object(forKey: "regType") as? String ?? ""
    }
    func saveRegCode(regCode: String) {
        self.set(regCode, forKey: "regCode")
    }
    func getRegCode() -> String {
        return self.object(forKey: "regCode") as? String ?? ""
    }
}

extension UIDevice {
    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }

        return identifier
    }()
    
    var modelNameShow: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPhone10,1":                              return "iPhone 8"
        case "iPhone10,2":                              return "iPhone 8 Plus"
        case "iPhone10,3":                              return "iPhone X"
        case "iPhone10,4":                              return "iPhone 8"
        case "iPhone10,5":                              return "iPhone 8 Plus"
        case "iPhone10,6":                              return "iPhone X"
        case "iPhone11,2":                              return "iPhone XS"
        case "iPhone11,4":                              return "iPhone XS Max"
        case "iPhone11,8":                              return "iPhone XR"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4", "iPad6,7", "iPad6,8":return "iPad Pro"
        case "AppleTV5,3":                              return "Apple TV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
}

extension Data {
    var attributedString: NSAttributedString? {
        do {
            let options: [NSAttributedString.DocumentReadingOptionKey: Any] = [
                NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html,
                NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue
            ]
            return try NSAttributedString(data: self, options:options, documentAttributes: nil)
        } catch {
            print(error)
        }
        return nil
    }
}

extension UITableViewCell {
    func makeShadowAndCornerRadius() {
        self.contentView.backgroundColor = UIColor.white
        self.contentView.layer.cornerRadius = 6.0
        self.contentView.layer.masksToBounds = true
        
        self.backgroundColor = UIColor.clear
        self.layer.shadowRadius = 3.0
        self.layer.shadowOpacity = 1
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.22).cgColor
        self.layer.masksToBounds = false
    }
}

extension UITableView {
    func makeContentInset() {
        if #available(iOS 13.0, *) {
         let top = UIScreen.main.bounds.height <= 667 ? 34 : 0
         self.contentInset = UIEdgeInsets.init(top: CGFloat(top), left: 0, bottom: 44, right: 0)
        }
    }
}
