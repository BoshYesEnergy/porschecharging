//
//  KJLPickerOne.swift
//  ListenAPP
//
//  Created by s5346 on 2017/5/3.
//  Copyright © 2017年 s5346. All rights reserved.
//

import UIKit
import CountryPicker

class KJLPickerForCountry: UIView {

    typealias KJLPickerOneCompletion = (String, String) -> Void
    var pickerCompletion: KJLPickerOneCompletion?
    
    @IBOutlet weak var wrapView: UIView!
    @IBOutlet weak var warpViewBottom: NSLayoutConstraint!
    @IBOutlet weak var kPickerView: CountryPicker!
    
    var code: String = ""
    var name: String = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let locale = Locale.current
        let code = (locale as NSLocale).object(forKey: NSLocale.Key.countryCode) as! String?
        //init Picker
//        kPickerView.displayOnlyCountriesWithCodes = ["DK", "SE", "NO", "DE"] //display only
//        kPickerView.exeptCountriesWithCodes = ["RU"] //exept country
        kPickerView.countryPickerDelegate = self
        kPickerView.showPhoneNumbers = true
        kPickerView.setCountry(code!)
    }
    
    deinit {
        print(self)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        pickerCompletion?("", "")
        close()
    }
    
    @IBAction func sureAction(_ sender: Any?) {
        pickerCompletion?(code, name)
        close()
    }
    
    func show() {
        frame = UIScreen.main.bounds
        backgroundColor = UIColor(white: 0.0, alpha: 0.0)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.addSubview(self)
        warpViewBottom.constant = 0.0
        
        UIView.animate(withDuration: 0.3) {
            self.backgroundColor = UIColor(white: 0.0, alpha: 0.2)
            self.layoutIfNeeded()
        }
    }
    
    func close() {
        frame = UIScreen.main.bounds
        
        warpViewBottom.constant = -260
        
        UIView.animate(withDuration: 0.3, animations: { 
            self.backgroundColor = UIColor(white: 0.0, alpha: 0.0)
            self.layoutIfNeeded()
        }) { (completion) in
            self.removeFromSuperview()
        }
    }

}

extension KJLPickerForCountry: CountryPickerDelegate {
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        //pick up anythink
        self.code = phoneCode
        self.name = name
    }
}
