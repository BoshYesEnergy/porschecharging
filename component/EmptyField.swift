//
//  EmptyField.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

protocol EmptyFieldDelegate {
    func EmptyFieldDelegateForDelete(textField: UITextField)
}

class EmptyField: UITextField {

    var myDelegate: EmptyFieldDelegate?
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

//    var hasText: Bool {
//        return true
//    }
    
    
    override func deleteBackward() {
        if let mydelegate = myDelegate {
            mydelegate.EmptyFieldDelegateForDelete(textField: self)
        }
        print("Delete backward")
    }
}
