//
//  ShowTextFieldService.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/5/12.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class ShowTextFieldService: UIView {

    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
     
    typealias SelectCompletion = (String) -> Void
    var completion: SelectCompletion? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        settingView()
    }
}


//MARK:- Setting View
extension ShowTextFieldService {
    func settingView() {
        messageView.layer.masksToBounds = true
        messageView.layer.cornerRadius = 10
        titleLabel.text = ""
        countLabel.text = "0/8"
        titleTextField.delegate = self
        titleTextField.placeholder = ""
        titleTextField.becomeFirstResponder()
    }
}


//MARK:- Action
extension ShowTextFieldService {
    @IBAction func confirmAction(_ sender: UIButton) {
        if titleTextField.text?.count ?? 0 == 0 {
            ShowMessageView.showMessage(title: nil, message: "請輸入卡片名稱", completion: { (isOK) in })
        } else {
            titleTextField.resignFirstResponder()
            if let completion = completion {
                completion(titleTextField.text ?? "")
            }
            self.removeFromSuperview()
        }
    }
    
    @IBAction func cancelAction(_ sender: UIButton) {
        self.removeFromSuperview()
    }
}


//MARK:- text field delegate
extension ShowTextFieldService: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let count = text.count + string.count - range.length
        if count <= 8 {
            countLabel.text = "\(count)/8"
        } 
        return count <= 8
    }
}


//MARK:- custom
extension ShowTextFieldService {
    class func customView(title: String, textfield: String, confirm: String, cancel: String, completion: @escaping SelectCompletion) {
        guard let vw = Bundle.main.loadNibNamed("ShowTextFieldService", owner: self, options: nil)?.first as? ShowTextFieldService else {
            return
        }
        vw.titleLabel.text = title
        vw.titleTextField.placeholder = textfield
        vw.confirmBtn.setTitle(confirm, for: .normal)
        vw.cancelBtn.setTitle(cancel, for: .normal)
        vw.completion = completion
        vw.frame = UIScreen.main.bounds
        vw.layoutIfNeeded()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.addSubview(vw)
    }
}
