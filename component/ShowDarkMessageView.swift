//
//  ShowDarkMessageView.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class ShowDarkMessageView: UIView {

    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var sureBtn: UIButton!
//    @IBOutlet weak var cancelBtn: UIButton!
    
    // MARK: - Property
    typealias SelectCompletion = (Bool) -> Void
    var completion: SelectCompletion? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layoutIfNeeded()
        bottomView.backgroundColor = UIColor.clear
        
        messageView.layer.masksToBounds = true
        messageView.layer.cornerRadius = 8.0
        
        sureBtn.layer.masksToBounds = true
        sureBtn.layer.cornerRadius = 8.0
        
        titleLabel.text = ""
        messageLabel.text = ""
        lineView.isHidden = true
    }
    
    // MARK: - IBAction
    @IBAction func sureAction(_ sender: UIButton) {
        if let completion = completion {
            completion(true)
        }
        self.removeFromSuperview()
    }
    
    
//    @IBAction func cancelAction(_ sender: UIButton) {
//        self.removeFromSuperview()
//    }
}

extension ShowDarkMessageView {
    class func showOneBtnMessageWith(title: String?, message: String, btnTitle: String? = "確定" , btnTextColor: UIColor? = #colorLiteral(red: 0.2392156863, green: 0.7607843137, blue: 0.7843137255, alpha: 1), completion: @escaping SelectCompletion) {
        guard let vw = Bundle.main.loadNibNamed("ShowDarkMessageView", owner: self, options: nil)?.first as? ShowDarkMessageView else {
            return
        }
        
        if let btnTitle = btnTitle {
            vw.sureBtn.setTitle(btnTitle, for: .normal)
        }
        
        if let btnTextColor = btnTextColor {
            vw.sureBtn.setTitleColor(btnTextColor, for: .normal)
        }
        
        if let title = title, title.count > 0 {
            vw.titleLabel.text = "\n" + title + "\n"
        }
        
        vw.messageLabel.text = message
        vw.completion = completion
        vw.frame = UIScreen.main.bounds
        vw.layoutIfNeeded()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.addSubview(vw)
    }
    
//    class func showTwoBtnMessageWith(title: String?, message: String, sureBtnTitle: String? = "確定" , sureBtnTextColor: UIColor? = #colorLiteral(red: 0.2392156863, green: 0.7607843137, blue: 0.7843137255, alpha: 1) , cancelBtnTitle: String? = "取消" , cancelBtnTextColor: UIColor? = #colorLiteral(red: 0.4, green: 0.4, blue: 0.4, alpha: 1), completion: @escaping SelectCompletion) {
//        
//        guard let vw = Bundle.main.loadNibNamed("ShowDarkMessageView", owner: self, options: nil)?.last as? ShowDarkMessageView else {
//            return
//        }
//        
//        if let sureBtnTitle = sureBtnTitle {
//            vw.sureBtn.setTitle(sureBtnTitle, for: .normal)
//        }
//        
//        if let sureBtnTextColor = sureBtnTextColor {
//            vw.sureBtn.setTitleColor(sureBtnTextColor, for: .normal)
//        }
//        
//        if let cancelBtnTitle = cancelBtnTitle {
//            vw.cancelBtn.setTitle(cancelBtnTitle, for: .normal)
//        }
//        
//        if let cancelBtnTextColor = cancelBtnTextColor {
//            vw.cancelBtn.setTitleColor(cancelBtnTextColor, for: .normal)
//        }
//        
//        if let title = title, title.count > 0 {
//            vw.titleLabel.text = "\n" + title + "\n"
//        }
//        
//        vw.messageLabel.text = message
//        vw.completion = completion
//        vw.frame = UIScreen.main.bounds
//        vw.layoutIfNeeded()
//        UIApplication.shared.keyWindow?.addSubview(vw)
//    }
}
