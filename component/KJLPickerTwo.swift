//
//  KJLPickerTwo.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class KJLPickerTwo: UIView {
    
    typealias KJLPickerOneCompletion = (String?) -> Void
    var pickerCompletion: KJLPickerOneCompletion?
    
    @IBOutlet weak var wrapView: UIView!
    @IBOutlet weak var warpViewBottom: NSLayoutConstraint!
    @IBOutlet weak var kPickerView: UIPickerView!
    
    var locData: [SystemVerbClass.LocList] = []
    var secData: [SystemVerbClass.SecData] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let data = KJLCommon.getSaveSystemVerbClass()
        locData = data?.datas?.locData?.locList ?? []
        secData = locData.first?.secDatas ?? []
        
        kPickerView.reloadAllComponents()
    }
    
    deinit {
        print(self)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        pickerCompletion?(nil)
        close()
    }
    
    @IBAction func sureAction(_ sender: Any) {
        pickerCompletion?(secData[kPickerView.selectedRow(inComponent: 1)].locName)
        close()
    }
    
    func show() {
        frame = UIScreen.main.bounds
        backgroundColor = UIColor(white: 0.0, alpha: 0.0)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.addSubview(self)
        
        warpViewBottom.constant = 0.0
        
        UIView.animate(withDuration: 0.3) {
            self.backgroundColor = UIColor(white: 0.0, alpha: 0.2)
            self.layoutIfNeeded()
        }
    }
    
    func close() {
        frame = UIScreen.main.bounds
        
        warpViewBottom.constant = -260
        
        UIView.animate(withDuration: 0.3, animations: {
            self.backgroundColor = UIColor(white: 0.0, alpha: 0.0)
            self.layoutIfNeeded()
        }) { (completion) in
            self.removeFromSuperview()
        }
    }
    
}

extension KJLPickerTwo: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            return locData[row].secName
        }
        return secData[row].locName
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component == 0 {
            secData = locData[row].secDatas ?? []
            pickerView.reloadComponent(1)
        }
    }
}

extension KJLPickerTwo: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return locData.count
        }
        return secData.count
    }
}
