//
//  ScheduledTimer.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/9/16.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class ScheduledTimer {
    static let shared = ScheduledTimer()
    
    public func setLabel(_ label: UILabel, _ texts: String, time: Double) {
        var charIndex = 0.0
        for text in texts  {
            Timer.scheduledTimer(withTimeInterval: time * charIndex, repeats: false) { (_) in
                label.text?.append(text)
            }
            charIndex += 1
        }
    }
}
