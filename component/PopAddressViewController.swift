//
//  PopAddressViewController.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/6/16.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class PopAddressViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    
    var titles = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = titles
    }
     
}
