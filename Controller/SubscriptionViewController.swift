//
//  SubscriptionViewController.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/22.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class SubscriptionViewController: UIViewController {
    
    @IBOutlet weak var subTable: UITableView!
    
    private var sectionArray = ["Porsche Charging Service Subscription", "Remind"]
    private var isExpandList = [false, false]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        subTable.register(UINib(nibName: "SubscriptionHeaderCell", bundle: nil), forCellReuseIdentifier: "SubscriptionHeaderCell")
        subTable.register(UINib(nibName: "RemindCell", bundle: nil), forCellReuseIdentifier: "RemindCell")
        subTable.register(UINib(nibName: "AddSubscriptionCell", bundle: nil), forCellReuseIdentifier: "AddSubscriptionCell")
    }
}


//MARK: Table view delegate & data source
extension SubscriptionViewController: UITableViewDelegate, UITableViewDataSource  {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isExpandList[section] {
            return 1
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "AddSubscriptionCell", for: indexPath) as? AddSubscriptionCell {
                return cell
            } else {
                return UITableViewCell()
            }
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "RemindCell", for: indexPath) as? RemindCell {
                return cell
            } else {
                return UITableViewCell()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 {
            return UITableViewAutomaticDimension
        } else {
            return 250
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let header = tableView.dequeueReusableCell(withIdentifier: "SubscriptionHeaderCell") as? SubscriptionHeaderCell else { return nil }
        header.sectionString    = sectionArray[section]
        header.section          = section
        header.headerIcon.image = UIImage(named: isExpandList[section] == true ? "icon_arrow_down_default" : "icon_arrow_up_default")
        header.delegate         = self
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
}


extension SubscriptionViewController: SubscriptionHeaderPorocol {
    func headerTapAction(section: Int) { 
        isExpandList[section] = !isExpandList[section]
        self.subTable.reloadData() 
    }
}
