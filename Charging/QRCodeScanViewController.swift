//
//  QRCodeScanViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit
import AVFoundation

class QRCodeScanViewController: KJLNavView {
    @IBOutlet weak var topLayoutConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var idField: UITextField!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var mainViewTop: NSLayoutConstraint!
    @IBOutlet weak var sureBtn: UIButton!
    
    var captureSession = AVCaptureSession()
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var qrCodeFrameView: UIView?
    var deviceDiscoverySession: AVCaptureDevice? = nil
    var carType: String?
    private var qrCodeReadTime = 0
    private var callApiCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createNavViewForRightBtn(rightName: "", rightImage: "", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: UIColor.black)
        titleLab?.text = "QR Code"
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 14, height: self.idField.frame.height))
        idField.leftView = paddingView
        idField.leftViewMode = UITextFieldViewMode.always
        idField.delegate = self
        if (UIScreen.main.bounds.height == 568.0) {
            self.topLayoutConstraint.constant = 70
        }
        
        idField.layer.borderColor = #colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9333333333, alpha: 1)
        idField.layer.borderWidth = 1
        idField.keyboardType = .asciiCapable
        idField.autocapitalizationType = .allCharacters;
        idField.autocorrectionType = .yes;
        let mediaType = AVMediaType.video.rawValue
        let position: AVCaptureDevice.Position? = .back
        // 取得後置鏡頭來擷取影片
        if #available(iOS 10.0, *) {
            if let position = position {
                deviceDiscoverySession = AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: position)
            }else {
                deviceDiscoverySession = AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: .back)
            }
        } else {
            let devices = AVCaptureDevice.devices(for: AVMediaType(rawValue: mediaType))
            let devicePosition = position
            for deviceObj in devices {
                if deviceObj.position == devicePosition {
                    deviceDiscoverySession = deviceObj
                }
            }
            
            if let device = devices.first {
                deviceDiscoverySession = device
            }
        }
        
        guard let captureDevice = deviceDiscoverySession else {
            print("Failed to get the camera device")
            return
        }
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        do {
            let input = try AVCaptureDeviceInput(device: captureDevice)
            captureSession.addInput(input)
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession.addOutput(captureMetadataOutput)
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
            videoPreviewLayer?.frame = cameraView.layer.bounds
            cameraView.layer.addSublayer(videoPreviewLayer!)
            captureSession.startRunning()
        } catch {
            print(error)
            return
        }
        
        KJLRequest.shareInstance().kJLRequestDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        qrCodeReadTime = 0
        callApiCount = 0
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // 開燈
    @IBAction func lightAction(_ sender: UIButton) {
        self.changeFlashMode()
    }
    
    func changeFlashMode(isForceClose:Bool = false) {
        guard let device = deviceDiscoverySession else { return }
        
        do{
            if (device.hasTorch){
                try device.lockForConfiguration()
                if device.torchMode == .on || isForceClose == true{
                    device.torchMode = .off
                    //                    device.flashMode = .off
                    let photoSettings = AVCapturePhotoSettings()
                    photoSettings.flashMode = .off
                } else {
                    device.torchMode = .on
                    let photoSettings = AVCapturePhotoSettings()
                    photoSettings.flashMode = .on
                    //                    device.flashMode = .on
                }
                device.unlockForConfiguration()
            }
        }catch{
            //DISABEL FLASH BUTTON HERE IF ERROR
            print("Device tourch Flash Error ");
        }
    }
    
    // 確認
    @IBAction func sureAction(_ sender: Any) {
        guard let qrNo = idField.text else {
            ShowMessageView.showMessage(title: nil, message: "編號錯誤，請再重新輸入")
            qrCodeReadTime = 0
            return
        }
        
        guard qrNo.count > 0 else {
            ShowMessageView.showMessage(title: nil, message: "請輸入充電槍代碼")
            qrCodeReadTime = 0
            return
        }
        
        DispatchQueue.main.async {
            self.changeFlashMode(isForceClose: true)
        }
        
        // call api count = 1, if count != 1 is return,
        // check sureAction don't double enter
        callApiCount += 1
        guard callApiCount == 1 else { return }
        // API:401
        KJLRequest.requestForChargerPairing(qrNo: qrNo, completion: { (response) in
            guard let response = response as? ChargerPairingModel else {
                self.callApiCount = 0
                return
            } 
            guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReadyChargingViewController") as? ReadyChargingViewController else { return }
            vc.data = response
            vc.qrNo = qrNo
            let nav = UINavigationController(rootViewController: vc)
            nav.modalPresentationStyle = .overFullScreen
            self.present(nav, animated: true, completion: nil)
            
        })
        
    }
}

extension QRCodeScanViewController: AVCaptureMetadataOutputObjectsDelegate {
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        idField.resignFirstResponder()
        
        if metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            return
        }
        
        // 取得元資料（metadata）物件
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if metadataObj.type == AVMetadataObject.ObjectType.qr {
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            
            if metadataObj.stringValue != nil {
                //                messageLabel.text = metadataObj.stringValue
                if (metadataObj.stringValue?.hasPrefix("https://s.yulon-energy.com:8891/d/"))!
                {
                    let filteredData = metadataObj.stringValue?.components(separatedBy: "https://s.yulon-energy.com:8891/d/")
                    idField.text = filteredData?[1]
                    qrCodeReadTime += 1
                    if (qrCodeReadTime == 1) {
                        self.sureAction(self.sureBtn)
                        return
                    }
                }
            }
        }
    }
}

extension QRCodeScanViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.2) {
            self.mainViewTop.constant = -200
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.2) {
            self.mainViewTop.constant = 0.0
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension QRCodeScanViewController: KJLRequestDelegate {
    func apiFailResultWith(resCode: String , message: String) {
        if (resCode == "0802") {
            ShowMessageView.showQuestionWithCustomButton(messageTitle: nil, message: message, sureBtnTitle: "前往預約記錄", cancelBtnTitle: "取消") { (_) in
                self.qrCodeReadTime = 0
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReserveRecordViewController") as? ReserveRecordViewController {
                    self.navigationController?.show(vc, sender: self)
                }
            }
        } else {
            ShowMessageView.showMessage(title: nil, message: message, completion: { (_) in
                self.qrCodeReadTime  = 0
            })
        }
    }
}

extension QRCodeScanViewController {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //防止删除键无效
        if string == "" {
            return true;
        }
        
        let typeString = UserDefaults.standard.string(forKey: "filterCarType") ?? "1"
        if (typeString == "1") {   // 汽車
            //设置只能输入str后面字符串中的字符
            let str = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXY";
            let cs:NSCharacterSet = NSCharacterSet.init(charactersIn: str);
            let str1 = string.components(separatedBy: cs as CharacterSet);
            let str2 = str1.joined(separator: "");
            let result = str2 == string;
            print(result);
            return !result;
        }
        return true
    }
}
