//
//  ChargingListViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class ChargingListViewController: KJLNavView {

    @IBOutlet weak var timeLab: UILabel!
    @IBOutlet weak var ktable: UITableView!
    @IBOutlet weak var cornerView: UIView!
    var userLocation:CLLocation?
    var titleStr: String?
    var lists: [StationListClass.Data] = []
    var isLove: Bool = false
    var isMotorcycle: Bool = false
    var krefresh = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.isCarButtonOpen = true
        createNavViewForRightBtn(rightName: "", rightImage: "home_btn_filter", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: UIColor.black)
        self.carBtn!.addTarget(self, action: #selector(changeCarType), for: .touchUpInside)
        titleLab?.text = titleStr
              // 判斷是不是 iPhoneX
        rightBtn?.addTarget(self, action: #selector(filterAction), for: .touchUpInside)
        self.backBtn?.isHidden = true
        self.cornerView.layer.cornerRadius = self.cornerView.bounds.size.height / 2
        self.cornerView.layer.borderColor = UIColor.black.cgColor
        self.cornerView.layer.borderWidth = 1
        
        //ktable.setupMerryChristmas()
        ktable.register(UINib(nibName: "ChargingListCell", bundle: nil), forCellReuseIdentifier: "ChargingListCell")
//        ktable.contentInset = UIEdgeInsetsMake(15, 0, 0, 0)
        
        krefresh.addTarget(self, action: #selector(loadAPI), for: .valueChanged)
        ktable.addSubview(krefresh)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) 
        let typeString = UserDefaults.standard.string(forKey: "filterCarType") ?? "1"
        if (typeString == "1") {
            //點選車子狀態顯示
            isMotorcycle = false
            self.carBtn!.setImage(UIImage(named: "home_btn_car"), for: .normal)
        } else {
            //點選機車狀態顯示
            isMotorcycle = true
            self.carBtn!.setImage(UIImage(named: "home_btn_scooter"), for: .normal)
        }
        
        
        LocationManagers.updateLocation { [unowned self] (status, location) in
            if  status == .Success {
                self.userLocation = location
            }else{
                self.userLocation = CLLocation.init(latitude: 25.0329893, longitude: 121.5326985);
            }
            self.loadAPI()
        }
        
    }

    
    @objc func changeCarType() {
        let typeString = UserDefaults.standard.string(forKey: "filterCarType") ?? "1"
        if (typeString == "1") {
            //點選車子狀態顯示
            UserDefaults.standard.set("2", forKey: "filterCarType")
            isMotorcycle = true
            self.carBtn!.setImage(UIImage(named: "home_btn_scooter"), for: .normal)
        } else {
            //點選機車狀態顯示
            UserDefaults.standard.set("1", forKey: "filterCarType")
             isMotorcycle = false
            self.carBtn!.setImage(UIImage(named: "home_btn_car"), for: .normal)
        }
        loadAPI()
    }
    
    @objc func loadAPI() {
        let nowDate = Date(timeIntervalSinceNow: 0)
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd HH:mm"
        let str = format.string(from: nowDate)
        timeLab.text = str
        let typeString = UserDefaults.standard.string(forKey: "filterCarType") ?? "1"
        let queryType = (UserDefaults.standard.bool(forKey: "showFavourite") == true) ? KJLRequest.RequestQueryType.Favourite : KJLRequest.RequestQueryType.Station
        KJLRequest.requestForStationList(queryType: queryType, queryString: "", type: typeString) { (response) in
            self.krefresh.endRefreshing()
            guard let response = response as? StationListClass else {
                return
            }
            
            //            let range = Float((response.distance ?? 0) / 1000)
            if let lists = response.datas {
                var temps: [StationListClass.Data] = []
                for t in lists {
                    var n = t
                    n.myDistance = self.calDistance(data: t)
                    //                    let f = Float((n.myDistance ?? "0")) ?? 0.0
                    if self.isLove == true {
                        temps.append(n)
                    }else {
                        //                        if f < range {
                        //                            temps.append(n)
                        //                        }
                        temps.append(n)
                    }
                }
                temps = temps.sorted(by: { (a, b) -> Bool in
                    let f1 = Float((a.myDistance ?? "0")) ?? 0.0
                    let f2 = Float((b.myDistance ?? "0")) ?? 0.0
                    return f1 < f2
                })
                
                // 篩選 汽車/機車
                var tempDatas = [StationListClass.Data]()
                for aData in temps
                {
                    if (self.isMotorcycle == false)
                    {
                        // select car
                        if aData.type == "1"
                        {
                            tempDatas.append(aData)
                        }
                    }
                    else
                    {
                        // select motorcycle
                        if aData.type == "2"
                        {
                            tempDatas.append(aData)
                        }
                    }
                    
                    if (aData.type == "3") {
                        tempDatas.append(aData)
                    }
                }
                
                self.lists = tempDatas
                self.ktable.reloadData()
            }else {
                self.lists = []
                self.ktable.reloadData()
            }
        }
    }
    
    func calDistance(data: StationListClass.Data?) -> String {
        guard let data = data else {
            return ""
        }
        
        let lat = Float(data.latitude ?? "0") ?? 0
        let long = Float(data.longitude ?? "0") ?? 0
        if let location = self.userLocation  {
            let current = CLLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            let location = CLLocation(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(long))
            let distance = current.distance(from: location)
            let meter = "\(distance)"
            return String(format: "%.2f", (Float(meter) ?? 0)/1000)
        }else{
            return "0"
        }
//        distanceLab.text = km + "公里"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func filterAction() {
        guard KJLRequest.isLogin() == true else {
            KJLCommon.showLogin(subcontrol: self)
            return
        } 
    }
}

extension ChargingListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChargingListCell", for: indexPath)
        
        if let cell = cell as? ChargingListCell {
            let data = lists[indexPath.row]
            cell.userLocation = self.userLocation
            cell.changeData(data: data)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) { 
        if let vc = storyboard?.instantiateViewController(withIdentifier: "ChargingStationDetailViewController") as? ChargingStationDetailViewController {
            let data = lists[indexPath.row]
            if let no = data.stationId {
                vc.setupVCWith(stationNo: "\(no)")
            }
            
            navigationController?.show(vc, sender: self)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
}
