//
//  ChargingNameTableViewCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class ChargingNameTableViewCell: UITableViewCell {

//    @IBOutlet weak var imageCell: UIImageView!
//    @IBOutlet weak var nameCell: UILabel!
//    @IBOutlet weak var imageBottomView: UIView!

    
    @IBOutlet weak var headImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var noBottomBorderView: UIView!
    @IBOutlet weak var withBottomBorderView: UIView!
    @IBOutlet weak var grayLineView: UIView!
    @IBOutlet weak var cornerImage: UIImageView!
    
    private var isLastCell: Bool = false
    private var indexPath: IndexPath?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.noBottomBorderView.isHidden = true
        self.withBottomBorderView.isHidden = true
    }
 
    
    func changeColor(isChange: Bool) {
        if (isLastCell == true) {
            if isChange == true {
                if self.isSelected == true {
                    self.cornerImage.image = UIImage(named:"ac_bottom_gray")
                    self.nameLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                } else {
                    self.cornerImage.image = UIImage(named:"ac_bottom")
                    self.nameLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//                    self.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                }
            } else {
                // isChange == false
                if self.isSelected == true {
                    self.cornerImage.image = UIImage(named:"ac_bottom_cycan")
                    self.nameLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
//                    self.backgroundColor = #colorLiteral(red: 0.2392156863, green: 0.7607843137, blue: 0.7843137255, alpha: 1)
                }else {
                    self.cornerImage.image = UIImage(named:"ac_bottom")
                    self.nameLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//                    self.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                }
            }
        } else {
            if isChange == true {
                if self.isSelected == true {
                    self.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
                    self.nameLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                } else {
                    self.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                    self.nameLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                }
            } else {
                // isChange == false
                if self.isSelected == true {
                    self.backgroundColor = #colorLiteral(red: 0, green: 0.768627451, blue: 0.7960784314, alpha: 1)
                    self.nameLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                }else {
                    self.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                    self.nameLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                }
            }
        }
    }
    
    func setupCellWith(isMotorTable: Bool , indexPath: IndexPath , title: String , isSelected: Bool , isChangeBlackColor: Bool , isLastCell: Bool) {
        self.indexPath = indexPath
        self.nameLabel.text = title
        self.isSelected = isSelected
        self.isLastCell = isLastCell

        if (title == "SAE J1772") {
            nameLabel.text = "SAEJ1772\nCNS15511\nIEC62196-2"
        }
        
        if (isMotorTable == true) {
            if (isSelected == true) {
                headImage.image = UIImage(named: "TES_white")
            } else {
                headImage.image = UIImage(named: "TES")
            }
        } else {
            if (isSelected == true) {
                let name = title
                var rename = name.replacingOccurrences(of: "/", with: ":")
                rename = "home_img_" + rename + "_white"
                headImage.image = UIImage(named: rename)
            } else {
                let name = title
                var rename = name.replacingOccurrences(of: "/", with: ":")
                rename = "home_img_" + rename
                headImage.image = UIImage(named: rename)
            }
        }
        
        if (isLastCell == true) {
            self.withBottomBorderView.isHidden = false
            self.noBottomBorderView.isHidden = true
            self.grayLineView.isHidden = true
        } else {
            self.withBottomBorderView.isHidden = true
            self.noBottomBorderView.isHidden = false
        }
        
        self.changeColor(isChange: isChangeBlackColor)
        
    }
}
