//
//  ChargingPileListViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class ChargingPileListViewController: KJLNavView {

    var titleStr: String = ""
    var stationId: String?
    
    @IBOutlet weak var ktable: UITableView!
    var lists: [SocketListClass.Data] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        createNavViewForRightBtn(rightName: "", rightImage: "", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: UIColor.black)
//        titleLab?.text = "充電樁列表"
//        titleLab?.text = "準備充電"
        titleLab?.text = titleStr
        
        
        ktable.register(UINib(nibName: "ChargingPieCell", bundle: nil), forCellReuseIdentifier: "ChargingPieCell")
        ktable.contentInset = UIEdgeInsetsMake(0, 0, 103, 0)
        
        KJLRequest.requestForSocketList(stationId: stationId ?? "", stationCategory: "") { (response) in
            guard let response = response as? SocketListClass else {
                return
            }
            
            if let temp = response.datas {
                self.lists = temp
                self.ktable.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension ChargingPileListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChargingPieCell", for: indexPath)
        if let cell = cell as? ChargingPieCell {
            cell.changeData(data: lists[indexPath.row])
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        guard let qrNo = lists[indexPath.row].qrNo else {
//            return
//        }
//
//        KJLRequest.requestForChargerPair(qrNo: qrNo, completion: { (response) in
//            guard let response = response as? ChargerPairClass else {
//                return
//            }
//
//            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReadyChargingViewController") as? ReadyChargingViewController {
//                vc.data = response
//                self.navigationController?.show(vc, sender: self)
//            }
//        })
        
        
        
//        guard KJLRequest.isLogin() == true else {
//            KJLCommon.showLogin(subcontrol: self)
//            return
//        }
        
        
//        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "QRCodeScanViewController") as? QRCodeScanViewController {
//            self.navigationController?.show(vc, sender: self)
//        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.makeShadowAndCornerRadius()
    }
}
