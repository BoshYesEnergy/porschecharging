//
//  ChargingPieCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class ChargingPieCell: UITableViewCell {

    @IBOutlet weak var imageCell: UIImageView!
    @IBOutlet weak var typeLab: UILabel!
    @IBOutlet weak var chargingNameLab: UILabel!
    @IBOutlet weak var chargingIDLab: UILabel!
    @IBOutlet weak var wLab: UILabel!
    @IBOutlet weak var dataView: UIView!
    
    @IBOutlet weak var chargPieStateLab: UILabel!
    @IBOutlet weak var pieNameLab: UILabel!
    
    override var frame: CGRect {
        get {
            return super.frame
        }
        set(newFrame) {
            var frame = newFrame
            frame.origin.x += 20
            frame.origin.y += 10
            frame.size.width -= 40
            frame.size.height -= 20
            super.frame = frame
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()

        imageCell.image = nil
        typeLab.text = ""
        chargingNameLab.text = ""
        chargingIDLab.text = ""
        wLab.text = ""
        chargPieStateLab.text = ""
        pieNameLab.text = ""
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func changeData(data: SocketListClass.Data) {
        let name = data.socketType ?? ""
        let rename = name.replacingOccurrences(of: "/", with: ":")
        imageCell.image = UIImage(named: rename)
        typeLab.text = data.powerType
        chargingNameLab.text = data.socketType
        chargingIDLab.text = data.stationAbrv
        wLab.text = data.kwh
        chargPieStateLab.text = data.status
//        pieNameLab.text = (data.qrNo ?? "") + "樁"
        pieNameLab.text = "充電槍代碼：" + (data.qrNo ?? "")
    }
    
    override func layoutIfNeeded() {
        dataView.layoutIfNeeded()
        dataView.shadowsType(kcolor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
    }
}
