//
//  TimeoutFeeCalculateViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

extension Date {
    func daysBetweenDate(toDate: Date) -> Int {
        let components = Calendar.current.dateComponents([.second], from: self, to: toDate)
        return components.second ?? 0
        
    }
    
    func stringByDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        return dateFormatter.string(from: self)
    }
}

extension String {
    func dateByString() -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        return dateFormatter.date(from: self)!
    }
}


class TimeoutFeeCalculateViewController: KJLNavView {
    @IBOutlet weak var chargingStateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var chargingImageView: UIImageView!
    var reserveDetailClass:ReserveDetailClass?
    
    var timer: Timer?
    var timer10sec: Timer?
    var sec: NSInteger = 0
    var tranNo: Int64?
    var today: Date?
    var old: Date?
    var isFetchData: Bool = false
    
    @IBAction func buttonAction(_ sender: Any) {
        KJLRequest.checkChargingOvertimeStatus(tranNo:"\(tranNo ?? 0)") { (response) in
            guard let response = response as? ReserveDetailClass else {
                return
            }
            
            guard let data = response.datas else {
                return
            }
            
            if data.gunIsEnd == "Y" {
                //充電結果頁
                self.invalidateTime()
                //充電結果頁
                self.openCompleteChargingViewController()
            }else{
                ShowMessageView.showMessage(title: "", message: "您尚未掛回充電槍，\n請再次檢查，\n是否已正常掛回。") { (isOK) in
                }
            }
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchData()
        
//        createNavViewForRightBtn(rightName: "", rightImage: "", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: UIColor.black) 
        titleLab?.text = "逾時倒數警示( 3/5 )"
        self.backBtn?.isHidden = true
    }
    
    func dateToDateString(_ date:Date) -> String {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.locale = Locale(identifier: "zh_Hant_TW") // 設定地區(台灣)
        dateFormatter.timeZone = TimeZone(identifier: "Asia/Taipei") // 設定時區(台灣)
        return dateFormatter.string(from: date)
    }
    
    func timeStringToDate(_ dateStr:String) ->Date {
        let dateFormatter2 = DateFormatter()
        dateFormatter2.timeZone = TimeZone(secondsFromGMT: 0)
        dateFormatter2.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter2.date(from: dateStr)!
    }
    
    @objc func fetchData() {
        if isFetchData { return }
        self.isFetchData = true

        // 假裝呼叫 api
        KJLRequest.checkChargingOvertimeStatus(tranNo:"\(tranNo ?? 0)") { (response) in
            guard let response = response as? ReserveDetailClass else {
                return
            }
            
            guard let data = response.datas else {
                return
            }
            
            if data.gunIsEnd == "Y"
            {
                //充電結果頁
                self.invalidateTime()
                self.openCompleteChargingViewController()
            }else{  
                self.chargingStateLabel.text = "" + (data.finishCode_ch ?? "")
                let evEndTime : String? = data.evEndTime //"2019-07-02 01:36:37" // 充電結束時間 yyyy-MM-dd HH:mm:ss
                if evEndTime != nil && evEndTime != "" {
                    let overTime : String? = data.overTime ?? "0"//"120" // 緩衝時間 (分鐘)
                    let nowTime : String? = data.nowTime//"2019-07-02 01:44:37" // Server 現在時間 yyyy-MM-dd HH:mm:ss
                    
                    var dc = DateComponents()
                    dc.minute = Int(overTime!)
                    dc.second = 1
                    self.old = Calendar.current.date(byAdding: dc, to: self.timeStringToDate(evEndTime!))
                    self.today = self.timeStringToDate(nowTime!)
                    self.sec = self.today!.daysBetweenDate(toDate: self.old!)
                    self.configSec()
                    self.reserveDetailClass = response
                }
            }
        }
        
        self.isFetchData = false
        self.fetchData10Sec()
    }
    
    func fetchData10Sec() {
        if timer10sec == nil {
            timer10sec = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(fetchData), userInfo: nil, repeats: true)
            timer10sec?.fire()
        }
    }
    
    func configSec() {
        if timer == nil {
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(countDown), userInfo: nil, repeats: true)
            timer?.fire()
        }
    }
    
    @objc func countDown() {
        if (self.sec <= 0) {
            self.timeLabel.text = "00:00:00"
            self.invalidateTime()
            let timeoutFeeBufferViewController = TimeoutFeeBufferViewController()
            timeoutFeeBufferViewController.reserveDetailClass = reserveDetailClass;
            timeoutFeeBufferViewController.tranNo = tranNo
            self.navigationController?.pushViewController(timeoutFeeBufferViewController, animated: true)
        } else {
            self.sec = self.sec - 1
            let hh = String(format: "%02d", self.sec / 3600)
            let mm = String(format: "%02d", (self.sec / 60) % 60)
            let ss = String(format: "%02d", self.sec % 60)
            self.timeLabel.text = "\(hh):\(mm):\(ss)"
        }
    }
    
    func invalidateTime() {
        if (timer != nil) {
            timer?.invalidate()
        }
        
        if (timer10sec != nil) {
            timer10sec?.invalidate()
        }
    }
    
    @objc override func backAction() {
        self.invalidateTime()
//        if self.navigationController == nil {
//            self.dismiss(animated: true, completion: nil)
//        }else {
//            self.navigationController?.popViewController(animated: true)
//        }
        self.openCompleteChargingViewController()
    }
    
    func openCompleteChargingViewController() {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        if let vc = storyboard.instantiateViewController(withIdentifier: "CompleteChargingViewController") as? CompleteChargingViewController {
//            vc.tranNo = self.tranNo
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
    }
    
}
