//
//  DetailImageCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit
import Kingfisher

class DetailImageCell: UICollectionViewCell {
    
    @IBOutlet weak var imageCell: UIImageView!
    
    func changeData(data: StationDetailClass.Data.PicArray?) {
        guard let data = data else {
            imageCell.image = nil
            return
        }
        
        let tempUrl = URL(string: data.picPath ?? "")
        imageCell.kf.setImage(with: tempUrl, placeholder: nil, options: nil, progressBlock: nil) { (img, error, type, url) in
            self.imageCell.image = img//UIImage(named: "text_icon2")
        }
    }
    
    func changeCharging(name: String) {
        let rename = name.replacingOccurrences(of: "/", with: ":")
        imageCell.image = UIImage(named: rename)
    }
}
