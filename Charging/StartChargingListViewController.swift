//
//  StartChargingListViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class StartChargingListViewController: KJLNavView {

    @IBOutlet weak var ktable: UITableView!
    var lists: [ChargingListClass.ChargingList]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        createNavViewForRightBtn(rightName: "", rightImage: "", leftName: "", leftImage: "ic_back", groundColor: #colorLiteral(red: 0.1497560143, green: 0.7302395701, blue: 0.7572383881, alpha: 1), isShadow: true, titleColor: UIColor.white)
        titleLab?.text = "充電中列表"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        KJLRequest.requestForChargingListData { (response) in
            guard let response = response as? ChargingListClass
                else { return }
            self.lists = []
            self.lists?.append(response.datas!)
            self.ktable.reloadData()
        }
    }
}

extension StartChargingListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.lists?.count ?? 0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StartChargingListCell", for: indexPath)
        
        if let cell = cell as? StartChargingListCell, let lists = lists {
            if indexPath.row + 1 == lists.count {
                cell.changeData(data: lists[indexPath.row], isLine: true)
            }else {
                cell.changeData(data: lists[indexPath.row], isLine: false)
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let lists = lists else {
            return
        }
        if let vc = storyboard?.instantiateViewController(withIdentifier: "StartChargingViewController") as? StartChargingViewController {
            let data = lists[indexPath.row]
//            vc.chargingNo = data.tranNo
//            vc.nameStr = data.stationName
//            vc.idStr = data.tranNo == nil ? "" : "\((data.tranNo)!)"
            vc.tranNo = data.tranNo
            vc.nameStr = data.stationName
            vc.idStr = data.qrNo
            navigationController?.show(vc, sender: self)
        }
    }
}
