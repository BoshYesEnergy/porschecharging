//
//  CheckChargingViewController.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/8/24.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class CheckChargingViewController: KJLNavView { 
    @IBOutlet weak var pieImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var cancelBtn: UIButton!
    
    var data: ChargerPairingModel?
    var qrNo = ""
    var tranNo: Int64?
    var timer: Timer?
    var second: TimeInterval = 5 
     
    override func viewDidLoad() {
        super.viewDidLoad()
        setView()
        setNav()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setTimer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopTimer()
    }
}


//MARK:- setting View
extension CheckChargingViewController {
    private func setView() {
        
        guard let datas = data else { return }
        
        self.nameLabel.text = datas.datas?.stationName
        self.codeLabel.text = "充電槍代碼：" + qrNo
        let name = datas.datas?.socketType ?? ""
        let rename = name.replacingOccurrences(of: "/", with: ":")
        self.pieImage.image = UIImage(named: rename)
    }
    
    private func setNav() {
        createNavViewForRightBtn(rightName: "", rightImage: "", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: UIColor.black)
        titleLab?.text = "確認啟動充電"
        rightBtn?.removeTarget(self, action: nil, for: .allEvents)
        rightBtn?.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
    }
    
    private func setTimer() {
        timer = Timer.scheduledTimer(timeInterval: second,
                                     target: self,
                                     selector: #selector(callAPIRequestForChargingStatus),
                                     userInfo: nil, repeats: true)
        timer?.fire()
    }
    
    private func stopTimer() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
            print("stop ~ timer ")
        }
    }
}


//MARK:- Action
extension CheckChargingViewController {
    @IBAction func cancel(_ sender: UIButton) {
        dismissVC()
    }
    
    @objc private func dismissVC() {
        KJLRequest.requestForStopCharging(tranNo: "\(self.tranNo ?? 0)") { (response) in
            guard response != nil else { return }
            self.dismiss(animated: true, completion: nil)
        } 
    }
    
    @objc func callAPIRequestForChargingStatus() {
        KJLRequest.requestForChargingStatus(tranNo:"\(tranNo ?? 0)") {[weak self] (response) in
            guard let response = response as? ChargingStatusClass
                else { return }
            // status : 充電中Charging ; 完成 Finish ; 中斷 Interrupt ; 原頁等待 available ; 返回 timeout  
            if response.datas?.status?.lowercased() == "charging" {
                self?.stopTimer()
                let vc = self?.storyboard?.instantiateViewController(withIdentifier: "StartChargingViewController") as! StartChargingViewController
                vc.tranNo = Int64(self?.tranNo ?? 0)
                vc.nameStr = self?.data?.datas?.stationName
                vc.idStr = self?.qrNo
                self?.navigationController?.show(vc, sender: self)
            } else if response.datas?.status?.lowercased() == "timeout"{
                self?.stopTimer()
//                ShowMessageView.showMessage(title: "提示", message: "因超過按下充電樁開始按鈕的時間，請返回重新操作一次，謝謝") { (_) in
//                    let vc = self?.storyboard?.instantiateViewController(withIdentifier: "CompleteChargingViewController") as! CompleteChargingViewController
//                    vc.tranNo = self?.tranNo
//                    self?.navigationController?.show(vc, sender: self)
//                }
            }
        }
    }
    
}
