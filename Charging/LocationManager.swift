//
//  LocalNotification.swift
//  AppleMap
//
//  Created by AllenShiu on 2017/3/23.
//  Copyright © 2017年 AllenShiu. All rights reserved.
//

import UIKit
import CoreLocation
import Foundation

enum LocationManagerStatus : NSInteger{
    case Success
    case LocationServiceDisable
    case Fail
}

// closure 縮寫
typealias UpdateComplection = (_ status: LocationManagerStatus, _ location: CLLocation?)-> Void

// MARK: CLLocationManagerDelegate
extension LocationManager : CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        myLocation = CLLocation(latitude: locations[0].coordinate.latitude, longitude: locations[0].coordinate.longitude)
        if self.complection != nil {
            self.complection!(.Success,locations.first!)
            self.complection = nil
        }
        stopUpdating()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error){
        if self.complection != nil {
            self.complection!(.Fail,nil)
            self.complection = nil
        }
        stopUpdating()
    }
}

// MARK - public method
extension LocationManager{
    /**
     @abstract 更新使用者 gps 座標
     @discussion 從回調中可以拿回座標取得狀態, 與 gps 座標
     @param completion 回調傳回 status 成功或是失敗, location 為其 gps 座標
     */
    func updateLocation(completion:@escaping UpdateComplection){
        if CLLocationManager.locationServicesEnabled() == false {
            let location: CLLocation = CLLocation()
            completion(.LocationServiceDisable,location)
            return
        }
        complection = completion
        startUpdating()
    }
}

// MARK - private method
extension LocationManager {
    fileprivate func startUpdating() {
        if !isLocationUpdating {
            isLocationUpdating = true
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
    
    fileprivate func stopUpdating(){
        locationManager.stopUpdatingLocation()
        isLocationUpdating = false
    }
}

let LocationManagers:LocationManager = LocationManager.instanceShare

// MARK - life cycle
class LocationManager: NSObject {
    // 實例分享: 讓所有人都使用同一個對象
    static let instanceShare:LocationManager = {
        let locationManager = LocationManager()
        return locationManager
    }()
    
    var isLocationUpdating:Bool = false
    let locationManager:CLLocationManager = CLLocationManager()
    var complection:UpdateComplection?
    var myLocation: CLLocation = CLLocation()
    
    override init() {
        super.init()
        locationManager.delegate = self
    }
}
