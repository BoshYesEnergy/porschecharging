//
//  CarTimeoutFeeBufferViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class CarTimeoutFeeBufferViewController: TimeoutFeeBufferViewController {

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.chargingImageView.image = UIImage(named: "Car_Unplug")
    }

    override func openCompleteChargingViewController() {
        self.invalidateTime()
        let carCompleteChargingViewController = CarCompleteChargingViewController()
        carCompleteChargingViewController.tranNo = self.tranNo
        self.navigationController?.pushViewController(carCompleteChargingViewController, animated: true)
    }
    
}
