//
//  ParkingSpaceStatusInfoCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class ParkingSpaceStatusInfoCell: UITableViewCell {
    @IBOutlet weak var currentView: UIView!
    @IBOutlet weak var voltageView: UIView!
    @IBOutlet weak var kwView: UIView!
    
    @IBOutlet weak var currentLab: UILabel!
    @IBOutlet weak var voltageLab: UILabel!
    @IBOutlet weak var kwhLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        currentView.layer.cornerRadius = 10
        currentView.layer.masksToBounds = true
        currentView.layer.borderWidth = 1
        currentView.layer.borderColor = #colorLiteral(red: 0.8980392157, green: 0.8980392157, blue: 0.8980392157, alpha: 1)
        
        voltageView.layer.cornerRadius = 10
        voltageView.layer.masksToBounds = true
        voltageView.layer.borderWidth = 1
        voltageView.layer.borderColor = #colorLiteral(red: 0.8980392157, green: 0.8980392157, blue: 0.8980392157, alpha: 1)
        
        kwView.layer.cornerRadius = 10
        kwView.layer.masksToBounds = true
        kwView.layer.borderWidth = 1
        kwView.layer.borderColor = #colorLiteral(red: 0.8980392157, green: 0.8980392157, blue: 0.8980392157, alpha: 1)
    }
}
