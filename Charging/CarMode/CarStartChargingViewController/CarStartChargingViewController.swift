//
//  CarStartChargingViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

enum StartChargingCellType:Int {
    case StartChargingCellTypeInfo = 0
    case StartChargingCellTypeGIF = 1
    case StartChargingCellTypeParkingSpaceStatusInfo = 2
    case StartChargingCellTypeChargingStatusInfo = 3
    case StartChargingCellTypeChargingInfo = 4
    case StartChargingCellTypeStopMessage = 5
}

extension CarStartChargingViewController {
    func setupInitValue() {
        if #available(iOS 13.0, *) {
            //self.chargingTableView.automaticallyAdjustsScrollIndicatorInsets = false
        } else if #available(iOS 11.0, *) {
            self.startChargingTableView.contentInsetAdjustmentBehavior = .never
        }else{
            self.automaticallyAdjustsScrollViewInsets = false
        }
    }
    
    func setupNavView() {
        createNavViewForRightBtn(rightName: "", rightImage: "", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: UIColor.black)
        
        if self.lockCode == "" {
            titleLab?.text = "充電中(2/5)"
        }else{
            titleLab?.text = "充電中(3/5)"
        }
    }
    
    func setupTableView() {
        self.topLayoutConstraint.constant = KJLCommon.isX() == true ? 88 : 64
        self.startChargingTableView.dataSource = self
        self.startChargingTableView.delegate = self
        self.startChargingTableView.backgroundColor = UIColor.clear
        self.startChargingTableView.separatorStyle = .none
        
        self.startChargingTableView.register(UINib(nibName: "LockInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "LockInfoTableViewCell")
        self.startChargingTableView.register(UINib(nibName: "CarInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "CarInfoTableViewCell")
        self.startChargingTableView.register(UINib(nibName: "ParkingSpaceStatusInfoCell", bundle: nil), forCellReuseIdentifier: "ParkingSpaceStatusInfoCell")
        self.startChargingTableView.register(UINib(nibName: "UseInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "UseInfoTableViewCell")
        self.startChargingTableView.register(UINib(nibName: "ImageViewGIFTableViewCell", bundle: nil), forCellReuseIdentifier: "ImageViewGIFTableViewCell")
        self.startChargingTableView.register(UINib(nibName: "StopMessageTableViewCell", bundle: nil), forCellReuseIdentifier: "StopMessageTableViewCell")
    }
    
    func fetchData5Sec() {
           if timer10sec == nil {
               timer10sec = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(fetchData), userInfo: nil, repeats: true)
               timer10sec?.fire()
               RunLoop.current.add(timer10sec!, forMode: .commonModes)
           }
       }
    
}

//MARK: UITableViewDataSource
extension CarStartChargingViewController: UITableViewDataSource ,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return StartChargingCellType.StartChargingCellTypeStopMessage.rawValue + 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellType:StartChargingCellType = StartChargingCellType(rawValue: indexPath.item)!
        if cellType == .StartChargingCellTypeChargingStatusInfo ,self.lockCode == ""{
            return 0
        }
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellType:StartChargingCellType = StartChargingCellType(rawValue: indexPath.item)!
        switch cellType {
        case .StartChargingCellTypeInfo:
            let cell = tableView.dequeueReusableCell(withIdentifier: "LockInfoTableViewCell", for: indexPath) as! LockInfoTableViewCell
            cell.siteNameLabel.text = self.nameStr
            cell.charginCode.text = "充電槍代碼 " + self.idStr
            cell.lockCode.text = "地鎖狀態 " + self.lockCode
            cell.lockCode.isHidden = (self.lockCode == "")
            cell.selectionStyle = .none
            cell.contentView.backgroundColor = UIColor.white
            cell.backgroundColor = UIColor.white
            return cell
        case .StartChargingCellTypeGIF:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ImageViewGIFTableViewCell", for: indexPath) as! ImageViewGIFTableViewCell
            cell.imageHeightLayoutConstraint.constant = 100
            cell.imageWidthLayoutConstraint.constant = 320
            cell.GIFImageView.loadGif(name: "Car_Charging_GIF_2")
            cell.GIFImageView.contentMode = .scaleAspectFit
            cell.GIFImageView.backgroundColor = UIColor.clear
            cell.selectionStyle = .none
            cell.contentView.backgroundColor = UIColor.white
            cell.backgroundColor = UIColor.white
            return cell
        case .StartChargingCellTypeParkingSpaceStatusInfo:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ParkingSpaceStatusInfoCell", for: indexPath) as! ParkingSpaceStatusInfoCell
//            cell.currentLab.text = self.chargingStatusClass?.datas?.current ?? ""
//            cell.voltageLab.text = self.chargingStatusClass?.datas?.voltage ?? ""
//            cell.kwhLabel.text = self.chargingStatusClass?.datas?.kwh ?? "0"
            cell.selectionStyle = .none
            cell.contentView.backgroundColor = UIColor.white
            cell.backgroundColor = UIColor.white
            return cell
        case .StartChargingCellTypeChargingStatusInfo:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UseInfoTableViewCell", for: indexPath) as! UseInfoTableViewCell
            cell.contentView.clipsToBounds = true
            cell.clipsToBounds = true
            cell.useTimeTitleLabel.text = "車位使用累計時間 "
            cell.useFeeTitleLabel.text = "車位使用費率 "
            cell.useCostTitleLabel.text = "車位使用累計費用 "
            cell.useTimeLabel.text = self.carTotalTime
//            cell.useFeeLabel.text = self.chargingStatusClass?.datas?.parkRate ?? ""
//            cell.useCostLabel.text = "\(self.chargingStatusClass?.datas?.parkAmount ?? 0) 元"
            cell.selectionStyle = .none
            cell.contentView.backgroundColor = UIColor.white
            cell.backgroundColor = UIColor.white
            return cell
        case .StartChargingCellTypeChargingInfo:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UseInfoTableViewCell", for: indexPath) as! UseInfoTableViewCell
            cell.useTimeTitleLabel.text = "充電累計時間 "
            cell.useFeeTitleLabel.text = "充電費率 "
            cell.useCostTitleLabel.text = "充電累計費用 "
            cell.useTimeLabel.text = chargingTotalTime
//            cell.useFeeLabel.text = self.chargingStatusClass?.datas?.chargeRate ?? ""
//            cell.useCostLabel.text = "\(self.chargingStatusClass?.datas?.chargeAmount ?? 0) 元"
            cell.selectionStyle = .none
            cell.contentView.backgroundColor = UIColor.white
            cell.backgroundColor = UIColor.white
            return cell
        case .StartChargingCellTypeStopMessage:
            let cell = tableView.dequeueReusableCell(withIdentifier: "StopMessageTableViewCell", for: indexPath) as! StopMessageTableViewCell
            cell.selectionStyle = .none
            cell.contentView.backgroundColor = UIColor.white
            cell.backgroundColor = UIColor.white
            return cell
        }
    }

    func callAPIRequestForCarChargingStatus() {
        print("tranNo = \(String(describing: tranNo))")
        KJLRequest.requestForCarChargingStatus(tranNo: "\(tranNo ?? 0)") { (response) in
            guard let response = response as? [ChargingStatusClass] else {
                    return
            }
            print(response[0].datas)
//            if let carTotalTime = response[0].datas?.parkTotalTime {
//                if (carTotalTime > self.carTimeNum) {
//                    self.carTimeNum = carTotalTime
//                }
//
//                if (carTotalTime > self.carTimeNum) {
//                    self.carTimeNum = carTotalTime
//                }
//            }
//
//            let totalTime = Int(response[0].datas?.totalTime ?? 0)
//            if (totalTime > self.chargingTimeNum) {
//                self.chargingTimeNum = totalTime
//            }
            
//            if (totalTime > self.chargingTimeNum) {
//                self.chargingTimeNum = totalTime
//            }
            
            self.chargingStatusClass = response[0]
            self.updateStartChargingCellTypeChargingStatusInfo()
            self.updateStartChargingCellTypeChargingInfo()
            self.updateStartChargingCellTypeParkingSpaceStatusInfo()
            
//            if let chargeisEnd = response[0].datas?.chargeIsEnd, chargeisEnd == "Y" {
//                self.checkChargingOvertimeStatus()
//            }
        }
    }
    
    func checkChargingOvertimeStatus() {
        //問418是否在充電
        KJLRequest.checkChargingOvertimeStatus(tranNo:"\(tranNo ?? 0)") { (response) in
            guard let response = response as? ReserveDetailClass else {
                return
            }
            guard let data = response.datas else {
                return
            }
            if self.lockCode == "" {
                //沒地鎖
                // 表示有逾時設定
                if data.isOvertime == "Y" {
                    if data.gunIsEnd == "Y" {
                        self.openCarCompleteChargingViewController()
                    }else{
                        self.invalidateTime()
                        if data.bufferFlag == "N" {
                            let carTimeoutFeeBufferViewController = CarTimeoutFeeBufferViewController(nibName: "TimeoutFeeBufferViewController", bundle: nil)
                            carTimeoutFeeBufferViewController.hidesBottomBarWhenPushed = true
                            carTimeoutFeeBufferViewController.tranNo = self.tranNo; self.navigationController?.pushViewController(carTimeoutFeeBufferViewController, animated: true)
                        }else{
                            let carTimeoutFeeCalculateViewController = CarTimeoutFeeCalculateViewController(nibName: "TimeoutFeeCalculateViewController", bundle: nil)
                            carTimeoutFeeCalculateViewController.tranNo = self.tranNo
                            carTimeoutFeeCalculateViewController.hidesBottomBarWhenPushed = true
                         self.navigationController?.pushViewController(carTimeoutFeeCalculateViewController, animated: true)
                        }
                    }
                }else{
                    //充電結束了
                    self.openCarCompleteChargingViewController()
                }
            }else{
                //有地鎖
                if data.gunIsEnd == "N" {
                    if data.chargeIsEnd == "Y" {
                        //充電結束了
                        self.openCarCompleteChargingViewController()
                    }
                }else{
                    self.invalidateTime()
                    let useParkingSpaceViewController = UseParkingSpaceViewController()
                    useParkingSpaceViewController.tranNo = self.tranNo
                    self.navigationController?.pushViewController(useParkingSpaceViewController, animated: true)
                }
            }
        }
    }
    
    func openCarCompleteChargingViewController() {
        self.invalidateTime()
        
        let carCompleteChargingViewController = CarCompleteChargingViewController()
        carCompleteChargingViewController.tranNo = self.tranNo
        self.navigationController?.pushViewController(carCompleteChargingViewController, animated: true)
    }
    
}
//MARK: fetchData
extension CarStartChargingViewController {
    @objc func fetchData() {
        self.callAPIRequestForCarChargingStatus()
    }
    
    func invalidateTime() {
        if (timer10sec != nil) {
            timer10sec?.invalidate()
        }
        
        if timer != nil {
            timer?.invalidate()
        }
    }
    
    @objc private func setupCarTotalTimeLabel() {
        
        let cycle = 60
        var second: Int = carTimeNum
        var minutes: Int = 0
        var hour: Int = 0
        if (second >= cycle) {
            minutes = second / cycle
            second = second % cycle
        }
        
        if (minutes >= cycle) {
            hour = minutes / cycle
            minutes = minutes % cycle
        }
        
        self.carTotalTime =  String(format: "%i:%02i:%02i", hour, minutes, second)
        self.updateStartChargingCellTypeChargingStatusInfo()
        self.carTimeNum += 1
    }
    
    func updateStartChargingCellTypeChargingStatusInfo() {
        let chargingInfo = IndexPath.init(row: StartChargingCellType.StartChargingCellTypeChargingStatusInfo.rawValue, section: 0)
        if let useInfoTableViewCell = self.startChargingTableView.cellForRow(at: chargingInfo) as? UseInfoTableViewCell {
            useInfoTableViewCell.useTimeLabel.text = self.carTotalTime
//            useInfoTableViewCell.useFeeLabel.text = self.chargingStatusClass?.datas?.parkRate ?? ""
//            useInfoTableViewCell.useCostLabel.text = "\(self.chargingStatusClass?.datas?.parkAmount ?? 0) 元"
        }
    }
    
    @objc private func setupChargingTotalTimeLabel() {
        
        let cycle = 60
        var second: Int = chargingTimeNum
        var minutes: Int = 0
        var hour: Int = 0
        if (second >= cycle) {
            minutes = second / cycle
            second = second % cycle
        }
        
        if (minutes >= cycle) {
            hour = minutes / cycle
            minutes = minutes % cycle
        }
        
        self.chargingTotalTime =  String(format: "%i:%02i:%02i", hour, minutes, second)
        self.updateStartChargingCellTypeChargingInfo()
        self.chargingTimeNum += 1
    }
    
    func updateStartChargingCellTypeChargingInfo() {
        let chargingInfo = IndexPath.init(row: StartChargingCellType.StartChargingCellTypeChargingInfo.rawValue, section: 0)
              if let useInfoTableViewCell = self.startChargingTableView.cellForRow(at: chargingInfo) as? UseInfoTableViewCell {
                   useInfoTableViewCell.useTimeLabel.text = chargingTotalTime
//                   useInfoTableViewCell.useFeeLabel.text = self.chargingStatusClass?.datas?.chargeRate ?? ""
//                   useInfoTableViewCell.useCostLabel.text = "\(self.chargingStatusClass?.datas?.chargeAmount ?? 0) 元"
               }
    }
    
    func updateStartChargingCellTypeParkingSpaceStatusInfo() {
        let parkingSpace = IndexPath.init(row: StartChargingCellType.StartChargingCellTypeParkingSpaceStatusInfo.rawValue, section: 0)
        if let cell = self.startChargingTableView.cellForRow(at: parkingSpace) as? ParkingSpaceStatusInfoCell {
//           cell.currentLab.text = self.chargingStatusClass?.datas?.current ?? ""
//           cell.voltageLab.text = self.chargingStatusClass?.datas?.voltage ?? ""
//           cell.kwhLabel.text = self.chargingStatusClass?.datas?.kwh ?? "0"
        }
    }
    @objc func setupCurrentSec() {
        self.setupCarTotalTimeLabel()
        self.setupChargingTotalTimeLabel()
    }
    
    @objc override func backAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
}

class CarStartChargingViewController: KJLNavView {
    var tranNo: Int64?
    var nameStr:String  = ""
    var idStr:String = ""
    var lockCode:String = ""
    var chargingStatusClass:ChargingStatusClass?
    var isHaveLock:Bool = true
    var timer10sec: Timer?
    var timer:Timer?
    var carTotalTime:String = "00:00:00"
    var chargingTotalTime:String = "00:00:00"
    private var carTimeNum: Int = 0
    private var chargingTimeNum: Int = 0
    @IBOutlet weak var startChargingTableView: UITableView!
    @IBOutlet weak var topLayoutConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInitValue()
        self.setupNavView()
        self.setupTableView()
        self.fetchData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.fetchData5Sec()
        NotificationCenter.default.addObserver(self, selector: #selector(fetchData), name: .carChargingStatus, object: nil) 
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(setupCurrentSec), userInfo: nil, repeats: true)
        RunLoop.current.add(timer!, forMode: .commonModes)
        timer?.fire()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: .carChargingStatus, object: nil)
    }
}
