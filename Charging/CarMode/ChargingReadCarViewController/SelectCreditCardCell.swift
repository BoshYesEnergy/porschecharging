//
//  SelectCreditCardCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

protocol SelectCreditCardCellDelegate {
    func unlockButtonAction()
    func selectCreditCardButtonAction()
}

class SelectCreditCardCell: UITableViewCell {
    @IBOutlet weak var cordNumberLabel: UILabel!
    @IBOutlet weak var lockButton: UIButton!
    @IBOutlet weak var expireContent: UILabel!
    
    var delegate: SelectCreditCardCellDelegate? = nil
    var selectCardInfo:ChargerPairingModel.Data.CardList? {
        didSet {
            expireContent.text = "" 
            let number = selectCardInfo?.cardNum ?? "****-****"
            if selectCardInfo!.paymentType!.lowercased().contains("line") {
                self.cordNumberLabel.text = selectCardInfo!.paymentType
            } else if selectCardInfo!.paymentType!.lowercased().contains("easywallet") {
                self.cordNumberLabel.text = selectCardInfo!.paymentType
            } else {
                self.cordNumberLabel.text = number.hiddenCardCarNumber()
            }
            if selectCardInfo?.disable == true || selectCardInfo?.expireSoon == true {
                expireContent.text = selectCardInfo!.warn
            }
        }
    } 
    
    @IBAction func unlockButtonAction(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.unlockButtonAction()
        }
    }
    
    @IBAction func selectCreditCardButtonAction(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.selectCreditCardButtonAction()
        }
    } 
}
