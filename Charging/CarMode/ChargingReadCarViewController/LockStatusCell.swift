//
//  LockStatusCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class LockStatusCell: UITableViewCell {
    @IBOutlet weak var lockStatusLabel: UILabel!
    
    @IBOutlet weak var lockStatusImageView: UIImageView!
    
    var data: ChargerPairingModel? {
        didSet {
            self.setupViews()
        }
    }
    
    func setupViews() {
        if let lockStatus = data?.datas?.lockStatus {
            if lockStatus == "1" {
                self.lockStatusImageView.image = UIImage.init(named: "lock_up")
                self.lockStatusLabel.text = "地鎖已升起"
            }else{
                self.lockStatusImageView.image = UIImage.init(named: "lock_down")
                self.lockStatusLabel.text = "地鎖已降下"
            }
        }
    }
}
