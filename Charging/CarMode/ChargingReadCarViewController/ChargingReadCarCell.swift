//
//  ChargingReadCarCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class ChargingReadCarCell: UITableViewCell {
    @IBOutlet weak var chargingCodeLabel: UILabel!
    @IBOutlet weak var chargingNameLabel: UILabel!
    @IBOutlet weak var unlockCodeLabel: UILabel!
    @IBOutlet weak var chargingImageView: UIImageView!
    
    var qrNo: String?
    var data: ChargerPairingModel? {
        didSet {
            self.setupViews()
        }
    }
    
    var chargingResultClass:ChargingResultClass? {
           didSet {
               self.setupViewsForChargingResultClass()
           }
       }
    
    func setupViewsForChargingResultClass() {
        if let chargingResultClass = self.chargingResultClass {
            let name = chargingResultClass.datas?.socketType ?? ""
            let rename = name.replacingOccurrences(of: "/", with: ":")
            self.chargingImageView.image = UIImage(named: rename)
            
            self.chargingNameLabel.text = chargingResultClass.datas?.stationName
            self.chargingCodeLabel.text = "充電槍代碼 " + (chargingResultClass.datas?.qrNo ?? "")
            self.unlockCodeLabel.isHidden = true
        }
    }
    
    func setupViews() {
        if let data = self.data {
            let name = data.datas?.socketType ?? ""
            let rename = name.replacingOccurrences(of: "/", with: ":")
            self.chargingImageView.image = UIImage(named: rename)
            
            self.chargingNameLabel.text = data.datas?.stationName
            self.chargingCodeLabel.text = "充電槍代碼：" + qrNo!
            
            if data.datas?.lock == false {
                self.unlockCodeLabel.isHidden = true
            }
        }
    }
    
}
