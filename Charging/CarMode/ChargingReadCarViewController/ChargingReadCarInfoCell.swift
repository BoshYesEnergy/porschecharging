//
//  ChargingReadCarInfoCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class ChargingReadCarInfoCell: UITableViewCell {
    
    @IBOutlet weak var gunHeadLabel: UILabel!
    @IBOutlet weak var usageRateLabel: UILabel!
    @IBOutlet weak var chargingRateLabel: UILabel!
    @IBOutlet weak var usageRateHeightLayoutConstraint: NSLayoutConstraint!
    
    var data: ChargerPairingModel? {
        didSet {
            self.setupViews()
        }
    }
    
    func setupViews() {
        if let data = self.data {
            self.gunHeadLabel.text = data.datas?.socketType
            self.chargingRateLabel.text = data.datas?.chargingFee
            self.usageRateLabel.text = data.datas?.parkingFee
        }
    }
    
}
