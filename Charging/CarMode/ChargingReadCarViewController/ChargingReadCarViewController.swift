//
//  ChargingReadCarViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

enum ChargingReadCellType:Int {
    case ChargingInfo = 0
    case ChargingReadCarInfo = 1
    case LockStatusType = 2
    case SelectCreditCard = 3
}

extension ChargingReadCarViewController {
    func setupInitValue() {
        if #available(iOS 13.0, *) {
            //self.chargingTableView.automaticallyAdjustsScrollIndicatorInsets = false
        } else if #available(iOS 11.0, *) {
            self.chargingTableView.contentInsetAdjustmentBehavior = .never
        }else{
            self.automaticallyAdjustsScrollViewInsets = false
        }
    }
    
    func setupNavView() {
        createNavViewForRightBtn(rightName: "", rightImage: "", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: UIColor.black)
        titleLab?.text = "準備充電(1/5)"
        
    }
    
    func checkData() {
        self.creditCards = data?.datas?.cardList ?? []
        for temp in self.creditCards {
            if temp.default == true {
                self.selectCardNo = temp.cardNo
                self.selectCardInfo = temp
                break
            }
        }
        
        if self.creditCards.count <= 0 {
            ShowMessageView.showMessage(title: nil, message: "請先設定付款方式")
        }
    }
    
    func setupTableView() {
        self.topLayoutConstraint.constant = KJLCommon.isX() == true ? 88 : 64
        self.chargingTableView.dataSource = self
        self.chargingTableView.backgroundColor = UIColor.clear
        self.chargingTableView.separatorStyle = .none
        
        self.chargingTableView.register(UINib(nibName: "ChargingReadCarCell", bundle: nil), forCellReuseIdentifier: "ChargingReadCarCell")
        self.chargingTableView.register(UINib(nibName: "ChargingReadCarInfoCell", bundle: nil), forCellReuseIdentifier: "ChargingReadCarInfoCell")
        self.chargingTableView.register(UINib(nibName: "LockStatusCell", bundle: nil), forCellReuseIdentifier: "LockStatusCell")
        self.chargingTableView.register(UINib(nibName: "SelectCreditCardCell", bundle: nil), forCellReuseIdentifier: "SelectCreditCardCell")
    }
    
}

//MARK: UITableViewDataSource
extension ChargingReadCarViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ChargingReadCellType.SelectCreditCard.rawValue + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellType:ChargingReadCellType = ChargingReadCellType(rawValue: indexPath.item)!
        switch cellType {
        case .ChargingInfo:
            let cell:ChargingReadCarCell = tableView.dequeueReusableCell(withIdentifier: "ChargingReadCarCell", for: indexPath) as! ChargingReadCarCell
            cell.selectionStyle = .none
            cell.qrNo = self.qrNo ?? ""
            cell.data = self.data
            cell.unlockCodeLabel.text = "地鎖代碼 " + self.qrNo!
            return cell
        case .ChargingReadCarInfo:
            let cell:ChargingReadCarInfoCell = tableView.dequeueReusableCell(withIdentifier: "ChargingReadCarInfoCell", for: indexPath) as! ChargingReadCarInfoCell
            cell.selectionStyle = .none
            cell.data = self.data
            return cell
        case .LockStatusType:
            let cell:LockStatusCell = tableView.dequeueReusableCell(withIdentifier: "LockStatusCell", for: indexPath) as! LockStatusCell
            cell.selectionStyle = .none
            cell.data = self.data
            cell.contentView.isHidden = (data?.datas?.lock == false)
            return cell
        case .SelectCreditCard:
            let cell:SelectCreditCardCell = tableView.dequeueReusableCell(withIdentifier: "SelectCreditCardCell", for: indexPath) as!SelectCreditCardCell
            cell.delegate = self
            cell.selectCardInfo = self.selectCardInfo
            let title = (data?.datas?.lock == true) ? "降下地鎖":"開始充電"
            cell.lockButton.setTitle(title, for: .normal)
            cell.selectionStyle = .none
            return cell
        }
    }
    
}

//MARK: SelectCreditCardCellDelegate
extension ChargingReadCarViewController:SelectCreditCardCellDelegate {
    func unlockButtonAction() {
        //let lockUseViewController = LockUseViewController()
        // 呼叫api
        
        var cardNo = ""
        if selectCardNo != nil {
            cardNo = "\(selectCardNo!)"
        }
        if cardNo.count <= 0 {
            ShowMessageView.showMessage(title: nil, message: "請先設定付款方式")
            return
        }
        
        for cards in creditCards {
            if cards.cardNo == selectCardNo {
                if cards.disable == true { 
                    return
                }
            }
        }
        
        if self.data?.datas?.lock == true {
            KJLRequest.requestForParkingLockDown(qrNo: qrNo ?? "") {[weak self] (response) in
                guard let parkingLockData = response as? ParkingLockDownModel else {
                    // 用不到,之後刪除 api CarLockStatus
//                    self?.checkCarLockStatus()
                    return
                }
                
//                if self.data?.datas?.lock == true {
                    let lockUseViewController = LockUseViewController()
                    lockUseViewController.tranNo = Int64(parkingLockData.datas?.tranNo ?? "0")
                    lockUseViewController.idStr = self?.qrNo ?? ""
                    lockUseViewController.nameStr = self?.data?.datas?.stationName ?? ""
                    self?.navigationController?.pushViewController(lockUseViewController, animated: true)
//                } else {
//                    let carStartChargingViewController = CarStartChargingViewController()
//                    carStartChargingViewController.tranNo = Int64(parkingLockData.datas?.tranNo ?? "0")
//                    carStartChargingViewController.idStr = self.qrNo ?? ""
//                    carStartChargingViewController.nameStr = self.data?.datas?.stationName ?? ""
//                    self.navigationController?.pushViewController(carStartChargingViewController, animated: true)
//                }
            }
        } else {
            // 開始充電
            KJLRequest.requestForBeginCharging(qrNo: qrNo ?? "", cardNo: cardNo) { [weak self] (response) in
                guard let response = response as? BeginChargingModel else { return }
                /// 0000 成功 -- CA01 授權失敗 -- CC01 充電樁回覆失敗
                if response.resCode == "CA01", response.resCode == "CC01" {
                    self?.navigationController?.popViewController(animated: true)
                    return
                }
                let carStartChargingViewController = CarStartChargingViewController()
                carStartChargingViewController.tranNo = Int64(response.datas?.tranNo ?? 0)
                carStartChargingViewController.idStr = self?.qrNo ?? ""
                carStartChargingViewController.nameStr = self?.data?.datas?.stationName ?? ""
                self?.navigationController?.pushViewController(carStartChargingViewController, animated: true)
            }
        }
    }
    
    func  checkCarLockStatus() {
        KJLRequest.checkCarLockStatus(qrNo: self.qrNo ?? "") { (response) in
            guard let response = response as? ParkingLockDownModel else { 
                return
            }
            
            if self.data?.datas?.lock == true {
                if let lockStatus = response.datas?.lockStatus ,lockStatus == 0 {
                    // 前往下一頁
                    let lockUseViewController = LockUseViewController()
                    lockUseViewController.tranNo = Int64(response.datas?.tranNo ?? "0")
                    lockUseViewController.idStr = self.qrNo ?? ""
                    lockUseViewController.nameStr = self.data?.datas?.stationName ?? ""
                    self.navigationController?.pushViewController(lockUseViewController, animated: true)
                }
            }else{
                let carStartChargingViewController = CarStartChargingViewController()
                carStartChargingViewController.tranNo = Int64(response.datas?.tranNo ?? "0")
                carStartChargingViewController.idStr = self.qrNo ?? ""
                carStartChargingViewController.nameStr = self.data?.datas?.stationName ?? ""
                self.navigationController?.pushViewController(carStartChargingViewController, animated: true)
            }
        }
        
    }
    
    // 選擇信用卡
    func selectCreditCardButtonAction() {
        print("selectCreditCardButtonAction")
        guard creditCards.count > 0 else {
            ShowMessageView.showMessage(title: nil, message: "請先建立信用卡")
            return
        }
        
        let control = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        var title = ""
        for temp in creditCards {
            print("Card Car Type : \(temp.cardType ?? "")")
            if temp.paymentType!.lowercased().contains("line") {
                title = "\(temp.paymentType!): " + (temp.cardName ?? "")
            } else if temp.paymentType!.lowercased().contains("easywallet") {
                title = "\(temp.paymentType!): " + (temp.cardName ?? "")
            } else {
                switch temp.cardType?.lowercased() {
                case "master", "jcb", "visa": title = "\(temp.cardType!): " + (temp.cardName ?? "")
                default: title = "\(temp.cardType ?? ""): " + (temp.cardName ?? "")
                }
            } 
            let action = UIAlertAction(title: title, style: .default, handler: { (alertAction) in
                self.selectCardInfo = temp
                self.selectCardNo = temp.cardNo
                self.chargingTableView.reloadData()
            })
            control.addAction(action)
        }
        
        let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        control.addAction(cancel)
        
        if control.actions.count > 1 {
            present(control, animated: true, completion: nil)
        }
    }
    
}

//MARK: life cycle
class ChargingReadCarViewController: KJLNavView {
    var data: ChargerPairingModel?
    var qrNo:String?
    
    @IBOutlet weak var chargingTableView: UITableView!
    @IBOutlet weak var topLayoutConstraint: NSLayoutConstraint!
    
    var creditCards: [ChargerPairingModel.Data.CardList] = []
    var selectCardNo: Int?
    var selectCardInfo:ChargerPairingModel.Data.CardList?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInitValue()
        self.setupNavView()
        self.checkData()
        self.setupTableView()
    }
}
