//
//  CarTimeoutFeeCalculateViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class CarTimeoutFeeCalculateViewController: TimeoutFeeCalculateViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.chargingImageView.image = UIImage(named: "Car_Unplug")
    }
    
    @objc override func countDown() {
        if (self.sec <= 0) {
            self.timeLabel.text = "00:00:00"
            self.invalidateTime()
            let carTimeoutFeeBufferViewController = CarTimeoutFeeBufferViewController(nibName: "TimeoutFeeBufferViewController", bundle: nil)
            carTimeoutFeeBufferViewController.reserveDetailClass = reserveDetailClass;
            carTimeoutFeeBufferViewController.tranNo = self.tranNo; self.navigationController?.pushViewController(carTimeoutFeeBufferViewController, animated: true)
        } else {
            self.sec = self.sec - 1
            let hh = String(format: "%02d", self.sec / 3600)
            let mm = String(format: "%02d", (self.sec / 60) % 60)
            let ss = String(format: "%02d", self.sec % 60)
            self.timeLabel.text = "\(hh):\(mm):\(ss)"
        }
    }
    
    override func openCompleteChargingViewController() {
        self.invalidateTime()
        let carCompleteChargingViewController = CarCompleteChargingViewController()
        carCompleteChargingViewController.tranNo = self.tranNo
        self.navigationController?.pushViewController(carCompleteChargingViewController, animated: true)
    }
}
