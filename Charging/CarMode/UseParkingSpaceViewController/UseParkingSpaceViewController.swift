//
//  UseParkingSpaceViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

enum UseParkingSpaceCellType:Int {
    case UseParkingSpaceCellTypeStatusImageView = 0
    case UseParkingSpaceCellTypeInfo = 1
    case UseParkingSpaceCellTypeMessage = 2
    case UseParkingSpaceCellTypeGIF = 3
    case UseParkingSpaceCellTypeUseInfo = 4
    case UseParkingSpaceCellTypeUplock = 5
    case UseParkingSpaceCellCustomPhone = 6
}

extension UseParkingSpaceViewController {
    func setupInitValue() {
        if #available(iOS 13.0, *) {
            //self.chargingTableView.automaticallyAdjustsScrollIndicatorInsets = false
        } else if #available(iOS 11.0, *) {
            self.useParkingTableView.contentInsetAdjustmentBehavior = .never
        }else{
            self.automaticallyAdjustsScrollViewInsets = false
        }
    }
    
    func fetchData10Sec() {
        if timer10sec == nil {
            timer10sec = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(fetchData), userInfo: nil, repeats: true)
            timer10sec?.fire()
            RunLoop.current.add(timer10sec!, forMode: .commonModes)
        }
    }
    
    func setupNavView() {
        createNavViewForRightBtn(rightName: "", rightImage: "", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: UIColor.black)
        titleLab?.text = "車位使用中(4/5)"
    }
    
    func setupTableView() {
        self.topLayoutConstraint.constant = KJLCommon.isX() == true ? 88 : 64
        self.useParkingTableView.dataSource = self
        self.useParkingTableView.backgroundColor = UIColor.clear
        self.useParkingTableView.separatorStyle = .none
        
        self.useParkingTableView.register(UINib(nibName: "StatusImageViewCell", bundle: nil), forCellReuseIdentifier: "StatusImageViewCell")
        self.useParkingTableView.register(UINib(nibName: "LockInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "LockInfoTableViewCell")
        self.useParkingTableView.register(UINib(nibName: "UpLockMessageCell", bundle: nil), forCellReuseIdentifier: "UpLockMessageCell")
        self.useParkingTableView.register(UINib(nibName: "ImageViewGIFTableViewCell", bundle: nil), forCellReuseIdentifier: "ImageViewGIFTableViewCell")
        self.useParkingTableView.register(UINib(nibName: "UseInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "UseInfoTableViewCell")
        self.useParkingTableView.register(UINib(nibName: "UseParkingSpaceMessageCell", bundle: nil), forCellReuseIdentifier: "UseParkingSpaceMessageCell")
        self.useParkingTableView.register(UINib(nibName: "UpLockTableViewCell", bundle: nil), forCellReuseIdentifier: "UpLockTableViewCell")
    }
}

//MARK: UITableViewDataSource
extension UseParkingSpaceViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return UseParkingSpaceCellType.UseParkingSpaceCellCustomPhone.rawValue + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellType:UseParkingSpaceCellType = UseParkingSpaceCellType(rawValue: indexPath.item)!
        switch cellType {
        case .UseParkingSpaceCellTypeStatusImageView:
            let cell = tableView.dequeueReusableCell(withIdentifier: "StatusImageViewCell", for: indexPath) as! StatusImageViewCell
            return cell
        case .UseParkingSpaceCellTypeInfo:
            let cell = tableView.dequeueReusableCell(withIdentifier: "LockInfoTableViewCell", for: indexPath) as! LockInfoTableViewCell
            cell.siteNameLabel.text = "充電已完成！"
            cell.charginCode.isHidden = true
            cell.lockCode.text = ""
            cell.lockCode.text = (reserveDetailClass?.datas?.finishCode_ch ?? "") 
            return cell
        case .UseParkingSpaceCellTypeMessage:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UseParkingSpaceMessageCell", for: indexPath) as! UseParkingSpaceMessageCell
            cell.messageLabel.text = "充電完成！請於確認離開車位後\n按下升起地鎖按鈕，結束車位使用計算。"
            return cell
        case .UseParkingSpaceCellTypeGIF:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ImageViewGIFTableViewCell", for: indexPath) as! ImageViewGIFTableViewCell
            cell.GIFImageView.animationRepeatCount = 1
            cell.GIFImageView.loadGif(name: "LockUpHour")
            return cell
        case .UseParkingSpaceCellTypeUseInfo:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UseInfoTableViewCell", for: indexPath) as! UseInfoTableViewCell
            cell.useTimeTitleLabel.text = "車位使用累計時間 "
            cell.useFeeTitleLabel.text = "車位使用費率 "
            cell.useCostTitleLabel.text = "車位使用累計費用 "
            cell.useTimeLabel.text = self.carTotalTime
//            cell.useFeeLabel.text = self.chargingStatusClass?.datas?.parkRate ?? ""
//            cell.useCostLabel.text = "\(self.chargingStatusClass?.datas?.parkAmount ?? 0) 元"
            return cell
        case .UseParkingSpaceCellTypeUplock:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UpLockTableViewCell", for: indexPath) as! UpLockTableViewCell
            cell.delegate = self
            return cell
        case .UseParkingSpaceCellCustomPhone:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UpLockMessageCell", for: indexPath) as! UpLockMessageCell
            cell.messageLabel.text = "若地鎖無法升起，請聯繫客服電話\n02-5592-0006"
            return cell
        }
    }
    
    func callAPIRequestForCarChargingStatus() {
        print("tranNo = \(String(describing: tranNo))")
        KJLRequest.requestForCarChargingStatus(tranNo: "\(tranNo ?? 0)") { (response) in
            guard let response = response as? ChargingStatusClass else {
                    return
            }
//            if let carTotalTime = response.datas?.parkTotalTime {
//                if (carTotalTime > self.carTimeNum) {
//                    self.carTimeNum = carTotalTime
//                }
//            }
            
            self.setupCarTotalTimeLabel()
            self.chargingStatusClass = response
        }
    }
    
    func checkChargingOvertimeStatus() {
        KJLRequest.checkChargingOvertimeStatus(tranNo:"\(tranNo ?? 0)") { (response) in
            guard let response = response as? ReserveDetailClass else {
                return
            }
            self.reserveDetailClass = response
            self.useParkingTableView.reloadRows(at: [IndexPath.init(row: UseParkingSpaceCellType.UseParkingSpaceCellTypeInfo.rawValue, section: 0)], with: .none)
        }
    }
    
    @objc private func setupCarTotalTimeLabel() {
        
        let cycle = 60
        var second: Int = carTimeNum
        var minutes: Int = 0
        var hour: Int = 0
        if (second >= cycle) {
            minutes = second / cycle
            second = second % cycle
        }
        
        if (minutes >= cycle) {
            hour = minutes / cycle
            minutes = minutes % cycle
        }
        
        self.carTotalTime =  String(format: "%i:%02i:%02i", hour, minutes, second)
        self.useParkingTableView.reloadRows(at: [IndexPath.init(row: UseParkingSpaceCellType.UseParkingSpaceCellTypeUseInfo.rawValue, section: 0)], with: .none)
        self.carTimeNum += 1
    }
    
    @objc func fetchData() {
        self.checkChargingOvertimeStatus()
    }
    
    func invalidateTime() {
           if (timer10sec != nil) {
               timer10sec?.invalidate()
           }
           
           if timer != nil {
               timer?.invalidate()
           }
       }
}

extension UseParkingSpaceViewController:UpLockTableViewCellDelegate {
    func upLockButtonAction() {
        KJLRequest.checkChargingParkingLockUp(tranNo: "\(self.tranNo ?? 0)") { (response) in
            guard let _ = response as? ParkingLockDownModel else {
                return
            }
            
            self.invalidateTime()
            let carCompleteChargingViewController = CarCompleteChargingViewController()
            carCompleteChargingViewController.tranNo = self.tranNo
            self.navigationController?.pushViewController(carCompleteChargingViewController, animated: true)
        }
    }
    
    @objc override func backAction() {
        self.dismiss(animated: true, completion: nil)
    }
}

class UseParkingSpaceViewController: KJLNavView {
    var tranNo: Int64?
    var chargingStatusClass:ChargingStatusClass?
    var reserveDetailClass:ReserveDetailClass?
    var carTotalTime:String = "00:00:00"
    var timer10sec: Timer?
    var timer:Timer?
    private var carTimeNum: Int = 0
    @IBOutlet weak var useParkingTableView: UITableView!
    @IBOutlet weak var topLayoutConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInitValue()
        self.setupNavView()
        self.setupTableView()
        self.callAPIRequestForCarChargingStatus()
        self.setupCarTotalTimeLabel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.fetchData10Sec()
        
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(setupCarTotalTimeLabel), userInfo: nil, repeats: true)
        RunLoop.current.add(timer!, forMode: .commonModes)
        timer?.fire()
    }

}

