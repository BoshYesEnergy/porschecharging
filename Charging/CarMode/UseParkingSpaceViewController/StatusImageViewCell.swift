//
//  StatusImageViewCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class StatusImageViewCell: UITableViewCell {
    
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var statusLab: UILabel!
    @IBOutlet weak var transactionLab: UILabel!
    
    var chargingResultClass: ChargingResultClass? {
        didSet {
            guard let status = chargingResultClass?.datas else { return }
            if status.paymentError != "" {
                iconImage.image = #imageLiteral(resourceName: "newFail")
                statusLab.textColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
                statusLab.text = status.paymentError ?? ""
                transactionLab.text = "本次交易付款失敗"
            } else {
                iconImage.image = #imageLiteral(resourceName: "Payment_ok")
                statusLab.textColor = #colorLiteral(red: 0.1254901961, green: 0.7960784314, blue: 0.8196078431, alpha: 1)
                statusLab.text = status.finishCode_ch ?? ""
                transactionLab.text = "充電流程已結束"
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
