//
//  CarCompleteChargingTableViewCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class CarCompleteChargingTableViewCell: UITableViewCell {

    @IBOutlet weak var startTimeTitleLabel: UILabel!
    @IBOutlet weak var startTimeLabel: UILabel!
    
    @IBOutlet weak var endTimeTitleLabel: UILabel!
    @IBOutlet weak var endTimeLabel: UILabel!
    
    @IBOutlet weak var costTimeTitleLabel: UILabel!
    @IBOutlet weak var costTimeLabel: UILabel!
    
    @IBOutlet weak var feeTitleLabel: UILabel!
    @IBOutlet weak var feeLabel: UILabel!
    
    @IBOutlet weak var costFeeTitleLabel: UILabel!
    @IBOutlet weak var costFeeLabel: UILabel!
    
    var isCarCell = true
    var chargingResultClass:ChargingResultClass? {
        didSet {
            self.setupViews()
        }
    }
    
    func setupViews() {
        if isCarCell == true{
            self.startTimeTitleLabel.text = "車位使用開始時間"
            self.startTimeLabel.text = chargingResultClass?.datas?.parkstartTime ?? ""
            self.endTimeTitleLabel.text = "車位使用結束時間"
            self.endTimeLabel.text = chargingResultClass?.datas?.parkendTime ?? ""
            self.costTimeTitleLabel.text = "車位使用累計時間"
            self.costTimeLabel.text = self.totalTime(totalTime: chargingResultClass?.datas?.parktotalTime ?? 0)
            self.feeTitleLabel.text = "車位使用費率"
            self.feeLabel.text = chargingResultClass?.datas?.parkRate ?? ""
            self.costFeeTitleLabel.text = "車位使用累計費用"
            self.costFeeLabel.text = "\(chargingResultClass?.datas?.parkamount ?? 0) 元"
        }else{
            self.startTimeTitleLabel.text = "充電開始時間"
            self.startTimeLabel.text = chargingResultClass?.datas?.chargestartTime ?? ""
            self.endTimeTitleLabel.text = "充電結束時間"
            self.endTimeLabel.text = chargingResultClass?.datas?.chargeendTime ?? ""
            self.costTimeTitleLabel.text = "充電累計時間"
            self.costTimeLabel.text = self.totalTime(totalTime: chargingResultClass?.datas?.chargetotalTime ?? 0)
            self.feeTitleLabel.text = "充電費率"
            self.feeLabel.text = chargingResultClass?.datas?.chargeRate ?? ""
            self.costFeeTitleLabel.text = "充電累計費用"
            self.costFeeLabel.text = "\(chargingResultClass?.datas?.chargeamount ?? 0) 元"
        }
    }
    
    func totalTime(totalTime:Int) -> String {
    //總時間給秒數, 需要自己轉成時間格式
        if (totalTime == 0) {
            return "00:00:00"
        } else {
            let sec = totalTime % 60
            var min = totalTime / 60
            let hour = min / 60
            min = min % 60
            let secString = (sec < 10) ? "0\(sec)" : "\(sec)"
            let minString = (min < 10) ? "0\(min)" : "\(min)"
            let hourString = (hour < 10) ? "0\(hour)" : "\(hour)"
            return "\(hourString):\(minString):\(secString)"
        }
        return "00:00:00"
    }
}
