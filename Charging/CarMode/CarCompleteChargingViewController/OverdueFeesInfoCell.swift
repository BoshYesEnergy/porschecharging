//
//  OverdueFeesInfoCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class OverdueFeesInfoCell: UITableViewCell {
    @IBOutlet weak var extendStartLab: UILabel!
    @IBOutlet weak var extendEndLab: UILabel!
    @IBOutlet weak var extendAmountLab: UILabel!
    @IBOutlet weak var extendTimeLab: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.clipsToBounds = true
    }

    func setupViews() {
    //逾時開始時間
        self.extendStartLab.text = self.chargingResultClass?.datas?.overtimeStart ?? "00:00:00"
        //逾時結束時間
        self.extendEndLab.text = self.chargingResultClass?.datas?.overtimeEnd ?? "00:00:00"
                
        //逾時累計時間 給秒數要自己轉
        self.extendTimeLab.text = "00:00:00"
             if let overtimes = self.chargingResultClass?.datas?.overtimes {
                   if (overtimes == 0) {
                        self.extendAmountLab.text = "00:00:00"
                    } else {
                        let sec = overtimes % 60
                        var min = overtimes / 60
                        let hour = min / 60
                        min = min % 60
                        let secString = (sec < 10) ? "0\(sec)" : "\(sec)"
                        let minString = (min < 10) ? "0\(min)" : "\(min)"
                        let hourString = (hour < 10) ? "0\(hour)" : "\(hour)"
                        self.extendTimeLab.text = "\(hourString):\(minString):\(secString)"
                    }
                }
                
                //逾時費用
                self.extendAmountLab.text = "$ " + (self.chargingResultClass?.datas?.overtimeAmt ?? "0")
    }
    
    var chargingResultClass:ChargingResultClass? {
        didSet {
            self.setupViews()
        }
    }
    
}
