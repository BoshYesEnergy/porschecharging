//
//  CarCompleteChargingViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

enum CarCompleteChargingCellType:Int {
    case CarCompleteChargingCellTypeStatusImageView         = 0
    case CarCompleteChargingCellTypeStatusInfo              = 1
    case CarCompleteChargingCellTypeStationInfo             = 2
    case CarCompleteChargingRePayCell                       = 3
    case CarCompleteChargingCellTypeParkingSpaceInfo        = 4
    case CarCompleteChargingCellTypeCharginUseInfo          = 5
    case CarCompleteChargingCellTypeCharginOverdueFeesInfo  = 6
    case CarCompleteChargingCellTypeCharginCompleteInfo     = 7
    case CarCompleteChargingCellTypePayInfo                 = 8
    case CarCompleteChargingCellCustomPhone                 = 9
}


//MARK: life cycle
class CarCompleteChargingViewController: KJLNavView {
    
    @IBOutlet weak var useParkingTableView: UITableView!
    @IBOutlet weak var topLayoutConstraint: NSLayoutConstraint!
    
    var showRePay = false
    var tranNo: Int64?
    var qrNo = ""
    var chargingResultClass:ChargingResultClass?
    // requestForCarChargingResult api 金流增加3秒延遲判斷，預設true
    var delay = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupInitValue()
        self.setupNavView()
        self.setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(presentRePayCreditCard), name: .presentRePayCreditCard, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(callAPIRequestForCarChargingComplete), name: .reloadCarCompleteCharging, object: nil)
        self.callAPIRequestForCarChargingComplete()
    }
}


//MARK:- Setting View
extension CarCompleteChargingViewController {
    func setupInitValue() {
        if #available(iOS 13.0, *) {
            //self.chargingTableView.automaticallyAdjustsScrollIndicatorInsets = false
        } else if #available(iOS 11.0, *) {
            self.useParkingTableView.contentInsetAdjustmentBehavior = .never
        }else{
            self.automaticallyAdjustsScrollViewInsets = false
        }
    }
    
    func setupNavView() {
        createNavViewForRightBtn(rightName: "", rightImage: "", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: UIColor.black)
        titleLab?.text = "充電結果(5/5)"
    }
    
    func setupTableView() {
        self.topLayoutConstraint.constant = KJLCommon.isX() == true ? 88 : 64
        self.useParkingTableView.dataSource = self
        self.useParkingTableView.delegate = self
        self.useParkingTableView.backgroundColor = UIColor.clear
        self.useParkingTableView.separatorStyle = .none
         
        self.useParkingTableView.register(UINib(nibName: "StatusImageViewCell", bundle: nil), forCellReuseIdentifier: "StatusImageViewCell")
        self.useParkingTableView.register(UINib(nibName: "CompleteStatusInfoCell", bundle: nil), forCellReuseIdentifier: "CompleteStatusInfoCell")
        self.useParkingTableView.register(UINib(nibName: "ChargingReadCarCell", bundle: nil), forCellReuseIdentifier: "ChargingReadCarCell")
        self.useParkingTableView.register(UINib(nibName: "CarCompleteChargingRePayCell", bundle: nil), forCellReuseIdentifier: "CarCompleteChargingRePayCell")
        self.useParkingTableView.register(UINib(nibName: "CarCompleteChargingTableViewCell", bundle: nil), forCellReuseIdentifier: "CarCompleteChargingTableViewCell")
        self.useParkingTableView.register(UINib(nibName: "UseInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "UseInfoTableViewCell")
        self.useParkingTableView.register(UINib(nibName: "UpLockTableViewCell", bundle: nil), forCellReuseIdentifier: "UpLockTableViewCell")
        self.useParkingTableView.register(UINib(nibName: "CompletePayInfoCell", bundle: nil), forCellReuseIdentifier: "CompletePayInfoCell")
        self.useParkingTableView.register(UINib(nibName: "UpLockMessageCell", bundle: nil), forCellReuseIdentifier: "UpLockMessageCell")
        self.useParkingTableView.register(UINib(nibName: "OverdueFeesInfoCell", bundle: nil), forCellReuseIdentifier: "OverdueFeesInfoCell")
    }
}


//MARK: UITableViewDataSource
extension CarCompleteChargingViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CarCompleteChargingCellType.CarCompleteChargingCellCustomPhone.rawValue + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellType:CarCompleteChargingCellType = CarCompleteChargingCellType(rawValue: indexPath.item)!
        switch cellType {
        case .CarCompleteChargingCellTypeStatusImageView:
            let cell = tableView.dequeueReusableCell(withIdentifier: "StatusImageViewCell", for: indexPath) as! StatusImageViewCell
            cell.selectionStyle = .none
            cell.chargingResultClass = self.chargingResultClass
            return cell
        case .CarCompleteChargingCellTypeStatusInfo:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CompleteStatusInfoCell", for: indexPath) as! CompleteStatusInfoCell
            cell.chargingResultClass = self.chargingResultClass
            cell.selectionStyle = .none
            return cell
        case .CarCompleteChargingCellTypeStationInfo:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChargingReadCarCell", for: indexPath) as! ChargingReadCarCell
            cell.chargingResultClass = self.chargingResultClass
            cell.selectionStyle = .none
            return cell
        case .CarCompleteChargingRePayCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CarCompleteChargingRePayCell", for: indexPath) as! CarCompleteChargingRePayCell
            cell.selectionStyle = .none
            return cell
        case .CarCompleteChargingCellTypeParkingSpaceInfo:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CarCompleteChargingTableViewCell", for: indexPath) as! CarCompleteChargingTableViewCell
            cell.isCarCell = true
            cell.chargingResultClass = self.chargingResultClass
            cell.selectionStyle = .none
            cell.contentView.clipsToBounds = true
            return cell
        case .CarCompleteChargingCellTypeCharginUseInfo:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CarCompleteChargingTableViewCell", for: indexPath) as! CarCompleteChargingTableViewCell
            cell.isCarCell = false
            cell.chargingResultClass = self.chargingResultClass
            cell.selectionStyle = .none
            return cell
        case .CarCompleteChargingCellTypeCharginOverdueFeesInfo:
            let cell = tableView.dequeueReusableCell(withIdentifier: "OverdueFeesInfoCell", for: indexPath) as! OverdueFeesInfoCell
            cell.chargingResultClass = self.chargingResultClass
            cell.selectionStyle = .none
            return cell
        case .CarCompleteChargingCellTypeCharginCompleteInfo:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CompletePayInfoCell", for: indexPath) as! CompletePayInfoCell
            cell.chargingResultClass = self.chargingResultClass
            cell.selectionStyle = .none
            return cell
        case .CarCompleteChargingCellTypePayInfo:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UpLockTableViewCell", for: indexPath) as! UpLockTableViewCell
            cell.lockButton.setTitle("確認", for: .normal)
            cell.delegate = self
            cell.selectionStyle = .none
            return cell
        case .CarCompleteChargingCellCustomPhone:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UpLockMessageCell", for: indexPath) as! UpLockMessageCell
            cell.messageLabel.text = "若地鎖無法升起，請聯繫客服電話\n02-5592-0006"
            cell.selectionStyle = .none
            return cell
        }
    }
}


//MARK: UpLockTableViewCellDelegate
extension CarCompleteChargingViewController :UpLockTableViewCellDelegate ,UITableViewDelegate {
    func upLockButtonAction() {
        self.dismiss(animated: true) { } 
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellType:CarCompleteChargingCellType = CarCompleteChargingCellType(rawValue: indexPath.item)!
        
        let overtimeFlag = self.chargingResultClass?.datas?.overtimeFlag ?? "N"
        if cellType == .CarCompleteChargingCellTypeCharginOverdueFeesInfo,(overtimeFlag == "N") {
            return 0
        }
        if cellType == .CarCompleteChargingCellTypeParkingSpaceInfo ,self.chargingResultClass?.datas?.lock == false{
            return 0
        }
        if cellType == .CarCompleteChargingCellTypeStatusInfo {
            return 0
        }
        if cellType == .CarCompleteChargingRePayCell {
            let status = chargingResultClass?.datas?.paymentStatus ?? ""
            if !status.contains("失敗") {
                return 0
            }
        }
        return UITableViewAutomaticDimension
    }
}


//MARK:- Action
extension CarCompleteChargingViewController  {
    @objc func presentRePayCreditCard() {
        print("RePayCreditCardViewController")
        let vc = RePayCreditCardViewController()
        vc.qrNo = qrNo
        vc.tranNo = tranNo
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        present(vc, animated: true, completion: nil)
    }
    
    @objc override func backAction() {
        self.dismiss(animated: true) { }
    }
}


//MARK:- Call Api
extension CarCompleteChargingViewController {
    @objc func callAPIRequestForCarChargingComplete() {
        KJLRequest.requestForCarChargingResult(tranNo: "\(tranNo ?? 0)", delay: delay) { (response) in
            guard let response = response as? ChargingResultClass
                else { return }
            self.chargingResultClass = response
            self.qrNo = self.chargingResultClass?.datas?.qrNo ?? ""
            self.useParkingTableView.reloadData()
            
            UIView.animate(withDuration: 0.3) {
                self.useParkingTableView.alpha = 1
            }
        }
    }
}
