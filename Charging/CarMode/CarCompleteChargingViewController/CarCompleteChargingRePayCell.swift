//
//  CarCompleteChargingRePayCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class CarCompleteChargingRePayCell: UITableViewCell {

    @IBOutlet weak var payBtn: UIButton! 
    
    override func awakeFromNib() {
        super.awakeFromNib()
        settingView()
    }
 
    private func settingView() {
        payBtn.layer.cornerRadius = 10
    }
    
    @IBAction func gotoAction(_ sender: UIButton) {
        NotificationCenter.default.post(name: .presentRePayCreditCard, object: nil)
    }
}
