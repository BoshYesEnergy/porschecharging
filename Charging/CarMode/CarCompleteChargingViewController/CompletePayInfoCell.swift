//
//  CompletePayInfoCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class CompletePayInfoCell: UITableViewCell {

    @IBOutlet weak var castCompleteLabel: UILabel!
    @IBOutlet weak var tradingResult: UILabel!
    @IBOutlet weak var tradingNumber: UILabel!

    var chargingResultClass:ChargingResultClass? {
        didSet {
            self.setupViews()
        }
    }
    
    func setupViews() {
        self.castCompleteLabel.text = "$ \(chargingResultClass?.datas?.paymentamount ?? 0)"
        self.tradingResult.text = chargingResultClass?.datas?.chargeStatus ?? ""
        self.tradingNumber.text = chargingResultClass?.datas?.transactionNo ?? ""
    }
}
