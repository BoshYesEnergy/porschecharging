//
//  CompleteStatusInfoCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class CompleteStatusInfoCell: UITableViewCell {

    @IBOutlet weak var endLabel: UILabel!
    
    var chargingResultClass:ChargingResultClass? {
        didSet {
            self.setupViews()
        }
    }
 
    func setupViews() {
        endLabel.text = ""
//        if let chargingResultClass = self.chargingResultClass {
//            self.endLabel.text = chargingResultClass.datas?.finishCode_ch ?? ""
//        }
    }
    
}
