//
//  UseInfoTableViewCell.swift
//  新的頁面
//
//  Created by Daisuke on 2019/11/21.
//  Copyright © 2019 Daisuke. All rights reserved.
//

import UIKit

class UseInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var useTimeTitleLabel: UILabel!
    @IBOutlet weak var useTimeLabel: UILabel!
    @IBOutlet weak var useFeeTitleLabel: UILabel!
    @IBOutlet weak var useFeeLabel: UILabel!
    @IBOutlet weak var useCostTitleLabel: UILabel!
    @IBOutlet weak var useCostLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
