//
//  ImageViewGIFTableViewCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class ImageViewGIFTableViewCell: UITableViewCell {
    @IBOutlet weak var imageHeightLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageWidthLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var GIFImageView: UIImageView!
}
