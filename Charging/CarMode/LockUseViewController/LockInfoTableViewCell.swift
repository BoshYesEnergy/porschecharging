//
//  LockInfoTableViewCell.swift
//  新的頁面
//
//  Created by Daisuke on 2019/11/21.
//  Copyright © 2019 Daisuke. All rights reserved.
//

import UIKit

class LockInfoTableViewCell: UITableViewCell {
    @IBOutlet weak var siteNameLabel: UILabel!
    @IBOutlet weak var charginCode: UILabel!
    @IBOutlet weak var lockCode: UILabel!
    
    var reserveDetailClass:ReserveDetailClass? {
        didSet {
            self.setupViews()
        }
    }
    
    func setupViews() {
        
    }

}
