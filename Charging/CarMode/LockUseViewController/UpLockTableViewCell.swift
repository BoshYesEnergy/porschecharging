//
//  UpLockTableViewCell.swift
//  新的頁面
//
//  Created by Daisuke on 2019/11/21.
//  Copyright © 2019 Daisuke. All rights reserved.
//

import UIKit

protocol UpLockTableViewCellDelegate {
    func upLockButtonAction()
}

class UpLockTableViewCell: UITableViewCell {

    @IBOutlet weak var lockButton: UIButton!
     var delegate: UpLockTableViewCellDelegate? = nil

     @IBAction func upLockButtonAction(_ sender: Any) {
         if let delegate = self.delegate {
             delegate.upLockButtonAction()
         }
     }
}
