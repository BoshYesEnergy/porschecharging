//
//  LockUseViewController.swift
//  新的頁面
//
//  Created by Daisuke on 2019/11/21.
//  Copyright © 2019 Daisuke. All rights reserved.
//

import UIKit

enum LockUseCellType:Int {
    case LockUseCellTypeInfo = 0
    case LockUseCellTypeCarGif = 1
    case LockUseCellTypeUseInfo = 2
    case LockUseCellTypeCarInfo = 3
    case LockUseCellTypeMessage = 4
    case LockUseCellTypeUpLock = 5
    case LockUseCellTypeCustomPhone = 6
}

//MARK: setup
extension LockUseViewController {
    func fetchData10Sec() {
        if timer10sec == nil {
            timer10sec = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(fetchData), userInfo: nil, repeats: true)
            timer10sec?.fire()
            RunLoop.current.add(timer10sec!, forMode: .commonModes)
        }
    }
    
    func setupInitValue() {
        if #available(iOS 13.0, *) {
            //self.chargingTableView.automaticallyAdjustsScrollIndicatorInsets = false
        } else if #available(iOS 11.0, *) {
            self.lockUseTableView.contentInsetAdjustmentBehavior = .never
        }else{
            self.automaticallyAdjustsScrollViewInsets = false
        }
    }
    
    func setupNavView() {
        createNavViewForRightBtn(rightName: "", rightImage: "", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: UIColor.black)
        titleLab?.text = "車位使用中(2/5)"
    }
    
    func setupTableView() {
        self.topLayoutConstraint.constant = KJLCommon.isX() == true ? 88 : 64
        self.lockUseTableView.backgroundColor = UIColor.clear
        self.lockUseTableView.separatorStyle = .none
        self.lockUseTableView.dataSource = self
        self.lockUseTableView.register(UINib.init(nibName: "LockInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "LockInfoTableViewCell")
        self.lockUseTableView.register(UINib.init(nibName: "ImageViewGIFTableViewCell", bundle: nil), forCellReuseIdentifier: "ImageViewGIFTableViewCell")
        self.lockUseTableView.register(UINib.init(nibName: "CarInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "CarInfoTableViewCell")
        self.lockUseTableView.register(UINib.init(nibName: "UseInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "UseInfoTableViewCell")
        self.lockUseTableView.register(UINib.init(nibName: "UpLockMessageCell", bundle: nil), forCellReuseIdentifier: "UpLockMessageCell")
        self.lockUseTableView.register(UINib.init(nibName: "UpLockTableViewCell", bundle: nil), forCellReuseIdentifier: "UpLockTableViewCell")
    }
    
    func checkChargingOvertimeStatus() {
        KJLRequest.checkChargingOvertimeStatus(tranNo:"\(tranNo ?? 0)") { (response) in
            guard let response = response as? ReserveDetailClass else {
                return
            }
            guard let data = response.datas else {
                return
            }
            if data.chargeIsStart == "Y" {
                //前往充電頁面
                self.invalidateTime()
                self.openCarStartChargingViewController()
            }
        }
    }
    
    func openCarStartChargingViewController() {
        //開始充電
        let carStartChargingViewController = CarStartChargingViewController()
        carStartChargingViewController.tranNo = tranNo;
        carStartChargingViewController.idStr = self.idStr
        carStartChargingViewController.nameStr = self.nameStr
        carStartChargingViewController.lockCode = "已降下"
        self.navigationController?.pushViewController(carStartChargingViewController, animated: true)
    }
    
    // API-403：充電狀態查詢 Step3
    func callAPIRequestForCarChargingStatus() {
        print("tranNo = \(String(describing: tranNo))")
        KJLRequest.requestForCarChargingStatus(tranNo: "\(tranNo ?? 0)") { (response) in
            guard let response = response as? ChargingStatusClass else {
                    return
            }
//            if let totalTime = response.datas?.parkTotalTime {
//                if (totalTime > self.timeNum) {
//                    self.timeNum = totalTime
//                }
//
//                if (totalTime > self.timeNum) {
//                    self.timeNum = totalTime
//                }
//            }
            
            self.chargingStatusClass = response
            self.lockUseTableView.reloadRows(at: [IndexPath.init(row: LockUseCellType.LockUseCellTypeUseInfo.rawValue, section: 0)], with: .none)
        }
    }
}

//MARK: UITableViewDataSource
extension LockUseViewController:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return LockUseCellType.LockUseCellTypeCustomPhone.rawValue + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellType:LockUseCellType = LockUseCellType(rawValue: indexPath.item)!
        switch cellType {
        case .LockUseCellTypeInfo:
            let cell = tableView.dequeueReusableCell(withIdentifier: "LockInfoTableViewCell", for: indexPath) as! LockInfoTableViewCell
            cell.siteNameLabel.text = self.nameStr
            cell.charginCode.text = "充電槍代碼 " + self.idStr
            cell.lockCode.text = "地鎖狀態 已降下"
            cell.selectionStyle = .none
            return cell
        case .LockUseCellTypeCarGif:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ImageViewGIFTableViewCell", for: indexPath) as! ImageViewGIFTableViewCell
            cell.GIFImageView.animationRepeatCount = 1
            cell.GIFImageView.loadGif(name: "lockDownHour")
            cell.selectionStyle = .none
            return cell
        case .LockUseCellTypeUseInfo:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UseInfoTableViewCell", for: indexPath) as! UseInfoTableViewCell
            cell.useTimeTitleLabel.text = "車位使用累計時間 "
            cell.useFeeTitleLabel.text = "車位使用費率 "
            cell.useCostTitleLabel.text = "車位使用累計費用 "
            cell.useTimeLabel.text = self.totalTime
//            cell.useFeeLabel.text = self.chargingStatusClass?.datas?.parkRate ?? ""
//            cell.useCostLabel.text = "\(self.chargingStatusClass?.datas?.parkAmount ?? 0) 元"
            cell.selectionStyle = .none
            return cell
            case .LockUseCellTypeCarInfo:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CarInfoTableViewCell", for: indexPath) as! CarInfoTableViewCell
            cell.selectionStyle = .none
            return cell
        case .LockUseCellTypeMessage:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UpLockMessageCell", for: indexPath) as! UpLockMessageCell
            cell.messageLabel.text = "請於確認離開車位後\n按下升起地鎖，結束車位使用"
            cell.selectionStyle = .none
            return cell
        case .LockUseCellTypeUpLock:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UpLockTableViewCell", for: indexPath) as! UpLockTableViewCell
            cell.delegate = self
            cell.selectionStyle = .none
            return cell
        case .LockUseCellTypeCustomPhone:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UpLockMessageCell", for: indexPath) as! UpLockMessageCell
            cell.messageLabel.text = "若地鎖無法升起，請聯繫客服電話\n02-5592-0006"
            cell.selectionStyle = .none
            return cell
        }
    }
    
    @objc private func setupTotalTimeLabel() {
        
        let cycle = 60
        var second: Int = timeNum
        var minutes: Int = 0
        var hour: Int = 0
        if (second >= cycle) {
            minutes = second / cycle
            second = second % cycle
        }
        
        if (minutes >= cycle) {
            hour = minutes / cycle
            minutes = minutes % cycle
        }
        
        self.totalTime =  String(format: "%i:%02i:%02i", hour, minutes, second)
        self.lockUseTableView.reloadRows(at: [IndexPath.init(row: LockUseCellType.LockUseCellTypeUseInfo.rawValue, section: 0)], with: .none)
        self.timeNum += 1
    }
}

//MARK: UpLockTableViewCellDelegate
extension LockUseViewController: UpLockTableViewCellDelegate {
    func upLockButtonAction() {
        //問418是否在充電
        //如果在充電,就幫他跳第三頁
        //如果沒充電就call 5414
        //5414]/ParkingLockUp
        //拿到結果在跳結果夜
        KJLRequest.checkChargingOvertimeStatus(tranNo:"\(tranNo ?? 0)") { (response) in
            guard let response = response as? ReserveDetailClass else {
                return
            }
            guard let data = response.datas else {
                return
            }
            
            if data.chargeIsStart == "Y" {
                self.invalidateTime()
                ShowMessageView.showMessage(title: "", message: "車輛正在充電中，無法升起地鎖。", completion: { (isTrue) in
                    self.openCarStartChargingViewController()
                })
            }else{
                KJLRequest.checkChargingParkingLockUp(tranNo: "\(self.tranNo ?? 0)") { (response) in
//                    guard let response = response as? ParkingLockOpenClass else {
//                        return
//                    }
                    self.invalidateTime()
                    let carCompleteChargingViewController = CarCompleteChargingViewController()
                    carCompleteChargingViewController.tranNo = self.tranNo
                    self.navigationController?.pushViewController(carCompleteChargingViewController, animated: true)
                    
                }
            }
        }
    }
}

//MARK: fetchData
extension LockUseViewController {
    @objc func fetchData() {
        self.checkChargingOvertimeStatus()
        self.callAPIRequestForCarChargingStatus()
    }
    
    func invalidateTime() {
        if (timer10sec != nil) {
            timer10sec?.invalidate()
        }
        
        if timer != nil {
            timer?.invalidate()
        }
    }
    
    @objc override func backAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
}

//MARK: life cycle
class LockUseViewController: KJLNavView {
    var tranNo: Int64?
    var nameStr:String  = ""
    var idStr:String = ""
    var timer10sec: Timer?
    var timer:Timer?
    var totalTime:String = "00:00:00"
    private var timeNum: Int = 0
    var chargingStatusClass:ChargingStatusClass?
    @IBOutlet weak var lockUseTableView: UITableView!
    @IBOutlet weak var topLayoutConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
        self.setupNavView()
        self.setupInitValue()
        self.fetchData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.fetchData10Sec()
        
//        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(setupStartTime), userInfo: nil, repeats: true)
        RunLoop.current.add(timer!, forMode: .commonModes)
        timer?.fire()
    }
}
