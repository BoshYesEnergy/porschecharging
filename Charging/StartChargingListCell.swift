//
//  StartChargingListCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class StartChargingListCell: UITableViewCell {

    @IBOutlet weak var nameLab: UILabel!
    @IBOutlet weak var timeLab: UILabel!
    @IBOutlet weak var lineView: UIView!
    var myData: ChargingListClass.ChargingList?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        nameLab.text = ""
        timeLab.text = ""
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func changeData(data: ChargingListClass.ChargingList, isLine: Bool) {
        myData = data
        nameLab.text = data.stationName
        timeLab.text = data.startTime
        lineView.isHidden = isLine
    }
}
