//
//  StartChargingViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit
import Alamofire

class StartChargingViewController: KJLNavView {

    @IBOutlet weak var mainScrollView: UIScrollView!
    // MARK: - IBOutlet
    @IBOutlet weak var nameLab: UILabel!
    @IBOutlet weak var idLab: UILabel!
    //@IBOutlet weak var kilowattView: UIView!
    @IBOutlet weak var currentView: UIView!
    @IBOutlet weak var voltageView: UIView!
    @IBOutlet weak var bigImage: UIImageView!
    @IBOutlet weak var currentLab: UILabel!
    @IBOutlet weak var voltageLab: UILabel!
    //@IBOutlet weak var kwhLab: UILabel!
    @IBOutlet weak var totalTimeLab: UILabel!
    @IBOutlet weak var chargingFeeLab: UILabel!
    @IBOutlet weak var chargeRateLab: UILabel!
    @IBOutlet weak var chargeRatefee: UILabel!
    @IBOutlet weak var motorView: UIView!
    @IBOutlet weak var motorImage: UIImageView!
    @IBOutlet weak var motorLabel: UILabel!
    @IBOutlet weak var motorRemainLabel: UILabel!
    @IBOutlet weak var remainPointLabel: UILabel!
    @IBOutlet weak var remainPointView: UIView!
    @IBOutlet weak var amountTypeLabel: UILabel!
    @IBOutlet weak var pointEndDateLabel: UILabel!
    @IBOutlet weak var overtimeRateLabel: UILabel!
    @IBOutlet weak var kwhLabel: UILabel!
    @IBOutlet weak var overtimeRateView: UIView!
    @IBOutlet weak var pointEndDateView: UIView!
    @IBOutlet weak var kwView: UIView!
    @IBOutlet weak var infoStackView: UIStackView!
    @IBOutlet weak var noneQRcodeBtn: UIButton!
    @IBOutlet weak var noneQRcodeHeight: NSLayoutConstraint!
    
    // MARK: -  Property
    var retryNum = 0
    var timer: Timer?
    var tranNo: Int64?
    var nameStr: String?
    var idStr: String?
    private var timeNum: Int = 0
    private var licensePlate = ""
    private var chargetimeStart: String?
    
    // MARK: - 生命週期
    override func viewDidLoad() {
        super.viewDidLoad()
       self.mainScrollView.alpha = 0
        createNavViewForRightBtn(rightName: "", rightImage: "", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: UIColor.black)
        titleLab?.text = "充電中( 2/5 )"
        self.motorLabel.text = "目前電池電量： 讀取中..."
        totalTimeLab.text = ""
        self.setupView(view: currentView)
        self.setupView(view: voltageView)
        self.setupView(view: kwView)
        
        for temp in backBtn?.allTargets ?? [] {
            backBtn?.removeTarget(self, action: temp as? Selector, for: .touchUpInside)
        }
        backBtn?.addTarget(self, action: #selector(sureAction(_:)), for: .touchUpInside)
        nameLab.text = nameStr
        idLab.text = "充電槍代碼：\(idStr ?? "")"
        
        // car part
//        bigImage.loadGif(name: "charging_car")
//        bigImage.isHidden = true
        
        // motor part
        motorView.backgroundColor = UIColor.clear
        motorImage.loadGif(name: "battery5")
        motorView.isHidden = false
        noneQRcodeBtn.layer.cornerRadius = 5
        self.callAPIRequestForChargingStatus()
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        startTimer()
        NotificationCenter.default.addObserver(self, selector: #selector(chargingStatus), name: .chargingStatus, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(startTimer), name: .startChargingTimer, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopTimer()
    }
    
    // MARK: - API
    // API-403：充電狀態查詢 Step3
    func callAPIRequestForChargingStatus() {
        print("tranNo = \(String(describing: tranNo))")
        KJLRequest.requestForChargingStatus(tranNo: "\(tranNo ?? 0)") {[weak self] (response) in
            self?.retryNum = 5
            guard let response = response as? ChargingStatusClass
                else { return }
            
//            self?.noneQRcodeHeight.constant = response.datas?.bind == true ? 0 : 0
//
//            self?.licensePlate = response.datas?.licensePlate ?? ""
//            self?.currentLab.text = response.datas?.current ?? ""
//            self?.voltageLab.text = response.datas?.voltage ?? ""
//            self?.kwhLabel.text = response.datas?.kwh ?? "0"
//            self?.chargeRateLab.text = response.datas?.chargeRate ?? ""
//            self?.remainPointLabel.text = response.datas?.remainPoint ?? ""
//            self?.pointEndDateLabel.text = response.datas?.pointEndTime ?? ""
//            self?.overtimeRateLabel.text = response.datas?.overtimeFee ?? ""
//            if (response.datas?.amountType == "P") {
//                self?.amountTypeLabel.text = "扣點"
//                self?.remainPointView.isHidden = false
//                self?.pointEndDateView.isHidden = false
//            } else {
//                self?.amountTypeLabel.text = "扣款"
//                self?.remainPointView.isHidden = true
//                self?.pointEndDateView.isHidden = true
//            }
            
//            if (response.datas?.amountType == "P") {
//                self?.chargeRatefee.text = "累計點數："
//                self?.chargingFeeLab.text = "\(response.datas?.chargeAmount ?? 0)" + " 點"
//            } else {
//                self?.chargeRatefee.text = "累計費用："
//                self?.chargingFeeLab.text = "$" + "\(response.datas?.chargeAmount ?? 0)"
//            }
//
//            self?.chargetimeStart = response.datas?.chargetimeStart
//            let totalTime = Int(response.datas?.totalTime ?? 0)
//            if totalTime <= 0 {
//                self.timeNum = 0
//            } else {
//                self.timeNum = totalTime
//            }
            
            // status : 充電中Charging ; 完成 Finish ; 中斷 Interrupt ; 原頁等待 available ; 返回 timeout  
            if response.datas?.status?.lowercased() == "finish"
                || response.datas?.status?.lowercased() == "interrupt" {
                self?.stopTimer()
                self?.checkChargingOvertimeStatus()
            }
            // 1:汽車, 2:機車
//            if response.datas?.type == "2" {
//                self?.motorLabel.text = "目前電池電量：" + (response.datas?.percentage ?? "") + "%"
//            }
//            
            if self?.mainScrollView.alpha == 0 {
                UIView.animate(withDuration: 0.5, animations: {
                    self?.mainScrollView.alpha = 1
                })
            }
        }
    }
    
    // API-405：停止充電
    private func callAPIRequestForStopCharging() {
        KJLRequest.requestForStopCharging(tranNo: "\(self.tranNo ?? 0)") { (response) in
            guard response != nil else {
                return
            }
            self.checkChargingOvertimeStatus()
        
        }
    }
    
    @objc func chargingStatus() {
        callAPIRequestForChargingStatus()
    }
    
    @objc func startTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(countDown), userInfo: nil, repeats: true)
        timer?.fire()
    }
    
    func stopTimer(){
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
    }
    
    // MARK: - 自訂 Func
    @objc func countDown() {
        self.setupTotalTimeLabel()
        
        guard retryNum > 0
            else { return }
        
        retryNum = retryNum - 1
        
        if retryNum == 0 {
            AF.session.getAllTasks { (tasks) in
                if tasks.count > 0 {
                    
                } else {
                    self.callAPIRequestForChargingStatus()
                }
            }
            /// 舊語法
//            Alamofire.SessionManager.default.session.getAllTasks { (tasks) in
//                if tasks.count > 0 {
//
//                } else {
//                    self.callAPIRequestForChargingStatus()
//                }
//            }
        }
    }

    private func setupTotalTimeLabel() {
        guard let chargetimeStart = chargetimeStart else { return }
        let formatter = DateFormatter()
        formatter.locale = Locale.init(identifier: "zh_CN")
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if chargetimeStart == "0" {
                totalTimeLab.text = ""
                ScheduledTimer.shared.setLabel(totalTimeLab, "計 算 中...", time: 0.15)
        } else {
//            let date = formatter.date(from: chargetimeStart)!
//            let startTime = Int(date.timeIntervalSince1970)
//            let now = Int(Date().timeIntervalSince1970)
//            let total = now - startTime
//            print("now: \(now)\nstartTime: \(startTime)\ntotal: \(total)")
//            
//            let cycle = 60
//            //        var second: Int = timeNum
//            var second: Int = total
//            var minutes: Int = 0
//            var hour: Int = 0
//            
//            if (second >= cycle) {
//                minutes = second / cycle
//                second = second % cycle
//            }
//            
//            if (minutes >= cycle) {
//                hour = minutes / cycle
//                minutes = minutes % cycle
//            }
//            totalTimeLab.text = ""
//            self.totalTimeLab.text = String(format: "%i:%02i:%02i", hour, minutes, second)
        }
//        self.timeNum += 1
    }
    
    private func setupView(view: UIView) {
        view.layer.cornerRadius = 10
        view.layer.masksToBounds = true
        view.layer.borderWidth = 1
        view.layer.borderColor = #colorLiteral(red: 0.8980392157, green: 0.8980392157, blue: 0.8980392157, alpha: 1)
    }
    
    // MARK: - IBAction
    @IBAction func stopChargingAction(_ sender: Any) {
        // 停止充電 
        ShowMessageView.showQuestion(title: nil, message: "是否要停止充電?") { (isOK) in
            if isOK == true {
                    self.callAPIRequestForStopCharging()
            }
        }
    }
    
    func checkChargingOvertimeStatus() {
        KJLRequest.checkChargingOvertimeStatus(tranNo:"\(self.tranNo ?? 0)") {[weak self] (response) in
            guard let response = response as? ReserveDetailClass,
                let data = response.datas
                else { return }
            
            // 停止 Timer
            self?.stopTimer()
            
//            if data.gunIsEnd == "Y" {
                //充電結果頁
//                if let vc = self?.storyboard?.instantiateViewController(withIdentifier: "CompleteChargingViewController") as? CompleteChargingViewController {
//                    vc.tranNo = self?.tranNo
//                    self?.navigationController?.show(vc, sender: self)
//                }
//            } else {
//                if data.bufferFlag == "N" {
//                    //N:去逾時計費頁面
//                    let timeoutFeeBufferViewController = TimeoutFeeBufferViewController()
//                    timeoutFeeBufferViewController.reserveDetailClass = response;
//                    timeoutFeeBufferViewController.tranNo = self?.tranNo
//                    self?.navigationController?.show(timeoutFeeBufferViewController, sender: self)
//                } else {
//                    //Y:去逾時緩衝頁面
//                    let timeoutFeeCalculateViewController = TimeoutFeeCalculateViewController()
//                    timeoutFeeCalculateViewController.reserveDetailClass = response;
//                    timeoutFeeCalculateViewController.tranNo = self?.tranNo
//                    self?.navigationController?.show(timeoutFeeCalculateViewController, sender: self)
//                }
//            }
        }
    }
    
    
    @IBAction func gotoBindPate(_ sender: UIButton) {
        
    }

    @IBAction func sureAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
