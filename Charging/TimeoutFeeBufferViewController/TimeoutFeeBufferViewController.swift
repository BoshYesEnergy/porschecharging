//
//  TimeoutFeeBufferViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class TimeoutFeeBufferViewController: KJLNavView {
    var reserveDetailClass:ReserveDetailClass?
    @IBOutlet weak var chargingStateLabel: UILabel!
    @IBOutlet weak var timeOutLabel: UILabel!
    
    @IBOutlet weak var chargingImageView: UIImageView!
    @IBOutlet weak var pointLabel: UILabel!
    @IBOutlet weak var feeLabel: UILabel!
    @IBOutlet weak var serviceLabel: UILabel!
    var tranNo: Int64?
    var timer: Timer?
    var timer10sec: Timer?
    var sec: NSInteger = 0
    var today: Date?
    var old: Date?
    var isFetchData: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchData()
        
//        createNavViewForRightBtn(rightName: "", rightImage: "", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: UIColor.black)
        titleLab?.text = "逾時費用計算 ( 4/5 )"
        self.pointLabel.text = "累積逾時費用: 讀取中..."
        self.feeLabel.text = "逾時費率: 讀取中..."
        self.backBtn?.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let tapAction = UITapGestureRecognizer(target: self, action: #selector(callPhoneAction))
        tapAction.numberOfTapsRequired = 1
        self.serviceLabel.addGestureRecognizer(tapAction)
    }
    
    @objc public func callPhoneAction(sender: UITapGestureRecognizer) {
        let number = self.serviceLabel.text!.replacingOccurrences(of: " ", with: "-")
        if number.count > 0 {
            let phoneUrl = URL(string: "tel://\(number)")
            UIApplication.shared.open(phoneUrl!, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func checkAction(_ sender: Any) {
        // 假裝呼叫 api 
        KJLRequest.checkChargingOvertimeStatus(tranNo:"\(tranNo ?? 0)") { (response) in
            guard let response = response as? ReserveDetailClass else {
                return
            }
            guard let data = response.datas else {
                return
            }
            if data.gunIsEnd == "Y" {
                //充電結果頁
                self.invalidateTime()
                self.openCompleteChargingViewController()
            }else{
                ShowMessageView.showMessage(title: "", message: "您尚未掛回充電槍，\n請再次檢查，\n是否已正常掛回。") { (isOK) in
                }
            }
            
        }
    }
    
    func dateToDateString(_ date:Date) -> String {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.locale = Locale(identifier: "zh_Hant_TW") // 設定地區(台灣)
        dateFormatter.timeZone = TimeZone(identifier: "Asia/Taipei") // 設定時區(台灣)
        return dateFormatter.string(from: date)
    }
    
    func timeStringToDate(_ dateStr:String) ->Date {
        let dateFormatter2 = DateFormatter()
        dateFormatter2.timeZone = TimeZone(secondsFromGMT: 0)
        dateFormatter2.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter2.date(from: dateStr)!
    }
    
    @objc func fetchData() {
        if isFetchData { return }
        self.isFetchData = true
        
        // 假裝呼叫 api
        KJLRequest.checkChargingOvertimeStatus(tranNo:"\(tranNo ?? 0)") { (response) in
            guard let response = response as? ReserveDetailClass else {
                return
            }
            
            guard let data = response.datas else {
                return
            }
            
            
            if data.gunIsEnd == "Y" {
                //充電結果頁
                self.invalidateTime()
                self.openCompleteChargingViewController()
            }else{
                self.chargingStateLabel.text = "" + (data.finishCode_ch ?? "")
                let evEndTime : String? = data.evEndTime // 充電結束時間 yyyy-MM-dd HH:mm:ss
                if evEndTime != nil && evEndTime != ""{
                    let overTime : String? = data.overTime // 緩衝時間 (分鐘)
                    let nowTime : String? =  data.nowTime // Server 現在時間 yyyy-MM-dd HH:mm:ss
                    var dc = DateComponents()
                    dc.minute = Int(overTime!)
                    self.old = Calendar.current.date(byAdding: dc, to: self.timeStringToDate(evEndTime!))
                    self.today = self.timeStringToDate(nowTime!)
                    self.sec = (self.today!.daysBetweenDate(toDate: self.old!) * -1)
                    print("self.sec = \(self.sec)")
                    self.configSec()
                    self.pointLabel.text = "累積逾時費用: $ " + data.overTimeAmt!
                    self.feeLabel.text = "逾時費率: " + data.overTimeFee!
                    self.serviceLabel.text = data.servicePhone
                }
            }
        }
        
        self.isFetchData = false
        self.fetchData10Sec()
    }
    
    func fetchData10Sec() {
        if timer10sec == nil {
            timer10sec = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(fetchData), userInfo: nil, repeats: true)
            timer10sec?.fire()
        }
    }
    
    func configSec() {
        if timer == nil {
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(countDown), userInfo: nil, repeats: true)
            timer?.fire()
        }
    }
    
    @objc func countDown() {
        if (self.sec <= 0) {
            self.timeOutLabel.text = "00:00:00"
            timer?.invalidate()
        } else {
            self.sec = self.sec + 1
            let hh = String(format: "%02d", self.sec / 3600)
            let mm = String(format: "%02d", (self.sec / 60) % 60)
            let ss = String(format: "%02d", self.sec % 60)
            self.timeOutLabel.text = "\(hh):\(mm):\(ss)"
        }
    }
    
    func invalidateTime() {
        if (timer != nil) {
            timer?.invalidate()
        }
        
        if (timer10sec != nil) {
            timer10sec?.invalidate()
        }
    }
    
    @objc override func backAction() {
        self.invalidateTime()
        if self.navigationController == nil || self.navigationController?.viewControllers.count == 1 {
            self.dismiss(animated: true, completion: nil)
        }else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func openCompleteChargingViewController() {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        if let vc = storyboard.instantiateViewController(withIdentifier: "CompleteChargingViewController") as? CompleteChargingViewController {
//            vc.tranNo = self.tranNo
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
    }
}
