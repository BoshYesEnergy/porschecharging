//
//  StationPhotoCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

protocol StationPhotoCellDelegate: class {
    func photoCollectionCellDidSelect(indexPath: IndexPath)
}

class StationPhotoCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    
    private var picDataArray: [StationDetailClass.Data.PicArray] = []
    weak var stationPhotoCellDelegate: StationPhotoCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    
    func setupCellWith(picDataArray: [StationDetailClass.Data.PicArray] , delegate: StationPhotoCellDelegate) {
        self.picDataArray = picDataArray
        self.stationPhotoCellDelegate = delegate
        self.collectionView.reloadData()
    }
}

extension StationPhotoCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.picDataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StationImageCollectionCell", for: indexPath) as? StationImageCollectionCell {
            cell.setupPhotoCellWith(picData: self.picDataArray[indexPath.item])
            return cell
        }
        return UICollectionViewCell()
    }
}

extension StationPhotoCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.stationPhotoCellDelegate?.photoCollectionCellDidSelect(indexPath: indexPath)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 14
    }
}
