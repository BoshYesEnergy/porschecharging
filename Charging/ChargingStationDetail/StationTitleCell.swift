//
//  StationTitleCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

protocol StationTitleCellDelegate: class {
    func naviAction()
    func chargeReplyAction()
    func reserveChargeAction()
}

class StationTitleCell: UITableViewCell {

    // MARK: IBOutlet
    @IBOutlet weak var titleImage: UIImageView!
     
    @IBOutlet weak var stationIdLabel: UILabel!
    @IBOutlet weak var stationNameLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var naviBtn: UIButton!
    @IBOutlet weak var chargeReplyBtn: UIButton!
    @IBOutlet weak var reserveChargeBtn: UIButton!
//    @IBOutlet weak var kmWidth: NSLayoutConstraint!
    // AC、DC
    @IBOutlet weak var acEmptyLabel: UILabel!
    @IBOutlet weak var acTotalLabel: UILabel!
    @IBOutlet weak var dcEmptyLabel: UILabel!
    @IBOutlet weak var dcTotalLabel: UILabel!
    @IBOutlet weak var statusIcon: UIView!
    @IBOutlet weak var statusLab: UILabel!
    
    @IBOutlet weak var acCategoryViewHeight: NSLayoutConstraint!
    @IBOutlet weak var dcCategoryViewHeight: NSLayoutConstraint!
    @IBOutlet weak var acDcViewHeight: NSLayoutConstraint!
    
    var detail: StationDetailClass.Data?
    
    // MARK: Property
    weak var stationTitleCellDelegate: StationTitleCellDelegate?
    
    // MARK: - 生命週期
    override func awakeFromNib() {
        super.awakeFromNib()
         
        self.stationNameLabel.text = ""
        self.distanceLabel.text = ""
        self.stationIdLabel.text = ""
        self.acEmptyLabel.text = ""
        self.acTotalLabel.text = ""
        self.dcEmptyLabel.text = ""
        self.dcTotalLabel.text = "'"
        self.statusIcon.layer.cornerRadius = statusIcon.bounds.width / 2
//        chargeReplyBtn.setWhiteBackgroundBtn()
//        reserveChargeBtn.setWhiteBackgroundBtn()
        chargeReplyBtn.isHidden = true
//        self.kmWidth.constant = 60
    }
    
    // MARK: - IBAction
    @IBAction func naviAction(_ sender: UIButton) {
        self.stationTitleCellDelegate?.naviAction()
    }
    
    @IBAction func chargeReplyAction(_ sender: UIButton) {
        self.stationTitleCellDelegate?.chargeReplyAction()
    }
    
    @IBAction func reserveChargeAction(_ sender: UIButton) {
        self.stationTitleCellDelegate?.reserveChargeAction()
    }
    
    func defultImage() -> UIImage? {
        let typeString = UserDefaults.standard.string(forKey: "filterCarType") ?? "1"
        if typeString == "1" {
            // 汽車
            return UIImage(named: "charge_img_defult")
        }else{
            return UIImage(named: "Scooter_default_pic")
        }
    }
    // MARK: - 自訂 Func
    func setupCellWith(titleDataDicArray: [String: String] , acDcDataDicArray: [String: String], delegate: StationTitleCellDelegate) {
        let score = titleDataDicArray["Score"]
        if (score == "0.0") {
            
        }
        
//        switch detail?.openingTimeStatus {
//        case "opening":
//            statusLab.text = "營業中"
//            statusIcon.backgroundColor = #colorLiteral(red: 0.4841502905, green: 0.9742047191, blue: 0.3511900902, alpha: 1)
//        case "closed":
//            statusLab.text = "休息中"
//            statusIcon.backgroundColor = #colorLiteral(red: 1, green: 0.5333333333, blue: 0, alpha: 1)
//        case "going_rest":
//            statusLab.text = "即將休息"
//            statusIcon.backgroundColor = #colorLiteral(red: 1, green: 0.5333333333, blue: 0, alpha: 1)
//        default: break
//        }
        
        if (titleDataDicArray["Reserve"] == "true") {
            self.reserveChargeBtn.isHidden = true
        } else {
            self.reserveChargeBtn.isHidden = true
        }
        
        self.stationNameLabel.text = titleDataDicArray["StationName"]
        self.distanceLabel.text = titleDataDicArray["Distance"]
        self.distanceLabel.adjustsFontSizeToFitWidth = true
//        self.kmWidth.constant = self.distanceLabel.frame.width + 5
        self.stationIdLabel.text = titleDataDicArray["StationId"]
        
//        let imageUrl = URL(string: titleDataDicArray["TitleImageString"] ?? "")
//        let image = (detail?.status ?? "01" == "05") ? UIImage.init(named: "coming_soon") : self.defultImage()
        self.titleImage.image = UIImage(named: "img_photo01_01")
//        self.titleImage.kf.setImage(with: imageUrl, placeholder: image, options: nil, progressBlock: nil) { (img, error, type, url) in
//            if img != nil {
//                self.titleImage.image = img//UIImage(named: "text_icon2")
//            }
//        }
        
        // AC、DC
        if (KJLRequest.isLogin() == true) {
            self.acEmptyLabel.text = "空位：\(acDcDataDicArray["AcEmpty"] ?? "0")"
            self.acTotalLabel.text = "總數：\(acDcDataDicArray["AcTotal"] ?? "0")"
            self.dcEmptyLabel.text = "空位：\(acDcDataDicArray["DcEmpty"] ?? "0")"
            self.dcTotalLabel.text = "總數：\(acDcDataDicArray["DcTotal"] ?? "0")"
        } else {
            
            self.acEmptyLabel.text = "空位：請先登入"
            self.acTotalLabel.text = "總數：\(acDcDataDicArray["AcTotal"] ?? "0")"
            self.dcEmptyLabel.text = "空位：請先登入"
            self.dcTotalLabel.text = "總數：\(acDcDataDicArray["DcTotal"] ?? "0")"
        }
       
        
        if (acDcDataDicArray["Status"] == "03") {
            self.acEmptyLabel.text = "空位：--"
            self.dcEmptyLabel.text = "空位：--"
        }
        
        if (acDcDataDicArray["HasAC"] == "true" && acDcDataDicArray["HasDC"] == "false") {
            self.acCategoryViewHeight.constant = 30
            self.dcCategoryViewHeight.constant = 0
        } else if (acDcDataDicArray["HasAC"] == "false" && acDcDataDicArray["HasDC"] == "true") {
            self.acCategoryViewHeight.constant = 0
            self.dcCategoryViewHeight.constant = 30
        } else {
            self.acCategoryViewHeight.constant = 30
            self.dcCategoryViewHeight.constant = 30
        }
        self.acDcViewHeight.constant = self.acCategoryViewHeight.constant + self.dcCategoryViewHeight.constant
        
        self.stationTitleCellDelegate = delegate
    }
}
