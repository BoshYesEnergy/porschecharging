//
//  BusinessHoursViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class BusinessHoursViewController: UIViewController {

    let table = UITableView()
    var rowHeight: CGFloat = 30
    var contentList: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        settingView()
        setTable()
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
}


//MARK:- Setting View
extension BusinessHoursViewController {
    private func settingView() {
        view.addSubview(table)
        table.translatesAutoresizingMaskIntoConstraints                         = false
        table.topAnchor.constraint(equalTo: view.topAnchor).isActive            = true
        table.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive    = true
        table.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive      = true
        table.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive  = true
        table.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }
    
    private func setTable() {
        table.separatorStyle = .none
        table.delegate       = self
        table.dataSource     = self
    }
}


//MARK:- Table view delegate & data source
extension BusinessHoursViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contentList.count 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.selectionStyle = .none
        cell.textLabel?.text = contentList[indexPath.row]
        cell.textLabel?.textAlignment = .center
        cell.textLabel?.adjustsFontSizeToFitWidth = true
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowHeight
    }
}
