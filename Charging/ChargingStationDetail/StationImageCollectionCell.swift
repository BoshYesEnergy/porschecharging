//
//  StationImageCollectionCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class StationImageCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var photoImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupPhotoCellWith(picData: StationDetailClass.Data.PicArray?) {
        guard let data = picData else {
            photoImage.image = KJLCommon.defaultImage()
            return
        }
        
        let tempUrl = URL(string: data.picPath ?? "")
        photoImage.kf.setImage(with: tempUrl, placeholder: KJLCommon.defaultImage(), options: nil, progressBlock: nil) { (img, error, type, url) in
            if img == nil {
                self.photoImage.image = KJLCommon.defaultImage()
            } else {
                self.photoImage.image = img//UIImage(named: "text_icon2")
            }
        }
    }
    
    func setupChargeGunCellWith(name: String) {
        let rename = name.replacingOccurrences(of: "/", with: ":")
        photoImage.image = UIImage(named: "home_img_\(rename)")
    }
}
