//
//  StationShowMoreCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

protocol StationShowMoreCellDelegate: class {
    func showMoreAction()
}

class StationShowMoreCell: UITableViewCell {
    
    @IBOutlet weak var showMoreBtn: UIButton!
    
    weak var stationShowMoreCellDelegate: StationShowMoreCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        showMoreBtn.setBackgroundImage(UIImage(named: "charge_btn_moreevaluation_normal"), for: .normal)
        showMoreBtn.setBackgroundImage(UIImage(named: "station_btn_moreevaluation_pressed"), for: .highlighted)
    }

    @IBAction func showMoreAction(_ sender: UIButton) {
        self.stationShowMoreCellDelegate?.showMoreAction()
    }
    
    func setupCellWith(isShowMoreBtnHidden: Bool , delegate: StationShowMoreCellDelegate) {
        self.showMoreBtn.isHidden = isShowMoreBtnHidden
        self.stationShowMoreCellDelegate = delegate
    }
}
