//
//  StationTableHeaderCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class StationTableHeaderCell: UITableViewCell {

    @IBOutlet weak var headerLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.headerLabel.text = ""
    }

    func setupCellWith(headerString: String) {
        self.headerLabel.text = headerString
    }
}
