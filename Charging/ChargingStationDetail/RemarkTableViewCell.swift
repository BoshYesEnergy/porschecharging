//
//  RemarkTableViewCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class RemarkTableViewCell: UITableViewCell {
    @IBOutlet weak var remarkLabel: UILabel!
    @IBOutlet weak var remarkImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.remarkLabel.numberOfLines = 0
        self.remarkImageView.image = UIImage.init(named: "iconfinder_icon")
    }
}
