//
//  ChargingStationDetailViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit
 
class ChargingStationDetailViewController: KJLNavView{
    
    // MARK: - IBOutlet
    @IBOutlet weak var tableView:
    UITableView!

    // MARK: - Property
    var stationCategory = "YES"
    private var stationNo: String = ""
    private var detail: StationDetailClass.Data? = nil
    private var majiDetail: MajiStationDetailClass? = nil
    //標題
    private var titleDataDicArray: [String: String] = [:]
    private var picArray: [StationDetailClass.Data.PicArray] = []
    private var reservePrepareData: ReservePrepareClass.Data?  = nil
    // AcDC
    private var chargingTypeArray: [String] = []
    private var acDcDataDicArray: [String: String] = [:]
    private var fullBusinessHours: [String] = []
    private var isShowOneCategory: Bool = false
    // 充電站資訊
    //20190503後用的icon圖
        private var hintImageStringArray: [String] = ["9C9C9C",
                                                      "",
                                                      "address",
                                                      "車位",
                                                      "charge_ic_scooter",
                                                      "floor",
                                                      "charge_ic_actuate",
                                                      "charge_ic_power",
                                                      "car",
                                                      "停車費",
                                                      "floor",
                                                      "charge_ic_reservation",
                                                      "charge_ic_phone"]
    //原始圖陣列
//    private var hintImageStringArray: [String] = ["charge_ic_hours",
//                                                  "",
//                                                  "charge_ic_site",
//                                                  "charge_ic_car",
//                                                  "charge_ic_scooter",
//                                                  "charge_ic_actuate",
//                                                  "charge_ic_floor",
//                                                  "charge_ic_power",
//                                                  "charge_ic_car",
//                                                  "charge_ic_fee",
//                                                  "charge_ic_floor",
//                                                  "charge_ic_reservation",
//                                                  "charge_ic_phone"]
    
//    private var majiPayImageArrar:[String] = []
//    private var lineViewArray : [Bool] = [true,true,true,true,true,true,true,false,true,true,true,true,true]
   // private var line: [String] = [""]
    private var infoTitleArray: [String] = []
    private var infoDataArray: [String] = []
    // 充電評價
    private var showMessageCount: Int = 0
    private var isShowMoreBtnHidden = false
    private var evaluationDataArray: [ChargingReportListClass.Data] = []
    // 充電次數走勢
    private var sevenChargeDateArray: [String] = []
    private var sevenChargeTotalArray: [Double] = []
    var majiPayString = ""
    
    // MARK: - 生命週期
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 20, 0)
        self.tableView.register(UINib.init(nibName: "RemarkTableViewCell", bundle: nil), forCellReuseIdentifier: "RemarkTableViewCell")
        createNavViewForRightBtn(rightName: "", rightImage: "", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: UIColor.black)
        titleLab?.text = "充電站"
//        rightBtn?.addTarget(self, action: #selector(favoriteAction), for: .touchUpInside)
        self.callAPIRequestForStationDetail()
        self.callAPIRequestForSevenDaysCharge()
        KJLRequest.shareInstance().kJLRequestDelegate = self
        
//        self.tableView.separatorInset=UIEdgeInsetsMake(0,10, 0, 10);           //top left bottom right 左右边距相同
//        self.tableView.separatorStyle=UITableViewCell.SeparatorStyle.singleLine
//        self.tableView.separatorColor = UIColor.red
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.callAPIRequestForChargingReportList()
    }

    // MARK: - 自訂 Func
    // 我的最愛
    @objc func favoriteAction() {
        guard KJLRequest.isLogin() == true else {
            KJLCommon.showLogin(subcontrol: self)
            return
        }
        
        guard let no = detail?.stationId
            else { return }
        
        let isadd = self.detail?.favourite == true ? KJLRequest.RequestBOOL.FALSE : KJLRequest.RequestBOOL.TRUE
        KJLRequest.requestForAddFavourite(stationId: "\(no)", add: isadd) { (response) in
            guard (response as? NormalClass) != nil
                else { return }
            
            if self.detail?.favourite == true {
                self.detail?.favourite = false
                self.rightBtn?.setImage(UIImage(named: "charge_ic_love_empty"), for: .normal)
            } else {
                self.detail?.favourite = true
                self.rightBtn?.setImage(UIImage(named: "charge_ic_love"), for: .normal)
            }
        }
    }
    
    func setupVCWith(stationNo: String) {
        self.stationNo = stationNo
    }
    
    private func showPopoverMessage(indexPath: IndexPath) {
        if let pop = self.storyboard?.instantiateViewController(withIdentifier: "PopAddressViewController") as? PopAddressViewController { 
            pop.titles = "複製地址"
            guard let cell = self.tableView.cellForRow(at: indexPath) else { return }
            popover(pop: pop, size: CGSize(width: 80 , height: 45), arch: cell, Direction: .down)
            self.perform(#selector(self.removePopoverVC(_:)), with: pop, afterDelay: 1)
        }
        self.copyChargingStationAddress(addressString: self.infoDataArray[indexPath.row])
    }
    
    @objc func removePopoverVC(_ popoverVC: UIViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /// 複製地址
    private func copyChargingStationAddress(addressString: String) {
        let pastboard = UIPasteboard.general
        pastboard.string = addressString
    }
    
    // MARK: - API
    /// 302: 站點明細
    private func callAPIRequestForStationDetail() {
        //        stationNo = "2166" /* 測試 API 的代碼 */
        KJLRequest.requestForStationDetail(stationId: stationNo) { (response) in
            guard let response = response as? StationDetailClass,
                let data = response.datas
                else { return }
            
            self.detail = response.datas
            
            // Title
            self.titleDataDicArray["Score"] = data.stationScore
            self.titleDataDicArray["StationName"] = data.stationName
            
            let lat = Float(data.latitude ?? "0") ?? 0
            let long = Float(data.longitude ?? "0") ?? 0
            self.titleDataDicArray["Distance"] = "0 km"
            LocationManagers.updateLocation { (status, location) in
                if  status == .Success {
                    let current = CLLocation(latitude: (location?.coordinate)!.latitude, longitude: (location?.coordinate)!.longitude)
                    let location = CLLocation(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(long))
                    let distance = current.distance(from: location)
                    let meter = "\(distance)"
                    let km = String(format: "%.2f", (Float(meter) ?? 0)/1000)
                    self.titleDataDicArray["Distance"] = "\(km) km"
                }
            }
            
            self.titleDataDicArray["StationId"] = data.stationAbrv
            self.titleDataDicArray["Reserve"] = (data.reserve == true) ? "true" : "false"
            self.picArray = data.picArray ?? []
            self.titleDataDicArray["TitleImageString"] = data.picArray?.first?.picPath ?? ""
            // AC、DC
            self.acDcDataDicArray["AcEmpty"] = "\(data.acEmpty ?? 0)"
            self.acDcDataDicArray["AcTotal"] = "\(data.acTotal ?? 0)"
            self.acDcDataDicArray["DcEmpty"] = "\(data.dcEmpty ?? 0)"
            self.acDcDataDicArray["DcTotal"] = "\(data.dcTotal ?? 0)"
            self.acDcDataDicArray["Status"] = data.status
            self.acDcDataDicArray["HasAC"] = (data.hasAC ?? false) ? "true" : "false"
            self.acDcDataDicArray["HasDC"] = (data.hasDC ?? false) ? "true" : "false"
            if (data.hasAC == true && data.hasDC == true) {
                self.isShowOneCategory = false
            } else {
                self.isShowOneCategory = true
            }
            // 資訊
            self.callMajiDetailById(majiId: data.majiId ?? "", majiToken: data.majiToken ?? "")
//            guard let majiData = self.majiDetail?.data else{return}
//                for majiDatas in majiData{
//
//                    self.infoDataArray.append(majiDatas.current_business_hours ?? "")
//                    self.infoDataArray.append(majiDatas.address ?? "")
//                    self.infoDataArray.append(data.carSpace ?? "")
//                    self.infoDataArray.append(data.motorSpace ?? "")
//                    self.infoDataArray.append(data.ocppType ?? "")
//                    self.infoDataArray.append(data.spotFloor ?? "")
//                    self.infoDataArray.append(data.chargingFee ?? "")
//                    self.infoDataArray.append(majiDatas.extra_info?.current_parking_price ?? "")
//                    self.infoDataArray.append(data.reserveFee ?? "")
//                    self.infoDataArray.append(majiDatas.telephone ?? "")
//                }

//            self.infoTitleArray = ["營業狀態：","地址：","車位：","車位：","啟動方式：","充電樁樓層：","充電費：","停車費：","預約費：","電話："]
            
//

//            self.infoDataArray.append(data.businessHours ?? "")
//            self.infoDataArray.append(data.stationAddress ?? "")
//            self.infoDataArray.append(data.carSpace ?? "")
//            self.infoDataArray.append(data.motorSpace ?? "")
//            self.infoDataArray.append(data.ocppType ?? "")
//            self.infoDataArray.append(data.spotFloor ?? "")
//            self.infoDataArray.append(data.chargingFee ?? "")
//            self.infoDataArray.append(data.parkingFee ?? "")
//            self.infoDataArray.append(data.reserveFee ?? "")
//            self.infoDataArray.append(data.stationPhone ?? "")

//            self.infoTitleArray = ["營業狀態：","地址：","車位：","車位：","啟動方式：","充電樁樓層：","充電費：","停車費：","預約費：","電話："]

            // 最愛
            if data.favourite == true {
                self.rightBtn?.setImage(UIImage(named: "charge_ic_love"), for: .normal)
            } else {
                self.rightBtn?.setImage(UIImage(named: "charge_ic_love_empty"), for: .normal)
            }
            
            // 充電槍頭類型
            let chargingAC = data.interface_AC ?? ""
            let chargingDC = data.interface_DC ?? ""
            
            var chargingACs = chargingAC.components(separatedBy: ",")
            if (chargingACs.last?.count ?? 0) <= 0 {
                chargingACs.removeLast()
            }
            var chargingDCs = chargingDC.components(separatedBy: ",")
            if (chargingDCs.last?.count ?? 0) <= 0 {
                chargingDCs.removeLast()
            }
            self.chargingTypeArray.removeAll()
            if chargingACs.count > 0 {
                self.chargingTypeArray = chargingACs
            }
            if chargingDCs.count > 0 {
                self.chargingTypeArray = self.chargingTypeArray + chargingDCs
            }
            
            if let content = data.full_business_hours {
                self.fullBusinessHours = content.components(separatedBy: ",")
                print("fullBusinessHours: \n\(content)\n\(self.fullBusinessHours)\n")
                if self.fullBusinessHours[0] == "" {
                    self.fullBusinessHours = []
                }
            }
           
            self.view.layoutIfNeeded()
            self.view.setNeedsLayout()
            self.tableView.reloadData()
            
        }
    }

    func businessDetailBuild() {
        self.infoTitleArray.append("營業時間：")
        self.infoDataArray.append(self.detail?.businessHours ?? "")     //"營業狀態："
        self.infoTitleArray.append("營業狀態：")
        self.infoDataArray.append("")                                   //"營業狀態："
        self.infoTitleArray.append("地址：")
        self.infoDataArray.append(self.detail?.stationAddress ?? "")    //"地址："
        self.infoTitleArray.append("車位：")
        self.infoDataArray.append(self.detail?.carSpace ?? "")          //"汽車車位："
        self.infoTitleArray.append("車位：")
        self.infoDataArray.append(self.detail?.motorSpace ?? "")        //"機車車位："
        
        self.infoTitleArray.append("充電樁樓層：")
        self.infoDataArray.append(self.detail?.spotFloor ?? "")         //"充電樁樓層："
        self.infoTitleArray.append("充電費：")
        self.infoDataArray.append(self.detail?.chargingFee ?? "")       //"充電費："
        self.infoTitleArray.append("啟動方式：")
        self.infoDataArray.append(self.detail?.ocppType ?? "")          //"啟動方式："
        
        self.infoTitleArray.append("停車場空位數：")
        self.infoDataArray.append("")                                   //"停車場空位數："
        self.infoTitleArray.append("停車費：")
        self.infoDataArray.append(self.detail?.parkingFee ?? "")        //"停車費："
        self.infoTitleArray.append("建築規格")
        self.infoDataArray.append("")                                   //"建築規格"
        self.infoTitleArray.append("預約費：")
        self.infoDataArray.append("")                                   //"預約費："
        self.infoTitleArray.append("電話：")
        self.infoDataArray.append(self.detail?.stationPhone ?? "")      //"電話："
        self.tableView.reloadData()
    }
    
    private func callMajiDetailById(majiId: String, majiToken: String) {

        print("majiIdmajiIdmajiId = \(majiId)")
        print("majiTokenmajiTokenmajiToken = \(majiToken)")
        if majiId == "" || majiToken == "" {
            self.businessDetailBuild()
            return
        }
        
        //        if (majiId != nil && majiToken != nil) {
            KJLRequest.requestForStationDetailByMaji(majiId: majiId, majiToken: majiToken) { (response) in
                //self.infoTitleArray = ["營業狀態：","營業狀態：","地址：","車位：","車位：","啟動方式：","充電樁樓層：","充電費：","停車場空位數：","停車費：","建築規格","預約費：","電話："]
                //majiId或majiToken其一為空則全部顯示""
                guard let majiData = response as? MajiStationDetailClass else {
                    self.businessDetailBuild()
                    return
                }
                self.majiDetail = majiData
                //print("majiData: \(majiData.data![0])")
                
                //陣列中的"營業狀態"
//                if (self.majiDetail?.data?[0].current_business_hours != nil) {
//                    self.infoDataArray.append(self.majiDetail?.data?[0].current_business_hours ?? "")
//                    self.infoTitleArray.append("營業狀態：")
//                    self.infoTitleArray.append("營業狀態：")
//                    if (self.majiDetail?.data?[0].current_business_hours == self.majiDetail?.data?[0].full_business_hours) && (self.majiDetail?.data?[0].current_business_hours != nil) && (self.majiDetail?.data?[0].full_business_hours != nil){
//                        self.infoDataArray.append("")
//                    } else {
//                        self.infoDataArray.append(self.majiDetail?.data?[0].full_business_hours ?? "")
//                    }
//                }
                
                if self.detail?.businessHours != "" {
                    self.infoTitleArray.append("營業時間：")
                    self.infoTitleArray.append("營業狀態：")
                    self.infoDataArray.append(self.detail?.businessHours ?? "")
                    self.infoDataArray.append("")
                } else {
                    self.infoTitleArray.remove(at: 0)
                    self.infoDataArray.remove(at: 0)
                    self.infoTitleArray.remove(at: 1)
                    self.infoDataArray.remove(at: 1)
                }
                
                //陣列中的"地址"
                if (self.majiDetail?.data?[0].address != nil) {
                    self.infoDataArray.append(self.majiDetail?.data?[0].address ?? "")
                    self.infoTitleArray.append("地址：")
                } else {
                    self.infoTitleArray.append("地址：")
                    self.infoDataArray.append("")
                }
                
                //陣列中的"車位" for car
                print("carSpace: \(self.detail?.carSpace)")
                if (self.detail?.carSpace != nil) {
                    self.infoDataArray.append(self.detail?.carSpace ?? "")
                    self.infoTitleArray.append("車位：")
                } else {
                    self.infoDataArray.append("")
                    self.infoTitleArray.append("車位：")
                }
                
                //陣列中的"車位" for motor
                print("motorSpace: \(self.detail?.motorSpace)")
                if (self.detail?.motorSpace != nil) {
                    self.infoDataArray.append(self.detail?.motorSpace ?? "")
                    self.infoTitleArray.append("車位：")
                } else {
                    self.infoDataArray.append("")
                    self.infoTitleArray.append("車位：")
                }
                
                //陣列中的"啟動方式"
                if(self.detail?.ocppType != nil){
                    self.infoDataArray.append(self.detail?.ocppType ?? "")
                    self.infoTitleArray.append("啟動方式：")
                } else {
                    self.infoDataArray.append("")
                    self.infoTitleArray.append("啟動方式：")
                }
                
                //陣列中的"充電樁樓層"
                if (self.detail?.spotFloor != nil) {
                    self.infoDataArray.append(self.detail?.spotFloor ?? "")
                    self.infoTitleArray.append("充電樁樓層：")
                } else {
                    self.infoDataArray.append("")
                    self.infoTitleArray.append("充電樁樓層：")
                }
                
                //陣列中的"充電費"
                if (self.detail?.chargingFee != nil) {
                    self.infoDataArray.append(self.detail?.chargingFee ?? "")
                    self.infoTitleArray.append("充電費：")
                } else {
                    self.infoDataArray.append("")
                    self.infoTitleArray.append("充電費：")
                }
                
                //陣列中的"停車場空位數："
                if (self.majiDetail?.data?[0].extra_info?.total_lots != nil && self.majiDetail?.data?[0].extra_info?.available_lots != nil) {
                    self.infoDataArray.append(String(self.majiDetail?.data?[0].extra_info?.available_lots ?? 0) + " • 總車位：" + String(self.majiDetail?.data?[0].extra_info?.total_lots ?? 0))
                    self.infoTitleArray.append("停車場空位數：")
                } else if (self.majiDetail?.data?[0].extra_info?.total_lots != nil && self.majiDetail?.data?[0].extra_info?.available_lots == nil) {
                    self.infoDataArray.append(String(self.majiDetail?.data?[0].extra_info?.total_lots ?? 0))
                    self.infoTitleArray.append("停車總位數：")
                } else {
                    self.infoDataArray.append("--  • 總車位： --")
                    self.infoTitleArray.append("停車場空位數：")
                }

                //"停車費："
                if (self.majiDetail?.data?[0].extra_info?.current_parking_price != nil) {
                    self.infoDataArray.append(self.majiDetail?.data?[0].extra_info?.current_parking_price ?? (self.majiDetail?.data?[0].extra_info?.full_parking_price ?? ""))
                    self.infoTitleArray.append("停車費：")
                } else {
                    self.infoTitleArray.append("停車費：")
                    self.infoDataArray.append("")
                }
                
//                self.infoDataArray.append((self.majiDetail?.data?[0].extra_info?.full_parking_price_array?[0])!)
                
                
                //                guard let majiFullParkingPrice1 = majiData.data?[0].extra_info?.full_parking_price_array?[0] else {return}
                //                 guard let majiFullParkingPrice2 = majiData.data?[0].extra_info?.full_parking_price_array?[1] else {return}
                //                 guard let majiFullParkingPrice3 = majiData.data?[0].extra_info?.full_parking_price_array?[2] else {return}
                //                self.infoDataArray.append(majiData.data?[0].extra_info?.full_parking_price != nil ? "\(majiFullParkingPrice1)\n\(majiFullParkingPrice2)\n\(majiFullParkingPrice3)" : (majiData.data?[0].extra_info?.current_parking_price ?? ""))
                //車麻吉 支援麻吉付
                
                
                
                //充電開始跟結束是讀哪個欄位
                

//                guard let cooperationState = self.majiDetail?.data?[0].cooperation_state else {return}
//                let payString = "[支援麻吉付]"
//
//                if cooperationState == "in_cooperated"{
//                    self.infoDataArray.append(payString)
//                }else {
//                    self.infoDataArray.append("")
//                }
                //陣列中"建築規格"
                if (self.majiDetail?.data?[0].extra_info?.building != nil) {
                    self.infoDataArray.append(self.majiDetail?.data?[0].extra_info?.building ?? "")
                    self.infoTitleArray.append("建築規格")
                } else {
                    self.infoDataArray.append("")
                    self.infoTitleArray.append("建築規格")
                }
                
                //陣列中"預約費“
                if(self.detail?.reserveFee != nil) {
                    self.infoDataArray.append(self.detail?.reserveFee ?? "")
                    self.infoTitleArray.append("預約費：")
                } else {
                    self.infoDataArray.append("")
                    self.infoTitleArray.append("預約費：")
                }
                
                //"電話："
                if (self.detail?.stationPhone != nil) {
                    self.infoDataArray.append(self.detail?.stationPhone ?? "")
                    self.infoTitleArray.append("電話：")
                } else {
                    self.infoTitleArray.append("電話：")
                    self.infoDataArray.append("")
                }
                
                print("infoDataArray: \(self.infoDataArray)")
                guard let cooperationState = self.majiDetail?.data?[0].cooperation_state else {return}
                let payString = "支援麻吉付"
            
                if cooperationState == "in_cooperated"{
                    self.majiPayString = payString
                }
                self.tableView.reloadData()
            }
    }
    
    /// 308: 查詢充電回報
    private func callAPIRequestForChargingReportList() {
        //        stationNo = "1002u" /* 測試API 的代碼 */
        KJLRequest.requestForChargingReportList(stationId: stationNo) { (response) in
            guard let response = response as? ChargingReportListClass else {
                self.showMessageCount = 0
                self.isShowMoreBtnHidden = true
                return
            }
            
            if let data = response.datas {
                if (data.count <= 3) {
                    self.showMessageCount = data.count
                    self.isShowMoreBtnHidden = true
                } else {
                    self.showMessageCount = 3
                    self.isShowMoreBtnHidden = false
                }
                self.evaluationDataArray = data
            }
            self.tableView.reloadData()
        }
    }
    
    /// 310: 近七天充電次數統計
    private func callAPIRequestForSevenDaysCharge() {
        KJLRequest.requestForSevenDaysCharge(stationId: stationNo) { (response) in
            guard let response = response as? SevenDaysChargeClass
                else { return }
            
            if let dataArray = response.datas {
                for i in 0..<dataArray.count {
                    let data = dataArray[i]
                    if let chargeDate = data.chargeDate {
                        self.sevenChargeDateArray.append(chargeDate)
                    }
                    
                    if let chargeTotal = data.chargeTotal {
                        self.sevenChargeTotalArray.append(Double(chargeTotal))
                    }
                }
            }
            self.tableView.reloadData()
        }
    }
    
    /// 801: 預約準備
    private func callAPIForRequestReservePrepare() {
        KJLRequest.requestForReservePrepare(stationId: stationNo) { (response) in
            guard let response = response as? ReservePrepareClass,
                let data = response.datas
                else { return }
            
            self.reservePrepareData = data
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReserveChargingViewController") as? ReserveChargingViewController {
                vc.setupStationId(stationId: self.stationNo , reservePrepareData: self.reservePrepareData!)
                self.navigationController?.show(vc, sender: self)
            }
        }
    }
}

// MARK: - TableView DataSource
extension ChargingStationDetailViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
         return 11
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 1) {      // 資訊
            return self.infoTitleArray.count
        } else if (section == 6) {      // 充電評價
            return (self.evaluationDataArray.count == 0) ? 0 : 1;
        } else if (section == 7) {       // 充電列表
            if (showMessageCount >= self.evaluationDataArray.count ) {
                return self.evaluationDataArray.count
            } else  {
                return showMessageCount
            }
        } else {
            return 1
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // 標題
        if (indexPath.section == 0) {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "StationTitleCell", for: indexPath) as? StationTitleCell { 
                cell.detail = self.detail
                cell.setupCellWith(titleDataDicArray: self.titleDataDicArray ,acDcDataDicArray: self.acDcDataDicArray, delegate: self)
                return cell
            }
        } else if (indexPath.section == 1) { // 資訊
            if let cell = tableView.dequeueReusableCell(withIdentifier: "StationInfoCell", for: indexPath) as? StationInfoCell {
//                cell.setupCellWith(imageString: hintImageStringArray[indexPath.row] , titleString: infoTitleArray[indexPath.row] , infoDataString: infoDataArray[indexPath.row], majiPayString: majiPayString)
                cell.setupCellWith(imageString: "", titleString: infoTitleArray[indexPath.row] , infoDataString: infoDataArray[indexPath.row], majiPayString: majiPayString)
                return cell
            }
        } else if (indexPath.section == 2) { // 備註
            if let cell = tableView.dequeueReusableCell(withIdentifier: "RemarkTableViewCell", for: indexPath) as? RemarkTableViewCell {
                cell.clipsToBounds = true
                if let detail = self.detail,let note = detail.note1 { 
                    print("備註：\(note.replace(target: "\\n", withString: "\n"))")
                    cell.remarkLabel.text = note.replace(target: "\\n", withString: "\n")
                }
                return cell
            }
        } else if (indexPath.section == 3) { // 充電樁列表
            if let cell = tableView.dequeueReusableCell(withIdentifier: "StationChargeGunCell", for: indexPath) as? StationChargeGunCell {
                cell.setupCellWith(chargeTypeArray: self.chargingTypeArray, delegate: self)
                return cell
            }
        } else if (indexPath.section == 4) {  // 照片 - 標題
            if let cell = Bundle.main.loadNibNamed("StationTableHeaderCell", owner: self, options: nil)?[0] as? StationTableHeaderCell {
                cell.setupCellWith(headerString: "照片")
                return cell
            }
        } else if (indexPath.section == 5) { // 照片
            if let cell = tableView.dequeueReusableCell(withIdentifier: "StationPhotoCell", for: indexPath) as? StationPhotoCell {
                cell.setupCellWith(picDataArray: self.picArray , delegate: self)
                return cell
            }
        } else if (indexPath.section == 6) { // 充電評價 - 標題
            if (self.evaluationDataArray.count != 0) {
                if let cell = Bundle.main.loadNibNamed("StationTableHeaderCell", owner: self, options: nil)?[0] as? StationTableHeaderCell {
                    cell.setupCellWith(headerString: "充電評價")
                    return cell
                }
            }
        } else if (indexPath.section == 7) { // 充電評價
            if (self.evaluationDataArray.count != 0) {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "StationEvaluationCell", for: indexPath) as? StationEvaluationCell {
                    cell.setupCellWith(evaluationData: self.evaluationDataArray[indexPath.row])
                    return cell
                }
            }
        } else if (indexPath.section == 8) { // 顯示更多留言
            if let cell = tableView.dequeueReusableCell(withIdentifier: "StationShowMoreCell", for: indexPath) as? StationShowMoreCell {
//                cell.setupCellWith(isShowMoreBtnHidden: self.isShowMoreBtnHidden , delegate: self)
                cell.setupCellWith(isShowMoreBtnHidden: true , delegate: self)
                return cell
            }
        } else if (indexPath.section == 9) { // 充電次數走勢 - 標題
            if let cell = Bundle.main.loadNibNamed("StationTableHeaderCell", owner: self, options: nil)?[0] as? StationTableHeaderCell {
                cell.setupCellWith(headerString: "充電次數走勢")
                return cell
            }
        } else if (indexPath.section == 10) {  // Charts View
            if let cell = Bundle.main.loadNibNamed("ChargingChartsCell", owner: self, options: nil)?[0] as? ChargingChartsCell {
                
//                var dateArray = [Double]()
//                for _ in 0..<7
//                {
//                    let y = arc4random()%100
//                    dateArray.append(Double(y))
//                }
//                cell.setChartViewData(dataArray: dateArray)
                
//                cell.setChartViewData(dataArray: self.severChargeTotalArray)
                cell.setChartViewData(dataArray: self.sevenChargeTotalArray)
                return cell
            }
        }
        return UITableViewCell(style: .default, reuseIdentifier: "")
    }
}


// MARK: - TableView Delegate
extension ChargingStationDetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat = 0
        if (indexPath.section == 0) {              // 標題
            if (self.isShowOneCategory == true) {
                height = 370
            } else {
                height = 370
            }
        } else if (indexPath.section == 1) {       // 資訊
            switch infoTitleArray[indexPath.row] {
            case "充電費：":
                height = (infoDataArray[indexPath.row].count == 0) ? 0 : 60
            case "停車費：":
                if majiPayString == "支援麻吉付" {
                    height = (infoDataArray[indexPath.row].count == 0) ? 0 : 60
                } else {
                    height = (infoDataArray[indexPath.row].count == 0) ? 0 : 40
                }
            default:
                height = (infoDataArray[indexPath.row].count == 0) ? 0 : 40
            }
        } else if (indexPath.section == 2) {      // 充電樁列表
            if let detail = self.detail,let note = detail.note1,(note != "") {
                height = UITableViewAutomaticDimension
            }else{
                height = 0
            }
        } else if (indexPath.section == 3) {      // 充電樁列表
            height = 180
        } else if (indexPath.section == 4) {      // 照片 - 標題
            height = (self.picArray.count == 0) ? 0 : 50
        } else if (indexPath.section == 5) {      // 照片
            height = (self.picArray.count == 0) ? 0 : 180
        } else if (indexPath.section == 6) {      // 充電評價 - 標題
            height = (self.evaluationDataArray.count == 0) ? 0 : 50
        } else if (indexPath.section == 7) {      // 充電評價
            height = (self.evaluationDataArray.count == 0) ? 0 : UITableViewAutomaticDimension
        } else if (indexPath.section == 8) {      // 顯示更多留言
            height = (self.evaluationDataArray.count == 0) ? 0 : 150
            if (showMessageCount >= self.evaluationDataArray.count ) {
                height = 10
            }
        } else if (indexPath.section == 9) {      // 充電次數走勢 - 標題
            height = 50
        } else if (indexPath.section == 10) {      // 充電次數圖表
            height = 200
        }
        return height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath.section == 1) {
            if (infoTitleArray[indexPath.row] == "電話：") {
                if (infoDataArray[indexPath.row].count != 0) {
                    var number = infoDataArray[indexPath.row]
                    // 電話號碼不可包含空白字串，會閃退
                    number = number.replacingOccurrences(of: " ", with: "-")
                    let phoneUrl = URL(string: "tel://\(number)")
                    UIApplication.shared.open(phoneUrl!, options: [:], completionHandler: nil)
                }
            } else if (infoTitleArray[indexPath.row] == "營業時間：") {
                if fullBusinessHours.count > 0 {
                    let vc = BusinessHoursViewController()
                    vc.contentList = fullBusinessHours
                    guard let cell = tableView.cellForRow(at: indexPath) else { return }
                    popover(pop: vc, size: CGSize(width: 200, height: CGFloat(fullBusinessHours.count) * vc.rowHeight), arch: cell, Direction: .up)
                }
            } else if (infoTitleArray[indexPath.row] == "地址：") {
                self.showPopoverMessage(indexPath: indexPath)
            }
        }
        if (indexPath.section == 9){
            let cell = tableView.cellForRow(at: indexPath)
            cell?.selectionStyle = .none
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        if (section == 1) {
//            let view = UIView(frame: CGRect(x: 0 , y: 0, width: tableView.bounds.width, height: 11))
//            view.backgroundColor = UIColor.white
//            let imageVivew = UIImageView(frame: CGRect(x: tableView.bounds.size.width - 135, y: 2, width: 120, height: 11))
//            imageVivew.contentMode = .scaleAspectFill
//            imageVivew.image = UIImage(named: "home_img_word_02.png")
//            view.addSubview(imageVivew)
//            return view
//        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return (section == 1) ? 15 : 0.1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
}

// MARK: - 標題 Delegate
extension ChargingStationDetailViewController: StationTitleCellDelegate {
    // 導航
    func naviAction() {
        KJLCommon.openNavMap(lat: Double(detail?.latitude ?? ""), long: Double(detail?.longitude ?? ""), subControl: self)
    }
    // 充電回報
    func chargeReplyAction() {
        guard KJLRequest.isLogin() == true else {
            KJLCommon.showLogin(subcontrol: self)
            return
        }
        
        guard let no = detail?.stationId else {
            KJLCommon.showAlert(title: nil, message: "no station no")
            return
        }
        if let vc = storyboard?.instantiateViewController(withIdentifier: "ChargingReportViewController") as? ChargingReportViewController {
            vc.stationNo = no
            navigationController?.show(vc, sender: self)
        }
    }
    // 預約充電
    func reserveChargeAction() {
        guard (KJLRequest.isLogin() == true) else {
            ShowMessageView.showQuestionWithCustomButton(messageTitle: "請登入帳號", message: "登入會員，享有充電站預約功能", sureBtnTitle: "前往登入", cancelBtnTitle: "取消") { (isOK) in
                if isOK == true {
                    if let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectLoginViewController") as? SelectLoginViewController {
                        vc.hidesBottomBarWhenPushed = true
                        self.navigationController?.show(vc, sender: self)
                    }
                }
            }
            return
        }
        callAPIForRequestReservePrepare()
    }
}

// MARK: - 充電槍 Delegate
extension ChargingStationDetailViewController: StationChargeGunCellDelegate {
    func chargeGunCollectionCellDidSelect(indexPath: IndexPath) {
        var msg = self.chargingTypeArray[indexPath.item]//SAE J1772
        if msg == "SAE J1772" {
            msg = "SAEJ1772\nCNS15511\nIEC62196-2"
        }
        ShowMessageView.showMessage(title: nil, message: msg)
    }
    
    func chargingListAction() {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "ChargingPileListViewController") as? ChargingPileListViewController {
            guard let no = detail?.stationId else {
                KJLCommon.showAlert(title: nil, message: "no station no")
                return
            }
            vc.stationId = no
            vc.titleStr = self.detail?.stationName ?? "" 
            navigationController?.show(vc, sender: self)
        }
    }
}


// MARK: - 照片 Delegate
extension ChargingStationDetailViewController: StationPhotoCellDelegate {
    func photoCollectionCellDidSelect(indexPath: IndexPath) {
        if let vw = Bundle.main.loadNibNamed("PhotoView", owner: self, options: nil)?.first as? PhotoView {
            vw.frame = self.view.bounds
            self.view.addSubview(vw)
            vw.changeData(arr: picArray, index: indexPath.item)
        }
    }
}

// MARK: - 顯示更多 Delegate
extension ChargingStationDetailViewController: StationShowMoreCellDelegate {
    func showMoreAction() {
        self.showMessageCount += 5
        if (showMessageCount >= self.evaluationDataArray.count) {
            showMessageCount = self.evaluationDataArray.count
            self.isShowMoreBtnHidden = true
        }
        self.tableView.reloadData()
    }
}
 

// MARK: - KJLRequest Delegate
extension ChargingStationDetailViewController: KJLRequestDelegate {
    func apiFailResultWith(resCode: String , message: String) {
        ShowMessageView.showQuestionWithCustomButton(messageTitle: "", message: message, sureBtnTitle: "前往預約記錄", cancelBtnTitle: "取消") { (isOK) in
            if(isOK == true) {
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReserveRecordViewController") as? ReserveRecordViewController {
                    self.navigationController?.show(vc, sender: self)
                }
            }
        }
    }
}

