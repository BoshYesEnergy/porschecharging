//
//  StationEvaluationCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class StationEvaluationCell: UITableViewCell {

    @IBOutlet weak var headshotImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var chargeStatusImage: UIImageView!
    @IBOutlet weak var chargeStatusLabel: UILabel!
    @IBOutlet weak var evaMessageLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        makeImageCornerRadius(imageView: headshotImage)
        self.nameLabel.text = ""
        self.evaMessageLabel.text = ""
        self.chargeStatusLabel.text = ""
        self.timeLabel.text = ""
    }
    
    func setupCellWith(evaluationData: ChargingReportListClass.Data) {
        self.nameLabel.text = evaluationData.nickname
        self.timeLabel.text = evaluationData.reportTime
        self.evaMessageLabel.text = evaluationData.comment
        let headshotUrl = URL(string: evaluationData.picUrl ?? "")
        self.headshotImage.kf.setImage(with: headshotUrl, placeholder: nil, options: nil, progressBlock: nil) { (image, error, cacheType, url) in
            if image == nil {
                self.headshotImage.image = UIImage(named: "home_ic_person")
            }else {
                self.headshotImage.image = image
            }
        }
        switch (evaluationData.reportType) {
        case "OK"?:
            self.chargeStatusLabel.text = "充電成功"
            self.chargeStatusLabel.textColor = #colorLiteral(red: 0.1490196078, green: 0.7294117647, blue: 0.7568627451, alpha: 1)
            self.chargeStatusImage.image = UIImage(named: "charge_ic_success")
            break
        case "Fail"?:
            self.chargeStatusLabel.text = "充電失敗"
            self.chargeStatusLabel.textColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
            self.chargeStatusImage.image = UIImage(named: "charge_ic_failure")
            break
        case "Charging"?:
            self.chargeStatusLabel.text = "充電中"
            self.chargeStatusLabel.textColor = #colorLiteral(red: 0.1490196078, green: 0.7294117647, blue: 0.7568627451, alpha: 1)
            self.chargeStatusImage.image = UIImage(named: "charge_ic_charging")
            break
        case "Other"?:
            self.chargeStatusLabel.text = "其他"
            self.chargeStatusLabel.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            self.chargeStatusImage.image = UIImage(named: "charge_ic_other")
            break
        default:
            break
        }
    }
    
    private func makeImageCornerRadius(imageView: UIImageView) {
        imageView.layer.cornerRadius = imageView.bounds.width / 2
        imageView.layer.masksToBounds = true
    }
}
