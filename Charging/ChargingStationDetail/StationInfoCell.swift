//
//  StationInfoCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class StationInfoCell: UITableViewCell {

    @IBOutlet weak var hintImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var downImage: UIImageView! 
    
    override func awakeFromNib() {
        super.awakeFromNib()
          
        self.titleLabel.text = ""
        self.infoLabel.text = ""
        self.titleLabel.textAlignment = .left
        self.infoLabel.textAlignment = .left
        
        infoLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        infoLabel.numberOfLines = 0
         
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.infoLabel.textColor = UIColor.black
    }

    func setupCellWith(imageString: String , titleString: String , infoDataString: String, majiPayString: String) {
        print("titleString: \(titleString), infoDataString: \(infoDataString)")

        self.hintImage.image = UIImage(named: imageString)

        self.titleLabel.text = titleString
        self.infoLabel.text = infoDataString
        downImage.image = UIImage(named: "")
        
        if titleString == "營業時間：" {
            self.titleLabel.text = ""
            downImage.image = UIImage(named: "ic_expand_more")
        }
        
        if (titleString == "營業狀態：") {
            self.titleLabel.text = ""
            if (infoDataString == "營業中") {
                self.infoLabel.textColor = #colorLiteral(red: 0.5450980392, green: 0.8078431373, blue: 0.5137254902, alpha: 1)
            } else {
                self.infoLabel.textColor = #colorLiteral(red: 0.6039215686, green: 0.6039215686, blue: 0.6039215686, alpha: 1)
            }
        } else if (titleString == "地址：") {
            self.titleLabel.text = ""
            self.infoLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            
        } else if (titleString == "電話：") {
            self.titleLabel.text = ""
            self.infoLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        } else {
            self.infoLabel.textColor = UIColor.black
        }
        
        if (titleString == "充電費：") {
            
        } else {
            
        }
        
        if(majiPayString == "支援麻吉付") {
            if (titleString == "停車費：") {
                
            } else {
                
            }
        }
        
        if(titleString == "建築規格"){
            self.titleLabel.text = ""
        }
        
        func changeSomeTextColor(text: String, inText result: String, color: UIColor) -> NSAttributedString {
            let attributeStr = NSMutableAttributedString(string: result)
            let colorRange = NSMakeRange(attributeStr.mutableString.range(of: text).location, attributeStr.mutableString.range(of: text).length)
            attributeStr.addAttribute(NSAttributedStringKey.foregroundColor, value:color , range: colorRange)
            return attributeStr
        }

        //        else if (titleString == "啟動方式：") {
        //            self.infoLabel.textColor = #colorLiteral(red: 0.5803921569, green: 0.6274509804, blue: 0.9647058824, alpha: 1)
        //        } else if (titleString == "充電樁樓層：") {
        //            self.infoLabel.textColor = #colorLiteral(red: 0.5803921569, green: 0.6274509804, blue: 0.9647058824, alpha: 1)
        //        }
    }
}

