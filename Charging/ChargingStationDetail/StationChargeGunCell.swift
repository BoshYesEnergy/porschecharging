//
//  StationChargeGunCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

protocol StationChargeGunCellDelegate: class {
    func chargeGunCollectionCellDidSelect(indexPath: IndexPath)
    func chargingListAction()
}

class StationChargeGunCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var chargingListBtn: UIButton!
    @IBOutlet weak var chargeCollectionLeft: NSLayoutConstraint!
    @IBOutlet weak var chargeCollectionRight: NSLayoutConstraint!
    
    private var chargeTypeArray: [String] = []
    weak var stationChargeGunCellDelegate: StationChargeGunCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.collectionView.dataSource = self
        self.collectionView.delegate = self

//        chargingListBtn.setBackgroundImage(UIImage(named: "charge_btn_cyan_normal"), for: .normal)
//        chargingListBtn.setBackgroundImage(UIImage(named: "charge_btn_cyan_pressed"), for: .highlighted)
    }

    func setupCellWith(chargeTypeArray: [String] , delegate: StationChargeGunCellDelegate) {
        self.chargeTypeArray = chargeTypeArray
//        let cellWidth: CGFloat = 45
//        let count = CGFloat(self.chargeTypeArray.count)
//        let cellGap: CGFloat = ((UIScreen.main.bounds.width - (cellWidth * count))) / (count + 1)
        //self.chargeCollectionLeft.constant = -UIScreen.main.bounds.width
//        self.chargeCollectionLeft.constant = (UIScreen.main.bounds.width / 2) - (UIScreen.main.bounds.width - (cellWidth * count) - (cellGap * 2) - ((count - 1) * 14)) / 2
        
        self.contentView.layoutIfNeeded()
        self.collectionView.reloadData()
        self.stationChargeGunCellDelegate = delegate
    }
    
    @IBAction func chargingListAction(_ sender: UIButton) {
        self.stationChargeGunCellDelegate?.chargingListAction()
    }
}

extension StationChargeGunCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("chargeTypeArraychargeTypeArray =\(chargeTypeArray)")
        return self.chargeTypeArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StationImageCollectionCell", for: indexPath) as? StationImageCollectionCell {
            cell.setupChargeGunCellWith(name: self.chargeTypeArray[indexPath.item])
            
            return cell
        }
        return UICollectionViewCell()
    }
}

extension StationChargeGunCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.stationChargeGunCellDelegate?.chargeGunCollectionCellDidSelect(indexPath: indexPath)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 14
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let totalCellWidth = 45 * collectionView.numberOfItems(inSection: 0)
        let totalSpacingWidth = 14 * (collectionView.numberOfItems(inSection: 0) - 1)
        
        let leftInset = (collectionView.layer.frame.size.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset
        
        return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
        
    }
}





