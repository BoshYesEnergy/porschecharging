//
//  StationAcDcCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class StationAcDcCell: UITableViewCell {

    @IBOutlet weak var acEmptyLabel: UILabel!
    @IBOutlet weak var acTotalLabel: UILabel!
    @IBOutlet weak var dcEmptyLabel: UILabel!
    @IBOutlet weak var dcTotalLabel: UILabel!
    
    @IBOutlet weak var acView: UIView!
    @IBOutlet weak var dcView: UIView!
    
    @IBOutlet weak var acViewWidth: NSLayoutConstraint!
    @IBOutlet weak var dcViewWidth: NSLayoutConstraint!
    @IBOutlet weak var acViewLeading: NSLayoutConstraint!
    @IBOutlet weak var dcViewLeading: NSLayoutConstraint!
    @IBOutlet weak var categoryViewTrailing: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.acEmptyLabel.text = ""
        self.acTotalLabel.text = ""
        self.dcEmptyLabel.text = ""
        self.dcTotalLabel.text = ""
    }
    
    func setupCellWith(acDcDicArray: [String: String]) {
        self.acEmptyLabel.text = "空位 \(acDcDicArray["AcEmpty"] ?? "0")"
        self.acTotalLabel.text = "總數 \(acDcDicArray["AcTotal"] ?? "0")"
        self.dcEmptyLabel.text = "空位 \(acDcDicArray["DcEmpty"] ?? "0")"
        self.dcTotalLabel.text = "總數 \(acDcDicArray["DcTotal"] ?? "0")"
        
        if (acDcDicArray["AcEmpty"] == "03") {
            self.acEmptyLabel.text = "空位 --"
            self.dcEmptyLabel.text = "空位 --"
        }
        
        let hasAC = (acDcDicArray["HasAC"] == "true") ? true : false
        let hasDC = (acDcDicArray["HasDC"] == "true") ? true : false
        let halfFrameWidth = self.frame.width / 2
        
        if hasAC == true, hasDC == false {
            self.acView.isHidden = false
            self.dcView.isHidden = true
            self.acViewLeading.constant = 0
            self.dcViewLeading.constant = 0
            self.acViewWidth.constant = halfFrameWidth
            self.dcViewWidth.constant = halfFrameWidth
            self.categoryViewTrailing.constant = 0
        } else if hasAC == false, hasDC == true {
            self.acView.isHidden = true
            self.dcView.isHidden = false
            self.acViewLeading.constant = 0
            self.dcViewLeading.constant = -halfFrameWidth
            self.acViewWidth.constant = halfFrameWidth
            self.dcViewWidth.constant = halfFrameWidth
            self.categoryViewTrailing.constant = halfFrameWidth
        } else {
            self.acView.isHidden = false
            self.dcView.isHidden = false
            self.acViewLeading.constant = 0
            self.dcViewLeading.constant = 0
            self.acViewWidth.constant = halfFrameWidth
            self.dcViewWidth.constant = halfFrameWidth
            self.categoryViewTrailing.constant = 0
        }
    }
}
