//
//  ReadyChargingViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class ReadyChargingViewController: BaseViewController {
    
    // MARK: - IBOutlet
    @IBOutlet weak var failView: UIView!
    @IBOutlet weak var nameLab: UILabel!
    @IBOutlet weak var codeLab: UILabel!
    @IBOutlet weak var scoketTypeLab: UILabel! 
    @IBOutlet weak var creditCardLab: UILabel!
    @IBOutlet weak var expireLab: UILabel!
    @IBOutlet weak var pieImage: UIImageView!
    //手機條碼
    @IBOutlet weak var phoneBarcodeLab: UILabel!
    // 開始充電
    @IBOutlet weak var oneBtnView: UIView!
    @IBOutlet weak var rechargeBtn: UIButton!
    @IBOutlet weak var hundredLabel: UILabel!
    // 優惠畫面
    @IBOutlet weak var discountContentTitle: UILabel!
    @IBOutlet weak var discountDateTitle: UILabel!
    @IBOutlet weak var discount: UILabel!
    @IBOutlet weak var discountContent: UILabel!
    @IBOutlet weak var discountDate: UILabel!
    @IBOutlet weak var disconutViewHeight: NSLayoutConstraint!
    @IBOutlet weak var chargePercentViewHeight: NSLayoutConstraint!
    
    // MARK: - Property
    var creditCards: [ChargerPairingModel.Data.CardList] = []
    var selectCardNo: Int?
    var data: ChargerPairingModel?
    var elecPercentage: String = ""
    var qrNo = ""
    
    // MARK: - 生命週期
    override func viewDidLoad() {
        super.viewDidLoad()
        setNav()
        settingView()
        setDiscountView(data?.datas?.discount ?? false)
        callAPIForRequestInvoiceSettingList()
        settingData()
        NotificationCenter.default.addObserver(self, selector: #selector(callApiForBack), name: .startChargError, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
}


//MARK:- Setting View
extension ReadyChargingViewController {
    private func setNav() {
        setHeaderView("", action: .DISMISS)
    }
    
    private func settingView() {
        hundredLabel.text = "80% (建議充電量)"
        self.elecPercentage = "80"
        self.chargePercentViewHeight.constant = 0
        nameLab.text = ""
        codeLab.text = ""
        scoketTypeLab.text = ""
        creditCardLab.text = ""
        phoneBarcodeLab.text = ""
        discount.text = "本站目前有優惠活動！"
        self.failView.isHidden = true
        data?.datas?.noneQrcode == true ? rechargeBtn.setTitle("準備充電", for: .normal) : rechargeBtn.setTitle("開始充電", for: .normal) 
    }
    
    private func setDiscountView(_ isDiscount: Bool) {
        discount.isHidden = !isDiscount
        discountContentTitle.isHidden = !isDiscount
        discountContent.isHidden = !isDiscount
        discountDateTitle.isHidden = !isDiscount
        discountDate.isHidden = !isDiscount
        disconutViewHeight.constant = isDiscount == true ? 160 : 30
    }
}


// MARK: - IBAction
extension ReadyChargingViewController {
    // 授權失敗確認按鈕
    @IBAction func failSureAction(_ sender: Any) {
        self.failView.isHidden = true
    }
    
    // 選擇信用卡
    @IBAction func selectCreditCardAction(_ sender: Any) {
        guard creditCards.count > 0 else {
            ShowMessageView.showMessage(title: nil, message: "請先設定付款方式")
            return
        }
        
        let control = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        var title = ""
        for temp in creditCards {
            print("Card Car Type : \(temp.cardType ?? "")")
            if temp.paymentType!.lowercased().contains("line") ||
                temp.paymentType!.lowercased().contains("easywallet") {
                title = "\(temp.paymentType!): " + (temp.cardName ?? "")
            } else {
                switch temp.cardType?.lowercased() {
                case "master", "jcb", "vias": title = "\(temp.cardType!): " + (temp.cardName ?? "")
                default: title = "\(temp.cardType ?? ""): " + (temp.cardName ?? "")
                }
            }
            
            let action = UIAlertAction(title: title, style: .default, handler: { (alertAction) in
                self.expireLab.text = ""
                let number = temp.cardNum ?? "****-****"
                if temp.paymentType!.lowercased().contains("line") {
                    self.creditCardLab.text = temp.paymentType!
                } else {
                    self.creditCardLab.text = number.hiddenCardCarNumber()
                }
                if temp.disable == true || temp.expireSoon == true {
                    self.expireLab.text = temp.warn!
                }
                self.selectCardNo = temp.cardNo
            })
            control.addAction(action)
        }
        let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        control.addAction(cancel)
        if control.actions.count > 1 {
            present(control, animated: true, completion: nil)
        }
    }
    
    // 開始充電
    @IBAction func startChargingAction(_ sender: Any) {
//        if data?.datas?.noneQrcode == false {
//            if UserDefaults.standard.bool(forKey: "Flameout") == false {
//                ShowImageMessage.customView(title: "請您再次確認\n是否準備好充電" , confirm: "確定") { [weak self] (status) in
//                    self?.charging()
//                }
//            } else {
//                charging()
//            }
//        } else {
            charging()
//        }
    }
    
    @IBAction func hundredBtnAction(_ sender: Any) { 
        let control = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        var action = UIAlertAction(title: "80% (建議充電量)", style: .default, handler: { [unowned self](alertAction) in
            self.elecPercentage = "80"
            self.hundredLabel.text = "80% (建議充電量)"
        })
        control.addAction(action)
        
        action = UIAlertAction(title: "95%", style: .default, handler: { (alertAction) in
            ShowMessageView.showQuestionWithCustomButton(messageTitle: "提醒", message: "為保護您的電池，充電超過80%後每分鐘充電量會自動降低，將需要負擔較多時間與費用，請問是否確定要充到95%？", sureBtnTitle: "確定", cancelBtnTitle: "取消") { [unowned self] (isOK) in
                if isOK == true {
                    self.elecPercentage = "95"
                    self.hundredLabel.text = "95%"
                }
            }
        })
        control.addAction(action)
        let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        control.addAction(cancel)
        present(control, animated: true, completion: nil)
    }
    
    
    @IBAction func gotoDescription(_ sender: UIButton) {
        
    }
    
    @IBAction func gotoRate(_ sender: UIButton) {
        
    }
}


//MARK:- Action
extension ReadyChargingViewController {
    private func settingData() {
        if let data = data {
            self.nameLab.text = data.datas?.stationName
            self.codeLab.text = "充電槍代碼：" + qrNo 
            self.scoketTypeLab.text = data.datas?.socketType
            self.expireLab.text = ""
            let name = data.datas?.socketType ?? ""
            let rename = name.replacingOccurrences(of: "/", with: ":")
            self.pieImage.image = UIImage(named: rename)
            discountDate.text = data.datas?.discountDate ?? ""
            discountContent.text = data.datas?.discountDesc ?? ""
            
            self.creditCards = data.datas?.cardList ?? []
            
            for temp in self.creditCards {
                if temp.default == true {
                    let number = temp.cardNum ?? "****-****"
                    if temp.paymentType!.lowercased().contains("line") ||
                        temp.paymentType!.lowercased().contains("easywallet"){
                        self.creditCardLab.text = temp.paymentType!
                    } else {
                        self.creditCardLab.text = number.hiddenCardCarNumber()
                    }
                    if temp.disable == true || temp.expireSoon == true {
                        self.expireLab.text = temp.warn!
                    }
                    self.selectCardNo = temp.cardNo
                    break
                }
            }
            
            if self.creditCards.count <= 0 {
                ShowMessageView.showMessage(title: nil, message: "請先設定付款方式")
            }
            if let carType = data.datas?.type {
                if (carType == "1") {   // 汽車
                    self.chargePercentViewHeight.constant = 60
                } else {    // 機車
                    self.chargePercentViewHeight.constant = 60
                }
            }
        }
    }
    
    // back
    @objc func callApiForBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    /// API-531 查詢電子發票設定
    private func callAPIForRequestInvoiceSettingList() {
        KJLRequest.requestForInvoiceSettingList { [weak self] (response) in
            guard let response = response as? ElectronicInvoiceClass
                else { return  }
            
            guard let data = response.datas
                else { return }
            
            if let taxidName = data.taxidName , let taxidNum = data.taxidNum {
                if (taxidName.count == 0 && taxidNum.count == 0 ) {
                    // 二聯式
                    if let vehicleType = data.vehicleType {
                        if (vehicleType == "02") { // 手機條碼："02"
                            if let vehicleNum = data.vehicleNum {
                                self?.phoneBarcodeLab.text = vehicleNum
                            }
                        }
                    }
                }
            }
        }
    }
    
    fileprivate func charging() {
        var cardNo = ""
        if selectCardNo != nil {
            cardNo = "\(selectCardNo!)"
        }
        
        // API-402 開始充電：Step2
        KJLRequest.requestForBeginCharging(qrNo: qrNo, cardNo: cardNo) { [weak self] (response) in
            guard let response = response as? BeginChargingModel else {
                self?.failView.isHidden = false
                return
            }
            /// 0000 成功 -- CA01 授權失敗 -- CC01 充電樁回覆失敗
//            if response.resCode == "CA01", response.resCode == "CC01" {
//                self?.failView.isHidden = false
//                self?.navigationController?.popViewController(animated: true)
//                return
//            }
            
//            if self?.data?.datas?.noneQrcode == false { /// 舊流程
//                let vc = self?.storyboard?.instantiateViewController(withIdentifier: "StartChargingViewController") as! StartChargingViewController
//                vc.tranNo = Int64(response.datas?.tranNo ?? 0)
//                vc.nameStr = self?.data?.datas?.stationName
//                vc.idStr = self?.qrNo
//                self?.navigationController?.show(vc, sender: self)
//            } else { /// 新流程:免掃QRcode
                let vc = self?.storyboard?.instantiateViewController(withIdentifier: "CheckChargingViewController") as! CheckChargingViewController
                vc.data = self?.data
                vc.qrNo = self?.qrNo ?? ""
                vc.tranNo = Int64(response.datas?.tranNo ?? 0)
            self?.navigationController?.pushViewController(vc, animated: true) 
//            }
        }
    }
}
