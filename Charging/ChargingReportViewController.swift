//
//  ChargingReportViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class ChargingReportViewController: KJLNavView {

    var names = ["充電成功", "充電失敗", "充電中", "其他"]
    var imgs = ["text_icon2", "text_icon2", "text_icon2", "text_icon2"]
    var stationNo: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        createNavViewForRightBtn(rightName: "", rightImage: "", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: UIColor.black)
        titleLab?.text = "充電評價"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

extension ChargingReportViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return names.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChargingReportCell", for: indexPath)
        
        if let cell = cell as? ChargingReportCell {
            switch indexPath.row {
            case 0:
//                cell.imgCell.image = UIImage(named: "ic_sentiment_very_satisfied")
                cell.imgCell.image = UIImage(named: "charge_ic_success")
            case 1:
//                cell.imgCell.image = UIImage(named: "ic_sentiment_very_dissatisfied")
                cell.imgCell.image = UIImage(named: "charge_ic_failure")
            case 2:
//                cell.imgCell.image = UIImage(named: "ic_sentiment_satisfied")
                cell.imgCell.image = UIImage(named: "charge_ic_charging")
            case 3:
//                cell.imgCell.image = UIImage(named: "ic_help_outline")
                cell.imgCell.image = UIImage(named: "charge_ic_other")
            default:
                break
            }
            
            cell.titleCell.text = names[indexPath.row]
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let stationNo = stationNo else {
            return
        }
        switch indexPath.row {
        case 0:
//            ShowTextMessage.show(type: .ChargingSuccess, stationNo: stationNo)
            ShowTextMessage.showReruen(type: .ChargingSuccess, stationNo: stationNo, completion: { (isOK) in
                if isOK == true {
                    self.navigationController?.popViewController(animated: true)
                }
            })
            break
        case 1:
            ShowTextMessage.showReruen(type: .ChargingFaile, stationNo: stationNo, completion: { (isOK) in
                if isOK == true {
                    self.navigationController?.popViewController(animated: true)
                }
            })
            break
        case 2:
            ShowTextMessage.showReruen(type: .Charging, stationNo: stationNo, completion: { (isOK) in
                if isOK == true {
                    self.navigationController?.popViewController(animated: true)
                }
            })
            break
        case 3:
            ShowTextMessage.showReruen(type: .Other, stationNo: stationNo, completion: { (isOK) in
                if isOK == true {
                    self.navigationController?.popViewController(animated: true)
                }
            })
            break
        default:
            break
        }
    }
}
