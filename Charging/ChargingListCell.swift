//
//  ChargingListCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class ChargingListCell: UITableViewCell {

    @IBOutlet weak var iconCell: UIImageView!
    @IBOutlet weak var scrollLab: UILabel!
    @IBOutlet weak var distanceLab: UILabel!
    @IBOutlet weak var nameLab: UILabel!
    @IBOutlet weak var acNumLab: UILabel!
    @IBOutlet weak var dcNumLab: UILabel!
    @IBOutlet weak var acViewHeight: NSLayoutConstraint!
    @IBOutlet weak var dcViewHeight: NSLayoutConstraint!
    @IBOutlet weak var bottomViewHeight: NSLayoutConstraint!
    @IBOutlet weak var scoreViewWidth: NSLayoutConstraint!
    var userLocation:CLLocation?
    @IBOutlet weak var dcViewTopSpace: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
                
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func changeData(data: StationListClass.Data?) {
        if let data = data {
            var imgName: String = AnnotationType.NONE.rawValue
            if data.hasDC == true {
                switch data.status {
                case "01": imgName = AnnotationType.DcAvailable.rawValue
                case "02": imgName = AnnotationType.DcAvailable.rawValue
                case "03": imgName = AnnotationType.DcAvailable.rawValue
                case "04": imgName = AnnotationType.DcAvailable.rawValue
                case "05": imgName = AnnotationType.DcAvailable.rawValue
                default: break
                }
//                if data.status == "01" {
//                    imgName = AnnotationType.DcDisable.rawValue
//                } else if data.status == "02" {
//                    imgName = AnnotationType.DcAvailable.rawValue
//                } else if data.status == "03" {
//                    imgName = AnnotationType.DcPorsche.rawValue
//                } else if data.status == "04" {
//                    imgName = AnnotationType.DcFavorite.rawValue
//                }
            }else {
                switch data.status {
                case "01": imgName = AnnotationType.AcAvailable.rawValue
                case "02": imgName = AnnotationType.AcAvailable.rawValue
                case "03": imgName = AnnotationType.AcAvailable.rawValue
                case "04": imgName = AnnotationType.AcAvailable.rawValue
                case "05": imgName = AnnotationType.AcAvailable.rawValue
                default: break
                }
//                if data.status == "01" {
//                    imgName = AnnotationType.AcCyan.rawValue
//                } else if data.status == "02" {
//                    imgName = AnnotationType.AcOrange.rawValue
//                } else if data.status == "03" {
//                    imgName = AnnotationType.AcBlack.rawValue
//                } else if data.status == "04" {
//                    imgName = AnnotationType.AcGray.rawValue
//                }
            }
            iconCell.image = UIImage(named: imgName)
            nameLab.text = data.stationName
            scrollLab.text = data.stationScore //== nil ? "" : "\(data.stationScore ?? 0)"
            
            if (KJLRequest.isLogin() == true) {
                let ac1 = data.acEmpty == nil ? "" : "\(data.acEmpty ?? 0)"
                let ac2 = data.acEmpty == nil ? "" : "\(data.acTotal ?? 0)"
                acNumLab.text = String("空位：" + ac1 + " / " + "總數：" + ac2)
                let dc1 = data.dcEmpty == nil ? "" : "\(data.dcEmpty ?? 0)"
                let dc2 = data.dcEmpty == nil ? "" : "\(data.dcTotal ?? 0)"
                dcNumLab.text = String("空位：" + dc1 + " / " + "總數：" + dc2)
            } else {
                let ac2 = data.acEmpty == nil ? "" : "\(data.acTotal ?? 0)"
                acNumLab.text = String("空位：請先登入 / " + "總數：" + ac2)
                let dc2 = data.dcEmpty == nil ? "" : "\(data.dcTotal ?? 0)"
                dcNumLab.text = String("空位：請先登入 / " + "總數：" + dc2)
            }
            
            if data.status == "03" {
                if KJLRequest.isLogin() == true {
                    self.acNumLab.text = "空位：-- / 總數：\(data.acTotal ?? 0)"
                    self.dcNumLab.text = "空位：-- / 總數：\(data.dcTotal ?? 0)"
                }else{
                    self.acNumLab.text = "空位：請先登入 / 總數：\(data.acTotal ?? 0)"
                    self.dcNumLab.text = "空位：請先登入 / 總數：\(data.dcTotal ?? 0)"
                }
            }
        
            let lat = Float(data.latitude ?? "0") ?? 0
            let long = Float(data.longitude ?? "0") ?? 0
            if let location = self.userLocation  {
                let current = CLLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
                let location = CLLocation(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(long))
                let distance = current.distance(from: location)
                let meter = "\(distance)"
                if let distance = Float(meter),distance > 1000.0
                {
                    let m = String(format: "%.0f", (distance/1000.0))
                    distanceLab.text = m + " km"
                }
                else
                {
                    let m = String(format: "%.0f", (Float(meter) ?? 0))
                    distanceLab.text = m + " m"
                }
            }else{
                distanceLab.text = "0 km"
            }
//            distanceLab.text = (data.myDistance ?? "") + " km"
            
            if data.hasAC == true, data.hasDC == false {
                self.acViewHeight.constant = 22
                self.dcViewHeight.constant = 0
                self.dcViewTopSpace.constant = 0
                self.bottomViewHeight.constant = 88
            } else if data.hasAC == false, data.hasDC == true {
                self.acViewHeight.constant = 0
                self.dcViewHeight.constant = 22
                self.dcViewTopSpace.constant = 0
                self.bottomViewHeight.constant = 88
            } else {
                self.acViewHeight.constant = 22
                self.dcViewHeight.constant = 22
                self.dcViewTopSpace.constant = 7
                self.bottomViewHeight.constant = 110
            }
            
            if let score = data.stationScore {
                self.scrollLab.text = score
                let scoreF = Float(score) ?? 0.0
//                self.titleNameLeft.constant = 10
//                self.lineView.isHidden = false
                scoreViewWidth.constant = 60
                if scoreF <= 0 {
                    scrollLab.text = ""
                    self.scoreViewWidth.constant = 0
//                    titleNameLeft.constant = -13
//                    lineView.isHidden = true
                }
            } else {
                scrollLab.text = ""
//                titleNameLeft.constant = -13
//                lineView.isHidden = true
            }
        } else {
            iconCell.image = UIImage(named: "")
            nameLab.text = ""
            distanceLab.text = ""
            acNumLab.text = ""
            dcNumLab.text = ""
            scrollLab.text = ""
            
//            titleNameLeft.constant = -13
//            lineView.isHidden = true
        }
        
        
    }
}
