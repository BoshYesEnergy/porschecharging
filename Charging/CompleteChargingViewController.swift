////
////  CompleteChargingViewController.swift
////  PorscheChargingApp
////
////  Created by Daisuke on 2020/8/1.
////  Copyright © 2020 Heng.Wang. All rights reserved.
////
//
//import UIKit
//
//class CompleteChargingViewController: KJLNavView {
//
//    @IBOutlet weak var mainScrollView: UIScrollView!
//    @IBOutlet weak var okImage: UIImageView!
//    @IBOutlet weak var statusLab: UILabel!
//    @IBOutlet weak var resultLab: UILabel!
//    @IBOutlet weak var transactionLab: UILabel!
//    @IBOutlet weak var nameLab: UILabel!
//    @IBOutlet weak var codeLab: UILabel!
//    @IBOutlet weak var pieImage: UIImageView!
//    @IBOutlet weak var contentLabel: UILabel!                         /// 未付款內容
//    @IBOutlet weak var payBtn: UIButton!
//
//    // section 1
//    @IBOutlet weak var sectionOneView: UIView!
//    @IBOutlet weak var reservationStartLab: UILabel!
//    @IBOutlet weak var reservationFeeLab: UILabel!
//    
//    // section 2
//    @IBOutlet weak var sectionTwoView: UIView!
//    @IBOutlet weak var startTimeLab: UILabel!
//    @IBOutlet weak var tradeTimeLab: UILabel!
//    @IBOutlet weak var totalTimeLab: UILabel!
//
//    // section 3
//    @IBOutlet weak var sectionThreeView: UIView!
//    @IBOutlet weak var applicableRateLab: UILabel!
//    @IBOutlet weak var currentRateLab: UILabel!
//    @IBOutlet weak var amountRateLab: UILabel!
//    @IBOutlet weak var amountRateTitleLab: UILabel!
//    @IBOutlet weak var remainderPointLab: UILabel!
//    @IBOutlet weak var remainderPointHeight: NSLayoutConstraint!
//
//    // section 4
//    @IBOutlet weak var sectionFourView: UIView!
//    @IBOutlet weak var extendStartLab: UILabel!
//    @IBOutlet weak var extendEndLab: UILabel!
//    @IBOutlet weak var extendAmountLab: UILabel!
//    @IBOutlet weak var extendFeeLab: UILabel!
//
//    // section 優惠
//    @IBOutlet weak var discountView: UIView!
//    @IBOutlet weak var discountTypeTitle: UILabel!
//    @IBOutlet weak var discountFeeLabel: UILabel!
//    @IBOutlet weak var discountTypeLabel: UILabel!
//
//    // section 5
//    @IBOutlet weak var sectionFiveView: UIView!
//    @IBOutlet weak var amountLab: UILabel!
//    @IBOutlet weak var authLab: UILabel!
//
//    @IBOutlet weak var amountPointLabel: UILabel!
//
//    @IBOutlet weak var logoHeight: NSLayoutConstraint!
//    @IBOutlet weak var amountHeight: NSLayoutConstraint!
//    @IBOutlet weak var actualPaymentAmountLabel: UILabel!
//    @IBOutlet weak var chargingStateLabel: UILabel!
//    @IBOutlet weak var transactionNumberLabel: UILabel!
//
//    @IBOutlet weak var actualDeductionAmountHeight: NSLayoutConstraint!
//    @IBOutlet weak var topViewHeightConstraint: NSLayoutConstraint!   /// 充電槍 底view 的 高
//
//    var tranNo: Int64?
//    var qrNo = ""
//    var delay = true
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        settingView()
//        setButtonTarget()
//
//    }
//
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated) 
//        NotificationCenter.default.addObserver(self, selector: #selector(callChargingResult), name: .repayLoadView, object: nil)
//        callChargingResult()
//
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//}
//
//
////MARK:- Setting view
//extension CompleteChargingViewController {
//    private func settingView() {
//        self.mainScrollView.alpha = 0
//        createNavViewForRightBtn(rightName: "", rightImage: "", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: UIColor.black)
//        titleLab?.text = "充電結果( 5/5 )"
//        self.backBtn?.isHidden = false
//        payBtn.layer.cornerRadius = 10
//        if let cs = navigationController?.viewControllers {
//            if cs.count >= 2 {
//                if let _ = navigationController?.viewControllers[1] as? ChargingRecordViewController {
//                    titleLab?.text = "充電記錄明細"
//                }
//            }
//        }
//    }
//
//    private func setButtonTarget() {
//        for temp in backBtn?.allTargets ?? [] {
//            backBtn?.removeTarget(self, action: temp as? Selector, for: .touchUpInside)
//        }
//        backBtn?.addTarget(self, action: #selector(sureAction(_:)), for: .touchUpInside)
//    }
//
//    private func displayRePayView(constant: CGFloat, hidden: Bool, status: String, ststusColor: UIColor ) {
//        /// true = constant 165, false = constrant 80
//        topViewHeightConstraint.constant = constant
//        statusLab.text = status
//        statusLab.textColor = ststusColor
//        payBtn.isHidden = hidden
//        contentLabel.isHidden = hidden
//        view.layoutIfNeeded()
//    }
//}
//
//
////MARK:- Action
//extension CompleteChargingViewController {
//    @IBAction func payAction(_ sender: UIButton) {
//        let vc = RePayCreditCardViewController()
//        vc.qrNo = qrNo
//        vc.tranNo = tranNo
//        vc.modalPresentationStyle = .overFullScreen
//        vc.modalTransitionStyle = .crossDissolve
//        present(vc, animated: true, completion: nil)
//    }
//
//    @IBAction func copyButtonAction(_ sender: Any) {
//        let pasteBoard = UIPasteboard.general
//        pasteBoard.string = self.transactionNumberLabel.text
//        ShowMessageView.showMessage(title: nil, message: "複製成功")
//    }
//
//    @IBAction func sureAction(_ sender: Any) {
//        if self.navigationController?.viewControllers.first is MainViewController || self.navigationController?.viewControllers.first is ChargingRecordViewController{
//            self.navigationController?.popToRootViewController(animated: true)
//        } else {
//            self.dismiss(animated: true) { }
//        }
//    }
//
//    @objc private func callChargingResult() {
//        var chargingNoStr = ""
//        if let chargingNo = tranNo {
//            chargingNoStr = "\(chargingNo)"
//        }
////        KJLRequest.requestForChargingResult(tranNo: chargingNoStr, delay: delay) { [weak self] (response) in
////            guard let response = response as? ChargingResultClass
////                else { return }
////            if let paymentError = response.datas?.paymentError {
////                if paymentError != "" {
////                    let fee = response.datas?.thisFee ?? "0"
////                    if Int(fee)! > 0 { // check fee
////                        self?.okImage.image = #imageLiteral(resourceName: "Payment_wrong")
////                        self?.transactionLab.text = "本次交易付款失敗"
////                        self?.displayRePayView(constant: 165, hidden: false, status: paymentError, ststusColor: #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1))
////                    }
////                } else {
////                    if let finishCode = response.datas?.finishCode_ch {
////                        self?.okImage.image = #imageLiteral(resourceName: "Payment_ok")
////                        self?.transactionLab.text = "充電流程已結束"
////                        self?.displayRePayView(constant: 80, hidden: true, status: finishCode, ststusColor: #colorLiteral(red: 0.2392156863, green: 0.7607843137, blue: 0.7843137255, alpha: 1))
////                    }
////                }
////            }
////
////            self?.chargingStateLabel.text = ""
////            let finishCode_ch = response.datas?.finishCode_ch ?? ""
////            if finishCode_ch != "" {
////                self?.chargingStateLabel.text = "" + finishCode_ch
////            }
////
////            // 最上面的充電槍資訊
////            let name = response.datas?.socketType ?? ""
////            let rename = name.replacingOccurrences(of: "/", with: ":")
////            self?.pieImage.image = UIImage(named: rename)
////            self?.nameLab.text = response.datas?.stationName
////            self?.codeLab.text = "充電槍代碼：" + (response.datas?.qrNo ?? "")//response.datas?.qrNo
////            self?.qrNo = response.datas?.qrNo ?? ""
////            self?.resultLab.text = response.datas?.chargeStatus
////            //優惠的頁面
////            let useFee = response.datas?.useFee ?? ""
////
//
//            // section 1 預約
//            //新增加的欄位
//            //            // == 是否預約 (Y/N)==
//            //            private String reserveFlag;
//            //            // == 預約開始 ==
//            //            private String reserveStart;
//            //            // == 預約金額 ==
//            //            private String reserveAmt;
////            let reserveFlag = response.datas?.reserveFlag ?? "N"
////            self?.sectionOneView.isHidden = (reserveFlag == "N")
////            self?.reservationStartLab.text = response.datas?.reserveStart ?? ""
////            self?.reservationFeeLab.text = "$ " + (response.datas?.reserveAmt  ?? "0")
////
////
////            // section 2 不隱藏
////            self?.startTimeLab.text = response.datas?.startTime
////            self?.tradeTimeLab.text = response.datas?.endTime
////            //總時間給秒數, 需要自己轉成時間格式
////            if let totalTime = response.datas?.totalTime {
////                if (totalTime == 0) {
////                    self?.totalTimeLab.text = "00:00:00"
////                } else {
////                    let sec = totalTime % 60
////                    var min = totalTime / 60
////                    let hour = min / 60
////                    min = min % 60
////                    let secString = (sec < 10) ? "0\(sec)" : "\(sec)"
////                    let minString = (min < 10) ? "0\(min)" : "\(min)"
////                    let hourString = (hour < 10) ? "0\(hour)" : "\(hour)"
////                    self?.totalTimeLab.text = "\(hourString):\(minString):\(secString)"
////                }
////            }
//
//            // section 3 不隱藏
//            //            // == 適用費率 (中華點數 / 一般費率) ==
//            //            private String useFee;
//            //            // == 本次費率 ( 10元 / 1分鐘) ==
//            //            private String thisFee;
//            //            // == 扣款金額 ==
//            //            private String deductFee;
//            //            // == 剩餘點數 ==
//            //            private String remainPoint;
////            //
////            self?.sectionThreeView.isHidden = false
////            //適用費率
////
////            self?.applicableRateLab.text = (useFee == "P") ? "扣點" : "扣款"
////
////            //本次費率
////            self?.currentRateLab.text = response.datas?.thisFee ?? ""
////
////            //累計費用
////            var deduct = ""
////            var discount = ""
////            if let deductFee = response.datas?.deductFee {
////                if deductFee == "" {
////                    deduct = "0"
////                } else {
////                    deduct = deductFee
////                }
////            }
////            if let discountAmt = response.datas?.discountAmt {
////                if discountAmt == "" {
////                    discount = "0"
////                } else {
////                    discount = discountAmt
////                }
////            }
////
////            let amount = String(Int(deduct)! + Int(discount)!)
////
////            if (useFee == "P") {
////                self?.amountRateTitleLab.text = "累計點數"
////                self?.amountRateLab.text = amount + " 點"
////                //剩餘點數
////                self?.remainderPointLab.text = (response.datas?.remainPoint ?? "0") + " 點"
////                self?.remainderPointHeight.constant = 20
////            }else{
////                self?.amountRateTitleLab.text = "累計費用"
////                self?.amountRateLab.text = "$ " + amount
////            }
////
////            if let isDiscount = response.datas?.isDiscount ,isDiscount == "Y" {
////                self?.discountView.isHidden = false
////                self?.discountTypeTitle.text = (useFee == "P") ? "本次優惠點數" : "本次優惠金額"
////                self?.discountFeeLabel.text = response.datas?.discountFee
////                if discount != "0" {
////                    if useFee == "P" {
////                        self?.discountTypeLabel.text = "- " + discount
////                    } else {
////                        self?.discountTypeLabel.text = "- $ " + discount
////                    }
////                } else {
////                    self?.discountTypeLabel.text = "- $ 0"
////                }
////            }else{
////                self?.discountView.isHidden = true
////            }
//
//            // section 4
//            //            // == 是否逾時 (Y/N)==
//            //            private String overtimeFlag;
//            //            // == 是否逾時開始 ==
//            //            private String overtimeStart;
//            //            // == 是否逾時結束 ==
//            //            private String overtimeEnd;
//            //            // == 是否逾時時間 ==
//            //            private String overtimes;
//            //            // == 是否逾時金額 ==
////            //            private String overtimeAmt;
////            //是否隱藏 section 4
////            let overtimeFlag = response.datas?.overtimeFlag ?? "N"
////            self?.sectionFourView.isHidden = (overtimeFlag == "N")
////            //逾時開始時間
////            self?.extendStartLab.text = response.datas?.overtimeStart ?? "00:00:00"
////            //逾時結束時間
////            self?.extendEndLab.text = response.datas?.overtimeEnd ?? "00:00:00"
////
////            //逾時累計時間 給秒數要自己轉
////            self?.extendAmountLab.text = "00:00:00"
////            if let overtimes = response.datas?.overtimes {
////                if (overtimes == 0) {
////                    self?.extendAmountLab.text = "00:00:00"
////                } else {
////                    let sec = overtimes % 60
////                    var min = overtimes / 60
////                    let hour = min / 60
////                    min = min % 60
////                    let secString = (sec < 10) ? "0\(sec)" : "\(sec)"
////                    let minString = (min < 10) ? "0\(min)" : "\(min)"
////                    let hourString = (hour < 10) ? "0\(hour)" : "\(hour)"
////                    self?.extendAmountLab.text = "\(hourString):\(minString):\(secString)"
////                }
////            }
//
//            //逾時費用
////            self?.extendFeeLab.text = "$ " + (response.datas?.overtimeAmt ?? "0")
////
////            // section 5
////            //實際扣除點數, 如果今天是 P 就顯示這一列資訊, 不然就隱藏
////            if (useFee == "P") {
////                self?.amountLab.text = (response.datas?.deductFee  ?? "0") + " 點"
////                self?.amountHeight.constant = 20
////            }
////
////            // 交易單號
////            self?.transactionNumberLabel.text = response.datas?.transactionNo
////
////            //實際扣除金額
////            if (useFee == "P") {
////                self?.actualDeductionAmountHeight.constant = (overtimeFlag == "Y") ? 20 : 0
////                self?.amountPointLabel.isHidden = (overtimeFlag != "Y")
////                self?.amountPointLabel.text = "$ " + (response.datas?.overtimeAmt ?? "0")
////            } else {
////                self?.amountPointLabel.text = "$ " + "\(response.datas?.amount ?? 0)"
////            }
////            //交易結果
////            self?.authLab.text = response.datas?.paymentStatus ?? ""
////
////            let isAICharge = response.datas?.isAICharge ?? "N"
////            self?.logoHeight.constant = (isAICharge == "Y") ? 33 : 0
////            if self?.mainScrollView.alpha == 0 {
////                UIView.animate(withDuration: 0.5, animations: {
////                    self?.mainScrollView.alpha = 1
////                })
////            }
////        }
////        self.view.layoutIfNeeded()
////    }
////}
//
