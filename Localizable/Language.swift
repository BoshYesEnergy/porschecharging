//
//  Language.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/12.
//  Copyright © 2020 s5346. All rights reserved.
//

import Foundation
import UIKit

enum LanguageID: String {
    case Chinese
    case English
    
    var toString: String {
        switch self {
        case .Chinese: return "zh-Hant"
        case .English: return "en"
        }
    }
}

enum Language: String {
    // SelectLoginViewController
    case Loginlater = "Loginlater"
    case PorscheID  = "PorscheID"
    
    // HomeTabBarController
    case Map     = "Map"
    case Route   = "Route"
    case Scan    = "Scan"
    case Profile = "Profile"
    case Whereto = "Whereto"
    
    // ProfileDetailViewController
    case Nickname = "nickname"
    case Account  = "Account"
    case Validity = "SubscriptionValidity"
    case Activate = "Activate"
    case Logout   = "Logout"
    case Payment  = "PaymentMethod"
    case Partner  = "PartnerAccount"
    case AddPartner = "AddPartner"
    case tologout = "tologout"
    
    
    // AddSubscriptionViewController
    case SubContent = "SubscriptionContent"
    case SubID = "SubscriptionPorsche" 
    case SubError = "SubscriptionError"
    case SubInfo = "SubscriptionInfo"
    case SubVin = "SubscriptionVin"
    case SubModel = "SubscriptionModel"
//    case SubValidity = "SubscriptionValidity" //rep
    case SubAlert = "SubscriptionAlert"
    case SubStatus = "SubscriptionStatus"
    case SubStartDate = "SubscriptionStartDeate"
    case SubEndDate = "SubscriptionExpiryDate"
    case SubAdd = "SubscriptionAdd"
     
    //
    case PaymentTitle = "PaymentTitle"
    case AddCreditCard = "AddCreditCard"
    case LINEPay = "LINEPay"
    case PaymentAdd = "PaymentAdd"
    case PaymentExpiry = "PaymentExpiry"
    case PaymentSecurity = "PaymentSecurity"
    case PaymentContent = "PaymentContent"
    case PaymentInfo = "PaymentInfo"
    case PaymentSuccessfully = "PaymentSuccessfully"
    case PaymentCreditCard = "PaymentCreditCard"
    case PaymentVALID = "PaymentVALID"
    case PaymentRemoveContent = "PaymentRemoveContent"
    case Invoice = "e-Invoice"
    case Peronal = "Peronal "
    case BackupEmailInfo = "BackupEmailInfo"
    case CloudinvoiceInfo = "CloudinvoiceInfo"
    case BusinessInfo = "BusinessInfo"
    case CompanyName = "CompanyName"
    case CompanyUnicode = "CompanyUnicode"
    
    // Tool
    case Confirm = "Confirm"
    case Cancel  = "Cancel"
    case Remove  = "Remove"
    case Email   = "Email"
    case Card    = "Card"
    
    // Alert
    case placeholderName = "placeholderName"
    case AlertNickname   = "AlertNickname"
}
