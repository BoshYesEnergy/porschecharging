//
//  LocalizeUtils.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/9/23.
//  Copyright © 2020 s5346. All rights reserved.
//

import Foundation
import UIKit

class LocalizeUtils {
    static let shared = LocalizeUtils()
    /// 依語系檔裡的 key 取得 value 值
    /// - Parameters:
    /// - key: 多國語系檔的 key 值
    /// - localizationFileNmae: 多國語系檔的檔名
    /// - Returns: String
    static public func localized(key: String) -> String {
        // 取得 Bundle 下的的多國語系檔
        //            let localizationFileNmae = UserDefaults.standard.string(forKey: "UserLanguage") ?? LanguageID.Chinese.toString
        let localizationFileNmae = KJLRequest.shareInstance().language ?? ""
        let path = Bundle.main.path(forResource: localizationFileNmae, ofType: "lproj")
        let bundle = Bundle(path: path!)
        // 依 key 值和 Bundle 的多國語系檔取得對應的 valu
        return NSLocalizedString(key, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
    
    /// 第一次開啟 App 依使用者 OS 設定的語系來決定 App 內的語系
    func settingUserLanguageCode() {
        let preferredLanguages: String = Locale.preferredLanguages.first!
        // 繁體=zh-Hant-TW、簡體=zh-Hans-TW
        let currentLanguageCode = preferredLanguages.split(separator: "-")[0]
        print("currentLanguageCode: \(currentLanguageCode)")
        ///
        ///  for select language to change
        ///
        let setLanguage = UserDefaults.standard.string(forKey: "setUserLanguage") ?? ""
        
        // 設定要取得的 Bundle 的多國語系檔名
        var bundleLocalizeFileName: String = ""
        switch currentLanguageCode.lowercased() {
        case LanguageID.Chinese.toString:  // 繁體
            bundleLocalizeFileName = setLanguage == "" ? LanguageID.Chinese.toString : setLanguage
        case LanguageID.English.toString:  // 英文
            bundleLocalizeFileName = setLanguage == "" ? LanguageID.English.toString : setLanguage
        default:
            bundleLocalizeFileName = LanguageID.Chinese.toString
        }
        print("\n多國語系檔： \(bundleLocalizeFileName)\n")
        // 設定多國語系檔的檔名
        
        UserDefaults.standard.set(bundleLocalizeFileName, forKey: "UserLanguage")
        KJLRequest.shareInstance().language = bundleLocalizeFileName
    }
    
    ///
    func toLanguage() -> String {
        let language = UserDefaults.standard.string(forKey: "UserLanguage") ?? ""
        return language == "en" ? "en" : "zh"
    }
}

