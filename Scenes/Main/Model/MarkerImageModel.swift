//
//  ImageNameModel.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/2/7.
//  Copyright © 2020 s5346. All rights reserved.
//
 
enum CenterVCType {
    case Map
}

enum FilterCarType {
    case car
    case motorcycle
}

enum AnnotationType: String {
    case NONE = "none"
    
    case AcFavorite    = "thumbnail_station_type_ac_favorite" //
    case AcFavoritedisable = "thumbnail_station_type_ac_favorite_disable" //
    case AcFavoriteUnAvailableAndDisable = "thumbnail_station_type_ac_favorite_disconnect" //
    case AcFavoriteOffLine     = "thumbnail_station_type_ac_favorite_Offline" //
    
    case AcAvailable   = "thumbnail_station_type_ac" //
    case AcUnAvailable = "thumbnail_station_type_ac_disconnect" //
    case Acdisable     = "thumbnail_station_type_ac_disable" //
    case AcOffLine     = "thumbnail_station_type_ac_Offline" //
     
    case DcAvailable   = "thumbnail_station_type_dc" //
    case Dcdisable     = "thumbnail_station_type_dc_disable" //
    case DcUnAvailable = "thumbnail_station_type_dc_disconnect" //
    case DcOffLine     = "thumbnail_station_type_dc_Offline" //
    
    case DcFavorite    = "thumbnail_station_type_dc_favorite" //
    case DcFavoritedisable = "thumbnail_station_type_dc_favorite_disable" //
    case DcFavoriteUnAvailableAndDisable = "thumbnail_station_type_dc_favorite_disconnect" //
    case DcFavoriteOffLine = "thumbnail_station_type_dc_favorite_Offline" //
     
    
}

enum ZoomImages: String {
    case NONE = "none"
    
    case AcFavorite    = "thumbnail_station_type_ac_favorite_big" //
    case AcFavoritedisable   = "thumbnail_station_type_ac_favorite_disable_big" //
    case AcFavoriteUnAvailableAndDisable = "thumbnail_station_type_ac_favorite_disconnect_big" //
    case AcFavoriteOffLine     = "thumbnail_station_type_ac_favorite_Offline_big" //
    
    case AcAvailable   = "thumbnail_station_type_ac_big" //
    case AcUnAvailable = "thumbnail_station_type_ac_disconnect_big" //
    case Acdisable     = "thumbnail_station_type_ac_disable_big" //
    case AcOffLine     = "thumbnail_station_type_ac_Offline_big" //
    
    
    case DcAvailable   = "thumbnail_station_type_dc_big" //
    case Dcdisable  = "thumbnail_station_type_dc_disable_big" //
    case DcUnAvailable = "thumbnail_station_type_dc_disconnect_big" //
    case DcOffLine = "thumbnail_station_type_dc_Offline_big" //
    
    case DcFavorite    = "thumbnail_station_type_dc_favorite_big" //
    case DcFavoritedisable = "thumbnail_station_type_dc_favorite_disable_big" //
    case DcFavoriteUnAvailableAndDisable = "thumbnail_station_type_dc_favorite_disconnect_big"//
    case DcFavoriteOffLine = "thumbnail_station_type_dc_favorite_Offline_big" //
    
}

enum PorscheICON: String {
    
    case AcOffLine              = "thumbnail_station_type_ac_porsche" //
    case AcOffLine_big          = "thumbnail_station_type_ac_porsche_big" //
    
    case AcFavoriteOffLine      = "thumbnail_station_type_ac_porsche_favorite" //
    case AcFavoriteOffLine_big  = "thumbnail_station_type_ac_porsche_favorite_big" // 
     
    case Dc = "thumbnail_station_type_dc_porsche" //
    case Dc_big = "thumbnail_station_type_dc_porsche_big"//
    
    case Dc_Favorite = "thumbnail_station_type_dc_porsche_favorite" //
    case Dc_Favorite_big = "thumbnail_station_type_dc_porsche_favorite_big" //
}

enum MaskImages: String {
    case icon1 = "mask_icon1"
    case icon3 = "mask_icon3"
    case maskON = "main_btn_on"
    case maskOFF = "main_btn_off"
}

enum ZoomMaskImages: String {
    case icon1_zoom = "mask_icon1_zoom"
    case icon3_zoom = "mask_icon3_zoom"
}
 
