//
//  MainModel.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/14.
//  Copyright © 2020 s5346. All rights reserved.
//
 

enum MainCellStatus {
    case Normal
    case Favorite
    case Search
}

enum Direction {
    case UP
    case DOWN
    case LIFT
    case RIGHT
}

protocol StationProtocol: class {
    func stationDetailClose()
    func stationnNvigation()
    func stationBookingBtn(detail: StationDetailClass.Data, parkingSpace: Int)
    func stationCharging()
    func stationFavorite(detail: StationDetailClass.Data?, idAdd: KJLRequest.RequestBOOL)
    func stationChangeheight()
    func stationUnlock()
}
