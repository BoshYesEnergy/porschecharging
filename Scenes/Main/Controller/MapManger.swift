//
//  MapManger.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/3/23.
//  Copyright © 2020 s5346. All rights reserved.
//

import Foundation
import UIKit

//
// 站點狀態：01: 可使用 , 02: 使用中 , 03: 未使用 , 04: 未營運
//
class MapManger {
    /// just on Favorite
    static public func checkFavoriteMarker(_ tempData: StationListClass.Data) -> String  {
        var imgName = ""
        if tempData.stationCategory?.lowercased() == "yes" {
            if tempData.hasDC == true {
                switch tempData.status {
                case "01": imgName = AnnotationType.DcFavorite.rawValue
                case "02": imgName = AnnotationType.DcFavorite.rawValue
//                case "03": imgName = AnnotationType.DcFavoriteUnAvailable.rawValue
//                case "04": imgName = AnnotationType.DcFavoriteUnAvailable.rawValue
//                case "05": imgName = AnnotationType.DcFavoriteUnAvailable.rawValue
                default: break
                }
            } else {
                switch tempData.status {
                case "01": imgName = AnnotationType.DcFavorite.rawValue
                case "02": imgName = AnnotationType.DcFavorite.rawValue
//                case "03": imgName = AnnotationType.DcFavoriteUnAvailable.rawValue
//                case "04": imgName = AnnotationType.DcFavoriteUnAvailable.rawValue
//                case "05": imgName = AnnotationType.DcFavoriteUnAvailable.rawValue
                default: break
                }
            }
        } else {
            if tempData.hasDC == true {
                switch tempData.status {
                case "01":
//                    if tempData.dcTotal == 1 {
//                        imgName = PorscheICON.DcFavorite_1_1.rawValue
//                    } else if tempData.dcTotal == 2 {
//                        imgName = "thumbnail_station_type_dc_porsche_favorite_\(tempData.dcEmpty ?? 0)_2"
//                    } else if tempData.dcTotal == 4 {
//                        imgName = "thumbnail_station_type_dc_porsche_favorite_\(tempData.dcEmpty ?? 0)_4"
//                    }
                break
                case "02":
                    break
//                    if tempData.dcTotal == 1 {
//                        imgName = PorscheICON.DcFavoriteUnAvailable_0_1.rawValue
//                    } else if tempData.dcTotal == 2 {
//                        imgName = PorscheICON.DcFavoriteUnAvailable_0_2.rawValue
//                    } else if tempData.dcTotal == 4 {
//                        imgName = PorscheICON.DcFavoriteUnAvailable_0_4.rawValue
//                    }
//                case "03": imgName = AnnotationType.DcFavoriteUnAvailable.rawValue
//                case "04": imgName = AnnotationType.DcFavoriteUnAvailable.rawValue
//                case "05": imgName = AnnotationType.DcFavoriteUnAvailable.rawValue
                default: break
                }
            } else {
                switch tempData.status {
//                case "01": imgName = PorscheICON.AcFavorite.rawValue
//                case "02": imgName = PorscheICON.AcFavorite.rawValue
//                case "03": imgName = AnnotationType.AcFavoriteUnAvailable.rawValue
//                case "04": imgName = AnnotationType.AcFavoriteUnAvailable.rawValue
//                case "05": imgName = AnnotationType.AcFavoriteUnAvailable.rawValue
                default: break
                }
            }
        }
        
        return imgName
    }
    
    static public func checkFavoriteZoomMarker(_ tempData: StationListClass.Data) -> String  {
        var imgName = ""
        if tempData.stationCategory?.lowercased() == "yes" {
            if tempData.hasDC == true {
                switch tempData.status {
                case "01": imgName = ZoomImages.DcFavorite.rawValue
                case "02": imgName = ZoomImages.DcFavorite.rawValue
//                case "03": imgName = ZoomImages.DcFavoriteUnAvailable.rawValue
//                case "04": imgName = ZoomImages.DcFavoriteUnAvailable.rawValue
//                case "05": imgName = ZoomImages.DcFavoriteUnAvailable.rawValue
                default:  break
                }
            } else { 
                switch tempData.status {
                case "01": imgName = ZoomImages.AcFavorite.rawValue
                case "02": imgName = ZoomImages.AcFavorite.rawValue
//                case "03": imgName = ZoomImages.AcFavoriteUnAvailable.rawValue
//                case "04": imgName = ZoomImages.AcFavoriteUnAvailable.rawValue
//                case "05": imgName = ZoomImages.AcFavoriteUnAvailable.rawValue
                default: break
                }
            }
        } else {
            if tempData.hasDC == true {
                switch tempData.status {
                case "01": break
//                    if tempData.dcTotal == 1 {
//                        imgName = PorscheICON.DcFavorite_1_1_big.rawValue
//                    } else if tempData.dcTotal == 2 {
//                        imgName = "thumbnail_station_type_dc_porsche_favorite_\(tempData.dcEmpty ?? 0)_2_big"
//                    } else if tempData.dcTotal == 4 {
//                        imgName = "thumbnail_station_type_dc_porsche_favorite_\(tempData.dcEmpty ?? 0)_4_big"
//                    }
                case "02":
                    break
//                    if tempData.dcTotal == 1 {
//                        imgName = PorscheICON.DcFavoriteUnAvailable_0_1_big.rawValue
//                    } else if tempData.dcTotal == 2 {
//                        imgName = PorscheICON.DcFavoriteUnAvailable_0_2_big.rawValue
//                    } else if tempData.dcTotal == 4 {
//                        imgName = PorscheICON.DcFavoriteUnAvailable_0_4_big.rawValue
//                    }
//                case "03": imgName = ZoomImages.DcFavoriteUnAvailable.rawValue
//                case "04": imgName = ZoomImages.DcFavoriteUnAvailable.rawValue
//                case "05": imgName = ZoomImages.DcFavoriteUnAvailable.rawValue
                default: break
                }
            } else {
                switch tempData.status {
//                case "01": imgName = PorscheICON.AcFavorite_big.rawValue
//                case "02": imgName = PorscheICON.AcFavorite_big.rawValue
//                case "03": imgName = ZoomImages.AcFavoriteUnAvailable.rawValue
//                case "04": imgName = ZoomImages.AcFavoriteUnAvailable.rawValue
//                case "05": imgName = ZoomImages.AcFavoriteUnAvailable.rawValue
                default: break
                }
            }
        }
        return imgName
    }
    
    static public func dcPorscheICON(_ tempData: StationListClass.Data) -> String {
        var imgName = ""
        
        switch tempData.favourite {
        case true:
            if tempData.connection == false {
                imgName = AnnotationType.DcFavoriteOffLine.rawValue
            } else {
                if tempData.isBussinessTime?.lowercased() == "n" {
                    imgName = AnnotationType.DcFavoritedisable.rawValue
                } else {
                    switch tempData.status {
                    case "01": imgName = PorscheICON.Dc_Favorite.rawValue
                    case "02": imgName = AnnotationType.DcFavoriteUnAvailableAndDisable.rawValue
                    case "03": imgName = AnnotationType.DcFavoriteUnAvailableAndDisable.rawValue
                    case "04": imgName = AnnotationType.DcFavoritedisable.rawValue
                    default: break
                    }
                }
            }
        case false:
            if tempData.connection == false  {
                imgName = AnnotationType.DcOffLine.rawValue
            } else {
                if tempData.isBussinessTime?.lowercased() == "n" {
                    imgName = AnnotationType.Dcdisable.rawValue
                } else {
                    switch tempData.status {
                    case "01": imgName = PorscheICON.Dc.rawValue
                    case "02": imgName = AnnotationType.DcUnAvailable.rawValue
                    case "03": imgName = AnnotationType.DcUnAvailable.rawValue
                    case "04": imgName = AnnotationType.Dcdisable.rawValue
                    default: break
                    }
                }
            }
        default: break
        }
        
        return imgName
    }
    
    static public func acPorscheICON(_ tempData: StationListClass.Data) -> String {
        var imgName = ""
        
        switch tempData.favourite {
        case true:
            if tempData.connection == false  {
                imgName = PorscheICON.AcFavoriteOffLine.rawValue
            } else {
                if tempData.isBussinessTime?.lowercased() == "n" {
                    imgName = AnnotationType.AcFavoritedisable.rawValue
                } else {
                    switch tempData.status {
                    case "01": imgName = PorscheICON.AcFavoriteOffLine.rawValue
                    case "02": imgName = PorscheICON.AcFavoriteOffLine.rawValue
                    case "03": imgName = PorscheICON.AcFavoriteOffLine.rawValue
                    case "04": imgName = PorscheICON.AcFavoriteOffLine.rawValue
                    default: break
                    }
                }
            }
        case false:
            if tempData.connection == false  {
                imgName = PorscheICON.AcOffLine.rawValue
            } else {
                if tempData.isBussinessTime?.lowercased() == "n" {
                    imgName = AnnotationType.Acdisable.rawValue
                } else {
                    switch tempData.status {
                    case "01": imgName = PorscheICON.AcOffLine.rawValue
                    case "02": imgName = PorscheICON.AcOffLine.rawValue
                    case "03": imgName = PorscheICON.AcOffLine.rawValue
                    case "04": imgName = PorscheICON.AcOffLine.rawValue
                    default: break
                    }
                } 
            }
        default: break
        }
        return imgName
    }
    
    static public func dcPorscheICONzoom(_ tempData: StationListClass.Data) -> String {
        var imgName = ""
        switch tempData.favourite {
        case true:
            if tempData.connection == false  {
                imgName = ZoomImages.DcFavoriteOffLine.rawValue
            } else {
                if tempData.isBussinessTime?.lowercased() == "n" {
                    imgName = ZoomImages.DcFavoritedisable.rawValue
                } else {
                    switch tempData.status {
                    case "01": imgName = PorscheICON.Dc_Favorite_big.rawValue
                    case "02": imgName = ZoomImages.DcFavoriteUnAvailableAndDisable.rawValue
                    case "03": imgName = ZoomImages.DcFavoriteUnAvailableAndDisable.rawValue
                    case "04": imgName = ZoomImages.DcFavoritedisable.rawValue
                    default: break
                    }
                }
            }
        case false:
            if tempData.connection == false  {
                imgName = ZoomImages.DcOffLine.rawValue
            } else {
                if tempData.isBussinessTime?.lowercased() == "n" {
                    imgName = ZoomImages.Dcdisable.rawValue
                } else {
                    switch tempData.status {
                    case "01": imgName = PorscheICON.Dc_big.rawValue
                    case "02": imgName = ZoomImages.DcUnAvailable.rawValue
                    case "03": imgName = ZoomImages.DcUnAvailable.rawValue
                    case "04": imgName = ZoomImages.Dcdisable.rawValue
                    default: break
                    }
                }
            }
        default: break
        }
        return imgName
    }
    
    static public func acPorscheICONzoom(_ tempData: StationListClass.Data) -> String {
        var imgName = ""
        switch tempData.favourite {
        case true:
            if tempData.connection == false  {
                imgName = ZoomImages.AcFavoriteOffLine.rawValue
            } else {
                if tempData.isBussinessTime?.lowercased() == "n" {
                    imgName = ZoomImages.AcFavoritedisable.rawValue
                } else {
                    switch tempData.status {
                    case "01": imgName = PorscheICON.AcFavoriteOffLine_big.rawValue
                    case "02": imgName = ZoomImages.AcFavoriteOffLine.rawValue
                    case "03": imgName = ZoomImages.AcFavoriteOffLine.rawValue
                    case "04": imgName = ZoomImages.AcFavoriteOffLine.rawValue
                    default: break
                    }
                }
            }
        case false:
            if tempData.connection == false  {
                imgName = ZoomImages.AcOffLine.rawValue
            } else {
                if tempData.isBussinessTime?.lowercased() == "n" {
                    imgName = ZoomImages.Acdisable.rawValue
                } else {
                    switch tempData.status {
                    case "01": imgName = PorscheICON.AcOffLine_big.rawValue
                    case "02": imgName = ZoomImages.AcOffLine.rawValue
                    case "03": imgName = ZoomImages.AcOffLine.rawValue
                    case "04": imgName = ZoomImages.AcOffLine.rawValue
                    default: break
                    }
                }
            }
        default: break
        }
        return imgName
    }
    
    static public func dcICON(_ tempData: StationListClass.Data) -> String {
        var imgName = ""
        switch tempData.favourite {
        case true:
            if tempData.connection == false  {
                imgName = AnnotationType.DcFavoriteOffLine.rawValue
            } else {
                if tempData.isBussinessTime?.lowercased() == "n" {
                    imgName = AnnotationType.DcFavoritedisable.rawValue
                } else {
                    switch tempData.status {
                    case "01": imgName = AnnotationType.DcFavorite.rawValue
                    case "02": imgName = AnnotationType.DcFavoriteUnAvailableAndDisable.rawValue
                    case "03": imgName = AnnotationType.DcFavoriteUnAvailableAndDisable.rawValue
                    case "04": imgName = AnnotationType.DcFavoritedisable.rawValue
                    default: break
                    }
                }
            }
        case false:
            if tempData.connection == false  {
                imgName = AnnotationType.DcOffLine.rawValue
            } else {
                if tempData.isBussinessTime?.lowercased() == "n" {
                    imgName = AnnotationType.Dcdisable.rawValue
                } else {
                    switch tempData.status {
                    case "01": imgName = AnnotationType.DcAvailable.rawValue
                    case "02": imgName = AnnotationType.DcUnAvailable.rawValue
                    case "03": imgName = AnnotationType.DcUnAvailable.rawValue
                    case "04": imgName = AnnotationType.Dcdisable.rawValue
                    default: break
                    }
                }
            }
        default: break
        }
        return imgName
    }
    
    static public func acICON(_ tempData: StationListClass.Data) -> String {
        var imgName = ""
        switch tempData.favourite {
        case true:
            if tempData.connection == false  {
                imgName = AnnotationType.AcFavoriteOffLine.rawValue
            } else {
                if tempData.isBussinessTime?.lowercased() == "n" {
                    imgName = AnnotationType.AcFavoritedisable.rawValue
                } else {
                    switch tempData.status {
                    case "01": imgName = AnnotationType.AcFavorite.rawValue
                    case "02": imgName = AnnotationType.AcFavoriteUnAvailableAndDisable.rawValue
                    case "03": imgName = AnnotationType.AcFavoriteUnAvailableAndDisable.rawValue
                    case "04": imgName = AnnotationType.AcFavoritedisable.rawValue
                    default: break
                    }
                }
            }
        case false:
            if tempData.connection == false  {
                imgName = AnnotationType.AcOffLine.rawValue
            } else {
                if tempData.isBussinessTime?.lowercased() == "n" {
                    imgName = AnnotationType.Acdisable.rawValue
                } else {
                    switch tempData.status {
                    case "01": imgName = AnnotationType.AcAvailable.rawValue
                    case "02": imgName = AnnotationType.AcUnAvailable.rawValue
                    case "03": imgName = AnnotationType.AcUnAvailable.rawValue
                    case "04": imgName = AnnotationType.Acdisable.rawValue
                    default: break
                    }
                }
            }
        default: break
        }
        return imgName
    }
    
    static public func dcICONzoom(_ tempData: StationListClass.Data) -> String  {
        var imgName = ""
        switch tempData.favourite {
        case true:
            if tempData.connection == false  {
                imgName = ZoomImages.DcFavoriteOffLine.rawValue
            } else {
                if tempData.isBussinessTime?.lowercased() == "n" {
                    imgName = ZoomImages.DcFavoritedisable.rawValue
                } else {
                    switch tempData.status {
                    case "01": imgName = ZoomImages.DcFavorite.rawValue
                    case "02": imgName = ZoomImages.DcFavoriteUnAvailableAndDisable.rawValue
                    case "03": imgName = ZoomImages.DcFavoriteUnAvailableAndDisable.rawValue
                    case "04": imgName = ZoomImages.DcFavoritedisable.rawValue
                    default: break
                    }
                }
            }
        case false:
            if tempData.connection == false  {
                imgName = ZoomImages.DcOffLine.rawValue
            } else {
                if tempData.isBussinessTime?.lowercased() == "n" {
                    imgName = ZoomImages.Dcdisable.rawValue
                } else {
                    switch tempData.status {
                    case "01": imgName = ZoomImages.DcAvailable.rawValue
                    case "02": imgName = ZoomImages.DcUnAvailable.rawValue
                    case "03": imgName = ZoomImages.DcUnAvailable.rawValue
                    case "04": imgName = ZoomImages.Dcdisable.rawValue
                    default: break
                    }
                }
            }
        default: break
        }
        return imgName
    }
    
    static public func acICONzoom(_ tempData: StationListClass.Data) -> String  {
        var imgName = ""
        switch tempData.favourite {
        case true:
            if tempData.connection == false  {
                imgName = ZoomImages.AcFavoriteOffLine.rawValue
            } else {
                if tempData.isBussinessTime?.lowercased() == "n" {
                    imgName = ZoomImages.AcFavoritedisable.rawValue
                } else {
                    switch tempData.status {
                    case "01": imgName = ZoomImages.AcFavorite.rawValue
                    case "02": imgName = ZoomImages.AcFavoriteUnAvailableAndDisable.rawValue
                    case "03": imgName = ZoomImages.AcFavoriteUnAvailableAndDisable.rawValue
                    case "04": imgName = ZoomImages.AcFavoritedisable.rawValue
                    default: break
                    }
                }
            }
        case false:
            if tempData.connection == false  {
                imgName = ZoomImages.AcOffLine.rawValue
            } else {
                if tempData.isBussinessTime?.lowercased() == "n" {
                    imgName = ZoomImages.Acdisable.rawValue
                } else {
                    switch tempData.status {
                    case "01": imgName = ZoomImages.AcAvailable.rawValue
                    case "02": imgName = ZoomImages.AcUnAvailable.rawValue
                    case "03": imgName = ZoomImages.AcUnAvailable.rawValue
                    case "04": imgName = ZoomImages.Acdisable.rawValue
                    default: break
                    }
                }
            }
        default:  break
        }
        return imgName
    }
    
    static public func tapZoomMarker(marker: GMSMarker, icon: UIImage?) {
        switch icon {
        case UIImage(named: AnnotationType.DcAvailable.rawValue):
            marker.icon = UIImage(named: ZoomImages.DcAvailable.rawValue)
        case UIImage(named: AnnotationType.DcFavorite.rawValue):
            marker.icon = UIImage(named: ZoomImages.DcFavorite.rawValue)
        case UIImage(named: AnnotationType.DcUnAvailable.rawValue):
            marker.icon = UIImage(named: ZoomImages.DcUnAvailable.rawValue)
            
        case UIImage(named: AnnotationType.AcAvailable.rawValue):
            marker.icon = UIImage(named: ZoomImages.AcAvailable.rawValue)
        case UIImage(named: AnnotationType.AcFavorite.rawValue):
            marker.icon = UIImage(named: ZoomImages.AcFavorite.rawValue)
        case UIImage(named: AnnotationType.AcUnAvailable.rawValue):
            marker.icon = UIImage(named: ZoomImages.AcUnAvailable.rawValue)
             
            
        default:  break
        }
    }
    
    static public func tapZoomOutMarker(_ mapView: GMSMapView, _ marker: GMSMarker) {
        switch marker.icon {
            
        case UIImage(named: ZoomImages.DcAvailable.rawValue):
            marker.icon = UIImage(named: AnnotationType.DcAvailable.rawValue)
        case UIImage(named: ZoomImages.DcFavorite.rawValue):
            marker.icon = UIImage(named: AnnotationType.DcFavorite.rawValue)
        case UIImage(named: ZoomImages.DcUnAvailable.rawValue):
            marker.icon = UIImage(named: AnnotationType.DcUnAvailable.rawValue)
            
        case UIImage(named: ZoomImages.AcAvailable.rawValue):
            marker.icon = UIImage(named: AnnotationType.AcAvailable.rawValue)
        case UIImage(named: ZoomImages.AcFavorite.rawValue):
            marker.icon = UIImage(named: AnnotationType.AcFavorite.rawValue)
        case UIImage(named: ZoomImages.AcUnAvailable.rawValue):
            marker.icon = UIImage(named: AnnotationType.AcUnAvailable.rawValue)
             
        default: break
        }
    }
} 
