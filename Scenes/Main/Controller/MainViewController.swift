//
//  MainViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import SVProgressHUD
import FacebookLogin
import FacebookCore
import SDWebImage

class MainViewController: UIViewController {
    
    // MARK: - IBOutlet
    @IBOutlet weak var favoriteBtn: UIButton!
    @IBOutlet weak var leftScroll: UIScrollView!
    //    @IBOutlet weak var centerView: UIView!
    @IBOutlet weak var leftView: UIView!
    //    @IBOutlet weak var leftViewRight: NSLayoutConstraint!
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var clearTextBtn: UIButton!
    
    //@IBOutlet weak var scanBtn: UIButton!
    //    @IBOutlet weak var searchViewTop: NSLayoutConstraint!
    //    @IBOutlet weak var userLocationBtnBottom: NSLayoutConstraint!
    @IBOutlet weak var naviBtnBottom: NSLayoutConstraint!
    //@IBOutlet weak var scanBtnBottom: NSLayoutConstraint!
    @IBOutlet weak var maskView: UIView!
    @IBOutlet weak var searchTable: UITableView!
    // action Btn
    @IBOutlet weak var filterBtn: UIButton!
    @IBOutlet weak var locationBtn: UIButton!
    @IBOutlet weak var infoBtn: UIButton!
    
    // map nav view
    @IBOutlet weak var selectPinView: UIView!
    @IBOutlet weak var selectPinNameLab: UILabel!
    @IBOutlet weak var selectPinViewBottom: NSLayoutConstraint!
    @IBOutlet weak var acEmptyLab: UILabel!
    @IBOutlet weak var acTotalLab: UILabel!
    @IBOutlet weak var dcEmptyLab: UILabel!
    @IBOutlet weak var dcTotalLab: UILabel!
    @IBOutlet weak var pinIDLab: UILabel!
    @IBOutlet weak var supportLabel: UILabel!
    @IBOutlet weak var pinShadowView: UIView!
    @IBOutlet weak var pinScoreLab: UILabel!
    
    @IBOutlet weak var searchBarView: UIView!
     
    @IBOutlet weak var searchTextBottomView: UIView!
    @IBOutlet weak var scoreViewWidth: NSLayoutConstraint!
    @IBOutlet weak var dcCategoryViewHeight: NSLayoutConstraint!
    @IBOutlet weak var acCategoryViewHeight: NSLayoutConstraint!
    @IBOutlet weak var acDcViewHeight: NSLayoutConstraint!
    @IBOutlet weak var showPinInfoViewHeight: NSLayoutConstraint!
    @IBOutlet weak var mapView: GMSMapView!
    private var clusterManager: GMUClusterManager!
    @IBOutlet weak var masksDetailBtn: UIButton!
    @IBOutlet weak var carDetailView: UIView!
    @IBOutlet weak var kmLabel: UILabel!
    
    @IBOutlet weak var searchBarLine: UIView!
    @IBOutlet weak var searchBorderView: UIView!
    @IBOutlet weak var serchBorderViewHeight: NSLayoutConstraint!
    @IBOutlet weak var searchBarBottomConstraint: NSLayoutConstraint!
    
    /// Mask Detail View
    @IBOutlet weak var masksDetailView: UIView!
    
    // Fliter View
    @IBOutlet weak var fliterTitle: UILabel!
    @IBOutlet weak var fliterACTitle: UILabel!
    @IBOutlet weak var fliterDcTitle: UILabel!
    @IBOutlet weak var fliterAvilableTitle: UILabel!
    @IBOutlet weak var reserBtn: UIButton!
    
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var porscheSwitch: UISwitch!
    @IBOutlet weak var acSwitch: UISwitch!
    @IBOutlet weak var dcSwitch: UISwitch!
    @IBOutlet weak var availableSwitch: UISwitch!
    @IBOutlet weak var reserAllBtn: UIButton!
    @IBOutlet weak var cencelFilterBtn: UIButton!
    @IBOutlet weak var fliterViewBottomConstraint: NSLayoutConstraint!
    
    // Station Detail
    @IBOutlet weak var stationTable: UITableView!
    @IBOutlet weak var stationMaskView: UIView!
    @IBOutlet weak var stationDetailBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var stationMaskHeightConstraint: NSLayoutConstraint!
    
    // charging Status view
    @IBOutlet weak var statusAlertView: UIView!
    @IBOutlet weak var statusColorView: UIView!
    @IBOutlet weak var statusPromptBtn: UIButton!
    @IBOutlet weak var statusContentLabel: UILabel!
    @IBOutlet weak var statusCheckBtn: UIButton!
    @IBOutlet weak var statusTimeLabel: UILabel!
    
    // MARK: - Property
    static var mainController: MainViewController?
    var currentVC: UIViewController? = nil
    var leftVC: LeftViewController? = nil
    var isRunCenter: Bool = false
    var pinArray: [CustomPin] = []
    private var showMarkerArray: [POIItem] = []
    var stationListArray: [StationListClass.Data] = []
    var selectPin: CustomPin? = nil
    var searchDataArray: [StationSearchModel.Data] = []
    var favoriteListArray: [StationListClass.Data] = [] 
    var searchText: String = ""
    var isShowPreferences = false
    var isFristInto:Bool = true
    private var filterCarType: FilterCarType = .car
    private var googleSearchArray: [Dictionary<String,AnyObject>] = Array()
    private var googleAnnotationArray: [CustomGMSMarker] = []
    private var googleAnnoTitle: String = ""
    // save google map market
    private var markerImage: UIImage?
    private var tapMarker: GMSMarker?
    // Mask
    private var masksMarker: GMSMarker?
    private var tapMaskMarker: GMSMarker?
    private var maskDetailArray: [MaskDetailModel.Data] = []
    private var singleMaskDetail: MaskDetailModel.Data?
    
    private var cellStatus: MainCellStatus = .Normal
    private var stationDetailData: StationDetailClass?
    // 充電次數走勢
    private var sevenChargeDateArray: [String] = []
    private var sevenChargeTotalArray: [Double] = []
    // 充電槍頭類型
    private var chargerGunlists: [SocketListClass.Data] = []
    // station demo cell row height
    private var stationDemoHeight: CGFloat = 0
    private var stationKM = ""
    
    // 正在充電,取得充電槍號碼
    var chargingTranNo = 0
    
    override func viewDidLoad() {
        super.viewDidLoad() 
        MainViewController.setMainViewController(self)
        //輸入欄位增加刪除鈕
        searchField.clearButtonMode = UITextFieldViewMode.whileEditing
        if KJLRequest.isLogin() == true {
            // 更新語系
            KJLRequest.requestForChangeLanguage(LocalizeUtils.shared.toLanguage()) { (_) in }
        } 
        
        setView()
        setStatusAlertView()
        self.setupClusterManager()
        
//        UserDefaults.standard.register(defaults: ["notFirst": false]) // 顯示導覽頁
//        UserDefaults.standard.register(defaults: ["showFavourite": false]) // 顯示我的最愛
        UserDefaults.standard.register(defaults: ["showTraffic": false]) // 顯示map traffic
//        UserDefaults.standard.register(defaults: ["LoveDefault": true]) // filter default
        searchTable.register(UINib(nibName: "NormalCell", bundle: nil), forCellReuseIdentifier: "NormalCell")
        searchTable.register(UINib(nibName: "FavoriteCell", bundle: nil), forCellReuseIdentifier: "FavoriteCell")
        searchTable.register(UINib(nibName: "NormalDataCell", bundle: nil), forCellReuseIdentifier: "NormalDataCell")
        stationTable.register(UINib(nibName: "StationDemoCell", bundle: nil), forCellReuseIdentifier: "StationDemoCell")
        stationTable.register(UINib(nibName: "StationDetailCell", bundle: nil), forCellReuseIdentifier: "StationDetailCell")
        stationTable.register(UINib(nibName: "ChargerTypeCell", bundle: nil), forCellReuseIdentifier: "ChargerTypeCell")
        stationTable.register(UINib(nibName: "ChargingChartsCell", bundle: nil), forCellReuseIdentifier: "ChargingChartsCell")
        stationTable.register(UINib(nibName: "BusinessHoursCell", bundle: nil), forCellReuseIdentifier: "BusinessHoursCell")
//        stationTable.isUserInteractionEnabled = true
        
        view.layoutIfNeeded()
        if KJLCommon.isX() == true {  // check iPhoneX
            naviBtnBottom.constant = 204
        } else {
            naviBtnBottom.constant = 170
        }
        MainViewController.mainController = self
        self.setupLocation()  /// 定位
        initView()
        leftViewAction(nil)
        KJLRequest.shareInstance().kJLRequestDelegate = self
        
        print("Tutoral: \(UserDefaults.standard.string(forKey: "Tutoral"))")
        if UserDefaults.standard.string(forKey: "Tutoral") == nil {
            UserDefaults.standard.set("1", forKey: "Tutoral")
            popTutoral()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) 
        setNotificationCenter()
        leftVC?.viewWillAppear(false)
        //call預約檢查API 
        if KJLRequest.isLogin() == true {  ///已登入狀態
            if UserDefaults.standard.bool(forKey: "showTraffic") == true {
                self.mapView.isTrafficEnabled = true
            } else {
                self.mapView.isTrafficEnabled = false
            }
            let distanceValue = UserDefaults.standard.float(forKey: "FilterDistanceValue")
//            if (UserDefaults.standard.bool(forKey: "LoveDefault") == true) && (distanceValue > 201) {
//
//            }
        }
        // 預設 switch
        porscheSwitch.isOn   = false
        acSwitch.isOn        = true
        dcSwitch.isOn        = true
        availableSwitch.isOn = false
        checkFilterType()
        getStationData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //pinShadowView畫面初始化
        pinShadowView.backgroundColor = UIColor.clear
        pinShadowView.layer.masksToBounds = false
        pinShadowView.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        pinShadowView.layer.shadowOffset = CGSize(width: 5.0, height: 5.0)
        pinShadowView.layer.shadowOpacity = 0.2
        pinShadowView.layer.shadowRadius = 4.0
        setLanguage() 
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated) //移除監聽器
//        NotificationCenter.default.removeObserver(self, name: .UITextFieldTextDidChange, object: searchField)
        NotificationCenter.default.removeObserver(self)
        closeFliterView()
        stationDetailClose()
        searchField.text = ""
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        //        closeSelectPinGesture(nil)
    }
    
    private func setLanguage() {
        fliterTitle.text = LocalizeUtils.localized(key: "filterTitle")
        fliterACTitle.text = LocalizeUtils.localized(key: "filterAC")
        fliterDcTitle.text = LocalizeUtils.localized(key: "filterDC")
        fliterAvilableTitle.text = LocalizeUtils.localized(key: "filterAvailable")
        reserBtn.setTitle(LocalizeUtils.localized(key: "filterReset"), for: .normal)
        searchField.placeholder = LocalizeUtils.localized(key: "Whereto")
    }
    
    private func setView() {
        self.stationMaskHeightConstraint.constant = 0
        self.stationMaskView.alpha = 0
        self.selectPinViewBottom.constant = -272
        self.stationDetailBottomConstraint.constant = -self.view.frame.height
        self.stationTable.isScrollEnabled = false
        self.searchBarBottomConstraint.constant = 15
        self.searchBarLine.isHidden = true
        self.serchBorderViewHeight.constant = 36
        self.searchTable.isHidden = true
        self.searchBorderView.layer.cornerRadius = 5
        self.searchBorderView.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 1)
        self.searchBorderView.layer.borderWidth = 1
        self.searchTable.layer.cornerRadius = 5
        self.clearTextBtn.isHidden = true
        reserAllBtn.layer.borderWidth = 1
        reserAllBtn.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        locationBtn.setImage(UIImage(named: ToolImage.location_click), for: .highlighted)
        infoBtn.setImage(UIImage(named: ToolImage.information_click), for: .highlighted)
        filterBtn.setImage(UIImage(named: ToolImage.filter_click), for: .highlighted)
         
        let stationSwipeUp = UISwipeGestureRecognizer(target: self, action: #selector(swipeUP))
        stationSwipeUp.direction = .up
        let stationSwipeDown = UISwipeGestureRecognizer(target: self, action: #selector(stationDetailClose))
        stationSwipeDown.direction = .down
        stationTable.addGestureRecognizer(stationSwipeUp)
        stationTable.addGestureRecognizer(stationSwipeDown)
    
    }
    
    func popTutoral() {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AppGuideViewController")
        let nav = UINavigationController(rootViewController: vc)
        nav.isNavigationBarHidden  = true
        nav.modalPresentationStyle = .overFullScreen
        present(nav, animated: true, completion: nil)
    }
    
    //MARK: Set status Alert
    @objc func showStatusView() {
        /// check data , will show
        statusAlertView.isHidden = false
    }
    
    @objc func hiddenStatusView() {
        statusAlertView.isHidden = true
    }
    
    @objc func getChargingTranNo(notification: Notification) -> Void {
        guard let chargingTranNo = notification.userInfo!["tranNo"] else { return }
        print("chargingTranNo: \(chargingTranNo)")
        self.chargingTranNo = chargingTranNo as! Int
    }
    
    fileprivate func setStatusAlertView() {
        statusAlertView.isHidden           = true
        statusAlertView.layer.borderWidth  = 1
        statusAlertView.layer.borderColor  = ThemeManager.shared.colorPalettes.garyLine.cgColor
        statusColorView.backgroundColor    = ThemeManager.shared.colorPalettes.greenPrompt
        statusCheckBtn.layer.borderColor   = ThemeManager.shared.colorPalettes.blackBorder.cgColor
        statusCheckBtn.layer.borderWidth   = 1
        statusPromptBtn.setImage(UIImage(named: ToolPhoto.icon_success.rawValue), for: .normal)
        statusTimeLabel.text = ""
    }
    
    // station View
    @objc fileprivate func swipeUP() {
        self.stationMaskHeightConstraint.constant = self.view.frame.height
        naviBtnBottom.constant = 200
        displayBtns(true)
        UIView.animate(withDuration: 0.3) {
            self.stationMaskView.alpha = 0.5
            self.stationDetailBottomConstraint.constant = -40
            self.stationTable.isScrollEnabled = true
            self.view.layoutIfNeeded()
        }
    }
    
    // station View
    fileprivate func swipeDOWN() {
        self.stationMaskHeightConstraint.constant = 0
        naviBtnBottom.constant = 20
        self.stationTable.isScrollEnabled = false
        var gapConstant: CGFloat = 90
        if KJLCommon.isX() == true {
            gapConstant = 100
        }
        UIView.animate(withDuration: 0.3, animations: {
            self.stationMaskView.alpha = 0
            self.stationDetailBottomConstraint.constant = -self.view.frame.height + self.stationDemoHeight + gapConstant
            self.view.layoutIfNeeded()
        }) { (_) in
            self.displayBtns()
        }
    }
    
    // display action button
    private func displayBtns(_ hidden: Bool = false) {
        filterBtn.isHidden   = hidden
        locationBtn.isHidden = hidden
        infoBtn.isHidden     = hidden
    }
    
    private func setNotificationCenter() {
        NotificationCenter.default.addObserver(self, selector: #selector(searchChange(obj:)), name: .UITextFieldTextDidChange, object: searchField)
        NotificationCenter.default.addObserver(self, selector: #selector(getStationData), name: .mainReloadMarkerList, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(versionCheck), name: .mainVersionCheck, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(closeAllView), name: .mapAllUpdate, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showStatusView), name: .showChargingStatusAlert, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hiddenStatusView), name: .hiddenChargingStatusAlert, object: nil)
        NotificationCenter.default.addObserver(forName: .sendChargingTranNo, object: nil, queue: nil, using: getChargingTranNo(notification:))
    }
    
    func setupClusterManager() {
        self.mapView.delegate = self
        self.mapView.isMyLocationEnabled = true
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "google_map_style", withExtension: "json") {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        
        let iconGenerator = GMUDefaultClusterIconGenerator(buckets: [8,10,50,100,200], backgroundColors: [#colorLiteral(red: 0.07058823529, green: 0.5254901961, blue: 0.7529411765, alpha: 1),#colorLiteral(red: 0.07058823529, green: 0.5254901961, blue: 0.7529411765, alpha: 1),#colorLiteral(red: 0.07058823529, green: 0.5254901961, blue: 0.7529411765, alpha: 1),#colorLiteral(red: 0.07058823529, green: 0.5254901961, blue: 0.7529411765, alpha: 1),#colorLiteral(red: 0.07058823529, green: 0.5254901961, blue: 0.7529411765, alpha: 1)])
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: mapView,
                                                 clusterIconGenerator: iconGenerator)
        #warning("Google Map Minimum Cluster ")
        renderer.minimumClusterSize = 8
        renderer.delegate = self
        clusterManager = GMUClusterManager(map: mapView, algorithm: algorithm,
                                           renderer: renderer)
        clusterManager.setDelegate(self, mapDelegate: self)
        #warning("Emitter 下雪動畫")
        /// Emitter 下雪動畫
        //self.mapView.setupMerryChristmas()
    }
    
    func setupLocation() {
        guard CLLocationManager.authorizationStatus() != .denied else {
            if #available(iOS 13.0, *) {
                let region = MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake(25.0329893, 121.5326985), 5000, 5000)
                let camera = GMSCameraPosition.camera(withLatitude: region.center.latitude, longitude: region.center.longitude, zoom: 15)
                self.mapView.camera = camera
                KJLCommon.requestAuthorize(desc: "需開啟定位")
            }
            return
        }
        self.checkLocationManagers()
    }
    
    @objc func getStationData() {
        self.callProfile()
        self.callAPIRequestForStationList(queryString: nil, queryType: .Station , type:"1")
    }
    
    // 登入成功 呼叫個資api
    func callProfile() {
        // [201] 查詢基本資料
        guard KJLRequest.isLogin() == true  else { return }
        KJLRequest.requestForProfile { (response) in
            guard let response = response as? ProfileClass else { return }
            KJLRequest.shareInstance().profile = response
            
        }
    }
    
    func checkLocationManagers() {
        LocationManagers.updateLocation { [unowned self] (status, location) in
            if  status == .Success {
                let region = MKCoordinateRegionMakeWithDistance(location!.coordinate, 5000, 5000)
                let camera = GMSCameraPosition.camera(withLatitude: region.center.latitude, longitude: region.center.longitude, zoom: 15)
                self.mapView.camera = camera
            }
        }
    }
    
    /// Call Mask API
    @objc fileprivate func callMasksAPI() { 
        self.showMoreMerkerWith(stationListDatas: self.stationListArray)
        KJLRequest.requestForMaskDetail { [weak self] (response) in
            guard let data = response as? MaskDetailModel else { return }
            guard let detail = data.datas else { return }
            self?.maskDetailArray = detail
            self?.addMasksMarker(maskData: detail)
        }
    }
    
    /// Add Masks Marker to mapView
    fileprivate func addMasksMarker(maskData: [MaskDetailModel.Data]) {
        for data in maskData {
            self.masksMarker = GMSMarker()
            let latitude = Double(data.latitude ?? "0.0")!
            let longitude = Double(data.longitude ?? "0.0")!
            self.masksMarker?.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            //            self.masksMarker?.title = data.pharmacyName
            if data.adultMaskCounts == "0" || data.kidMaskCounts == "0" {
                self.masksMarker?.icon = UIImage(named: MaskImages.icon3.rawValue)
            } else {
                self.masksMarker?.icon = UIImage(named: MaskImages.icon1.rawValue)
            }
            self.masksMarker?.map = self.mapView
        }
    }
    
    //MARK: - IBAction
    // 左邊選單
    @IBAction func leftViewAction(_ sender: UIButton?) {
        //左側menu打開時
        if leftScroll.contentOffset.x > 0 {
            leftVC?.viewWillAppear(false) 
            leftScroll.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        } else {
            leftScroll.setContentOffset(CGPoint(x: leftScroll.frame.width, y: 0), animated: true)
        }
    }
    
    @IBAction func chargingStatusCheck(_ sender: UIButton) {
        KJLRequest.requestForChargingData {[weak self] (response) in
            guard let response = response as? ChargingDataModel,
                  let data = response.datas
            else {
                self?.hiddenStatusView()
                return }
            
            let charging = data.charging ?? false
            switch charging {
            case true:
                if data.process?.lowercased() ?? "" == "start" { 
                    let vc = ReadyChargingVC(nibName: "ReadyChargingVC", bundle: nil)
                    vc.chargingId = data.chargespotId ?? ""
                    let nav = UINavigationController(rootViewController: vc)
                    nav.modalPresentationStyle = .overFullScreen
                    nav.navigationBar.isHidden = true
                    self?.present(nav, animated: true, completion: nil)
                } else {
                    let vc = StartChargingVC(nibName: "StartChargingVC", bundle: nil)
                    vc.tranNo = data.tranNo ?? 0
                    vc.chargingId = data.chargespotId ?? ""
                    let nav = UINavigationController(rootViewController: vc)
                    nav.modalPresentationStyle = .overFullScreen
                    self?.present(nav, animated: true, completion: nil)
                } 
            case false:
                self?.hiddenStatusView()
            }
        }
    }
     
    @IBAction func information(_ sender: UIButton) {
        let vc = IServiceViewController()
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        present(vc, animated: true, completion: nil)
    }
     
    @IBAction func clearTextField(_ sender: UIButton) {
        searchField.text = ""
        clearTextBtn.isHidden = true
    }
     
    /// 收尋輸入
    @IBAction func searchTextEditingChange(_ sender: UITextField) {
        if let text = sender.text {
            if (text.count <= 0) {
                self.showMoreMerkerWith(stationListDatas: self.stationListArray)
            }
        }
    }
    
    // 我的最愛
    @IBAction func showFavouriteAction(_ sender: UIButton) {
        view.endEditing(true)
        guard KJLRequest.isLogin() == true else {
            gotoLogin()
            return }
        sender.isSelected = !sender.isSelected
        sender.setImage(sender.isSelected == false ? #imageLiteral(resourceName: "icon_bar_favorite_default") : #imageLiteral(resourceName: "icon_bar_favorite_click"), for: .normal)
        self.searchBarLine.isHidden = sender.isSelected == false ? true : false
        UIView.animate(withDuration: 0.3) {
            var constant: CGFloat = 100
            if KJLCommon.isX() == true {
                constant = 140
            }
            let toTop = (self.mapView.frame.height - constant)
            self.searchBarBottomConstraint.constant = 15
            self.serchBorderViewHeight.constant = sender.isSelected == false ? 36 : toTop + 20
            self.searchTable.isHidden = sender.isSelected == false ? true : false
            self.view.layoutIfNeeded()
            self.cellStatus = sender.isSelected == false ? .Normal : .Favorite
            if sender.isSelected == true {
//                self.callAPIRequestForStationList(queryString: "", queryType: .Favourite, type: "")
                self.searchTable.reloadData()
            }
        }
    }
    
    // 篩選 - onOff filter view
    @IBAction func filterAction(_ sender: UIButton) {
        stationDetailClose()
        sender.isSelected = !sender.isSelected
        if KJLCommon.isX() == true {
            self.naviBtnBottom.constant = sender.isSelected == false ? 204 : 140 + 366
        } else {
            self.naviBtnBottom.constant = sender.isSelected == false ? 170 : 106 + 366
        }
        UIView.animate(withDuration: 0.3) {
                self.fliterViewBottomConstraint.constant = sender.isSelected == false ? -366 : 0
                self.view.layoutIfNeeded()
        }
        checkFilterType()
    }
    
    // 我的定位
    @IBAction func myLocationAction(_ sender: UIButton) {
        guard CLLocationManager.authorizationStatus() != .denied else {
            let region = MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake(25.0329893, 121.5326985), 5000, 5000)
            let camera = GMSCameraPosition.camera(withLatitude: region.center.latitude, longitude: region.center.longitude, zoom: 15)
            self.mapView.camera = camera
            KJLCommon.requestAuthorize(desc: "需開啟定位")
            return
        }
        self.checkLocationManagers()
    }
    
    // 導航 change button image
    @IBAction func navMapTouchAction(_ sender: UIButton) {
        sender.setImage(UIImage(named: "icon_navigation_click"), for: .highlighted)
    }
    
    // 導航
    @IBAction func navMapAction(_ sender: Any) {
        KJLCommon.openNavMap(lat: selectPin?.coordinate.latitude, long: selectPin?.coordinate.longitude, subControl: self)
    } 
    
    // 點擊下方顯示
    // 跳頁至充電站詳細資訊頁
    @IBAction func selectPinViewAction(_ sender: UIButton) {
        stationDetailClose()
        let data = selectPin?.data
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "ChargingStationDetailViewController") as? ChargingStationDetailViewController else { return }
        if let stationName = data?.stationName {
        }
        vc.stationCategory = data?.stationCategory ?? "YES"
        if let no = data?.stationId {
            vc.setupVCWith(stationNo: "\(no)")
        }
        navigationController?.show(vc, sender: self)
    }
    
    /// Mask Button
    @IBAction func displayMasks(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true {
            if UserDefaults.standard.bool(forKey: "MasksDescription") == false {
                ShowMessageView.showSingleMessage(title: "", message: "本功能所提供之資料來自健保署資料開放服務，實際數量仍以藥局存量為主。", sureBtnTitle: "確定") { (isOk) in
                    if isOk == true {
                        UserDefaults.standard.set(true, forKey: "MasksDescription")
                        self.callMasksAPI()
                    }
                }
            } else {
                callMasksAPI()
            }
            masksDetailBtn.setImage(UIImage(named: MaskImages.maskON.rawValue), for: .normal)
        } else {
            masksDetailBtn.setImage(UIImage(named: MaskImages.maskOFF.rawValue), for: .normal)
            mapView.clear()
            maskDetailArray = []
            self.showMoreMerkerWith(stationListDatas: self.stationListArray)
        }
    }
     
    // 篩選 - reset defult
    @IBAction func reserFilterAction(_ sender: UIButton) {
        defaultSwitch()
    }
    
    // 篩選 - cancel filter view
    @IBAction func cancelFliterView(_ sender: UIButton) {
        filterBtn.setImage(#imageLiteral(resourceName: "button_floating_filter_default"), for: .normal)
        filterBtn.isSelected = false
        checkFilterType()
        self.naviBtnBottom.constant = KJLCommon.isX() == true ? 204 : 170
        UIView.animate(withDuration: 0.3) {
            self.fliterViewBottomConstraint.constant =  -366
            self.view.layoutIfNeeded()
        }
    }
    
    // 篩選 - Porsche, AC, DC, Available
    @IBAction func filterSwitch(_ sender: UISwitch) {
        DispatchQueue.main.async {
            self.callAPIRequestForStationList(queryString: nil, queryType: .Station , type:"1")
        }
        checkFilterType()
    }
    
    
    // MARK: - 自訂Func
    // cancel fliter View
    private func closeFliterView() {
        if filterBtn.isSelected == true {
            filterBtn.setImage(#imageLiteral(resourceName: "button_floating_filter_default"), for: .normal)
            filterBtn.isSelected = false
            checkFilterType()
            fliterViewBottomConstraint.constant = -366
        }
    }
    
    private func checkFilterType() {
        if porscheSwitch.isOn       == false
            && acSwitch.isOn        == true
            && dcSwitch.isOn        == true
            && availableSwitch.isOn == false
        {
            filterBtn.setImage(UIImage(named: "button_floating_filter_default"), for: .normal)
        } else {
            filterBtn.setImage(UIImage(named: "button_floating_filter_click"), for: .normal)
        }
    }
    
    private func defaultSwitch() {
        porscheSwitch.isOn  = false
        acSwitch.isOn       = true
        dcSwitch.isOn       = true
        availableSwitch.isOn = false
        checkFilterType() 
        callAPIRequestForStationList(queryString: nil, queryType: .Station , type:"1")
    }
    
    private func showSearchTable() {
        self.searchBarLine.isHidden = false
        UIView.animate(withDuration: 0.3) {
            var constant: CGFloat = 100
            if KJLCommon.isX() == true {
                constant = 140
            }
            let toTop = (self.mapView.frame.height - constant )
            print("toTop \(toTop)")
            self.searchBarBottomConstraint.constant = (toTop * 0.55) - 30
            self.serchBorderViewHeight.constant = toTop * 0.55
            self.searchTable.isHidden = false
            self.view.layoutIfNeeded()
        }
    }
    
    private func closeSearchTable() {
        self.searchBarLine.isHidden = true
        UIView.animate(withDuration: 0.3) {
            self.searchBarBottomConstraint.constant = 15
            self.serchBorderViewHeight.constant = 36
            self.searchTable.isHidden = true
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func searchChange(obj: Notification) {
        cellStatus = .Search
        let pattern = "^[\\u4E00-\\u9FA5A-Za-z0-9_]+$"
        let matcher = MyRegex(pattern)
        let chinese = searchField.text ?? ""
        self.searchDataArray.removeAll()
        self.googleSearchArray.removeAll()
        self.searchTable.reloadData()
        if searchText != searchField.text {
            searchText = searchField.text ?? ""
            if searchText.count > 0 {
                callStationSerch(searchText) {
                    //導入Google Map
                    self.searchPlaceFromGoogle(place: self.searchText) {
                        if self.googleSearchArray.count < 3 {
                            //    self.searchGeocodeFromGoogle(place: self.searchText)
                        }
                    }
                }
            } else {
                self.searchDataArray = []
                self.googleSearchArray = []
                searchTable.reloadData()
            }
        }
    }
    
    fileprivate func gotoLogin() {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SelectLoginViewController") as! SelectLoginViewController
        let nav:UINavigationController = UINavigationController.init(rootViewController: vc)
        nav.isNavigationBarHidden = true
        nav.modalPresentationStyle = .overFullScreen
        nav.modalTransitionStyle   = .crossDissolve
        self.present(nav, animated: true, completion: nil)
    }
    
    fileprivate func onOffFavoriteStatus(onOff: KJLRequest.RequestBOOL) {
        favoriteBtn.isSelected = onOff == .FALSE ? false : true
        favoriteBtn.setImage( onOff == .FALSE ?  #imageLiteral(resourceName: "icon_bar_favorite_default") : #imageLiteral(resourceName: "icon_bar_favorite_click") , for: .normal)
    }
    
    //MARK:- Google Geocode Api
    private func searchGeocodeFromGoogle(place: String) {
        var geoCodeURL = "https://maps.googleapis.com/maps/api/geocode/json?address=\(place)+in+Taiwan&key=\(KJLRequest.googleKey)"
        geoCodeURL = geoCodeURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        var urlRequest = URLRequest(url: URL(string: geoCodeURL)!)
        urlRequest.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: urlRequest) {[weak self] (data, response, error) in
            guard data != nil else { return }
            let jsonDic = try? JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
            if let dics = jsonDic as? Dictionary<String, AnyObject> {
                guard let results = dics["results"] as? [Dictionary<String,AnyObject>]
                    else { return }
//                print("GeoCode json: \(results)")
                if self?.googleSearchArray.count ?? 0 < 3 {
                    for i in 0..<results.count {
                        if self?.googleSearchArray.count ?? 0 < 3 {
                            self?.googleSearchArray.insert(results[i], at: 0)
                        }
                    }
                    DispatchQueue.main.async {
                        self?.searchTable.reloadData()
                    }
                }
            }
        }
        task.resume()
    }
    
    // MARK: - Google Search Api
    private func searchPlaceFromGoogle(place: String, completion: @escaping ()-> Void) {
        /* Google Text Search Requests */
        // HTTP Url : https://maps.googleapis.com/maps/api/place/textsearch/output?parameters
        var strGoogleApi = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=\(place)+in+Taiwan&key=\(KJLRequest.googleKey)"
        //AIzaSyDrsdFkWJEsCElvhfQr7i5RXYevNwNgBqM
        strGoogleApi = strGoogleApi.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        var urlRequest = URLRequest(url: URL(string: strGoogleApi)!)
        urlRequest.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: urlRequest) {[weak self] (data, response, error) in
            if error == nil {
                if data != nil {
                    let jsonDic = try? JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
//                    print(jsonDic)
                    if let dics = jsonDic as? Dictionary<String, AnyObject> {
                        if let results = dics["results"] as? [Dictionary<String,AnyObject>] {
                            print("json: \(results)")
                            for i in 0..<results.count {
                                if i < 3 {
                                    self?.googleSearchArray.append(results[i])
                                }
                            }
                            DispatchQueue.main.async {
                                self?.searchTable.reloadData()
                            }
                            completion()
                        }
                    }
                }
            }  
        }
        task.resume()
    }
    
    func initView() {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "LeftViewController") as? LeftViewController {
            vc.delegate = self
            vc.mainController = self
            leftVC = vc
            leftView.addSubview(vc.view)
            vc.view.frame = leftView.bounds
            addChildViewController(vc)
        }
        //        changeViewController(type: .Map)
        /// API-001 版本確認
        versionCheck()
        /// API-002 系統參數
        KJLRequest.requestForSystemVerb { (response) in
            guard let response = response as? SystemVerbClass else { return }
            KJLCommon.saveSystemVerbClass(data: response)
            printLog(response)
        }
    }
    
    @objc private func versionCheck() {
        /// API-001 版本確認
        KJLRequest.requestForVersionCheck { (response) in
            guard let response = response as? VersionCheckClass else { return }
            let urlStr = response.datas?.appUrl ?? ""
            if response.datas?.required == true {
                ShowMessageView.showMessage(title: "版本更新通知", message: "已發布更新版本，請至App Store更新應用程式", completion: { (isTrue) in
                    if let url = URL(string: urlStr) {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(url)
                        }
                    }
                })
            } else {
                if response.datas?.newVersion == true {
                    ShowMessageView.showQuestionWithCustomButton(messageTitle: "版本更新通知", message: "已發布更新版本，請至App Store更新應用程式", sureBtnTitle: "確定", cancelBtnTitle: "稍後") { (isTrue) in
                        if isTrue ,let url = URL(string: urlStr) {
                            if #available(iOS 10.0, *) {
                                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                            } else {
                                UIApplication.shared.openURL(url)
                            }
                        }
                    }
                }
            }
            printLog(response)
        }
    }
    
    func showMasksOnePin(_ detail: MaskDetailModel.Data ,action: (()->())?) {
        carDetailView.isHidden = true
        supportLabel.isHidden = true
        masksDetailView.isHidden = false
        self.selectPinViewBottom.constant = -272
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.2, animations: {
            if KJLCommon.isX() == true {
                self.selectPinViewBottom.constant = 82
                self.naviBtnBottom.constant = 20
            } else {
                self.selectPinViewBottom.constant = 48
                self.naviBtnBottom.constant = 20
            }
            self.view.layoutIfNeeded()
        }) { (bool) in
            if let callback = action {
                callback()
            }
        }
        
        let lat = Float(detail.latitude ?? "0") ?? 0
        let long = Float(detail.longitude ?? "0") ?? 0
        LocationManagers.updateLocation { (status, location) in
            if  status == .Success {
                let current = CLLocation(latitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!)
                let location = CLLocation(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(long))
                let distance = current.distance(from: location)
                let meter = "\(distance)"
                let km = String(format: "%.1f", (Float(meter) ?? 0)/1000)
            }
        }
    }
    
    func displayStationDetail(pin: CustomPin, action: (()->())?) {
        closeFliterView()
        closeSearchTable()
        self.stationMaskHeightConstraint.constant = 0
        selectPin = pin
        guard let data = selectPin?.data else { return }
        let lat = Float(data.latitude ?? "0") ?? 0
        let long = Float(data.longitude ?? "0") ?? 0
        LocationManagers.updateLocation { (_, _) in}
        let myLocation = LocationManager.instanceShare.myLocation.coordinate
        let current = CLLocation(latitude: myLocation.latitude, longitude: myLocation.longitude)
        let location = CLLocation(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(long))
        let distance = current.distance(from: location)
        let meter = "\(distance)"
        self.stationKM = String(format: "%.2f", (Float(meter) ?? 0)/1000)
         print("stationKM \(stationKM)")
        self.callStationHistory(data.stationId!)
        self.callAPIRequestForSevenDaysCharge(stationNo: data.stationId!)
        self.requestForStationDetail(stationId: data.stationId!) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                UIView.animate(withDuration: 0.3) {
                    var gapConstant: CGFloat = 90
                    if KJLCommon.isX() == true {
                        self.naviBtnBottom.constant = 20
                        gapConstant = 100
                    } else {
                        self.naviBtnBottom.constant = 20
                    }
                    self.stationMaskView.alpha = 0
                    self.stationDetailBottomConstraint.constant = -self.view.frame.height + self.stationDemoHeight + gapConstant
                    self.stationTable.isScrollEnabled = false
                    self.view.layoutIfNeeded()
                    self.view.setNeedsLayout()
                }
            }
        }
// 增加回談效果
//        self.stationDetailBottomConstraint.constant = -self.view.frame.height
//        self.view.layoutIfNeeded()
        
    }
    
    // MARK: - API
    /// API-301 站點查詢
    func callAPIRequestForStationList(queryString: String?, queryType: KJLRequest.RequestQueryType , type: String, _ tag: Bool = false, _ searchLocation2D: CLLocationCoordinate2D? = nil) {
        print("301進入")
        KJLRequest.requestForStationList(queryType: queryType, queryString: queryString, type: type) {[weak self] (response) in
            guard let response = response as? StationListClass else { return }
            
            if queryString == nil {
                if let lists = response.datas {
                    self?.stationListArray = lists
                    self?.showMoreMerkerWith(stationListDatas: lists, nil, tag, searchLocation2D)
                } else {
                    self?.stationListArray = []
                    self?.showMoreMerkerWith(stationListDatas: [])
                }
            }
             
            self?.favoriteListArray.removeAll()
            if let lists = response.datas {
                for station in lists {
                    if station.favourite == true {
                        self?.favoriteListArray.append(station)
                    }
                }
            } else {
                self?.favoriteListArray = []
            }
            self?.searchTable.reloadData()
            
//            let notFirst = UserDefaults.standard.object(forKey: "notFirst") as? Bool
//            if notFirst == false { return }
//            if self?.isFristInto == true {
//                self?.isFristInto = false
//            }
        }
        print("301結束")
    }
    
    /// search bar
    fileprivate func callStationSerch(_ queryString: String, completion: @escaping ()-> Void ) {
        KJLRequest.requestForStationSearch(queryString) {[weak self] (response) in
            guard let response = response as? StationSearchModel else { return }
            print("response: \(response)")
            if let data = response.datas {
                self?.searchDataArray = data
                self?.searchTable.reloadData()
            } else {
                self?.searchDataArray = []
                self?.googleSearchArray = []
                self?.searchTable.reloadData()
            }
            completion()
        }
    }
    
    /// tap marker ,get infomation
    fileprivate func callStationHistory(_ queryString: String) {
        KJLRequest.requestForSetStationHistory(queryString) { (response) in
            guard (response as? NormalClass) != nil else { return }
        }
    }
    
    /// Station Detail 充電站資訊
    func requestForStationDetail(stationId:String, completion: (()-> Void)? ) {
        KJLRequest.requestForStationDetail(stationId: stationId) { [weak self](response) in
            guard let response = response as? StationDetailClass else { return }
            self?.stationDetailData = nil
            self?.stationDetailData = response
            self?.stationTable.reloadData()
            if let callback = completion {
                callback()
            }
        }
    }
    
    /// Chart 近七天充電次數統計
    private func callAPIRequestForSevenDaysCharge(stationNo: String) {
        KJLRequest.requestForSevenDaysCharge(stationId: stationNo) {[weak self] (response) in
            guard let response = response as? SevenDaysChargeClass else { return }
            if let dataArray = response.datas {
                for i in 0..<dataArray.count {
                    let data = dataArray[i]
                    if let chargeDate = data.chargeDate {
                        self?.sevenChargeDateArray.append(chargeDate)
                    }
                    
                    if let chargeTotal = data.chargeTotal {
                        self?.sevenChargeTotalArray.append(Double(chargeTotal))
                    }
                }
            }
        }
    }
}

// MARK: - Extension
extension MainViewController {
    //MARK: 放上所有的大頭針
    func showMoreMerkerWith(stationListDatas: [StationListClass.Data], _ indexPath: Int? = nil,_ tag: Bool = false, _ searchLocation2D: CLLocationCoordinate2D? = nil) {
        self.showMarkerArray.removeAll()
        self.pinArray.removeAll()
        mapView.clear()
        clusterManager.clearItems()
        var index = 0
        var tempDatas = [StationListClass.Data]()
        for aData in stationListDatas {
            if porscheSwitch.isOn == true {
                if aData.stationCategory?.lowercased() == "porsche" {
                    if acSwitch.isOn == true && dcSwitch.isOn == true && availableSwitch.isOn == true {
                        if aData.status == "01" || aData.status == "02" {
                            tempDatas.append(aData)
                        }
                    } else if acSwitch.isOn == true && dcSwitch.isOn == true {
                        tempDatas.append(aData)
                    } else if acSwitch.isOn == true && availableSwitch.isOn == true {
                        if aData.hasAC == true {
                            if aData.status == "01" || aData.status == "02" {
                                tempDatas.append(aData)
                            }
                        }
                    } else if dcSwitch.isOn == true && availableSwitch.isOn == true {
                        if aData.hasDC == true {
                            if aData.status == "01" || aData.status == "02" {
                                tempDatas.append(aData)
                            }
                        }
                    } else if acSwitch.isOn == true {
                        if aData.hasAC == true {
                            tempDatas.append(aData) 
                        }
                    } else if dcSwitch.isOn == true {
                        if aData.hasDC == true {
                            tempDatas.append(aData)
                        }
                    }
                }
            } else {
                if acSwitch.isOn == true && dcSwitch.isOn == true && availableSwitch.isOn == true {
                    if aData.status == "01" || aData.status == "02" {
                        tempDatas.append(aData)
                    }
                } else if acSwitch.isOn == true && availableSwitch.isOn == true {
                    if aData.hasAC == true {
                        if aData.status == "01" || aData.status == "02" {
                            tempDatas.append(aData)
                        }
                    }
                } else if dcSwitch.isOn == true && availableSwitch.isOn == true {
                    if aData.hasDC == true {
                        if aData.status == "01" || aData.status == "02" {
                            tempDatas.append(aData)
                        }
                    }
                } else if acSwitch.isOn == true && dcSwitch.isOn == true {
                    tempDatas.append(aData)
                } else if acSwitch.isOn == true {
                    if aData.hasAC == true {
                        tempDatas.append(aData)
                    }
                } else if dcSwitch.isOn == true {
                    if aData.hasDC == true {
                        tempDatas.append(aData)
                    }
                } else {
                    if aData.stationCategory?.lowercased() == "porsche" {
                        if aData.hasAC == true && aData.hasDC == true && aData.status == "01" && aData.status == "02" {
                            tempDatas.append(aData)
                        }
                    }
                }
            }
        }
        
        for (i,tempData) in tempDatas.enumerated() {
            let location = CLLocationCoordinate2D(latitude: Double(tempData.latitude ?? "0") ?? 0, longitude: Double(tempData.longitude ?? "0") ?? 0)
            let pin = CustomPin(coordinate: location)
            pin.title = tempData.stationName
            pin.pk = tempData.stationId ?? ""
            pin.num = "\(i + 1)"
            if i + 1 > 19 {
                pin.num = "0"
            }
            // 站點狀態：01: 可使用 , 02: 使用中 , 03: 未連線 , 04: 未營運
            var imgName: String = AnnotationType.NONE.rawValue
            let stationCarGory = tempData.stationCategory?.lowercased()

            if tempData.hasDC == true {
                if stationCarGory == "porsche"{
                    imgName = MapManger.dcPorscheICON(tempData)
                } else {
                    imgName = MapManger.dcICON(tempData)
                }
            } else {
                if stationCarGory == "porsche"{
                    imgName = MapManger.acPorscheICON(tempData)
                } else{
                    imgName = MapManger.acICON(tempData)
                }
            }
            pin.imageName = imgName
            pin.data = tempData
              
            let marker = POIItem(position: CLLocationCoordinate2DMake(location.latitude, location.longitude), name: pin.title!)
            
            /// check empty total
            var emptyToString = ""
            if tempData.connection == false {
                var total = ""
                if tempData.hasDC == true {
                    total = "\(tempData.dcTotal ?? 0)"
                } else {
                    total = "\(tempData.acTotal ?? 0)"
                }
                emptyToString = total
            } else {
                if tempData.hasDC == true {
                    let empty = "\(tempData.dcEmpty ?? 0)"
                    let total = "\(tempData.dcTotal ?? 0)"
                    emptyToString = empty + "/" + total
                } else {
                    if stationCarGory == "porsche"{
                        let total = "\(tempData.acTotal ?? 0)"
                        emptyToString = total
                    } else {
                        let empty = "\(tempData.acEmpty ?? 0)"
                        let total = "\(tempData.acTotal ?? 0)"
                        emptyToString = empty + "/" + total
                    }
                }
            }
            /// 檢查tag marker
            if tag == true && searchLocation2D?.latitude == Double(tempData.latitude!) && searchLocation2D?.longitude == Double(tempData.longitude!){
                if tempData.connection == false {
                    let toImage = ImageManager.share.textToImage(drawText: emptyToString, inImage: UIImage(named: pin.imageName) ?? UIImage())
                    let image   = ImageManager.share.imageWithImage(image: toImage, scaledToSize: CGSize(width: 90, height: 90))
                    marker.iconImage = image
                } else {
                    if tempData.isBussinessTime?.lowercased() == "n" {
                        marker.iconImage = UIImage(named: imgName)
                    } else {
                        let toImage = ImageManager.share.textToImage(drawText: emptyToString, inImage: UIImage(named: pin.imageName) ?? UIImage())
                        let image   = ImageManager.share.imageWithImage(image: toImage, scaledToSize: CGSize(width: 90, height: 90))
                        marker.iconImage = image
                    }
                }
            } else {
                if tempData.connection == false {
                    let toImage = ImageManager.share.textToImage(drawText: emptyToString, inImage: UIImage(named: pin.imageName) ?? UIImage())
                    marker.iconImage = toImage
                } else {
                    if tempData.isBussinessTime?.lowercased() == "n" {
                        marker.iconImage = UIImage(named: imgName)
                    } else {
                        let toImage = ImageManager.share.textToImage(drawText: emptyToString, inImage: UIImage(named: pin.imageName) ?? UIImage())
                        marker.iconImage = toImage
                    }
                }
            }
            
            marker.index = index
            index += 1
            self.showMarkerArray.append(marker)
            self.pinArray.append(pin)
            clusterManager.add(marker)
        }
        clusterManager.cluster()
    }
    
    class func calculateDistance(lat: Float, long: Float) -> String {
        if lat == 0 || long == 0 {
            return ""
        }
        // 25.178039, 121.443685
        //        let cl = CLLocation(latitude: CLLocationDegrees(25.178039), longitude: CLLocationDegrees(121.443685))
        if let myLocation = MainViewController.mainController?.mapView.camera.target {
            let current = CLLocation(latitude: myLocation.latitude, longitude: myLocation.longitude)
            let location = CLLocation(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(long))
            let distance = current.distance(from: location)
            return "\(distance)"
        }
        return ""
    }
}

// MARK: - LeftView Delegate
extension MainViewController: LeftViewControllerDelegate {
    func closeLeftView() {
        leftViewAction(nil)
    }
    
    func hideLeftView() {
        leftScroll.setContentOffset(CGPoint(x: leftScroll.frame.width, y: 0), animated: true)
    }
}

// MARK: - TextField Delegate
extension MainViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder() 
        return true
    }
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (self.googleSearchArray.count == 0 || textField.text?.count == 0) {
            if (self.googleAnnotationArray.count > 0) {
                for marker in googleAnnotationArray {
                    marker.map = nil
                }
            }
            self.googleAnnotationArray.removeAll()
        }
        self.showMoreMerkerWith(stationListDatas: self.stationListArray)
        onOffFavoriteStatus(onOff: .FALSE)
        cellStatus = .Search
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        onOffFavoriteStatus(onOff: .FALSE)
        cellStatus = .Search
        searchTable.reloadData()
        callStationSerch(""){ }
        showSearchTable()
    }
}

// MARK: - TableView DataSource & Delegate
extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == searchTable {
            if cellStatus == .Search {
                return searchDataArray.count + googleSearchArray.count
            } else { 
                cellStatus = .Favorite
                return favoriteListArray.count == 0 ? 1 : favoriteListArray.count
            }
        } else {
            cellStatus = .Normal
            #warning("charts is hidden")
            /// charts = 5 , hidden charts = 4
            return stationDetailData == nil ? 0 : 4
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == searchTable {
            if cellStatus == .Normal {
                let normalDataCell = tableView.dequeueReusableCell(withIdentifier: "NormalDataCell", for: indexPath) as! NormalDataCell
                normalDataCell.selectionStyle = .none
                return normalDataCell
            } else if cellStatus == .Search {
                let cell = tableView.dequeueReusableCell(withIdentifier: "NormalCell", for: indexPath) as! NormalCell
                if (indexPath.row <= searchDataArray.count - 1) {
                    let data = searchDataArray[indexPath.row]
                    cell.list = data
                } else {
                    let data = googleSearchArray[indexPath.row - searchDataArray.count]
                    if data["name"] != nil {
                        cell.titleCell.text      = data["name"] as? String ?? ""
                        cell.stationAddress.text = data["formatted_address"] as? String ?? ""
                        cell.stationICON.image   = UIImage(named: "icon_search_location_default")
                    } else {
                        cell.stationAddress.text = data["formatted_address"] as? String ?? ""
                        cell.stationICON.image   = UIImage(named: "icon_search_location_default")
                    }
                }
                return cell
            } else {
                if favoriteListArray.count == 0 {
                    let cell = UITableViewCell()
                    cell.textLabel?.text = LocalizeUtils.localized(key: "noFavoriteMessage")
                    cell.textLabel?.textColor = #colorLiteral(red: 0.7921568627, green: 0.7960784314, blue: 0.8, alpha: 1)
                    cell.textLabel?.textAlignment = .left
                    cell.textLabel?.font = UIFont(name: "PorscheNext-Regular", size: 16)
                    cell.selectionStyle = .none
                    return cell
                } else {
                    let favoriteCell = tableView.dequeueReusableCell(withIdentifier: "FavoriteCell", for: indexPath) as! FavoriteCell
                    let data = self.favoriteListArray[indexPath.row]
                    let lat = Float(data.latitude ?? "0") ?? 0
                    let long = Float(data.longitude ?? "0") ?? 0
                    LocationManagers.updateLocation { (_, _) in }
                    
                    let myLocation = LocationManager.instanceShare.myLocation.coordinate
                    if myLocation.latitude != 0.0 && myLocation.longitude != 0.0 {
                        let current = CLLocation(latitude: (myLocation.latitude), longitude: (myLocation.longitude))
                        let location = CLLocation(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(long))
                        let distance = current.distance(from: location)
                        let meter = "\(distance)"
                        let km = String(format: "%.1f", (Float(meter) ?? 0)/1000)
                        favoriteCell.km = km
                    }
                    
                    favoriteCell.listArray = self.favoriteListArray[indexPath.row]
                    return favoriteCell
                }
            }
        } else {
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "StationDemoCell", for: indexPath) as! StationDemoCell
                cell.km = stationKM
                cell.list = selectPin?.data
                cell.stationCategory = selectPin?.data?.stationCategory ?? "YES"
                cell.data = stationDetailData?.datas 
                cell.delegate = self
                return cell
            case 1:
                let businessHoursCell = tableView.dequeueReusableCell(withIdentifier: "BusinessHoursCell", for: indexPath) as! BusinessHoursCell
                businessHoursCell.list = selectPin?.data
                businessHoursCell.data = stationDetailData?.datas
                return businessHoursCell
            case 2:
                let detailCell = tableView.dequeueReusableCell(withIdentifier: "StationDetailCell", for: indexPath) as! StationDetailCell
                detailCell.stationCategory = selectPin?.data?.stationCategory ?? "YES"
                detailCell.data = stationDetailData
                detailCell.delegate = self
                return detailCell
            case 3:
                 let chargerTypeCell = tableView.dequeueReusableCell(withIdentifier: "ChargerTypeCell", for: indexPath) as! ChargerTypeCell
                chargerTypeCell.data = stationDetailData?.datas
                return chargerTypeCell
            case 4:
                let chartCell = Bundle.main.loadNibNamed("ChargingChartsCell", owner: self, options: nil)?[0] as! ChargingChartsCell
                chartCell.setChartViewData(dataArray: self.sevenChargeTotalArray)
                return chartCell
            default: return UITableViewCell()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        // get cell row height to station table view bottom Constraint
        if tableView == stationTable && indexPath.row == 0 {
            stationDemoHeight = cell.frame.height
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if cellStatus == .Search {
            closeSearchTable()
            if (indexPath.row <= searchDataArray.count - 1) {
//                let cell = tableView.cellForRow(at: indexPath) as! NormalCell
//                searchField.text = cell.titleCell.text
                searchField.text = ""
                //使用者點到所要的目的地
                let data = searchDataArray[indexPath.row]
                let selectAnnotation = MKPointAnnotation()
                if let lat = Double("\(data.latitude ?? "0")"),
                    let long = Double("\(data.longitude ?? "0")") {
                    let coodinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
                    self.showMoreMerkerWith(stationListDatas: self.stationListArray, nil, true, coodinate)
                    let camera = GMSCameraPosition.camera(withLatitude: coodinate.latitude, longitude: coodinate.longitude, zoom: 15)
                    self.mapView.animate(to: camera)
                    selectAnnotation.coordinate = coodinate
                }
                for temp in self.pinArray {
                    if temp.pk == data.stationId { //地圖上show圖釘
                        displayStationDetail(pin: temp, action: nil)
                        break
                    }
                }
            } else {
                let data = googleSearchArray[indexPath.row - searchDataArray.count]
                if let dataTitle = data["name"] as? String {
//                    searchField.text = dataTitle
                    searchField.text = ""
                    self.googleAnnoTitle = dataTitle
                }
                if let geometry = data["geometry"] as? Dictionary<String,AnyObject>,
                    let location = geometry["location"] as? Dictionary<String,AnyObject>,
                    let lat = location["lat"] as? Double,let long = location["lng"] as? Double {
                    let coodinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
                    let camera = GMSCameraPosition.camera(withLatitude: coodinate.latitude, longitude: coodinate.longitude, zoom: 15)
                    self.mapView.animate(to: camera)
                    let marker = CustomGMSMarker()
                    marker.title = data["name"] as? String ?? ""
                    marker.position = coodinate
                    marker.map = self.mapView
                    marker.isNotFound = true
                    self.googleAnnotationArray.append(marker)
                }
            }
        } else if cellStatus == .Favorite {
            if favoriteListArray.count != 0 {
                let data = favoriteListArray[indexPath.row]
                let selectAnnotation = MKPointAnnotation()
                if let lat = Double("\(data.latitude ?? "0")"),
                    let long = Double("\(data.longitude ?? "0")") {
                    let coodinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
                    self.showMoreMerkerWith(stationListDatas: self.stationListArray, nil, true, coodinate)
                    let camera = GMSCameraPosition.camera(withLatitude: coodinate.latitude, longitude: coodinate.longitude, zoom: 15)
                    self.mapView.animate(to: camera)
                    selectAnnotation.coordinate = coodinate
                }
                for temp in self.pinArray {
                    if temp.pk == data.stationId { //地圖上show圖釘
                        displayStationDetail(pin: temp, action: nil)
                        break
                    }
                }
                closeSearchTable()
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}

// MARK: - ScrollView Delegate
extension MainViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == leftScroll {
            /// 測選單必須擁有的，雖然暫時關閉
            let alphaf = 0.7 - (scrollView.contentOffset.x/scrollView.frame.width * 0.7)
            maskView.alpha = alphaf
            scrollView.alpha = alphaf <= 0 ? CGFloat(0) : CGFloat(1)
        }
        
        if scrollView == stationTable {
            if scrollView.contentOffset.y < -30 {
                swipeDOWN()
            }
        }
    }
}

//MARK:- KJLRequestDelegate
extension MainViewController: KJLRequestDelegate {
    func apiFailResultWith(resCode: String , message: String) {
        ShowMessageView.showQuestionWithCustomButton(messageTitle: nil, message: message, sureBtnTitle: "前往預約記錄", cancelBtnTitle: "取消") { (isOK) in
            guard
                isOK == true,
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReserveRecordViewController") as? ReserveRecordViewController
                else { return }
            self.navigationController?.show(vc, sender: self)
        }
    }
    
    private struct AssociatedKeys {
        static var mainViewControllerKey = "MainViewController"
    }
    
    //MARK: runtime objects
    class func setMainViewController(_ mainViewController:MainViewController?){
        objc_setAssociatedObject(self, &AssociatedKeys.mainViewControllerKey, mainViewController, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
    
    class func share() -> MainViewController?{
        return objc_getAssociatedObject(self, &AssociatedKeys.mainViewControllerKey) as? MainViewController
    }
}


//MARK:- Map View Delegate
extension MainViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let poiItem = marker.userData as? POIItem, poiItem.isNotFound == false {
            
            searchField.text = ""
            
            let newCamera = GMSCameraPosition.camera(withTarget: poiItem.position, zoom: mapView.camera.zoom)
            let update = GMSCameraUpdate.setCamera(newCamera)
            mapView.animate(with: update)
            let pin = pinArray[poiItem.index]
            /// show detail
            displayStationDetail(pin: pin, action: nil)
            tapZoomMarker(mapView, marker)
            tapMarker = marker  /// save marker ot tapZoomOutMarker
            marker.icon = ImageManager.share.imageWithImage(image: marker.icon ?? UIImage(), scaledToSize: CGSize(width: 90, height: 90))
        } else {
            
        }
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        self.stationDetailClose()
        self.closeFliterView()
        self.closeSearchTable()
        onOffFavoriteStatus(onOff: .FALSE)
        if cellStatus == .Favorite {
            self.showMoreMerkerWith(stationListDatas: self.stationListArray)
        }
         
        if tapMarker != nil {
            MapManger.tapZoomOutMarker(mapView, tapMarker!)
            tapMarker?.icon = ImageManager.share.imageWithImage(image: tapMarker?.icon ?? UIImage(), scaledToSize: CGSize(width: 70, height: 70))
        }
        self.mapView.endEditing(true)
    }
    
    fileprivate func tapZoomMarker(_ mapView: GMSMapView, _ marker: GMSMarker) {
        saveMarkerIcon(mapView, marker)
        MapManger.tapZoomMarker(marker: marker, icon: markerImage)
    }
    
    fileprivate func saveMarkerIcon(_ mapView: GMSMapView, _ marker: GMSMarker) {
        if let selectMarker = mapView.selectedMarker { /// return market icon
            selectMarker.icon = markerImage
        }
        mapView.selectedMarker = marker
        markerImage = marker.icon
    }
}


//MARK:- GMUCluster Manager Delegate
extension MainViewController :GMUClusterManagerDelegate {
    func clusterManager(_ clusterManager: GMUClusterManager, didTap clusterItem: GMUClusterItem) -> Bool {
        return false
    }
    
    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool {
        let newCamera = GMSCameraPosition.camera(withTarget: cluster.position, zoom: mapView.camera.zoom + 1)
        let update = GMSCameraUpdate.setCamera(newCamera)
        mapView.animate(with: update)
        return true
    }
}

extension MainViewController: GMUClusterRendererDelegate {
    func renderer(_ renderer: GMUClusterRenderer, willRenderMarker marker: GMSMarker) {
        marker.groundAnchor = CGPoint(x: 0.5, y: 1)
        if let markerData = (marker.userData as? POIItem) {
            let icon = markerData.iconImage
            marker.icon = icon
        }
    }
} 
 

//MARK: Station Protocol
extension MainViewController: StationProtocol {
    @objc func stationDetailClose() {
        stationDetailData = nil
        stationTable.reloadData()
        onOffFavoriteStatus(onOff: .FALSE)
        self.view.endEditing(true)
        if tapMarker != nil {
            MapManger.tapZoomOutMarker(mapView, tapMarker!)
        }
        self.stationMaskHeightConstraint.constant = 0
        UIView.animate(withDuration: 0.3) {
            self.naviBtnBottom.constant = KJLCommon.isX() == true ? 204 : 170
            self.stationMaskView.alpha = 0
            self.stationDetailBottomConstraint.constant = -self.view.frame.height
            self.stationTable.isScrollEnabled = false
            self.view.layoutIfNeeded()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.filterBtn.isHidden   = false
            self.locationBtn.isHidden = false
            self.infoBtn.isHidden     = false
        }
    }
    
    func stationnNvigation() {
        KJLCommon.openNavMap(lat: selectPin?.coordinate.latitude, long: selectPin?.coordinate.longitude, subControl: self)
    }
    
    func stationBookingBtn(detail: StationDetailClass.Data, parkingSpace: Int) {
        KJLRequest.requestForReserveStart(stationId: detail.stationId ?? "", parkingSpace: parkingSpace) { (response) in
            guard let response = response as? ReserveStartModel else { return }
            if response.resCode == "0000" {
                let vc = BookingStationDeatilVC(nibName: "BookingStationDeatilVC", bundle: nil)
                vc.stationName       = detail.stationName ?? ""
                vc.parkingSpace      = parkingSpace
                vc.endTime           = response.datas?.reserveEndTime ?? ""
                UserDefaults.standard.set(parkingSpace, forKey: "BookingParkingSpace")
//                vc.stationDetailData = detail
//                vc.stationListData   = self.selectPin?.data
                let nav = UINavigationController(rootViewController: vc)
                nav.modalPresentationStyle = .overFullScreen
                nav.isNavigationBarHidden  = true
                self.present(nav, animated: true, completion: nil)
            }
        }
    }
    
    func stationCharging() {
        let profile = KJLRequest.shareInstance().profile?.datas
        if profile?.subscription == true
            || profile?.partnerMails?.count ?? 0 >= 1
            || profile?.porscheMan == true {
            let vc = ScanQRcodeViewController()
            vc.status = .Scan
            vc.content = LocalizeUtils.localized(key: "ScanCharging")
            present(vc, animated: true, completion: nil)
        } else if  profile?.voucher == true {
            if profile?.cardNum ?? "" != "" {
                let vc = ScanQRcodeViewController()
                vc.status = .Scan
                vc.content = LocalizeUtils.localized(key: "ScanCharging")
                present(vc, animated: true, completion: nil)
            } else {
                self.popMessage(content: LocalizeUtils.localized(key: "popMessageToPayment"))
            }
        } else {
            popMessage(content: LocalizeUtils.localized(key: "mainMessage"))
        }
    }
    
    func stationUnlock() {
        let vc = UnlockChargeVC(nibName: "UnlockChargeVC", bundle: nil)
        let nav = UINavigationController(rootViewController: vc)
        nav.isNavigationBarHidden = true
        nav.modalPresentationStyle = .overFullScreen
        present(nav, animated: true, completion: nil)
    }
    
    func stationFavorite(detail: StationDetailClass.Data?, idAdd: KJLRequest.RequestBOOL) {
        guard KJLRequest.isLogin() == true else {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SelectLoginViewController") as! SelectLoginViewController
            let nav:UINavigationController = UINavigationController.init(rootViewController: vc)
            nav.isNavigationBarHidden = true
            nav.modalPresentationStyle = .overFullScreen
            nav.modalTransitionStyle   = .crossDissolve
            self.present(nav, animated: true, completion: nil)
            return }
        
        let lat = Double("\(selectPin?.data?.latitude ?? "0")") ?? 0
        let long = Double("\(selectPin?.data?.longitude ?? "0")") ?? 0
        let coodinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
        
        guard let data = detail else { return }
        KJLRequest.requestForAddFavourite(stationId: data.stationId ?? "", add: idAdd) {[weak self] (response) in
            guard (response as? NormalClass) != nil else { return }
            guard let data = self?.selectPin?.data else { return }
            DispatchQueue.main.async {
                self?.requestForStationDetail(stationId: data.stationId!, completion: nil)
                self?.callAPIRequestForStationList(queryString: nil, queryType: .Station, type: "1", true, coodinate)
            }
        }
    }
    
    func stationChangeheight() { 
        if self.stationDetailBottomConstraint.constant == -self.view.frame.height + 500 {
            self.stationMaskHeightConstraint.constant = self.view.frame.height
            naviBtnBottom.constant = 200
            displayBtns(true)
            UIView.animate(withDuration: 0.3) {
                self.stationMaskView.alpha = 0.5
                self.stationDetailBottomConstraint.constant = -40
                self.stationTable.isScrollEnabled = true
                self.view.layoutIfNeeded()
            }
        } else {
            // station View
                self.stationMaskHeightConstraint.constant = 0
                naviBtnBottom.constant = 20
                self.stationTable.isScrollEnabled = false
                UIView.animate(withDuration: 0.3, animations: {
                    self.stationMaskView.alpha = 0
                    self.stationDetailBottomConstraint.constant = -self.view.frame.height + 500
                    self.view.layoutIfNeeded()
                }) { (_) in
                    self.displayBtns()
                }
        }
    }
} 


//MARK: Station detail cell
extension MainViewController: StationDetailDelegate {
    func callOut() {
        let phone = stationDetailData?.datas?.stationPhone ?? ""
        let phoneUrl = URL(string: "tel://\(phone)")
        UIApplication.shared.open(phoneUrl!, options: [:], completionHandler: nil)
    } 
}


//MARK: Home VC
extension MainViewController {
    @objc func closeAllView() {
        closeFliterView()
        closeSearchTable()
        stationDetailClose()
    }
}


extension MainViewController: ShowCheckMessageDelegate {
    func tapConfirm(tagCount: Int) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func tapCancel() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func popMessage(content: String) {
        let vc = ShowCheckMessageVC()
        vc.toContent = content
        vc.toTitle   = ""
        vc.delegate  = self
        vc.custom    = .one
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        present(vc, animated: true, completion: nil)
    }
}

