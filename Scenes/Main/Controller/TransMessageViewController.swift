//
//  TransMessageViewController.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/5/20.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class TransMessageViewController: UIViewController {
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var contentTextView: UITextView!
    @IBOutlet weak var moneyLabel: UILabel! 
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var paymentCount: UILabel!
    var money  = ""
    var tranNo: Int64 = 0
    var type   = "" 
    var total  = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        settingView() 
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        callChargingList()
    } 
}


//MARK:- Setting View
extension TransMessageViewController {
    private func settingView() {
        bottomView.layer.cornerRadius = 5
        borderView.layer.cornerRadius = 5 
        contentTextView.text = "尚有未完成付款的充電交易\n請完成付款已繼續使用"
        moneyLabel.text = money
        paymentCount.text = "待處理比數： \(total)"  
        nextBtn.layer.cornerRadius = 5
    }
}


//MARK:- Action
extension TransMessageViewController {
    @IBAction func pushAction(_ sender: UIButton) {
        switch type {
        case "1":
            let vc = CarCompleteChargingViewController()
            vc.tranNo = tranNo 
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        case "2":
//            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CompleteChargingViewController") as! CompleteChargingViewController
//            vc.tranNo = tranNo
//            vc.modalPresentationStyle = .fullScreen
//            self.present(vc, animated: true, completion: nil)
            break
        default: break
        }
    }
    
    @IBAction func tapGesture(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
}


//MARK:- Api
extension TransMessageViewController {
    private func callChargingList() {
        KJLRequest.requestForChargingListData {[weak self] (response) in
            guard let response = response as? ChargingListClass else { return }
            let trans = response.datas?.trans ?? []
            guard trans.count > 0 else { return } 
            self?.moneyLabel.text  = "\(trans[0].paymentAmount ?? 0)"
            self?.paymentCount.text = "待處理筆數： \(trans.count)"
            self?.tranNo = trans[0].tranNo ?? 0
            self?.type   = "\(trans[0].type ?? "")"
        }
    }
}
