//
//  ChargerTypeCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/15.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class ChargerTypeCell: UITableViewCell {
    
    @IBOutlet weak var chargerTypeTitle: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var one: UILabel!
    @IBOutlet weak var two: UILabel!
    @IBOutlet weak var three: UILabel!
    @IBOutlet weak var four: UILabel!
    @IBOutlet weak var five: UILabel!
    @IBOutlet weak var leftBtn: UIButton!
    @IBOutlet weak var rightBtn: UIButton!
    @IBOutlet weak var redview: UIView!
    @IBOutlet weak var redLeadingConstrint: NSLayoutConstraint!  /// defult = -5 , next + 40
    
    var chargerGunlists: [SocketListClass.Data] = []
    var stationDetailGunList: [String] = []
    var totalIndex = 0
    var index = 1
    var counts = 0
    
    var data: StationDetailClass.Data? {
        didSet {
            guard let data = data else { return }
            stationDetailGunList = []
            var acArray = [""]
            var dcArray = [""]
            if data.interface_AC != "" {
                acArray = data.interface_AC?.components(separatedBy: ",") ?? []
                stationDetailGunList += acArray
            }
            if data.interface_DC != "" {
                dcArray = data.interface_DC?.components(separatedBy: ",") ?? []
                stationDetailGunList += dcArray
            }
            totalIndex = stationDetailGunList.count
            checkShowPage()
            collectionView.reloadData()
            print("stationDetailGunList#--\(stationDetailGunList)")
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        chargerTypeTitle.text = LocalizeUtils.localized(key: "StationGun")
        collectionView.register(UINib(nibName: "ChargerTypeCollectionCell", bundle: nil), forCellWithReuseIdentifier: "ChargerTypeCollectionCell")
        collectionView.dataSource = self
        collectionView.delegate   = self
        
        one.isHidden = false
        numberIsHidden(true)
        checkShowPage()
        leftBtn.isHidden  = true
        rightBtn.isHidden = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func numberIsHidden(_ hidden: Bool) {
        two.isHidden   = hidden
        three.isHidden = hidden
        four.isHidden  = hidden
        five.isHidden  = hidden
    }
    
    func checkIsHidden() {
        if counts == 2 {
            two.isHidden   = false
            three.isHidden = true
            four.isHidden  = true
            five.isHidden  = true
        } else if counts == 3 {
            two.isHidden   = false
            three.isHidden = false
            four.isHidden  = true
            five.isHidden  = true
        } else if counts == 4 {
            two.isHidden   = false
            three.isHidden = false
            four.isHidden  = false
            five.isHidden  = true
        } else {
            numberIsHidden(false)
        }
    }
    
    fileprivate func checkShowPage() {
        if totalIndex < 3 {
            leftBtn.setImage(UIImage(named: "icon_previous_disable"), for: .normal)
            rightBtn.setImage(UIImage(named: "icon_next_disable"), for: .normal)
            two.isHidden   = true
            three.isHidden = true
            four.isHidden  = true
            five.isHidden  = true
        } else {
            if (totalIndex % 2) != 0 {
                counts = (totalIndex / 2) + 1
                checkIsHidden()
            } else {
                counts = (totalIndex / 2)
                checkIsHidden()
            }
        }
    }
    
    @objc func moveRedLine() {
        if index == 1 {
            UIView.animate(withDuration: 0.5) {
                self.redLeadingConstrint.constant = -5
            }
        } else {
            UIView.animate(withDuration: 0.5) {
                self.redLeadingConstrint.constant = CGFloat(-5 + (40 * (self.index - 1)))
            }
        }
    }
    
    @IBAction func leftAction(_ sender: UIButton) {
        if totalIndex < 3 {
            leftBtn.setImage(UIImage(named: "icon_previous_disable"), for: .normal)
            rightBtn.setImage(UIImage(named: "icon_next_disable"), for: .normal)
            return
        }
        if index <= 0 {
            index = 0
            leftBtn.setImage(UIImage(named: "icon_previous_disable"), for: .normal)
            rightBtn.setImage(UIImage(named: "icon_next_default"), for: .normal)
        } else {
            leftBtn.setImage(UIImage(named: "icon_previous_default"), for: .normal)
            rightBtn.setImage(UIImage(named: "icon_next_disable"), for: .normal)
            if index >= counts {
                index -= 1
                collectionView.scrollToItem(at: IndexPath(item: index - 1, section: 0), at: .left, animated: true)
                collectionView.layoutIfNeeded()
            }
        }
        checkIsHidden()
    }
    
    @IBAction func rightACtion(_ sender: UIButton) {
        if totalIndex < 3 {
            leftBtn.setImage(UIImage(named: "icon_previous_disable"), for: .normal)
            rightBtn.setImage(UIImage(named: "icon_next_disable"), for: .normal)
            return
        }
        
        if index >= counts {
            leftBtn.setImage(UIImage(named: "icon_previous_default"), for: .normal)
            rightBtn.setImage(UIImage(named: "icon_next_disable"), for: .normal)
        } else {
            leftBtn.setImage(UIImage(named: "icon_previous_disable"), for: .normal)
            rightBtn.setImage(UIImage(named: "icon_next_default"), for: .normal)
            if index < counts {
                index += 1
                collectionView.scrollToItem(at: IndexPath(item: index + 1, section: 0), at: .right, animated: true)
                rightBtn.setImage(UIImage(named: "icon_next_default"), for: .normal)
                collectionView.layoutIfNeeded()
            }
        }
        checkIsHidden()
    }
}


//MARK: Charger Type Cell -- Colection View delegate & datasource
extension ChargerTypeCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return chargerGunlists.count
        return stationDetailGunList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChargerTypeCollectionCell", for: indexPath) as! ChargerTypeCollectionCell
        cell.garyLine.isHidden = indexPath.item % 2 == 1 ? true : false 
        if stationDetailGunList.count == 1 {
            cell.garyLine.isHidden = true
        }
        cell.gunList = stationDetailGunList[indexPath.item]
        print(indexPath.item)
        return cell
    }
}


extension ChargerTypeCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        /// 避免layout 跑掉,記得去storyboard 關掉collectionView Estimate Size = none
        var displayCount: CGFloat = 2
        if stationDetailGunList.count == 1 {
            displayCount     = 1
            redview.isHidden = true
            one.isHidden     = true
        } else if stationDetailGunList.count == 2  {
            redview.isHidden = true
            one.isHidden     = true
        } else {
            redview.isHidden = false
            one.isHidden     = false
        }
        let width = (collectionView.frame.width - 10) / displayCount
        return CGSize(width: width, height: collectionView.frame.width - 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    /// bottom spacing
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}

// MARK: - ScrollView Delegate
extension ChargerTypeCell: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        print(scrollView.contentOffset.x)
        DispatchQueue.main.async {
//            print(scrollView.contentOffset.x)
            if scrollView.contentOffset.x > 10 {
                if self.index < self.counts {
                    self.index += 1
                    self.moveRedLine()
                }  
            } else {
                if self.index >= self.counts {
                    self.index -= 1
                    self.moveRedLine()
                }
            }
        }
    }
}
