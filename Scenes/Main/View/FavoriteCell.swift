//
//  FavoriteCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/14.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class FavoriteCell: UITableViewCell {

    @IBOutlet weak var station: UILabel!
    @IBOutlet weak var powerIcon: UIImageView!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var kmLabel: UILabel!
    @IBOutlet weak var emptyCount: UILabel!
     
    var km = ""
    var listArray: StationListClass.Data? {
        didSet {
            guard let list = listArray else { return }
            station.text      = list.stationName
            contentLabel.text = list.stationAddress
            kmLabel.text      = km + "km" 
            // 站點狀態：01: 可使用 , 02: 使用中 , 03: 未連線 , 04: 未營運
            var imgName: String = AnnotationType.NONE.rawValue
            let stationCarGory = list.stationCategory?.lowercased()
            if list.hasDC == true {
                if stationCarGory == "porsche"{
                    imgName = MapManger.dcPorscheICON(list)
                } else {
                    imgName = MapManger.dcICON(list)
                }
            } else {
                if stationCarGory == "porsche"{
                    imgName = MapManger.acPorscheICON(list)
                } else{
                    imgName = MapManger.acICON(list)
                }
            }
            powerIcon.image = UIImage(named: imgName)
            
            /// check empty total
            var emptyToString = ""
            if list.connection == false {
                let total = "\(list.acTotal ?? 0)"
                emptyToString = total
            } else {
                if list.isBussinessTime?.lowercased() == "n" {
                    emptyToString = ""
                } else {
                    if list.hasDC == true {
                        let empty = "\(list.dcEmpty ?? 0)"
                        let total = "\(list.dcTotal ?? 0)"
                        emptyToString = empty + "/" + total
                    } else {
                        if stationCarGory == "porsche"{
                            let total = "\(list.acTotal ?? 0)"
                            emptyToString = total
                        } else {
                            let empty = "\(list.acEmpty ?? 0)"
                            let total = "\(list.acTotal ?? 0)"
                            emptyToString = empty + "/" + total
                        }
                    }
                }
            }
            emptyCount.text = emptyToString
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    } 
}
