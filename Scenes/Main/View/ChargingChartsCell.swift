//
//  ChargingChartsCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit
import Charts

class ChargingChartsCell: UITableViewCell {
    
    @IBOutlet weak var chartView: LineChartView!
    private var yAxisMaxValue: Double = 0
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        initChartView()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        //self.backgroundColor = .red
        if selected == true{
            self.selectionStyle = .none
            self.selectedBackgroundView?.backgroundColor = .white
        }else{
            self.selectionStyle = .none

            self.selectedBackgroundView?.backgroundColor = .white
        }
        // Configure the view for the selected state
    }
    
    func initChartView() {
        // init
        chartView.chartDescription?.enabled = false
        
        chartView.dragEnabled = false
        chartView.setScaleEnabled(false)
        chartView.pinchZoomEnabled = false
        chartView.drawGridBackgroundEnabled = false
        chartView.rightAxis.enabled = false
        chartView.leftAxis.enabled = true
        chartView.noDataText = ""
        chartView.legend.enabled = false
        
        
        
        // y axis
        let leftAxis = chartView.leftAxis                         // 获取左边 Y 轴
        leftAxis.inverted = false                                 // 是否将 Y 轴进行上下翻转
        leftAxis.axisMinimum = 0                                  // 设置 Y 轴的最小值
        leftAxis.axisMaximum = 2                                // 左邊 x 座標
        leftAxis.axisLineColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)                               // 设置 Y 轴颜色
        leftAxis.labelTextColor = #colorLiteral(red: 0.2823529412, green: 0.3294117647, blue: 0.3960784314, alpha: 1)                              // label 文字颜色
        leftAxis.labelCount = 2                                 // label 数量，数值不一定
        
        // x asix
        let xAxis = chartView.xAxis
        xAxis.labelPosition = .bottom                              // 设置 X 轴的显示位置，默认是显示在上面的
        xAxis.axisLineColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)                                   // 设置 X 轴颜色
        xAxis.labelTextColor = #colorLiteral(red: 0.2823529412, green: 0.3294117647, blue: 0.3960784314, alpha: 1)                                  // label 文字颜色
        chartView.xAxis.labelFont = .systemFont(ofSize: 7)         //刻度文字大小
        xAxis.drawGridLinesEnabled = true
        xAxis.gridColor = .clear
        
        // 計算 六天前 ~ 今天
        var dateArray = [String]()
        let currentDate = NSDate()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd"
        
        //  六天前
        for i in 0..<6
        {
            let dayBeforeToday = 6 - i
            let hoursToAddInSeconds: TimeInterval = TimeInterval(dayBeforeToday * 24 * 60 * 60)
            let calculatedDate = currentDate.addingTimeInterval(-hoursToAddInSeconds)
            
            let convertedDate = dateFormatter.string(from: calculatedDate as Date)
            dateArray.append(convertedDate)
        }
        // 今天
        let currentDateString = dateFormatter.string(from: currentDate as Date)
        dateArray.append(currentDateString)
        
        //自定义刻度标签文字
        let xValues = dateArray
        chartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: xValues)
        chartView.xAxis.labelCount = 7
        chartView.xAxis.granularity = 2 //最小间隔
        chartView.xAxis.axisMinimum = 0 //最小刻度值
        chartView.xAxis.axisMaximum = 6 //最大刻度值
        chartView.xAxis.forceLabelsEnabled = true
        chartView.xAxis.granularityEnabled = true
    }
    
    func setChartViewData(dataArray:[Double]) {
        
        var tempValue: Double = 0
        let newDataArray = upSideDownDataWith(dataArray: dataArray)
        
        //生成数据
        var dataEntries = [ChartDataEntry]()
        for i in 0..<newDataArray.count
        {
//            print("data: \(newDataArray[i])")
            let yAxisValue = newDataArray[i]
            
            if (yAxisValue > tempValue) {
                self.yAxisMaxValue = yAxisValue
                tempValue = yAxisValue
            }
            
//            print("temp: \(tempValue)")
//            print("max: \(yAxisMaxValue)")
            
            let entry = ChartDataEntry.init(x: Double(i), y: Double(yAxisValue))
            dataEntries.append(entry)
        }
        
        let leftAxis = chartView.leftAxis                         // 获取左边 Y 轴
        leftAxis.axisMaximum = self.yAxisMaxValue + 1             // 左邊 x 座標
        
        if (self.yAxisMaxValue == 0) {
            chartView.leftAxis.enabled = false
        }

        //这些数据作为1根折线里的所有数据
        let chartDataSet = LineChartDataSet(entries: dataEntries, label: "")
        chartDataSet.highlightEnabled = false  //不启用十字线
        chartDataSet.colors = [#colorLiteral(red: 0.8352941176, green: 0, blue: 0.1098039216, alpha: 1)] //设置线条颜色
        chartDataSet.lineWidth = 2 //修改线条大小
        chartDataSet.circleColors = [#colorLiteral(red: 0.8352941176, green: 0, blue: 0.1098039216, alpha: 1)]  //外圆颜色
        chartDataSet.circleHoleColor = .white  //内圆颜色
        chartDataSet.circleHoleRadius = 2 //内圆半径
        chartDataSet.circleRadius = 4  //外圆半径
        chartDataSet.drawValuesEnabled = true //不绘制拐点上的文字
        chartDataSet.valueFont = .systemFont(ofSize: 10)
        chartDataSet.valueColors = [#colorLiteral(red: 0.8352941176, green: 0, blue: 0.1098039216, alpha: 1)]
        
        // 把數值的 Double，強制改為 Int
        let formatter = NumberFormatter()  //自定义格式
        formatter.positiveSuffix = ""  //数字后缀单位
        chartDataSet.valueFormatter = DefaultValueFormatter(formatter: formatter)
        
        //开启填充色绘制
        chartDataSet.drawFilledEnabled = true
        //渐变颜色数组
        let startColor = #colorLiteral(red: 0.8352941176, green: 0, blue: 0.1098039216, alpha: 1)
        let gradientColors = [startColor.cgColor, UIColor.white.cgColor] as CFArray
        //每组颜色所在位置（范围0~1)
        let colorLocations:[CGFloat] = [1.0, 0.0]
        //生成渐变色
        let gradient = CGGradient.init(colorsSpace: CGColorSpaceCreateDeviceRGB(),
                                       colors: gradientColors, locations: colorLocations)
        //将渐变色作为填充对象s
        chartDataSet.fill = Fill.fillWithLinearGradient(gradient!, angle: 90.0)
        
        //目前折线图只包括1根折线
        let chartData = LineChartData(dataSets: [chartDataSet])
        
        //设置折现图数据
        chartView.data = chartData
        
    }
    
    private func upSideDownDataWith(dataArray: [Double]) -> [Double] {
        // API 給的日期是相反的，所以資料要先轉換
        var afterArray = [Double]()
        for data in dataArray {
            afterArray.insert(data, at: 0)
        }
        return afterArray
    }
}
