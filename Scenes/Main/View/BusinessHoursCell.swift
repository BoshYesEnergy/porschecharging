//
//  BusinessHoursCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/30.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class BusinessHoursCell: UITableViewCell {

    @IBOutlet weak var businessHoursTitle: UILabel!
    @IBOutlet weak var businessHours: UILabel!
    @IBOutlet weak var businessStatus: UILabel!
    
    var list: StationListClass.Data?
    
    var data: StationDetailClass.Data? {
        didSet{
            guard let datas     = data,
                  let lists     = list
                  else { return }
            if lists.isBussinessTime?.lowercased() == "n" {
                if data?.full_business_hours?.lowercased() == "n/a" ||
                   data?.full_business_hours?.lowercased() == "未提供" {
                    businessStatus.text  = ""
                } else {
                    businessStatus.text  = LocalizeUtils.localized(key: "NonOperating")
                }
                businessHours.text   = datas.full_business_hours ?? ""
            } else if lists.isBussinessTime?.lowercased() == "y" {
                if data?.full_business_hours?.lowercased() == "n/a" ||
                    data?.full_business_hours?.lowercased() == "未提供" {
                    businessStatus.text  = ""
                } else {
                    businessStatus.text  = LocalizeUtils.localized(key: "currentlyOpen")
                }
                businessHours.text   = datas.full_business_hours ?? ""
            } else {
                businessStatus.text = ""
                businessHours.text  = datas.full_business_hours ?? ""
            }
            print("businessHours--: \( datas.full_business_hours ?? "")")
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        businessHoursTitle.text = LocalizeUtils.localized(key: "StationBusinessHours")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    } 
}
