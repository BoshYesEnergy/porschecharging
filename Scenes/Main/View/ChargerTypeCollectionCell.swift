//
//  ChargerTypeCollectionCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/15.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class ChargerTypeCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var chargerIcon: UIImageView!
    @IBOutlet weak var chargerTypeTitle: UILabel!
    @IBOutlet weak var chargerNumber: UILabel!
    @IBOutlet weak var garyLine: UIView!
     
    var gunList: String? {
        didSet {
            guard let gun = gunList else { return }
            chargerNumber.text    = gun
            
            if gun == "CHAdeMO" {
                chargerIcon.image = UIImage(named: "thumbnail_station_dc_chademo")
            } else if gun == "SAE J1772" {
                chargerTypeTitle.text = "AC"
                chargerIcon.image = UIImage(named: "thumbnail_station_ac_j1772")
            } else if gun == "CCS1" {
                chargerTypeTitle.text = "DC"
                chargerIcon.image = UIImage(named: "thumbnail_station_dc_ccs1")
            } else if gun == "Type 2" {
                chargerIcon.image = UIImage(named: "thumbnail_station_ac_mennekes")
            } else if gun == "Type1 CCS" {
                chargerTypeTitle.text = "DC"
                chargerIcon.image = UIImage(named: "thumbnail_station_dc_ccs1")
            } else {
                chargerTypeTitle.text = "DC"
                chargerIcon.image = UIImage(named: "thumbnail_station_dc_ccs1")
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.isUserInteractionEnabled = true
    } 
}
