//
//  StationDemoCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/14.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class StationDemoCell: UITableViewCell { 
    @IBOutlet weak var favoriteBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    
    @IBOutlet weak var navigationBtn: UIButton!
    @IBOutlet weak var bookingBtn: UIButton!
    @IBOutlet weak var chargingBtn: UIButton!
    
    @IBOutlet weak var dcView: UIView!
    @IBOutlet weak var acView: UIView!
    
    @IBOutlet weak var stationLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var dcSpaceLabel: UILabel!
    @IBOutlet weak var acSpaceLabel: UILabel!
    
    weak var delegate: StationProtocol?
    var km = ""
    var stationCategory: String = ""
    var parkingSpace: Int = 0
    
    var list: StationListClass.Data?
    
    var data: StationDetailClass.Data? {
        didSet {
            guard let datas = data else { return }
            stationLabel.text = datas.stationName ?? ""
            addressLabel.text = datas.stationAddress ?? ""
            distanceLabel.text = km + "km"
            favoriteBtn.setImage(datas.favourite == true ? #imageLiteral(resourceName: "icon_bar_favorite_click") : #imageLiteral(resourceName: "icon_bar_favorite_default"), for: .normal)
            
            let dcTotal = datas.dcTotal ?? 0
            let dcConut = datas.dcEmpty ?? 0
            let acTotal = datas.acTotal ?? 0
            let acConut = datas.acEmpty ?? 0
            
            dcView.isHidden = dcTotal == 0 ? true : false
            acView.isHidden = acTotal == 0 ? true : false
            // 站點狀態：01: 可使用 , 02: 使用中 , 03: 未使用 , 04: 未營運
            if datas.status ==  "04" || datas.status ==  "05" || datas.connection == false || list?.isBussinessTime?.lowercased() == "n" {
                dcSpaceLabel.text = LocalizeUtils.localized(key: "StationInstalled") + ":\(dcTotal)"
                acSpaceLabel.text = LocalizeUtils.localized(key: "StationInstalled") + ":\(acTotal)"
            } else {
                dcSpaceLabel.text =  LocalizeUtils.localized(key: "StationAvailable") + ":\(dcConut)" + " / " + LocalizeUtils.localized(key: "StationInstalled") + ":\(dcTotal)"
                acSpaceLabel.text = LocalizeUtils.localized(key: "StationAvailable") + ":\(acConut)" + " / " + LocalizeUtils.localized(key: "StationInstalled") + ":\(acTotal)"
            }
            if KJLRequest.isLogin() == false {
                /// 未登入 隱藏按鈕
                chargingBtn.isHidden = true
                bookingBtn.isHidden  = true
                setCharginBtnUI(toString: LocalizeUtils.localized(key: "StationCharge"))
            } else {
                if stationCategory.lowercased() == "yes" {
                    navigationBtn.isHidden = false
                    bookingBtn.isHidden    = true
                    chargingBtn.isHidden   = true
                } else {  //porsche
                    // check lcok
                    if datas.hasDC == true {
                        //                    if datas.lock == true {
                        //                        setCharginBtnUI(toString: "Unlock &\nCharge")
                        //                        navigationBtn.isHidden   = false
                        //                        bookingBtn.isHidden      = false
                        //                        chargingBtn.isHidden     = false
                        //                        /// check parkingSpace
                        //                        /// 0 = 無車位 , 1 = left , 2 = right
                        //                        bookingBtn.backgroundColor = datas.parkingSpace ?? 0 == 0 ? ThemeManager.shared.colorPalettes.garyBackgroundColor : ThemeManager.shared.colorPalettes.redBackgroundColor
                        //                        bookingBtn.isEnabled = datas.parkingSpace ?? 0 == 0 ? false : true
                        //                    } else {
                        setCharginBtnUI(toString: LocalizeUtils.localized(key: "StationCharge"))
                        navigationBtn.isHidden   = false
                        bookingBtn.isHidden      = true
                        chargingBtn.isHidden     = false
                        //                    }
                        if list?.isBussinessTime?.lowercased() == "n" {
                            chargingBtn.backgroundColor = ThemeManager.shared.colorPalettes.garyBackgroundColor
                            chargingBtn.isEnabled = false
                        } else {
                            if data?.dcEmpty ?? 0 == 0 {
                                chargingBtn.backgroundColor = ThemeManager.shared.colorPalettes.garyBackgroundColor
                                chargingBtn.isEnabled = false
                                bookingBtn.isHidden      = true
                            } else {
                                chargingBtn.backgroundColor = ThemeManager.shared.colorPalettes.redBackgroundColor
                                chargingBtn.isEnabled = true
                            }
                        }
                    } else {
                        setCharginBtnUI(toString: LocalizeUtils.localized(key: "StationCharge"))
                        navigationBtn.isHidden   = false
                        bookingBtn.isHidden      = true
                        chargingBtn.isHidden     = true
                    }
                }
            }
             
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        setClickStatus()
        navigationBtn.setTitle(LocalizeUtils.localized(key: "StataionNavigate"), for: .normal)
        bookingBtn.setTitle(LocalizeUtils.localized(key: "StationReserve"), for: .normal)
        navigationBtn.backgroundColor = #colorLiteral(red: 0.8352941176, green: 0, blue: 0.1098039216, alpha: 1)
        bookingBtn.backgroundColor    = #colorLiteral(red: 0.8352941176, green: 0, blue: 0.1098039216, alpha: 1)
        chargingBtn.backgroundColor   = #colorLiteral(red: 0.8352941176, green: 0, blue: 0.1098039216, alpha: 1)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func setClickStatus() {
        navigationBtn.setImage(UIImage(named: "icon_navigation_click"), for: .highlighted)
        bookingBtn.setImage(UIImage(named: "icon_reserve_click"), for: .highlighted)
    }
    
    private func setCharginBtnUI(toString: String) {
        if let font = UIFont(name: "PorscheNext-Regular", size: 16) {
            let attrs: [ NSAttributedStringKey : Any] =
                [ NSAttributedStringKey.font : font,
                  NSAttributedStringKey.foregroundColor : UIColor.white ]
            
            let buttonTitleStr = NSMutableAttributedString(string: toString, attributes: attrs)
            chargingBtn.titleLabel?.numberOfLines = 0
            chargingBtn.setAttributedTitle(buttonTitleStr, for: .normal)
            chargingBtn.setImage(UIImage(named: "icon_charge_default")?.withRenderingMode(.alwaysTemplate), for: .normal)
            chargingBtn.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            chargingBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: -10, bottom: 0, right: 0)
            
        }
    }
    
    @IBAction func closeAction(_ sender: UIButton) {
        delegate?.stationDetailClose()
    }
    
    @IBAction func navAction(_ sender: UIButton) {
        delegate?.stationnNvigation()
    }
    
    @IBAction func bookingAction(_ sender: UIButton) {
        guard let deatil = data else { return }
        delegate?.stationBookingBtn(detail: deatil, parkingSpace: parkingSpace)
    }
    
    @IBAction func favoriteAction(_ sender: UIButton) {
        guard let datas = data else { return }
        let idAdd = datas.favourite == true ? KJLRequest.RequestBOOL.FALSE  : KJLRequest.RequestBOOL.TRUE 
        delegate?.stationFavorite(detail: data, idAdd: idAdd)
    }
    
    @IBAction func chargingAction(_ sender: UIButton) {
        //        if data?.lock == true {
        //            delegate?.stationUnlock()
        //        } else {
        delegate?.stationCharging()
        //        }
    }
    
    @IBAction func heightChage(_ sender: UIButton) {
        delegate?.stationChangeheight()
    }
}
