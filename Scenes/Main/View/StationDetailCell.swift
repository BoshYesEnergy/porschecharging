//
//  StationDetailCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/14.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

protocol StationDetailDelegate: class {
    func callOut()
}

class StationDetailCell: UITableViewCell {
    
    @IBOutlet weak var photoTitle: UILabel!
    @IBOutlet weak var floorTitle: UILabel!
    @IBOutlet weak var parkingTitle: UILabel!
    @IBOutlet weak var feeTitle: UILabel!
    @IBOutlet weak var phoneTitle: UILabel!
    
    @IBOutlet weak var photoView: UIImageView!
    @IBOutlet weak var floorLabel: UILabel!
    @IBOutlet weak var parkingNoLabel: UILabel!
    @IBOutlet weak var feeLabel: UILabel!
    @IBOutlet weak var phoneNubmer: UILabel!
    
    var stationCategory: String = ""
    
    var data: StationDetailClass? {
        didSet {
            guard let datas     = data?.datas else { return }
            feeLabel.text       = datas.chargingFee
            phoneNubmer.text    = datas.stationPhone
            parkingNoLabel.text = datas.carSpace
            floorLabel.text     = datas.spotFloor
            if datas.picArray?.first?.picPath ?? "" == "" { 
                photoView.image = UIImage(named: "thumbnail_image")
            } else {
                photoView.setImageWithUrlPath(datas.picArray?.first?.picPath ?? "")
            }
        }
    }
    
    weak var delegate: StationDetailDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        photoTitle.text = LocalizeUtils.localized(key: "StationPhotos")
        floorTitle.text = LocalizeUtils.localized(key: "StationFloor")
        parkingTitle.text = LocalizeUtils.localized(key: "StationParkingNo")
        feeTitle.text = LocalizeUtils.localized(key: "StationFee")
        phoneTitle.text = LocalizeUtils.localized(key: "StationPhone")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func callOut(_ sender: UIButton) {
        delegate?.callOut()
    } 
}
