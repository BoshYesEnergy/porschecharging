//
//  StartChargingVC.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/10.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit
import Lottie

class StartChargingVC: BaseViewController {
    
    @IBOutlet weak var stationTitle: UILabel!
    @IBOutlet weak var chargerIDLabel: UILabel!
    
    @IBOutlet weak var parkingTimeLabel: UILabel!
    @IBOutlet weak var chargingTimeLabel: UILabel!
    
    @IBOutlet weak var powerLabel: UILabel!
    @IBOutlet weak var batteryLabel: UILabel!
     
    @IBOutlet weak var powerAnimationView: AnimationView!
    @IBOutlet weak var batteryAnimationView: AnimationView!
     
    @IBOutlet weak var stopBtn: UIButton!
    
    @IBOutlet weak var elapsedTimeTitle: UILabel!
    @IBOutlet weak var statusTitle: UILabel!
    @IBOutlet weak var powerTitle: UILabel!
    @IBOutlet weak var batteryTitle: UILabel!
    @IBOutlet weak var clickTitle: UILabel!
     
    var tranNo = 0
    var chargingId = ""
    var timer: Timer?
    var second: TimeInterval = 1
    var retryNum = 0
    
    private var chargetimeStart: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setHeaderView(LocalizeUtils.localized(key: "ChargingProcess"), action: .DISMISS)
        hiddenNavigationBar(isHidden: true)
        setUI()
        setLanguage()
        UserDefaults.standard.set(tranNo, forKey: "chargeTranNo") 

        callAPIRequestForChargingStatus()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setTimer()
        setAnimationView()
        NotificationCenter.default.addObserver(self, selector: #selector(setAnimationView), name: .startChargingAnimate, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(setTimer), name: .chargingStatus, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        stopTimer()
        NotificationCenter.default.post(name: .checkChargingStatus, object: nil)
    }
    
    func setLanguage() {
        parkingTimeLabel.text   = LocalizeUtils.localized(key: "ChargingProcessParkingTime")
        chargingTimeLabel.text  = LocalizeUtils.localized(key: "ChargingProcessChargingTime")
        elapsedTimeTitle.text = LocalizeUtils.localized(key: "ChargingProcessElpsedTime")
        statusTitle.text = LocalizeUtils.localized(key: "ChargingProcessChargingstatus")
        powerTitle.text = LocalizeUtils.localized(key: "ChargingProcessPower")
        batteryTitle.text = LocalizeUtils.localized(key: "ChargingProcessBattery")
        clickTitle.text = LocalizeUtils.localized(key: "ChargingProcessContent3")
        stopBtn.setTitle(LocalizeUtils.localized(key: "ChargingProcessStop"), for: .normal)
        chargerIDLabel.text    = LocalizeUtils.localized(key: "ChargerProcessID") + " \(chargingId)"
    }
    
    fileprivate func setUI() {
        stationTitle.text       = ""
        powerLabel.text         = ""
        batteryLabel.text       = "" 
    }
    
    @objc fileprivate func setAnimationView() {
        powerAnimationView.loopMode = .loop
        powerAnimationView.play()
        
        batteryAnimationView.loopMode = .loop
        batteryAnimationView.play()
    }
    
    @IBAction func stop(_ sender: UIButton) {
        let vc =  ShowCheckMessageVC()
        vc.delegate               = self
        vc.toContent              = LocalizeUtils.localized(key: "ChargingProcessContent4")
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        present(vc, animated: true) { }
    }
}


//MARK: Timer
extension StartChargingVC {
    @objc private func setTimer() {
        stopTimer()
        print("Start timer")
        timer = Timer.scheduledTimer(timeInterval: second,
                                     target: self,
                                     selector: #selector(updateChargingData),
                                     userInfo: nil, repeats: true)
    }
    
    private func stopTimer() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
            print("stop ~ timer ")
        }
    }
    
    private func setupStartTime() {
        guard let chargetimeStart = chargetimeStart else { return }
        let formatter = DateFormatter()
        formatter.locale = Locale.init(identifier: "zh_CN")
        formatter.dateFormat = "yyyy-MM-dd HH:mm"
        if chargetimeStart == "0" {
            chargingTimeLabel.text = LocalizeUtils.localized(key: "ChargingProcessChargingTime")
//                ScheduledTimer.shared.setLabel(totalTimeLab, "計 算 中...", time: 0.15)
        } else {
            if chargetimeStart != "" {
                let date = formatter.date(from: chargetimeStart)!
                let startTime = Int(date.timeIntervalSince1970)
                let now = Int(Date().timeIntervalSince1970)
                let total = now - startTime
                print("now: \(now)\nstartTime: \(startTime)\ntotal: \(total)")
                
                let cycle = 60
                //        var second: Int = timeNum
                var second: Int = total
                var minutes: Int = 0
                var hour: Int = 0
                
                if (second >= cycle) {
                    minutes = second / cycle
                    second = second % cycle
                }
                
                if (minutes >= cycle) {
                    hour = minutes / cycle
                    minutes = minutes % cycle
                }
                chargingTimeLabel.text = LocalizeUtils.localized(key: "ChargingProcessChargingTime") + String(format: "%02i:%02i:%02i", hour, minutes, second)
            } 
        }
    }
    
    @objc func updateChargingData() {
        setupStartTime()
        
        guard retryNum > 0
            else { return }
        
        retryNum = retryNum - 1
        if retryNum == 0 {
            callAPIRequestForChargingStatus()
        }
    }
}


//MARK: Api
extension StartChargingVC {
    @objc func callAPIRequestForChargingStatus() {
        KJLRequest.requestForChargingStatus(tranNo:"\(tranNo)") {[weak self] (response) in
            guard let response = response as? ChargingStatusClass else { return }
            self?.retryNum = 5
            if response.resCode != "0000" {
                self?.popErrorMessage(reMsg: response.resMsg ?? "")
                return
            }
            
            self?.stationTitle.text      = response.datas?.stationName ?? "" 
            self?.powerLabel.text        = "\(response.datas?.kwh ?? "") kWh"
            self?.batteryLabel.text      = "\(response.datas?.soc ?? "")%"
            self?.chargetimeStart        = response.datas?.chargingStartTime ?? ""
            // status : 充電中Charging ; 完成 Finish ; 中斷 Interrupt ; 原頁等待 available ; 返回 timeout
            if response.datas?.status?.lowercased() == "finish"
                || response.datas?.status?.lowercased() == "interrupt" {
                self?.stopTimer()
                DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
                    UserDefaults.standard.set(0, forKey: "chargeTranNo")
                    
                    let vc = ChargingHistoryVC(nibName: "ChargingHistoryVC", bundle: nil)
                    vc.status = .Charging
                    vc.modalPresentationStyle = .overFullScreen
                    vc.tranNo = self?.tranNo ?? 0
                    self?.navigationController?.pushViewController(vc, animated: true)
                }
                
            }
        }
    }
    
    private func callStopCharging() {
        KJLRequest.requestForStopCharging(tranNo: "\(self.tranNo)") { (response) in
            guard let response = response as? NormalClass else { return }
            if response.resCode != "0000" {
                self.popErrorMessage(reMsg: response.resMsg ?? "")
            } else { 
                self.dismiss(animated: true) {
                    let vc = ChargingHistoryVC(nibName: "ChargingHistoryVC", bundle: nil)
                    vc.status = .Charging
                    vc.tranNo = self.tranNo
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
            }
        }
    }
}


//MARK: Message Delegate
extension StartChargingVC: ShowMessageAlertDelegate {
    func popErrorMessage(reMsg: String) {
        let vc = ShowMessageAlertVC()
        vc.stringToContent = reMsg
        vc.titleToBtn = LocalizeUtils.localized(key: Language.Confirm.rawValue)
        vc.delegate = self
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        present(vc, animated: true) { }
    }
    
    func messageTapAction() {
        self.dismiss(animated: true, completion: nil)
    }
}


//MARK: message
extension StartChargingVC: ShowCheckMessageDelegate {
    func tapConfirm(tagCount: Int) {
        stopTimer()
        callStopCharging() 
    }
    
    func tapCancel() {
        self.dismiss(animated: true, completion: nil)
    } 
}

