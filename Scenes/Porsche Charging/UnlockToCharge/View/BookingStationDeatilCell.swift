//
//  BookingStationDeatilCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/20.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

protocol BookingStationDelegate: class {
    func tagFavourite(detail: StationDetailClass.Data?, idAdd: KJLRequest.RequestBOOL)
}

class BookingStationDeatilCell: UITableViewCell {

    @IBOutlet weak var stationName: UILabel!
    @IBOutlet weak var favouriteBtn: UIButton!
    @IBOutlet weak var stationAddress: UILabel!
    @IBOutlet weak var stationKm: UILabel!
    @IBOutlet weak var parkContent: UILabel!
    @IBOutlet weak var parkView: UIView!
    @IBOutlet weak var parkLeftImage: UIImageView!
    @IBOutlet weak var parRightiImage: UIImageView!
    
    var data: StationDetailClass.Data? {
        didSet {
            guard let data = data else { return }
            favouriteBtn.setImage(data.favourite == true ? #imageLiteral(resourceName: "icon_bar_favorite_click") : #imageLiteral(resourceName: "icon_bar_favorite_default"), for: .normal)
            
            var direction = ""
            if data.parkingSpace == 1 {
                direction = "left"
            } else if data.parkingSpace == 2 {
                direction = "Right"
            }
            
            parkContent.text = "The " + "\(direction)" + " charger has been reserved for you"
            
            switch data.parkingSpace ?? 0 {
            case 0:
                parkLeftImage.image = UIImage(named: ToolPhoto.booking_car_disable.rawValue)
                parRightiImage.image = UIImage(named: ToolPhoto.booking_car_disable.rawValue)
            case 1:
                parkLeftImage.image = UIImage(named: ToolPhoto.booking_car.rawValue)
                parRightiImage.image = UIImage(named: ToolPhoto.booking_car_disable.rawValue)
            case 2:
                parkLeftImage.image = UIImage(named: ToolPhoto.booking_car_disable.rawValue)
                parRightiImage.image = UIImage(named: ToolPhoto.booking_car.rawValue)
            default: break
            }
            
        }
    }
    weak var delegate: BookingStationDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        parkView.layer.borderWidth = 1
        parkView.layer.borderColor = ThemeManager.shared.colorPalettes.blackBorder.cgColor
        
        parkLeftImage.image = UIImage(named: ToolPhoto.booking_car_disable.rawValue)
        parRightiImage.image = UIImage(named: ToolPhoto.booking_car_disable.rawValue)
        
        favouriteBtn.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    @IBAction func tapFavourite(_ sender: UIButton) {
        guard let data = data else { return }
        let idAdd = data.favourite == true ? KJLRequest.RequestBOOL.FALSE : KJLRequest.RequestBOOL.TRUE
        delegate?.tagFavourite(detail: data, idAdd: idAdd)
    }
}
