//
//  UnlockRaiseUpVC.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/19.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class UnlockRaiseUpVC: BaseViewController {

    @IBOutlet weak var titleName: UILabel!
    @IBOutlet weak var wayTitle: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var unlockImage: UIImageView!
    @IBOutlet weak var unlockContent: UILabel!
    @IBOutlet weak var raiseUpBtn: UIButton!
     
    fileprivate var animationArray = [UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setHeaderView("Unlock to Charge", action: .POP)
        hiddenNavigationBar(isHidden: true)
        unlockImage.loadGif(name: "smart_gate_lock@3")
    }
 
    @IBAction func raiseUp(_ sender: UIButton) {
        
    }
    
    func popCheckMessage() {
        let vc = ShowCheckMessageVC()
        vc.delegate = self
        vc.toTitle  = ""
        vc.toContent = "Connection timeout. Please try again."
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        present(vc, animated: true, completion: nil)
    }
}


//MARK: ShowCheckMessageVC delegate
extension UnlockRaiseUpVC: ShowCheckMessageDelegate {
    func tapConfirm(tagCount: Int) {
        self.dismiss(animated: true) {
            /// go to where
        }
    }
    
    func tapCancel() {
        self.dismiss(animated: true, completion: nil)
    }
}
