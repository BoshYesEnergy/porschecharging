//
//  BookingUnlockVC.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/22.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class BookingUnlockVC: UIViewController {

    @IBOutlet weak var headerTitle: UIButton!
    @IBOutlet weak var stationName: UILabel!
    @IBOutlet weak var unlockImage: UIImageView!
    @IBOutlet weak var stationContent: UILabel!
    @IBOutlet weak var retryBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        settingView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func settingView() {
        unlockImage.loadGif(name: "smart_gate_unlock@3")
        retryBtn.isHidden = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
//            self.retryBtn.isHidden = false
        }
    }
    
    func callParkingLockDown(qrNo: String) {
        KJLRequest.requestForParkingLockDown(qrNo: qrNo) {(response) in
            guard let parkingLockData = response as? ParkingLockDownModel
                else { return }
        }
    }
    
    @IBAction func retryLockDown(_ sender: UIButton) {
        
    }
}
