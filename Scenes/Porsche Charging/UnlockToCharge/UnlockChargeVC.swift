//
//  UnlockChargeVC.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/19.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class UnlockChargeVC: BaseViewController {
 
    @IBOutlet weak var titleName: UILabel!
    @IBOutlet weak var titleMessage: UILabel!
    @IBOutlet weak var imageStackView: UIStackView!
    @IBOutlet weak var imageOne: UIImageView!
    @IBOutlet weak var imageTwo: UIImageView!
    @IBOutlet weak var selectLeftBtn: UIButton!
    @IBOutlet weak var selectRightBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var unlockBtn: UIButton!
    
    var direction = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setHeaderView("Unlock to Charge", action: .DISMISS)
        hiddenNavigationBar(isHidden: true)
        settingView()
    }
    
    func settingView() {
        imageStackView.layer.borderWidth = 1
        imageStackView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        selectLeftBtn.setImage(UIImage(named: ToolImage.radio_click), for: .normal)
        selectRightBtn.setImage(UIImage(named: ToolImage.radio_click), for: .normal)
    }
     
    @IBAction func selectLeft(_ sender: UIButton) {
        changeCarImage(left: true)
    }
    
    @IBAction func selectRight(_ sender: UIButton) {
        changeCarImage(left: false)
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func unlock(_ sender: UIButton) {
        popCheckMessage(content: "Are you sure to drop down smart gate?", tagCount: 0)
    }
    
    func changeCarImage(left: Bool) {
        imageOne.image = left == true ? UIImage( named: ToolPhoto.booking_car.rawValue) : UIImage( named: ToolPhoto.booking_car_disable.rawValue)
        imageTwo.image = left == true ? UIImage( named: ToolPhoto.booking_car_disable.rawValue) : UIImage( named: ToolPhoto.booking_car.rawValue)
        
        let leftImage = left == true ? UIImage(named: ToolImage.radio_default) : UIImage(named: ToolImage.radio_click) 
        let rightImage = left == true ? UIImage(named: ToolImage.radio_click) : UIImage(named: ToolImage.radio_default)
        selectLeftBtn.setImage(leftImage, for: .normal)
        selectRightBtn.setImage(rightImage, for: .normal)
        
        direction = left == true ? 1 : 2
    }
}


//MARK: ShowCheckMessageVC & delegate
extension UnlockChargeVC: ShowCheckMessageDelegate {
    fileprivate func popCheckMessage(content: String, tagCount: Int, showBtn: MessageCheckBtn = .two) {
        let vc = ShowCheckMessageVC()
        vc.delegate  = self
        vc.toContent = content
        vc.toTitle   = ""
        vc.tagCount  = tagCount
        vc.custom    = showBtn
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        present(vc, animated: true, completion: nil)
    }
    
    func tapConfirm(tagCount: Int) {
        self.dismiss(animated: true) {
            let vc = BookingUnlockVC(nibName: "BookingUnlockVC", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tapCancel() {
        self.dismiss(animated: true, completion: nil)
    }
}
