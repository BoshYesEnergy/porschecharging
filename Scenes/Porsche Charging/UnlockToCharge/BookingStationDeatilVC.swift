//
//  ReservationDeatilVC.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/20.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit
import MapKit
import GoogleMaps
import SVProgressHUD
import CoreLocation

class BookingStationDeatilVC: UIViewController {
    /// Top Status view
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusColorView: UIView!
    @IBOutlet weak var statusName: UILabel!
    @IBOutlet weak var statusCancelBtn: UIButton!
    @IBOutlet weak var statusTieme: UILabel!
    /// Map
    @IBOutlet weak var googleMapView: GMSMapView!
    /// I alert
    @IBOutlet weak var iMessageViewBtn: UIButton!
    /// Table
    @IBOutlet weak var bookingTable: UITableView!
    /// Bottom
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var navugateBtn: UIButton!
    @IBOutlet weak var chargeBtn: UIButton!
    /// Map
    private var googleAPIManager =  GoogleAPIRequestManager()
    private var clusterManager: GMUClusterManager!
    /// station data
    var stationName: String = ""
    var stationDetailData: StationDetailClass.Data?
    var stationListData: StationListClass.Data?
    var markerImage: UIImage?
    var parkingSpace = 0
    var endTime: String? = "2020-12-22 09:10"
    
    var timer: Timer?
    var second: TimeInterval = 1
    var retryNum = 0
     
    override func viewDidLoad() {
        super.viewDidLoad()
        settingView()
        setTable()
        setGoogleMapView()
        setLocation()
        
        callStationList(stationName: stationName) {
            self.callStationDetail {
                self.addStationMarker()
                self.setTimer()
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        stopTimer()
    }
    
    func callStationList(stationName: String, completion: @escaping ()-> Void) {
        print("1")
        KJLRequest.requestForStationList(queryType: .Station, queryString: nil, type: "1") { (response) in
            guard let response = response as? StationListClass,
                  let list = response.datas
                  else { return }
            for i in list {
                if i.stationName == stationName {
                    self.stationListData = i
                    print("stationListData: \(self.stationListData)")
                    break
                }
            }
            completion()
        }
    }
    
    func callStationDetail(completion: @escaping ()-> Void) {
        print("2")
        guard let list = stationListData else { return }
        KJLRequest.requestForStationDetail(stationId: list.stationId ?? "") { (response) in
            guard let response = response as? StationDetailClass else { return }
            self.stationDetailData = response.datas
            self.bookingTable.reloadData()
            completion()
        }
    }
}

//MARK: Setting view
extension BookingStationDeatilVC {
    private func settingView() {
        statusView.layer.borderWidth = 1
        statusView.layer.borderColor = ThemeManager.shared.colorPalettes.garyLine.cgColor
        
        statusCancelBtn.layer.borderColor = ThemeManager.shared.colorPalettes.blackBorder.cgColor
        statusCancelBtn.layer.borderWidth = 1
    }
    
    private func setTable() {
        bookingTable.register(UINib(nibName: "BookingStationDeatilCell", bundle: nil), forCellReuseIdentifier: "BookingStationDeatilCell")
    }
}


//MARK: Setting google map
extension BookingStationDeatilVC {
    fileprivate func setLocation() {
//        LocationManagers.updateLocation { [unowned self] (status, location) in
            let lat    = stationDetailData?.latitude ?? "0.0"
            let long   = stationDetailData?.longitude ?? "0.0"
            let station = CLLocationCoordinate2D(latitude: Double(lat)!, longitude: Double(long)!)
            let region = MKCoordinateRegionMakeWithDistance(station, 5000, 5000)
            let camera = GMSCameraPosition.camera(withLatitude: region.center.latitude, longitude: region.center.longitude, zoom: 15)
            self.googleMapView.camera = camera
//        }
    }
    
    fileprivate func setGoogleMapView() {
        googleMapView.delegate = self
        googleMapView.isMyLocationEnabled = true
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "google_map_style", withExtension: "json") {
                googleMapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        
        let iconGenerator = GMUDefaultClusterIconGenerator(buckets: [100,200], backgroundColors: [#colorLiteral(red: 0.07058823529, green: 0.5254901961, blue: 0.7529411765, alpha: 1),#colorLiteral(red: 0.07058823529, green: 0.5254901961, blue: 0.7529411765, alpha: 1)])
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: googleMapView, clusterIconGenerator: iconGenerator)
        renderer.delegate = self
        clusterManager = GMUClusterManager(map: googleMapView, algorithm: algorithm, renderer: renderer)
        clusterManager.setDelegate(self, mapDelegate: self)
    }
    
    fileprivate func addStationMarker() {
        print("3")
        googleMapView.clear()
        clusterManager.clearItems()
        
        guard let list = stationListData,
              let detail = stationDetailData
              else { return }
        // 站點狀態：01: 可使用 , 02: 使用中 , 03: 未連線 , 04: 未營運
        var imgName: String = AnnotationType.NONE.rawValue
        let stationCarGory = stationListData?.stationCategory?.lowercased() ?? ""
        
        if list.hasDC == true {
            if stationCarGory == "porsche"{
                imgName = MapManger.dcPorscheICON(list)
            } else {
                imgName = MapManger.dcICON(list)
            }
        } else {
            if stationCarGory == "porsche"{
                imgName = MapManger.acPorscheICON(list)
            } else{
                imgName = MapManger.acICON(list)
            }
        }
        
        let stationName = stationDetailData?.stationName ?? ""
        let lat    = stationDetailData?.latitude ?? "0.0"
        let long   = stationDetailData?.longitude ?? "0.0"
        let marker = POIItem(position: CLLocationCoordinate2DMake( Double(lat) ?? 0.0, Double(long) ?? 0.0 ), name: stationName)
        
        // check empty total
        var emptyToString = ""
        if detail.connection == false {
            var total = ""
            if detail.hasDC == true {
                total = "\(detail.dcTotal ?? 0)"
            } else {
                total = "\(detail.acTotal ?? 0)"
            }
            emptyToString = total
        } else {
            if detail.hasDC == true {
                let empty = "\(detail.dcEmpty ?? 0)"
                let total = "\(detail.dcTotal ?? 0)"
                emptyToString = empty + "/" + total
            } else {
                if stationCarGory == "porsche"{
                    let total = "\(detail.acTotal ?? 0)"
                    emptyToString = total
                } else {
                    let empty = "\(detail.acEmpty ?? 0)"
                    let total = "\(detail.acTotal ?? 0)"
                    emptyToString = empty + "/" + total
                }
            }
        }
        
        if list.isBussinessTime?.lowercased() == "n" {
            marker.iconImage = UIImage(named: imgName)
        } else {
            let toImage = ImageManager.share.textToImage(drawText: emptyToString, inImage: UIImage(named: imgName) ?? UIImage())
            marker.iconImage = toImage
        }
        
        clusterManager.add(marker)
        clusterManager.cluster()
    }
}


//MARK: Timer
extension BookingStationDeatilVC {
    private func setTimer() {
        print("4")
        timer = Timer.scheduledTimer(timeInterval: second,
                                     target: self,
                                     selector: #selector(updateChargingData),
                                     userInfo: nil, repeats: true)
    }
    
    private func stopTimer() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
            print("stop ~ timer ")
        }
    }
    
    private func setupStartTime() {
        guard let end = endTime else { return }
        let formatter = DateFormatter()
        formatter.locale = Locale.init(identifier: "zh_CN")
        formatter.dateFormat = "yyyy-MM-dd HH:mm"
        
        let now = Date()
        let endTime1970 = Int(formatter.date(from: end)!.timeIntervalSince1970)
        let now1970 = Int(now.timeIntervalSince1970)
        let total = endTime1970 - now1970 
        
        let cycle = 60
        var second: Int = total
        var minutes: Int = 0
        var hour: Int = 0
        print(second)
        
        if second <= 0 {
            stopTimer()
            statusTieme.text = "00:00"
            self.dismiss(animated: false) {
                self.dismiss(animated: true, completion: nil)
            }
        } else {
            if (second >= cycle) {
                minutes = second / cycle
                second = second % cycle
            }
            
            if (minutes >= cycle) {
                hour = minutes / cycle
                minutes = minutes % cycle
            }
            statusTieme.text = String(format: "%02i:%02i", minutes, second)
        } 
    }
    
    @objc func updateChargingData() {
        setupStartTime()
        
        guard retryNum > 0
            else { return }
        
        retryNum = retryNum - 1
        if retryNum == 0 {
            callReserveCheck(parkingSpace: parkingSpace)
        }
    }
}


//MARK: Api
extension BookingStationDeatilVC {
    fileprivate func callReserveCheck(parkingSpace: Int) {
//        retryNum = 5
//        KJLRequest.requestForReserveCheck(reserveNo: parkingSpace) { (response) in
//            guard let response = response as? NormalClass else { return }
//            if response.resCode == "0000" {
//                // ????
//            } else {
//                self.popCheckMessage(content: response.resMsg ?? "", tagCount: 1, showBtn: .one)
//            }
//        }
    }
}


//MARK: Action
extension BookingStationDeatilVC {
    @IBAction func statusCancel(_ sender: UIButton) {
        popCheckMessage(content: "Are you sure to cancel reservation", tagCount: 0)
    }
    
    @IBAction func showiMessageView(_ sender: UIButton) {
        let vc = IServiceViewController()
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func goNavigate(_ sender: UIButton) {
        let lat  = Double(stationDetailData?.latitude ?? "0")
        let long = Double(stationDetailData?.longitude ?? "0")
        KJLCommon.openNavMap(lat: lat, long: long, subControl: self)
    }
    
    @IBAction func charge(_ sender: UIButton) {
//        let vc = BookingUnlockVC(nibName: "BookingUnlockVC", bundle: nil)
//        vc.navigationController?.isNavigationBarHidden = true
//        self.navigationController?.pushViewController(vc, animated: true)
    }
}


//MARK: Table view delegate & data source
extension BookingStationDeatilVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "BookingStationDeatilCell", for: indexPath) as? BookingStationDeatilCell {
            cell.delegate = self
            cell.data = stationDetailData
            return cell
        } else { return UITableViewCell() }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}


//MARK: Google map delegate
extension BookingStationDeatilVC: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
    }
    
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        
    }
}


//MARK: GMUClusterRendererDelegate
extension BookingStationDeatilVC: GMUClusterRendererDelegate {
    func renderer(_ renderer: GMUClusterRenderer, willRenderMarker marker: GMSMarker) {
        marker.groundAnchor = CGPoint(x: 0.5, y: 1)
        if let markerData = (marker.userData as? POIItem) {
            let icon = markerData.iconImage
            marker.icon = icon
        }
    }
}


//MARK: GMUClusterManagerDelegate
extension BookingStationDeatilVC :GMUClusterManagerDelegate {
    func clusterManager(_ clusterManager: GMUClusterManager, didTap clusterItem: GMUClusterItem) -> Bool {
        return false
    }
    
    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool {
        let newCamera = GMSCameraPosition.camera(withTarget: cluster.position,
                                                 zoom: googleMapView.camera.zoom + 1)
        let update = GMSCameraUpdate.setCamera(newCamera)
        googleMapView.animate(with: update)
        return true
    }
}


//MARK: Booking Station cell Delegate
extension BookingStationDeatilVC: BookingStationDelegate {
    func tagFavourite(detail: StationDetailClass.Data?, idAdd: KJLRequest.RequestBOOL) {
//        guard let data = detail else { return }
//        KJLRequest.requestForAddFavourite(stationId: data.stationId ?? "", add: idAdd) {[weak self] (response) in
//            guard (response as? NormalClass) != nil else { return }
//            
//        }
    }
}


//MARK: ShowCheckMessageVC & delegate
extension BookingStationDeatilVC: ShowCheckMessageDelegate {
    fileprivate func popCheckMessage(content: String, tagCount: Int, showBtn: MessageCheckBtn = .two) {
        let vc = ShowCheckMessageVC()
        vc.delegate  = self
        vc.toContent = content
        vc.toTitle   = ""
        vc.tagCount  = tagCount
        vc.custom    = showBtn
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        present(vc, animated: true, completion: nil)
    }
    
    func tapConfirm(tagCount: Int) {
        KJLRequest.requestForReserveCancel { (response) in
            guard (response as? NormalClass) != nil else { return }
            self.dismiss(animated: false) {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func tapCancel() {
        self.dismiss(animated: true, completion: nil)
    }
}

