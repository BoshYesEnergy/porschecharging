//
//  ReadyChargingVC.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/10.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class ReadyChargingVC: BaseViewController {

    @IBOutlet weak var stationTitle: UILabel!
    @IBOutlet weak var stationNo: UILabel!
    @IBOutlet weak var step1: UILabel!
    @IBOutlet weak var content1: UILabel!
    @IBOutlet weak var step2: UILabel!
    @IBOutlet weak var content2: UILabel!
     
    @IBOutlet weak var cancelBtn: UIButton!
      
    var tranNo = 0
    var chargingId = ""
    var timer: Timer?
    var second: TimeInterval = 5
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setHeaderView(LocalizeUtils.localized(key: "ChargingProcess"), action: .DISMISS)
        hiddenNavigationBar(isHidden: true)
        tranNo = UserDefaults.standard.integer(forKey: "chargeTranNo") 
        callAPIRequestForChargingStatus()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setTimer()
        setLanguage()
        NotificationCenter.default.addObserver(self, selector: #selector(setTimer), name: .chargingStatus, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        stopTimer()
        NotificationCenter.default.post(name: .checkChargingStatus, object: nil)
    } 
    
    func setLanguage() {
        step1.text = LocalizeUtils.localized(key: "ChargingProcessStep1")
        content1.text = LocalizeUtils.localized(key: "ChargingProcessContent1")
        step2.text = LocalizeUtils.localized(key: "ChargingProcessStep2")
        content2.text = LocalizeUtils.localized(key: "ChargingProcessContent2")
        cancelBtn.setTitle(LocalizeUtils.localized(key: "ChargingProcessCancel"), for: .normal)
        stationNo.text = LocalizeUtils.localized(key: "ChargerProcessID") + " \(chargingId)"
    }
 
    @IBAction func cancel(_ sender: UIButton) { 
        let vc =  ShowCheckMessageVC()
        vc.delegate = self
        vc.toTitle = LocalizeUtils.localized(key: "ChargingProcessMessage")
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        present(vc, animated: true) { }
    }
}


//MARK: Api
extension ReadyChargingVC {
    @objc func callAPIRequestForChargingStatus() {
        KJLRequest.requestForChargingStatus(tranNo:"\(tranNo)") {[weak self] (response) in
            guard let response = response as? ChargingStatusClass
                else { return }
            self?.stationTitle.text = response.datas?.stationName ?? ""
             
            if response.resCode != "0000" {
                self?.popErrorMessage(reMsg: response.resMsg ?? "")
                return
            }
            // status : 充電中Charging ; 完成 Finish ; 中斷 Interrupt ; 原頁等待 available ; 返回 timeout
            if response.datas?.status?.lowercased() == "charging" {
                
                self?.stopTimer()
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                    let vc = StartChargingVC(nibName: "StartChargingVC", bundle: nil)
                    vc.tranNo = self?.tranNo ?? 0
                    vc.chargingId = self?.chargingId ?? ""
                    self?.navigationController?.pushViewController(vc, animated: true)
                }
            } else if response.datas?.status?.lowercased() == "timeout"{
                self?.stopTimer()
                self?.callStopCharging()
            } else {
                print("ReadyChargingVC api status: \(response.datas?.status?.lowercased())")
            }
        }
    }
    
    private func callStopCharging() {
        KJLRequest.requestForStopCharging(tranNo: "\(self.tranNo)") { (response) in
            guard let response = response as? NormalClass else { return }
            if response.resCode != "0000" {
                self.popErrorMessage(reMsg: response.resMsg ?? "")
            } else {
                UserDefaults.standard.set("", forKey: "chargeTranNo")
                self.dismiss(animated: false) {
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
}


//MARK: Timer
extension ReadyChargingVC {
    @objc private func setTimer() {
        stopTimer()
        print("Start timer")
        timer = Timer.scheduledTimer(timeInterval: second,
                                     target: self,
                                     selector: #selector(callAPIRequestForChargingStatus),
                                     userInfo: nil, repeats: true)
    }
    
    private func stopTimer() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
            print("stop ~ timer ")
        }
    }
}

//MARK: Message Delegate
extension ReadyChargingVC: ShowMessageAlertDelegate {
    func popErrorMessage(reMsg: String) {
        let vc = ShowMessageAlertVC()
        vc.stringToContent = reMsg
        vc.titleToBtn = LocalizeUtils.localized(key: Language.Confirm.rawValue)
        vc.delegate = self
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        present(vc, animated: true) { }
    }
    
    func messageTapAction() {
        self.dismiss(animated: true, completion: nil)
    }
}


//MARK: Check message
extension ReadyChargingVC: ShowCheckMessageDelegate {
    func tapConfirm(tagCount: Int) {
        callStopCharging()
    }
    
    func tapCancel() {
        self.dismiss(animated: true, completion: nil)
    }
}
