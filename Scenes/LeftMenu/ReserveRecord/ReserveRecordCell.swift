//
//  ReserveRecordCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class ReserveRecordCell: UITableViewCell {

    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var stationLabel: UILabel!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setupCellWith(timeString: String, stationString: String , resultString: String , price: Int) {
        self.timeLabel.text = timeString
        self.stationLabel.text = stationString
        self.resultLabel.text = resultString
        self.priceLabel.text = "$\(price)"
        if (resultString == "預約中") {
            self.resultLabel.textColor = #colorLiteral(red: 0, green: 0.768627451, blue: 0.7960784314, alpha: 1)
        } else {
            self.resultLabel.textColor = #colorLiteral(red: 0.4588235294, green: 0.4470588235, blue: 0.5215686275, alpha: 1)
        }
    }
}
