//
//  ReserveRecordViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class ReserveRecordViewController: KJLNavView {

    //MARK: - IBOutlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var startDateBtn: UIButton!
    @IBOutlet weak var endDateBtn: UIButton!
    @IBOutlet weak var allBtn: UIButton!
    @IBOutlet weak var easyCardBtn: UIButton!
    @IBOutlet weak var creditCardBtn: UIButton!
    @IBOutlet weak var thirdpartyBtn: UIButton!
    @IBOutlet weak var selectLine: UIView!
    @IBOutlet weak var selectLineLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var cornerView: UIView!
    
    //MARKL: - Property
    private var startDateStr = ""
    private var endDateStr = ""
    private weak var tempBtn: UIButton? = nil
    private var reserveListArray: [ReserveListClass.Data] = []
    private var payment: KJLRequest.ReservePayment = .none
    
    // MARK: - 生命週期
    override func viewDidLoad() {
        super.viewDidLoad()

        self.cornerView.backgroundColor = UIColor.clear
        self.cornerView.layer.cornerRadius = self.cornerView.bounds.height / 2
        self.cornerView.layer.masksToBounds = true
        self.cornerView.layer.borderColor = UIColor.black.cgColor
        self.cornerView.layer.borderWidth = 1
        
        setupNaviTitle()
        setupDateLabel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.callAPIRequestForReserveList(startDateStr: self.startDateStr, endDateStr: self.endDateStr, payment: self.payment)
    }
    
    //MARK: - IBAction
    @IBAction func selectTypeAction(_ sender: UIButton) {
        allBtn.setTitleColor(#colorLiteral(red: 0.6077751517, green: 0.6078823209, blue: 0.6077683568, alpha: 1), for: .normal)
        easyCardBtn.setTitleColor(#colorLiteral(red: 0.6077751517, green: 0.6078823209, blue: 0.6077683568, alpha: 1), for: .normal)
        creditCardBtn.setTitleColor(#colorLiteral(red: 0.6077751517, green: 0.6078823209, blue: 0.6077683568, alpha: 1), for: .normal)
        thirdpartyBtn.setTitleColor(#colorLiteral(red: 0.6077751517, green: 0.6078823209, blue: 0.6077683568, alpha: 1), for: .normal)
        
        sender.setTitleColor(#colorLiteral(red: 0.1497560143, green: 0.7302395701, blue: 0.7572383881, alpha: 1), for: .normal)
        selectLineLeadingConstraint.constant = (sender.superview?.center.x ?? 0) - (selectLine.frame.width/2)
        
        selectData(sender: sender)
    }
    
    @IBAction func selectDateAction(_ sender: UIButton) {
        let picker = Bundle.main.loadNibNamed("KJLDatePickerView", owner: self, options: nil)?[0] as? KJLDatePickerView
        picker?.show()
        if (sender == startDateBtn) {
            let format = DateFormatter()
            format.dateFormat = "yyyy-MM-dd"
            var tempDate = format.date(from: endDateStr)
            picker?.kPickerView.maximumDate = tempDate
            tempDate = format.date(from: startDateStr)
            if let tempDate = tempDate {
                picker?.kPickerView.setDate(tempDate, animated: true)
            }
        } else {
            let format = DateFormatter()
            format.dateFormat = "yyyy-MM-dd"
            var tempDate = format.date(from: startDateStr)
            picker?.kPickerView.minimumDate = tempDate
            picker?.kPickerView.maximumDate = Date(timeIntervalSinceNow: 0)
            tempDate = format.date(from: endDateStr)
            if let tempDate = tempDate {
                picker?.kPickerView.setDate(tempDate, animated: true)
            }
        }
        picker?.pickerCompletion = { (date) in
            if date != nil {
                let format = DateFormatter()
                format.dateFormat = "yyyy-MM-dd"
                if (sender == self.startDateBtn) {
                    self.startDateStr = format.string(from: date!)
                    //                    sender.setTitle(self.startDateStr, for: .normal)
                    var t = self.startDateStr
                    t = t.replacingOccurrences(of: "-", with: "-")
                    self.startDateLabel.text = t
                } else {
                    self.endDateStr = format.string(from: date!)
                    //                    sender.setTitle(self.endDateStr, for: .normal)
                    var t = self.endDateStr
                    t = t.replacingOccurrences(of: "-", with: "-")
                    self.endDateLabel.text = t
                }
                
                //                if sender == self.lastDateBtn {
                //                    if let temp = self.tempBtn {
                //                        self.selectMenuAction(temp)
                //                    }
                //                }
                if let temp = self.tempBtn {
                    self.selectTypeAction(temp)
                }
            }
        }
    }
    
    // MARK: - 自訂 Func
    private func setupNaviTitle() {
        createNavViewForRightBtn(rightName: "", rightImage: "", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: UIColor.black)
        titleLab?.text = "預約紀錄"
    }
    
    private func setupDateLabel() {
        self.startDateLabel.text = ""
        self.endDateLabel.text = ""
        
        /* startDate: 現在日期的前 7 天 , endDate: 現在日期 */
        let startDate = Date(timeIntervalSinceNow: -7 * 24 * 60 * 60)
        let endDate = Date(timeIntervalSinceNow: 0)
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd"
        startDateStr = format.string(from: startDate)
        endDateStr = format.string(from: endDate)
        
        var t1 = self.startDateStr
        t1 = t1.replacingOccurrences(of: "-", with: "-")
        var t2 = self.endDateStr
        t2 = t2.replacingOccurrences(of: "-", with: "-")
        startDateLabel.text = t1
        endDateLabel.text = t2
        
        self.selectTypeAction(self.allBtn)
    }
    
    func selectData(sender: UIButton) {
        tempBtn = sender
    
        if (sender == allBtn) {
            self.payment = KJLRequest.ReservePayment.All
        } else if (sender == easyCardBtn) {
            self.payment = KJLRequest.ReservePayment.UUCard
        } else if (sender == creditCardBtn) {
            self.payment = KJLRequest.ReservePayment.CreditCard
        } else if (sender == thirdpartyBtn) {
            self.payment = KJLRequest.ReservePayment.ThirdParty
        }
        self.callAPIRequestForReserveList(startDateStr: self.startDateStr, endDateStr: self.endDateStr, payment: self.payment)
    }
    
    /// API-804 預約列表
    private func callAPIRequestForReserveList (startDateStr: String , endDateStr: String , payment: KJLRequest.ReservePayment) {
        /* endDate 要加 1 天 , startDate 不用加 */
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let endDate = dateFormatter.date(from: endDateStr)
        let newEndDate = endDate! + (24 * 60 * 60)
        let newEndDateStr = dateFormatter.string(from: newEndDate)
        
        self.reserveListArray.removeAll()
        
        KJLRequest.requestForReserveList(dateEnd: newEndDateStr, dateStart: startDateStr, payment: payment.rawValue) { (response) in
            guard let response = response as? ReserveListClass else {
                return
            }
            self.reserveListArray = response.datas ?? []
            self.tableView.reloadData()
        }
    }
}

// MARK: - TableView DataSource
extension ReserveRecordViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reserveListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ReserveRecordCell", for: indexPath) as? ReserveRecordCell {
            let data = reserveListArray[indexPath.row]
            cell.setupCellWith(timeString: data.reserveTime ?? "", stationString: data.stationName ?? "", resultString: data.reserveStatus ?? "", price: data.reserveAmount ?? 0)
            return cell
        }
        return UITableViewCell(style: .default, reuseIdentifier: "")
    }
}

// MARK: - TableView Delegate
extension ReserveRecordViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "ReserveDetailViewController") as? ReserveDetailViewController {
            if let stationName = reserveListArray[indexPath.row].stationName { 
            }
            vc.setupDataWith(reserveNo: reserveListArray[indexPath.row].reserveNo ?? 0 , isFromReserveRecordVC: true)
            navigationController?.show(vc, sender: self)
        }
    }
}
