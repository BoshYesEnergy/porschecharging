//
//  InboxViewModel.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/7/14.
//  Copyright © 2020 s5346. All rights reserved.
//

import Foundation 

enum ViewType:Int {
    case news
    case activity
    case maintain
    case push
}

enum newsType:String {
    //充電提醒
    case chargingReminder = "1"
    
    //營運資訊
    case perationalInformation = "2"
    
    //活動情報
    case activityInformation = "3"
    
    //緊急通知
    case emegencyNotice = "4"
    
    //電子報
    case Newsletter = "5"
    
    //媒體報導
    case mediaReports = "6"
    
}

enum newsTypeInclude:Int {
    //回傳分類除了帶入分類外的其他資訊
    case notInclude = 0
    
    //只回傳帶入的分類
    case include = 1
}

class PushInfo:NSObject {
    var content:String?
    var createDate:String?
    var detailId:String?
    var readDate:String?
    var subject:String?
    var taskId:Int?
}
 
class NewsInfo: NSObject, Codable {
    var newsNo: String?
}

