//
//  InboxViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit
import SVProgressHUD

// MARK: - InboxViewController
class InboxViewController: KJLNavView {
    @IBOutlet weak var selectLineLeft: NSLayoutConstraint!
    @IBOutlet weak var selectLine: UIView!
    @IBOutlet weak var activityButton: UIButton!
    @IBOutlet weak var pushButton: UIButton!
    @IBOutlet weak var newsButton: UIButton!
    @IBOutlet weak var maintainButton: UIButton!
    @IBOutlet weak var mainCollectionView: UICollectionView!
    // 紅點
    @IBOutlet weak var newsIcon: UIView!
    @IBOutlet weak var activeIcon: UIView!
    @IBOutlet weak var maintainIcon: UIView!
    @IBOutlet weak var pushIcon: UIView!
    
    var isLoadPushGetList = true
    var newstableView:      UITableView!
    var activitytableView:  UITableView!
    var maintainTableView:  UITableView!
    var pushtableView:      UITableView!
    var funPushDeviceInboxItems:NSMutableArray = NSMutableArray()
    let pushrefreshControl      = UIRefreshControl()
    let activityrefreshControl  = UIRefreshControl()
    let newsrefreshControl      = UIRefreshControl()
    let maintainrefreshControl  = UIRefreshControl()
    let pushrefreshView         = RefreshViews()
    let activityrefreshView     = RefreshViews()
    let newrefreshView          = RefreshViews()
    let maintainrefreshView     = RefreshViews()
    var activityLists: [NewsListClass.Data] = []
    var newsLists:     [NewsListClass.Data] = []
    var maintainLists: [NewsListClass.Data] = []
    var viewType:ViewType = .news
    var pageIndex = -1
    var pageAmount = 10
    var isRequesting = false
    var isEndOfData = false
    var pushCount = 0
    private var indexItem = 0
    
    // MARK: - 生命週期
    override func viewDidLoad() {
        super.viewDidLoad()
        createNavViewForRightBtn(rightName: "", rightImage: "", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: UIColor.black)
        titleLab?.text = "收件匣"
        self.setupTableViews()
        self.setupCollectionView()
        #warning("暫時關閉 setRefreshView , 等 api")
//        setRefreshView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) 
        self.getAllActivityInfo()
        self.getPushGetList()
        self.callNewTypeCount()
        
    }
    
    private func setupTableViews() {
        pushtableView = UITableView()
        setTable(table: pushtableView, cellID: "InboxCell")
        pushtableView.reloadData()
        activitytableView = UITableView()
        setTable(table: activitytableView, cellID: "InboxNewsCell")
        newstableView = UITableView()
        setTable(table: newstableView, cellID: "InboxNewsCell")
        maintainTableView = UITableView()
        setTable(table: maintainTableView, cellID: "InboxNewsCell")
    }
    
    private func setTable(table: UITableView, cellID: String) {
        table.makeContentInset()
        table.separatorStyle = .none
        table.delegate = self
        table.dataSource = self
        table.register(UINib(nibName: cellID, bundle: nil), forCellReuseIdentifier: cellID)
        table.estimatedRowHeight = 1000
        table.rowHeight = UITableViewAutomaticDimension
    }
    
    private func setRefreshView() {
        pushtableView.addSubview(pushrefreshControl)
//        activitytableView.addSubview(activityrefreshControl)
//        newstableView.addSubview(newsrefreshControl)
//        maintainTableView.addSubview(maintainrefreshControl)
        setRefreshViewLayout(pushrefreshView, pushrefreshControl)
//        setRefreshViewLayout(activityrefreshView, activityrefreshControl)
//        setRefreshViewLayout(newrefreshView, newsrefreshControl)
//        setRefreshViewLayout(maintainrefreshView, maintainrefreshControl)
//        newrefreshView.readBtn.addTarget(self, action: #selector(read), for: .touchUpInside)
//        newrefreshView.cancelBtn.addTarget(self, action: #selector(deleteList), for: .touchUpInside)
//        activityrefreshView.readBtn.addTarget(self, action: #selector(read), for: .touchUpInside)
//        activityrefreshView.cancelBtn.addTarget(self, action: #selector(deleteList), for: .touchUpInside)
//        maintainrefreshView.readBtn.addTarget(self, action: #selector(read), for: .touchUpInside)
//        maintainrefreshView.cancelBtn.addTarget(self, action: #selector(deleteList), for: .touchUpInside)
    }
    
    private func setRefreshViewLayout(_ refreshView: UIView, _ control: UIControl) {
        control.addSubview(refreshView)
        control.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        control.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        refreshView.translatesAutoresizingMaskIntoConstraints = false
        refreshView.topAnchor.constraint(equalTo: control.topAnchor).isActive = true
        refreshView.leadingAnchor.constraint(equalTo: control.leadingAnchor).isActive = true
        refreshView.bottomAnchor.constraint(equalTo: control.bottomAnchor).isActive = true
        refreshView.trailingAnchor.constraint(equalTo: control.trailingAnchor).isActive = true
    }
    
    func setupCollectionView() {
        // 初始化
        let collectionViewFlowLayout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionViewFlowLayout.scrollDirection = .horizontal
        collectionViewFlowLayout.itemSize = CGSize.init(width:UIScreen.main.bounds.width, height: self.mainCollectionView.frame.height)
        collectionViewFlowLayout.minimumLineSpacing = 0
        collectionViewFlowLayout.minimumInteritemSpacing = 0
        self.mainCollectionView.collectionViewLayout = collectionViewFlowLayout
        self.mainCollectionView.delegate = self
        self.mainCollectionView.dataSource = self
        self.mainCollectionView.isScrollEnabled = false
        self.mainCollectionView.isPagingEnabled = true
        self.mainCollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "CollectionViewCell")
        
        self.mainCollectionView.layoutIfNeeded()
        if #available(iOS 13.0, *) {
//            self.mainCollectionView.contentInsetAdjustmentBehavior = false
        }
    }
    
    /// delete: delete == true , read: delete == false
    @objc func callNewsBatchReadDelete(delete: Bool, list: [Any]) {
        var newNoInbox: [NewsInfo] = []  
        guard list.count != 0 else { return }
        for data in list as! [NewsListClass.Data] {
            let newsNo = NewsInfo()
            newsNo.newsNo = String(data.newsNo ?? 0)
            newNoInbox.append(newsNo)
        }
        let jsonNewNo = newNoInbox.map{ $0.newsNo!.parameterValue }
        print("--start--\n\(jsonNewNo)--end--\n")
        KJLRequest.requestForNewsBatchReadDelete(delNews: delete, jsonNewNo: jsonNewNo) { (response) in
            guard response != nil else { return }
            self.getAllActivityInfo()
            self.callNewTypeCount()
        }
    }
    
    private func callNewTypeCount() {
        KJLRequest.requestForGetNewsBadge {[weak self] (response) in
            guard let response = response as? NewTypeCountModel,
                let data = response.datas
                else { return }
            let news    = data.newestCount ?? 0
            let active  = data.activeCount ?? 0
            let warning = data.warningCount ?? 0
            let push    = data.pushCount ?? 0
            let total = news + active + warning + push
            UIApplication.shared.applicationIconBadgeNumber = total
            UserDefaults.standard.set(total, forKey: "UnReadCount")
            self?.newsIcon.isHidden     = news > 0 ? false : true
            self?.activeIcon.isHidden   = active > 0 ? false : true
            self?.maintainIcon.isHidden = warning > 0 ? false : true
            self?.pushIcon.isHidden     = push > 0 ? false : true
            if let pushDataCount = data.pushCount {
                self?.pushCount = pushDataCount
            }
        }
    }
    
    func getPushGetList() {
        guard let deviceId = UserDefaults.standard.string(forKey: "deviceId"),let memberNo = UserDefaults.standard.string(forKey: "memberNo"),
            deviceId != ""
            else { return }
        
        self.pageIndex  = self.pageIndex + 1
        KJLRequest.requestForPushGetList(deviceId: deviceId, memberNo: memberNo, pageNum: self.pageIndex ,rowsPerPage:self.pageAmount) { [unowned self](response) in
            guard let response = response as? NewsListClass,
                let datas = response.datas else { return }
            for index in 0..<datas.count {
                let data = datas[index]
                let pushInfo = PushInfo()
                pushInfo.content = data.content
                pushInfo.createDate = data.createDate
                pushInfo.detailId = data.detailId
                pushInfo.readDate = data.readDate
                pushInfo.subject = data.subject
                pushInfo.taskId = data.taskId
                self.funPushDeviceInboxItems.add(pushInfo)
            }
            self.isEndOfData = (datas.count < self.pageAmount)
            self.isRequesting = false;
            self.pushtableView.reloadData()
        }
    }
    
    func getAllActivityInfo() {
        KJLRequest.requestForNewsList(newsType: "0", isInclude: newsTypeInclude.notInclude.rawValue) { (response) in
            guard let response = response as? NewsListClass,
                let datas = response.datas else { return }
            self.maintainLists.removeAll()
            self.activityLists.removeAll()
            self.newsLists.removeAll()
            for activityInfo in datas {
                if activityInfo.newsType == "緊急通知" {
                     self.maintainLists.append(activityInfo)
                } else if activityInfo.newsType == "活動情報" {
                    self.activityLists.append(activityInfo)
                } else{
                    self.newsLists.append(activityInfo)
                }
            }
            self.activitytableView.reloadData()
            self.maintainTableView.reloadData()
            self.newstableView.reloadData()
        }
    }
    
    
    //MARK: - IBAction
    @IBAction func selectMenuAction(_ sender: UIButton) {
        activityButton.setTitleColor(#colorLiteral(red: 0.6077751517, green: 0.6078823209, blue: 0.6077683568, alpha: 1), for: .normal)
        pushButton.setTitleColor(#colorLiteral(red: 0.6077751517, green: 0.6078823209, blue: 0.6077683568, alpha: 1), for: .normal)
        newsButton.setTitleColor(#colorLiteral(red: 0.6077751517, green: 0.6078823209, blue: 0.6077683568, alpha: 1), for: .normal)
        maintainButton.setTitleColor(#colorLiteral(red: 0.6077751517, green: 0.6078823209, blue: 0.6077683568, alpha: 1), for: .normal)
        sender.setTitleColor(#colorLiteral(red: 0.1497560143, green: 0.7302395701, blue: 0.7572383881, alpha: 1), for: .normal)
        callNewTypeCount()
        if sender == pushButton {
            self.mainCollectionView.scrollToItem(at: IndexPath.init(item: 3, section: 0), at: .centeredHorizontally, animated: true)
            selectLineLeft.constant = (UIScreen.main.bounds.width / 4) * 3;
        }else if(sender == activityButton) {
            self.mainCollectionView.scrollToItem(at: IndexPath.init(item: 1, section: 0), at: .centeredHorizontally, animated: true)
            selectLineLeft.constant = UIScreen.main.bounds.width / 4;
        }else if(sender == maintainButton) {
            self.mainCollectionView.scrollToItem(at: IndexPath.init(item: 2, section: 0), at: .centeredHorizontally, animated: true)
            selectLineLeft.constant = (UIScreen.main.bounds.width / 4) * 2;
        }else {
            self.mainCollectionView.scrollToItem(at: IndexPath.init(item: 0, section: 0), at: .centeredHorizontally, animated: true)
            selectLineLeft.constant = 0;
        }
    }
}


// MARK: - TableView DataSource
extension InboxViewController:  UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == pushtableView {
            return funPushDeviceInboxItems.count
        }else if(tableView == newstableView) {
            return self.newsLists.count
        }else if(tableView == maintainTableView) {
            return self.maintainLists.count
        }else{
            return self.activityLists.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == pushtableView {
            // 自動讀取新資料
            if ((indexPath.row > self.funPushDeviceInboxItems.count - 3) && !self.isEndOfData && !self.isRequesting) {
                self.isRequesting = true;
                self.getPushGetList()
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "InboxCell", for: indexPath) as! InboxCell
            cell.selectionStyle = .none
            let data:PushInfo = self.funPushDeviceInboxItems[indexPath.row] as! PushInfo
            cell.data = data
            return cell
        }else if tableView == maintainTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InboxNewsCell", for: indexPath) as! InboxNewsCell
            let data:NewsListClass.Data = self.maintainLists[indexPath.row]
                cell.data = data
                cell.selectionStyle = .none
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "InboxNewsCell", for: indexPath) as! InboxNewsCell
                let data:NewsListClass.Data = (tableView == newstableView) ? self.newsLists[indexPath.row]:self.activityLists[indexPath.row]
            cell.data = data
            cell.selectionStyle = .none
            return cell
        }
    }
}


// MARK: - TableView Delegate
extension InboxViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == pushtableView {
            return UITableViewAutomaticDimension
        }
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.newstableView || tableView == self.activitytableView || tableView == maintainTableView {
            let inboxNewsCell = tableView.cellForRow(at: indexPath) as! InboxNewsCell
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            if let vc = mainStoryboard.instantiateViewController(withIdentifier: "LatestNewsDetailViewController") as? LatestNewsDetailViewController {
                vc.newsNo = "\(inboxNewsCell.data.newsNo ?? 0)"
                navigationController?.show(vc, sender: self)
            }
        } else {
            let data:PushInfo = self.funPushDeviceInboxItems[indexPath.row] as! PushInfo
            guard let memberNo = UserDefaults.standard.string(forKey: "memberNo") else { return }
            guard data.readDate == nil || data.readDate == "" else { return }
            KJLRequest.requestForPushSetRead(detailId: data.detailId!, memberNo: memberNo, taskId: "\(data.taskId!)") { (response) in
                guard let _ = response as? NormalClass else { return }
                data.readDate = "1"
                self.pushCount -= 1
                if self.pushCount == 0 {
                    self.callNewTypeCount()
                }
                tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
            }
            /// badge count, total == "UnReadCount", read == -1
            let badgeCount = UserDefaults.standard.integer(forKey: "UnReadCount")
            if badgeCount >= 1 {
                UIApplication.shared.applicationIconBadgeNumber = badgeCount - 1
                UserDefaults.standard.set(badgeCount - 1, forKey: "UnReadCount")
            } else {
                UserDefaults.standard.set(0, forKey: "UnReadCount")
            }
        }
    }
}

//MARK: UICollectionViewDataSource
extension InboxViewController: UICollectionViewDataSource {
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let collectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath)
        // 清掉 cell subview
        collectionViewCell.contentView.subviews.forEach { subview in
            subview.removeFromSuperview()
        }
        collectionViewCell.backgroundColor = UIColor.clear
        collectionViewCell.contentView.backgroundColor = UIColor.clear
        return collectionViewCell
    }
}

//MARK: UICollectionViewDelegate
extension InboxViewController: UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath){
        let homeView:UIView
        switch indexPath.row {
        case  0:
            homeView = self.newstableView
            viewType = .news
        case  1:
            homeView = self.activitytableView
            viewType = .activity
        case  2:
            homeView = self.maintainTableView
            viewType = .maintain
        default:
            homeView = self.pushtableView
            viewType = .push
        }
        print("viewType: -- \(viewType)")
        // 共用行為
        cell.contentView.addSubview(homeView)
        homeView.addFillConstraint()
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize.init(width: UIScreen.main.bounds.width, height: collectionView.frame.height)
    }
}


extension InboxViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y >= CGFloat(-35) {
//            pushrefreshControl.endRefreshing()
            newsrefreshControl.endRefreshing()
            maintainrefreshControl.endRefreshing()
            activityrefreshControl.endRefreshing()
        }
    }
}

 
extension InboxViewController {
    @objc func read() {
        switch viewType {
        case .news:
            callNewsBatchReadDelete(delete: false, list: newsLists)
        case .activity:
            callNewsBatchReadDelete(delete: false, list: activityLists)
        case .maintain:
            callNewsBatchReadDelete(delete: false, list: maintainLists)
        case .push:
            #warning("缺api")
            break
        }
    }
    
    @objc func deleteList() {
        switch viewType {
        case .news:
            callNewsBatchReadDelete(delete: true, list: newsLists)
        case .activity:
            callNewsBatchReadDelete(delete: true, list: activityLists)
        case .maintain:
            callNewsBatchReadDelete(delete: true, list: maintainLists)
        case .push:
            #warning("缺api")
            break
        }
    }
}

