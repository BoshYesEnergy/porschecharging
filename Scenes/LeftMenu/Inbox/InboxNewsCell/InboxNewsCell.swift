//
//  InboxNewsCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class InboxNewsCell: UITableViewCell {
    @IBOutlet weak var titleLab: UILabel!
    @IBOutlet weak var contentLab: UILabel!
    @IBOutlet weak var dateLab: UILabel!
    @IBOutlet weak var typeLab: UILabel!
    @IBOutlet weak var redView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.typeLab.layer.cornerRadius = 5
    }
    
    var data:NewsListClass.Data! {
        didSet{
            self.setupViews()
        }
    }
    enum newsType:String {
        //充電提醒
        case chargingReminder = "1"
        
        //營運資訊
        case perationalInformation = "2"
        
        //活動情報
        case activityInformation = "3"
        
        //緊急通知
        case emegencyNotice = "4"
        
        //電子報
        case Newsletter = "5"
        
        //媒體報導
        case mediaReports = "6"
        
    }
    func setupViews() {
        guard let data = data else {
            titleLab.text = ""
            contentLab.text = ""
            dateLab.text = ""
            return
        }
        
        if data.newsType == "緊急通知" || data.newsType == "活動情報" {
            typeLab.isHidden = true
        }else{
            typeLab.isHidden = false
        }
        
        if data.read == true {
            redView.isHidden = true
        } else {
            redView.isHidden = false
        }
        
        titleLab.text = data.title
        contentLab.text = data.outline
        dateLab.text = data.datetime
        typeLab.text = data.newsType
//        switch data.newsType {
//        case "1":
//            self.typeLab.text = "充電提醒"
//        case "2":
//            self.typeLab.text = "營運資訊"
//        case "3":
//            self.typeLab.text = "活動情報"
//        case "4":
//            self.typeLab.text = "緊急通知"
//        case "5":
//            self.typeLab.text = "電子報"
//        case "6":
//            self.typeLab.text = "媒體報導"
//        default:
//            break
//        }
        
    }
}
