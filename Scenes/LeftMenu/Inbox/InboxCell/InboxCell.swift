//
//  InboxCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class InboxCell: UITableViewCell {
    
    var data:PushInfo! {
        didSet{
            self.setupViews()
        }
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var readDetailLabel: UILabel!
    @IBOutlet weak var readDetailImageView: UIImageView!
    @IBOutlet weak var readDetailView: UIView!
    
    
    func setupViews() {
        let unreadColor = UIColor.init(red: 0.85, green: 0.98, blue: 0.97, alpha: 1)
        self.contentView.backgroundColor = (data.readDate == nil || data.readDate == "") ? unreadColor : UIColor.white
        self.readDetailView.isHidden = true
        self.titleLabel.text = data.subject ?? ""
        self.contentLabel.text = data.content ?? ""
        self.dateLabel.text = data.createDate ?? ""
    }
}
