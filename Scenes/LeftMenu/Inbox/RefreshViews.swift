//
//  RefreshViews.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/7/15.
//  Copyright © 2020 s5346. All rights reserved.
//

import Foundation
import UIKit

class RefreshViews: UIView {
    
    let view: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    let readBtn: UIButton = {
       let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
       return btn
    }()
    let cancelBtn: UIButton = {
       let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
       return btn
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        settingView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func settingView() {
        self.addSubview(view)
        view.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        view.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
         
        view.addSubview(readBtn)
        view.addSubview(cancelBtn)
         
        readBtn.layer.cornerRadius = 22.5
        readBtn.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        readBtn.layer.borderWidth = 2
        cancelBtn.layer.cornerRadius = 22.5
        cancelBtn.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        cancelBtn.layer.borderWidth = 2
        
        readBtn.setTitle("全部\n已讀", for: .normal)
        readBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        readBtn.titleLabel?.lineBreakMode = .byWordWrapping
        readBtn.titleLabel?.font = .boldSystemFont(ofSize: 14)
        cancelBtn.setTitle("全部\n刪除", for: .normal)
        cancelBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        cancelBtn.titleLabel?.lineBreakMode = .byWordWrapping
        cancelBtn.titleLabel?.font = .boldSystemFont(ofSize: 14)
        
        readBtn.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 80).isActive = true
        readBtn.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        readBtn.widthAnchor.constraint(equalToConstant: 45).isActive = true
        readBtn.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        cancelBtn.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -80).isActive = true
        cancelBtn.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        cancelBtn.widthAnchor.constraint(equalToConstant: 45).isActive = true
        cancelBtn.heightAnchor.constraint(equalToConstant: 45).isActive = true
    } 
}
