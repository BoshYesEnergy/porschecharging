//
//  LatestNewsViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class LatestNewsViewController: KJLNavView {

    @IBOutlet weak var timeLab: UILabel!
    @IBOutlet weak var ktable: UITableView!
    @IBOutlet weak var cornerView: UIView!
    
    var lists: [NewsListClass.Data] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        createNavViewForRightBtn(rightName: "", rightImage: "", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: UIColor.black)
        titleLab?.text = "最新消息"
        
        self.cornerView.backgroundColor = UIColor.clear
        self.cornerView.layer.cornerRadius = self.cornerView.bounds.height / 2
        self.cornerView.layer.masksToBounds = true
        self.cornerView.layer.borderColor = UIColor.black.cgColor
        self.cornerView.layer.borderWidth = 1
//        rightBtn?.addTarget(self, action: #selector(gotoFilter), for: .touchUpInside)
        
        
        let nowDate = Date(timeIntervalSinceNow: 0)
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd HH:mm"
        let str = format.string(from: nowDate)
        timeLab.text = str
        
        KJLRequest.requestForNewsList(newsType: "1", isInclude: 0) { (response) in
            guard let response = response as? NewsListClass else {
                return
            }
            
            self.lists = response.datas ?? []
            
            self.ktable.reloadData()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) 
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func gotoFilter() {
        guard KJLRequest.isLogin() == true else {
            KJLCommon.showLogin(subcontrol: self)
            return
        }
        
        if let vc = storyboard?.instantiateViewController(withIdentifier: "NewsFilterViewController") as? NewsFilterViewController {
            navigationController?.show(vc, sender: self)
        }
    }
}

extension LatestNewsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LatestNewsCell", for: indexPath)
        
        if let cell = cell as? LatestNewsCell {
            cell.delegate = self
            cell.changeData(data: lists[indexPath.row])
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
}

extension LatestNewsViewController: LatestNewsCellDelegate {
    func LatestNewsCellForDetail(data: NewsListClass.Data?) {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "LatestNewsDetailViewController") as? LatestNewsDetailViewController {
            vc.newsNo = "\(data?.newsNo ?? 0)"
            navigationController?.show(vc, sender: self)
        }
    }
    
    
}
