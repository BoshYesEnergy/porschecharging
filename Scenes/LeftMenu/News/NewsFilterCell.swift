//
//  NewsFilterCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class NewsFilterCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLab: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLab.layer.borderWidth = 1
        titleLab.layer.borderColor = #colorLiteral(red: 0.2392156863, green: 0.7607843137, blue: 0.7843137255, alpha: 1)
    }
}
