//
//  LatestNewsCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

protocol LatestNewsCellDelegate {
    func LatestNewsCellForDetail(data: NewsListClass.Data?)
}

class LatestNewsCell: UITableViewCell {

    @IBOutlet weak var titleLab: UILabel!
    @IBOutlet weak var contentLab: UILabel!
    @IBOutlet weak var detailBtn: UIButton!
    @IBOutlet weak var dateLab: UILabel!
    var delegate: LatestNewsCellDelegate? = nil
    
    var myData: NewsListClass.Data?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
//    override func layoutIfNeeded() {
//        detailBtn.setLikeButton()
//    }
    
    func changeData(data: NewsListClass.Data?) {
        guard let data = data else {
            titleLab.text = ""
            contentLab.text = ""
            dateLab.text = ""
            return
        }
        myData = data
        
        titleLab.text = data.title
        contentLab.text = data.outline
        dateLab.text = data.datetime
    }

    @IBAction func detailAction(_ sender: UIButton) {
        guard let delegate = delegate, let data = myData else {
            return
        }
        
        delegate.LatestNewsCellForDetail(data: data)
    }
}
