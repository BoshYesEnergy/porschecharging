//
//  LatestNewsDetailViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit
import Kingfisher
import WebKit

class LatestNewsDetailViewController: KJLNavView {

    @IBOutlet weak var webView: WKWebView!
    
    var newsNo: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad() 
        createNavViewForRightBtn(rightName: "", rightImage: "", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: UIColor.black)
        titleLab?.text = "最新消息"
//        rightBtn?.addTarget(self, action: #selector(gotoFilter), for: .touchUpInside)
        
        // API-[602] 查詢消息明細
        KJLRequest.requestForNewsDetail(newsNo: newsNo) { (response) in
            guard let response = response as? NewsDetailClass else {
                return
            }
            
            let txt = response.datas?.text ?? ""
            print(txt)
            self.webView.loadHTMLString(txt, baseURL: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func gotoFilter() {
        guard KJLRequest.isLogin() == true else {
            KJLCommon.showLogin(subcontrol: self)
            return
        }
        
        if let vc = storyboard?.instantiateViewController(withIdentifier: "NewsFilterViewController") as? NewsFilterViewController {
            navigationController?.show(vc, sender: self)
        }
    }
}



