//
//  NewsFilterViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class NewsFilterViewController: KJLNavView { 
    @IBOutlet weak var categoryCollection: UICollectionView!
    @IBOutlet weak var kcollection: UICollectionView!
    @IBOutlet weak var kcollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var categoryBtn: UIButton!
    @IBOutlet weak var areaBtn: UIButton!
    var locData: [SystemVerbClass.LocList] = []
    var secData: [SystemVerbClass.SecData] = []
    let categories = ["充電提醒", "營運資訊", "活動情報", "緊急通知", "電子報", "媒體報導"]
    var isCallApi = true
    var maxAreaCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        createNavViewForRightBtn(rightName: "", rightImage: "", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: UIColor.black)
        titleLab?.text = "最新消息"
        for temp in backBtn?.allTargets ?? [] {
            backBtn?.removeTarget(self, action: temp as? Selector, for: .touchUpInside)
        }
        backBtn?.addTarget(self, action: #selector(backSave), for: .touchUpInside)
        
        kcollection.layer.borderColor = #colorLiteral(red: 0, green: 0.768627451, blue: 0.7960784314, alpha: 1)
        kcollection.layer.borderWidth = 1
        kcollection.allowsMultipleSelection = true
        categoryCollection.allowsMultipleSelection = true
        
        let data = KJLCommon.getSaveSystemVerbClass()
        locData = data?.datas?.locData?.locList ?? []
        secData = locData.first?.secDatas ?? []
        
        var total = 0
        for loc in locData {
            let secs = loc.secDatas ?? []
            total = total + secs.count
        }
        maxAreaCount = total
         
        kcollection.reloadData()
        categoryCollection.reloadData()
        
        kcollection.setNeedsLayout()
        kcollection.layoutIfNeeded()
        kcollectionHeight.constant = kcollection.contentSize.height
        
        categoryBtn.layer.borderWidth = 1
        categoryBtn.layer.borderColor = #colorLiteral(red: 0, green: 0.768627451, blue: 0.7960784314, alpha: 1)
        areaBtn.layer.borderWidth = 1
        areaBtn.layer.borderColor = #colorLiteral(red: 0, green: 0.768627451, blue: 0.7960784314, alpha: 1)
        
        KJLRequest.requestForShowSubscribe { (response) in
            guard let response = response as? ShowSubscribeClass else { return }
            if response.datas?.useDefault == true {
                self.categoryBtn.isSelected = false
                self.areaBtn.isSelected = false
                self.categoryAction(self.categoryBtn)
                self.areaAction(self.areaBtn)
            } else {
                self.isCallApi = false
                let type = response.datas?.preferences?.type ?? ""
                let types = type.components(separatedBy: ",")
                for name in types {
                    for (i, name2) in self.categories.enumerated() {
                        if name == name2 {
                            self.categoryCollection.selectItem(at: IndexPath(item: i, section: 0), animated: false, scrollPosition: .top)
                            self.categoryCollection.delegate?.collectionView?(self.categoryCollection, didSelectItemAt: IndexPath(item: i, section: 0))
                        }
                    }
                }
                
                let locs = response.datas?.preferences?.locList ?? []
                for loc in locs {
                    for (i, loc2) in self.locData.enumerated() {
                        if loc.secName == loc2.secName {
                            let secs = loc.secDatas ?? []
                            let secs2 = loc2.secDatas ?? []
                            for (j, sec2) in secs2.enumerated() {
                                let sec2Name = sec2.locName ?? ""
                                for sec in secs {
                                    let secName = sec.locName
                                    if secName == sec2Name {
                                        self.kcollection.selectItem(at: IndexPath(item: j, section: i), animated: false, scrollPosition: .top)
                                        self.kcollection.delegate?.collectionView?(self.kcollection, didSelectItemAt: IndexPath(item: j, section: i))
                                    }
                                }
                            }
                        }
                    }
                }
                self.isCallApi = true
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) 
    }
    
    func saveApi() {
        isSelectAll()
    }
    
    @objc func backSave() {
        guard isCallApi == true else { return }
        
        var type = ""
        for indexpath in categoryCollection.indexPathsForSelectedItems ?? [] {
            let name = categories[indexpath.row]
            
            if type.count == 0 {
                type = name
            }else {
                type = type + "," + name
            }
        }
        
        var loc = ""
        for indexpath in kcollection.indexPathsForSelectedItems ?? [] {
            let a = locData[indexpath.section]
            let b = a.secDatas ?? []
            let name = b[indexpath.row].locName ?? ""
            
            if loc.count == 0 {
                loc = name
            } else {
                loc = loc + "," + name
            }
        }
        
        
        KJLRequest.requestForSetSubscribe(useDefault: .FALSE, type: type, loc: loc) { (response) in
            guard response != nil else { return }
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func isSelectAll() {
        if (categoryCollection.indexPathsForSelectedItems?.count ?? 0) == categories.count {
            categoryBtn.isSelected = true
            categoryBtn.setTitleColor(UIColor.white, for: .normal)
            categoryBtn.backgroundColor = #colorLiteral(red: 0, green: 0.768627451, blue: 0.7960784314, alpha: 1)
        } else {
            categoryBtn.isSelected = false
            categoryBtn.setTitleColor(#colorLiteral(red: 0, green: 0.768627451, blue: 0.7960784314, alpha: 1), for: .normal)
            categoryBtn.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        
        if (kcollection.indexPathsForSelectedItems?.count ?? 0) == maxAreaCount {
            areaBtn.isSelected = true
            areaBtn.setTitleColor(UIColor.white, for: .normal)
            areaBtn.backgroundColor = #colorLiteral(red: 0, green: 0.768627451, blue: 0.7960784314, alpha: 1)
        } else {
            areaBtn.isSelected = false
            areaBtn.setTitleColor(#colorLiteral(red: 0, green: 0.768627451, blue: 0.7960784314, alpha: 1), for: .normal)
            areaBtn.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
    }
    
    // 類別 全部
    @IBAction func categoryAction(_ sender: UIButton) {
        isCallApi = false
        if sender.isSelected == true {
            sender.isSelected = false
            sender.setTitleColor(#colorLiteral(red: 0, green: 0.768627451, blue: 0.7960784314, alpha: 1), for: .normal)
            sender.backgroundColor = UIColor.white
            for (i, _) in categories.enumerated() {
                categoryCollection.deselectItem(at: IndexPath(item: i, section: 0), animated: false)
                categoryCollection.delegate?.collectionView?(categoryCollection, didDeselectItemAt: IndexPath(item: i, section: 0))
            }
        } else {
            sender.isSelected = true
            sender.setTitleColor(UIColor.white, for: .normal)
            sender.backgroundColor = #colorLiteral(red: 0, green: 0.768627451, blue: 0.7960784314, alpha: 1)
            for (i, _) in categories.enumerated() {
                categoryCollection.selectItem(at: IndexPath(item: i, section: 0), animated: false, scrollPosition: .top)
                categoryCollection.delegate?.collectionView?(categoryCollection, didSelectItemAt: IndexPath(item: i, section: 0))
            }
        }
        isCallApi = true
        saveApi()
    }
    
    // 地區 全部
    @IBAction func areaAction(_ sender: UIButton) {
        isCallApi = false
        if sender.isSelected == true {
            sender.isSelected = false
            sender.setTitleColor(#colorLiteral(red: 0, green: 0.768627451, blue: 0.7960784314, alpha: 1), for: .normal)
            sender.backgroundColor = UIColor.white
            for (section, _) in locData.enumerated() {
                let sec = locData[section].secDatas ?? []
                for (index, _) in sec.enumerated() {
                    kcollection.deselectItem(at: IndexPath(item: index, section: section), animated: false)
                    kcollection.delegate?.collectionView?(kcollection, didDeselectItemAt: IndexPath(item: index, section: section))
                }
            }
        } else {
            sender.isSelected = true
            sender.setTitleColor(UIColor.white, for: .normal)
            sender.backgroundColor = #colorLiteral(red: 0, green: 0.768627451, blue: 0.7960784314, alpha: 1)
            for (section, _) in locData.enumerated() {
                let sec = locData[section].secDatas ?? []
                for (index, _) in sec.enumerated() {
                    kcollection.selectItem(at: IndexPath(item: index, section: section), animated: false, scrollPosition: .top)
                    kcollection.delegate?.collectionView?(kcollection, didSelectItemAt: IndexPath(item: index, section: section))
                }
            }
        }
        isCallApi = true
        saveApi()
    }
}

extension NewsFilterViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == categoryCollection {
            return categories.count
        }
        return locData[section].secDatas?.count ?? 0
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == categoryCollection {
            return 1
        }
        return locData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewsFilterCell", for: indexPath)
        if collectionView == categoryCollection {
            if let cell = cell as? NewsFilterCell {
                cell.titleLab.text = categories[indexPath.row]
            }
        } else {
            if let cell = cell as? NewsFilterCell {
                let a = locData[indexPath.section]
                let b = a.secDatas?[indexPath.row]
                cell.titleLab.text = b?.locName
            }
        }
        return cell
    }
    
    // header
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "NewsFilterHeader", for: indexPath)
        if let header = header as? NewsFilterHeader {
            header.titleLab.text = locData[indexPath.section].secName
        }
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? NewsFilterCell
            else { return }
        cell.titleLab.backgroundColor = #colorLiteral(red: 0, green: 0.768627451, blue: 0.7960784314, alpha: 1)
        cell.titleLab.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        saveApi()
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? NewsFilterCell
            else { return }
        cell.titleLab.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.9921568627, blue: 0.9960784314, alpha: 1)
        cell.titleLab.textColor = #colorLiteral(red: 0, green: 0.768627451, blue: 0.7960784314, alpha: 1)
        saveApi()
    }
}

extension NewsFilterViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let viewWidth = kcollection.frame.width - 11 * 2
        let avgWidth = (viewWidth / 3) - 2;
        return CGSize(width: avgWidth, height: 32)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 11
    }
}
