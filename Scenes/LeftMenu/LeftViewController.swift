//
//  LeftViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit
import Kingfisher

protocol LeftViewControllerDelegate: class {
    func closeLeftView()
}

class LeftViewController: UIViewController {
    enum LeftViewControllerListName: String {
        case CharingRecord      = "充電紀錄"
        case ReseveRecord       = "預約記錄"
//        case RoutePlaning       = "路線規劃"
        case PaySetting         = "付款設定"
        case News               = "最新消息"
//        case NewChargeStation   = "新建立充電站"
        case FunPushMailBox     = "收件匣"
        case PushSetting        = "推播設定"
        case Contact            = "關於我們"
        case AppGuide           = "使用教學"
        case Login              = "登    入"
        case Subscription       = "Subscription"
        case StationList        = "Station List"
        case PaymentSetting     = "Payment Setting"
        case ChargingHistory    = "Charging History"
    }
    
    // MARK: - IBOutlet
    @IBOutlet weak var rightSpace: NSLayoutConstraint!
    @IBOutlet weak var ktable: UITableView!
    // MARK: - Property
    weak var delegate: LeftViewControllerDelegate? = nil
    var lists: [LeftViewControllerListName] = []
    var mainController: MainViewController? = nil
    private var indexPath: IndexPath?
    private var messageTotal: Int = 0
    private var paymentIssues = false
    
    // MARK: - 生命週期
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        paymentIssues = KJLRequest.shareInstance().profile?.datas?.paymentIssues ?? false
        if (KJLRequest.isLogin() == true) {
            lists = [.Subscription, .StationList, .PaymentSetting, .ChargingHistory]
//            callNewTypeCount() 
        } else {
            lists = [.Subscription, .StationList, .PaymentSetting, .ChargingHistory] 
        }
        ktable.reloadData() 
    }
     
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = (touches as NSSet).anyObject() as? UITouch
        if touch?.view == view {
            delegate?.closeLeftView()
        }
    }
    
    // MARK: - IBAction
    // 編輯個人資料
    @IBAction func editProfileAction(_ sender: UIButton) { 
        guard KJLRequest.isLogin() == true else {
            if let vc = storyboard?.instantiateViewController(withIdentifier: "SelectLoginViewController") as? SelectLoginViewController {
                vc.hidesBottomBarWhenPushed = true
                mainController?.navigationController?.show(vc, sender: self)
            }
            return
        }
        
        
    }
    
    @IBAction func back(_ sender: UIButton) {
        mainController?.closeLeftView()
    }
}

//MARK: - TableView DataSource
extension LeftViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        self.indexPath = indexPath
        if let cell = tableView.dequeueReusableCell(withIdentifier: "LeftViewCell", for: indexPath) as? LeftViewCell {
            let title = lists[indexPath.row].rawValue
            cell.setupCellWith(titleString: title)
            if lists[indexPath.row] == .FunPushMailBox {
                if messageTotal > 0 {
                    if messageTotal >= 99 {
                        messageTotal = 99
                    }
                    cell.settingView(message: true, count: messageTotal)
                } else {
                    cell.settingView(message: false)
                }
            } else {
                cell.settingView(message: false)
            }
            return cell
        }
        return UITableViewCell(style: .default, reuseIdentifier: "")
    }
}

//MARK: - TableView Delegate
extension LeftViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let title = lists[indexPath.row]
        switch title {
        case .CharingRecord:
//            guard KJLRequest.isLogin() == true else {
//                KJLCommon.showLogin(subcontrol: self)
//                return
//            }
//            if let vc = storyboard?.instantiateViewController(withIdentifier: "ChargingRecordViewController") as? ChargingRecordViewController {
//                navigationController?.show(vc, sender: self)
//            }
            break
        case .PaySetting:
//            guard KJLRequest.isLogin() == true else {
//                KJLCommon.showLogin(subcontrol: self)
//                return
//            }
//            if let vc = storyboard?.instantiateViewController(withIdentifier: "PaymentSelectSettingViewController") as? PaymentSelectSettingViewController {
//                mainController?.navigationController?.show(vc, sender: self)
//            }
            break
//        case .RoutePlaning:
//            guard KJLRequest.isLogin() == true else {
//                KJLCommon.showLogin(subcontrol: self)
//                return
//            }
//            if let vc = storyboard?.instantiateViewController(withIdentifier: "RoutePlaningViewController") as? RoutePlaningViewController {
//                let nav = UINavigationController.init(rootViewController: vc)
//                nav.isNavigationBarHidden = true
//                nav.modalPresentationStyle = .fullScreen
//                mainController?.navigationController?.present(nav, animated: true, completion: nil)
//            }
//            break
        case .News:
//            if let vc = storyboard?.instantiateViewController(withIdentifier: "LatestNewsViewController") as? LatestNewsViewController {
//                mainController?.navigationController?.show(vc, sender: self)
//            }
            break
//        case .NewChargeStation:
//            guard KJLRequest.isLogin() == true else {
//                KJLCommon.showLogin(subcontrol: self)
//                return
//            }
//            if let vc = storyboard?.instantiateViewController(withIdentifier: "NewPlaceSearchViewController") as? NewPlaceSearchViewController {
//                mainController?.navigationController?.show(vc, sender: self)
//            }
//            break
        case .PushSetting:
//            guard KJLRequest.isLogin() == true else {
//                KJLCommon.showLogin(subcontrol: self)
//                return
//            }
//
//            if let vc = storyboard?.instantiateViewController(withIdentifier: "NewsFilterViewController") as? NewsFilterViewController {
//                navigationController?.show(vc, sender: self)
//            }
            break
        case .Contact:
//            if let vc = storyboard?.instantiateViewController(withIdentifier: "ContactListViewController") as? ContactListViewController {
//                mainController?.navigationController?.show(vc, sender: self)
//            }
            break
        case .AppGuide:
//            if let vc = storyboard?.instantiateViewController(withIdentifier: "AppGuideViewController") as? AppGuideViewController {
//                let nav = UINavigationController.init(rootViewController: vc)
//                nav.isNavigationBarHidden = true
//                vc.isFirst = false
//                navigationController?.show(nav, sender: self)
//            }
            break
        case .ReseveRecord:
//            guard KJLRequest.isLogin() == true else {
//                KJLCommon.showLogin(subcontrol: self)
//                return
//            }
//            if let vc = storyboard?.instantiateViewController(withIdentifier: "ReserveRecordViewController") as? ReserveRecordViewController {
//                navigationController?.show(vc, sender: self)
//            }
            break
        case .Login:
//            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SelectLoginViewController")
//            vc.modalPresentationStyle = .overFullScreen
//            vc.modalTransitionStyle   = .crossDissolve
//            self.present(vc, animated: true, completion: nil)
            break
        case .FunPushMailBox:
//            guard KJLRequest.isLogin() == true else {
//                KJLCommon.showLogin(subcontrol: self)
//                return
//            }
//            let inboxViewController = InboxViewController()
//            mainController?.navigationController?.hidesBottomBarWhenPushed = true; mainController?.navigationController?.show(inboxViewController, sender: self)
            break
        case .Subscription   : break
        case .StationList    : break
        case .PaymentSetting : break
        case .ChargingHistory: break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 48
    }
}


extension LeftViewController {
    @objc private func callNewTypeCount() {
        KJLRequest.requestForGetNewsBadge {[weak self] (response) in
            guard let response = response as? NewTypeCountModel,
                let data = response.datas
                else { return }
            let new = data.newestCount ?? 0
            let active = data.activeCount ?? 0
            let warning = data.warningCount ?? 0
            let push = data.pushCount ?? 0
            let total = new + active + warning + push
            UIApplication.shared.applicationIconBadgeNumber = total
            UserDefaults.standard.set(total, forKey: "UnReadCount")
            self?.messageTotal = new + active + warning + push
            self?.ktable.reloadData()
        }
    }
}
