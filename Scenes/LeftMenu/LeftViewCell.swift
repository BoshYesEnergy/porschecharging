//
//  LeftViewCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class LeftViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var countLab: UILabel!
     
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func settingView(message: Bool, count: Int = 0) {
        if message && KJLRequest.isLogin() == true {
            countLab.isHidden = false
            if count == 0 {
                countLab.text = ""
            }
            countLab.text = "\(count)"
        } else {
            countLab.isHidden = true
            countLab.text  = ""
        }
    }

    func setupCellWith(titleString: String) {
        self.titleLabel.text = titleString
        if (titleString == "登    入") {
//            self.titleLabel.textColor = #colorLiteral(red: 0, green: 0.768627451, blue: 0.7960784314, alpha: 1)
        }
    }
}
