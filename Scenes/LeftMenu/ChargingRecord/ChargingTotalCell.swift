//
//  ChargingTotalCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class ChargingTotalCell: UITableViewCell {

    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var titleCell: UILabel!
    @IBOutlet weak var countCell: UILabel!
    @IBOutlet weak var unitCell: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        titleCell.text = ""
        countCell.text = ""
        unitCell.text = ""
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func changeData(imageNameString: String , title: String?, count: String?, unit: String?) {
        self.bgImage.image = UIImage(named: imageNameString)
        titleCell.text = title
        countCell.text = count
        unitCell.text = unit
    }
    
    
}
