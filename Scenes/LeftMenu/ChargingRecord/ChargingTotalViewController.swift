//
//  ChargingTotalViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class ChargingTotalViewController: KJLNavView {

    @IBOutlet weak var ktable: UITableView!
    @IBOutlet weak var firstDateBtn: UIButton!
    @IBOutlet weak var lastDateBtn: UIButton!
    @IBOutlet weak var firstDateLab: UILabel!
    @IBOutlet weak var lastDateLab: UILabel!
    @IBOutlet weak var cornerView: UIView!
    
    var startDateStr = ""
    var endDateStr = ""
    
    let names = ["總金額", "總度數", "總時間", "總次數"]
    let units = ["元", "度", "秒", "次"]
    let imageArray = ["charging_ic_fee","charging_ic_degree ","charging_ic_time","charging_ic_times"]
    var data: ChargeStatisticsClass.Data?
    
    override func viewDidLoad() {
        super.viewDidLoad() 
        createNavViewForRightBtn(rightName: "", rightImage: "", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: UIColor.black)
        //titleLab?.text = "充電紀錄"
        titleLab?.text = "充電紀錄統計"
//        firstDateBtn.setTitle(startDateStr, for: .normal)
//        lastDateBtn.setTitle(endDateStr, for: .normal)
        
        self.cornerView.backgroundColor = UIColor.clear
        self.cornerView.layer.cornerRadius = self.cornerView.bounds.height / 2
        self.cornerView.layer.masksToBounds = true
        self.cornerView.layer.borderColor = UIColor.black.cgColor
        self.cornerView.layer.borderWidth = 1
        
        var t1 = self.startDateStr
        t1 = t1.replacingOccurrences(of: "-", with: "-")
        var t2 = self.endDateStr
        t2 = t2.replacingOccurrences(of: "-", with: "-")
        firstDateLab.text = t1
        lastDateLab.text = t2
        
        callApi()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func callApi() {
        KJLRequest.requestForChargeStatistics(startTime: startDateStr, endTime: endDateStr) { (response) in
            guard let response = response as? ChargeStatisticsClass else{
                return
            }
            
            self.data = response.datas
            self.ktable.reloadData()
        }
    }
    
    @IBAction func selectDate(_ sender: UIButton) {
        let picker = Bundle.main.loadNibNamed("KJLDatePickerView", owner: self, options: nil)?[0] as? KJLDatePickerView
        picker?.show()
        if sender == firstDateBtn {
            let format = DateFormatter()
            format.dateFormat = "yyyy-MM-dd"
            var tempDate = format.date(from: endDateStr)
            picker?.kPickerView.maximumDate = tempDate
            tempDate = format.date(from: startDateStr)
            if let tempDate = tempDate {
                picker?.kPickerView.setDate(tempDate, animated: true)
            }
        }else {
            let format = DateFormatter()
            format.dateFormat = "yyyy-MM-dd"
            var tempDate = format.date(from: startDateStr)
            picker?.kPickerView.minimumDate = tempDate
            picker?.kPickerView.maximumDate = Date(timeIntervalSinceNow: 0)
            tempDate = format.date(from: endDateStr)
            if let tempDate = tempDate {
                picker?.kPickerView.setDate(tempDate, animated: true)
            }
        }
        picker?.pickerCompletion = { (date) in
            if date != nil {
                let format = DateFormatter()
                format.dateFormat = "yyyy-MM-dd"
                if sender == self.firstDateBtn {
                    self.startDateStr = format.string(from: date!)
//                    sender.setTitle(self.startDateStr, for: .normal)
                    var t = self.startDateStr
                    t = t.replacingOccurrences(of: "-", with: "-")
                    self.firstDateLab.text = t
                }else {
                    self.endDateStr = format.string(from: date!)
//                    sender.setTitle(self.endDateStr, for: .normal)
                    var t = self.endDateStr
                    t = t.replacingOccurrences(of: "-", with: "-")
                    self.lastDateLab.text = t
                }
                
                self.callApi()
            }
        }
    }
    
    private func timeConvertToStringWith(time: Int64) -> String {
        let sec = time % 60
        let min = time / 60
        let newMin = min % 60
        let hour = min / 60
        //        let newHour = hour % 24
        //        let day = hour / 24
        let secString = (sec > 9) ? "\(sec)" : "0\(sec)"
        let minString = (min > 9) ? "\(newMin)" : "0\(newMin)"
        let hourString = (hour > 9) ? "\(hour)" : "0\(hour)"
        //        let hourString = (hour > 9) ? "\(newHour)" : "0\(newHour)"
        //        return "\(day)天又 \(hourString) 小時 \(minString) 分 \(secString) 秒"
        return "\(hourString):\(minString):\(secString)"
    }

}

extension ChargingTotalViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return names.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChargingTotalCell", for: indexPath)
        
        if let cell = cell as? ChargingTotalCell {
            let row = indexPath.row
            switch row {
            case 0:
                cell.changeData(imageNameString: imageArray[row] ,title: names[row], count: "\(data?.totalAmount ?? 0)", unit: units[row])
            case 1:
                cell.changeData(imageNameString: imageArray[row] ,title: names[row], count: data?.totalKwh, unit: units[row])
            case 2:
                cell.unitCell.isHidden = true
                let amt = data?.totalTime ?? 0
                let str = self.timeConvertToStringWith(time: amt)
                cell.changeData(imageNameString: imageArray[row] ,title: names[row], count: str, unit: units[row])
            case 3:
                let count = data?.totalCount ?? 0
                cell.changeData(imageNameString: imageArray[row] ,title: names[row], count: "\(count)", unit: units[row])
            default:
                break
            }
        }
        
        return cell
    }
    
    
}
