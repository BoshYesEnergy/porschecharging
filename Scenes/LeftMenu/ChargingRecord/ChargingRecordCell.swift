//
//  ChargingRecordCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class ChargingRecordCell: UITableViewCell {

    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var stationLabel: UILabel!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.timeLabel.text = ""
        self.stationLabel.text = ""
        self.resultLabel.text = ""
        self.priceLabel.text = ""
    }

    func changeData(data: ChargeListClass.Data) {
        switch data.paymentStatus {
        case "付款成功": self.resultLabel.textColor = #colorLiteral(red: 0.262745098, green: 0.6745098039, blue: 0.2039215686, alpha: 1)
        case "無需付款":  self.resultLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        case "交易未完成": self.resultLabel.textColor = #colorLiteral(red: 1, green: 0.7882352941, blue: 0, alpha: 1)
        case "付款失敗": self.resultLabel.textColor = #colorLiteral(red: 0.7843137255, green: 0.3254901961, blue: 0.2392156863, alpha: 1)
        default: self.resultLabel.textColor = #colorLiteral(red: 0.5568627451, green: 0.5568627451, blue: 0.5764705882, alpha: 1)  // 無付款紀錄
        }
        self.timeLabel.text = data.startTime
        self.stationLabel.text = data.stationName
        self.resultLabel.text = "交易結果：\(data.paymentStatus ?? "")"
        self.priceLabel.text = "$\(data.amount ?? 0)"
    }
}
