//
//  ChargingRecordViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class ChargingRecordViewController: KJLNavView {

    // MARK: - IBOutlet
    @IBOutlet weak var ktable: UITableView!
    @IBOutlet weak var selectLineLeft: NSLayoutConstraint!
    @IBOutlet weak var selectLine: UIView!
    @IBOutlet weak var allBtn: UIButton!
    @IBOutlet weak var easyCardBtn: UIButton!
    @IBOutlet weak var creditCardBtn: UIButton!
    @IBOutlet weak var thirdpartyBtn: UIButton!
    @IBOutlet weak var firstDateBtn: UIButton!
    @IBOutlet weak var lastDateBtn: UIButton!
    @IBOutlet weak var firstDateLab: UILabel!
    @IBOutlet weak var lastDateLab: UILabel!
    @IBOutlet weak var cornerView: UIView!
    
    let refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "更新資料中...", attributes: [.foregroundColor: UIColor.black])
        refreshControl.addTarget(self, action: #selector(callChargeList), for: .valueChanged)
        return refreshControl
    }()
    
    // MARK: - Property
    var selectLists: [ChargeListClass.Data] = []
    var startDateStr = ""
    var endDateStr = ""
    weak var tempBtn: UIButton? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.cornerView.backgroundColor = UIColor.clear
        self.cornerView.layer.cornerRadius = self.cornerView.bounds.height / 2
        self.cornerView.layer.masksToBounds = true
        self.cornerView.layer.borderColor = UIColor.black.cgColor
        self.cornerView.layer.borderWidth = 1
        
        //ktable.setupMerryChristmas()
        
        createNavViewForRightBtn(rightName: "", rightImage: "charging_ic_chart", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: UIColor.black)
        titleLab?.text = "充電紀錄"
        rightBtn?.addTarget(self, action: #selector(gotoTotalView), for: .touchUpInside)
        self.backBtn?.isHidden = true
        let startDate = Date(timeIntervalSinceNow: -7 * 24 * 60 * 60)
        let endDate = Date(timeIntervalSinceNow: 0)
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd"
        startDateStr = format.string(from: startDate)
        endDateStr = format.string(from: endDate)
        
//        firstDateBtn.setTitle(startDateStr, for: .normal)
//        lastDateBtn.setTitle(endDateStr, for: .normal)
        var t1 = self.startDateStr
        t1 = t1.replacingOccurrences(of: "-", with: "-")
        var t2 = self.endDateStr
        t2 = t2.replacingOccurrences(of: "-", with: "-")
        firstDateLab.text = t1
        lastDateLab.text = t2
//        dateRangeLab.text = startDateStr + " - " + endDateStr
        
        self.selectMenuAction(self.allBtn)
        ktable.refreshControl = refreshControl
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) 
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - 自訂 Func
    @objc func gotoTotalView() {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "ChargingTotalViewController") as? ChargingTotalViewController {
            vc.startDateStr = startDateStr
            vc.endDateStr = endDateStr
            navigationController?.show(vc, sender: self)
        }
    }
    
    func selectData(sender: UIButton) {
        tempBtn = sender
        guard KJLRequest.isLogin() == true else {
                self.selectLists.removeAll()
                self.ktable.reloadData()
                return
        }
        var payment: KJLRequest.RequestPayment = .none
        if sender == allBtn {
            payment = KJLRequest.RequestPayment.All
        } else if sender == easyCardBtn {
            payment = KJLRequest.RequestPayment.UUCard
        } else if sender == creditCardBtn {
            payment = KJLRequest.RequestPayment.CreditCard
        } else if sender == thirdpartyBtn {
            payment = KJLRequest.RequestPayment.ThirdParty
        }
        //MARK: API-406
        KJLRequest.requestForChargeList(startTime: startDateStr, endTime: endDateStr, payment: payment) { [weak self] (response) in
            guard let response = response as? ChargeListClass else { return }
            self?.selectLists.removeAll()
            self?.selectLists = response.datas ?? []
            self?.ktable.reloadData()
            self?.refreshControl.endRefreshing()
        }
    }
    
    @objc private func callChargeList() {
        if let temp = self.tempBtn {
            refreshControl.beginRefreshing()
            self.selectMenuAction(temp)
        }
    }
    
    //MARK: - IBAction
    @IBAction func selectMenuAction(_ sender: UIButton) {
        allBtn.setTitleColor(#colorLiteral(red: 0.6077751517, green: 0.6078823209, blue: 0.6077683568, alpha: 1), for: .normal)
        easyCardBtn.setTitleColor(#colorLiteral(red: 0.6077751517, green: 0.6078823209, blue: 0.6077683568, alpha: 1), for: .normal)
        creditCardBtn.setTitleColor(#colorLiteral(red: 0.6077751517, green: 0.6078823209, blue: 0.6077683568, alpha: 1), for: .normal)
        thirdpartyBtn.setTitleColor(#colorLiteral(red: 0.6077751517, green: 0.6078823209, blue: 0.6077683568, alpha: 1), for: .normal)
        
        sender.setTitleColor(#colorLiteral(red: 0.1497560143, green: 0.7302395701, blue: 0.7572383881, alpha: 1), for: .normal)
        selectLineLeft.constant = (sender.superview?.center.x ?? 0) - (selectLine.frame.width/2)
        
        selectData(sender: sender)
    }
    
    @IBAction func selectDate(_ sender: UIButton) {
        let picker = Bundle.main.loadNibNamed("KJLDatePickerView", owner: self, options: nil)?[0] as? KJLDatePickerView
        picker?.show()
        if sender == firstDateBtn {
            let format = DateFormatter()
            format.dateFormat = "yyyy-MM-dd"
            var tempDate = format.date(from: endDateStr)
            picker?.kPickerView.maximumDate = tempDate
            tempDate = format.date(from: startDateStr)
            if let tempDate = tempDate {
                picker?.kPickerView.setDate(tempDate, animated: true)
            }
        } else {
            let format = DateFormatter()
            format.dateFormat = "yyyy-MM-dd"
            var tempDate = format.date(from: startDateStr)
            picker?.kPickerView.minimumDate = tempDate
            picker?.kPickerView.maximumDate = Date(timeIntervalSinceNow: 0)
            tempDate = format.date(from: endDateStr)
            if let tempDate = tempDate {
                picker?.kPickerView.setDate(tempDate, animated: true)
            }
        }
        picker?.pickerCompletion = { (date) in
            if date != nil {
                let format = DateFormatter()
                format.dateFormat = "yyyy-MM-dd"
                if sender == self.firstDateBtn {
                    self.startDateStr = format.string(from: date!)
//                    sender.setTitle(self.startDateStr, for: .normal)
                    var t = self.startDateStr
                    t = t.replacingOccurrences(of: "-", with: "-")
                    self.firstDateLab.text = t
                } else {
                    self.endDateStr = format.string(from: date!)
//                    sender.setTitle(self.endDateStr, for: .normal)
                    var t = self.endDateStr
                    t = t.replacingOccurrences(of: "-", with: "-")
                    self.lastDateLab.text = t
                }
                
//                if sender == self.lastDateBtn {
//                    if let temp = self.tempBtn {
//                        self.selectMenuAction(temp)
//                    }
//                }
                if let temp = self.tempBtn {
                    self.selectMenuAction(temp)
                }
            }
        }
    }
}

// MARK: - TableView DataSource
extension ChargingRecordViewController:  UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectLists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChargingRecordCell", for: indexPath)  as! ChargingRecordCell
        cell.changeData(data: selectLists[indexPath.row]) 
        return cell
    }
}

// MARK: - TableView Delegate
extension ChargingRecordViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) { 
        let data = selectLists[indexPath.row]
        if data.type == "1" {
            let vc = CarCompleteChargingViewController()
            vc.tranNo = data.tranNo
            vc.delay = false
            let nav:UINavigationController = UINavigationController.init(rootViewController: vc)
            nav.isNavigationBarHidden = true
            nav.modalPresentationStyle = .fullScreen
            self.present(nav, animated: true, completion: nil)
        } else {
//            guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "CompleteChargingViewController") as? CompleteChargingViewController
//                else{ return }
//            let data = selectLists[indexPath.row]
//            vc.tranNo = data.tranNo
//            vc.delay = false
//            self.navigationController?.show(vc, sender: self)
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
}
