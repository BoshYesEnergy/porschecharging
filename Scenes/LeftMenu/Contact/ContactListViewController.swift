//
//  ContactListViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class ContactListViewController: KJLNavView {

    @IBOutlet weak var ktable: UITableView!
    var lists = ["聯絡客服", "申請建樁", "常見問題" ,"隱私權與條款" ,/*"APP Guide"*/]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        createNavViewForRightBtn(rightName: "", rightImage: "", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: UIColor.black)
        titleLab?.text = "關於我們"
        
        ktable.register(UINib(nibName: "NormalCell", bundle: nil), forCellReuseIdentifier: "NormalCell")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) 
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ContactListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ContactListCell", for: indexPath) as? ContactListCell {
        
            cell.titleLabel.text = lists[indexPath.row]
            
            return cell
        }
        return UITableViewCell(style: .default, reuseIdentifier: "")
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            if let vc = storyboard?.instantiateViewController(withIdentifier: "CustomerServiceViewController") as? CustomerServiceViewController {
                vc.lists = ["合作咨詢", "App使用問題", "其他"]
                navigationController?.show(vc, sender: self)
            }
        case 1:
            if let vc = storyboard?.instantiateViewController(withIdentifier: "ApplyGuideViewController") as? ApplyGuideViewController {
                navigationController?.show(vc, sender: self)
            }
        case 2:
            if let vc = storyboard?.instantiateViewController(withIdentifier: "CommonQuestionViewController") as? CommonQuestionViewController {
                navigationController?.show(vc, sender: self)
            }
        case 3:
            guard let vc = storyboard?.instantiateViewController(withIdentifier: "CustomerServiceViewController") as? CustomerServiceViewController else { return }
            vc.lists = ["隱私權政策" , "使用條款"]
            navigationController?.show(vc, sender: self)
            
        case 4:
//            if let vc = storyboard?.instantiateViewController(withIdentifier: "AppGuideViewController") as? AppGuideViewController {
//                let nav = UINavigationController.init(rootViewController: vc)
//                nav.isNavigationBarHidden = true
//                vc.isFirst = false
//                navigationController?.show(nav, sender: self)
//            }
            break
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
