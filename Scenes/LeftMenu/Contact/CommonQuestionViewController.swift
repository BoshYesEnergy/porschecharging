//
//  CommonQuestionViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class CommonQuestionViewController: KJLNavView {

    @IBOutlet weak var ktable: UITableView!
//    let lists = ["充電網icon說明", "充電樁是否與電動車相容？", "如何查詢充電紀錄？", "如何查詢站點？", "如何申請建樁？", "如何路線規劃？", "如何設定付款？"]
    var lists: [QuestionListClass.Data] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        createNavViewForRightBtn(rightName: "", rightImage: "", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: UIColor.black)
        titleLab?.text = "常見問題"
        
        ktable.register(UINib(nibName: "NormalCell", bundle: nil), forCellReuseIdentifier: "NormalCell")
        
        KJLRequest.requestForQuestionList { (response) in
            guard let response = response as? QuestionListClass else {
                return
            }
            
            self.lists = response.datas ?? []
            self.ktable.reloadData()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) 
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
}

extension CommonQuestionViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NormalCell", for: indexPath)
        
        if let cell = cell as? NormalCell {
//            cell.changeTitle(name: lists[indexPath.row].title ?? "")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = lists[indexPath.row]
        if let vc = storyboard?.instantiateViewController(withIdentifier: "CommonQuestionDetailViewController") as? CommonQuestionDetailViewController {
            vc.questionNo = "\((data.questionNo ?? 0))" 
            navigationController?.show(vc, sender: self)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
