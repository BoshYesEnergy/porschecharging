//
//  CustomerServiceCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class CustomerServiceCell: UITableViewCell {

    @IBOutlet weak var mainViewCell: UIView!
    @IBOutlet weak var titleLab: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
//    override func layoutIfNeeded() {
//        mainViewCell.layoutIfNeeded()
//        mainViewCell.shadowsType(kcolor: #colorLiteral(red: 0.1497560143, green: 0.7302395701, blue: 0.7572383881, alpha: 1))
//    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
