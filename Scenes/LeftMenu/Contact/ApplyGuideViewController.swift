//
//  ApplyGuideViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class ApplyGuideViewController: KJLNavView {

    override func viewDidLoad() {
        super.viewDidLoad()

        createNavViewForRightBtn(rightName: "", rightImage: "", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: UIColor.black)
        titleLab?.text = "申請流程說明"
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func applyAction(_ sender: Any) { 
        if let vc = storyboard?.instantiateViewController(withIdentifier: "ApplyViewController") as? ApplyViewController {
            navigationController?.show(vc, sender: self)
        }
    }
    
}
