//
//  CommonQuestionDetailViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class CommonQuestionDetailViewController: KJLNavView {

    var questionNo: String = ""
    
    @IBOutlet weak var picImage: UIImageView!
    @IBOutlet weak var picImageHeight: NSLayoutConstraint!
    @IBOutlet weak var questionTitleLab: UILabel!
    @IBOutlet weak var contentLab: UILabel!
    @IBOutlet weak var contentLabHeight: NSLayoutConstraint!
    @IBOutlet weak var contentText: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

       createNavViewForRightBtn(rightName: "", rightImage: "", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: UIColor.black)
        titleLab?.text = "常見問題"
        
        KJLRequest.requestForQuestionDetail(questionNo: questionNo) { (response) in
            guard let response = response as? QuestionDetailClass else {
                return
            }
            
            self.questionTitleLab.text = response.datas?.title
//            self.contentLab.text = response.datas?.text
            
            var txt = response.datas?.text ?? ""
            txt = "<font style=\"font-size:16px; color:#716F81\">" + txt + "</font>"
            let txt2 = txt.data.attributedString
            //            self.contentLab.text = response.datas?.text
            self.contentLab.attributedText = txt2
            self.contentText.attributedText = txt2
            self.contentLabHeight.constant = self.contentText.contentSize.height
            
            if let tempUrl = URL(string: response.datas?.picUrl ?? "") {
                self.picImage.kf.setImage(with: tempUrl, placeholder: nil, options: nil, progressBlock: nil) { (img, error, type, url) in
                    if img == nil {
                        self.picImage.image = KJLCommon.defaultImage()
                    }else {
                        self.picImage.image = img
                    }
                }
                self.picImageHeight.constant = 150
            }else {
                self.picImageHeight.constant = 0
            }
            
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.contentLabHeight.constant = self.contentText.contentSize.height
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}
