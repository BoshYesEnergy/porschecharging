//
//  ApplyViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class ApplyViewController: KJLNavView {

    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var areaField: UITextField!
    @IBOutlet weak var typeField: UITextField!
    
    var type = ""
    var loc = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        createNavViewForRightBtn(rightName: "", rightImage: "", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: UIColor.black)
        titleLab?.text = "申請建樁"
        
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    // 提交
    @IBAction func sendAction(_ sender: Any) {
        let name = nameField.text ?? ""
        let phone = phoneField.text ?? ""
        
        if name.isEmpty || phone.isEmpty || loc.isEmpty {
            var msg = ""
            if name.isEmpty {
                if msg.isEmpty {
                    msg = "姓名"
                }else {
                    msg = " / 姓名"
                }
            }
            if phone.isEmpty {
                if msg.isEmpty {
                    msg = msg + "手機"
                }else {
                    msg = msg + " / 手機"
                }
            }
            if loc.isEmpty {
                if msg.isEmpty {
                    msg = msg + "地區"
                }else {
                    msg = msg + " / 地區"
                }
            }
            msg = "請填寫 " + msg
            ShowMessageView.showMessage(title: nil, message: msg)
            return
        }
        
        // 手機格式判斷
        guard KJLCommon.is886Phone(str: phone) == true else {
            ShowMessageView.showMessage(title: nil, message: "手機格式輸入錯誤，請再重新輸入")
            return
        }
        
        KJLRequest.requestForApplyStation(name: name, phone: phone, loc: loc, carType: type) { (response) in
            if response == nil {
                ShowMessageView.showMessage(title: nil, message: "提交失敗")
            }else {
                ShowMessageView.showMessage(title: nil, message: "您成功提交需求，YES充電網將有專人盡快與您聯繫，謝謝！", completion: { (isOK) in
                    if let vc = self.navigationController?.viewControllers[1] {
                        self.navigationController?.popToViewController(vc, animated: true)
                    }
                })
            }
        }
    }
    
}

extension ApplyViewController: UITextFieldDelegate {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
//        var rect = self.view.frame
//        rect.origin.y = -100
//        self.view.frame = rect
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
//        var rect = self.view.frame
//        rect.origin.y = 0
//        self.view.frame = rect
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == typeField {
            // 車型
            let picker = Bundle.main.loadNibNamed("KJLPickerOne", owner: self, options: nil)?[0] as? KJLPickerOne
            picker?.show()
            picker?.pickerCompletion = { (data) in
                if let data = data {
                    if let name = data.carName {
                        textField.text = name
                        self.type = data.carNo == nil ? "" : "\(data.carNo!)"
                    } 
                } 
            }
            view.endEditing(true)
            return false
        }else if textField == areaField {
            // 地區
            let picker = Bundle.main.loadNibNamed("KJLPickerTwo", owner: self, options: nil)?[0] as? KJLPickerTwo
            picker?.show()
            picker?.pickerCompletion = { (name) in  
                if let name = name {
                    textField.text = name
                    self.loc = name
                }
            }
            view.endEditing(true)
            return false
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneField {
            if string == "" {
                return true
            }
            
            if (textField.text?.count ?? 0) >= 10 {
                return false
            }
            
            let aSet = CharacterSet(charactersIn: "0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            
            return string == numberFiltered
        }
        
        return true
    }
}
