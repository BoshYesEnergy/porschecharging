//
//  CustomerServiceViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit
import MessageUI

class CustomerServiceViewController: KJLNavView {

    @IBOutlet weak var ktable: UITableView!
    
    var lists: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        createNavViewForRightBtn(rightName: "", rightImage: "", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: UIColor.black)
        if lists.count == 2 {
            titleLab?.text = "隱私權與條款"
        } else {
            titleLab?.text = "聯絡客服"
        }
//        ktable.contentInset = UIEdgeInsetsMake(8, 0, 0, 0)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) 
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension CustomerServiceViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomerServiceCell", for: indexPath)
        
        if let cell = cell as? CustomerServiceCell {
            cell.titleLab.text = lists[indexPath.row]
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        sendEmail(subject: "我要聯繫充電網" + lists[indexPath.row] + "!")
        switch lists.count {
        case 2:
            switch indexPath.row {
            case 0:break
            case 1:break
            default: break
            }
            break
        case 3:
            switch indexPath.row {
            case 0: sendEmail(subject: "聯繫YES! 來電，進行合作諮詢！")
            case 1: sendEmail(subject: "聯繫YES! 來電，諮詢APP使用問題！")
            case 2: sendEmail(subject: "聯繫YES! 來電，我有其他問題！")
            default: break
            }
        default:  break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

extension CustomerServiceViewController: MFMailComposeViewControllerDelegate {
    func sendEmail(subject: String) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setSubject(subject)
            mail.setToRecipients(["yes@yulon-energy.com.tw"])
            mail.setMessageBody("", isHTML: true)//<p></p>
            
            let device = UIDevice.current.modelNameShow
            let version = UIDevice.current.systemVersion
            let appVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String ?? ""
            if subject == "聯繫YES! 來電，進行合作諮詢！" {
                let str = "<p>Hi 我們是YES! 來電團隊，請簡單描述您的合作概念，幫助我們第一時間更了解您的想法！</p><BR><BR><p>Device : \(device)</p><p>iOS Version : \(version)</p><p>APP Version : \(appVersion)</p>"
                mail.setMessageBody(str, isHTML: true)
            }else if subject == "聯繫YES! 來電，諮詢APP使用問題！" {
                let str = "<p>Hi 我們是YES! 來電團隊，請簡單描述您所遇到的問題，幫助我們第一時間為您解決！</p><BR><BR><p>Device : \(device)</p><p>iOS Version : \(version)</p><p>APP Version : \(appVersion)</p>"
                mail.setMessageBody(str, isHTML: true)
            }else if subject == "聯繫YES! 來電，我有其他問題！" {
                let str = "<p>Hi 我們是YES! 來電團隊，請簡單描述您所遇到的問題，幫助我們第一時間為您解決！</p><BR><BR><p>Device : \(device)</p><p>iOS Version : \(version)</p><p>APP Version : \(appVersion)</p>"
                mail.setMessageBody(str, isHTML: true)
            }
            mail.modalPresentationStyle = .fullScreen
            present(mail, animated: true)
        } else {
            ShowMessageView.showMessage(title: nil, message: "沒有email帳號")
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}
