//
//  KJLNavView.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class KJLNavView: UIViewController {

    var navView: UIView? = nil
    var backBtn: UIButton? = nil
    var titleLab: UILabel? = nil
    var rightBtn: UIButton? = nil
    
    var isCarButtonOpen = false
    var carBtn: UIButton? = nil
    let btnWidth: CGFloat = 60
    let btnHeight: CGFloat = 44
    var height = 0
    
    var closeCheck = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if closeCheck == false { /// use present modally , short navview need
            if KJLCommon.isX() == true {
                height = 88
            }else {
                height = 64
            }
        } else {
            height = 64
        }
         
        createNavView()
    }
     
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func createNavView() {
        // view
        if navView != nil {
            navView?.removeFromSuperview()
        }
        
        navView = UIView(frame: .zero)
        guard let navView = navView else {
            return
        }
        
        // back button
        if backBtn != nil {
            backBtn?.removeFromSuperview()
        }
        backBtn = UIButton(frame: .zero)
        guard let backBtn = backBtn else {
            return
        }
        
        // title
        if titleLab != nil {
            titleLab?.removeFromSuperview()
        }
        titleLab = UILabel(frame: .zero)
        guard let titleLab = titleLab else {
            return
        }
        
        // view
        navView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(navView)
        navView.backgroundColor = UIColor.clear
        
        var viewConstrains: [NSLayoutConstraint] = []
        var bindings: [String: UIView] = ["navView": navView]
        var horiStr = "H:|-0-[navView]-0-|"
        var virStr = "V:|-0-[navView(\(height))]"
        
        viewConstrains.append(contentsOf: NSLayoutConstraint.constraints(withVisualFormat: horiStr, options: .directionLeadingToTrailing, metrics: nil, views: bindings))
        viewConstrains.append(contentsOf: NSLayoutConstraint.constraints(withVisualFormat: virStr, options: .directionLeadingToTrailing, metrics: nil, views: bindings))
        
        view.addConstraints(viewConstrains)
//        view.sendSubview(toBack: navView)
        
        viewConstrains = []
        // back button
        backBtn.translatesAutoresizingMaskIntoConstraints = false
        backBtn.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        navView.addSubview(backBtn)
        backBtn.backgroundColor = UIColor.clear
        backBtn.setImage(UIImage(named: "ic_backspace_press"), for: .normal)
        bindings = ["backBtn": backBtn]
//        horiStr = "H:|-0-[backBtn(44)]|"
        virStr = "V:[backBtn(\(btnHeight))]-0-|"

//        viewConstrains.append(contentsOf: NSLayoutConstraint.constraints(withVisualFormat: horiStr, options: .directionLeadingToTrailing, metrics: nil, views: bindings))
        viewConstrains.append(contentsOf: NSLayoutConstraint.constraints(withVisualFormat: virStr, options: .directionLeadingToTrailing, metrics: nil, views: bindings))
                
        // title
        titleLab.textColor = #colorLiteral(red: 0.3176088035, green: 0.3176690638, blue: 0.3176049888, alpha: 1)
        titleLab.font = UIFont.systemFont(ofSize: 18.0)
        titleLab.translatesAutoresizingMaskIntoConstraints = false
        titleLab.textAlignment = .center
        navView.addSubview(titleLab)
        titleLab.backgroundColor = UIColor.clear
        bindings = ["titleLab": titleLab, "backBtn": backBtn]
        horiStr = "H:|-0-[backBtn(\(btnWidth))]-0-[titleLab]-\(btnWidth)-|"
        virStr = "V:[titleLab(\(btnHeight))]-0-|"
        
        viewConstrains.append(contentsOf: NSLayoutConstraint.constraints(withVisualFormat: horiStr, options: .directionLeadingToTrailing, metrics: nil, views: bindings))
        viewConstrains.append(contentsOf: NSLayoutConstraint.constraints(withVisualFormat: virStr, options: .directionLeadingToTrailing, metrics: nil, views: bindings))
        
        navView.addConstraints(viewConstrains)
    }
    
    func createNavViewForRightBtn(rightName: String, rightImage: String, leftName: String, leftImage: String, groundColor: UIColor?, isShadow: Bool, titleColor: UIColor?) {
        // view
        if navView != nil {
            navView?.removeFromSuperview()
        }
        
        navView = UIView(frame: .zero)
        guard let navView = navView else {
            return
        }
        
        // back button
        if backBtn != nil {
            backBtn?.removeFromSuperview()
        }
        backBtn = UIButton(frame: .zero)
        guard let backBtn = backBtn else {
            return
        }
        
        // title
        if titleLab != nil {
            titleLab?.removeFromSuperview()
        }
        titleLab = UILabel(frame: .zero)
        guard let titleLab = titleLab else {
            return
        }
        
        // right btn
        if rightBtn != nil {
            rightBtn?.removeFromSuperview()
        }
        rightBtn = UIButton(frame: .zero)
        guard let rightBtn = rightBtn else {
            return
        }
        
        if carBtn != nil {
            carBtn?.removeFromSuperview()
        }
        carBtn = UIButton(frame: .zero)
        guard let carBtn = carBtn else {
            return
        }
        
        // view
        navView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(navView)
        if groundColor == nil {
            navView.backgroundColor = UIColor.clear
        }else {
            navView.backgroundColor = groundColor
        }
        
        
        var viewConstrains: [NSLayoutConstraint] = []
        var bindings: [String: UIView] = ["navView": navView]
        var horiStr = "H:|-0-[navView]-0-|"
        var virStr = "V:|-0-[navView(\(height))]"
        
        viewConstrains.append(contentsOf: NSLayoutConstraint.constraints(withVisualFormat: horiStr, options: .directionLeadingToTrailing, metrics: nil, views: bindings))
        viewConstrains.append(contentsOf: NSLayoutConstraint.constraints(withVisualFormat: virStr, options: .directionLeadingToTrailing, metrics: nil, views: bindings))
        
        view.addConstraints(viewConstrains)
        //        view.sendSubview(toBack: navView)
        
        viewConstrains = []
        // back button
        backBtn.translatesAutoresizingMaskIntoConstraints = false
        backBtn.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        navView.addSubview(backBtn)
        backBtn.backgroundColor = UIColor.clear
        backBtn.setTitle(leftName, for: .normal)
        backBtn.setImage(UIImage(named: leftImage), for: .normal)
        bindings = ["backBtn": backBtn]
        //        horiStr = "H:|-0-[backBtn(44)]|"
        virStr = "V:[backBtn(\(btnHeight))]-0-|"
        
        //        viewConstrains.append(contentsOf: NSLayoutConstraint.constraints(withVisualFormat: horiStr, options: .directionLeadingToTrailing, metrics: nil, views: bindings))
        viewConstrains.append(contentsOf: NSLayoutConstraint.constraints(withVisualFormat: virStr, options: .directionLeadingToTrailing, metrics: nil, views: bindings))
        
        
        // right btn
        rightBtn.backgroundColor = UIColor.clear
        rightBtn.translatesAutoresizingMaskIntoConstraints = false
//        rightBtn.setTitleColor(UIColor.white, for: .normal)
        rightBtn.setTitleColor(UIColor.black, for: .normal)
        rightBtn.setTitle(rightName, for: .normal)
        rightBtn.setImage(UIImage(named: rightImage), for: .normal)
        navView.addSubview(rightBtn)
        bindings = ["rightBtn": rightBtn]
        virStr = "V:[rightBtn(\(btnHeight))]-0-|"
         viewConstrains.append(contentsOf: NSLayoutConstraint.constraints(withVisualFormat: virStr, options: .directionLeadingToTrailing, metrics: nil, views: bindings))
        if isCarButtonOpen == true {
            
            carBtn.backgroundColor = UIColor.clear
            carBtn.translatesAutoresizingMaskIntoConstraints = false
            carBtn.setTitleColor(UIColor.black, for: .normal)
            carBtn.setTitle(rightName, for: .normal)
            carBtn.setImage(UIImage(named: rightImage), for: .normal)
            carBtn.imageView?.contentMode = .scaleAspectFit
            navView.addSubview(carBtn)
            bindings = ["carBtn": carBtn]
            virStr = "V:[carBtn(\(btnHeight))]-0-|"
            viewConstrains.append(contentsOf: NSLayoutConstraint.constraints(withVisualFormat: virStr, options: .directionLeadingToTrailing, metrics: nil, views: bindings))
        }
        
        
        // title
        if let titleColor = titleColor {
            titleLab.textColor = titleColor
        }else {
            titleLab.textColor = #colorLiteral(red: 0.3176088035, green: 0.3176690638, blue: 0.3176049888, alpha: 1)
        }
        titleLab.font = UIFont.systemFont(ofSize: 18.0)
        titleLab.translatesAutoresizingMaskIntoConstraints = false
        titleLab.textAlignment = .center
        navView.addSubview(titleLab)
        titleLab.backgroundColor = UIColor.clear
        
        if isCarButtonOpen == true {
            bindings = ["titleLab": titleLab, "backBtn": backBtn, "rightBtn": rightBtn,"carBtn": carBtn]
            horiStr = "H:|-0-[backBtn(\(btnWidth))]-0-[titleLab]-0-[carBtn(\(btnWidth))]-0-[rightBtn(\(btnWidth))]-0-|"
            virStr = "V:[titleLab(\(btnHeight))]-0-|"
        }else{
            bindings = ["titleLab": titleLab, "backBtn": backBtn, "rightBtn": rightBtn]
            horiStr = "H:|-0-[backBtn(\(btnWidth))]-0-[titleLab]-0-[rightBtn(\(btnWidth))]-0-|"
            virStr = "V:[titleLab(\(btnHeight))]-0-|"
        }
        
        
        viewConstrains.append(contentsOf: NSLayoutConstraint.constraints(withVisualFormat: horiStr, options: .directionLeadingToTrailing, metrics: nil, views: bindings))
        viewConstrains.append(contentsOf: NSLayoutConstraint.constraints(withVisualFormat: virStr, options: .directionLeadingToTrailing, metrics: nil, views: bindings))
        
        navView.addConstraints(viewConstrains)
        view.bringSubview(toFront: navView)
        
//        self.view.layoutIfNeeded()
        
        if isShadow == true {
            navView.layer.shadowOffset = CGSize(width: 0, height: 1)
            navView.layer.shadowColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            navView.layer.shadowOpacity = 0.5
        }
    }

    func createNavViewWithWhiteRightBtn(rightName: String, rightImage: String, leftName: String, leftImage: String, groundColor: UIColor?, isShadow: Bool, titleColor: UIColor?) {
        // view
        if navView != nil {
            navView?.removeFromSuperview()
        }
        
        navView = UIView(frame: .zero)
        guard let navView = navView else {
            return
        }
        
        // back button
        if backBtn != nil {
            backBtn?.removeFromSuperview()
        }
        backBtn = UIButton(frame: .zero)
        guard let backBtn = backBtn else {
            return
        }
        
        // title
        if titleLab != nil {
            titleLab?.removeFromSuperview()
        }
        titleLab = UILabel(frame: .zero)
        guard let titleLab = titleLab else {
            return
        }
        
        // right btn
        if rightBtn != nil {
            rightBtn?.removeFromSuperview()
        }
        rightBtn = UIButton(frame: .zero)
        guard let rightBtn = rightBtn else {
            return
        }
        
        // view
        navView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(navView)
        if groundColor == nil {
            navView.backgroundColor = UIColor.clear
        }else {
            navView.backgroundColor = groundColor
        }
        
        
        var viewConstrains: [NSLayoutConstraint] = []
        var bindings: [String: UIView] = ["navView": navView]
        var horiStr = "H:|-0-[navView]-0-|"
        var virStr = "V:|-0-[navView(\(height))]"
        
        viewConstrains.append(contentsOf: NSLayoutConstraint.constraints(withVisualFormat: horiStr, options: .directionLeadingToTrailing, metrics: nil, views: bindings))
        viewConstrains.append(contentsOf: NSLayoutConstraint.constraints(withVisualFormat: virStr, options: .directionLeadingToTrailing, metrics: nil, views: bindings))
        
        view.addConstraints(viewConstrains)
        //        view.sendSubview(toBack: navView)
        
        viewConstrains = []
        // back button
        backBtn.translatesAutoresizingMaskIntoConstraints = false
        backBtn.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        navView.addSubview(backBtn)
        backBtn.backgroundColor = UIColor.clear
        backBtn.setTitle(leftName, for: .normal)
        backBtn.setImage(UIImage(named: leftImage), for: .normal)
        bindings = ["backBtn": backBtn]
        //        horiStr = "H:|-0-[backBtn(44)]|"
        virStr = "V:[backBtn(\(btnHeight))]-0-|"
        
        //        viewConstrains.append(contentsOf: NSLayoutConstraint.constraints(withVisualFormat: horiStr, options: .directionLeadingToTrailing, metrics: nil, views: bindings))
        viewConstrains.append(contentsOf: NSLayoutConstraint.constraints(withVisualFormat: virStr, options: .directionLeadingToTrailing, metrics: nil, views: bindings))
        
        
        // right btn
        rightBtn.backgroundColor = UIColor.clear
        rightBtn.translatesAutoresizingMaskIntoConstraints = false
        //        rightBtn.setTitleColor(UIColor.white, for: .normal)
        rightBtn.setTitleColor(UIColor.white, for: .normal)
        rightBtn.setTitle(rightName, for: .normal)
        rightBtn.setImage(UIImage(named: rightImage), for: .normal)
        navView.addSubview(rightBtn)
        bindings = ["rightBtn": rightBtn]
        virStr = "V:[rightBtn(\(btnHeight))]-0-|"
        viewConstrains.append(contentsOf: NSLayoutConstraint.constraints(withVisualFormat: virStr, options: .directionLeadingToTrailing, metrics: nil, views: bindings))
        
        // title
        if let titleColor = titleColor {
            titleLab.textColor = titleColor
        }else {
            titleLab.textColor = #colorLiteral(red: 0.3176088035, green: 0.3176690638, blue: 0.3176049888, alpha: 1)
        }
        titleLab.font = UIFont.systemFont(ofSize: 18.0)
        titleLab.translatesAutoresizingMaskIntoConstraints = false
        titleLab.textAlignment = .center
        navView.addSubview(titleLab)
        titleLab.backgroundColor = UIColor.clear
        bindings = ["titleLab": titleLab, "backBtn": backBtn, "rightBtn": rightBtn]
        horiStr = "H:|-0-[backBtn(\(btnWidth))]-0-[titleLab]-0-[rightBtn(\(btnWidth))]-0-|"
        virStr = "V:[titleLab(\(btnHeight))]-0-|"
        
        
        viewConstrains.append(contentsOf: NSLayoutConstraint.constraints(withVisualFormat: horiStr, options: .directionLeadingToTrailing, metrics: nil, views: bindings))
        viewConstrains.append(contentsOf: NSLayoutConstraint.constraints(withVisualFormat: virStr, options: .directionLeadingToTrailing, metrics: nil, views: bindings))
        
        navView.addConstraints(viewConstrains)
        view.bringSubview(toFront: navView)
        
        //        self.view.layoutIfNeeded()
        
        if isShadow == true {
            navView.layer.shadowOffset = CGSize(width: 0, height: 1)
            navView.layer.shadowColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            navView.layer.shadowOpacity = 0.5
        }
    }

}

extension KJLNavView {
    @objc func backAction() {
        if self.navigationController == nil || self.navigationController?.viewControllers.count == 1{
            self.dismiss(animated: true, completion: nil)
        }else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func backpop() {
        self.navigationController?.popToRootViewController(animated: false)
    }
}
