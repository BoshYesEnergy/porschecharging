//
//  BaseViewController.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/19.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit
import SnapKit

enum BackAction {
    case NONE
    case POP
    case DISMISS
}

typealias HeaderToolCompletion = (() -> Void)?

class BaseViewController: UIViewController {
    
    let headerView: UIView = {
        let view = UIView() 
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let garyLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.7960784314, blue: 0.8, alpha: 1)
        return view
    }()
    
    let backBtn: UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let headerTitle: UILabel = {
        let lab = UILabel()
        lab.translatesAutoresizingMaskIntoConstraints = false
        lab.font = UIFont(name: "PorscheNext-SemiBold", size: 16)
        lab.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        return lab
    }()
    
    let toolBtn: UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    fileprivate var action: (() -> Void)?
    
    override func loadView() {
        super.loadView()
        setTopView()
//        print("language: \(KJLRequest.shareInstance().language ?? "")")
//        print("PHONE: \(UIDevice.current.type)")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
}


//MARK: Set view
extension BaseViewController {
    func setTopView() {
        view.addSubview(headerView)
        view.addSubview(garyLine)
        headerView.snp.makeConstraints { (make) in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.leading.trailing.equalTo(0)
            make.height.equalTo(64)
        }
        
        garyLine.snp.makeConstraints { (make) in
            make.top.equalTo(headerView.snp.bottom)
            make.leading.trailing.equalTo(0)
            make.height.equalTo(0.7)
        }
    }
    
    func hiddenHeaderView() {
        headerView.isHidden = true
        garyLine.isHidden   = true
    }
    
    func hiddenNavigationBar(isHidden: Bool) {
        navigationController?.navigationBar.isHidden = isHidden
    }
}


//MARK: Custom View
extension BaseViewController {
    func setHeaderView(_ title: String, action: BackAction) {
        headerView.addSubview(backBtn)
        backBtn.snp.makeConstraints { (make) in
            make.centerY.equalTo(headerView)
            make.leading.equalTo(25)
            make.height.equalTo(48) 
        }
        backActionStyle(action: action)
        backBtn.setImage(UIImage(named: "icon_previous_default"), for: .normal)
        backBtn.imageEdgeInsets.left = -20
        backBtn.setTitle(title, for: .normal)
        backBtn.setTitleColor(.black, for: .normal)
        backBtn.titleLabel?.font = UIFont(name: "PorscheNext-SemiBold", size: 16)
    }
    
    func setCustomHeaderView(_ title: String, action: HeaderToolCompletion = nil) {
        headerView.addSubview(backBtn)
        backBtn.snp.makeConstraints { (make) in
            make.centerY.equalTo(headerView)
            make.leading.equalTo(25)
            make.height.equalTo(48)
        }
        self.action = action
        backBtn.addTarget(self, action: #selector(tapTool), for: .touchUpInside)
        backBtn.setImage(UIImage(named: "icon_previous_default"), for: .normal)
        backBtn.imageEdgeInsets.left = -20
        backBtn.setTitle(title, for: .normal)
        backBtn.setTitleColor(.black, for: .normal)
        backBtn.titleLabel?.font = UIFont(name: "PorscheNext-SemiBold", size: 16)
    }
    
    func setToolButton(_ title: String?, _ image: UIImage?, _ BtnHighlighted: UIImage?, action: HeaderToolCompletion = nil) {
        headerView.addSubview(toolBtn)
        toolBtn.snp.makeConstraints { (make) in
            make.trailing.equalTo(-25)
            make.height.width.equalTo(48)
            make.centerY.equalTo(backBtn)
        } 
        toolBtn.setTitle(title, for: .normal)
        toolBtn.setImage(image, for: .normal)
        toolBtn.setImage(BtnHighlighted, for: .highlighted)
        
        self.action = action
        toolBtn.addTarget(self, action: #selector(tapTool), for: .touchUpInside)
    }
}


//MARK: Action
extension BaseViewController {
    func backActionStyle(action: BackAction) {
        if action == .DISMISS {
            backBtn.addTarget(self, action: #selector(dismissVc), for: .touchUpInside)
        } else {
            backBtn.addTarget(self, action: #selector(backToVc), for: .touchUpInside)
        }
    }
    
    @objc func backToVc() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func dismissVc() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func tapTool() {
        self.action?()
    }
}
