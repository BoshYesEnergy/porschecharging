//
//  RePayCreditTableViewCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/5/21.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class RePayCreditTableViewCell: UITableViewCell {
    
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var cardName: UILabel!
    @IBOutlet weak var cardNumber: UILabel!
    
    var setData: CardListDataModel.Data? {
        didSet {
            if let name = setData?.cardName {
                cardName.text = name
            }
            
            if let num = setData?.cardNum {
                let end = num.index(num.endIndex, offsetBy: 0)
                let start = num.index(num.endIndex, offsetBy: -4)
                cardNumber.text = "****-" + String(num[start..<end])
            }
            
            if let pay = setData?.paymentType {
                if pay.lowercased() == "linepay" {
                    cardNumber.text = ""
                } 
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib() 
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected == true {
            iconImage.image = #imageLiteral(resourceName: "ic_mail_check_box_select")
        } else {
            iconImage.image = #imageLiteral(resourceName: "ic_mail_check_box_not_select")
        }
    } 
}
