//
//  RePayCreditCardViewController.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/5/21.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class RePayCreditCardViewController: UIViewController {

    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var cornerRadiusView: UIView!
    @IBOutlet weak var topTitle: UILabel!
    @IBOutlet weak var transNubmer: UILabel!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var topBtn: UIButton!
    @IBOutlet weak var confirmBtn: UIButton! 
    @IBOutlet weak var table: UITableView!
    
    var tranNo: Int64?
    var qrNo = ""
    var lists: [CardListDataModel.Data] = []
    private var cardNo = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        settingView()
        setTable()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(callCardList), name: .rePayCreditCard, object: nil)
        callCardList()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
}


//MARK:- Setting View
extension RePayCreditCardViewController {
    private func settingView() {
        bottomView.layer.cornerRadius = 10
        cornerRadiusView.layer.cornerRadius = 10 
        transNubmer.text = "交易單號： \(qrNo)"
        topBtn.layer.cornerRadius = 10
        confirmBtn.layer.cornerRadius = 10
    }
    
    private func setTable() {
        table.delegate = self
        table.dataSource = self
        table.register(UINib(nibName: "RePayCreditTableViewCell", bundle: nil), forCellReuseIdentifier: "RePayCreditTableViewCell")
        table.separatorStyle = .none
    }
}


//MARK:- Table Delegate
extension RePayCreditCardViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RePayCreditTableViewCell") as! RePayCreditTableViewCell
        cell.selectionStyle = .none
        cell.setData = lists[indexPath.row]
        if lists[indexPath.row].default == true {
            cell.setSelected(true, animated: true)
        } 
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let card = lists[indexPath.row].cardNo else { return }
        defaultCheckboxStatus(myData: lists[indexPath.row])
        cardNo = card 
    }
}


//MARK:- Action
extension RePayCreditCardViewController {
    @IBAction func gotoAction(_ sender: UIButton) { 
    }
    
    @IBAction func confirmAction(_ sender: UIButton) {
        guard let tranNo = tranNo,
            cardNo != 0
            else { return }
        KJLRequest.requestForpRepay(tranNo: Int(tranNo), cardNo: cardNo) { [weak self] (response) in
            guard response != nil else {
                ShowMessageView.showMessage(title: "提示", message: "付款失敗，請稍後再試。")
                return
            }
            NotificationCenter.default.post(name: .repayLoadView, object: nil)
            NotificationCenter.default.post(name: .reloadCarCompleteCharging, object: nil)
            self?.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func dismissAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}


//MARK:- API
extension RePayCreditCardViewController {
    @objc private func callCardList() {
        KJLRequest.requestForCardListData { [weak self] (response) in
            if let response = response as? CardListDataModel,
                let data = response.datas {
                self?.lists = []
                for list in data {
                    if list.disable == false && list.expireSoon == false {
                        self?.lists.append(list)
                    }
                }
            } else {
                self?.lists = []
            }
            self?.table.reloadData()
        }
    }
    
    func defaultCheckboxStatus(myData: CardListDataModel.Data) {
        let cardNo = myData.cardNo == nil ? "" : "\(myData.cardNo!)"
        KJLRequest.requestForEditCard(cardNo: cardNo, cardName: myData.cardName ?? "", defaultStr: .TRUE, completion: { (response) in
        })
    }
}
