//
//  ScanQRcodeViewController.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/22.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit
import AVFoundation
import SnapKit

enum CameraStatus {
    case Partner
    case Profile
    case Scan
    case ScanPartner
    case None
}

class ScanQRcodeViewController: UIViewController {
    
    @IBOutlet weak var dismissBtn: UIButton!
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var cameraViewHeight: NSLayoutConstraint!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var flashBtn: UIButton!
    @IBOutlet weak var idTitle: UILabel!
    
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var toolView: UIView!
    @IBOutlet weak var orLabel: UILabel!
    
    lazy var borderImage: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    var captureSession = AVCaptureSession()
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var deviceDiscoverySession: AVCaptureDevice? = nil
    var qrCodeFrameView: UIView?
    private var scanStirng = ""
    private var count = 0
    
    var content: String = ""
    var status : CameraStatus = .None
    var saveCode = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setView()
        setScan()
        codeTextField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        confirmBtn.setTitle(LocalizeUtils.localized(key: "Confirm"), for: .normal)
        idTitle.text = LocalizeUtils.localized(key: "ScanID")
        codeTextField.placeholder = LocalizeUtils.localized(key: "ScanID")
        orLabel.text = LocalizeUtils.localized(key: "Just_or_String")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("##########Scan viewWillDisappear##########")
    }
    
    fileprivate func setView() {
        
        view.addSubview(borderImage)
        borderImage.image = UIImage(named: "image_scan_outline")
        borderImage.snp.makeConstraints { (make) in
            make.centerX.centerY.equalTo(cameraView)
            make.height.width.equalTo(cameraView.frame.width * 0.55)
        }
        
        borderView.layer.borderWidth = 1
        borderView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        if content != "" {
            contentLabel.text = content
        }
        
        if status == .Scan {
            cameraViewHeight.constant = KJLCommon.isX() == true ? 380 : 280
            borderView.isHidden = false
            confirmBtn.isHidden = false
            toolView.isHidden   = false
        } else if status == .Profile {
            cameraViewHeight.constant = KJLCommon.isX() == true ? 450 : 420
            borderView.isHidden = true
            confirmBtn.isHidden = true
            toolView.isHidden   = true
        } else if status == .Partner || status == .ScanPartner {
            cameraViewHeight.constant = KJLCommon.isX() == true ? 450 : 420
            borderView.isHidden = true
            confirmBtn.isHidden = true
            toolView.isHidden   = true
        }
    }
    
    fileprivate func setScan() {
        let mediaType = AVMediaType.video.rawValue
        let position: AVCaptureDevice.Position? = .back
        // 取得後置鏡頭來擷取影片
        if #available(iOS 10.0, *) {
            if let position = position {
                deviceDiscoverySession = AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: position)
            }else {
                deviceDiscoverySession = AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: .back)
            }
        } else {
            let devices = AVCaptureDevice.devices(for: AVMediaType(rawValue: mediaType))
            let devicePosition = position
            for deviceObj in devices {
                if deviceObj.position == devicePosition {
                    deviceDiscoverySession = deviceObj
                }
            }
            
            if let device = devices.first {
                deviceDiscoverySession = device
            }
        }
        
        guard let captureDevice = deviceDiscoverySession else {
            print("Failed to get the camera device")
            return
        }
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        do {
            let input = try AVCaptureDeviceInput(device: captureDevice)
            captureSession.addInput(input)
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession.addOutput(captureMetadataOutput)
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
            print("cameraView \(cameraView.layer.bounds)")
            videoPreviewLayer?.frame = cameraView.layer.bounds
            cameraView.layer.addSublayer(videoPreviewLayer!)
            captureSession.startRunning()
        } catch {
            print(error)
            return
        }
    }
    
    @IBAction func flashAction(_ sender: UIButton) {
        changeFlashMode()
    }
    
    func changeFlashMode(isForceClose:Bool = false) {
        guard let device = deviceDiscoverySession else { return }
        
        do{
            if (device.hasTorch){
                try device.lockForConfiguration()
                if device.torchMode == .on || isForceClose == true{
                    device.torchMode = .off
                    //                    device.flashMode = .off
                    let photoSettings = AVCapturePhotoSettings()
                    photoSettings.flashMode = .off
                } else {
                    device.torchMode = .on
                    let photoSettings = AVCapturePhotoSettings()
                    photoSettings.flashMode = .on
                    //                    device.flashMode = .on
                }
                device.unlockForConfiguration()
            }
        }catch{
            //DISABEL FLASH BUTTON HERE IF ERROR
            print("Device tourch Flash Error ");
        }
    }
    
    @IBAction func dismiss(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func confirm(_ sender: UIButton) {
        print(codeTextField.text ?? "")
        UserDefaults.standard.setValue(codeTextField.text ?? "", forKey: "chargeQRno")
        callBeginChargin(qrNo: codeTextField.text ?? "")
    }
}


extension ScanQRcodeViewController: AVCaptureMetadataOutputObjectsDelegate {
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if metadataObjects.count == 0 { return }
        
        // 取得元資料（metadata）物件
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if metadataObj.type == AVMetadataObject.ObjectType.qr {
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            guard count == 0 else { return }
            if let metadata = metadataObj.stringValue {
                self.count += 1
                self.scanStirng = metadata
                print(metadata)
                if status == .Scan {
                    if metadata.contains("https://appqr.yes-energy.com.tw/porsche/") == true {
                        let filteredData = metadataObj.stringValue?.components(separatedBy: "https://appqr.yes-energy.com.tw/porsche/")
                        let qrcode = filteredData?[1]
                        print("metadata \(qrcode?.replace(target: ";", withString: "") ?? "")")
                        UserDefaults.standard.setValue(qrcode?.replace(target: ";", withString: ""), forKey: "chargeQRno")
                        callBeginChargin(qrNo: qrcode?.replace(target: ";", withString: "") ?? "")
                    } else {
                        popMessage(content: LocalizeUtils.localized(key: "qrcodeErrorMessage"))
                    }
                } else {
                    /// https://s.yulon-energy.com:8891/d/
                    if metadata.contains("https://appqr.yes-energy.com.tw/porsche/") == true { /// check
                        let filteredData = metadataObj.stringValue?.components(separatedBy: "https://appqr.yes-energy.com.tw/porsche/")
                        let qrcode = filteredData?[1] ?? ""
                        print("metadata \(qrcode.replace(target: ";", withString: ""))")
                        UserDefaults.standard.setValue(qrcode.replace(target: ";", withString: ""), forKey: "chargeQRno")
                        callBeginChargin(qrNo: qrcode.replace(target: ";", withString: ""))
                    } else {
                        print(metadata)
                        callScanQrCode(qrcode: metadata)
                    }
                }
                return
            }
            
        }
    }
} 


//MARK: Api
extension ScanQRcodeViewController {
    fileprivate func callBeginChargin(qrNo: String) {
        let cardNO = KJLRequest.shareInstance().profile?.datas?.cardNo ?? 0
        self.callBeginChargin(qrNo: qrNo, cardNo: "\(cardNO)")
    }
    
    //requestForBeginCharging
    fileprivate func callBeginChargin(qrNo: String, cardNo: String = "") {
        KJLRequest.requestForBeginCharging(qrNo: qrNo, cardNo: cardNo) {[weak self] (response) in
            guard let response = response as? BeginChargingModel else { return }
            if response.resCode != "0000" {
                self?.popErrorMessage(reMsg: response.resMsg ?? "")
            } else {
                let tranNo = response.datas?.tranNo ?? 0
                UserDefaults.standard.set(tranNo, forKey: "chargeTranNo")
                self?.dismiss(animated: true) {
                    NotificationCenter.default.post(name: .gotoCharging, object: nil)
                }
            }
        }
    }
    
    /// scan partner
    fileprivate func callScanQrCode(qrcode: String) {
        if saveCode == 0 {
            saveCode += 1
            print("saveCode: \(saveCode)")
            KJLRequest.requestForScanQrCode(qrcode: qrcode) {[weak self] (response) in
                guard let response = response as? NormalClass else { return }
                if response.resCode != "0000" {
                    self?.popErrorMessage(reMsg: response.resMsg ?? "")
                    
                } else {
                    UserDefaults.standard.set(self?.scanStirng, forKey: "shareQRcode")
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                        self?.dismiss(animated: true) {
                            NotificationCenter.default.post(name: .apiUpdatePartner, object: self, userInfo: nil)
                        }
                    }
                }
            }
        }
    }
}



//MARK: Text field delegate
extension ScanQRcodeViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let count = text.count + string.count - range.length
        if textField == codeTextField {
            return count > 9 ?  false : true
        }
         
        return true
    }
}


//MARK: Message Delegate
extension ScanQRcodeViewController: ShowMessageAlertDelegate {
    func popErrorMessage(reMsg: String) {
        let vc = ShowMessageAlertVC()
        vc.stringToContent = reMsg
        vc.titleToBtn = LocalizeUtils.localized(key: Language.Confirm.rawValue)
        vc.delegate = self
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        present(vc, animated: true) { }
    }
    
    func messageTapAction() {
        self.dismiss(animated: true, completion: nil)
    }
}


extension ScanQRcodeViewController: ShowCheckMessageDelegate {
    func tapConfirm(tagCount: Int) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func tapCancel() {
        self.dismiss(animated: true, completion: nil)
    }
    
    fileprivate func popMessage(content: String) {
        let vc = ShowCheckMessageVC()
        vc.toContent = content
        vc.toTitle   = ""
        vc.custom    = .one
        vc.delegate  = self
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        present(vc, animated: true, completion: nil)
    }
}


