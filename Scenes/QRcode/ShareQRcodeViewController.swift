//
//  ShareQRcodeViewController.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/22.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class ShareQRcodeViewController: UIViewController {
    
    @IBOutlet weak var qrCodeImage: UIImageView!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var downloadBtn: UIButton!
    @IBOutlet weak var shardBtn: UIButton!
    @IBOutlet weak var dismissBtn: UIButton!
    
    private var saveQrcode: UIImage?
    private var apiQRcode = ""
    var content: String = ""
    var timer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        callApiGenerateQrCode()
        contentLabel.text = LocalizeUtils.localized(key: "parnterShareQrcodeConten")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopTimer()
    }
}


//MARK: Setting View
extension ShareQRcodeViewController {
    fileprivate func setView() {
        downloadBtn.setImage(UIImage(named: "icon_download_click"), for: .highlighted)
        shardBtn.setImage(UIImage(named: "icon_share_click"), for: .highlighted)
        dismissBtn.setImage(UIImage(named: "button_close_click"), for: .highlighted)
    }
}


//MARK: API
extension ShareQRcodeViewController {
    fileprivate func callApiGenerateQrCode() {
        KJLRequest.requestForGenerateQrCode {[weak self] (response) in
            guard let data = response as? GenerateQrCodeModel else { return }
            self?.apiQRcode = data.datas?.qrcode ?? ""
            UserDefaults.standard.set(self?.apiQRcode, forKey: "shareQRcode")
            print("###########\nqrcode\(self?.apiQRcode)")
            self?.qrCodeImage.image = self?.createQRcode(string: self?.apiQRcode ?? "")
            self?.startTimer()
        }
    }
    
    @objc fileprivate func callApiCheckPartner() {
        KJLRequest.requestForCheckPartner(qrcode: apiQRcode, subscription: true) { (response) in
            guard let data = response as? CheckPartnerModel else { return } 
            guard data.resCode == "0000" ,
                  let _ = data.datas
                  else { return }
            self.dismiss(animated: true) {
                NotificationCenter.default.post(name: .checkPartnerRequest, object: self, userInfo: nil)
            }
        }
    }
    
    func startTimer() {
        timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(callApiCheckPartner), userInfo: nil, repeats: true)
        timer?.fire()
    }
    
    func stopTimer(){
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
    }
}


//MARK: Create QRcode
extension ShareQRcodeViewController {
    fileprivate func createQRcode(string: String) -> UIImage? {
        let data = string.data(using: .ascii)
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 10, y: 10) // 修正模糊
            if let output = filter.outputImage?.transformed(by: transform) {
                let context = CIContext()
                if let cgImage = context.createCGImage(output, from: output.extent) {
                    saveQrcode = UIImage(cgImage: cgImage)
                }
                return UIImage(ciImage: output)
            }
        }
        return nil
    }
}


//MARK: Action
extension ShareQRcodeViewController {
    @IBAction func download(_ sender: UIButton) {
        if let _ = createQRcode(string: "Bosh"),
           let photo = saveQrcode {
            UIImageWriteToSavedPhotosAlbum(photo, self, #selector(image), nil)
        }
    }
    
    @IBAction func share(_ sender: UIButton) {
        #warning("wait ??? - ???")
    }
    
    @IBAction func dismissVC(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "Your altered image has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
}

