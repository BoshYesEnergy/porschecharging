//
//  ChargingHistoryVC.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/11.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit 

enum WhereToVc {
    case NONE
    case Charging
    case ProfileToHistory
}

enum HistoryTitle: String {
    case Battery  = "Battery Information"
    case Charging = "Charging Time"
    case Payment  = "Payment"
}

class ChargingHistoryVC: BaseViewController {
    
    @IBOutlet weak var historyTable: UITableView!
    
    var status: WhereToVc = .NONE
    var tranNo = 0
    var list: PorscheChargingResultModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTable()
        hiddenNavigationBar(isHidden: true)
        if status == .Charging {
            setHeaderView(LocalizeUtils.localized(key: "ChargingHistory"), action: .DISMISS)
        } else {
            setHeaderView(LocalizeUtils.localized(key: "ChargingHistory"), action: .POP)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        callChargingResult("\(tranNo)")
    }
    
    func callChargingResult(_ tranNo: String) {
        KJLRequest.requestForChargingResult(tranNo) { (response) in
            guard let response = response as? PorscheChargingResultModel else { return }
            self.list = response
            self.historyTable.reloadData()
        } 
    }
}


//MARK: Set UI
extension ChargingHistoryVC {
    fileprivate func setTable() {
        historyTable.register(UINib(nibName: "StationHistoryCell", bundle: nil), forCellReuseIdentifier: "StationHistoryCell")
        historyTable.register(UINib(nibName: "ChargingHistoryDetailCell", bundle: nil), forCellReuseIdentifier: "ChargingHistoryDetailCell")
        historyTable.register(UINib(nibName: "TapOneButtonCell", bundle: nil), forCellReuseIdentifier: "TapOneButtonCell")
        
    }
}


//MARK: Table view delegate & data source
extension ChargingHistoryVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if status == .ProfileToHistory {
            return 3
        } else {
            return 3
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 4 { /// hidden ... why!!!
            if let tapCell = tableView.dequeueReusableCell(withIdentifier: "TapOneButtonCell", for: indexPath) as? TapOneButtonCell {
                tapCell.delegate = self
                tapCell.setView(custom: false)
                tapCell.tapBtn.setTitle(LocalizeUtils.localized(key: Language.Confirm.rawValue), for: .normal)
                return tapCell
            } else {
                return UITableViewCell()
            }
        } else if indexPath.row == 0 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "StationHistoryCell", for: indexPath) as? StationHistoryCell {
                cell.toText = list?.datas?.stationName
                return cell
            } else {
                return UITableViewCell()
            }
        } else if indexPath.row == 1 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "ChargingHistoryDetailCell", for: indexPath) as? ChargingHistoryDetailCell {
                cell.titleLabel.text = LocalizeUtils.localized(key: "HistoryBattery")
                cell.titleOne.text = LocalizeUtils.localized(key: "HistoryStart")
                if list?.datas?.batteryInfo?.start ?? "00:00" == "" {
                    cell.valueOne.text = "00:00"
                } else {
                    cell.valueOne.text = list?.datas?.batteryInfo?.start ?? "00:00"
                }
                
                cell.titleTwo.text = LocalizeUtils.localized(key: "HistoryStop")
                if list?.datas?.batteryInfo?.end ?? "00:00" == "" {
                    cell.valueTwo.text = "00:00"
                } else {
                    cell.valueTwo.text = list?.datas?.batteryInfo?.end ?? "00:00"
                }
                
                cell.titleThree.text = LocalizeUtils.localized(key: "ChargingProcessPower")
                cell.valueThree.text = list?.datas?.batteryInfo?.kwh ?? ""
                if list?.datas?.batteryInfo?.kwh ?? "" == "" {
                    cell.titleThree.text = ""
                }
                
                cell.titleFour.text = LocalizeUtils.localized(key: "HistoryChargingNote")
                cell.valueFour.text = list?.datas?.batteryInfo?.status ?? ""
                return cell
            } else {
                return UITableViewCell()
            }
        } else if indexPath.row == 2 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "ChargingHistoryDetailCell", for: indexPath) as? ChargingHistoryDetailCell {
                cell.titleLabel.text = LocalizeUtils.localized(key: "HistoryChargingTime")
                cell.titleOne.text = LocalizeUtils.localized(key: "HistoryStart")
                if list?.datas?.chargingInfo?.startTime ?? "00:00" == "" {
                    cell.valueOne.text = "00:00"
                } else {
                    cell.valueOne.text = list?.datas?.chargingInfo?.startTime ?? "00:00"
                }
                
                cell.titleTwo.text = LocalizeUtils.localized(key: "HistoryStop")
                if list?.datas?.chargingInfo?.endTime ?? "00:00" == "" {
                    cell.valueTwo.text = "00:00"
                } else {
                    cell.valueTwo.text = list?.datas?.chargingInfo?.endTime ?? "00:00"
                }
                
                cell.titleThree.text = LocalizeUtils.localized(key: "totalTime")
                cell.valueThree.text = list?.datas?.chargingInfo?.total ?? ""
                
                cell.titleFour.text = ""
                cell.valueFour.text = ""
                return cell
            } else {
                return UITableViewCell()
            }
        } else if indexPath.row == 3 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "ChargingHistoryDetailCell", for: indexPath) as? ChargingHistoryDetailCell {
                
                cell.valueOne.text = list?.datas?.paymentStatusInfo?.status ?? ""
                cell.valueTwo.text = list?.datas?.paymentStatusInfo?.method ?? ""
                cell.valueThree.text = list?.datas?.paymentStatusInfo?.transactionId ?? ""
                cell.titleLabel.text = HistoryTitle.Payment.rawValue
                cell.titleOne.text = LocalizeUtils.localized(key: "Status")
                cell.titleTwo.text = "Method"
                cell.titleThree.text = "Transaction ID"

//                cell.titleTwo.text = LocalizeUtils.localized(key: "HistoryStop")
//                cell.titleThree.text = LocalizeUtils.localized(key: "HistoryChargingNote")
                cell.titleFour.text = ""
                cell.valueFour.text = ""
                return cell
            } else {
                return UITableViewCell()
            }
        } else {
                return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}


//MARK: Tap cell protocol
extension ChargingHistoryVC: TapOneButtonProtocol {
    func tapAction(tagNumber: Int) {
        if status == .Charging {
            self.dismiss(animated: true, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
}
