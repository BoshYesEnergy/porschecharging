//
//  StationHistoryCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/11.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class StationHistoryCell: UITableViewCell {

    @IBOutlet weak var stationTitle: UILabel!
    
    var toText: String? {
        didSet {
            guard let text = toText else { return }
            stationTitle.text = text
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
 
    }
    
}
