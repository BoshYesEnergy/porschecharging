//
//  ChargingHistoryDetailCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/11.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class ChargingHistoryDetailCell: UITableViewCell {

    @IBOutlet weak var borderView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleOne: UILabel!
    @IBOutlet weak var titleTwo: UILabel!
    @IBOutlet weak var titleThree: UILabel!
    @IBOutlet weak var titleFour: UILabel!
    
    @IBOutlet weak var valueOne: UILabel!
    @IBOutlet weak var valueTwo: UILabel!
    @IBOutlet weak var valueThree: UILabel! 
    @IBOutlet weak var valueFour: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        setUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setUI() {
        borderView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        borderView.layer.borderWidth = 1
    }
}
