//
//  LoadingViewController.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/6.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class LoadingViewController: UIViewController {
    
    @IBOutlet weak var logoImage: UIImageView! 
    @IBOutlet weak var logoCenterY: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.async {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainTabBarController")
            vc.modalTransitionStyle   = .crossDissolve
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: true, completion: nil)
            
        } 
    }
}
