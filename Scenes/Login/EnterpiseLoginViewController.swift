//
//  EnterpiseLoginViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

//MARK: life cycle
class EnterpiseLoginViewController: KJLNavView {
    
    @IBOutlet weak var accountTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        accountTextField.becomeFirstResponder()
        accountTextField.delegate = self
        passwordTextField.delegate = self 
        createNavViewForRightBtn(rightName: "", rightImage: "", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: UIColor.black)
        titleLab?.text = "登入"
    }
    
    @IBAction func loginAction(_ sender: Any) {
        guard self.passwordTextField.text!.count >= 0 else {
            ShowMessageView.showMessage(title: nil, message: "請輸入密碼")
            return
        }
        let password = passwordTextField.text!.sha256
        let passWordWeb = passwordTextField.text!.sha512
        KJLRequest.requestForEnterpiseLogin(account: accountTextField.text!, passWord: password, passWordWeb: passWordWeb) { (response) in
            guard let response = response as? AppLoginClass else {
                return
            }
            let authToken = response.datas?.authToken ?? ""
            if authToken.count > 0 {
                KJLRequest.saveToekn(token: authToken)
                // wait
                //                self.callProfile(type: type)
                UIViewController.updateFCMToken(appLoginClass: response)
                let isChangePassword = response.datas?.showChangePass ?? false
                KJLRequest.requestForProfile { (response) in
                    guard let response = response as? ProfileClass else {
                        return
                    }
                    
                    //                    KJLRequest.shareInstance().name = response.datas?.nickName ?? ""
                    //                    KJLRequest.shareInstance().picUrl = response.datas?.picUrl ?? ""
                    KJLRequest.shareInstance().profile = response
                    
                    if isChangePassword == true {
                        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                         
                        
                        
                    }else {
                        UserDefaults.standard.set("EnterpiseLogin", forKey: "EnterpiseLogin")
                        UserDefaults.standard.synchronize()
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                }
            } else {
                ShowMessageView.showMessage(title: nil, message: "No AuthToken")
            }
        } 
    }
    
    @IBAction func forgetButtonAction(_ sender: Any) {
        self.navigationController?.show(EnterpiseForgetPasswordViewController(), sender: self)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}


//MARK:- Text Field Delegate
extension EnterpiseLoginViewController: UITextFieldDelegate {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
