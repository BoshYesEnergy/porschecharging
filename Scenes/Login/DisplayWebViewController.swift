//
//  DisplayWebViewController.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/13.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit
import WebKit

enum WebURL: String {
    case privatepolicy = "https://www.porsche.com/taiwan/en/aboutporsche/e-performance/privacypolicy/"
    case privatepolicyZh = "https://www.porsche.com/taiwan/zh-tw/aboutporsche/e-performance/privacypolicy/"
    case termsandconditions  = "https://www.porsche.com/taiwan/en/aboutporsche/e-performance/termsandconditions/"
    case termsandconditionsZh = "https://www.porsche.com/taiwan/zh-tw/aboutporsche/e-performance/termsandconditions/"
    
    /// Porsche login
    case PorscheLogin = "https://login.porsche.com/as/authorization.oauth2?response_type=code&client_id=aLYKUmMspvPnUx2g0AglwMoMGWtpWrsk&redirect_uri=PorscheApp%3A%2F%2F%2F&country=tw&locale=zh_TW"
    case PorscheLogin_en = "https://login.porsche.com/as/authorization.oauth2?response_type=code&client_id=aLYKUmMspvPnUx2g0AglwMoMGWtpWrsk&redirect_uri=PorscheApp%3A%2F%2F%2F&country=tw&locale=en_US"
    /// FQA
    case FAQ   = "https://www.porsche.com/taiwan/zh-tw/aboutporsche/e-performance/faq/"
    case FAQen = "https://www.porsche.com/taiwan/en/aboutporsche/e-performance/faq/"
//    case ChineseTermsandconditions = "https://www.porsche.com/taiwan/zh-tw/aboutporsche/e-performance/termsandconditions/"
//    case ChinesePrivatepolicy = "https://www.porsche.com/taiwan/zh-tw/aboutporsche/e-performance/privacypolicy/"
}

class DisplayWebViewController: BaseViewController, WKNavigationDelegate {

    @IBOutlet weak var webview: WKWebView!
    
    var status: WebURL = .privatepolicy
    var url: URL?
    
    var titleName = LocalizeUtils.localized(key: "Login")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        /// clear
        WebCacheCleaner.clean()
        
        setHeaderView(titleName, action: .DISMISS)
        setWeb()
        let handler = ScriptMessageHandler()
        handler.delegate = self
        webview.configuration.userContentController.add(handler, name: "navigationTask")
        webview.navigationDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("viewDidDisappear DisplayWebViewController")
    }
    
    func webView(_ webView: WKWebView,
                 decidePolicyFor navigationAction: WKNavigationAction,
                 decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        print("qefwefwef目前url =\(navigationAction.request.url!.absoluteString)")
        
        let urlString = navigationAction.request.url!.absoluteString
        if urlString.contains("porscheapp") {
            let stringArray = urlString.components(separatedBy: "code=")
            UserDefaults.standard.set(stringArray[1], forKey: "porscheappIDcode")
            self.dismiss(animated: true) {
                NotificationCenter.default.post(name: .porscheLogin, object: nil)
            } 
        }
        
        if let url = navigationAction.request.url,
            url.absoluteString.contains("code=")
             {
            //跳出去做登入動作
            print(" =\(navigationAction.request.url!.absoluteString)")
            decisionHandler(.cancel)
        }
        else {
            // allow the request
            decisionHandler(.allow)
        }
    } 
    
    private func setWeb() {
        switch status {
        case .privatepolicy:
            url = URL(string: WebURL.privatepolicy.rawValue)
        case .privatepolicyZh:
            url = URL(string: WebURL.privatepolicyZh.rawValue)
        case .termsandconditions:
            url = URL(string: WebURL.termsandconditions.rawValue)
        case .termsandconditionsZh:
            url = URL(string: WebURL.termsandconditionsZh.rawValue)
        case .FAQ:
            url = URL(string: WebURL.FAQ.rawValue)
        case .FAQen:
            url = URL(string: WebURL.FAQen.rawValue)
        case .PorscheLogin:
            url = URL(string: WebURL.PorscheLogin.rawValue)
        case .PorscheLogin_en:
            url = URL(string: WebURL.PorscheLogin_en.rawValue)
        }
        
        let request = URLRequest(url: url!)
        webview.sizeToFit()
        webview.load(request)
    }
}



extension DisplayWebViewController: WKScriptMessageHandler {
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        print(message.name)
        print(message.body)
        if let message = message.body as? [String : Any] {
            print(message)
        }
    }
}


class ScriptMessageHandler: NSObject {
    // 設置代理，把抓到的JS事件轉發給代理處理
    weak var delegate: WKScriptMessageHandler?
}


extension ScriptMessageHandler: WKScriptMessageHandler {
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if let target = delegate {
            target.userContentController(userContentController, didReceive: message)
        }
    }
}


final class WebCacheCleaner {
    class func clean() {
        HTTPCookieStorage.shared.removeCookies(since: Date.distantPast)
        print("[WebCacheCleaner] All cookies deleted")
        
        WKWebsiteDataStore.default().fetchDataRecords(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes()) { records in
            records.forEach { record in
                WKWebsiteDataStore.default().removeData(ofTypes: record.dataTypes, for: [record], completionHandler: {})
                print("[WebCacheCleaner] Record \(record) deleted")
            }
        }
    }
    
}
