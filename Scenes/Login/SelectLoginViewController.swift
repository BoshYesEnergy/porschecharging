//
//  SelectLoginViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKLoginKit
import FacebookCore
import AuthenticationServices

class SelectLoginViewController: UIViewController {
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var fbBtn: UIButton!
    @IBOutlet weak var googleBtn: UIButton!
    @IBOutlet weak var appleBtn: UIButton! 
    @IBOutlet weak var laterBtn: UIButton!
    @IBOutlet weak var porscheIDloginBtn: UIButton!
    @IBOutlet weak var contentLabel: UILabel!
    
    var carGestureCount = 0
    var logoGestureCount = 0
    var regCode: String = ""
    var isTransPush = Bool ()
    var pictureURL = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        settingView()
        setUI()
        NotificationCenter.default.addObserver(self, selector: #selector(goPrivacyPolicy), name: .porscheLogin, object: nil)
        fbBtn.isHidden     = true
        googleBtn.isHidden = true
        appleBtn.isHidden  = true
        
        URLCache.shared.removeAllCachedResponses() 
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setUI() {
        laterBtn.setTitle(LocalizeUtils.localized(key: Language.Loginlater.rawValue), for: .normal)
        porscheIDloginBtn.setTitle(LocalizeUtils.localized(key: Language.PorscheID.rawValue), for: .normal)
        contentLabel.text = LocalizeUtils.localized(key: "loginContent")
    }
}


//MARK:- Setting View
extension SelectLoginViewController {
    private func settingView() {
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = self
        /// check ios version from the apple sign in
        if #available(iOS 13.0, *) {
            appleBtn.isHidden = false
        } else {
            appleBtn.isHidden = true
        }
    }
}


// MARK: - IBAction
extension SelectLoginViewController {
    @objc func goPrivacyPolicy() {
        var language = KJLRequest.shareInstance().language ?? ""
        let code = UserDefaults.standard.string(forKey: "porscheappIDcode") ?? ""
        if language == "zh-Hant" {
            language = "zh"
        } 
        KJLRequest.requestForPorscheLogin(code, language) { (response) in
            guard let response = response as? PorscheLoginModel else { return }
            let authToken = response.datas?.authToken ?? ""
            
            KJLRequest.saveToekn(token: authToken)
            
            let deviceId = response.datas?.deviceId ?? ""
            
            guard let memberNo = response.datas?.memberNo else { return }
            
            UIViewController.callApiUpdateFCMToken(deviceId: deviceId, memberNo: memberNo)
            UserDefaults.standard.set("Porsche", forKey: "loginMethod")
            DispatchQueue.main.async {
                self.callProfile(type: .none , completion: {
                    if response.datas?.checkTerms == false {
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PrivacyPolicyViewController" ) as! PrivacyPolicyViewController
                        self.navigationController?.pushViewController(vc, animated: true)
                    } else {
                        self.dismiss(animated: false) { }
                    }
                })
            }
        }
    }
    
    @IBAction func porscheIDlogin(_ sender: UIButton) {
//        if UserDefaults.standard.string(forKey: "UserLanguage") == "en" {
//            UIApplication.shared.open(URL(string: "https://login.porsche.com/as/authorization.https://login.porsche.com/as/authorization.oauth2?response_type=code&client_id=aLYKUmMspvPnUx2g0AglwMoMGWtpWrsk&redirect_uri=PorscheApp%3A%2F%2F%2F&country=tw&locale=en_US")!, options: [:], completionHandler: nil)
//        } else {
//            UIApplication.shared.open(URL(string: "https://login.porsche.com/as/authorization.oauth2?response_type=code&client_id=aLYKUmMspvPnUx2g0AglwMoMGWtpWrsk&redirect_uri=PorscheApp%3A%2F%2F%2F&country=tw&locale=zh_TW")!, options: [:], completionHandler: nil)
//        } 
        
        let vc = DisplayWebViewController(nibName: "DisplayWebViewController", bundle: nil)
        if UserDefaults.standard.string(forKey: "UserLanguage") == "en" {
            vc.status = .PorscheLogin_en
        } else {
            vc.status = .PorscheLogin
        }
        vc.modalPresentationStyle = .overFullScreen
        present(vc, animated: true, completion: nil)
    }
    
    // fb
    @IBAction func fbLoginAction(_ sender: UIButton) {
        if AccessToken.current?.tokenString == nil {
            let loginManager = LoginManager()
            loginManager.logIn(permissions: ["public_profile", "email"], from: self, handler: { (loginResult, error) in
                if error != nil {
                    printLog(error.debugDescription)
                }else{
                    printLog(loginResult!)
                    // 個資
                    let graphRequest : GraphRequest = GraphRequest(graphPath: "me", parameters: ["fields":"id,email,name,picture"])
                    graphRequest.start(completionHandler: { (connection, result, error) in
                        if error != nil {
                            printLog(error.debugDescription)
                        } else {
                            printLog("fetched user: \(String(describing: result))")
                            print("resultresultresult = \(result)")
                            if let result = result as? [String : Any] {
                                let userID = result["id"] as? String ?? ""
                                let mail = result["email"] as? String ?? ""
                                let name = result["name"] as? String ?? ""
                                if let photoData = result["picture"] as? [String : Any] {
                                    let data = photoData["data"] as! [String : Any]
                                    self.pictureURL = data["url"] as! String
                                }
                                let token = AccessToken.current?.tokenString ?? ""
                                self.regCode = token
                                self.callLoginApi(mail: mail, token: token, userID: userID, name: name, type: .fb)
                            } else {
                                self.regCode = ""
                                self.callLoginApi(mail: "", token: "", type: .fb)
                            }
                        }
                    })
                }
            })
        }else{
            // 個資
            let graphRequest : GraphRequest = GraphRequest(graphPath: "me", parameters: ["fields":"id,email,name,picture.width(480).height(480)"])
            graphRequest.start(completionHandler: { (connection, result, error) in
                if error != nil {
                    printLog(error.debugDescription)
                } else {
                    printLog("fetched user: \(String(describing: result))")
                    if let result = result as? [String : Any] {
                        let userID = result["id"] as? String ?? ""
                        let mail   = result["email"] as? String ?? ""
                        let name   = result["name"] as? String ?? ""
                        if let photoData = result["picture"] as? [String : Any] {
                            let data = photoData["data"] as! [String : Any]
                            self.pictureURL = data["url"] as! String 
                        }
                        let token = AccessToken.current?.tokenString ?? ""
                        self.regCode = token
                        self.callLoginApi(mail: mail, token: token, userID: userID, name: name, type: .fb)
                    } else {
                        self.regCode = ""
                        self.callLoginApi(mail: "", token: "", type: .fb)
                    }
                }
            })
        }
    }
    // Later
    @IBAction func laterAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // Apple Sign In
    @IBAction func appleSignIn(_ sender: UIButton) {
        tapAppleSignIn()
    }
    
    // google
    @IBAction func googleLoginAction(_ sender: UIButton) {
        GIDSignIn.sharedInstance().signOut()
        GIDSignIn.sharedInstance().signIn()
    }
    
    // 呼叫 登入api
    func callLoginApi(mail: String, token: String, userID: String = "", name: String = "", type: KJLRequest.RequestLoginType) {
        guard token.count > 0 else {
            ShowMessageView.showMessage(title: nil, message: "No Token")
            return
        }
        
        switch type {
        case .fb:// [103] Facebook登入
            KJLRequest.requestForFbLogin(fbToken: token, fbId: userID, email: mail, name: name, completion: { (response) in
                guard let response = response as? NormalClass else { return }
                let authToken = response.datas?.authToken ?? ""
                KJLRequest.saveToekn(token: authToken)
                UIViewController.updateFCMToken(normalClass: response)
                UserDefaults.standard.set("Facebook", forKey: "loginMethod")
                DispatchQueue.main.async {
                    self.callProfile(type: type, completion: {
                        self.goPrivacyPolicy()
//                        self.dismiss(animated: true) {
//                            NotificationCenter.default.post(name: .mainReloadMarkerList, object: nil)
//                        }
                    })
                } 
            })
        case .google:// [104] Google登入
            print(mail)
            KJLRequest.requestForGoogleLogin(gToken: token, gmail: mail, completion: { (response) in
                guard let response = response as? NormalClass else { return }
                let authToken = response.datas?.authToken ?? ""
                KJLRequest.saveToekn(token: authToken)
                UIViewController.updateFCMToken(normalClass: response)
                UserDefaults.standard.set("Google", forKey: "loginMethod")
                DispatchQueue.main.async {
                    self.callProfile(type: type, completion: {
                        self.goPrivacyPolicy()
//                        self.dismiss(animated: true) {
//                            NotificationCenter.default.post(name: .mainReloadMarkerList, object: nil)
//                        }
                    })
                }
            })
        case .apple:
            let email = mail == "" ? nil : mail 
            KJLRequest.requestForAppleLogin(appleID: userID, appleNickname: name, appleEmail: email) { (response) in
                guard let response = response as? NormalClass else { return }
                let authToken = response.datas?.authToken ?? ""
                KJLRequest.saveToekn(token: authToken)
                UIViewController.updateFCMToken(normalClass: response)
                UserDefaults.standard.set("Apple", forKey: "loginMethod")
                DispatchQueue.main.async {
                    self.callProfile(type: type) {
                        self.goPrivacyPolicy()
//                        self.dismiss(animated: true) {
//                            NotificationCenter.default.post(name: .mainReloadMarkerList, object: nil)
//                        }
                    }
                }
            }
        case .app:      break
        case .forgot:   break
        case .business: break
        case .none:     break
        }
    }
    
    // 登入成功 呼叫個資api
    func callProfile(type: KJLRequest.RequestLoginType, completion: (() -> Void)?) {
        // [201] 查詢基本資料
        KJLRequest.requestForProfile { (response) in
            guard let response = response as? ProfileClass else { return }
            if type != .apple {
                if response.datas?.picUrl == "" {
                    self.uploadMemberPicture()
                }
            }
            KJLRequest.shareInstance().profile = response
            if let completion = completion {
                completion()
            }
        }
    }
    
    fileprivate func uploadMemberPicture() {
        if self.pictureURL != "" {
            let url = URL(string: self.pictureURL)!
            URLSession.shared.dataTask(with: url) { (data, respone, error) in
                if data != nil {
                    if let data = data, let image = UIImage(data: data) {
                        let str = KJLCommon.convertImageToBase64(image: image)
                        KJLRequest.requestForUploadMemberPic(picBytes: str) { (response) in
                            guard response != nil else { return }
                        }
                    }
                } else {
                    print(error)
                }
            }.resume()
        }
    }
} 

extension SelectLoginViewController: GIDSignInDelegate/*, GIDSignInUIDelegate*/ {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
        } else {
            let token = user.authentication.idToken ?? ""
            let email = user.profile.email ?? ""
            if let imgUrl = user.profile.imageURL(withDimension: 480) {
                pictureURL = imgUrl.absoluteString
            }
            regCode = email
            callLoginApi(mail: email, token: token, type: .google)
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) { }
}


//MARK:- Apple sign in
extension SelectLoginViewController: ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding {
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return view.window!
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential {
        case let credentials as ASAuthorizationAppleIDCredential:
            let user = credentials.user
            let given = credentials.fullName?.givenName ?? ""
            let family = credentials.fullName?.familyName ?? ""
            let email = credentials.email ?? ""
            var token = ""
            if let tokenToSting = credentials.identityToken {
                token = String(data: tokenToSting, encoding: .ascii) ?? ""
                print("token:\n \(String(data: tokenToSting, encoding: .ascii) ?? "")")
            }
            // token need change to nsdata
            print("user: \(user)")
            print("givenName: \(given)")
            print("familyName: \(family)")
            print("email: \(email)")
            /// call api
            callLoginApi(mail: email, token: token, userID: user, name: given, type: .apple)
        /// present segue
        
        default: break
        }
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print("apple sign in error")
    }
    
    func tapAppleSignIn() {
        if #available(iOS 13.0, *) {
            let provider = ASAuthorizationAppleIDProvider()
            let request = provider.createRequest()
            request.requestedScopes = [.fullName, .email]
            let controller = ASAuthorizationController(authorizationRequests: [request])
            controller.delegate = self
            controller.presentationContextProvider = self
            controller.performRequests()
        }
    }
}
