//
//  EnterpiseForgetPasswordViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class EnterpiseForgetPasswordViewController: KJLNavView {
    @IBOutlet weak var accountTextField: UITextField!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        titleLab?.text = "忘記密碼"
        createNavViewForRightBtn(rightName: "", rightImage: "", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: UIColor.black)
    }
    
    @IBAction func loginAction(_ sender: Any) {
        guard self.accountTextField.text!.count >= 0 else {
            ShowMessageView.showMessage(title: nil, message: "請輸入帳號")
            return
        }
        
        KJLRequest.requestForCheckPhone(regType: "business", regCode: "", phoneNumber: self.accountTextField.text!) { (response) in
            guard let response = response as? CheckPhoneClass else {
                return
            }
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil) 
        }
    }

}
