//
//  NSString+TripleDES.h
//  Swapub
//
//  Created by 許佳豪 on 2018/12/5.
//  Copyright © 2018 com.gamania.Swapub. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSData+BWTBase64.h"

NS_ASSUME_NONNULL_BEGIN

@interface NSString (TripleDES)

/**
 字串加密
 
 @return 加密之后的密文
 */
- (NSString *)encryptTripleDES:(NSString *)string;

/**
 字串解密
 
 @return 解密之后的明文
 */
- (NSString *)decryptTripleDES:(NSString *)string;

/**
 十六進制加密
 
 @param hexString 待加密十六進制字串
 @return 加密之后的密文
 */
+ (NSString *)encryptHexString:(NSString *)hexString;

/**
 十六进進制解密
 
 @param encryptHexString 待解密十六進制字串
 @return 解密之后的明文
 */
+ (NSString *)decryptHexString:(NSString *)encryptHexString;

@end

NS_ASSUME_NONNULL_END
