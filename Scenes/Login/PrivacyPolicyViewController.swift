//
//  PrivacyPolicyViewController.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/11.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class PrivacyPolicyViewController: BaseViewController {
    
    @IBOutlet weak var serviceTitle: UILabel!
    @IBOutlet weak var privacyPolicyContent: UILabel!
    @IBOutlet weak var andLabel: UILabel!
    
    @IBOutlet weak var checkBtn: UIButton!
    @IBOutlet weak var firstBtn: UIButton!
    @IBOutlet weak var secondBtn: UIButton!
    @IBOutlet weak var lastBtn: UIButton!
    @IBOutlet weak var confirmBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        setHeaderView(LocalizeUtils.localized(key: "Login"), action: .POP)
        setHeaderView("Login", action: .POP)
        confirmBtn.setTitle(LocalizeUtils.localized(key: Language.Confirm.rawValue), for: .normal)
        serviceTitle.text = LocalizeUtils.localized(key: "PrivacyPolicyconten")
        setUI()
    }
     
    @IBAction func check(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        let image = UIImage(named: sender.isSelected == true ? "icon_check_default" : "icon_uncheck_default")
        checkBtn.setImage(image, for: .normal)
        confirmBtn.backgroundColor = sender.isSelected == true ? #colorLiteral(red: 0.8352941176, green: 0, blue: 0.1098039216, alpha: 1) : #colorLiteral(red: 0.7882352941, green: 0.7921568627, blue: 0.7960784314, alpha: 1)
        confirmBtn.isEnabled = sender.isSelected
    }
    
    @IBAction func popWebView(_ sender: UIButton) {
        if sender.tag == 1 || sender.tag == 2 {
            let vc = DisplayWebViewController(nibName: "DisplayWebViewController", bundle: nil)
            if LocalizeUtils.shared.toLanguage() == "en" {
                vc.status = .termsandconditions
            } else {
                vc.status = .termsandconditionsZh
            }
            present(vc, animated: true, completion: nil)
        } else { /// tag == 3
            let vc = DisplayWebViewController(nibName: "DisplayWebViewController", bundle: nil)
            if LocalizeUtils.shared.toLanguage() == "en" {
                vc.status = .privatepolicy
            } else {
                vc.status = .privatepolicyZh
            } 
            present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func confirm(_ sender: UIButton) {
        print(checkBtn.isSelected)
        if checkBtn.isSelected == true {
            KJLRequest.requestForCheckTerms { (_) in }
        }
        self.dismiss(animated: false) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3 ) {
                NotificationCenter.default.post(name: .popToProfile, object: nil)
            }
        }
    }
    
    private func setUI() {
        privacyPolicyContent.text = LocalizeUtils.localized(key: "PrivacyPolicyconten1")
        andLabel.text = LocalizeUtils.localized(key: "PrivacyPolicyconten3")
//        setCharginBtnUI(firstBtn, toString: "General Terms &")
        setCharginBtnUI(secondBtn, toString: LocalizeUtils.localized(key: "PrivacyPolicyconten2"))
        setCharginBtnUI(lastBtn, toString: LocalizeUtils.localized(key: "PrivacyPolicyconten4"))
        confirmBtn.backgroundColor = #colorLiteral(red: 0.7882352941, green: 0.7921568627, blue: 0.7960784314, alpha: 1)
        confirmBtn.isEnabled = false
    }
    
    private func setCharginBtnUI(_ button: UIButton, toString: String) {
        if let font = UIFont(name: "PorscheNext-Regular", size: 16) {
            let attrs: [ NSAttributedStringKey : Any] =
                       [ NSAttributedStringKey.font : font,
                         NSAttributedStringKey.foregroundColor : #colorLiteral(red: 0.5882352941, green: 0.5960784314, blue: 0.6, alpha: 1) ,
                         .underlineStyle : NSUnderlineStyle.styleSingle.rawValue ]
            
            let buttonTitleStr = NSMutableAttributedString(string: toString, attributes: attrs)
            button.titleLabel?.numberOfLines = 0
            button.setAttributedTitle(buttonTitleStr, for: .normal)   
        }
    }
}
