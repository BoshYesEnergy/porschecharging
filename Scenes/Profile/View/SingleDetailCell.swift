//
//  SingleDetailCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/28.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class SingleDetailCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
      
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
