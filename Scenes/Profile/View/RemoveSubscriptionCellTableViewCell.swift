//
//  RemoveSubscriptionCellTableViewCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/23.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

protocol RemoveSubscription {
    func remove()
}

class RemoveSubscriptionCellTableViewCell: UITableViewCell {

    @IBOutlet weak var removeBtn: UIButton!
    var delegate: RemoveSubscription?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle            = .none
        removeBtn.isEnabled       = true
        removeBtn.backgroundColor = #colorLiteral(red: 0.7882352941, green: 0.7921568627, blue: 0.7960784314, alpha: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUI(onOff: Bool) {
        removeBtn.backgroundColor = onOff == true ? #colorLiteral(red: 0.8352941176, green: 0, blue: 0.1098039216, alpha: 1) : #colorLiteral(red: 0.7882352941, green: 0.7921568627, blue: 0.7960784314, alpha: 1)
        removeBtn.isEnabled = onOff == true ? true : false
        removeBtn.setTitle(LocalizeUtils.localized(key: Language.Remove.rawValue), for: .normal)
    }
    
    @IBAction func remove(_ sender: UIButton) {
        delegate?.remove()
    }
}
