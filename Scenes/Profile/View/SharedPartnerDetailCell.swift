//
//  SharedPartnerDetailCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/26.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

protocol SharedPartnerDetailProtocol: class {
    func appendSharedToRemoveList(index: Int)
}

class SharedPartnerDetailCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var statusTitle: UILabel!
    @IBOutlet weak var modelTitle: UILabel!
     
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var licensePlate: UILabel!
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var endDate: UILabel!
    @IBOutlet weak var model: UILabel!
    @IBOutlet weak var checkBtn: UIButton!
     
    var partner: PartnerModel.Infos? {
        didSet {
            guard let user = partner else { return }
            nameLabel.text      = user.nickname ?? ""
            email.text          = user.email ?? ""
            statusLabel.text    = user.status == true ? LocalizeUtils.localized(key: "Active") : LocalizeUtils.localized(key: "Expired")
            licensePlate.text   = user.licensePlate ?? ""
            startDate.text      = user.startTime ?? ""
            endDate.text        = user.endTime ?? ""
            model.text          = user.carType ?? ""
        }
    }
    
    var delegate: SharedPartnerDetailProtocol?
    var index = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        setLanguage()
        borderView.layer.borderWidth = 1
        borderView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        checkBtn.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setLanguage() {
        titleLabel.text = LocalizeUtils.localized(key: "PorscheChargingService")
        statusTitle.text = LocalizeUtils.localized(key: "Status")
        modelTitle.text = LocalizeUtils.localized(key: "Model")
    }
    
    @IBAction func check(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        let image = UIImage(named: sender.isSelected == true ? "icon_check_default" : "icon_uncheck_default")
        checkBtn.setImage(image, for: .normal)
        delegate?.appendSharedToRemoveList(index: index)
        borderView.backgroundColor = sender.isSelected == true ? #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
}
