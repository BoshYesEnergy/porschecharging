//
//  PartnerDetailCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/26.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

protocol PartnerDetailProtocol: class {
    func appendToRemoveList(index: Int)
}

class PartnerDetailCell: UITableViewCell {

    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var checkBtn: UIButton!
    
    var partner: PartnerModel.Infos? {
        didSet {
            guard let user  = partner else { return }
            nameLabel.text  = user.nickname
            emailLabel.text = user.email
        }
    }
    var index = 0
    weak var delegate: PartnerDetailProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        borderView.layer.borderWidth = 1
        borderView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        checkBtn.isHidden = true
        titleLabel.text = LocalizeUtils.localized(key: "chargingPartner")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func checkStatus(selectSingle: Bool) {
        let image = UIImage(named: selectSingle == true ? "icon_check_default" : "icon_uncheck_default")
        checkBtn.setImage(image, for: .normal)
        borderView.backgroundColor = selectSingle == true ? #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    @IBAction func check(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        let image = UIImage(named: sender.isSelected == true ? "icon_check_default" : "icon_uncheck_default")
        checkBtn.setImage(image, for: .normal)
        delegate?.appendToRemoveList(index: index)
        borderView.backgroundColor = sender.isSelected == true ? #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
}
