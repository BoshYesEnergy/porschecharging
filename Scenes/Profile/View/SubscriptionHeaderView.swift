//
//  SubscriptionHeaderView.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/22.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

protocol subHeaderProtocol: class {
    func tapAction(_ section: Int)
}

class SubscriptionHeaderView: UITableViewCell {

    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var headerIcon: UIImageView!
    
    var section = 0
    weak var delegate: subHeaderProtocol?
    
    var toString: String? {
        didSet {
            guard let text = toString else { return }
            headerTitle.text = text
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib() 
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func headerAction(_ sender: UIButton) {
        delegate?.tapAction(section)
    }
}
