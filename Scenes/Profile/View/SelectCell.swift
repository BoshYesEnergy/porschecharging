//
//  SelectCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/28.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

protocol SelectProtocol: class {
    func selectToDo()
}

class SelectCell: UITableViewCell {

    @IBOutlet weak var selectBtn: UIButton!
    
    weak var delegate: SelectProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        selectBtn.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        selectBtn.layer.borderWidth = 1
        selectBtn.setImage(UIImage(named: "icon_select_click"), for: .highlighted)
//        selectBtn.setTitleColor(#colorLiteral(red: 0.8352941176, green: 0, blue: 0.1098039216, alpha: 1), for: .highlighted)
//        selectBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
     
    @IBAction func selectAction(_ sender: UIButton) {
        delegate?.selectToDo()
    }
}
