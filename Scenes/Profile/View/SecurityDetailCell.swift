//
//  SecurityDetailCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/21.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

protocol SecurityEditProfile: class {
    func updateProfile(name: String, backUpEmail: String)
    func backToProfile()
}

class SecurityDetailCell: UITableViewCell {

    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var emailTextfiled: UITextField!
    @IBOutlet weak var loginMethed: UILabel!
    
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var backUpEmailView: UIView!
    @IBOutlet weak var backUpEmailTextField: UITextField!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    
    weak var delegate: SecurityEditProfile?
    
    var data: ProfileClass? {
        didSet {
            guard let datas = data?.datas else { return }
            emailTextfiled.text = datas.mails?[0].email ?? ""
            nameTextField.text  = datas.nickName ?? ""
            if  datas.mails?.count == 2 {
                backUpEmailTextField.text = datas.mails?[1].email ?? ""
            } 
            loginMethed.text = UserDefaults.standard.string(forKey: "loginMethod") ?? ""
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none 
        setBorderLine(nameView, #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        setBorderLine(backUpEmailView, #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)) 
        setBorderLine(cancelBtn, #colorLiteral(red: 0.5882352941, green: 0.5960784314, blue: 0.6039215686, alpha: 1))
    }
    
    fileprivate func setBorderLine(_ view: UIView, _ lineColor: CGColor) {
        view.layer.borderColor = lineColor
        view.layer.borderWidth = 1
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func save(_ sender: UIButton) {
        delegate?.updateProfile(name: nameTextField.text ?? "", backUpEmail: backUpEmailTextField.text ?? "")
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        delegate?.backToProfile()
    }
}
