//
//  AddSubscriptionCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/22.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

protocol AddSubscriptionProtocol: class {
    func AddNewSubscription()
    func editSubscription()
}

class AddSubscriptionCell: UITableViewCell {

    @IBOutlet weak var errorMessage: UILabel!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var addContentLabel: UILabel!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var errorMessageHeight: NSLayoutConstraint!
    @IBOutlet weak var editBtnHeight: NSLayoutConstraint!
     
    weak var delegate: AddSubscriptionProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        borderView.layer.borderWidth = 1
        borderView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//        errorMessageHeight.constant = 25
//        editBtnHeight.constant = 50
        errorMessageHeight.constant = 0
        editBtnHeight.constant = 0
        addBtn.setImage(UIImage(named: "button_add_click"), for: .highlighted)
    }
    
    func setUI(_ content: String, _ message: String) {
        borderView.layer.cornerRadius   = 10
        addContentLabel.text            = content
        errorMessage.text               = message
        editBtnHeight.constant          = 0
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func addSubscription(_ sender: UIButton) {
        delegate?.AddNewSubscription()
    }
    
    @IBAction func edit(_ sender: UIButton) {
        delegate?.editSubscription()
    }
}
