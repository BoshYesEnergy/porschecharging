//
//  SubscriptionHeaderCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/22.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

protocol SubscriptionHeaderPorocol: class {
    func headerTapAction(section: Int)
}

class SubscriptionHeaderCell: UITableViewCell {

    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var headerIcon: UIImageView!
    @IBOutlet weak var headerBtn: UIButton!
    
    weak var delegate: SubscriptionHeaderPorocol?
    var section = 0
    var sectionString: String? {
        didSet {
            guard let string = sectionString else { return }
            headerTitle.text = string
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib() 
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func headerTapAction(_ sender: UIButton) {
        delegate?.headerTapAction(section: section)
    }
}
