//
//  RemoveEditCreditCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/25.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

protocol RemoveEditCreditProtocol: class{
    func checkRemove()
}

class RemoveEditCreditCell: UITableViewCell {

    @IBOutlet weak var removeBtn: UIButton!
    
    weak var delegate: RemoveEditCreditProtocol?
    var tapOff: Bool? {
        didSet {
            guard let tap = tapOff else { return }
            if tap == true {
                removeBtn.backgroundColor = #colorLiteral(red: 0.8352941176, green: 0, blue: 0.1098039216, alpha: 1)
            } else {
                removeBtn.backgroundColor = #colorLiteral(red: 0.7882352941, green: 0.7921568627, blue: 0.7960784314, alpha: 1)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func remove(_ sender: UIButton) {
        delegate?.checkRemove()
    }
}
