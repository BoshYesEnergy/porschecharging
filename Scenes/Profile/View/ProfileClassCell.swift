//
//  ProfileClassCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/20.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class ProfileClassCell: UICollectionViewCell {
    
    @IBOutlet weak var classTitle: UILabel!
    @IBOutlet weak var redLine: UIView!
    @IBOutlet weak var redIcon: UIView!
    
    var textString: String? {
        didSet {
            guard let text   = textString else { return }
            classTitle.text  = text
            #warning("wait check")
            redIcon.isHidden = true
//            switch text {
//            case "Subscription":
//                redIcon.isHidden = KJLRequest.shareInstance().profile?.datas?.subscription ?? false
//            case "Payment":
//                redIcon.isHidden = KJLRequest.shareInstance().profile?.datas?.creditCard ?? "" == "" ? false : true
//            case "Partner":
//                redIcon.isHidden = KJLRequest.shareInstance().profile?.datas?.partner ?? false
//            default: redIcon.isHidden = true
//            }
        }
    } 
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.isUserInteractionEnabled = true
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        redLine.isHidden = true
        redIcon.layer.cornerRadius = redIcon.frame.width / 2
    }
    
    override var isSelected: Bool {
        didSet { 
            redLine.isHidden = isSelected == true ? false : true
        }
    }
}
