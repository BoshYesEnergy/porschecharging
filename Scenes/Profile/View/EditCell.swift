//
//  EditCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/25.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

protocol EditProtocol: class {
    func gotoEditVC()
}

class EditCell: UITableViewCell {

    @IBOutlet weak var editBtn: UIButton!
    
    var delegate: EditProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        editBtn.layer.borderWidth = 1
        editBtn.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        editBtn.setImage(UIImage(named: "icon_edit_click"), for: .highlighted)
//        editBtn.setTitleColor(#colorLiteral(red: 0.8352941176, green: 0, blue: 0.1098039216, alpha: 1), for: .highlighted)
//        editBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
 
    } 
    
    @IBAction func edit(_ sender: UIButton) {
        delegate?.gotoEditVC()
    }
}
