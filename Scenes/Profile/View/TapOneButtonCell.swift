//
//  ShowPartnerCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/25.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

protocol TapOneButtonProtocol: class {
    func tapAction(tagNumber: Int)
}

class TapOneButtonCell: UITableViewCell {

    @IBOutlet weak var tapBtn: UIButton!
    
    weak var delegate: TapOneButtonProtocol?
    
    var tagNumber = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    func setView(custom: Bool) {
        if custom == true {
            tapBtn.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            tapBtn.layer.borderWidth = 1
            tapBtn.backgroundColor   = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            tapBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        } else {
            tapBtn.layer.borderWidth = 0
            tapBtn.backgroundColor = #colorLiteral(red: 0.8352941176, green: 0, blue: 0.1098039216, alpha: 1)
            tapBtn.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func tapAction(_ sender: UIButton) {
        print(tagNumber)
        delegate?.tapAction(tagNumber: tagNumber)
    }
    
}
