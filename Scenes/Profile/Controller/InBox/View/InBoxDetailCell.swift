//
//  InBoxDetailCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/19.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

protocol InBoxDetailDelegate: class {
    func inBoxToRemoveList(index: Int)
}

class InBoxDetailCell: UITableViewCell {

    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var inBoxImage: UIImageView!
    @IBOutlet weak var inBoxName: UILabel!
    @IBOutlet weak var nextImage: UIImageView!
    @IBOutlet weak var inboxContent: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var garyLine: UIView!
    @IBOutlet weak var checkBtn: UIButton!
    
    var delegate: InBoxDetailDelegate?
    var index = 0
    
    var list: NewsListClass.Data? {
        didSet {
            guard let list    = list else { return }
            
            inboxContent.font = list.readDate ?? "" != "" ? UIFont(name: "PorscheNext-Regular", size: 16) : UIFont(name: "PorscheNext-SemiBold", size: 16)
            
            inBoxName.font = list.readDate ?? "" != "" ? UIFont(name: "PorscheNext-Regular", size: 18) : UIFont(name: "PorscheNext-Bold", size: 18)
            
            inBoxName.text    = list.subject ?? ""
            inboxContent.text = list.content ?? ""
            dateLabel.text    = list.createDate ?? ""
            inBoxImage.image  = list.readDate ?? "" != "" ? UIImage(named: ToolPhoto.inbox_open_default.rawValue) : UIImage(named: ToolPhoto.inbox_default.rawValue)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle    = .none
        inBoxImage.image  = UIImage(named: ToolPhoto.inbox_default.rawValue)
        nextImage.image   = UIImage(named: ToolPhoto.next.rawValue)
//        dateLabel.text    = "2020/10/25 11:02"
        checkBtn.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
 
    }
    
    func setCheckStatus(selectSingle: Bool) {
        let image = UIImage(named: selectSingle == true ? ToolPhoto.check.rawValue : ToolPhoto.check_none.rawValue)
        checkBtn.setImage(image, for: .normal)
        bottomView.backgroundColor = selectSingle == true ? #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    func setCheckStatus(selectAll: Bool) {
        let image = UIImage(named: selectAll == true ? ToolPhoto.check.rawValue : ToolPhoto.check_none.rawValue)
        checkBtn.setImage(image, for: .normal)
        bottomView.backgroundColor = selectAll == true ? #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        checkBtn.isSelected  = selectAll
    }
    
    @IBAction func check(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        let image = UIImage(named: sender.isSelected == true ? ToolPhoto.check.rawValue : ToolPhoto.check_none.rawValue)
        checkBtn.setImage(image, for: .normal)  
        bottomView.backgroundColor = sender.isSelected == true ? #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        delegate?.inBoxToRemoveList(index: index)
    }
}
