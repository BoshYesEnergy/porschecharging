//
//  InBoxEditVC.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/20.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

enum InBoxSelectAll {
    case Default
    case None
    case All
}

class InBoxEditVC: BaseViewController {
    
    @IBOutlet weak var editTable: UITableView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var selectAllBtn: UIButton!
    @IBOutlet weak var removeBtn: UIButton!
    
    var pageIndex = -1
    var pageAmount = 10
    var pushGetList: [NewsListClass.Data] = []
    var deleteArray: [Int] = []
    var inboxStatus: InBoxSelectAll = .Default
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setHeaderView(LocalizeUtils.localized(key: "editInBox"), action: .POP)
        hiddenNavigationBar(isHidden: true)
        setTable()
        
        removeBtn.backgroundColor = ThemeManager.shared.colorPalettes.garyBackgroundColor
        removeBtn.isEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        DispatchQueue.main.async {
            self.getPushGetList()
        }
        
        selectAllBtn.setTitle(LocalizeUtils.localized(key: "InboxSelectAll"), for: .normal)
        removeBtn.setTitle(LocalizeUtils.localized(key: Language.Remove.rawValue), for: .normal)
    }
}


//MARK: Setting View
extension InBoxEditVC {
    fileprivate func setTable() {
        editTable.register(UINib(nibName: "InBoxDetailCell", bundle: nil), forCellReuseIdentifier: "InBoxDetailCell")
    }
}


//MARK: Action
extension InBoxEditVC {
    @IBAction func selectAllMessage(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        inboxStatus = sender.isSelected == true ? .All : .None
        if inboxStatus == .None {
            deleteArray = []
            print(deleteArray)
            editTable.reloadData()
        } else {
            if pushGetList.count >= 1 {
                let count = pushGetList.count - 1
                for i in 0...count {
                    inBoxToRemoveList(index: i)
                }
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.inboxStatus = .Default
        }
        
        if deleteArray.count >= 1 {
            removeBtn.backgroundColor = ThemeManager.shared.colorPalettes.redBackgroundColor
            removeBtn.isEnabled = true
        } else {
            removeBtn.backgroundColor = ThemeManager.shared.colorPalettes.garyBackgroundColor
            removeBtn.isEnabled = false
        }
    }
    
    @IBAction func removeMessage(_ sender: UIButton) {
        popMessage(content: LocalizeUtils.localized(key: "InboxRemovethese"))
    }
}


extension InBoxEditVC {
    fileprivate func getPushGetList() {
        guard let deviceId = UserDefaults.standard.string(forKey: "deviceId"),let memberNo = UserDefaults.standard.string(forKey: "memberNo"),
            deviceId != ""
            else { return }
        print("\(deviceId)\n\(memberNo)\n\(self.pageIndex)\n\(self.pageAmount)")
        self.pageIndex  = self.pageIndex + 1
        KJLRequest.requestForPushGetList(deviceId: deviceId, memberNo: memberNo, pageNum: self.pageIndex ,rowsPerPage:self.pageAmount) {[unowned self] (response) in
            guard let response = response as? NewsListClass,
                  let datas = response.datas
                  else { return }
            self.pushGetList.removeAll()
            self.pushGetList = datas
            self.editTable.reloadData()
        }
    }
    
    fileprivate func deleteMessage(completion: @escaping ()-> Void ) {
        let memberNo = UserDefaults.standard.string(forKey: "memberNo")!
        if deleteArray.count >= 1 {
            for i in deleteArray {
                print("deleteArray: \(i)")
                let detailId = pushGetList[i].detailId ?? ""
                let taskId   = pushGetList[i].taskId ?? 0
                KJLRequest.requestForPushDelete(detailId: detailId, memberNo: memberNo, taskId: "\(taskId)") { (response) in
                    guard (response as? NormalClass) != nil else { return }
                }
            }
        }
        completion()
    }
}


//MARK: Table view delegatee, data source
extension InBoxEditVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pushGetList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "InBoxDetailCell", for: indexPath) as? InBoxDetailCell {
            cell.checkBtn.isHidden  = false
            cell.nextImage.isHidden = true
            cell.index              = indexPath.row
            cell.delegate           = self
            cell.list               = pushGetList[indexPath.row]
            cell.setCheckStatus(selectSingle: deleteArray.contains(indexPath.row))
            
            if inboxStatus == .None {
                cell.setCheckStatus(selectAll: false)
            } else if inboxStatus == .All {
                cell.setCheckStatus(selectAll: true)
            }
            
            return cell
        } else {
            return UITableViewCell()
        }
    } 
}


//MARK: Protocol - InBoxDetailCell
extension InBoxEditVC: InBoxDetailDelegate {
    func inBoxToRemoveList(index: Int) {
        if deleteArray.contains(index) == false {
            deleteArray.append(index)
            editTable.reloadData()
            print("deleteArray: \(deleteArray)")
        } else {
            let newList = deleteArray.filter{ $0 != index }
            deleteArray.removeAll()
            deleteArray = newList
            editTable.reloadData()
            print("deleteArray: \(deleteArray)")
        }
        
        if deleteArray.count >= 1 {
            removeBtn.backgroundColor = ThemeManager.shared.colorPalettes.redBackgroundColor
            removeBtn.isEnabled = true
        } else {
            removeBtn.backgroundColor = ThemeManager.shared.colorPalettes.garyBackgroundColor
            removeBtn.isEnabled = false
        }
    }
}


extension InBoxEditVC: ShowCheckMessageDelegate {
    func tapConfirm(tagCount: Int) {
        self.dismiss(animated: true) {
            self.deleteMessage {
//                self.getPushGetList()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        } 
    }
    
    func tapCancel() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func popMessage(content: String) {
        let vc = ShowCheckMessageVC()
        vc.toTitle   = ""
        vc.toContent = content
        vc.delegate  = self
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        present(vc, animated: true, completion: nil)
    }
}

