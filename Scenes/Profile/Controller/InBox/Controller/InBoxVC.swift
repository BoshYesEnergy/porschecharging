//
//  InBoxVC.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/19.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class InBoxVC: BaseViewController {
 
    @IBOutlet weak var inBoxTable: UITableView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var removeBtn: UIButton!
    @IBOutlet weak var readAllBtn: UIButton!
     
    var pushGetList: [NewsListClass.Data] = []
    var pageIndex = -1
    var pageAmount = 50
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setHeaderView(LocalizeUtils.localized(key: "Inbox"), action: .DISMISS)
        hiddenNavigationBar(isHidden: true)
        setTable() 
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.getPushGetList(){ }
        }
        
        removeBtn.setTitle(LocalizeUtils.localized(key: Language.Remove.rawValue), for: .normal)
        readAllBtn.setTitle(LocalizeUtils.localized(key: "ReadAll"), for: .normal)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // 還原page
        pageIndex = -1
    }
}


//MARK: Setting View
extension InBoxVC {
    fileprivate func setTable() {
        inBoxTable.register(UINib(nibName: "InBoxDetailCell", bundle: nil), forCellReuseIdentifier: "InBoxDetailCell")
    }
    
    fileprivate func checkButtonStatus() {
        if self.pushGetList.count == 0 {
            self.removeBtn.backgroundColor = ThemeManager.shared.colorPalettes.garyBackgroundColor
            self.removeBtn.isEnabled = false
            
            self.readAllBtn.backgroundColor = ThemeManager.shared.colorPalettes.garyBackgroundColor
            self.readAllBtn.isEnabled = false
        } else {
            self.removeBtn.backgroundColor = ThemeManager.shared.colorPalettes.redBackgroundColor
            self.removeBtn.isEnabled = true
            
            self.readAllBtn.backgroundColor = ThemeManager.shared.colorPalettes.redBackgroundColor
            self.readAllBtn.isEnabled = true
        }
    }
}


//MARK: Action
extension InBoxVC {
    @IBAction func goRemove(_ sender: UIButton) {
        let vc = InBoxEditVC(nibName: "InBoxEditVC", bundle: nil)
        vc.pushGetList = pushGetList
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func readAllInBox(_ sender: UIButton) {
        let memberNo = UserDefaults.standard.string(forKey: "memberNo")!
        for i in pushGetList {
            let detailId = i.detailId ?? ""
            let taskId   = i.taskId ?? 0
            readMessage(detailId: detailId, memberNo: memberNo, taskId: "\(taskId)")
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.getPushGetList(){ }
        } 
    }
}


//MARK: Api
extension InBoxVC {
    func getPushGetList(completion: @escaping ()-> Void) {
        guard let deviceId = UserDefaults.standard.string(forKey: "deviceId"),let memberNo = UserDefaults.standard.string(forKey: "memberNo"),
            deviceId != ""
            else { return }
        print("\(deviceId)\n\(memberNo)\n\(self.pageIndex)\n\(self.pageAmount)")
//        self.pageIndex  = self.pageIndex + 1
        KJLRequest.requestForPushGetList(deviceId: deviceId, memberNo: memberNo, pageNum: self.pageIndex ,rowsPerPage:self.pageAmount) {[weak self] (response) in
            guard let response = response as? NewsListClass
                  else { return }
            self?.pushGetList.removeAll()
            self?.pushGetList = response.datas ?? []
            self?.inBoxTable.reloadData()
        }
        completion()
    }
    
    func readMessage(detailId: String, memberNo: String, taskId: String) {
        KJLRequest.requestForPushSetRead(detailId: detailId, memberNo: memberNo, taskId: taskId) { (response) in
            guard let _ = response as? NormalClass else { return }
            self.inBoxTable.reloadData()
        }
    }
}


//MARK: Table view delegate & datasoure
extension InBoxVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pushGetList.count == 0 ? 1 : pushGetList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if pushGetList.count == 0 {
            let cell = UITableViewCell()
            checkButtonStatus()
            cell.textLabel?.text = LocalizeUtils.localized(key: "noMessage")
            cell.textLabel?.textAlignment = .left
            cell.textLabel?.textColor = ThemeManager.shared.colorPalettes.garyToText
            cell.textLabel?.font = UIFont(name: "PorscheNext-Regular", size: 16)
            cell.selectionStyle = .none
            return cell
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "InBoxDetailCell", for: indexPath) as? InBoxDetailCell {
                checkButtonStatus()
                let data   = pushGetList[indexPath.row]
                cell.index = indexPath.row
                cell.list  = data
                return cell
            } else {
                return UITableViewCell()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return pushGetList.count == 0 ? 60 : 165
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if pushGetList.count != 0 {
            /// call read
            let detailId = pushGetList[indexPath.row].detailId ?? ""
            let memberNo = UserDefaults.standard.string(forKey: "memberNo")!
            let taskId   = pushGetList[indexPath.row].taskId ?? 0
            readMessage(detailId: detailId, memberNo: memberNo, taskId: "\(taskId)")
            /// check data
            let vc = InBoxMesageVC(nibName: "InBoxMesageVC", bundle: nil)
            vc.messageToTitle = pushGetList[indexPath.row].subject ?? ""
            vc.messageToContent = pushGetList[indexPath.row].content ?? ""
            vc.messageToDate = pushGetList[indexPath.row].createDate ?? ""
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}
