//
//  InBoxMesageVC.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/20.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class InBoxMesageVC: BaseViewController {

    @IBOutlet weak var messageName: UILabel!
    @IBOutlet weak var messageDate: UILabel!
    @IBOutlet weak var messageContent: UILabel!
    @IBOutlet weak var phoneBtn: UIButton!
    @IBOutlet weak var secondContent: UILabel!
    @IBOutlet weak var porscheLabel: UILabel!
    
    var messageToTitle   = ""
    var messageToContent = ""
    var messageToDate    = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setHeaderView(LocalizeUtils.localized(key: "InboxMessage"), action: .POP)
        hiddenNavigationBar(isHidden: true)
        
        messageName.text    = messageToTitle
        messageContent.text = messageToContent
        messageDate.text    = messageToDate
        phoneBtn.isHidden   = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        /// set language, 
    }
    
    @IBAction func phoneCall(_ sender: UIButton) {
        
    }
    
}
