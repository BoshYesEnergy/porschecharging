//
//  SubscriptionCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/22.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

protocol SubscriptionProtocol: class {
    func tapCheck(index: Int)
}

class SubscriptionCell: UITableViewCell {
    @IBOutlet weak var statusTitle: UILabel!
    @IBOutlet weak var vinTitle: UILabel!
    @IBOutlet weak var startDateTitle: UILabel!
    @IBOutlet weak var expiryDateTitle: UILabel!
     
    @IBOutlet weak var model: UILabel!
    @IBOutlet weak var iconBtn: UIButton!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var vinNumber: UILabel!
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var expiryDate: UILabel! 
    @IBOutlet weak var redIconImage: UIImageView!
     
    weak var delegte: SubscriptionProtocol?
    
    var index = 0
    
    var data: PorscheCarListModel.Infos? {
        didSet {
            guard let list = data else { return }
            model.text          = list.carType ?? ""
            vinNumber.text      = list.vin ?? ""
            status.text         = list.status == true ? LocalizeUtils.localized(key: "Active") : LocalizeUtils.localized(key: "Expired")
            startDate.text      = list.startTime 
            expiryDate.text     = list.endTime
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        setLanguage()
        borderView.layer.borderWidth = 1
        borderView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        iconBtn.isHidden             = true
        redIconImage.isHidden        = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setLanguage() {
        statusTitle.text = LocalizeUtils.localized(key: Language.SubStatus.rawValue)
        vinTitle.text = LocalizeUtils.localized(key: Language.SubVin.rawValue)
        startDateTitle.text = LocalizeUtils.localized(key: Language.SubStartDate.rawValue)
        expiryDateTitle.text = LocalizeUtils.localized(key: Language.SubEndDate.rawValue)
    }
    
    func checkStatus(selectSingle: Bool) {
        let image = UIImage(named: selectSingle == true ? "icon_check_default" : "icon_uncheck_default")
        iconBtn.setImage(image, for: .normal)
        borderView.backgroundColor = selectSingle == true ? #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    @IBAction func check(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        let image = UIImage(named: sender.isSelected == true ? "icon_check_default" : "icon_uncheck_default")
        iconBtn.setImage(image, for: .normal)
        borderView.backgroundColor = sender.isSelected == true ? #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        delegte?.tapCheck(index: index)
    }
}
