//
//  AddNewSubscriptionCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/6.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

protocol AddNewSubscriptionProtocol: class {
    func AddNewSubscription()
}

class AddNewSubscriptionCell: UITableViewCell {
 
    @IBOutlet weak var iconBtn: UIButton!
    @IBOutlet weak var contentLabel: UILabel!
    
    var delegate: AddNewSubscriptionProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        contentLabel.text = LocalizeUtils.localized(key: "SubscriptionAdd")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func tapAction(_ sender: UIButton) {
        delegate?.AddNewSubscription()
    } 
}
