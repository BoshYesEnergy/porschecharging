//
//  AddSubscriptionViewController.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/22.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

protocol SubscriptionReloadDelegate: class {
    func subReload()
}

class AddSubscriptionViewController: BaseViewController {
    
    @IBOutlet weak var accountTitle: UILabel!
    @IBOutlet weak var cardTitle: UILabel!
    
    @IBOutlet weak var content: UILabel!
    @IBOutlet weak var emailBorderView: UIView!
    @IBOutlet weak var vinTextField: UITextField!
    @IBOutlet weak var verifyBorderView: UIView!
    @IBOutlet weak var cardNumberTextField: UITextField!
    @IBOutlet weak var confirmBrn: UIButton!
    @IBOutlet weak var secureCodeBtn: UIButton!
    @IBOutlet weak var errorMessage: UILabel!
    @IBOutlet weak var redLine: UIView!
    
    /// default == 65
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    weak var delegate: SubscriptionReloadDelegate?
    
    var custom = false
    
    var subscriptionData :SubscriptionCarModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setHeaderView(LocalizeUtils.localized(key: "EditSubscription"), action: .POP) 
        setView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        callProfile()
        setLanguage()
        setDefualt() 
        topConstraint.constant = custom == true ? 0 : 65
        NotificationCenter.default.addObserver(self, selector: #selector(textDidChangeValue(obj:)), name: .UITextFieldTextDidChange, object: vinTextField)
        NotificationCenter.default.addObserver(self, selector: #selector(textDidChangeValue(obj:)), name: .UITextFieldTextDidChange, object: cardNumberTextField)
        NotificationCenter.default.addObserver(self, selector: #selector(subscriptionReload), name: .reloadSubscription, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(changeUI), name: .reloaAddSubscriptionView, object: nil)
    }
    
    func setLanguage() {
        accountTitle.text  = LocalizeUtils.localized(key: Language.SubID.rawValue)
        cardTitle.text = LocalizeUtils.localized(key: "ChargingCardNumber")
        content.text = LocalizeUtils.localized(key: Language.SubContent.rawValue)
        confirmBrn.setTitle(LocalizeUtils.localized(key: Language.Confirm.rawValue), for: .normal)
        errorMessage.text = LocalizeUtils.localized(key: Language.SubError.rawValue)
        vinTextField.placeholder = LocalizeUtils.localized(key: "Vin17")
        cardNumberTextField.placeholder = LocalizeUtils.localized(key: "Card12")
    }
    
    @objc fileprivate func subscriptionReload() {
        vinTextField.text      = ""
        cardNumberTextField.text = ""
        delegate?.subReload()
    }
    
    fileprivate func setView() {
        emailBorderView.layer.borderWidth       = 1
        emailBorderView.layer.borderColor       = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        verifyBorderView.layer.borderWidth      = 1
        verifyBorderView.layer.borderColor      = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        errorMessage.isHidden                   = true
        redLine.isHidden                        = true
        cardNumberTextField.isSecureTextEntry   = false
        confirmBrn.isEnabled                    = false
        self.onIQKeyboardManager()
        KJLRequest.shareInstance().kJLRequestDelegate = self
    }
    
    func setDefualt() {
        vinTextField.text = UserDefaults.standard.string(forKey: "subEmail") ?? ""
        cardNumberTextField.text = UserDefaults.standard.string(forKey: "subCardNumber") ?? ""
        
        confirmBrn.backgroundColor = #colorLiteral(red: 0.7882352941, green: 0.7921568627, blue: 0.7960784314, alpha: 1)
        confirmBrn.isEnabled       = false
        errorMessage.isHidden = true
        redLine.isHidden      = true
    }
    
    @objc func changeUI() {
        confirmBrn.backgroundColor = #colorLiteral(red: 0.8352941176, green: 0, blue: 0.1098039216, alpha: 1)
        confirmBrn.isEnabled       = true
    }
    
    private func errorType(_ isError: Bool) {
        emailBorderView.layer.borderColor   = isError == true ? #colorLiteral(red: 0.8352941176, green: 0, blue: 0.1098039216, alpha: 1) : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        verifyBorderView.layer.borderColor  = isError == true ? #colorLiteral(red: 0.8352941176, green: 0, blue: 0.1098039216, alpha: 1) : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        if isError {
            if cardNumberTextField.isSecureTextEntry == false {
                secureCodeBtn.setImage(UIImage(named: "icon_view_error"), for: .normal)
            } else {
                secureCodeBtn.setImage(UIImage(named: "icon_view_off_error"), for: .normal)
            }
        } else {
            if cardNumberTextField.isSecureTextEntry == false {
                secureCodeBtn.setImage(UIImage(named: "icon_view_default"), for: .normal)
            } else {
                secureCodeBtn.setImage(UIImage(named: "icon_view_off_default"), for: .normal)
            }
        } 
    }
    
    fileprivate func callProfile() {
        KJLRequest.requestForProfile { (response) in
            guard let data = response as? ProfileClass else { return }
            KJLRequest.shareInstance().profile = data
        }
    }
    
    @IBAction func confirm(_ sender: UIButton) { 
        if vinTextField.text == "" || cardNumberTextField.text == "" {
            return
        }
        
        if cardNumberTextField.text?.count ?? 0 < 12 {
            verifyBorderView.layer.borderColor = #colorLiteral(red: 0.8352941176, green: 0, blue: 0.1098039216, alpha: 1)
            return
        }
        
        if KJLRequest.shareInstance().profile?.datas?.cardNum == ""
            && KJLRequest.shareInstance().profile?.datas?.subscription == false {
            
            /// cal api
            KJLRequest.requestForViewSubscription(vinTextField.text ?? "", cardNum: cardNumberTextField.text?.uppercased() ?? "") { (response) in
                guard let data = response as? SubscriptionCarModel else { return }
                if data.resCode != "0000" {
                    self.errorMessage.text     = data.resMsg ?? ""
                    self.errorMessage.isHidden = false
                    self.redLine.isHidden      = false
                } else {
                    self.errorMessage.isHidden = true
                    self.redLine.isHidden      = true
                    UserDefaults.standard.set(self.vinTextField.text ?? "", forKey: "subEmail")
                    UserDefaults.standard.set(self.cardNumberTextField.text?.uppercased() ?? "", forKey: "subCardNumber")
                    
                    let vc = ShowInformationDetailVC()
                    vc.delegate = self
                    vc.detailToString  = data.datas?.validity ?? ""
                    vc.modelToString   = data.datas?.model ?? ""
                    vc.projectToString = data.datas?.vin ?? ""
                    vc.contentToString = LocalizeUtils.localized(key: "SubscriptionAlert")
                    vc.modalPresentationStyle = .overFullScreen
                    vc.modalTransitionStyle   = .crossDissolve
                    self.present(vc, animated: true, completion: nil)
                }
            }
        } else {
            KJLRequest.requestForSubscriptionCar(email: vinTextField.text ?? "", cardNum: cardNumberTextField.text?.uppercased() ?? "", bind: true) { (response) in
                guard let data = response as? SubscriptionCarModel else { return }
                self.subscriptionData = data
                if data.resCode != "0000" {
                    self.errorMessage.text     = data.resMsg ?? ""
                    self.errorMessage.isHidden = false
                    self.redLine.isHidden      = false
                } else {
                    self.errorMessage.isHidden = true
                    self.redLine.isHidden      = true
                    let vc = ShowInformationDetailVC()
                    vc.delegate = self
                    vc.detailToString  = data.datas?.validity ?? ""
                    vc.modelToString   = data.datas?.model ?? ""
                    vc.projectToString = data.datas?.vin ?? ""
                    vc.modalPresentationStyle = .overFullScreen
                    vc.modalTransitionStyle   = .crossDissolve
                    self.present(vc, animated: true, completion: nil)
                }
            }
        }
        
    }
    
    @IBAction func secureAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        let image = UIImage(named: sender.isSelected == false ? "icon_view_default" : "icon_view_off_default")
        secureCodeBtn.setImage(image, for: .normal)
        cardNumberTextField.isSecureTextEntry = sender.isSelected == false ? false : true
        cardNumberTextField.keyboardType = .emailAddress
    }
    
    @objc fileprivate func textDidChangeValue(obj: Notification) {
        if cardNumberTextField.text?.count ?? 0 >= 12 && vinTextField.text?.count ?? 0 >= 17 {
            confirmBrn.backgroundColor = #colorLiteral(red: 0.8352941176, green: 0, blue: 0.1098039216, alpha: 1)
            confirmBrn.isEnabled       = true
        } else {
            confirmBrn.backgroundColor = #colorLiteral(red: 0.7882352941, green: 0.7921568627, blue: 0.7960784314, alpha: 1)
            confirmBrn.isEnabled       = false
        }
        
        if vinTextField.text?.count ?? 0 < 17 {
            emailBorderView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }  
    } 
}


//MARK: Text field delegate
extension AddSubscriptionViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let count = text.count + string.count - range.length
        if textField == vinTextField {
            return count > 17 ? false : true
        }
        
        if textField == cardNumberTextField {
            if vinTextField.text?.count ?? 0 < 17 {
                emailBorderView.layer.borderColor = #colorLiteral(red: 0.8352941176, green: 0, blue: 0.1098039216, alpha: 1)
            } else {
                emailBorderView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            }
            
            return count > 12 ? false : true
        }
         
        return true
    }
}


extension AddSubscriptionViewController: ShowInformationDetailDelegate {
    func tapLeftWithAction() {
        if KJLRequest.shareInstance().profile?.datas?.cardNum == ""
            && KJLRequest.shareInstance().profile?.datas?.subscription == false { /// new case
            self.dismiss(animated: true, completion: nil)
        } else {
            KJLRequest.requestForSubscriptionCar(email: vinTextField.text ?? "", cardNum: cardNumberTextField.text?.uppercased() ?? "" , bind: false) { (response) in
                guard let _ = response as? SubscriptionCarModel else { return }
                self.dismiss(animated: false) {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    func tapRightWithAction() {
        if KJLRequest.shareInstance().profile?.datas?.cardNum == ""
            && KJLRequest.shareInstance().profile?.datas?.subscription == false { /// new case
            dismiss(animated: true) {
                let vc = PaymentViewController(nibName: "PaymentViewController", bundle: nil)
                vc.whereTo = .Subscription
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else if KJLRequest.shareInstance().profile?.datas?.cardNum != ""
                    && KJLRequest.shareInstance().profile?.datas?.subscription == true {
            
            self.dismiss(animated: false) {
                self.navigationController?.popViewController(animated: true)
            }
            
        } else if KJLRequest.shareInstance().profile?.datas?.cardNum != "" {
            
            self.dismiss(animated: true) {
                NotificationCenter.default.post(name: .reloadSubscription, object: nil)
            }
            
        } else {
            self.dismiss(animated: false) {
                self.navigationController?.popViewController(animated: true) 
            } 
        }
         
    }
}


extension AddSubscriptionViewController: KJLRequestDelegate {
    func apiFailResultWith(resCode: String, message: String) {
        let vc = ShowMessageAlertVC()
        vc.stringToContent = message
        vc.delegate = self
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        present(vc, animated: true) { }
    } 
}


extension AddSubscriptionViewController: ShowMessageAlertDelegate {
    func messageTapAction() {
        self.dismiss(animated: true, completion: nil)
    } 
}
 
