//
//  SubscriptionViewController.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/22.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class SubscriptionViewController: UIViewController {
    
    @IBOutlet weak var subTable: UITableView!
    
    var isExpendDataList = [true/*, false*/]
    var subHeader = ["Porsche Charging Service Subscription"/*, "Remind"*/]
    var dataList: [PorscheCarListModel.Infos] = []
    
    weak var delegate: SubscriptionReloadDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTable()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(subscriptionReload), name: .reloadSubscription, object: nil)
        DispatchQueue.main.async {
            self.callApiPorscheCarList()
        }
    }
    
    @objc fileprivate func subscriptionReload() { 
        delegate?.subReload()
    }
    
    fileprivate func setTable() {
        subTable.register(UINib(nibName: "SubscriptionHeaderView", bundle: nil), forCellReuseIdentifier: "SubscriptionHeaderView")
        subTable.register(UINib(nibName: "RemindCell", bundle: nil), forCellReuseIdentifier: "RemindCell")
        subTable.register(UINib(nibName: "SubscriptionCell", bundle: nil), forCellReuseIdentifier: "SubscriptionCell") 
        subTable.register(UINib(nibName: "AddNewSubscriptionCell", bundle: nil), forCellReuseIdentifier: "AddNewSubscriptionCell")
        subTable.register(UINib(nibName: "TapOneButtonCell", bundle: nil), forCellReuseIdentifier: "TapOneButtonCell")
    }
    
    fileprivate func popErrorMessage(reMsg: String) {
        let vc = ShowMessageAlertVC()
        vc.stringToContent = reMsg
        vc.titleToBtn = LocalizeUtils.localized(key: Language.Confirm.rawValue)
        vc.delegate = self
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        present(vc, animated: true) { }
    }
    
    fileprivate func callApiPorscheCarList() {
        KJLRequest.requestForPorscheCarList {[weak self] (response) in
            guard let data = response as? PorscheCarListModel
            else { return }
            if data.resCode != "0000" {
//                self?.popErrorMessage(reMsg: data.resMsg ?? "")
                self?.dataList.removeAll()
                self?.subTable.reloadData()
            } else {
                self?.dataList.removeAll()
                self?.dataList = data.datas?.infos ?? []
            }
            self?.subTable.reloadData()
        }
    }
}


extension SubscriptionViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return isExpendDataList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataList.count == 0 {
            return 1
        } else {
            return dataList.count + 2
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if dataList.count == 0 {
            if let addCell = tableView.dequeueReusableCell(withIdentifier: "AddNewSubscriptionCell", for: indexPath) as? AddNewSubscriptionCell {
                addCell.delegate = self
                return addCell
            } else {
                return UITableViewCell()
            }
        } else {
            if indexPath.row == dataList.count {
                if let addCell = tableView.dequeueReusableCell(withIdentifier: "AddNewSubscriptionCell", for: indexPath) as? AddNewSubscriptionCell {
                    addCell.delegate = self
                    return addCell
                } else {
                    return UITableViewCell()
                }
            } else if indexPath.row == dataList.count + 1 {
                if let tapCell = tableView.dequeueReusableCell(withIdentifier: "TapOneButtonCell", for: indexPath) as? TapOneButtonCell {
                    tapCell.delegate = self
                    tapCell.tapBtn.setTitle(LocalizeUtils.localized(key: Language.Remove.rawValue), for: .normal)
                    tapCell.setView(custom: false)
                    return tapCell
                } else {
                    return UITableViewCell()
                }
            } else {
                if let subscriptionCell = tableView.dequeueReusableCell(withIdentifier: "SubscriptionCell", for: indexPath) as? SubscriptionCell {
                    subscriptionCell.data = dataList[indexPath.row]
                    subscriptionCell.redIconImage.isHidden = false
                    return subscriptionCell
                } else {
                    return UITableViewCell()
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if dataList.count == 0 {
            AddNewSubscription()
        } else {
            if indexPath.row == dataList.count {
                AddNewSubscription()
            }
        }
    }
}

extension SubscriptionViewController: AddNewSubscriptionProtocol {
    func AddNewSubscription() {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddSubscriptionViewController") as! AddSubscriptionViewController
        vc.hiddenNavigationBar(isHidden: true)
        vc.custom = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
}


extension SubscriptionViewController: TapOneButtonProtocol { 
    func tapAction(tagNumber: Int) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditSubscriptionViewController") as! EditSubscriptionViewController
        vc.hiddenNavigationBar(isHidden: true)
        vc.dataList = dataList 
        self.navigationController?.pushViewController(vc, animated: true)
    }
}


extension SubscriptionViewController: ShowMessageAlertDelegate {
    func messageTapAction() {
        self.dismiss(animated: true, completion: nil)
    }
}
