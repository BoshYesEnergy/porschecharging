//
//  EditSubscriptionViewController.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/22.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class EditSubscriptionViewController: BaseViewController {
    
    @IBOutlet weak var editSubTable: UITableView!
    var dataList: [PorscheCarListModel.Infos]?
    fileprivate var saveList: [Int] = []
     
    override func viewDidLoad() {
        super.viewDidLoad()
        setHeaderView(LocalizeUtils.localized(key: "EditSubscription"), action: .POP)
        setTable()
    }
    
    fileprivate func setTable() {
        editSubTable.register(UINib(nibName: "RemoveSubscriptionCellTableViewCell", bundle: nil), forCellReuseIdentifier: "RemoveSubscriptionCellTableViewCell")
        editSubTable.register(UINib(nibName: "SubscriptionCell", bundle: nil), forCellReuseIdentifier: "SubscriptionCell")
    }
}


extension EditSubscriptionViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (dataList?.count ?? 0) + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == (dataList?.count ?? 0) {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "RemoveSubscriptionCellTableViewCell", for: indexPath) as? RemoveSubscriptionCellTableViewCell {
                cell.delegate = self
                cell.setUI(onOff: saveList.count != 0 )
                return cell
            } else { return UITableViewCell() }
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "SubscriptionCell", for: indexPath) as? SubscriptionCell {
                cell.index = indexPath.row
                cell.data = dataList?[indexPath.row]
                cell.delegte = self
                cell.iconBtn.isHidden = false 
                cell.checkStatus(selectSingle: saveList.contains(indexPath.row))
                return cell
            } else { return UITableViewCell() }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}


extension EditSubscriptionViewController: SubscriptionProtocol, RemoveSubscription {
    func tapCheck(index: Int) {
        guard (dataList?[index]) != nil else { return }
        if saveList.contains(index) == false {
            saveList.append(index)
        } else {
            let newlist = saveList.filter{ $0 != index }
            saveList.removeAll()
            saveList = newlist
        }
        editSubTable.reloadData()
    }
    
    func remove() { 
        for i in saveList {
            KJLRequest.requestForSubscriptionCar(email: dataList?[i].vin ?? "", cardNum: dataList?[i].cardNum ?? "" , bind: false) { (response) in
                guard let _ = response as? SubscriptionCarModel else { return }
                KJLRequest.requestForProfile {(response) in
                    guard let data = response as? ProfileClass else { return }
                    KJLRequest.shareInstance().profile = data
                    self.navigationController?.popToRootViewController(animated: true)
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        NotificationCenter.default.post(name: .reloadSubscription, object: nil)
                    }
                } 
            }
        }
        
    }
}
