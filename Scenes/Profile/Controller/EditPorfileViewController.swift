//
//  EditPorfileViewController.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/21.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit


///
///
///  2020/11/5 cancel the viewcontroller,
///  will app on online ,
///  please delete this viewcontroller and cell
///
///

class EditPorfileViewController: BaseViewController {
    
    @IBOutlet weak var showSecurityBtn: UIButton!
    @IBOutlet weak var securityTable: UITableView!
    @IBOutlet weak var securityIcon: UIImageView!
    @IBOutlet weak var logoutIcon: UIImageView! 
    @IBOutlet weak var showLogoutBtn: UIButton!
    @IBOutlet weak var logoutBtn: UIButton!
    
    @IBOutlet weak var securityTableHeight: NSLayoutConstraint!
    @IBOutlet weak var contentHeight: NSLayoutConstraint!
    @IBOutlet weak var logoutHeight: NSLayoutConstraint!
    
    var profileData: ProfileClass?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        callProfile()
        setTable()
        setHeaderView("Edit Profile", action: .POP)
        setView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
}


//MARK: Setting View
extension EditPorfileViewController {
    fileprivate func setView() {
        logoutBtn.layer.borderColor  = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        logoutBtn.layer.borderWidth  = 1
        securityTableHeight.constant = self.view.frame.height / 1.5
        contentHeight.constant       = 0
        logoutHeight.constant        = 0
        logoutBtn.isHidden           = true
        securityIcon.image           =  #imageLiteral(resourceName: "icon_arrow_down_default")
        // default show table
        showSecurityBtn.isSelected   = true
    }
    
    fileprivate func setTable() {
        securityTable.isHidden = false
        securityTable.register(UINib(nibName: "SecurityCell", bundle: nil), forCellReuseIdentifier: "SecurityCell")
        securityTable.register(UINib(nibName: "SecurityDetailCell", bundle: nil), forCellReuseIdentifier: "SecurityDetailCell")
    }
}


//MARK: Action
extension EditPorfileViewController {
    @IBAction func showSecurityTable(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        let constant = self.view.frame.height / 1.5
        UIView.animate(withDuration: 0.3) {
            self.securityIcon.image = sender.isSelected == true ? #imageLiteral(resourceName: "icon_arrow_down_default") : #imageLiteral(resourceName: "icon_arrow_up_default")
            self.securityTableHeight.constant = sender.isSelected == true ? constant : 0
            self.securityTable.isHidden = sender.isSelected == true ? false : true
            self.view.layoutIfNeeded()
        } 
    }
    
    @IBAction func showLogoutView(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        UIView.animate(withDuration: 0.3) {
            self.logoutIcon.image = sender.isSelected == true ? #imageLiteral(resourceName: "icon_arrow_down_default") : #imageLiteral(resourceName: "icon_arrow_up_default")
            self.contentHeight.constant = sender.isSelected == true ? 50 : 0
            self.logoutHeight.constant = sender.isSelected == true ? 48 : 0
            self.logoutBtn.isHidden = sender.isSelected == true ? false : true
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func logout(_ sender: UIButton) {
        logoutAction()
    }
    
    func selectSourceType(_ sourceType: UIImagePickerController.SourceType) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.allowsEditing = true
            myPickerController.sourceType = sourceType
            myPickerController.modalPresentationStyle = .fullScreen
            present(myPickerController, animated: true, completion: nil)
        }
    }
}


//MARK: Api
extension EditPorfileViewController {
    fileprivate func callProfile() {
        KJLRequest.requestForProfile {[weak self] (response) in
            guard let data = response as? ProfileClass else { return }
            KJLRequest.shareInstance().profile = data
            self?.profileData = nil
            self?.profileData = data
            self?.securityTable.reloadData()
        }
    }
    
    fileprivate func logoutAction() { 
        present(.confirmAlert("Log Out Alert", "", "Log Out", {
            UIViewController.pushLogOut()
            KJLRequest.shareInstance().loginMethod = .none
            KJLRequest.logout()
            UIApplication.shared.applicationIconBadgeNumber = 0
            UserDefaults.standard.set(0, forKey: "UnReadCount")
            NotificationCenter.default.post(name: .changeScanTabImage, object: nil)
            self.dismiss(animated: true, completion: nil)
        }, "Cancel", { }), animated: true, completion: nil)
    }
    
    /// Update user image
    fileprivate func updateUserImage(image: UIImage?) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            let str = KJLCommon.convertImageToBase64(image: image ?? UIImage())
            KJLRequest.requestForUploadMemberPic(picBytes: str) {[weak self] (response) in
                guard response != nil else { return }
                self?.callProfile()
            }
        }
    }
    
    fileprivate func updateEditProfile(name: String, backupEmail: String) {
        KJLRequest.requestForEditProfile(nickName: name, sex: "", carNo: "", backupEmail: backupEmail) { (response) in
            if response != nil {
                self.callProfile()
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}


//MARK: Table view delegate & data source
extension EditPorfileViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            if let securityCell = tableView.dequeueReusableCell(withIdentifier: "SecurityCell", for: indexPath) as? SecurityCell {
                securityCell.delegate = self
                securityCell.data = profileData
                return securityCell
            } else {
                return UITableViewCell()
            }
        } else if indexPath.row == 1 {
            if let securityDetailCell = tableView.dequeueReusableCell(withIdentifier: "SecurityDetailCell", for: indexPath) as? SecurityDetailCell {
                securityDetailCell.data = profileData
                securityDetailCell.delegate = self
                return securityDetailCell
            } else {
                return UITableViewCell()
            }
        } else {
            return UITableViewCell()
        } 
    }
}


//MARK: Protocol
extension EditPorfileViewController: POPImagePickProtocol, SecurityEditProfile {
    func popEditView() {
        
    }
    
    func updateProfile(name: String, backUpEmail: String) {
        updateEditProfile(name: name, backupEmail: backUpEmail)
    }
    
    func backToProfile() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func popImagePick() {
        let control = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let ac1 = UIAlertAction(title: "從相簿選取", style: .default, handler: { (alertAction) in
            self.selectSourceType(.photoLibrary)
        })
         
        let ac2 = UIAlertAction(title: "開啟相機", style: .default, handler: { (alertAction) in
            if KJLCommon.authorizeForCamera() == true {
                self.selectSourceType(.camera)
            }
        })
        
        let ac3 = UIAlertAction(title: "預設大頭貼", style: .default, handler: { (alertAction) in
            self.updateUserImage(image: KJLCommon.defaultImage2())
        })
        
        let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil) 
        control.addAction(ac1)
        control.addAction(ac2)
        control.addAction(ac3)
        control.addAction(cancel)
        self.present(control, animated: true, completion: nil)
    }
}


//MARK: Image Picker
extension EditPorfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            if let data = UIImageJPEGRepresentation(image, 0.5) {
                updateUserImage(image: UIImage(data: data))
            } else {
                updateUserImage(image: image)
            }
        } else {
            print("Something went wrong")
        }
        dismiss(animated: true, completion: nil)
    }
}
