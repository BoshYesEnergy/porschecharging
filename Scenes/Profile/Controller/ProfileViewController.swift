//
//  ProfileViewController.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/19.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class ProfileViewController: BaseViewController {
    
    @IBOutlet weak var classCollection: UICollectionView!
    @IBOutlet weak var leftBtn: UIButton!
    @IBOutlet weak var rightBtn: UIButton!
    @IBOutlet weak var childView: UIView!
    
    let stringArray = [LocalizeUtils.localized(key: "ProfileAccount"),
                       LocalizeUtils.localized(key: "Subscription"),
                       LocalizeUtils.localized(key: "Payment"),
                       LocalizeUtils.localized(key: "Partner"),
                       LocalizeUtils.localized(key: "Voucher"),
                       LocalizeUtils.localized(key: "ChargingHistory")]
    
    private lazy var detail = ProfileDetailViewController()
    private lazy var subscription = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SubscriptionViewController") as! SubscriptionViewController
    private var payment  = PaymentViewController()
    private var Partner  = PartnerViewController()
    private var vourcher = VourcherViewController()
    private var histroy  = ChargingHistroyListVC()
    
    private var addSubscription = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddSubscriptionViewController") as! AddSubscriptionViewController
    
    var selectPage: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setHeaderView(LocalizeUtils.localized(key: "Profile"), action: .DISMISS) 
        setToolButton(nil, UIImage(named: ToolPhoto.inbox_default.rawValue), UIImage(named: ToolPhoto.inbox_click.rawValue)) {
            self.popInBox()
        } 
        // add first child viewcontroller
        self.add(asChildViewController: detail)
        self.detail.delegate = self
        // 指定選擇item
        classCollection.selectItem(at: IndexPath(item: 0, section: 0), animated: false, scrollPosition: .centeredHorizontally)
        leftBtn.setImage(UIImage(named: "icon_previous_click"), for: .highlighted)
        rightBtn.setImage(UIImage(named: "icon_next_click"), for: .highlighted)
        leftBtn.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func popInBox() {
        let vc = InBoxVC(nibName: "InBoxVC", bundle: nil)
        let nav = UINavigationController(rootViewController: vc)
        nav.modalPresentationStyle = .overFullScreen
        present(nav, animated: true, completion: nil)
    }
}


//MARK: Set View
extension ProfileViewController {
    fileprivate func changePage(page: Int) {
        selectPage = page
        classCollection.reloadData()
        
        classCollection.selectItem(at: IndexPath(item: page, section: 0), animated: true, scrollPosition: .centeredHorizontally)
        addVC(IndexPath(item: page, section: 0))
        leftBtn.isHidden = selectPage == 0 ? true : false
        rightBtn.isHidden = selectPage == 5 ? true : false
    }
     
    fileprivate func addVC(_ indexPath: IndexPath) {
        if indexPath.item == 0 {
            self.add(asChildViewController: detail)
            remove(asChildViewController: subscription)
            remove(asChildViewController: payment)
            remove(asChildViewController: vourcher)
            remove(asChildViewController: Partner)
            remove(asChildViewController: histroy)
            remove(asChildViewController: addSubscription)
        } else if indexPath.item == 1 {
            if KJLRequest.shareInstance().profile?.datas?.subscription == true {
                subscription.delegate = self
                self.add(asChildViewController: subscription)
                remove(asChildViewController: addSubscription)
                remove(asChildViewController: detail)
                remove(asChildViewController: payment)
                remove(asChildViewController: vourcher)
                remove(asChildViewController: Partner)
                remove(asChildViewController: histroy)
            } else {
                UserDefaults.standard.set( "", forKey: "subEmail")
                UserDefaults.standard.set( "", forKey: "subCardNumber")
                addSubscription.custom = true
                addSubscription.headerView.isHidden = true
                addSubscription.garyLine.isHidden = true
                addSubscription.hiddenNavigationBar(isHidden: true)
                addSubscription.delegate = self 
                self.add(asChildViewController: addSubscription)
                remove(asChildViewController: subscription)
                remove(asChildViewController: detail)
                remove(asChildViewController: payment)
                remove(asChildViewController: vourcher)
                remove(asChildViewController: Partner)
                remove(asChildViewController: histroy)
            }
        } else if indexPath.item == 2 {
            payment.whereTo = .Profile
            payment.delegate = self
            self.add(asChildViewController: payment)
            remove(asChildViewController: detail)
            remove(asChildViewController: subscription)
            remove(asChildViewController: vourcher)
            remove(asChildViewController: Partner)
            remove(asChildViewController: histroy)
            remove(asChildViewController: addSubscription)
        } else if indexPath.item == 3 {
            Partner.stopTimer()
            Partner.delegate = self
            self.add(asChildViewController: Partner)
            remove(asChildViewController: detail)
            remove(asChildViewController: subscription)
            remove(asChildViewController: payment)
            remove(asChildViewController: vourcher)
            remove(asChildViewController: histroy)
            remove(asChildViewController: addSubscription)
        } else if indexPath.item == 4 {
            self.add(asChildViewController: vourcher)
            remove(asChildViewController: detail)
            remove(asChildViewController: subscription)
            remove(asChildViewController: payment)
            remove(asChildViewController: Partner)
            remove(asChildViewController: histroy)
            remove(asChildViewController: addSubscription)
        } else if indexPath.item == 5 {
            self.add(asChildViewController: histroy)
            remove(asChildViewController: detail)
            remove(asChildViewController: subscription)
            remove(asChildViewController: payment)
            remove(asChildViewController: Partner)
            remove(asChildViewController: vourcher)
            remove(asChildViewController: addSubscription)
        }
    }
}


//MARK: Action
extension ProfileViewController {
    @IBAction func backPage(_ sender: UIButton) { // 0
        if selectPage != 0 {
            selectPage -= 1
            changePage(page: selectPage)
        }
    }
    
    @IBAction func nextPage(_ sender: UIButton) { // 5
        if selectPage != 5 {
            selectPage += 1
            changePage(page: selectPage)
        }
    }
}


//MARK: Api
extension ProfileViewController {
    private func callProfile(page: Int) {
        KJLRequest.requestForProfile {[weak self](response) in
            guard let data = response as? ProfileClass else { return }
            KJLRequest.shareInstance().profile = data
            DispatchQueue.main.async {
                self?.changePage(page: page)
            }
        }
    }
}


//MARK: Add Child
extension ProfileViewController {
    private func add(asChildViewController viewController: UIViewController) {
        // Add Child View Controller
        addChildViewController(viewController)
        viewController.beginAppearanceTransition(true, animated: true)
        self.childView.addSubview(viewController.view)
        viewController.endAppearanceTransition()
        viewController.view.frame = self.childView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParentViewController: self)
    }
    
    private func remove(asChildViewController viewController: UIViewController) {
        viewController.willMove(toParentViewController: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParentViewController()
    }
}


//MARK: collection delegate & datasource
extension ProfileViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return stringArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? ProfileClassCell {
            cell.textString = stringArray[indexPath.item]
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        changePage(page: indexPath.item)
    }
}


//MARK:- Collection Delegate Flow Layout
extension ProfileViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if KJLRequest.shareInstance().language == "en" {
            if indexPath.item == 0 {
                return CGSize(width: 80, height: 53)
            } else if indexPath.item == 1 {
                return CGSize(width: 125, height: 53)
            } else if indexPath.item == 2 {
                return CGSize(width: 90, height: 53)
            } else  if indexPath.item == 3 ||  indexPath.item == 4 {
                return CGSize(width: 85, height: 53)
            } else {
                return CGSize(width: 140, height: 53)
            }
        } else {
            return CGSize(width: 80, height: 53)
        }
    }
}


//MARK: Protocol
extension ProfileViewController: ProfileDetailDelegate {
    func gotoActivate() {
        changePage(page: 1)
    }
    
    func gotoAddPartner() {
        changePage(page: 3)
    }
}


//MARK: Protocol - Subscription Reload UI
extension ProfileViewController: SubscriptionReloadDelegate {
    func subReload() { 
        callProfile(page: 0)
    }
}


//MARK: Protocol - Partner Reload UI
extension ProfileViewController: PartnerDelegate {
    func reloadPartnerViewcontroller() {
        callProfile(page: 3)
    }
}
