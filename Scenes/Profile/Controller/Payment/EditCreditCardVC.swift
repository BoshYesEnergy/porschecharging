//
//  EditCreditCardVC.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/25.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class EditCreditCardVC: BaseViewController {
    
    @IBOutlet weak var editTable: UITableView!
    
    var lists: [CardListDataModel.Data] = []
    var deleteArray: [Int] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setHeaderView(LocalizeUtils.localized(key: "EditPayment"), action: .POP)
        editTable.register(UINib(nibName: "CreditCardListCell", bundle: nil), forCellReuseIdentifier: "CreditCardListCell")
        editTable.register(UINib(nibName: "RemoveEditCreditCell", bundle: nil), forCellReuseIdentifier: "RemoveEditCreditCell")
        callApiCardList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(callApiDeleteCard), name: .apiDeleteCard, object: nil)
    }
}


//MARK: Api
extension EditCreditCardVC {
    fileprivate func callApiCardList() {
        KJLRequest.requestForCardListData { (response) in
            guard let response = response as? CardListDataModel else { return }
            self.lists.removeAll()
            self.lists = response.datas!
            self.editTable.reloadData()
        }
    }
    
    @objc fileprivate func callApiDeleteCard() {
        for index in deleteArray {
            let cardNo = "\(lists[index].cardNo ?? 0)"
            KJLRequest.requestForDeleteCard(cardNo: cardNo) { (response) in
                guard response != nil else { return}
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
}


//MARK: Table view delegate & data source
extension EditCreditCardVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lists.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == lists.count {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "RemoveEditCreditCell", for: indexPath) as? RemoveEditCreditCell {
                cell.delegate = self
                cell.tapOff = deleteArray.count != 0
                cell.removeBtn.setTitle(LocalizeUtils.localized(key: Language.Remove.rawValue), for: .normal)
                return cell
            } else {
                return UITableViewCell()
            }
        } else {
            if let creditCardListCell = tableView.dequeueReusableCell(withIdentifier: "CreditCardListCell", for: indexPath) as? CreditCardListCell {
                print("indexPath: -- \(indexPath.row)")
                creditCardListCell.list = lists[indexPath.row]
                creditCardListCell.checkBtn.isHidden = false
                creditCardListCell.index = indexPath.row
                creditCardListCell.delegate = self
                return creditCardListCell
            } else {
                return UITableViewCell()
            }
        }
    }
}


//MARK: Protocol
extension EditCreditCardVC: RemoveEditCreditProtocol, CreditCardListProtocol {
    func appendToRemoveList(index: Int) {
        if deleteArray.contains(index) == false {
            deleteArray.append(index)
            editTable.reloadData()
            print("deleteArray: \(deleteArray)")
        } else {
            let newList = deleteArray.filter{ $0 != index }
            deleteArray.removeAll()
            deleteArray = newList
            editTable.reloadData()
            print("deleteArray: \(deleteArray)")
        }
    } 
    
    func checkRemove() {
        let vc = ShowInformationVC()
        vc.modalPresentationStyle   = .overFullScreen
        vc.modalTransitionStyle     = .crossDissolve
        vc.cancelIsHidden           = false
        vc.titleString              = LocalizeUtils.localized(key: Language.Remove.rawValue)
        vc.contentString            = LocalizeUtils.localized(key: "PaymentRemoveContent")
        self.present(vc, animated: true, completion: nil)
    } 
}


