//
//  EInvoiceSetViewController.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/28.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class EInvoiceSetViewController: BaseViewController {
    
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var loginMethed: UILabel!
    @IBOutlet weak var backupView: UIView!
    @IBOutlet weak var backupTextfield: UITextField!
    @IBOutlet weak var codeView: UIView!
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var companyNameView: UIView!
    @IBOutlet weak var nameTextfield: UITextField!
    @IBOutlet weak var companyNumberView: UIView!
    @IBOutlet weak var numberTextField: UITextField!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setHeaderView("e-Invoice Settings", action: .POP)
        setView()
        callApiInvoiceSettingList() 
    }
}


//MARK: Setting View
extension EInvoiceSetViewController {
    fileprivate func setView() {
        setBorder(backupView)
        setBorder(codeView)
        setBorder(companyNameView)
        setBorder(companyNumberView)
        setBorder(cancelBtn)
        email.text = KJLRequest.shareInstance().profile?.datas?.mails?[0].email ?? ""
        loginMethed.text = UserDefaults.standard.string(forKey: "loginMethod") ?? ""
    }
    
    fileprivate func setBorder(_ view: UIView) {
        view.layer.borderWidth = 1
        view.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
}


//MARK: Api
extension EInvoiceSetViewController {
    fileprivate func callApiInvoiceSettingList() {
        KJLRequest.requestForInvoiceSettingList { (response) in
            guard let data = response as? InvoiceSettingListModel,
                  let datas = data.datas
            else { return }
            self.backupTextfield.text = datas.backupEmail ?? ""
            self.codeTextField.text   = datas.vehicleNum ?? ""
            self.nameTextfield.text   = datas.taxidName ?? ""
            self.numberTextField.text = datas.taxidNum ?? ""
        }
    }
    
    fileprivate func callApiSetInvoice() {
        KJLRequest.requestForSetInvoiceSettings(backupTextfield.text ?? "", codeTextField.text ?? "", nameTextfield.text ?? "", numberTextField.text ?? "") { (_) in
//            guard let _ = response else { return }
            self.navigationController?.popViewController(animated: true)
        }
    }
}


//MARK: Action
extension EInvoiceSetViewController {
    @IBAction func save(_ sender: UIButton) {
        callApiSetInvoice()
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}


//MARK: Text field delegate
extension EInvoiceSetViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let count = text.count + string.count - range.length
        if textField == numberTextField {
            return count > 8 ? false : true 
        }
        return true
    }
}
