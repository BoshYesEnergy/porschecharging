//
//  AddCreditCardViewController.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/23.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class AddCreditCardViewController: BaseViewController {

    @IBOutlet weak var paymentTitle: UILabel!
    @IBOutlet weak var cardNumberTitle: UILabel!
    @IBOutlet weak var expiryDateTitle: UILabel!
    @IBOutlet weak var securtyTitle: UILabel!
    @IBOutlet weak var content: UILabel!
    
    @IBOutlet weak var cardNumberView: UIView!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var securityView: UIView!
    
    @IBOutlet weak var cardNumberTextField: UITextField!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var securityTextField: UITextField!
    @IBOutlet weak var checkBtn: UIButton!
    @IBOutlet weak var confirmBtn: UIButton!
    
    var whereTo: WhereToPayment = .Profile
    
    /// change payment
    var lists: [CardListDataModel.Data] = []
    var status: PaymentStatus = .AddNewPayment
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setHeaderView(LocalizeUtils.localized(key: "AddCreditCard"), action: .POP)
        setBorderView(view: cardNumberView)
        setBorderView(view: dateView)
        setBorderView(view: securityView)
        KJLRequest.shareInstance().kJLRequestDelegate = self
        confirmBtn.isEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setlanguage()
        NotificationCenter.default.addObserver(self, selector: #selector(popToRootVC), name: .popToProfile, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(checkTextData(obj:)), name: .UITextFieldTextDidChange, object: securityTextField)
        NotificationCenter.default.addObserver(self, selector: #selector(checkCardNumber(obj:)), name: .UITextFieldTextDidChange, object: cardNumberTextField)
        NotificationCenter.default.addObserver(self, selector: #selector(checkDate(obj:)), name: .UITextFieldTextDidChange, object: dateTextField)
    }
    
    func setlanguage() {
        paymentTitle.text = LocalizeUtils.localized(key: "PaymentAdd")
        cardNumberTitle.text = LocalizeUtils.localized(key: "Card")
        expiryDateTitle.text = LocalizeUtils.localized(key: "PaymentExpiry")
        securtyTitle.text = LocalizeUtils.localized(key: "PaymentSecurity")
        content.text = LocalizeUtils.localized(key: "PaymentContent")
        confirmBtn.setTitle(LocalizeUtils.localized(key: "Confirm"), for: .normal)
        
        cardNumberTextField.placeholder = LocalizeUtils.localized(key: "YourCardnumnber")
        securityTextField.placeholder = LocalizeUtils.localized(key: "CVCorCVV")
        
    }
    
    fileprivate func setBorderView(view: UIView) {
        view.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        view.layer.borderWidth = 1
    }
    
    @objc fileprivate func popToRootVC() {
        guard let vc = self.navigationController?.viewControllers[0] else { return }
        self.navigationController?.popToViewController(vc, animated: true)
    }
    
    fileprivate func showInfoMessage(_ toContent: String, actionStatus: InfoActionStatus = .Back) {
        let vc = ShowInformationVC()
        vc.titleString   = LocalizeUtils.localized(key: "PaymentInfo")
        vc.contentString = toContent
        vc.actionStataus = actionStatus
        vc.custom        = .ONE
        vc.oneBtnTitle   = LocalizeUtils.localized(key: "Confirm")
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc fileprivate func checkCardNumber(obj: Notification) {
        if cardNumberTextField.text?.count ?? 0 < 16 {
            cardNumberView.layer.borderColor = #colorLiteral(red: 0.8352941176, green: 0, blue: 0.1098039216, alpha: 1)
        } else {
            cardNumberView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
    }
    
    @objc fileprivate func checkDate(obj: Notification) {
        if cardNumberTextField.text?.count ?? 0 < 16 {
            cardNumberView.layer.borderColor = #colorLiteral(red: 0.8352941176, green: 0, blue: 0.1098039216, alpha: 1)
        } else {
            cardNumberView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
        
        if dateTextField.text?.count ?? 0 < 5 {
            dateView.layer.borderColor = #colorLiteral(red: 0.8352941176, green: 0, blue: 0.1098039216, alpha: 1)
        } else {
            dateView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
    }
    
    @objc fileprivate func checkTextData(obj: Notification) {
        if cardNumberTextField.text?.count ?? 0 < 16 {
            cardNumberView.layer.borderColor = #colorLiteral(red: 0.8352941176, green: 0, blue: 0.1098039216, alpha: 1)
        } else {
            cardNumberView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
        
        if dateTextField.text?.count ?? 0 < 5 {
            dateView.layer.borderColor = #colorLiteral(red: 0.8352941176, green: 0, blue: 0.1098039216, alpha: 1)
        } else {
            dateView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
        
        if securityTextField.text?.count ?? 0 < 3 {
            securityView.layer.borderColor = #colorLiteral(red: 0.8352941176, green: 0, blue: 0.1098039216, alpha: 1)
        } else {
            securityView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
    }
    
    @IBAction func check(_ sender: UIButton) {
        if cardNumberTextField.text?.count ?? 0 < 16 {
            return
        }
        
        if dateTextField.text?.count ?? 0 < 5 {
            return
        }
        
        if securityTextField.text?.count ?? 0 < 3 {
            return
        }
        
        // change UI
        sender.isSelected = !sender.isSelected
        let image = sender.isSelected == true ? UIImage(named: "icon_check_default") : UIImage(named: "icon_uncheck_disable")
        checkBtn.setImage(image, for: .normal)
        confirmBtn.backgroundColor = sender.isSelected == true ? #colorLiteral(red: 0.8352941176, green: 0, blue: 0.1098039216, alpha: 1) : #colorLiteral(red: 0.7882352941, green: 0.7921568627, blue: 0.7960784314, alpha: 1)
        confirmBtn.isEnabled = sender.isSelected == true ? true : false
        view.endEditing(true)
    }
    

    @IBAction func confirm(_ sender: UIButton) {
        if status == .AddNewPayment {
            callApiAddCard() {
                if self.whereTo == .Subscription {
                    let email   = UserDefaults.standard.string(forKey: "subEmail") ?? ""
                    let cardNum =  UserDefaults.standard.string(forKey: "subCardNumber") ?? ""
                    KJLRequest.requestForSubscriptionCar(email: email, cardNum: cardNum, bind: true) { (response) in
                        guard let data = response as? SubscriptionCarModel else { return }
                        if data.resCode != "0000" {
                            self.popErrorMessage(reMsg: data.resMsg ?? "")
                        } else {
//                            /// remove sub
//                            UserDefaults.standard.set("", forKey: "subEmail")
//                            UserDefaults.standard.set("", forKey: "subCardNumber")
//                            self.navigationController?.popToRootViewController(animated: true)
//                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
//                                NotificationCenter.default.post(name: .reloadSubscription, object: nil)
//                            }
                            
                            let vc = EInvoiceEditViewController(nibName: "EInvoiceEditViewController", bundle: nil)
                            vc.hiddenNavigationBar(isHidden: true)
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                }
                
            }
        } else {
            callApiAddCard() {
                self.callApiDeleteCard(cardNo: "\(self.lists[0].cardNo ?? 0)")
            }
        }
        
    }
    
    fileprivate func callApiDeleteCard(cardNo: String) {
        KJLRequest.requestForDeleteCard(cardNo: cardNo) { (response) in
            guard response != nil else { return}
            
            let vc = EInvoiceEditViewController(nibName: "EInvoiceEditViewController", bundle: nil)
            vc.hiddenNavigationBar(isHidden: true)
            self.navigationController?.pushViewController(vc, animated: true)
            
//            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
//                self.navigationController?.popToRootViewController(animated: true)
//            }
        }
    }
    
    fileprivate func popErrorMessage(reMsg: String) {
        let vc = ShowMessageAlertVC()
        vc.stringToContent = reMsg
        vc.titleToBtn = LocalizeUtils.localized(key: "Confirm")
        vc.delegate = self
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        present(vc, animated: true) { }
    }
}


//MARK: Api
extension AddCreditCardViewController {
    func callApiAddCard(completion: @escaping ()-> Void) {
        let cardNumber = cardNumberTextField.text?.replacingOccurrences(of: "-", with: "").replacingOccurrences(of: " ", with: "") ?? ""
        let date = dateTextField.text?.replacingOccurrences(of: "/", with: "") ?? ""
        let security = securityTextField.text?.replacingOccurrences(of: " ", with: "") ?? ""
        KJLRequest.requestForAddCard(cardNum: cardNumber, expiry: date, checkCode: security) { (response) in
            guard let response = response as? NormalClass else { return }
            if response.resCode != "0000" {
                // response.resMsg ?? ""
                if self.whereTo == .Subscription {
                    self.showInfoMessage(response.resMsg ?? "", actionStatus: .None)
                } else {
                    self.showInfoMessage(response.resMsg ?? "")
                }
            } else {
                if self.status == .AddNewPayment {
                    self.showInfoMessage(LocalizeUtils.localized(key: "PaymentSuccessfully"))
                } else {
                    self.showInfoMessage(LocalizeUtils.localized(key: "ChangePaymentSuccessfully"))
                }
                completion()
            }
            
        }
    }
}


//MARK: textfiled delegate
extension AddCreditCardViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let count = text.count + string.count - range.length
        if textField == cardNumberTextField {
            cardNumberView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            return count > 16 ? false : true
        }
        
        if textField == dateTextField {
            if cardNumberTextField.text?.count ?? 0 < 16 {
                cardNumberView.layer.borderColor = #colorLiteral(red: 0.8352941176, green: 0, blue: 0.1098039216, alpha: 1)
            }
            if count == 3 && dateTextField.text?.contains("/") == false {
                dateTextField.text = text + "/" + string
                return false
            }
            dateView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            return count > 5 ? false : true
        }
        
        if textField == securityTextField {
            if dateTextField.text?.count ?? 0 < 5 {
                dateView.layer.borderColor = #colorLiteral(red: 0.8352941176, green: 0, blue: 0.1098039216, alpha: 1)
            }
            return count > 3 ? false : true
        }
        
        return true
    }
}


//MARK: Protocol
extension AddCreditCardViewController: KJLRequestDelegate {
    func apiFailResultWith(resCode: String, message: String) {
        
    }
}


//MARK: Protocol Show Message Alert
extension AddCreditCardViewController: ShowMessageAlertDelegate {
    func messageTapAction() {
        self.dismiss(animated: true, completion: nil)
    }
}
