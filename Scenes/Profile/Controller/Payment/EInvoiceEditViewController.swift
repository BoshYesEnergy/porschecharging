//
//  EInvoiceEditViewController.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/23.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class EInvoiceEditViewController: BaseViewController {

    @IBOutlet weak var einvoiceEidtTable: UITableView!
    var invoiceList: InvoiceSettingListModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setHeaderView(LocalizeUtils.localized(key: "e-Invoice"), action: .POP)
        hiddenNavigationBar(isHidden: true)
        einvoiceEidtTable.register(UINib(nibName: "AddEInvoiceChildCell", bundle: nil), forCellReuseIdentifier: "AddEInvoiceChildCell")
        einvoiceEidtTable.register(UINib(nibName: "TapOneButtonCell", bundle: nil), forCellReuseIdentifier: "TapOneButtonCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.callProfile {
            self.callApiInvoiceSettingList()
        }
    }
    
    func getBackupEmail() -> String {
        /// check backup email
        var backupEmail = ""
        if let mailList = KJLRequest.shareInstance().profile?.datas?.mails {
            for i in mailList {
                if i.source?.lowercased() == "backup" {
                    backupEmail = i.email ?? ""
                }
            }
        }
        return backupEmail
    }
}


//MARK: Api
extension EInvoiceEditViewController {
    fileprivate func callProfile(completion: (()->())? ) {
        KJLRequest.requestForProfile { (response) in
            guard let data = response as? ProfileClass else { return }
            KJLRequest.shareInstance().profile = data
            if let callback = completion {
                callback()
            }
        }
    }
    
    fileprivate func callApiInvoiceSettingList() {
        KJLRequest.requestForInvoiceSettingList { (response) in
            guard let data = response as? InvoiceSettingListModel else {  return }
            if data.resCode == "9000" {
                KJLRequest.requestForSetInvoiceSettings("", "" , "",  "") { (_) in }
            } else {
                self.invoiceList = data
                self.einvoiceEidtTable.reloadData()
            }
           
        }
    }
}


//MARK: Table view delegate & data source
extension EInvoiceEditViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1 + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "AddEInvoiceChildCell", for: indexPath) as? AddEInvoiceChildCell {
                cell.delegate    = self
                cell.invoiceList = invoiceList
                return cell
            } else {
                return UITableViewCell()
            }
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "TapOneButtonCell", for: indexPath) as? TapOneButtonCell {
                cell.setView(custom: false)
                cell.tapBtn.setTitle(LocalizeUtils.localized(key: Language.Confirm.rawValue), for: .normal)
                cell.delegate = self
                return cell
            } else {
                return UITableViewCell()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}


//MARK: protocol
extension EInvoiceEditViewController: AddEInvoiceDelegate {
    func editBackUpEmail() {
        let vc = ShowTextFieldInformationVC()
        vc.titlName    = LocalizeUtils.localized(key: "EditBackupEmail")
        vc.placeholder = LocalizeUtils.localized(key: "EnterEmail")
        
        let backupEmail = getBackupEmail()
        
        vc.content     = backupEmail
        vc.delegate    = self
        vc.delegateTag = 1
        vc.constraintText = 99
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        present(vc, animated: true, completion: nil)
    }
    
    func editCode() {
        let vc = ShowTextFieldInformationVC()
        vc.titlName    = LocalizeUtils.localized(key: "EditCloudinvoice")
        vc.placeholder = LocalizeUtils.localized(key: "EnterCloudinvoice")
        vc.content     = invoiceList?.datas?.vehicleNum ?? ""
        vc.delegate    = self
        vc.delegateTag = 2
        vc.constraintText = 8
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        present(vc, animated: true, completion: nil)
    }
    
    func editcompany() {
        let vc = ShowTwoTextFieldInformationVC()
        vc.stringToTitle = LocalizeUtils.localized(key: "EditCompany")
        vc.delegate    = self
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        present(vc, animated: true, completion: nil)
    }
    
    func updateSelectArea(useBackup: Bool, invoiceType: String, vehicleType: String) {
        KJLRequest.requestForEditInvoiceSetting(useBackup, invoiceType, vehicleType) { (response) in
            guard let _ = response else { return }
            self.callApiInvoiceSettingList()
        }
    }
}


extension EInvoiceEditViewController: ShowTextFieldInformationDelegate {
    func tapConfirm(text: String, delegateTag: Int) {
        if delegateTag == 1 {
            print(text)
            KJLRequest.requestForSetInvoiceSettings(text, invoiceList?.datas?.vehicleNum ?? "" , invoiceList?.datas?.taxidName ?? "", invoiceList?.datas?.taxidNum ?? "") { (_) in
    //            guard let _ = response else { return }
                self.dismiss(animated: true) {
                    self.callProfile {
                        self.callApiInvoiceSettingList()
                    }
                }
            }
        } else if delegateTag == 2 {
            print(text)
            let backupEmail = getBackupEmail()
            KJLRequest.requestForSetInvoiceSettings(backupEmail, text , invoiceList?.datas?.taxidName ?? "", invoiceList?.datas?.taxidNum ?? "") { (_) in
    //            guard let _ = response else { return }
                self.dismiss(animated: true) {
                    self.callProfile {
                        self.callApiInvoiceSettingList()
                    }
                }
            }
        }
    }
    
    func tapCancel() {
        self.dismiss(animated: true, completion: nil)
    }
}


extension EInvoiceEditViewController: TwoTextFieldDelegate {
    func confirm(first: String, second: String) {
        let backupEmail = getBackupEmail()
        KJLRequest.requestForSetInvoiceSettings(backupEmail, invoiceList?.datas?.vehicleNum ?? "" , first, second) { (_) in
//            guard let _ = response else { return }
            self.dismiss(animated: true) {
                self.callProfile {
                    self.callApiInvoiceSettingList()
                }
            }
        }
    }
    
    func cancel() {
        self.dismiss(animated: true, completion: nil)
    }
}


extension EInvoiceEditViewController: TapOneButtonProtocol {
    func tapAction(tagNumber: Int) {
        /// remove sub
        UserDefaults.standard.set("", forKey: "subEmail")
        UserDefaults.standard.set("", forKey: "subCardNumber")
        self.navigationController?.popToRootViewController(animated: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            NotificationCenter.default.post(name: .reloadSubscription, object: nil)
        }
    }
}
