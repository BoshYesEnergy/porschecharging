//
//  PaymentViewController.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/22.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

enum WhereToPayment {
    case Profile
    case Subscription
}

class PaymentViewController: UIViewController {
     
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var paymentTop: NSLayoutConstraint!
    @IBOutlet weak var paymenyTable: UITableView!
    /// add child view
    
    var linePaymentClass:NormalClass?
    var invoiceList: InvoiceSettingListModel?
    //    var isExpendDataList = [true, true]
    var paymentheader = [LocalizeUtils.localized(key: "PaymentMethod"), LocalizeUtils.localized(key: "e-Invoice")]
    
    var lists: [CardListDataModel.Data] = []
    
    private lazy var eInvoiceVC = EInvoiceViewController()
    
    var whereTo : WhereToPayment = .Profile
    
    weak var delegate: SubscriptionReloadDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTable()
        self.paymenyTable.isHidden = true
        if whereTo == .Profile {
            paymentTop.constant = 0
            headerView.isHidden = true
        } else {
            paymentTop.constant = 65
            headerView.isHidden = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(subscriptionReload), name: .reloadSubscription, object: nil)
        DispatchQueue.main.async {
            self.callApiCardList()
            self.callProfile {
                self.callApiInvoiceSettingList()
            }
        }
    }
    
    fileprivate func setTable() {
        paymenyTable.register(UINib(nibName: "SubscriptionHeaderView", bundle: nil), forCellReuseIdentifier: "SubscriptionHeaderView")
        paymenyTable.register(UINib(nibName: "CreditCardListCell", bundle: nil), forCellReuseIdentifier: "CreditCardListCell")
        paymenyTable.register(UINib(nibName: "TapOneButtonCell", bundle: nil), forCellReuseIdentifier: "TapOneButtonCell")
        paymenyTable.register(UINib(nibName: "ChooseToPayCell", bundle: nil), forCellReuseIdentifier: "ChooseToPayCell")
        paymenyTable.register(UINib(nibName: "AddEInvoiceChildCell", bundle: nil), forCellReuseIdentifier: "AddEInvoiceChildCell") 
    }
    
    @objc fileprivate func subscriptionReload() {
        delegate?.subReload()
    }
    
    func getBackupEmail() -> String {
        /// check backup email
        var backupEmail = ""
        if let mailList = KJLRequest.shareInstance().profile?.datas?.mails {
            for i in mailList {
                if i.source?.lowercased() == "backup" {
                    backupEmail = i.email ?? ""
                }
            }
        }
        return backupEmail
    }
     
    @IBAction func backBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            NotificationCenter.default.post(name: .reloaAddSubscriptionView, object: nil)
        }
    }
}


//MARK: Api
extension PaymentViewController {
    fileprivate func callApiCardList() {
        KJLRequest.requestForCardListData {[weak self] (response) in
            guard let response = response as? CardListDataModel
            else { return }
            self?.lists.removeAll()
            self?.lists = response.datas ?? []
            self?.paymenyTable.reloadData()
            self?.paymenyTable.isHidden = false
        }
    }
    
    fileprivate func callProfile(completion: (()->())? ) {
        KJLRequest.requestForProfile { (response) in
            guard let data = response as? ProfileClass else { return }
            KJLRequest.shareInstance().profile = data
            if let callback = completion {
                callback()
            }
        }
    }
    
    fileprivate func callApiInvoiceSettingList() {
        KJLRequest.requestForInvoiceSettingList { (response) in
            guard let data = response as? InvoiceSettingListModel else {  return } 
            if data.resCode == "9000" { /// 沒有發票設定 會拿到 "9000"
                /// 初始發票
                KJLRequest.requestForSetInvoiceSettings("", "" , "",  "") { (_) in }
            } else { /// 有發票
                self.invoiceList = data
                self.paymenyTable.reloadData()
            }
            
        }
    }
}


//MARK: Table view deleagate & data source
extension PaymentViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if lists.count == 0 {
            return 1
        } else {
            return paymentheader.count
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if lists.count == 0 {
                return 1
            } else {
                return lists.count + 1
            }
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if lists.count == 0 {
            if let payCell = tableView.dequeueReusableCell(withIdentifier: "ChooseToPayCell", for: indexPath) as? ChooseToPayCell {
                payCell.setLanguage()
                payCell.delegate = self
                return payCell
            } else { return UITableViewCell() } 
        } else {
            if indexPath.section == 0 {
                /// Payment Method
                if indexPath.row == lists.count {
                    if let removeCell = tableView.dequeueReusableCell(withIdentifier: "TapOneButtonCell", for: indexPath) as? TapOneButtonCell {
                        removeCell.setView(custom: false)
                        if KJLRequest.shareInstance().profile?.datas?.subscription == true {
                            removeCell.tapBtn.setTitle(LocalizeUtils.localized(key: "ChangePayment"), for: .normal)
                        } else {
                            removeCell.tapBtn.setTitle(LocalizeUtils.localized(key: Language.Remove.rawValue), for: .normal)
                        }
                        removeCell.delegate = self
                        return removeCell
                    } else { return UITableViewCell() }
                } else {
                    if let creditCardListCell = tableView.dequeueReusableCell(withIdentifier: "CreditCardListCell", for: indexPath) as? CreditCardListCell {
                        creditCardListCell.list = lists[indexPath.row]
                        return creditCardListCell
                    } else { return UITableViewCell() }
                }
            } else {
                /// e-Invoice Receive Method
                if let cell = tableView.dequeueReusableCell(withIdentifier: "AddEInvoiceChildCell", for: indexPath) as? AddEInvoiceChildCell {
                    cell.invoiceList = invoiceList
                    cell.delegate    = self 
                    return cell
                } else {
                    return UITableViewCell()
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}


extension PaymentViewController {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let header = tableView.dequeueReusableCell(withIdentifier: "SubscriptionHeaderView") as? SubscriptionHeaderView else { return nil }
        header.toString = paymentheader[section]
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if lists.count == 0 {
            return 0
        } else {
            return 40
        }
    } 
}


//MARK: creat credit card
extension PaymentViewController: ChooseToPayDelegate {
    func tapFirstAction() {
        let vc = AddCreditCardViewController(nibName: "AddCreditCardViewController", bundle: nil)
        vc.hiddenNavigationBar(isHidden: true)
        vc.whereTo = whereTo
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tapSecondAction() {
        //        KJLRequest.requestLinePayRegister { (response) in
        //            guard let response = response as? NormalClass else { return }
        //            if let datas = response.datas {
        //                self.linePaymentClass = response
        //                let url = datas.appLink    //可更換轉跳網址
        //                #warning("change yes url to porsche url")
        //                let lineURL = URL(string: url!)
        //                UIApplication.shared.open(lineURL!, options: [:], completionHandler: nil)
        //            }
        //        }
    }
}


//MARK: remove edit
extension PaymentViewController: TapOneButtonProtocol {
    func tapAction(tagNumber: Int) { 
        if KJLRequest.shareInstance().profile?.datas?.subscription == true {
            let vc = AddPaymentViewController(nibName: "AddPaymentViewController", bundle: nil)
            vc.hiddenNavigationBar(isHidden: true)
            vc.status = .ChangePayment
            vc.lists  = lists
            navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = EditCreditCardVC(nibName: "EditCreditCardVC", bundle: nil)
            vc.hiddenNavigationBar(isHidden: true)
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}


//MARK: edit EInvoice
extension PaymentViewController: AddEInvoiceDelegate {
    func editBackUpEmail() {
        let vc = ShowTextFieldInformationVC()
        vc.titlName    = LocalizeUtils.localized(key: "EditBackupEmail")
        vc.placeholder = LocalizeUtils.localized(key: "EnterEmail")
         
        let backupEmail = getBackupEmail()
        
        vc.content     = backupEmail
        vc.delegate    = self
        vc.delegateTag = 1
        vc.constraintText = 99
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        present(vc, animated: true, completion: nil)
    }
    
    func editCode() {
        let vc = ShowTextFieldInformationVC()
        vc.titlName    = LocalizeUtils.localized(key: "EditCloudinvoice")
        vc.placeholder = LocalizeUtils.localized(key: "EnterCloudinvoice")
        vc.content     = invoiceList?.datas?.vehicleNum ?? ""
        vc.delegate    = self
        vc.delegateTag = 2
        vc.constraintText = 8
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        present(vc, animated: true, completion: nil)
    }
    
    func editcompany() {
        let vc = ShowTwoTextFieldInformationVC()
        vc.stringToTitle = LocalizeUtils.localized(key: "EditCompany")
        vc.delegate    = self
        vc.stringToFirst = invoiceList?.datas?.taxidName ?? ""
        vc.stringToSecond = invoiceList?.datas?.taxidNum ?? ""
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        present(vc, animated: true, completion: nil)
    }
    
    func updateSelectArea(useBackup: Bool, invoiceType: String, vehicleType: String) {
        KJLRequest.requestForEditInvoiceSetting(useBackup, invoiceType, vehicleType) { (response) in
            guard let _ = response else { return }
            self.callApiInvoiceSettingList()
        }
    }
}


extension PaymentViewController: ShowTextFieldInformationDelegate {
    func tapConfirm(text: String, delegateTag: Int) {
        if delegateTag == 1 {
            KJLRequest.requestForSetInvoiceSettings(text, invoiceList?.datas?.vehicleNum ?? "" , invoiceList?.datas?.taxidName ?? "", invoiceList?.datas?.taxidNum ?? "") { (_) in
                self.dismiss(animated: true) {
                    self.callProfile {
                        self.callApiInvoiceSettingList()
                    }
                }
            }
        } else if delegateTag == 2 { 
            let backupEmail = getBackupEmail()
            KJLRequest.requestForSetInvoiceSettings(backupEmail, text , invoiceList?.datas?.taxidName ?? "", invoiceList?.datas?.taxidNum ?? "") { (_) in
                self.dismiss(animated: true) {
                    self.callProfile {
                        self.callApiInvoiceSettingList()
                    }
                }
            }
        }
    }
    
    func tapCancel() {
        self.dismiss(animated: true, completion: nil)
    }
}


extension PaymentViewController: TwoTextFieldDelegate {
    func confirm(first: String, second: String) {
        let backupEmail = getBackupEmail()
        KJLRequest.requestForSetInvoiceSettings(backupEmail, invoiceList?.datas?.vehicleNum ?? "" , first, second) { (_) in
            self.dismiss(animated: true) {
                self.callProfile {
                    self.callApiInvoiceSettingList()
                }
            }
        }
    }
    
    func cancel() {
        self.dismiss(animated: true, completion: nil)
    }
}
