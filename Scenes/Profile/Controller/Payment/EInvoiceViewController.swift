//
//  EInvoiceViewController.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/28.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class EInvoiceViewController: BaseViewController {  
    
    @IBOutlet weak var personalClassBtn: UIButton!
    @IBOutlet weak var businessClassBtn: UIButton!
    
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var settingBtn: UIButton!
    
    @IBOutlet weak var emailBtn: UIButton!
    @IBOutlet weak var backUpEmailBtn: UIButton!
    @IBOutlet weak var codeBtn: UIButton!
    
    @IBOutlet weak var companyName: UILabel!
    @IBOutlet weak var companyUnicode: UILabel!
    @IBOutlet weak var noSettings: UILabel!
    
    private var useBackup   = false
    private var invoiceType = "01"
    private var vehicleType = "01"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setHeaderView("e-Invoice Select", action: .POP)
        setView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.callProfile {
            self.callApiInvoiceSettingList()
        }
    }
}


//MARK: Setting view
extension EInvoiceViewController {
    fileprivate func setView() {
        cancelBtn.layer.borderWidth = 1
        cancelBtn.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        settingBtn.layer.borderWidth = 1
        settingBtn.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        personalClassBtn.setImage(UIImage(named: ToolImage.radio_click), for: .highlighted)
        businessClassBtn.setImage(UIImage(named: ToolImage.radio_disable), for: .highlighted)
        emailBtn.setImage(UIImage(named: ToolImage.radio_click), for: .highlighted)
        backUpEmailBtn.setImage(UIImage(named: ToolImage.radio_click), for: .highlighted)
        codeBtn.setImage(UIImage(named: ToolImage.radio_click), for: .highlighted)
        
        codeBtn.setTitle("Cloud invoice code: No settings", for: .normal)
        businessClassBtn.isEnabled      = false
        codeBtn.isEnabled               = false
        companyName.isHidden            = true
        companyUnicode.isHidden         = true
    }
    
    fileprivate func callProfile(completion: (()->())? ) {
        KJLRequest.requestForProfile { (response) in
            guard let data = response as? ProfileClass else { return }
            KJLRequest.shareInstance().profile = data
            if let email = data.datas?.mails?[0].email {
                self.emailBtn.setTitle(email, for: .normal)
                self.emailBtn.isEnabled = true
            } else {
                self.emailBtn.setTitle("No Setting", for: .normal)
                self.emailBtn.isEnabled = false
                
            }
            
            if data.datas?.mails?.count == 2 {
                let backupEmail = data.datas?.mails?[1].email ?? ""
                self.backUpEmailBtn.setTitle(backupEmail, for: .normal)
                self.backUpEmailBtn.isEnabled = true
            } else {
                self.backUpEmailBtn.setTitle("Backup E-mail: No settings", for: .normal)
                self.backUpEmailBtn.isEnabled = false
            }
            
            if let callback = completion {
                callback()
            }
        }
    }
    
    fileprivate func callApiInvoiceSettingList() {
        KJLRequest.requestForInvoiceSettingList { (response) in
            guard let data = response as? InvoiceSettingListModel,
                  let datas = data.datas
            else { // default
                self.personalClass(self.personalClassBtn) 
                self.personalClassBtn.isEnabled = true
                self.businessClassBtn.setImage(UIImage(named: ToolImage.radio_disable), for: .highlighted)
                self.businessClassBtn.isEnabled = false
                self.selectEmail(self.emailBtn)
                // save status
                self.useBackup   = false
                self.invoiceType = "01" // 01 = Personal, 02 = Business
                self.vehicleType = ""   // if vehicleNum == true ? "01" : ""
                return }
            
            if let code = datas.vehicleNum {
                self.codeBtn.setTitle(code, for: .normal)
                self.codeBtn.isEnabled = true
            }
            
            // save status
            self.useBackup   = datas.useBackup ?? false
            self.invoiceType = datas.invoiceType ?? "01" // 01 = Personal, 02 = Business
            self.vehicleType = datas.vehicleType ?? ""   // if vehicleNum == true ? "01" : ""
            
            /// check businessClass
            if let taxidName = datas.taxidName ,
               taxidName != "" {
                self.companyName.text        = taxidName
                self.companyUnicode.text     = datas.taxidNum ?? ""
                self.companyName.isHidden    = false
                self.companyUnicode.isHidden = false
                self.noSettings.isHidden     = true
                self.businessClassBtn.isEnabled = true
                self.businessClassBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                self.businessClassBtn.setImage(UIImage(named: ToolImage.radio_click), for: .highlighted)
            } else {
                self.companyName.isHidden    = true
                self.companyUnicode.isHidden = true
                self.noSettings.isHidden     = false
                self.businessClassBtn.isEnabled = false
                self.businessClassBtn.setTitleColor(#colorLiteral(red: 0.5882352941, green: 0.5960784314, blue: 0.6039215686, alpha: 1), for: .normal)
                self.businessClassBtn.setImage(UIImage(named: ToolImage.radio_disable), for: .highlighted)
            }
            
            /// check area
            if datas.invoiceType == "01" {
                self.personalClassBtn.setImage(UIImage(named: ToolImage.radio_default), for: .normal)
                self.personalClassBtn.isSelected = true
                self.businessClassBtn.setImage(UIImage(named: ToolImage.radio_disable), for: .normal)
                self.businessClassBtn.isSelected = false
                self.personalClassBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                self.businessClassBtn.setTitleColor(#colorLiteral(red: 0.5882352941, green: 0.5960784314, blue: 0.6039215686, alpha: 1), for: .normal)
                if datas.vehicleType == "" {
                    if datas.useBackup == true { // is backup email
                        self.selectBackupEmail(self.backUpEmailBtn)
                    } else { // app login email
                        self.selectEmail(self.emailBtn)
                    }
                } else {
                    self.selectCode(self.codeBtn)
                }
            } else {
                self.personalClassBtn.setImage(UIImage(named: ToolImage.radio_disable), for: .normal)
                self.personalClassBtn.isSelected = false
                self.businessClassBtn.setImage(UIImage(named: ToolImage.radio_default), for: .normal)
                self.businessClassBtn.isSelected = true
                self.businessClassBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                self.personalClassBtn.setTitleColor(#colorLiteral(red: 0.5882352941, green: 0.5960784314, blue: 0.6039215686, alpha: 1), for: .normal)
            }
        }
    }
    
    fileprivate func callApiEditInvoiceSetting() {
        KJLRequest.requestForEditInvoiceSetting(useBackup, invoiceType, vehicleType) { (response) in
            guard let _ = response else { return }
            self.navigationController?.popViewController(animated: true)
        }
    }
}


//MARK: Action
extension EInvoiceViewController {
    @IBAction func personalClass(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true {
            personalClassBtn.setImage(UIImage(named: ToolImage.radio_default), for: .normal)
            personalClassBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            businessClassBtn.setImage(UIImage(named: ToolImage.radio_disable), for: .normal)
            codeBtn.setImage(UIImage(named: ToolImage.radio_click), for: .normal)
            businessClassBtn.isSelected = false
            codeBtn.isSelected = false
            invoiceType = "01"
        }
    }
    
    @IBAction func businessClass(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true {
            businessClassBtn.setImage(UIImage(named: ToolImage.radio_default), for: .normal)
            businessClassBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            personalClassBtn.setImage(UIImage(named: ToolImage.radio_disable), for: .normal)
            codeBtn.setImage(UIImage(named: ToolImage.radio_click), for: .normal)
            emailBtn.setImage(UIImage(named: ToolImage.radio_click), for: .normal)
            backUpEmailBtn.setImage(UIImage(named: ToolImage.radio_click), for: .normal)
            personalClassBtn.isSelected = false
            codeBtn.isSelected = false
            emailBtn.isSelected = false
            backUpEmailBtn.isSelected = false
            useBackup = false
            invoiceType = "02"
        }
    }
    
    @IBAction func selectEmail(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true {
            emailBtn.setImage(UIImage(named: ToolImage.radio_default), for: .normal)
            businessClassBtn.setImage(UIImage(named: ToolImage.radio_disable), for: .normal)
            businessClassBtn.setTitleColor(#colorLiteral(red: 0.5882352941, green: 0.5960784314, blue: 0.6039215686, alpha: 1), for: .normal)
            codeBtn.setImage(UIImage(named: ToolImage.radio_click), for: .normal)
            backUpEmailBtn.setImage(UIImage(named: ToolImage.radio_click), for: .normal)
            personalClassBtn.setImage(UIImage(named: ToolImage.radio_default), for: .normal)
            personalClassBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            backUpEmailBtn.isSelected = false
            codeBtn.isSelected = false
            personalClassBtn.isSelected = true
            businessClassBtn.isSelected = false
            useBackup = false
            vehicleType = ""
            invoiceType = "01"
        }
    }
    
    @IBAction func selectBackupEmail(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true {
            backUpEmailBtn.setImage(UIImage(named: ToolImage.radio_default), for: .normal)
            businessClassBtn.setImage(UIImage(named: ToolImage.radio_disable), for: .normal)
            businessClassBtn.setTitleColor(#colorLiteral(red: 0.5882352941, green: 0.5960784314, blue: 0.6039215686, alpha: 1), for: .normal)
            emailBtn.setImage(UIImage(named: ToolImage.radio_click), for: .normal)
            codeBtn.setImage(UIImage(named: ToolImage.radio_click), for: .normal)
            personalClassBtn.setImage(UIImage(named: ToolImage.radio_default), for: .normal)
            personalClassBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            emailBtn.isSelected = false
            codeBtn.isSelected = false
            personalClassBtn.isSelected = true
            businessClassBtn.isSelected = false
            useBackup = true
            vehicleType = ""
            invoiceType = "01"
        }
    }
    
    @IBAction func selectCode(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true {
            codeBtn.setImage(UIImage(named: ToolImage.radio_default), for: .normal)
            businessClassBtn.setImage(UIImage(named: ToolImage.radio_disable), for: .normal)
            businessClassBtn.setTitleColor(#colorLiteral(red: 0.5882352941, green: 0.5960784314, blue: 0.6039215686, alpha: 1), for: .normal)
            emailBtn.setImage(UIImage(named: ToolImage.radio_click), for: .normal)
            backUpEmailBtn.setImage(UIImage(named: ToolImage.radio_click), for: .normal)
            personalClassBtn.setImage(UIImage(named: ToolImage.radio_default), for: .normal)
            personalClassBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            emailBtn.isSelected = false
            backUpEmailBtn.isSelected = false
            personalClassBtn.isSelected = true
            businessClassBtn.isSelected = false
            vehicleType = "02"
            invoiceType = "01"
        }
    }
    
    @IBAction func save(_ sender: UIButton) {
        callApiEditInvoiceSetting()
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func gotoSetting(_ sender: UIButton) {
        let vc = EInvoiceSetViewController(nibName: "EInvoiceSetViewController", bundle: nil)
        vc.hiddenNavigationBar(isHidden: true)
        navigationController?.pushViewController(vc, animated: true)
    }
}


