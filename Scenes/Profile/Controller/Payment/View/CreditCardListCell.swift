//
//  CreditCardListCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/25.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

protocol CreditCardListProtocol: class {
    func appendToRemoveList(index: Int) 
}

class CreditCardListCell: UITableViewCell {
    @IBOutlet weak var creditCartName: UILabel!
    @IBOutlet weak var validtitle: UILabel!
    
    @IBOutlet weak var linePayImage: UIImageView!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var creditCardNumber: UILabel!
    @IBOutlet weak var creditCardDate: UILabel!
    @IBOutlet weak var checkBtn: UIButton!
    
    var index = 0
    var delegate: CreditCardListProtocol?
    
    var list: CardListDataModel.Data? {
        didSet {
            guard let data = list else { return }
            creditCartName.text = data.cardName
            if let cardDate = data.expiry,
               cardDate != "" {
                let yearStart = cardDate.index(cardDate.startIndex, offsetBy: 0)
                let yearEnd = cardDate.index(cardDate.startIndex, offsetBy: 1)
                let year = String(cardDate[yearStart...yearEnd])
                
                let monthStrat = cardDate.index(cardDate.startIndex, offsetBy: 2)
                let monthEnd = cardDate.index(cardDate.startIndex, offsetBy: 3)
                let month = String(cardDate[monthStrat...monthEnd])
                creditCardDate.text = "\(month)/\(year)"
            }
            
            if let number = data.cardNum {
                let end = number.index(number.endIndex, offsetBy: 0)
                let start = number.index(number.endIndex, offsetBy: -4)
                creditCardNumber.text = "・・・・    ・・・・    ・・・・    " + String(number[start..<end])
                print(String(number[start..<end]))
            }
            
            if data.paymentType?.lowercased() == "linepay" {
                creditCardNumber.text = "LINE Pay"
                linePayImage.isHidden = false
            } else {
                linePayImage.isHidden = true
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        setLanguage()
        borderView.layer.cornerRadius = 10
        borderView.layer.borderColor  = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        borderView.layer.borderWidth  = 1
        linePayImage.isHidden = true
        linePayImage.layer.cornerRadius = 10
        checkBtn.isHidden = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setLanguage() {
        creditCartName.text = LocalizeUtils.localized(key: "PaymentCreditCard")
        validtitle.text = LocalizeUtils.localized(key: "PaymentVALID")
    }
    
    @IBAction func check(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        let image = UIImage(named: sender.isSelected == true ? "icon_check_default" : "icon_uncheck_default")
        checkBtn.setImage(image, for: .normal)
        delegate?.appendToRemoveList(index: index)
        borderView.backgroundColor = sender.isSelected == true ? #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    } 
}
