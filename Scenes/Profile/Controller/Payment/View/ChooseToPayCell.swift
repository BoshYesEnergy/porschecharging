//
//  ChooseToPayCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/6.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

protocol ChooseToPayDelegate: class {
    func tapFirstAction()
    func tapSecondAction()
}

class ChooseToPayCell: UITableViewCell {
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var contentLablel: UILabel!
    @IBOutlet weak var topBtn: UIButton!
    @IBOutlet weak var bottomBtn: UIButton!
     
    var delegate: ChooseToPayDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        setLanguage()
        topBtn.layer.borderColor    = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        topBtn.layer.borderWidth    = 1
        bottomBtn.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        bottomBtn.layer.borderWidth = 1
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setLanguage() {
        contentLablel.text = LocalizeUtils.localized(key: "PaymentTitle")
        topBtn.setTitle(LocalizeUtils.localized(key: "AddCreditCard"), for: .normal)
        bottomBtn.setTitle(LocalizeUtils.localized(key: "LINEPay"), for: .normal)
    }
    
    @IBAction func tapAction(_ sender: UIButton) {
        delegate?.tapFirstAction()
    }
    
    @IBAction func bottomAction(_ sender: UIButton) {
        delegate?.tapSecondAction()
    }
}
