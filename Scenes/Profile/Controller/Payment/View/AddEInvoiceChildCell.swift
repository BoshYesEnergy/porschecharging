//
//  AddEInvoiceChildCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/8.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit 

protocol AddEInvoiceDelegate: class {
    func editBackUpEmail()
    func editCode()
    func editcompany()
    func updateSelectArea(useBackup: Bool, invoiceType: String, vehicleType: String)
}

class AddEInvoiceChildCell: UITableViewCell {
    
    @IBOutlet weak var personalClassBtn: UIButton!
    @IBOutlet weak var businessClassBtn: UIButton!
    
    @IBOutlet weak var emailBtn: UIButton!
    @IBOutlet weak var backUpEmailBtn: UIButton!
    @IBOutlet weak var codeBtn: UIButton!
    
    @IBOutlet weak var companyName: UILabel!
    @IBOutlet weak var companyUnicode: UILabel!
    @IBOutlet weak var noSettings: UILabel!
    
    @IBOutlet weak var editBackUpEmailBtn: UIButton!
    @IBOutlet weak var editCodeBtn: UIButton!
    @IBOutlet weak var editCompanyBtn: UIButton!
    
    private var useBackup   = false
    private var invoiceType = "01"
    private var vehicleType = "01"
    
    weak var delegate: AddEInvoiceDelegate?
    
    var invoiceList: InvoiceSettingListModel? {
        didSet {
            guard let data = invoiceList,
                  let datas = data.datas
            else {
                self.personalClass(self.personalClassBtn)
                self.personalClassBtn.isEnabled = true
                self.businessClassBtn.setImage(UIImage(named: ToolImage.radio_disable), for: .highlighted)
                self.businessClassBtn.isEnabled = false
                self.selectEmail(self.emailBtn)
                // save status
                self.useBackup   = false
                self.invoiceType = "01" // 01 = Personal, 02 = Business
                self.vehicleType = ""   // if vehicleNum == true ? "01" : ""
                
                let profile = KJLRequest.shareInstance().profile
                self.emailBtn.setTitle(profile?.datas?.mails?[0].email ?? "", for: .normal)
                self.emailBtn.isEnabled = true
                
                self.backUpEmailBtn.setTitle(LocalizeUtils.localized(key: "BackupEmailInfo"), for: .normal)
                self.backUpEmailBtn.isEnabled = false
                self.backUpEmailBtn.setImage(UIImage(named: ToolImage.radio_disable), for: .normal)
                
                self.codeBtn.setTitle(LocalizeUtils.localized(key: "CloudinvoiceInfo"), for: .normal)
                self.codeBtn.setImage(UIImage(named: ToolImage.radio_disable), for: .normal)
                 
                self.editCompanyBtn.setImage(UIImage(named: ToolImage.edit_default), for: .normal)
                return }
            
            /// default email
            let profile = KJLRequest.shareInstance().profile
            if let email = profile?.datas?.mails?[0].email {
                self.emailBtn.setTitle(email, for: .normal)
                self.emailBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                self.emailBtn.isEnabled = true
                self.emailBtn.setImage(UIImage(named: ToolImage.radio_click), for: .normal)
            } else {
                self.emailBtn.setTitle("No Setting", for: .normal)
                self.emailBtn.setTitleColor(#colorLiteral(red: 0.5882352941, green: 0.5960784314, blue: 0.6039215686, alpha: 1), for: .normal)
                self.emailBtn.isEnabled = false
                self.emailBtn.setImage(UIImage(named: ToolImage.radio_disable), for: .normal)
            }
            
            /// backup email
            var backupEmail = ""
            if let mailList = profile?.datas?.mails {
//                print("mailList: \(mailList)")
                for i in mailList {
                    if i.source?.lowercased() == "backup" {
                        backupEmail = i.email ?? ""
                    }
                }
            }
            if backupEmail != "" {
                self.backUpEmailBtn.setTitle(backupEmail, for: .normal)
                self.backUpEmailBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                self.backUpEmailBtn.isEnabled = true
                self.backUpEmailBtn.setImage(UIImage(named: ToolImage.radio_click), for: .normal)
            } else {
                self.backUpEmailBtn.setTitle(LocalizeUtils.localized(key: "BackupEmailInfo"), for: .normal)
                self.backUpEmailBtn.setTitleColor(#colorLiteral(red: 0.5882352941, green: 0.5960784314, blue: 0.6039215686, alpha: 1), for: .normal)
                self.backUpEmailBtn.isEnabled = false
                self.backUpEmailBtn.setImage(UIImage(named: ToolImage.radio_disable), for: .normal)
            }
            
            // Code
            if let code = datas.vehicleNum,
               code != "" {
                self.codeBtn.setTitle(code, for: .normal)
                self.codeBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                self.codeBtn.isEnabled = true
                self.codeBtn.setImage(UIImage(named: ToolImage.radio_click), for: .normal)
            } else {
                self.codeBtn.setTitle(LocalizeUtils.localized(key: "CloudinvoiceInfo"), for: .normal)
                self.codeBtn.setTitleColor(#colorLiteral(red: 0.5882352941, green: 0.5960784314, blue: 0.6039215686, alpha: 1), for: .normal)
                self.codeBtn.isEnabled = false
                self.codeBtn.setImage(UIImage(named: ToolImage.radio_disable), for: .normal)
            }
            
            // save status
            self.useBackup   = datas.useBackup ?? false
            self.invoiceType = datas.invoiceType ?? "01" // 01 = Personal, 02 = Business
            self.vehicleType = datas.vehicleType ?? ""   // if vehicleNum == true ? "01" : ""
            
            noSettings.text = LocalizeUtils.localized(key: "TaxNosettings")
            /// check businessClass
            let taxidName = datas.taxidName ?? ""
            if taxidName != "" {
                self.noSettings.isHidden     = true
                self.companyName.isHidden    = false
                self.companyUnicode.isHidden = false
                self.businessClassBtn.isEnabled = true
                self.businessClassBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                self.businessClassBtn.setImage(UIImage(named: ToolImage.radio_click), for: .highlighted)
                
                self.companyName.text        = LocalizeUtils.localized(key: "CompanyName") + ": \(taxidName)"
                self.companyUnicode.text     = LocalizeUtils.localized(key: "CompanyUnicode") + ": \(datas.taxidNum ?? "")"
            } else {
                self.noSettings.isHidden     = false
                self.companyName.isHidden    = true
                self.companyUnicode.isHidden = true
                self.businessClassBtn.isEnabled = false
                self.businessClassBtn.setTitleColor(#colorLiteral(red: 0.5882352941, green: 0.5960784314, blue: 0.6039215686, alpha: 1), for: .normal)
                self.businessClassBtn.setImage(UIImage(named: ToolImage.radio_disable), for: .highlighted)
            }
            
            /// check area
            if datas.invoiceType == "01" {
                self.personalClassBtn.setImage(UIImage(named: ToolImage.radio_default), for: .normal)
                self.personalClassBtn.isSelected = true
                
                if taxidName != "" {
                    self.businessClassBtn.setImage(UIImage(named: ToolImage.radio_click), for: .normal)
                } else {
                    self.businessClassBtn.setImage(UIImage(named: ToolImage.radio_disable), for: .normal)
                }
                self.businessClassBtn.isSelected = false
                if datas.vehicleType ?? "" == "" || datas.vehicleType ?? "" == "01" {
                    if datas.useBackup == true { // is backup email
                        self.backUpEmailBtn.setImage(UIImage(named: ToolImage.radio_default), for: .normal)
                    } else { // app login email
                        self.emailBtn.setImage(UIImage(named: ToolImage.radio_default), for: .normal)
                    }
                } else {
                    self.codeBtn.setImage(UIImage(named: ToolImage.radio_default), for: .normal)
                }
            } else {
                self.personalClassBtn.setImage(UIImage(named: ToolImage.radio_click), for: .normal)
                self.personalClassBtn.isSelected = false
                self.businessClassBtn.setImage(UIImage(named: ToolImage.radio_default), for: .normal)
                self.businessClassBtn.isSelected = true
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        setLanguage()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func setLanguage() { 
        personalClassBtn.setTitle(LocalizeUtils.localized(key: "Peronal"), for: .normal)
        businessClassBtn.setTitle(LocalizeUtils.localized(key: "BusinessInfo"), for: .normal)
    }
    
    @IBAction func personalClass(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true {
            invoiceType = "01"
            delegate?.updateSelectArea(useBackup: useBackup, invoiceType: invoiceType, vehicleType: vehicleType)
        }
    }
    
    @IBAction func businessClass(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true {
            useBackup = false
            invoiceType = "02"
            delegate?.updateSelectArea(useBackup: useBackup, invoiceType: invoiceType, vehicleType: vehicleType)
        }
    }
    
    
    @IBAction func selectEmail(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true {
//            checkPersonal(tapEara: true)
//            checkCompany(tapEara: false)
//
//            emailBtn.setImage(UIImage(named: ToolImage.radio_default), for: .normal)
//            businessClassBtn.setImage(UIImage(named: ToolImage.radio_disable), for: .normal)
//            businessClassBtn.setTitleColor(#colorLiteral(red: 0.5882352941, green: 0.5960784314, blue: 0.6039215686, alpha: 1), for: .normal)
//            codeBtn.setImage(UIImage(named: ToolImage.radio_click), for: .normal)
//            backUpEmailBtn.setImage(UIImage(named: ToolImage.radio_click), for: .normal)
//            personalClassBtn.setImage(UIImage(named: ToolImage.radio_default), for: .normal)
//            personalClassBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
//            backUpEmailBtn.isSelected = false
//            codeBtn.isSelected = false
//            personalClassBtn.isSelected = true
//            businessClassBtn.isSelected = false
            useBackup = false
            vehicleType = ""
            invoiceType = "01"
            delegate?.updateSelectArea(useBackup: useBackup, invoiceType: invoiceType, vehicleType: vehicleType)
        }
    }
    
    @IBAction func selectBackupEmail(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true {
//            checkPersonal(tapEara: true)
//            checkCompany(tapEara: false)
//
//            backUpEmailBtn.setImage(UIImage(named: ToolImage.radio_default), for: .normal)
//            businessClassBtn.setImage(UIImage(named: ToolImage.radio_disable), for: .normal)
//            businessClassBtn.setTitleColor(#colorLiteral(red: 0.5882352941, green: 0.5960784314, blue: 0.6039215686, alpha: 1), for: .normal)
//            emailBtn.setImage(UIImage(named: ToolImage.radio_click), for: .normal)
//            codeBtn.setImage(UIImage(named: ToolImage.radio_click), for: .normal)
//            personalClassBtn.setImage(UIImage(named: ToolImage.radio_default), for: .normal)
//            personalClassBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
//            emailBtn.isSelected = false
//            codeBtn.isSelected = false
//            personalClassBtn.isSelected = true
//            businessClassBtn.isSelected = false
            useBackup = true
            vehicleType = ""
            invoiceType = "01"
            delegate?.updateSelectArea(useBackup: useBackup, invoiceType: invoiceType, vehicleType: vehicleType)
        }
    }
    
    @IBAction func selectCode(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true {
//            checkPersonal(tapEara: true)
//            checkCompany(tapEara: false)
//
//            codeBtn.setImage(UIImage(named: ToolImage.radio_default), for: .normal)
//            businessClassBtn.setImage(UIImage(named: ToolImage.radio_disable), for: .normal)
//            businessClassBtn.setTitleColor(#colorLiteral(red: 0.5882352941, green: 0.5960784314, blue: 0.6039215686, alpha: 1), for: .normal)
//            emailBtn.setImage(UIImage(named: ToolImage.radio_click), for: .normal)
//            backUpEmailBtn.setImage(UIImage(named: ToolImage.radio_click), for: .normal)
//            personalClassBtn.setImage(UIImage(named: ToolImage.radio_default), for: .normal)
//            personalClassBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
//            emailBtn.isSelected = false
//            backUpEmailBtn.isSelected = false
//            personalClassBtn.isSelected = true
//            businessClassBtn.isSelected = false
            vehicleType = "02"
            invoiceType = "01"
            delegate?.updateSelectArea(useBackup: useBackup, invoiceType: invoiceType, vehicleType: vehicleType)
        }
    }
    
    @IBAction func editBackUpEmail(_ sender: UIButton) {
        delegate?.editBackUpEmail()
    }
    
    @IBAction func editCode(_ sender: UIButton) {
        delegate?.editCode()
    }
    
    @IBAction func editCompany(_ sender: UIButton) {
        delegate?.editcompany()
    }
}
