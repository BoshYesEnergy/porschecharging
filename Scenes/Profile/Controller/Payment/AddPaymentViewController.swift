//
//  AddPaymentViewController.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/22.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

enum PaymentStatus {
    case AddNewPayment
    case ChangePayment
}

class AddPaymentViewController: BaseViewController {

    @IBOutlet weak var content: UILabel!
    @IBOutlet weak var createCardBtn: UIButton!
    @IBOutlet weak var linePayBtn: UIButton!
    
    var linePaymentClass:NormalClass?
    /// change payment
    var lists: [CardListDataModel.Data] = []
    var status: PaymentStatus = .AddNewPayment
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setHeaderView(LocalizeUtils.localized(key: "AddCreditCard"), action: .POP)
        setUI()
    }
    
    fileprivate func setUI() {
        createCardBtn.layer.borderWidth = 1
        createCardBtn.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        linePayBtn.layer.borderWidth = 1
        linePayBtn.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        linePayBtn.isHidden          = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.linePayUpdate), name: .linePayUpdate, object: nil)
        content.text = LocalizeUtils.localized(key: "PaymentTitle")
        createCardBtn.setTitle(LocalizeUtils.localized(key: "AddCreditCard"), for: .normal) 
    }
    
    @IBAction func createCard(_ sender: UIButton) {
        let vc = AddCreditCardViewController(nibName: "AddCreditCardViewController", bundle: nil)
        vc.hiddenNavigationBar(isHidden: true)
        vc.status = .ChangePayment
        vc.lists  = lists
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func createLinePay(_ sender: UIButton) {
//        postAction()
    }
}


//MARK: Api
extension AddPaymentViewController {
    private func postAction() {
        KJLRequest.requestLinePayRegister { (response) in
            guard let response = response as? NormalClass else { return }
            if let datas = response.datas {
                self.linePaymentClass = response
                let url = datas.appLink    //可更換轉跳網址
                #warning("change yes url to porsche url")
                let lineURL = URL(string: url!)
                UIApplication.shared.open(lineURL!, options: [:], completionHandler: nil)
            }
        }
    }
    
    @objc func linePayUpdate() {
        if self.linePaymentClass?.datas?.transactionId != nil {
            let transactionID = self.linePaymentClass?.datas?.transactionId
            KJLRequest.requestLinePayConfirm(transactionId: transactionID!) { (response) in
                guard let response = response as? NormalClass else { return }
                guard response.resCode == "0000",
                    response.resMsg == "OK"
                    else { return }
                
                #warning("Present Alert")
            }
        }
    }
}
