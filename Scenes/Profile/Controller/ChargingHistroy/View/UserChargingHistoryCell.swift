//
//  UserChargingHistoryCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/12.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

protocol ChargingHistoryDelegate: class {
    func gotoChargingHistoryDetail(index: Int)
}

class UserChargingHistoryCell: UITableViewCell {
     
    @IBOutlet weak var stationTitle: UILabel!
    @IBOutlet weak var chargingTimeTitle: UILabel!
    @IBOutlet weak var socTitle: UILabel!
    
    @IBOutlet weak var completedTitle: UILabel!
    @IBOutlet weak var stationName: UILabel!
    @IBOutlet weak var chargingTime: UILabel!
    @IBOutlet weak var soc: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var nextBtn: UIButton! 
    
    var index = 0
    
    weak var delegate: ChargingHistoryDelegate?
    
    var data: ChargingHistoryModel.Datas? {
        didSet {
            guard let list = data else { return }
            
            if list.status?.contains("null") == true {
                completedTitle.text = ""
            } else {
                completedTitle.text = list.status ?? ""
            }
            
            if list.stationName?.contains("null") == true {
                stationName.text    = ""
            } else {
                stationName.text    = list.stationName ?? ""
            }
            
            if list.chargingTime?.contains("null") == true {
                chargingTime.text   = ""
            } else {
                chargingTime.text   = list.chargingTime ?? ""
            }
            
            if list.soc?.contains("null") == true {
                soc.text            = ""
            } else {
                soc.text            = list.soc ?? ""
            }
            
            if list.startTime?.contains("null") == true {
                date.text           = ""
            } else {
                date.text           = list.startTime ?? ""
            } 
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        chargingTimeTitle.text = LocalizeUtils.localized(key: "HistoryChargingTime")
        stationTitle.text = LocalizeUtils.localized(key: "station")
        socTitle.text = LocalizeUtils.localized(key: "SOC")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func next(_ sender: UIButton) {
        delegate?.gotoChargingHistoryDetail(index: index)
    }
}
