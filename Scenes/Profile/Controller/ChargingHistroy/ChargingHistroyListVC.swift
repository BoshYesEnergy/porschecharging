//
//  ChargingHistroyListVC.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/11.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class ChargingHistroyListVC: UIViewController , UIScrollViewDelegate{
    
    @IBOutlet weak var dateSegmented: UISegmentedControl!
    @IBOutlet weak var dateBottomView: UIView!
    @IBOutlet weak var tapLeftDate: UIButton!
    @IBOutlet weak var tapRightDate: UIButton!
    @IBOutlet weak var dateLabel: UILabel! 
    @IBOutlet weak var historyTable: UITableView!
    @IBOutlet weak var bottomViewHeight: NSLayoutConstraint!
    @IBOutlet weak var stackView: UIStackView!
    
    var histroyList: [ChargingHistoryModel.Datas] = []
    var month: Int    = 0
    var date: String = ""
    var serviceDate: String = ""
    
    let today = Date()
    let mMFormatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTable()
        changeDateToUI(onOFF: false)
        setDateActionUI(hidden: true)
        setGestureRecognizer()
        date = "7" 
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.setLanguage()
            self.callApiChargingHistory(self.date)
        }
    }
    
    fileprivate func setLanguage() {
        dateSegmented.setTitle(LocalizeUtils.localized(key: "7date"), forSegmentAt: 0)
        dateSegmented.setTitle(LocalizeUtils.localized(key: "30date"), forSegmentAt: 1)
        dateSegmented.setTitle(LocalizeUtils.localized(key: "Bymonth"), forSegmentAt: 2)
    }
    
    fileprivate func setGestureRecognizer() {
        let swipeAddDate = UISwipeGestureRecognizer(target: self, action: #selector(addMonthAction))
        swipeAddDate.direction = .left
        let swipeCutDate = UISwipeGestureRecognizer(target: self, action: #selector(cutMonthAction))
        swipeCutDate.direction = .right
         
        dateBottomView.addGestureRecognizer(swipeAddDate)
        dateBottomView.addGestureRecognizer(swipeCutDate)
    }
    
    fileprivate func getDateForMatter(month: Int) {
        mMFormatter.dateFormat = "MMMM, yyyy"
        var dateComponent = DateComponents()
        dateComponent.month = month
        let futureDate = Calendar.current.date(byAdding: dateComponent, to: today)
         
        if LocalizeUtils.shared.toLanguage() == "en" {
            mMFormatter.locale = Locale(identifier: "en")
        } else {
            mMFormatter.locale = Locale(identifier: "zh_Hant_TW")
        }
        
        date = mMFormatter.string(from: futureDate!)
        print(mMFormatter.string(from: futureDate!))
    }
    
    fileprivate func dateForServer() -> String {
        mMFormatter.dateFormat = "yyyyMM"
        var dateComponent = DateComponents()
        dateComponent.month = month
        let futureDate = Calendar.current.date(byAdding: dateComponent, to: today)
        serviceDate = mMFormatter.string(from: futureDate!) 
        return mMFormatter.string(from: futureDate!)
    }
    
    fileprivate func setTable() {
        historyTable.register(UINib(nibName: "UserChargingHistoryCell", bundle: nil), forCellReuseIdentifier: "UserChargingHistoryCell")
    }
    
    private func changeDateToUI(onOFF: Bool) {
        UIView.animate(withDuration: 0.5) {
            self.bottomViewHeight.constant = onOFF == true ? 59 : 15
            self.view.layoutIfNeeded()
        }
    }
    
    fileprivate func setDateActionUI(hidden: Bool) {
        UIView.animate(withDuration: 0.3) {
            self.stackView.isHidden = hidden
        }
    }
}

 

//MARK: Set Action
extension ChargingHistroyListVC {
    @IBAction func selectSegmented(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            date = "7"
            changeDateToUI(onOFF: false)
            setDateActionUI(hidden: true)
            callApiChargingHistory(date)
        case 1:
            date = "30"
            changeDateToUI(onOFF: false)
            setDateActionUI(hidden: true)
            callApiChargingHistory(date)
        case 2:
            getDateForMatter(month: 0)
            dateLabel.text = date
            changeDateToUI(onOFF: true)
            setDateActionUI(hidden: false)
            callApiChargingHistory(dateForServer())
        default: break
        }
    }
    
    @IBAction func cutMonth(_ sender: UIButton) {
        month -= 1
        getDateForMatter(month: month)
        dateLabel.text = date
        callApiChargingHistory(dateForServer())
    }
    
    @IBAction func addMonth(_ sender: UIButton) {
        if month < 0 {
            month += 1
            getDateForMatter(month: month)
            dateLabel.text = date
            callApiChargingHistory(dateForServer())
        }
    }
    
    @objc private func cutMonthAction() {
        month -= 1
        getDateForMatter(month: month)
        dateLabel.text = date
        callApiChargingHistory(dateForServer())
    }
    
    @objc private func addMonthAction() {
        if month < 0 {
            month += 1
            getDateForMatter(month: month)
            dateLabel.text = date
            callApiChargingHistory(dateForServer())
        }
    }
}


//MARK: Table view delegate & data source
extension ChargingHistroyListVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return histroyList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "UserChargingHistoryCell", for: indexPath) as? UserChargingHistoryCell {
            cell.data = histroyList[indexPath.row]
            cell.index = indexPath.row
            cell.delegate = self
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = ChargingHistoryVC(nibName: "ChargingHistoryVC", bundle: nil)
        vc.status = .ProfileToHistory
        vc.tranNo = histroyList[indexPath.row].tranNo ?? 0 
        self.navigationController?.pushViewController(vc, animated: true)
    }
}


//MARK: Api
extension ChargingHistroyListVC {
    func callApiChargingHistory(_ getDate: String) {
        print("getDate \(getDate)")
        KJLRequest.requestForSetChargingHistory(getDate) {[weak self] (response) in
            guard let response = response as? ChargingHistoryModel else { return }
            if response.resCode != "0000" {
                self?.histroyList.removeAll()
                self?.historyTable.reloadData()
            } else {
                self?.histroyList.removeAll()
                let data = response.datas ?? []
                self?.histroyList = data
                self?.historyTable.reloadData()
            }
        }
    }
}


//MARK: Protocol
extension ChargingHistroyListVC: ChargingHistoryDelegate {
    func gotoChargingHistoryDetail(index: Int) {
        let vc = ChargingHistoryVC(nibName: "ChargingHistoryVC", bundle: nil)
        vc.status = .ProfileToHistory
        vc.tranNo = histroyList[index].tranNo ?? 0
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
