//
//  ProfileDetailVC.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/10.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class ProfileDetailVC: UIViewController {

    @IBOutlet weak var profileTable: UITableView!
    
    weak var delegate: ProfileDetailDelegate?
    
    var profileList: ProfileClass?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        callProfile()
    }
 
    fileprivate func setTable() {
        profileTable.layer.borderWidth = 1
        profileTable.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        profileTable.register(UINib(nibName: "SecurityCell", bundle: nil), forCellReuseIdentifier: "SecurityCell")
        profileTable.register(UINib(nibName: "AccountCell", bundle: nil), forCellReuseIdentifier: "AccountCell")
        
    }
}


//MARK: Api
extension ProfileDetailVC {
    fileprivate func callProfile() {
        KJLRequest.requestForProfile { (response) in
            guard let data = response as? ProfileClass else { return }
            KJLRequest.shareInstance().profile = data
            self.profileList = data
            self.profileTable.reloadData()
        }
    }
    
    /// Update user image
    fileprivate func updateUserImage(image: UIImage?) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            let str = KJLCommon.convertImageToBase64(image: image ?? UIImage())
            KJLRequest.requestForUploadMemberPic(picBytes: str) {[weak self] (response) in
                guard response != nil else { return }
                self?.callProfile()
            }
        }
    }
    
    /// edit profile for nick name
    fileprivate func updateEditProfile(name: String) {
        KJLRequest.requestForEditProfile(nickName: name, sex: "", carNo: "", backupEmail: "") {[weak self] (response) in
            if response != nil {
                self?.callProfile()
                self?.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func selectSourceType(_ sourceType: UIImagePickerController.SourceType) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.allowsEditing = true
            myPickerController.sourceType = sourceType
            myPickerController.modalPresentationStyle = .fullScreen
            present(myPickerController, animated: true, completion: nil)
        }
    }
}


//MARK: Table View delegate & data source
extension ProfileDetailVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "SecurityCell", for: indexPath) as? SecurityCell {
                cell.data     = profileList
                cell.delegate = self
                return cell
            } else { return UITableViewCell() }
        } else if indexPath.row == 1 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "AccountCell", for: indexPath) as? AccountCell {
                cell.titleLabel.text = "Account"
                cell.valueLabel.text = profileList?.datas?.mails?[0].email ?? ""
                cell.tapBtn.isHidden = true
                return cell
            } else { return UITableViewCell() }
        } else if indexPath.row == 2 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "AccountCell", for: indexPath) as? AccountCell {
                cell.titleLabel.text = "Subscription Validity"
                cell.valueLabel.text = profileList?.datas?.subscriptionMail ?? ""
                cell.tapBtn.setTitle("Activate", for: .normal)
                cell.tapBtn.isHidden = profileList?.datas?.subscription ?? true
                return cell
            } else { return UITableViewCell() }
        } else if indexPath.row == 3 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "AccountCell", for: indexPath) as? AccountCell {
                cell.titleLabel.text = "Payment Method"
                // payment number
                var numbers = ""
                if let creditCard = profileList?.datas?.cardNum,
                   creditCard != "" {
                    let end = creditCard.index(creditCard.endIndex, offsetBy: 0)
                    let start = creditCard.index(creditCard.endIndex, offsetBy: -4)
                    let number = String(creditCard[start..<end])
                    
                    let type   = profileList?.datas?.cardType ?? ""
                    numbers = type + "⋯⋯" + number
                }
                cell.valueLabel.text = numbers
                cell.tapBtn.isHidden = true
                return cell
            } else { return UITableViewCell() }
        } else if indexPath.row == 4 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "AccountCell", for: indexPath) as? AccountCell {
                cell.titleLabel.text = "Partner Account"
//                cell.valueLabel.text = profileList?.datas?.mails?[0].email ?? ""
                cell.tapBtn.isHidden = profileList?.datas?.subscription ?? true
                return cell
            } else { return UITableViewCell() }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}


//MARK: Image Picker
extension ProfileDetailVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            if let data = UIImageJPEGRepresentation(image, 0.5) {
                updateUserImage(image: UIImage(data: data))
            } else {
                updateUserImage(image: image)
            }
        } else {
            print("Something went wrong")
        }
        dismiss(animated: true, completion: nil)
    }
}


//MARK: User Photo
extension ProfileDetailVC: POPImagePickProtocol {
    func popImagePick() {
        let control = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let ac1 = UIAlertAction(title: "從相簿選取", style: .default, handler: { (alertAction) in
            self.selectSourceType(.photoLibrary)
        })
         
        let ac2 = UIAlertAction(title: "開啟相機", style: .default, handler: { (alertAction) in
            if KJLCommon.authorizeForCamera() == true {
                self.selectSourceType(.camera)
            }
        })
        
        let ac3 = UIAlertAction(title: "預設大頭貼", style: .default, handler: { (alertAction) in
            self.updateUserImage(image: KJLCommon.defaultImage2())
        })
        
        let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        control.addAction(ac1)
        control.addAction(ac2)
        control.addAction(ac3)
        control.addAction(cancel)
        self.present(control, animated: true, completion: nil)
    }
    
    func popEditView() {
        let vc = ShowTextFieldInformationVC()
        vc.delegate = self
        vc.content  = KJLRequest.shareInstance().profile?.datas?.nickName ?? ""
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        present(vc, animated: true, completion: nil)
    }
}


//MARK: User name
extension ProfileDetailVC: ShowTextFieldInformationDelegate {
    func tapConfirm(text: String, delegateTag: Int) {
        updateEditProfile(name: text)
    }
    
    func tapCancel() {
        dismiss(animated: true, completion: nil)
    }
}
