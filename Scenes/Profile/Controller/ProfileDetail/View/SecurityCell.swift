//
//  SecurityCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/21.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

protocol POPImagePickProtocol: class {
    func popImagePick()
    func popEditView()
}

class SecurityCell: UITableViewCell { 
    @IBOutlet weak var borderView: UIImageView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var porscheIcon: UIImageView!
    @IBOutlet weak var editImageBtn: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var editNameBtn: UIButton!
    
    weak var delegate: POPImagePickProtocol?
    var data: ProfileClass? {
        didSet {
            guard let datas = data?.datas else { return }
            userImage.setImageWithUrlPath(datas.picUrl ?? "")
            porscheIcon.isHidden = datas.porscheMan == true ? false : true 
            nameLabel.textColor  = datas.nickName == "" ? #colorLiteral(red: 0.7882352941, green: 0.7921568627, blue: 0.7960784314, alpha: 1) : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            nameLabel.text       = datas.nickName ?? "Enter your nickname"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        userImage.layer.cornerRadius    = userImage.frame.width / 2
        borderView.layer.cornerRadius   = borderView.frame.width / 2
        borderView.layer.borderWidth    = 2
        borderView.layer.borderColor    = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        editImageBtn.layer.cornerRadius = editImageBtn.frame.width / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func editImage(_ sender: UIButton) {
        delegate?.popImagePick()
    }
    
    
    @IBAction func editName(_ sender: UIButton) {
        delegate?.popEditView()
    }
}
