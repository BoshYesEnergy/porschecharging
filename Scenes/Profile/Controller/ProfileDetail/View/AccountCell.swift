//
//  AccountCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/10.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

protocol AccountCellDeleagte: class {
    func tapPageAction()
}

class AccountCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var tapBtn: UIButton!
     
    var tagNumber = 0
    
    weak var delegate: AccountCellDeleagte?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle  = .none
        tapBtn.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
 
    }
    @IBAction func tap(_ sender: UIButton) {
        delegate?.tapPageAction()
    }
}
