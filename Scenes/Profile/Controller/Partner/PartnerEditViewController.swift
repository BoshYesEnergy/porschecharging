//
//  PartnerEditViewController.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/29.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class PartnerEditViewController: BaseViewController {
    
    @IBOutlet weak var infoTitle: UILabel!
    @IBOutlet weak var partnerEditTable: UITableView!
    
    fileprivate var partner: PartnerModel.Datas?
    fileprivate var isSubscription: Bool = false
    
    var deleteArray: [Int] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setHeaderView(LocalizeUtils.localized(key: "EditPartner"), action: .POP)
        setTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        infoTitle.text = LocalizeUtils.localized(key: "PartnerInfo")
        NotificationCenter.default.addObserver(self, selector: #selector(callApiDeletePartner), name: .apiDeletePartner, object: nil)
        callProfile {
            self.callApiPartner()
        }
    }
}


//MARK: Setting view
extension PartnerEditViewController {
    fileprivate func setTable() {
        partnerEditTable.register(UINib(nibName: "MentodMessageCell", bundle: nil), forCellReuseIdentifier: "MentodMessageCell")
        partnerEditTable.register(UINib(nibName: "TapOneButtonCell", bundle: nil), forCellReuseIdentifier: "TapOneButtonCell")
        partnerEditTable.register(UINib(nibName: "PartnerDetailCell", bundle: nil), forCellReuseIdentifier: "PartnerDetailCell")
        partnerEditTable.register(UINib(nibName: "SharedPartnerDetailCell", bundle: nil), forCellReuseIdentifier: "SharedPartnerDetailCell")
        partnerEditTable.register(UINib(nibName: "MentodMessageCell", bundle: nil), forCellReuseIdentifier: "MentodMessageCell")
        
    }
    
    fileprivate func popErrorMessage(reMsg: String) {
        let vc = ShowMessageAlertVC()
        vc.stringToContent = reMsg
        vc.titleToBtn = LocalizeUtils.localized(key: Language.Confirm.rawValue)
        vc.delegate = self
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        present(vc, animated: true) { }
    }
}


//MARK: Api
extension PartnerEditViewController {
    fileprivate func callProfile(completion: @escaping ()-> Void ) {
        KJLRequest.requestForProfile { (response) in
            guard let data = response as? ProfileClass else { return }
            KJLRequest.shareInstance().profile = data
            self.isSubscription = data.datas?.subscription ?? false
            self.partner = nil
            self.partnerEditTable.reloadData()
            completion()
        }
    }
    
    fileprivate func callApiPartner() {
        KJLRequest.requestForPartner(isSubscription: isSubscription) {[weak self] (response) in
            guard let data = response as? PartnerModel else { return }
            if data.resCode != "0000" {
                self?.popErrorMessage(reMsg: data.resMsg ?? "")
            } else {
                self?.partner = data.datas
                self?.partnerEditTable.reloadData()
            }
        }
    }
    
    @objc fileprivate func callApiDeletePartner() {
        for index in deleteArray {
            let memberNo = "\(partner?.infos?[index].memberNo ?? 0)"
            KJLRequest.requestForDeletePartner(isSubscription: isSubscription, partnerMemberNo: memberNo) {[weak self] (response) in
                guard let response = response as? NormalClass else { return }
                if response.resCode != "0000" {
                    self?.popErrorMessage(reMsg: response.resMsg ?? "")
                } else {
                    self?.partner = nil
                    self?.partnerEditTable.reloadData()
                    self?.callProfile {
                        self?.callApiPartner()
                    }
                }
            }
        }
    }
    
    @objc fileprivate func update() {
        DispatchQueue.main.async {
            self.partner = nil
            self.callProfile {
                self.callApiPartner()
            }
        }
    }
}


//MARK: Table view delegae & data source
extension PartnerEditViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let partnerCount = partner?.infos?.count ?? 0
        if isSubscription {
            return partnerCount + 1
        } else {
            return partnerCount + 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isSubscription {
            if partner != nil {
                if indexPath.row == partner?.infos?.count ?? 0 {
                    if let tapOneButtonCell = tableView.dequeueReusableCell(withIdentifier: "TapOneButtonCell", for: indexPath) as? TapOneButtonCell {
                        tapOneButtonCell.delegate = self
                        tapOneButtonCell.tapBtn.setTitle(LocalizeUtils.localized(key: Language.Remove.rawValue), for: .normal)
                        tapOneButtonCell.setView(custom: false)
                        return tapOneButtonCell
                    } else {
                        return UITableViewCell()
                    }
                } else {
                    if let partnerDetailCell = tableView.dequeueReusableCell(withIdentifier: "PartnerDetailCell", for: indexPath) as? PartnerDetailCell {
                        partnerDetailCell.partner           = partner?.infos?[indexPath.row]
                        partnerDetailCell.delegate          = self
                        partnerDetailCell.checkBtn.isHidden = false
                        partnerDetailCell.index             = indexPath.row
                        partnerDetailCell.checkStatus(selectSingle: deleteArray.contains(indexPath.row))
                        return partnerDetailCell
                    } else {
                        return UITableViewCell()
                    }
                }
            } else {
                return UITableViewCell()
            }
        } else {
            if partner != nil {
                if indexPath.row == partner?.infos?.count ?? 0 {
                    if let tapOneButtonCell = tableView.dequeueReusableCell(withIdentifier: "TapOneButtonCell", for: indexPath) as? TapOneButtonCell {
                        tapOneButtonCell.delegate = self
                        tapOneButtonCell.tapBtn.setTitle(LocalizeUtils.localized(key: Language.Remove.rawValue), for: .normal)
                        tapOneButtonCell.setView(custom: false)
                        return tapOneButtonCell
                    } else {
                        return UITableViewCell()
                    }
                } else {
                    if let sharedPartnerDetailCell = tableView.dequeueReusableCell(withIdentifier: "SharedPartnerDetailCell", for: indexPath) as? SharedPartnerDetailCell {
                        sharedPartnerDetailCell.partner             = partner?.infos?[indexPath.row]
                        sharedPartnerDetailCell.delegate            = self
                        sharedPartnerDetailCell.index               = indexPath.row
                        sharedPartnerDetailCell.checkBtn.isHidden   = false
                        return sharedPartnerDetailCell
                    } else {
                        return UITableViewCell()
                    }
                }
            } else {
                return UITableViewCell()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}


//MARK: Protocol
extension PartnerEditViewController: TapOneButtonProtocol, PartnerDetailProtocol, SharedPartnerDetailProtocol { 
    func tapAction(tagNumber: Int) {
        if partner != nil { 
            let vc = ShowInformationVC()
            vc.modalPresentationStyle   = .overFullScreen
            vc.modalTransitionStyle     = .crossDissolve
            vc.cancelIsHidden           = false
            vc.titleString              = LocalizeUtils.localized(key: "Information")
            vc.contentString            = LocalizeUtils.localized(key: "PartnerAlertMessage")
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func appendSharedToRemoveList(index: Int) {
        if deleteArray.contains(index) == false {
            deleteArray.append(index)
            partnerEditTable.reloadData()
            print("deleteArray: \(deleteArray)")
        } else {
            let newList = deleteArray.filter{ $0 != index }
            deleteArray.removeAll()
            deleteArray = newList
            partnerEditTable.reloadData()
            print("deleteArray: \(deleteArray)")
        }
    }
    
    func appendToRemoveList(index: Int) {
        if deleteArray.contains(index) == false {
            deleteArray.append(index)
            partnerEditTable.reloadData()
            print("deleteArray: \(deleteArray)")
        } else {
            let newList = deleteArray.filter{ $0 != index }
            deleteArray.removeAll()
            deleteArray = newList
            partnerEditTable.reloadData()
            print("deleteArray: \(deleteArray)")
        }
    }
}


extension PartnerEditViewController: ShowMessageAlertDelegate {
    func messageTapAction() {
        self.dismiss(animated: true, completion: nil)
    }
}
