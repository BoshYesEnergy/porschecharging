//
//  PartnerViewController.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/23.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

protocol PartnerDelegate: class {
    func reloadPartnerViewcontroller()
}

class PartnerViewController: UIViewController {
    
    @IBOutlet weak var partnerTable: UITableView!
    
    fileprivate var partner: PartnerModel.Datas?
    fileprivate var isSubscription: Bool = false
    fileprivate var isPartner: Bool = false
    fileprivate var isVoucher: Bool = false
    var shareQrcode = ""
    
    var timer: Timer?
    var count = 0
    var total = 50
    
    var confirmCount = 0
    
    var delegate: PartnerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTable()
        partnerTable.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNotification()
        DispatchQueue.main.async {
            self.callProfile {
                self.callApiPartner()
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        DispatchQueue.main.async {
            self.stopTimer()
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        DispatchQueue.main.async {
            self.stopTimer()
            self.confirmCount = 0
        }
    }
}


//MARK: Setting view
extension PartnerViewController {
    fileprivate func setTable() {
        partnerTable.register(UINib(nibName: "TapOneButtonCell", bundle: nil), forCellReuseIdentifier: "TapOneButtonCell")
        partnerTable.register(UINib(nibName: "PartnerDetailCell", bundle: nil), forCellReuseIdentifier: "PartnerDetailCell")
        partnerTable.register(UINib(nibName: "SharedPartnerDetailCell", bundle: nil), forCellReuseIdentifier: "SharedPartnerDetailCell")
        partnerTable.register(UINib(nibName: "MentodMessageCell", bundle: nil), forCellReuseIdentifier: "MentodMessageCell")
        partnerTable.register(UINib(nibName: "ShareQrcodeCell", bundle: nil), forCellReuseIdentifier: "ShareQrcodeCell")
        partnerTable.register(UINib(nibName: "PartnerInformationCell", bundle: nil), forCellReuseIdentifier: "PartnerInformationCell")
        
    }
    
    fileprivate func setNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(startTimer), name: .apiUpdatePartner, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(partnerRequest), name: .checkPartnerRequest, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(confirmPartner), name: .confirmPartner, object: nil)
    }
    
    fileprivate func popErrorMessage(reMsg: String) {
        let vc = ShowMessageAlertVC()
        vc.stringToContent = reMsg
        vc.titleToBtn = LocalizeUtils.localized(key: Language.Confirm.rawValue)
        vc.delegate = self
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        present(vc, animated: true) { }
    }
}


//MARK: Api
extension PartnerViewController {
    fileprivate func callProfile(completion: @escaping ()-> Void ) {
        KJLRequest.requestForProfile {[weak self] (response) in
            guard let data = response as? ProfileClass else { return }
            KJLRequest.shareInstance().profile = data
            self?.isSubscription = data.datas?.subscription ?? false
            self?.isVoucher      = data.datas?.voucher ?? false
            self?.partner        = nil
            
            if data.datas?.subscription == true {
                if data.datas?.partnerMails?.count == 0 {
                    self?.callApiGenerateQrCode()
                }
            }
            completion()
        }
    }
    
    fileprivate func callApiPartner() {
        count += 1
//        print("count: \(count)")
        KJLRequest.requestForPartner(isSubscription: isSubscription) {[weak self] (response) in
            guard let data = response as? PartnerModel else {
                self?.partnerTable.reloadData()
                return }
            if data.resCode != "0000" && data.resCode != "9000" {
                self?.popErrorMessage(reMsg: data.resMsg ?? "")
                self?.partnerTable.isHidden = false
            } else {
                self?.partner = data.datas
                self?.partnerTable.reloadData()
                self?.partnerTable.isHidden = false
            }
        }
    }
    
    @objc fileprivate func confirmPartner(delete: Bool, completion: @escaping ()-> Void ) {
        if confirmCount == 0 {
            confirmCount += 1
            print("confirmCount: \(confirmCount)")
            let qrcode = UserDefaults.standard.string(forKey: "shareQRcode") ?? ""
            KJLRequest.requestForConfirmPartner(qrcode, delete: delete) {[weak self] (response) in
                guard let response = response as? NormalClass else { return }
                if response.resCode != "0000" {
                    self?.popErrorMessage(reMsg: response.resMsg ?? "")
                    self?.stopTimer()
                } else {
                    self?.stopTimer()
                    self?.update()
                }
            }
        }
        completion()
    }
    
    fileprivate func callApiGenerateQrCode() {
        KJLRequest.requestForGenerateQrCode {[weak self] (response) in
            guard let data = response as? GenerateQrCodeModel else { return }
            if data.resCode != "0000" {
                self?.popErrorMessage(reMsg: data.resMsg ?? "")
            } else {
                self?.shareQrcode = data.datas?.qrcode ?? ""
                UserDefaults.standard.set(data.datas?.qrcode ?? "", forKey: "shareQRcode")
                self?.partnerTable.reloadData()
                self?.startTimer()
                self?.partnerTable.isHidden = false
            }
        }
    }
    
    @objc fileprivate func callApiCheckPartner() {
        KJLRequest.requestForCheckPartner(qrcode: shareQrcode, subscription: true) { (response) in
            guard let data = response as? CheckPartnerModel else { return }
            guard data.resCode == "0000",
                let datas = data.datas
                else { return }
            NotificationCenter.default.post(name: .checkPartnerRequest, object: self, userInfo: nil)
        }
    }
    
    @objc fileprivate func update() {
        DispatchQueue.main.async {
            self.partner = nil
            self.callProfile {
                self.callApiPartner() 
            }
        }
    }
    
    @objc fileprivate func partnerRequest() {
        showCheckMessage()
    }
    
    @objc func startTimer() {
        if count == total {
            stopTimer()
        } else {
            if isSubscription == true {
                timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(callApiCheckPartner), userInfo: nil, repeats: true)
            } else {
                timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(update), userInfo: nil, repeats: true)
            }
        }
    }
    
    func stopTimer() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
            print(" Partner update,\n stop timer ")
        }
    }
}


//MARK: Table view delegae & data source
extension PartnerViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let partnerCount = partner?.infos?.count ?? 0
        if isSubscription {
            if partner != nil {
                if partner?.porscheMan == true {
                    return partner != nil ? partnerCount + 2 : 2
                } else {
                    return partner != nil ? partnerCount + 1 : 2
                }
            } else {
                return 1
            }
        } else {
            return partner != nil ? partnerCount + 0 : 2
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isSubscription {
            if partner != nil {
                if partner?.porscheMan == true {
                    if indexPath.row == 0 {
                        if let cell = tableView.dequeueReusableCell(withIdentifier: "PartnerInformationCell", for: indexPath) as? PartnerInformationCell {
                            cell.delegate = self
                            return cell
                        } else { return UITableViewCell() }
                    } else if indexPath.row == (partner?.infos?.count ?? 0) + 1 {
                        if let tapOneButtonCell = tableView.dequeueReusableCell(withIdentifier: "TapOneButtonCell", for: indexPath) as? TapOneButtonCell {
                            tapOneButtonCell.delegate = self
                            tapOneButtonCell.tapBtn.setTitle(LocalizeUtils.localized(key: Language.Remove.rawValue), for: .normal)
                            tapOneButtonCell.setView(custom: false)
                            return tapOneButtonCell
                        } else {
                            return UITableViewCell()
                        }
                    } else {
                        if let partnerDetailCell = tableView.dequeueReusableCell(withIdentifier: "PartnerDetailCell", for: indexPath) as? PartnerDetailCell {
                            partnerDetailCell.titleLabel.text = LocalizeUtils.localized(key: "PartnerAccount")
                            partnerDetailCell.partner = partner?.infos?[indexPath.row - 1]
                            stopTimer()
                            return partnerDetailCell
                        } else {
                            return UITableViewCell()
                        }
                    }
                } else {
                    if indexPath.row == (partner?.infos?.count ?? 0) {
                        if let tapOneButtonCell = tableView.dequeueReusableCell(withIdentifier: "TapOneButtonCell", for: indexPath) as? TapOneButtonCell {
                            tapOneButtonCell.delegate = self
                            tapOneButtonCell.tapBtn.setTitle(LocalizeUtils.localized(key: Language.Remove.rawValue), for: .normal)
                            tapOneButtonCell.setView(custom: false)
                            return tapOneButtonCell
                        } else {
                            return UITableViewCell()
                        }
                    } else {
                        if let partnerDetailCell = tableView.dequeueReusableCell(withIdentifier: "PartnerDetailCell", for: indexPath) as? PartnerDetailCell {
                            partnerDetailCell.titleLabel.text = LocalizeUtils.localized(key: "PartnerAccount")
                            partnerDetailCell.partner = partner?.infos?[indexPath.row]
                            stopTimer()
                            return partnerDetailCell
                        } else {
                            return UITableViewCell()
                        }
                    }
                }
            } else {
                if let shareQrcodeCell = tableView.dequeueReusableCell(withIdentifier: "ShareQrcodeCell", for: indexPath) as? ShareQrcodeCell {
                    shareQrcodeCell.qrCode = shareQrcode
                    return shareQrcodeCell
                } else {
                    return UITableViewCell()
                }
            }
        } else {
            if partner != nil {
                    if let sharedPartnerDetailCell = tableView.dequeueReusableCell(withIdentifier: "SharedPartnerDetailCell", for: indexPath) as? SharedPartnerDetailCell {
                        sharedPartnerDetailCell.partner = partner?.infos?[indexPath.row]
                        stopTimer()
                        return sharedPartnerDetailCell
                    } else {
                        return UITableViewCell()
                    }
            } else {
                if indexPath.row == 0 {
                    if let mentodMessageCell = tableView.dequeueReusableCell(withIdentifier: "MentodMessageCell", for: indexPath) as? MentodMessageCell {
                        mentodMessageCell.message.text = LocalizeUtils.localized(key: "NoPartnerMessage")
                        return mentodMessageCell
                    } else {
                        return UITableViewCell()
                    }
                } else {
                    if let tapOneButtonCell = tableView.dequeueReusableCell(withIdentifier: "TapOneButtonCell", for: indexPath) as? TapOneButtonCell {
                        tapOneButtonCell.delegate = self
                        if isVoucher == true {
                            tapOneButtonCell.tapBtn.setTitle(LocalizeUtils.localized(key: "AddPartner"), for: .normal)
                        } else {
                            tapOneButtonCell.tapBtn.setTitle(LocalizeUtils.localized(key: "ScanPartner"), for: .normal)
                        }
                        tapOneButtonCell.setView(custom: false)
                        return tapOneButtonCell
                    } else {
                        return UITableViewCell()
                    }
                }
            }
        }
    }
}


//MARK: Protocol
extension PartnerViewController: TapOneButtonProtocol {
    func tapAction(tagNumber: Int) {
        if partner != nil {
            let vc = PartnerEditViewController(nibName: "PartnerEditViewController", bundle: nil)
            vc.hiddenNavigationBar(isHidden: true)
            navigationController?.pushViewController(vc, animated: true)
        } else {
            if isSubscription { // 有訂閱的
                let vc = ShareQRcodeViewController()
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle   = .crossDissolve
                vc.content = LocalizeUtils.localized(key: "PartnerShareQrcodeContent")
                present(vc, animated: true, completion: nil)
            } else {
                ///Scan
                let vc     = ScanQRcodeViewController()
                vc.status  = .Partner
                vc.content = LocalizeUtils.localized(key: "ScanPartner")
                present(vc, animated: true, completion: nil)
            }
        }
    } 
}


extension PartnerViewController: PartnerInformationDelegate {
    func tapShareQrcode() { 
        let vc = ShareQRcodeViewController()
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        vc.content = LocalizeUtils.localized(key: "PartnerShareQrcodeContent")
        present(vc, animated: true, completion: nil)
    }
}


extension PartnerViewController: ShowMessageAlertDelegate {
    func messageTapAction() {
        self.dismiss(animated: true, completion: nil)
    }
}


extension PartnerViewController :ShowCheckMessageDelegate {
    func tapConfirm(tagCount: Int) {
        DispatchQueue.main.async {
            self.confirmPartner(delete: false){
                self.confirmCount = 0
            }
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func tapCancel() {
        DispatchQueue.main.async {
            self.confirmPartner(delete: true){
                self.confirmCount = 0
            }
            self.delegate?.reloadPartnerViewcontroller()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func showCheckMessage () {
        let vc = ShowCheckMessageVC()
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        vc.toTitle = ""
        vc.toContent = LocalizeUtils.localized(key: "AddChargingPartner")
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
}
