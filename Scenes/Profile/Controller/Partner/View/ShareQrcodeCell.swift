//
//  ShareQrcodeCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/8.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class ShareQrcodeCell: UITableViewCell {

    @IBOutlet weak var qrCodeImage: UIImageView!
    @IBOutlet weak var contentLabel: UILabel!
     
    var qrCode: String? {
        didSet {
            guard let code = qrCode else { return }
            qrCodeImage.isHidden = false
            qrCodeImage.image = createQRcode(string: code) 
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        qrCodeImage.isHidden = true
        contentLabel.text = LocalizeUtils.localized(key: "PartnerShareQrcodeContent")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    fileprivate func createQRcode(string: String) -> UIImage? {
        let data = string.data(using: .ascii)
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 10, y: 10) // 修正模糊
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        return nil
    }
    
}
