//
//  PartnerInformationCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/8.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

protocol PartnerInformationDelegate: class {
    func tapShareQrcode()
}

class PartnerInformationCell: UITableViewCell {

    @IBOutlet weak var titlLabel: UILabel!
    @IBOutlet weak var tapBtn: UIButton!
    
    weak var delegate: PartnerInformationDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        titlLabel.text = LocalizeUtils.localized(key: "YourPartner")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func tap(_ sender: UIButton) {
        delegate?.tapShareQrcode()
    }
}
