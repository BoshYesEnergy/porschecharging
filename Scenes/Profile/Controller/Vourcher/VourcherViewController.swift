//
//  VourcherViewController.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/23.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class VourcherViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var validityTitle: UILabel!
    @IBOutlet weak var validityEndTitle: UILabel!
    @IBOutlet weak var sendByTitle: UILabel!
    
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var validityFrom: UILabel!
    @IBOutlet weak var validtyEnd: UILabel!
    @IBOutlet weak var SendBy: UILabel!
    @IBOutlet weak var borderView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        callApiGetVoucher()
        setLanguage()
    }

    fileprivate func setView() {
        borderView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        borderView.layer.borderWidth = 1
        borderView.isHidden          = true
        iconImage.isHidden           = true
    }
    
    func setLanguage() {
        titleLabel.text = LocalizeUtils.localized(key: "VoucherOneTimeTitle")
        validityTitle.text = LocalizeUtils.localized(key: "VoucherFrom")
        validityEndTitle.text = LocalizeUtils.localized(key: "VoucherEnd")
        sendByTitle.text = LocalizeUtils.localized(key: "VoucherSendBy")
        message.text = LocalizeUtils.localized(key: "noV")
    }
    
    fileprivate func popErrorMessage(reMsg: String) {
        let vc = ShowMessageAlertVC()
        vc.stringToContent = reMsg
        vc.titleToBtn = LocalizeUtils.localized(key: Language.Confirm.rawValue)
        vc.delegate = self
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        present(vc, animated: true) { }
    }
    
    fileprivate func callApiGetVoucher() {
        let mail = KJLRequest.shareInstance().profile?.datas?.mails?[0].email ?? ""
        KJLRequest.requestForGetVoucher(email: mail) {[weak self] (response) in
            guard let data = response as? GetVoucherModel else {
                
                return }
            if data.resCode != "0000" {
                self?.popErrorMessage(reMsg: data.resMsg ?? "")
            } else {
                if data.datas?.issueStime == "" && data.datas?.issueEtime == "" {
                    self?.borderView.isHidden = true
                    self?.iconImage.isHidden  = true
                } else {
                    self?.message.text = ""
                    self?.borderView.isHidden = false
                    self?.iconImage.isHidden  = false
                    self?.validityFrom.text   = data.datas?.issueStime
                    self?.SendBy.text         = data.datas?.companyName
                    if data.datas?.unlimit == true {
                        self?.validtyEnd.text = LocalizeUtils.localized(key: "Unlimited")
                    } else {
                        self?.validtyEnd.text = data.datas?.issueEtime
                    }
                }
            } 
        }
    }
}


extension VourcherViewController: ShowMessageAlertDelegate {
    func messageTapAction() {
        self.dismiss(animated: true, completion: nil)
    }
}
