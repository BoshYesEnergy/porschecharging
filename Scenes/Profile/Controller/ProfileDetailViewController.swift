//
//  ProfileDetailViewController.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/20.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit
import SDWebImage

protocol ProfileDetailDelegate: class {
    func gotoActivate()
    func gotoAddPartner()
}

class ProfileDetailViewController: UIViewController {
    @IBOutlet weak var accountTitle: UILabel!
    @IBOutlet weak var subscroptionValidityTitle: UILabel!
    @IBOutlet weak var paymentTitle: UILabel!
    @IBOutlet weak var partnerTitle: UILabel!
     
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var borderImage: UIImageView!
    @IBOutlet weak var porscheIcon: UIImageView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var paymentMethed: UILabel!
    @IBOutlet weak var validityTime: UILabel!
    @IBOutlet weak var partnerAccount: UILabel!
     
    @IBOutlet weak var subStackView: UIStackView!
    @IBOutlet weak var paymentStackView: UIStackView!
    @IBOutlet weak var partnerStactView: UIStackView!
    
    @IBOutlet weak var activateBtn: UIButton!
    @IBOutlet weak var addPartnerBtn: UIButton!
    @IBOutlet weak var logOutBtn: UIButton!
    @IBOutlet weak var logoutTopConstraint: NSLayoutConstraint!
     
    weak var delegate: ProfileDetailDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) 
        DispatchQueue.main.async {
            self.setLanguage()
            self.callProfile()
        } 
    }
}


//MARK: Set View
extension ProfileDetailViewController {
    fileprivate func setView() {
        borderView.layer.borderWidth   = 1
        borderView.layer.borderColor   = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        borderImage.layer.borderWidth  = 2
        borderImage.layer.borderColor  = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        borderImage.layer.cornerRadius = borderImage.frame.width / 2
        userImage.layer.cornerRadius   = userImage.frame.width / 2
        userName.text                  = ""
        email.text                     = ""
        validityTime.text              = ""
        partnerAccount.text            = ""
        paymentMethed.text             = ""
        porscheIcon.isHidden           = true
        addPartnerBtn.isHidden         = true 
        
    }
    
    func setLanguage() {
        setCharginBtnUI(button: logOutBtn, toString: LocalizeUtils.localized(key: Language.Logout.rawValue))
        accountTitle.text = LocalizeUtils.localized(key: Language.Account.rawValue)
        activateBtn.setTitle(LocalizeUtils.localized(key: Language.Activate.rawValue), for: .normal)
        paymentTitle.text = LocalizeUtils.localized(key: Language.Payment.rawValue)
        partnerTitle.text = LocalizeUtils.localized(key: Language.Partner.rawValue)
        addPartnerBtn.setTitle(LocalizeUtils.localized(key: Language.AddPartner.rawValue), for: .normal)
        
        /// if not have subscription End Date will show ...**/xx/**
        if KJLRequest.shareInstance().profile?.datas?.subscriptionEndDate ?? ""  == "" {
            subscroptionValidityTitle.text = LocalizeUtils.localized(key: "Subscription")
        } else {
            subscroptionValidityTitle.text = LocalizeUtils.localized(key: Language.Validity.rawValue)
        }
    }
    
    private func setCharginBtnUI(button: UIButton, toString: String) {
        if let font = UIFont(name: "PorscheNext-Regular", size: 16) {
            let attrs: [ NSAttributedStringKey : Any] =
                [ NSAttributedStringKey.font : font,
                  NSAttributedStringKey.foregroundColor : #colorLiteral(red: 0.5882352941, green: 0.5960784314, blue: 0.6039215686, alpha: 1),
                  .underlineStyle : NSUnderlineStyle.styleSingle.rawValue ]
            
            let buttonTitleStr = NSMutableAttributedString(string: toString, attributes: attrs)
            button.setAttributedTitle(buttonTitleStr, for: .normal)
        }
    }
}


//MARK: Action
extension ProfileDetailViewController {
    @IBAction func editImage(_ sender: UIButton) {
        popImagePick()
    }
    
    @IBAction func editName(_ sender: UIButton) {
        let vc = ShowTextFieldInformationVC()
        vc.delegate = self 
        vc.content  = KJLRequest.shareInstance().profile?.datas?.nickName ?? ""
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func activate(_ sender: UIButton) {
        delegate?.gotoActivate()
    }
    
    @IBAction func addPartner(_ sender: UIButton) {
        delegate?.gotoAddPartner()
    }
    
    @IBAction func logOut(_ sender: UIButton) {
        logoutAction() 
    }
    
    fileprivate func logoutAction() {
        present(.confirmAlert(LocalizeUtils.localized(key: Language.tologout.rawValue),
                              "",
                              LocalizeUtils.localized(key: Language.Logout.rawValue), {
                                KJLRequest.requestForLogout { (response) in
                                    UIViewController.pushLogOut()
                                    KJLRequest.shareInstance().loginMethod = .none
                                    KJLRequest.logout()
                                } 
            UserDefaults.standard.set(0, forKey: "UnReadCount")
            NotificationCenter.default.post(name: .changeScanTabImage, object: nil)
            self.dismiss(animated: true, completion: nil)
        },  LocalizeUtils.localized(key: Language.Cancel.rawValue), { }), animated: true, completion: nil)
    }
    
    fileprivate func popImagePick() {
        let control = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let ac1 = UIAlertAction(title: "從相簿選取", style: .default, handler: { (alertAction) in
            self.selectSourceType(.photoLibrary)
        })
         
        let ac2 = UIAlertAction(title: "開啟相機", style: .default, handler: { (alertAction) in
            if KJLCommon.authorizeForCamera() == true {
                self.selectSourceType(.camera)
            }
        })
        
        let ac3 = UIAlertAction(title: "預設大頭貼", style: .default, handler: { (alertAction) in
            self.updateUserImage(image: KJLCommon.defaultImage2())
        })
        
        let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        control.addAction(ac1)
        control.addAction(ac2)
        control.addAction(ac3)
        control.addAction(cancel)
        self.present(control, animated: true, completion: nil)
    }
    
    func selectSourceType(_ sourceType: UIImagePickerController.SourceType) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.allowsEditing = true
            myPickerController.sourceType = sourceType
            myPickerController.modalPresentationStyle = .fullScreen
            present(myPickerController, animated: true, completion: nil)
        }
    }
}


//MARK: Api
extension ProfileDetailViewController { 
    fileprivate func callProfile() {
        KJLRequest.requestForProfile {[weak self] (response) in
            guard let data = response as? ProfileClass else { return }
            KJLRequest.shareInstance().profile = data
            self?.userName.textColor        = data.datas?.nickName ?? "" == "" ? #colorLiteral(red: 0.7882352941, green: 0.7921568627, blue: 0.7960784314, alpha: 1) : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            self?.userName.text             = data.datas?.nickName ?? ""
            if self?.userName.text == "" {
                self?.userName.text = LocalizeUtils.localized(key: Language.Nickname.rawValue)
            }
            
            // image  
//            self?.userImage.image           = UIImage(named: "")
            if data.datas?.picUrl ?? "" == "" {
                self?.userImage.image = KJLCommon.defaultImage2()
            } else {   
                self?.userImage.sd_setImage(with: URL.init(string: data.datas?.picUrl ?? ""), placeholderImage: KJLCommon.defaultImage2(), options: .refreshCached, completed: nil)
            }
            
            if data.datas?.mails?.count ?? 0 != 0 {
                self?.email.text            = data.datas?.mails?[0].email ?? ""
            } else {
                self?.email.text            = ""
            }
            
            let isSubscription              = data.datas?.subscription ?? false
            
            if isSubscription == true {
                self?.addPartnerBtn.isHidden    = data.datas?.partnerMails?.count ?? 0 > 0 ? true : false
                self?.partnerStactView.isHidden = false
                self?.partnerAccount.isHidden   = data.datas?.partnerMails?.count ?? 0 > 0 ? false : true
            } else {
                self?.addPartnerBtn.isHidden    = true
                self?.partnerStactView.isHidden = true
            }
            let porscheMan = data.datas?.porscheMan ?? true
            self?.porscheIcon.isHidden      = !porscheMan
            
            self?.activateBtn.isHidden      = isSubscription == false ? false : true
            self?.paymentStackView.isHidden = isSubscription == false ? true : false
             
            self?.logoutTopConstraint.constant = isSubscription == false ? 40 : 140
            self?.validityTime.isHidden     = isSubscription == true ? false : true
            self?.validityTime.text         = data.datas?.subscriptionEndDate ?? ""
            
            // payment number
            if let creditCard = data.datas?.cardNum,
               creditCard != "" {
                let end = creditCard.index(creditCard.endIndex, offsetBy: 0)
                let start = creditCard.index(creditCard.endIndex, offsetBy: -4)
                let number = String(creditCard[start..<end])
                
                let type   = data.datas?.cardType ?? ""
                self?.paymentMethed.text = type + "⋯⋯" + number
            } else {
                self?.paymentMethed.text = ""
            }
            #warning("please check api title , and sub did show partner account")
            if isSubscription == true {
                if data.datas?.partnerMails?.count ?? 0 > 0 {
                    self?.partnerAccount.text       = data.datas?.partnerMails?[0].email ?? ""
                } else {
                    self?.partnerAccount.text       = ""
                }
            } else {
                self?.partnerAccount.text       = ""
            }
        } 
    }
    
    /// Update user image
    fileprivate func updateUserImage(image: UIImage?) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let str = KJLCommon.convertImageToBase64(image: image ?? UIImage())
            KJLRequest.requestForUploadMemberPic(picBytes: str) { [weak self] (response) in
                guard response != nil else { return }
                self?.callProfile()
            }
        }
    }
    
    /// edit profile for nick name
    fileprivate func updateEditProfile(name: String) {
        KJLRequest.requestForEditProfile(nickName: name, sex: "", carNo: "", backupEmail: "") {[weak self] (response) in
            if response != nil {
                self?.callProfile()
                self?.dismiss(animated: true, completion: nil)
            }
        }
    }
}
 

//MARK: Image Picker
extension ProfileDetailViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            if let data = UIImageJPEGRepresentation(image, 0.5) {
                updateUserImage(image: UIImage(data: data))
            } else {
                updateUserImage(image: image)
            }
        } else {
            print("Something went wrong")
        }
        dismiss(animated: true, completion: nil)
    }
}


extension ProfileDetailViewController: ShowTextFieldInformationDelegate {
    func tapConfirm(text: String, delegateTag: Int) {
        userName.text = text
        updateEditProfile(name: text)
    } 
    
    func tapCancel() {
        dismiss(animated: true, completion: nil)
    }
}
