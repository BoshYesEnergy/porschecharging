//
//  ChargingExperienceVC.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/23.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class ChargingExperienceVC: UIViewController {

    @IBOutlet weak var startBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) 
        startBtn.setTitle(LocalizeUtils.localized(key: "ChargingExperienceMessage"), for: .normal)
    }
    
    @IBAction func gotoStartVC(_ sender: UIButton) {
        if KJLRequest.isLogin() == true {
            self.dismiss(animated: true, completion: nil)
        } else {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SelectLoginViewController") as! SelectLoginViewController
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}
