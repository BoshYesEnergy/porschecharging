//
//  AppGuideViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class AppGuideViewController: UIViewController {
    
    @IBOutlet weak var kscroll: UIScrollView!
    @IBOutlet weak var scrollTop: NSLayoutConstraint!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var imageView1: UIImageView!
    
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var imageView2: UIImageView!
    
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var imageView3: UIImageView!
    
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var imageView4: UIImageView!
    
    @IBOutlet weak var view5: UIView!
    @IBOutlet weak var iamgeView5: UIImageView!
    
    @IBOutlet weak var view6: UIView!
    @IBOutlet weak var imageView6: UIImageView!
    
    @IBOutlet weak var view8: UIView!
    @IBOutlet weak var imageView8: UIImageView!
    
    @IBOutlet weak var dismissBtn: UIButton!
    
    @IBOutlet weak var pageLabel: UILabel!
    
    var isFirst: Bool = false
    var pageCount: Int = 0
    private var isSHowConfirmationPrivacy = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        settingView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isFirst == true && isSHowConfirmationPrivacy {
            isSHowConfirmationPrivacy = false 
        }
    }
    
    private func setCharginBtnUI(button: UIButton, toString: String) {
        if let font = UIFont(name: "PorscheNext-Regular", size: 16) {
            let attrs: [ NSAttributedStringKey : Any] =
                [ NSAttributedStringKey.font : font,
                  NSAttributedStringKey.foregroundColor : #colorLiteral(red: 0.5882352941, green: 0.5960784314, blue: 0.6039215686, alpha: 1),
                  .underlineStyle : NSUnderlineStyle.styleSingle.rawValue ]
            
            let buttonTitleStr = NSMutableAttributedString(string: toString, attributes: attrs)
            button.setAttributedTitle(buttonTitleStr, for: .normal)
        }
    }
    
    @IBAction func dismissGuide(_ sender: UIButton) {
        popCheckMessage(content: LocalizeUtils.localized(key: "skipContent"), tagCount: 0, showBtn: .two)
    }
    
    @IBAction func backPage(_ sender: UIButton) {
        if pageCount <= 0 {
            pageCount = 0
        } else {
            pageCount = pageCount - 1
            let width = view1.frame.width
            kscroll.setContentOffset(CGPoint(x: width * CGFloat(pageCount), y: 0), animated: true)
        }
    }
    
    @IBAction func nextPage(_ sender: UIButton) {
        if pageCount >= 6 {
            isFirst = true
            sureAction()
        } else {
            pageCount = pageCount + 1
            let width = view1.frame.width
            kscroll.setContentOffset(CGPoint(x: width * CGFloat(pageCount), y: 0), animated: true)
        }
    }
}


//MARK:- Setting View
extension AppGuideViewController {
    private func createView() {
        pageLabel.layer.cornerRadius = pageLabel.frame.width / 2
        pageLabel.layer.masksToBounds = true
        pageLabel.text = "1/7"
    }
    
    func settingView() {
        setCharginBtnUI(button: dismissBtn, toString: LocalizeUtils.localized(key: "skip"))
        let language = LocalizeUtils.shared.toLanguage()
        print(language)
        if language == "en" {
            imageView1.image = UIImage(named: GuideImage.en01.rawValue)
            imageView2.image = UIImage(named: GuideImage.en02.rawValue)
            imageView3.image = UIImage(named: GuideImage.en03.rawValue)
            imageView4.image = UIImage(named: GuideImage.en04.rawValue)
            iamgeView5.image = UIImage(named: GuideImage.en05.rawValue)
            imageView6.image = UIImage(named: GuideImage.en06.rawValue)
            imageView8.image = UIImage(named: GuideImage.en07.rawValue)
        } else {
            imageView1.image = UIImage(named: GuideImage.zh01.rawValue)
            imageView2.image = UIImage(named: GuideImage.zh02.rawValue)
            imageView3.image = UIImage(named: GuideImage.zh03.rawValue)
            imageView4.image = UIImage(named: GuideImage.zh04.rawValue)
            iamgeView5.image = UIImage(named: GuideImage.zh05.rawValue)
            imageView6.image = UIImage(named: GuideImage.zh06.rawValue)
            imageView8.image = UIImage(named: GuideImage.zh07.rawValue)
        }
    }
}


//MARK:- Action
extension AppGuideViewController {
    private func sureAction() {
        guard isFirst == true else { return }
        let vc = ChargingExperienceVC(nibName: "ChargingExperienceVC", bundle: nil)
        navigationController?.pushViewController(vc, animated: true)
    }
}


//MARK:- Scroll View Delegate
extension AppGuideViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let page = lroundf(Float(scrollView.contentOffset.x)/Float(scrollView.frame.width))
        pageLabel.text = String(page + 1) + "/7"
        pageCount = page
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView.contentOffset.x > (scrollView.frame.width * 6) {
            isFirst = true
            sureAction()
        }
    }
}
  

//MARK: ShowCheckMessageVC & delegate
extension AppGuideViewController: ShowCheckMessageDelegate {
    fileprivate func popCheckMessage(content: String, tagCount: Int, showBtn: MessageCheckBtn = .two) {
        let vc = ShowCheckMessageVC()
        vc.delegate  = self
        vc.toContent = content
        vc.toTitle   = ""
        vc.tagCount  = tagCount
        vc.custom    = showBtn
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        present(vc, animated: true, completion: nil)
    }
    
    func tapConfirm(tagCount: Int) {
        self.dismiss(animated: false) {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func tapCancel() {
        self.dismiss(animated: true, completion: nil)
    }
}
