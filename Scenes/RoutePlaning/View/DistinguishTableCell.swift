//
//  DistinguishTableCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

protocol DistinguishTableCellDelegate: class {
    func arrowBtnDidPress()
}

class DistinguishTableCell: UITableViewCell {

    @IBOutlet weak var hintImage: UIImageView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var hintImageWidth: NSLayoutConstraint!
    @IBOutlet weak var arrowBtnWidth: NSLayoutConstraint!
    
    private weak var delegate: DistinguishTableCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        infoLabel.text = ""
        self.arrowBtnWidth.constant = 0
    }
    
    @IBAction func arrowAction(_ sender: UIButton) {
        self.delegate?.arrowBtnDidPress()
    }
    
    func setupCellWith(indexPath: IndexPath , imageTitle: String , distance: String , duration: String , isSelect: Bool , delegate: DistinguishTableCellDelegate) {
        
        self.infoLabel.text = "\(distance) / \(duration)"
        let image = UIImage(named: imageTitle)?.withRenderingMode(.alwaysTemplate)
        self.hintImage.image = image
        self.delegate = delegate
        
        if (indexPath.row == 1) {
            hintImageWidth.constant = 130
        } else {
            hintImageWidth.constant = 112
        }
        let buleColor = UIColor.init(red: 0, green: 196/255, blue: 203/255, alpha: 1)
        if (isSelect == true) {
            self.contentView.backgroundColor = buleColor
            self.hintImage.tintColor = .white
            self.infoLabel.textColor = .white
        } else {
            self.hintImage.tintColor = buleColor
            self.infoLabel.textColor = .black
            self.contentView.backgroundColor = UIColor.white
        }
    }
}
