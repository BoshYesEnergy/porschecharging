//
//  RouteDetailCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class RouteDetailCell: UITableViewCell {

    @IBOutlet weak var hintImage: UIImageView!
    // 起始
    @IBOutlet weak var startView: UIView!
    @IBOutlet weak var startLabel: UILabel!
    @IBOutlet weak var stationLabel: UILabel!
    // 距離
    @IBOutlet weak var distanceView: UIView!
    @IBOutlet weak var distanceLabel: UILabel!
    // 綠色
    @IBOutlet weak var greenView: UIView!
    @IBOutlet weak var greenStationLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.stationLabel.text = ""
        self.distanceLabel.text = ""
        self.greenStationLabel.text = ""
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.distanceView.isHidden = false
    }
    
    func setupCellWith(indexPath: IndexPath , dataString: String , distanceString: String , isLastCell: Bool) {
        // 起始、目的地
        if (indexPath.row == 0 || isLastCell == true) {
            self.greenView.isHidden = true
            self.startView.isHidden = false
            if (indexPath.row == 0) {
                self.startLabel.text = "起點"
                self.hintImage.image = UIImage(named: "plan_ic_site_big")
            } else {
                self.hintImage.image = UIImage(named: "plan_ic_destination_big")
                self.startLabel.text = "目的地"
                self.distanceView.isHidden = true
            }
            self.stationLabel.text = dataString
            self.distanceLabel.text = distanceString
        } else {
            // 綠色
            self.startView.isHidden = true
            self.greenView.isHidden = false
            self.hintImage.image = UIImage(named: "plan_ic_station")
            self.greenStationLabel.text = dataString
            self.distanceLabel.text = distanceString
        }
    }
}
