//
//  RouteDisplayViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit
import GoogleMaps
import SVProgressHUD
import MapKit

enum GoogleRouteAvoidType: String {
    case None = ""
    case Tolls = "tolls"        // 路線應避開 收費公路/橋樑。
    case Highways = "highways"  // 路線應避開 高速公路。
    case Ferries = "ferries"    // 路線應避開 渡輪。
}

enum GeocodingLocationType: String {
    case None = "none"
    case Start = "start"        // 開始位置
    case End = "end"            // 目的地位置
    case Station = "station"    // 充電站位置
    case Coodinate = "coodinate"// 座標轉地址
}

enum DistanceMatrixType: String {
    case None = "none"
    case Begin = "begin"        // 起點到目的地
    case End = "end"            // 站點到目的地
    case Filter = "filter"      // 站點篩選
}

class RouteDisplayViewController: KJLNavView {

    //MARK: - IBOutlet
    @IBOutlet weak var googleMapView: GMSMapView!
    // 路線規劃
    @IBOutlet weak var stationDetailView: UIView!
    @IBOutlet weak var checkListBtn: UIButton!
    @IBOutlet weak var stationNameLabel: UILabel!
    @IBOutlet weak var stationIdLabel: UILabel!
    @IBOutlet weak var stationAmountLabel: UILabel!
    @IBOutlet weak var scoreViewWidth: NSLayoutConstraint!
    @IBOutlet weak var stationInfoView: UIView!
    @IBOutlet weak var hintView: UIView!
    @IBOutlet weak var stationDetailViewHeight: NSLayoutConstraint!
    @IBOutlet weak var stationScoreLabel: UILabel!
    // 路線篩選
    @IBOutlet weak var routeSelectViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var stationInfoBtn: UIButton!
    @IBOutlet weak var addLineBtn: UIButton!
    @IBOutlet weak var stationDistanceLabel: UILabel!
    // MARK: - Property
    private var startLocation = ""
    private var endLocation = ""
    private var startCoodinate: CLLocationCoordinate2D?
    private var endCoodinate: CLLocationCoordinate2D?
    
    private var distinguishNumber: Int = 0
    private var cellImageArray: [String] = ["plan_ic_best","plan_ic_highway","plan_ic_free"]
    private var cellSelectIndex: Int = 0
    
    private var selectPin: CustomPin? = nil
    private var selectDistance: String = ""
    var isShouldSave:Bool = false
    // API 所有的充電站
    private var pinArray: [CustomPin] = []
    private var routePolyline: GMSPolyline!
    private var distanceArray: [String] = ["0 公里","0 公里","0 公里"]
    private var durationArray: [String] = ["0 小時 0 分","0 小時 0 分","0 小時 0 分"]
    private var googleAPIManager = GoogleAPIRequestManager()
    private var avoidType: GoogleRouteAvoidType = .None
    private var pinIndex = 0
    // 已選取的站點
    private var selectCustomPinArray: [CustomPin] = []
    // 要顯示的大頭針
    private var showMarkerArray: [POIItem] = []
    // 站點間的距離
    private var stationCoodinateArray: [CLLocationCoordinate2D] = []
    private var stationDistanceArray: [String] = []
    private var stationToEndDistance: String = ""
    private var routeListArray: [RoutePlanObjClass] = []
    // 儲存站點名稱、距離、站點ID
    private var stationInfoArray: [RouteStationInfoObjClass] = []
    private var temporaryFileName: String = "**CheckTheStationsInList**"
    private var routeSaveName: String = ""
    private var googleMapCircle = GMSCircle()
    private var previousMarker: GMSMarker?
    private var temporaryMarker: GMSMarker?
    private var clusterManager: GMUClusterManager!
    // 在 Google Map 加大頭針
    private func addMarkerOnGoogleMapWith(title: String , coodinate: CLLocationCoordinate2D , geocodingLocationType: GeocodingLocationType) { 
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: coodinate.latitude, longitude: coodinate.longitude)
        marker.title = title
        switch geocodingLocationType {
        case .Start: marker.icon = UIImage(named: "plan_ic_star")
        case .End:   marker.icon = UIImage(named: "plan_ic_stop")
        default: break
        }
        marker.map = self.googleMapView
    }
    
    // 清除路線
    func clearRoute() {
        if (self.routePolyline != nil) {
            routePolyline.map = nil
            routePolyline = nil
        }
    }
    
    func setupVCWith(startLocation: String , endLocation: String , distinguishNumber: Int) {
        self.startLocation = startLocation
        self.endLocation = endLocation
        self.distinguishNumber = distinguishNumber
    }
    
    // MARK: 儲存按鈕
    @objc func saveAction() {
        if isShouldSave == false {
            isShouldSave = true
            self.arrowBtnDidPress()
            self.rightBtn?.setTitle("儲存", for: .normal)
        } else {
            ShowTextFieldMessage.showMessage(title: "儲存路線", message: "儲存前，請在下方更新您的路線名稱，之後比較容易找到") { (textFieldText) in
                    self.routeSaveName = textFieldText
                    /* -------------- 路線清單 -------------- */
                    self.routeListDataActionWith(routeSaveName: textFieldText)
                    /* -------------- 路線詳細資訊 -------------- */
                    self.stationInfoListDataActionWith(routeSaveName: textFieldText)
            }
        }
    }
    
    private func setupTableViewData() {
        guard let startCoodinate = self.startCoodinate,
            let endCoodinate = self.endCoodinate
            else { return }
        // GoogleAPI - 導航路線
        self.callGoogleDirectionAPIWith(startCoodinate: startCoodinate, endCoodinate: endCoodinate, avoidType: .None)
        // GoogleAPI - 最佳路線
        self.callGoogleDistanceMatrixAPIWith(startCoodinate: startCoodinate, endCoodinate: endCoodinate, avoidType: .None , resultType: .Begin)
        // GoogleAPI - 避免高速公路
        self.callGoogleDistanceMatrixAPIWith(startCoodinate: startCoodinate, endCoodinate: endCoodinate, avoidType: .Highways , resultType: .Begin)
        // GoogleAPI - 避免收費站
        self.callGoogleDistanceMatrixAPIWith(startCoodinate: startCoodinate, endCoodinate: endCoodinate, avoidType: .Tolls ,resultType: .Begin)
    }
    
    // 設定站點大頭針並顯示
    func showStationPins(stationListDatas: [StationListClass.Data]) {
        var tempDatas = [StationListClass.Data]()
        var index = 0
        for aData in stationListDatas {
            if aData.type == "1" {   // select car
                tempDatas.append(aData)
            } else {
                if aData.type == "2" {   // select motorcycle
                    tempDatas.append(aData)
                }
            }
            
            if aData.type == "3" {   // 測試站
                tempDatas.append(aData)
            }
        }
        
        for (i,tempData) in tempDatas.enumerated() {
            let location = CLLocationCoordinate2D(latitude: Double(tempData.latitude ?? "0") ?? 0, longitude: Double(tempData.longitude ?? "0") ?? 0)
            let pin = CustomPin(coordinate: location)
            pin.title = tempData.stationName
            pin.pk = tempData.stationId ?? ""
            pin.num = "\(i + 1)"
            if i + 1 > 19 {
                pin.num = "0"
            } 
            // 站點狀態：01: 可使用 , 02: 使用中 , 03: 未連線 , 04: 未營運
            var imgName: String = AnnotationType.NONE.rawValue
            if tempData.hasDC == true {
                imgName = MapManger.dcICON(tempData)
            } else {
                imgName = MapManger.acICON(tempData)
            }
            pin.imageName = imgName
            pin.data = tempData
            let marker = POIItem(position: CLLocationCoordinate2DMake(location.latitude, location.longitude), name: pin.title!)
            marker.iconImage = UIImage(named: pin.imageName)
            marker.index = index
            index += 1
            self.showMarkerArray.append(marker)
            self.pinArray.append(pin)
            clusterManager.add(marker)
        }
        clusterManager.cluster()
    }
    
    private func drawCircleWith(coodinate: CLLocationCoordinate2D) {
        self.googleMapCircle = GMSCircle(position: coodinate, radius: CLLocationDistance(self.distinguishNumber * 1000))
        googleMapCircle.fillColor = UIColor(red: 0 / 255.0, green: 196 / 255.0, blue: 203 / 255.0, alpha: 0.1)
        googleMapCircle.strokeColor = UIColor(red: 0 / 255.0, green: 196 / 255.0, blue: 203 / 255.0, alpha: 1)
        googleMapCircle.strokeWidth = 2
        googleMapCircle.map = self.googleMapView
    }
    
    private func zoomInToTheLocationWith(latitude: CLLocationDegrees , longitude: CLLocationDegrees , number: Float) {
        let camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: number)
        googleMapView.camera = camera
    }
    
    // 路線清單 - 讀取、修改、修改
    private func routeListDataActionWith(routeSaveName: String) {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm"
        let createTime = formatter.string(from: date)
        // 讀檔
        let routeListDataFile = NSHomeDirectory() + "/Documents/RouteListData.data"
        if let tmp = NSKeyedUnarchiver.unarchiveObject(withFile: routeListDataFile) as? RoutePlanListManager {
            self.routeListArray = tmp.planListArray!
            print("路線規劃 - 路線清單 - 讀檔 - 成功")
        } else {
            print("路線規劃 - 路線清單 - 讀檔 - 失敗")
        }
        
        // 更新檔案
        let routePlanObj = RoutePlanObjClass(routeName: routeSaveName, createTime: createTime, stationCount: "\(self.selectCustomPinArray.count)", dataName: routeSaveName)
        self.routeListArray.append(routePlanObj)
        
        // 存檔
        let routeListManager = RoutePlanListManager(planListArray: self.routeListArray)
        if NSKeyedArchiver.archiveRootObject(routeListManager, toFile: routeListDataFile) == false {
            print("路線規劃 - 路線清單 - 存檔 - 失敗")
            ShowMessageView.showMessage(title: nil, message: "儲存失敗", completion: { (isOK) in
                
            })
        } else {
            print("路線規劃 - 路線清單 - 存檔 - 成功")
            ShowMessageView.showMessage(title: nil, message: "儲存完成", completion: { (isOK) in
                if (isOK == true) {
                    self.deleteTemporaryStationListFileWith(name: self.temporaryFileName)
                    if let controllerArray = self.navigationController?.viewControllers {
                        for controller in controllerArray {
                            if (controller is RoutePlaningViewController) {
                                self.navigationController?.popToViewController(controller, animated: true)
                            }
                        }
                    }
                }
            })
        }
    }
    
    // 路線清單詳細資訊 - 清單資訊處理
    private func stationInfoListDataActionWith(routeSaveName: String) {
        // 處理檔案
        self.stationCoodinateArray.append(self.endCoodinate!)
        self.calculateDistance()
        SVProgressHUD.show()
    }
    
    // 計算已存的所有站點間的距離
    private func calculateDistance() {
        let start = self.stationCoodinateArray[pinIndex]
        let end = self.stationCoodinateArray[pinIndex + 1]
        self.callGoogleDistanceMatrixAPIWith(startCoodinate: start, endCoodinate: end, avoidType: self.avoidType, resultType: .Filter)
    }
    // 儲存 Google Api 回來的距離
    private func saveGoogleDistanceWith(distanceData: GoogleDistanceMatrixClass) {
        if let distance = distanceData.distanceText {
            self.stationDistanceArray.append(distance)
            
        }
    }
    
    // 路線清單詳細資訊 - 修改、存檔
    private func saveRouteStationFileWith(routeSaveName: String) {
        
//        let startStationObj = RouteStationInfoObjClass(stationName: self.startLocation, distance: self.stationDistanceArray[0], stationId: "" , latitude: "\(self.stationCoodinateArray[0].latitude)" , longitude: "\(self.stationCoodinateArray[0].longitude)", address: nil)
//        self.stationInfoArray.append(startStationObj)
//        print("Coodinate Count: \(self.stationCoodinateArray.count)")
//        print("CustomPin Count: \(self.selectCustomPinArray.count)")
//        print("Distance Count: \(self.stationDistanceArray.count)")
//        for i in 0..<self.selectCustomPinArray.count {
//            let pin = self.selectCustomPinArray[i]
//            let distance = self.stationDistanceArray[i + 1]
//            let stationInfoObj = RouteStationInfoObjClass(stationName: pin.title, distance: distance, stationId: pin.data?.stationId , latitude: "\(pin.coordinate.latitude)" , longitude: "\(pin.coordinate.longitude)", address: nil)
//            self.stationInfoArray.append(stationInfoObj)
//        }
        
//
//        let endStationObj = RouteStationInfoObjClass(stationName: self.endLocation, distance: "", stationId: "" , latitude: "\((self.stationCoodinateArray.last?.latitude ?? 0))" , longitude: "\((self.stationCoodinateArray.last?.longitude ?? 0))", address: nil)
//        self.stationInfoArray.append(endStationObj)
        
        
        // 存檔
        let routeDetailDataFile = NSHomeDirectory() + "/Documents/" + routeSaveName + ".data"
        let routeStationInfoListManager = RouteStationInfoListManager(stationInfoArray: self.stationInfoArray)
        if NSKeyedArchiver.archiveRootObject(routeStationInfoListManager, toFile: routeDetailDataFile) == false {
            SVProgressHUD.dismiss()
            print("路線規劃 - 路線詳細資訊 - 存檔 - 失敗")
        } else {
            SVProgressHUD.dismiss()
            print("路線規劃 - 路線詳細資訊 - 存檔 - 成功")
        }
    }
    
    // 刪除站點清單的暫存檔
    private func deleteTemporaryStationListFileWith(name: String) {
        let fileManager = FileManager.default
        let file = NSHomeDirectory() + "/Documents/" + name + ".data"
        do {
            try fileManager.removeItem(atPath: file)
        } catch {
            print("檢視清單暫存檔刪除失敗")
        }
    }
    
    // MARK: - Google Maps API
    /// Google 地址座標轉換 API
    private func callGoogleGeocodingAPIWith(addressString: String , geocodingLocationType : GeocodingLocationType) {
        
        googleAPIManager.callGoogleAddressConvertToCoodinateWith(addressString: addressString , geocodingLocationType: geocodingLocationType)
    }
    
    /// Google 導航＆計算方位
    private func callGoogleDirectionAPIWith(startCoodinate: CLLocationCoordinate2D , endCoodinate: CLLocationCoordinate2D , avoidType: GoogleRouteAvoidType) {
        googleAPIManager.callGoogleDirectionAPIWith(startCoodinate: startCoodinate, endCoodinate: endCoodinate, avoidType: avoidType, googleMapView: self.googleMapView, lineColor: #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1))
        
    }
    
    /// Google 計算距離＆時間
    private func callGoogleDistanceMatrixAPIWith(startCoodinate: CLLocationCoordinate2D , endCoodinate: CLLocationCoordinate2D , avoidType: GoogleRouteAvoidType , resultType: DistanceMatrixType) {
        googleAPIManager.callGoogleDistanceMatrixAPIWith(startCoodinate: startCoodinate, endCoodinate: endCoodinate, avoidType: avoidType , resultType: resultType)
    }
    
    // MARK: - API
    /// API-301：站點查詢
    func callAPIRequestForStationList(queryString: String?, queryType: KJLRequest.RequestQueryType , carType: String) {
        KJLRequest.requestForStationList(queryType: queryType, queryString: queryString, type: carType) { (response) in
            guard let response = response as? StationListClass else {
                return
            }
            
            if queryString == nil {
                if let lists = response.datas {
                    self.showStationPins(stationListDatas: lists)
                } else {
                    self.showStationPins(stationListDatas: [])
                }
            }
        }
    }
}

// MARK: - Google MapView Delegate
extension RouteDisplayViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        guard let poiItem = marker.userData as? POIItem , poiItem.isNotFound == false else{
          return false
        }
    
        self.stationInfoBtn.isUserInteractionEnabled = true
        self.stationInfoBtn.setImage(UIImage(named: "plan_btn_info_normal"), for: .normal)
        self.stationInfoBtn.setImage(UIImage(named: "plan_btn_info_pressed"), for: .highlighted)
        self.addLineBtn.isUserInteractionEnabled = true
        self.addLineBtn.setImage(UIImage(named: "plan_btn_plus_normal"), for: .normal)
        self.addLineBtn.setImage(UIImage(named: "plan_btn_plus_pressed"), for: .highlighted)
        self.hintView.isHidden = true
        self.stationInfoView.isHidden = false
        
        let pin = pinArray[poiItem.index]
        self.selectPin = pin
        temporaryMarker = marker
        
        self.selectDistance = "\(poiItem.distance) 公里"
        if let data = pin.data {
            self.stationIdLabel.text = data.stationAbrv
            self.stationNameLabel.text = data.stationName
            self.stationDistanceLabel.text = "\(poiItem.distance) 公里"
            if let score = data.stationScore {
                self.stationScoreLabel.text = score
                let scoreF = Float(score) ?? 0.0
                if scoreF <= 0 {
                    self.scoreViewWidth.constant = 0

                } else {
                    scoreViewWidth.constant = 80
                }
            } else {
                self.stationScoreLabel.text = ""
                self.scoreViewWidth.constant = 0
            }
        }
        return true
    }
}

//MARK: GMUClusterManagerDelegate
extension RouteDisplayViewController :GMUClusterManagerDelegate {
    func clusterManager(_ clusterManager: GMUClusterManager, didTap clusterItem: GMUClusterItem) -> Bool {
        return false
    }
    
    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool {
        let newCamera = GMSCameraPosition.camera(withTarget: cluster.position,
          zoom: googleMapView.camera.zoom + 1)
        let update = GMSCameraUpdate.setCamera(newCamera)
        googleMapView.animate(with: update)
        return true
    }
}

//MARK: GMUClusterRendererDelegate
extension RouteDisplayViewController: GMUClusterRendererDelegate {
    func renderer(_ renderer: GMUClusterRenderer, willRenderMarker marker: GMSMarker) {
        marker.groundAnchor = CGPoint(x: 0.5, y: 1)
        if let markerData = (marker.userData as? POIItem) {
           let icon = markerData.iconImage
            marker.icon = icon
        }
    }
}

// MARK: - TableView DataSource
extension RouteDisplayViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return durationArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "DistinguishTableCell", for: indexPath) as? DistinguishTableCell {
            let isSelect = (cellSelectIndex == indexPath.row) ? true : false
            cell.setupCellWith(indexPath: indexPath , imageTitle: cellImageArray[indexPath.row], distance: self.distanceArray[indexPath.row] , duration: self.durationArray[indexPath.row] , isSelect: isSelect , delegate: self)
            return cell
        }
        return UITableViewCell(style: .default, reuseIdentifier: "")
    }
}

// MARK: - TableView Delegate
extension RouteDisplayViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let startCoodinate = self.startCoodinate {
            if let endCoodinate = self.endCoodinate {
                self.clearRoute()
                if (indexPath.row == 0) {
                    self.avoidType = .None
                } else if (indexPath.row == 1) {
                    self.avoidType = .Highways
                } else {
                    self.avoidType = .Tolls
                }
                self.callGoogleDirectionAPIWith(startCoodinate: startCoodinate, endCoodinate: endCoodinate, avoidType: self.avoidType)
            }
        }
        cellSelectIndex = indexPath.row
        self.tableView.reloadData()
    }
}

// MARK: - DistinguishTableCell Delegate
extension RouteDisplayViewController: DistinguishTableCellDelegate {
    // 箭頭按鈕
    func arrowBtnDidPress() {
        UIView.animate(withDuration: 0.3) {
            self.routeSelectViewHeight.constant = 0
            self.stationDetailViewHeight.constant = 215
            self.view.layoutIfNeeded()
        }
        if let startCoodinate = self.startCoodinate {
            self.drawCircleWith(coodinate: startCoodinate)
            googleMapView.animate(with: GMSCameraUpdate.fit(coordinate: self.startCoodinate!, radius: CLLocationDistance(self.distinguishNumber * 1000)))
        }
        // 所有充電站點API
        let filterCarType = UserDefaults.standard.string(forKey: "filterCarType")
        self.callAPIRequestForStationList(queryString: nil, queryType: KJLRequest.RequestQueryType.Station , carType: filterCarType ?? "1")
        // GoogleAPI - 算起始到終點的距離
        if let startCoodinate = self.startCoodinate {
            if let endCoodinate = self.endCoodinate {
                self.callGoogleDistanceMatrixAPIWith(startCoodinate: startCoodinate, endCoodinate: endCoodinate, avoidType: self.avoidType, resultType: .End)
            }
        }
    }
}

// MARK: - GoogleAPIManager Delegate {
extension RouteDisplayViewController: GoogleAPIRequestManagerDelegate {
    // Google - 導航
    func googleDirectionResultWith(directionData: GoogleDirectionDataClass) {
        if let status = directionData.status {
            if (status == "OK") {
                self.googleMapView = directionData.googleMapsView
                // 導航 - 折線圖
                self.routePolyline = directionData.routePolyline
            }
        }
    }
    
    // Google - 地址轉成座標
    func googleGeocodingResultWith(geocodingData: GoogleGeocodingDataClass) {
        if let status = geocodingData.status  {
            if (status == "OK") {
                if let formattedAddress = geocodingData.formattedAddress,let resultLatitude = geocodingData.lat,let resultLongitude = geocodingData.lng {
                    if (geocodingData.locationType == .Start) {
                        self.startCoodinate = CLLocationCoordinate2DMake(resultLatitude, resultLongitude)
                        self.addMarkerOnGoogleMapWith(title: formattedAddress, coodinate: self.startCoodinate!, geocodingLocationType: .Start)
                        self.stationCoodinateArray.append(self.startCoodinate!)
                    } else {
                        self.endCoodinate = CLLocationCoordinate2DMake(resultLatitude, resultLongitude)
                        self.addMarkerOnGoogleMapWith(title: formattedAddress, coodinate: self.endCoodinate!, geocodingLocationType: .End)
                    }
                    
                    if let startCoodinate = self.startCoodinate ,let endCoodinate = self.endCoodinate {
                        googleMapView.animate(with: GMSCameraUpdate.coordinateBounds(startCoordinate: startCoodinate, endCoordinate: endCoodinate))
                    }
                }
                
                self.setupTableViewData()
            } else {
                ShowMessageView.showMessage(title: nil, message: "找不到您搜尋的位置，請重新輸入", completion: { (isOK) in
                    self.navigationController?.popViewController(animated: true)
                })
            }
        } else {
            ShowMessageView.showMessage(title: nil, message: "找不到您搜尋的位置，請重新輸入", completion: { (isOK) in
                self.navigationController?.popViewController(animated: true)
            })
        }
    }
    
    // Google - 距離與時間
    func googleDistanceMatrixResultWith(distanceDurationData: GoogleDistanceMatrixClass) {
        guard let status = distanceDurationData.status else { return }
        if (status == "OK") {  // 起點到目的地
            if (distanceDurationData.resultType == .Begin) {
                guard let distance = distanceDurationData.distanceText,
                    let duration = distanceDurationData.durationText
                    else { return }
                var index = 0
                if (distanceDurationData.avoidType == .None) {
                    index = 0
                } else if (distanceDurationData.avoidType == .Highways) {
                    index = 1
                } else {
                    index = 2
                }
                self.distanceArray[index] = distance
                self.durationArray[index] = duration
                self.tableView.reloadData()
            } else if (distanceDurationData.resultType == .End) {  // 站點到目的地
                guard let distance = distanceDurationData.distanceText else { return }
                self.stationToEndDistance = distance
            } else {   // 站點篩選
                self.saveGoogleDistanceWith(distanceData: distanceDurationData)
                pinIndex += 1
                if (pinIndex < self.stationCoodinateArray.count - 1) {
                    self.calculateDistance()
                } else {
                    self.saveRouteStationFileWith(routeSaveName: self.routeSaveName)
                    if (self.routeSaveName == self.temporaryFileName) {
                        SVProgressHUD.dismiss()
                        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "RouteDetailViewController") as? RouteDetailViewController else { return }
                        vc.setupVCWith(title: "檢視已添加站點清單", dataName: self.temporaryFileName , isShowNaviBtn: false)
                        self.navigationController?.show(vc, sender: self)
                    }
                }
            }
        }
    }
}

//MARK: IBAction
extension RouteDisplayViewController {
    // 站點資訊
    @IBAction func stationInfoAction(_ sender: UIButton) {
        stationInfoBtn.isSelected = true
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChargingStationDetailViewController") as? ChargingStationDetailViewController else { return }
        let data = selectPin?.data
        if let no = data?.stationId {
            vc.setupVCWith(stationNo: no)
        } else {
            vc.setupVCWith(stationNo: "")
        }
        self.navigationController?.show(vc, sender: self)
    }
    
    // 添加到路線
    @IBAction func addStationAction(_ sender: UIButton) {
        guard let selectPin = self.selectPin else { return }
        if let tempPosition = temporaryMarker?.position {
            if let prePosition = previousMarker?.position {
                if (tempPosition.latitude == prePosition.latitude && tempPosition.longitude == prePosition.longitude) {
                    ShowMessageView.showMessage(title: nil, message: "此站點與上一站重複", completion: { (isOK) in })
                    return
                }
            }
        }
        
        if (self.selectCustomPinArray.count < 20) {
            previousMarker = temporaryMarker
            // 添加
            self.selectCustomPinArray.append(selectPin)
            self.stationCoodinateArray.append(selectPin.coordinate)
            self.stationAmountLabel.text = "已添加 \(self.selectCustomPinArray.count) 個地點"
            // 畫圓
            self.startCoodinate = selectPin.coordinate
            self.googleMapCircle.map = nil
            self.drawCircleWith(coodinate: self.startCoodinate!)
            googleMapView.animate(with: GMSCameraUpdate.fit(coordinate: self.startCoodinate!, radius: CLLocationDistance(self.distinguishNumber * 1000)))
        } else {
            ShowMessageView.showMessage(title: nil, message: "超過導航串接上限，無法再加入", completion: { (isOK) in
                
            })
        }
    }
    // 檢視已添加站點清單
    @IBAction func checkListAction(_ sender: UIButton) {
        self.routeSaveName = self.temporaryFileName
        self.deleteTemporaryStationListFileWith(name: self.temporaryFileName)
        self.stationInfoListDataActionWith(routeSaveName: self.temporaryFileName)
    }
}


// MARK: setup
extension RouteDisplayViewController {
    
    func setupInitValue() { 
        self.stationInfoArray = []
        // 為了避免檢視完已添加站點清單後，資料不對，所以看完清單回到頁面要把最後添加的目的地座標移除掉，觀看站點資訊回來時，不需要刪除最後一筆資料
        if (self.stationCoodinateArray.count != 0 && self.stationInfoBtn.isSelected == false) {
            self.stationCoodinateArray.removeLast()
        }
        self.stationInfoBtn.isSelected = false
        self.stationDistanceArray = []
        self.pinIndex = 0
    }

    func setupNavView() {
        createNavViewForRightBtn(rightName: "下一步", rightImage: "", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        titleLab?.text = "路線規劃"

        rightBtn?.addTarget(self, action: #selector(saveAction), for: .touchUpInside)
    }
    
    // MARK: - 自訂 Func
    // 設定使用者位置
    private func setupMyLocation() {
        guard CLLocationManager.authorizationStatus() != .denied else {
            let a = URL(string: UIApplicationOpenSettingsURLString)
            if UIApplication.shared.canOpenURL(a!) {
                UIApplication.shared.open(a!, options: [:], completionHandler: nil)
            }
            return
        }
        
        if CLLocationManager.authorizationStatus() == .notDetermined || CLLocationManager.authorizationStatus() == .denied {
            self.zoomInToTheLocationWith(latitude: 25.0329893, longitude: 121.5326985, number: 8)
        } else {
            if let myLocation = MainViewController.mainController?.mapView.camera.target {
                self.zoomInToTheLocationWith(latitude: myLocation.latitude, longitude: myLocation.longitude, number: 8)
            }
        }
    }
    
    func setupGoogleMapView() {
        googleMapView.delegate = self
        googleAPIManager.delegate = self
        googleMapView.isMyLocationEnabled = true
        
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "google_map_style", withExtension: "json") {
                googleMapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        
        let iconGenerator = GMUDefaultClusterIconGenerator()
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: googleMapView, clusterIconGenerator: iconGenerator)
        renderer.delegate = self
        clusterManager = GMUClusterManager(map: googleMapView, algorithm: algorithm, renderer: renderer)
        clusterManager.setDelegate(self, mapDelegate: self)
        
        // 標註起點
        self.callGoogleGeocodingAPIWith(addressString: self.startLocation, geocodingLocationType: .Start)
        self.callGoogleGeocodingAPIWith(addressString: self.endLocation, geocodingLocationType: .End)
        self.stationAmountLabel.text = "已添加 \(self.selectCustomPinArray.count) 個地點"
    }
    
    func setupStationDetailView() {
        self.stationDetailViewHeight.constant = 0
        self.stationDistanceLabel.isHidden = true
    }
    
    // MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavView()
        
        self.setupMyLocation()
        self.setupGoogleMapView()
        self.setupStationDetailView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupInitValue()
    }
}

extension GMSCameraUpdate {

    static func fit(coordinate: CLLocationCoordinate2D, radius: Double) -> GMSCameraUpdate {
        var leftCoordinate = coordinate
        var rigthCoordinate = coordinate

        let region = MKCoordinateRegionMakeWithDistance(coordinate, radius, radius)
        let span = region.span

        leftCoordinate.latitude = coordinate.latitude - span.latitudeDelta
        leftCoordinate.longitude = coordinate.longitude - span.longitudeDelta
        rigthCoordinate.latitude = coordinate.latitude + span.latitudeDelta
        rigthCoordinate.longitude = coordinate.longitude + span.longitudeDelta

        let bounds = GMSCoordinateBounds(coordinate: leftCoordinate, coordinate: rigthCoordinate)
        let update = GMSCameraUpdate.fit(bounds, withPadding: 10.0)
        return update
    }

    static func coordinateBounds(startCoordinate: CLLocationCoordinate2D, endCoordinate: CLLocationCoordinate2D) -> GMSCameraUpdate {
        let bounds = GMSCoordinateBounds(coordinate: startCoordinate, coordinate: endCoordinate)
        let update = GMSCameraUpdate.fit(bounds, withPadding: 50)
        return update
    }
}
