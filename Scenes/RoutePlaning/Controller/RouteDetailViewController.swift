//
//  RouteDetailViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class RouteDetailViewController: KJLNavView {

    // MARK: - IBOutlet
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var naviBtn: UIButton!
    // MARK: - Property
    private var titleString: String = ""
    private var dataArray: [RouteStationInfoObjClass] = []
    private var dataName = ""
    private var isShowNaviBtn: Bool = false
    var detail: StationDetailClass.Data? = nil
    
    // MARK: - 生命週期
    override func viewDidLoad() {
        super.viewDidLoad()

        createNavViewForRightBtn(rightName: "", rightImage: "", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        titleLab?.text = titleString
        
        self.naviBtn.isHidden = !isShowNaviBtn
        loadDataWith(dataName: self.dataName)
    }
    
    // MARK: - IBAction
    @IBAction func startNavAction(_ sender: UIButton) {
        let data = dataArray.last
        if let latitude = data?.latitude {
            if let longitude = data?.longitude {
                
                var coodinateArray : [CLLocationCoordinate2D] = []
                for data in dataArray {
                    if let latitude = data.latitude {
                        if let longitude = data.longitude {
                            let coodinate = CLLocationCoordinate2D(latitude: Double(latitude) ?? 0, longitude: Double(longitude) ?? 0)
                            coodinateArray.append(coodinate)
                        }
                    }
                }
                KJLCommon.openMultiStationMapGoogle(dataArray: coodinateArray)
            }
        }
        
    }
    
    // MARK: - 自訂 Func
    func setupVCWith(title: String ,dataName: String , isShowNaviBtn: Bool) {
        self.titleString = title
        self.dataName = dataName
        self.isShowNaviBtn = isShowNaviBtn
    }
    
    func loadDataWith(dataName: String) {
        let routeListDataFile = NSHomeDirectory() + "/Documents/" + dataName + ".data"
        if let tmp = NSKeyedUnarchiver.unarchiveObject(withFile: routeListDataFile) as? RouteStationInfoListManager {
            print("路線規劃清單 - 讀檔 - 成功")
            self.dataArray = tmp.stationInfoArray!
            self.tableView.reloadData()
        } else {
            print("路線規劃清單 - 讀檔 - 失敗")
        }
    }
}

// MARK: - TableView Data Source
extension RouteDetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "RouteDetailCell", for: indexPath) as? RouteDetailCell {
            let isLastCell = (indexPath.row == (dataArray.count - 1)) ? true : false
            let data = dataArray[indexPath.row]
            cell.setupCellWith(indexPath: indexPath , dataString: data.stationName ?? "" , distanceString: data.distance ?? "", isLastCell: isLastCell)
            return cell
        }
        return UITableViewCell(style: .default, reuseIdentifier: "")
    }
}

// MARK: - TableView Delegate
extension RouteDetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath.row == 0 || indexPath.row == dataArray.count - 1) {
            return
        } else  {
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChargingStationDetailViewController") as? ChargingStationDetailViewController {
                let data = dataArray[indexPath.row]
                vc.setupVCWith(stationNo: data.stationId ?? "")
                self.navigationController?.show(vc, sender: self)
            }
        }
    }
}
