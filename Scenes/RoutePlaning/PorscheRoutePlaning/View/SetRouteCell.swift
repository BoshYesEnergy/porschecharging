//
//  SetRouteCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/16.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

protocol SetRouteDelegate: class {
    func tapLeft()
    func tapRight()
}

class SetRouteCell: UITableViewCell {

    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var leftBtn: UIButton!
    @IBOutlet weak var rightBtn: UIButton!
     
    weak var delegate: SetRouteDelegate?
    
    var content: String = ""
    var hiddenButton: Bool? {
        didSet {
            leftBtn.isHidden  = hiddenButton ?? false
            contentLabel.text = hiddenButton ?? false == true ? content : ""
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
   
    @IBAction func leftAction(_ sender: UIButton) {
        delegate?.tapLeft()
    }
    
    @IBAction func rightAction(_ sender: Any) {
        delegate?.tapRight()
    }
    
}
