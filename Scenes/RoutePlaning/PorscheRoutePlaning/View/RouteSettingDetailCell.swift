//
//  RouteSettingDetailCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/18.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

protocol RouteSettingDetailDelegate: class {
    func deleteStation(index: Int)
}

class RouteSettingDetailCell: UITableViewCell {
    
    @IBOutlet weak var otherImage: UIImageView!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var titleName: UILabel!
    @IBOutlet weak var titleAddress: UILabel!
    @IBOutlet weak var titleKm: UILabel!
    @IBOutlet weak var blueLine: UIView!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var garyLine: UIView!
    var delegate: RouteSettingDetailDelegate?
    var index: Int = 0
    var data: RouteStationInfoObjClass? {
        didSet {
            guard let data = data else { return } 
            titleAddress.text = data.address ?? ""
            titleName.text    = data.stationName ?? ""
            titleKm.text      = data.distance ?? ""
            if data.stationCategory?.lowercased() == "yes" {
                if data.connection == false {
                    if data.hasDC == true && data.hasAC == true {
                        iconImage.image = UIImage(named: AnnotationType.DcOffLine.rawValue)
                    } else if data.hasDC == true {
                        iconImage.image = UIImage(named: AnnotationType.DcOffLine.rawValue)
                    } else if data.hasAC == true {
                        iconImage.image = UIImage(named: AnnotationType.AcOffLine.rawValue)
                    }
                } else {
                    if data.hasDC == true && data.hasAC == true {
                        iconImage.image = UIImage(named: AnnotationType.DcAvailable.rawValue)
                    } else if data.hasDC == true {
                        iconImage.image = UIImage(named: AnnotationType.DcAvailable.rawValue)
                    } else if data.hasAC == true {
                        iconImage.image = UIImage(named: AnnotationType.AcAvailable.rawValue)
                    }
                } 
            } else {
                if data.connection == false {
                    if data.hasDC == true && data.hasAC == true {
                        iconImage.image = UIImage(named: AnnotationType.DcOffLine.rawValue)
                    } else if data.hasDC == true {
                        iconImage.image = UIImage(named: AnnotationType.DcOffLine.rawValue)
                    } else if data.hasAC == true {
                        iconImage.image = UIImage(named: PorscheICON.AcOffLine.rawValue)
                    }
                } else {
                    if data.hasDC == true && data.hasAC == true {
                        iconImage.image = UIImage(named: PorscheICON.Dc.rawValue)
                    } else if data.hasDC == true {
                        iconImage.image = UIImage(named: PorscheICON.Dc.rawValue)
                    } else if data.hasAC == true {
                        iconImage.image = UIImage(named: PorscheICON.AcOffLine.rawValue)
                    }
                }
            }
        }
    }
     
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle     = .none
        deleteBtn.isHidden = true
        garyLine.isHidden  = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func deleteAction(_ sender: UIButton) {
        delegate?.deleteStation(index: index)
    }
    
}
