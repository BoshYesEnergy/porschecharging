//
//  RoutePlaningEditCell.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/18.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit
 
protocol RoutePlaningEditDelegate: class {
    func check(index: Int)
}

class RoutePlaningEditCell: UITableViewCell {

    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var checkBtn: UIButton!
    @IBOutlet weak var titleName: UILabel!
    @IBOutlet weak var titleDate: UILabel!
    
    var index = 0
    var data: RoutePlanObjClass? {
        didSet {
            guard let data = data else { return }
            titleName.text = data.dataName ?? ""
            titleDate.text   = data.createTime ?? ""
        }
    }
    var delegate: RoutePlaningEditDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated) 
    }
    
    @IBAction func check(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        let image = UIImage(named: sender.isSelected == true ? "icon_check_default" : "icon_uncheck_default")
        checkBtn.setImage(image, for: .normal)
        bottomView.backgroundColor = sender.isSelected == true ? #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        delegate?.check(index: index)
    }
}
