//
//  RouteSettingViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation
import MapKit
import SVProgressHUD

class RouteSettingViewController: BaseViewController {
    @IBOutlet weak var startTextField: UITextField!
    @IBOutlet weak var endTextField: UITextField!
    @IBOutlet weak var distinguishTextField: UITextField!
    @IBOutlet weak var googleMapView: GMSMapView!
    
    @IBOutlet weak var locationBorderView: UIView!
    @IBOutlet weak var destinationBorderView: UIView!
    @IBOutlet weak var rangeBorderView: UIView!
    @IBOutlet weak var kmTitle: UILabel!
    @IBOutlet weak var planRouteBtn: UIButton!
    @IBOutlet weak var locationBtn: UIButton!
    
    @IBOutlet weak var toolLoactionBtn: UIButton!
    @IBOutlet weak var toolFilterBtn: UIButton!
    @IBOutlet weak var toolLoactionBottom: NSLayoutConstraint!
    
    @IBOutlet weak var routeSettingBottomViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var routeSettingBottomView: UIView!
    @IBOutlet weak var routeSettingView: UIView!
    @IBOutlet weak var nextBtn: UIButton!
    
    // station detail view
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var detailViewBottom: NSLayoutConstraint!
    @IBOutlet weak var stationName: UILabel!
    @IBOutlet weak var stationAddress: UILabel!
    @IBOutlet weak var kmLabel: UILabel!
    @IBOutlet weak var dcStackView: UIStackView!
    @IBOutlet weak var acStackView: UIStackView!
    @IBOutlet weak var dcAvailable: UILabel!
    @IBOutlet weak var acAvailable: UILabel!
    @IBOutlet weak var closeStationDetailBtn: UIButton!
    @IBOutlet weak var addStationBtn: UIButton!
    
    // filter
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var closeFilterView: UIButton!
    @IBOutlet weak var filterTitle: UILabel!
    @IBOutlet weak var filterAcTitle: UILabel!
    @IBOutlet weak var filterDcTitle: UILabel!
    @IBOutlet weak var filterAcailable: UILabel!
    @IBOutlet weak var titlerSwitch: UISwitch!
    @IBOutlet weak var acSwitch: UISwitch!
    @IBOutlet weak var dcFilter: UISwitch!
    @IBOutlet weak var availableSwitch: UISwitch!
    @IBOutlet weak var reSetFilter: UIButton!
    @IBOutlet weak var filterViewBottom: NSLayoutConstraint!
    
    private var locationManager = CLLocationManager()
    private var googleAPIManager =  GoogleAPIRequestManager()
    private var focusType: GeocodingLocationType = .Start
    private var startMarker = GMSMarker()
    private var endMarker = GMSMarker()
    private var googleMapCircle = GMSCircle()
    private var isFirstSearch = true
    
    private var startCoodinate: CLLocationCoordinate2D?
    private var endCoodinate: CLLocationCoordinate2D?
    private var avoidType: GoogleRouteAvoidType = .None
    private var clusterManager: GMUClusterManager!
    private var stationCoodinateArray: [CLLocationCoordinate2D] = []
    // 要顯示的大頭針
    private var showMarkerArray: [POIItem] = []
    private var selectPin: CustomPin? = nil
    private var temporaryMarker: GMSMarker?
    private var previousMarker: GMSMarker?
    // 已選取的站點
    private var selectCustomPinArray: [StationListClass.Data] = []
    // API 所有的充電站
    private var pinArray: [CustomPin] = []
    private var distinguishNumber: Int = 0
    // 儲存站點名稱、距離、站點ID
    private var stationInfoArray: [RouteStationInfoObjClass] = []
    private var stationDistanceArray: [String] = []
    private var temporaryFileName: String = "**CheckTheStationsInList**"
    private var routeSaveName: String = ""
    private var pinIndex = 0
    
    var startLocation = ""
    var endLocation   = ""
    var viewHeight:CGFloat = 0
    var selectDistance: String = ""
     
    override func viewDidLoad() {
        super.viewDidLoad()
        setHeaderView("Route Planning", action: .POP)
        hiddenNavigationBar(isHidden: true)
        setToolButton(nil, UIImage(named: "button_close_default"), UIImage(named: "button_close_click")) {
            self.dismiss(animated: true, completion: nil)
        }
        settingView()
        setupMyLocation()
        setNotification()
        setRecognizer()
        setGoogleMapView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupInitValue()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        clusterManager.clearItems()
        googleMapView.clear()
        print("RouteSettingViewController viewWillDisappear")
    }
    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        self.view.endEditing(true)
//    }
    
    @IBAction func closeFilter(_ sender: UIButton) {
        closeFilterview()
    }
    
    @IBAction func filterSwitch(_ sender: UISwitch) {
        DispatchQueue.main.async {
            self.callAPIRequestForStationList(queryString: nil, queryType: .Station, carType: "1")
        }
        
        checkFilterType()
    }
    
    @IBAction func reSettingFilter(_ sender: UIButton) {
        defaultSwitch()
    }
    
    func closeFilterview() {
        UIView.animate(withDuration: 0.3) {
            self.filterViewBottom.constant = -400
            self.view.layoutIfNeeded()
        }
    }
    
    func showFilter() {
        UIView.animate(withDuration: 0.3) {
            self.filterViewBottom.constant = 0
            self.view.layoutIfNeeded()
        }
    }
}


//MARK: Setting View
extension RouteSettingViewController {
    fileprivate func settingView() {
        startTextField.delegate = self
        endTextField.delegate = self
        distinguishTextField.delegate = self
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        locationBorderView.layer.borderWidth = 1
        locationBorderView.layer.borderColor = ThemeManager.shared.colorPalettes.blackBorder.cgColor
        locationBorderView.layer.cornerRadius = 5
        
        destinationBorderView.layer.borderWidth = 1
        destinationBorderView.layer.borderColor = ThemeManager.shared.colorPalettes.blackBorder.cgColor
        destinationBorderView.layer.cornerRadius = 5
        
        rangeBorderView.layer.borderWidth = 1
        rangeBorderView.layer.borderColor = ThemeManager.shared.colorPalettes.blackBorder.cgColor
        
        toolLoactionBtn.setImage(UIImage(named: ToolImage.location_click), for: .highlighted)
        toolFilterBtn.setImage(UIImage(named: ToolImage.filter_click), for: .highlighted)
        nextBtn.isHidden = true
        nextBtn.isEnabled = false
        planRouteBtn.isEnabled = false
        viewHeight = self.view.frame.height
        willDisplayStationDetail(display: false)
        
        filterViewBottom.constant = -400
        titlerSwitch.isOn = false
        acSwitch.isOn     = true
        dcFilter.isOn     = true
        availableSwitch.isOn = false
        
        reSetFilter.layer.borderWidth = 1
        reSetFilter.layer.borderColor = ThemeManager.shared.colorPalettes.blackBorder.cgColor
    }
    
    private func checkFilterType() {
        if titlerSwitch.isOn       == false
            && acSwitch.isOn        == true
            && dcFilter.isOn        == true
            && availableSwitch.isOn == false
        {
            toolFilterBtn.setImage(UIImage(named: "button_floating_filter_default"), for: .normal)
        } else {
            toolFilterBtn.setImage(UIImage(named: "button_floating_filter_click"), for: .normal)
        }
    }
    
    private func defaultSwitch() {
        titlerSwitch.isOn  = false
        acSwitch.isOn       = true
        dcFilter.isOn       = true
        availableSwitch.isOn = false
        checkFilterType()
        callAPIRequestForStationList(queryString: nil, queryType: .Station , carType:"1")
    }
    
    fileprivate func setNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardSohw(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide(notification:)), name: .UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(checkTextField(notification:)), name: .UITextFieldTextDidChange, object: startTextField)
        NotificationCenter.default.addObserver(self, selector: #selector(checkTextField(notification:)), name: .UITextFieldTextDidChange, object: endTextField)
        NotificationCenter.default.addObserver(self, selector: #selector(checkTextField(notification:)), name: .UITextFieldTextDidChange, object: distinguishTextField)
    }
    
    fileprivate func setGoogleMapView() {
        googleMapView.delegate = self
        googleAPIManager.delegate = self
         
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "google_map_style", withExtension: "json") {
                googleMapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        
        let iconGenerator = GMUDefaultClusterIconGenerator()
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: googleMapView, clusterIconGenerator: iconGenerator)
        renderer.delegate = self
        clusterManager = GMUClusterManager(map: googleMapView, algorithm: algorithm, renderer: renderer)
        clusterManager.setDelegate(self, mapDelegate: self)
    }
    
    @objc private func checkTextField(notification: Notification) {
        if startTextField.text != "" && endTextField.text != "" && distinguishTextField.text != "" {
            planRouteBtn.backgroundColor = ThemeManager.shared.colorPalettes.redBackgroundColor
            planRouteBtn.isEnabled = true
        } else {
            planRouteBtn.backgroundColor = ThemeManager.shared.colorPalettes.garyBackgroundColor
            planRouteBtn.isEnabled = false
        }
    }
    
    @objc private func keyboardSohw(notification: Notification) {
        if let userInfo = notification.userInfo,
           let keyboardRectangle = userInfo[UIKeyboardFrameEndUserInfoKey] as? CGRect {
            print("keyboardRectangle height: \(keyboardRectangle.height)")
            
            UIView.animate(withDuration: 0.3) {
                self.routeSettingBottomViewBottomConstraint.constant = keyboardRectangle.height
                self.toolLoactionBtn.isHidden = true
                self.toolFilterBtn.isHidden = true
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc private func keyboardHide(notification: Notification) {
        UIView.animate(withDuration: 0.3) {
            self.routeSettingBottomViewBottomConstraint.constant = 0
            self.view.layoutIfNeeded()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.toolLoactionBtn.isHidden = false
            self.toolFilterBtn.isHidden = false
        }
    }
    
    fileprivate func setRecognizer() {
        let pan = UIPanGestureRecognizer(target: self, action: #selector(panBottom(_:)))
        routeSettingBottomView.addGestureRecognizer(pan)
    }
    
    @objc fileprivate func panBottom(_ recognizer: UIPanGestureRecognizer) {
        let location   = recognizer.location(in: self.view)
        viewHeight = self.view.frame.height
        print("--SWIPE--\(location.y)")
        print(routeSettingBottomView.frame.origin.y)
        
        if location.y >= viewHeight - 100 {
            UIView.animate(withDuration: 0.3) {
                self.routeSettingBottomView.frame.origin.y = self.viewHeight - 50
                self.toolLoactionBtn.frame.origin.y        = location.y - 110
                self.toolFilterBtn.frame.origin.y          = location.y - 170
                self.view.layoutIfNeeded()
            }
        } else if location.y <= viewHeight - 330 { // Constraint top
            
        } else {
            routeSettingBottomView.frame.origin.y = location.y
            toolLoactionBtn.frame.origin.y        = location.y - 60
            toolFilterBtn.frame.origin.y          = location.y - 120
            self.view.layoutIfNeeded()
        }
    }
}


// MARK: - IBAction
extension RouteSettingViewController {
    @IBAction func beginPlaningAction(_ sender: UIButton) {
        if startTextField.text == "" {
            return
        }
        
        if endTextField.text == "" {
            return
        }
        
        if distinguishTextField.text == "" {
            return
        }
        distinguishTextField.resignFirstResponder()
        downrouteSettingView()
        startLocation = startTextField.text ?? ""
        endLocation   = endTextField.text ?? ""
        distinguishNumber = Int(distinguishTextField.text ?? "") ?? 0
        
        
        self.callAPIRequestForStationList(queryString: nil, queryType: KJLRequest.RequestQueryType.Station , carType: "1")
        /// 標註起點
        self.callGoogleGeocodingAPIWith(addressString: self.startLocation, geocodingLocationType: .Start){ }
        self.callGoogleGeocodingAPIWith(addressString: self.endLocation, geocodingLocationType: .End){
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
                // GoogleAPI - 導航路線
                guard let startCoodinate = self.startCoodinate,
                    let endCoodinate = self.endCoodinate
                    else { return }
                self.callGoogleDirectionAPIWith(startCoodinate: startCoodinate, endCoodinate: endCoodinate, avoidType: .None, lineColor: #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1))
            }
        }
         
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) {
            // 畫圓
            if let startCoodinate = self.startCoodinate {
                self.drawCircleWith(coodinate: startCoodinate)
//                googleMapView.animate(with: GMSCameraUpdate.fit(coordinate: self.startCoodinate!, radius: CLLocationDistance(self.distinguishNumber * 1000)))
            }
        }
        // GoogleAPI - 算起始到終點的距離
//        if let startCoodinate = self.startCoodinate {
//            if let endCoodinate = self.endCoodinate {
//                self.callGoogleDistanceMatrixAPIWith(startCoodinate: startCoodinate, endCoodinate: endCoodinate, avoidType: self.avoidType, resultType: .End)
//            }
//        }
        
    }
    
    @IBAction func setLocation(_ sender: UIButton) {
        setupMyLocation()
    }
    
    
    @IBAction func toolFilter(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected == false {
            closeFilterview()
        } else {
            showFilter()
        }
        showFilter()
        checkFilterType()
    }
    
    @IBAction func toolLocation(_ sender: UIButton) {
        downrouteSettingView(animate: false)
        LocationManagers.updateLocation { [unowned self] (status, location) in
            let region = MKCoordinateRegionMakeWithDistance(location!.coordinate, 5000, 5000)
            let camera = GMSCameraPosition.camera(withLatitude: region.center.latitude, longitude: region.center.longitude, zoom: 15)
            self.googleMapView.camera = camera 
        }
    }
    
    // 檢視已添加站點清單
    @IBAction func next(_ sender: UIButton) {
        self.routeSaveName = self.temporaryFileName
        self.deleteTemporaryStationListFileWith(name: self.temporaryFileName)
        self.stationInfoListDataActionWith(routeSaveName: self.temporaryFileName)
        
    }
    
    // station detail
    @IBAction func closeStationDetail(_ sender: UIButton) {
        willDisplayStationDetail(display: false)
    }
    
    // 添加到路線
    @IBAction func addStation(_ sender: UIButton) {
        popMessage(content: "Porsche recommends to charge your vehicle to 80%. Depending on your driving behavior and external factors, ranges may vary. Please always make sure to check your driving range and if needed update your planned route after each charging stop.")
        
    }
}


// MARK: - Action
extension RouteSettingViewController {
    private func setupMyLocation() {
        guard CLLocationManager.authorizationStatus() != .denied else {
            let a = URL(string: UIApplicationOpenSettingsURLString)
            if UIApplication.shared.canOpenURL(a!) {
                UIApplication.shared.open(a!, options: [:], completionHandler: nil)
            }
            return
        }
        
        if CLLocationManager.authorizationStatus() == .notDetermined || CLLocationManager.authorizationStatus() == .denied {
            
            self.zoomInToTheLocationWith(latitude: 25.0329893, longitude: 121.5326985, zoomNumber: 15)
        } else {
            if let myLocation = MainViewController.mainController?.mapView.camera.target {
                
                self.zoomInToTheLocationWith(latitude: myLocation.latitude, longitude: myLocation.longitude, zoomNumber: 15)
                googleAPIManager.callGoogleCoodinateConvertToAddressWith(latitude: myLocation.latitude, longitude: myLocation.longitude, geocodingLocationType: .Coodinate)
            }
        }
    }
    
    private func removeMarker() {
        if (self.focusType == .Start) {
            self.startMarker.map = nil
        } else if (self.focusType == .End) {
            self.endMarker.map = nil
        }
    }
    
    private func zoomInToTheLocationWith(latitude: CLLocationDegrees , longitude: CLLocationDegrees , zoomNumber: Float) {
        let camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: zoomNumber)
        self.googleMapView.camera = camera
    }
    
    private func drawCircleWith(coodinate: CLLocationCoordinate2D) {
        /// defaut = distinguishNumber * 1000
        self.googleMapCircle = GMSCircle(position: coodinate, radius: CLLocationDistance(self.distinguishNumber * 900))
        googleMapCircle.fillColor = UIColor(red: 0 / 255.0, green: 196 / 255.0, blue: 203 / 255.0, alpha: 0.1)
        googleMapCircle.strokeColor = UIColor(red: 0 / 255.0, green: 196 / 255.0, blue: 203 / 255.0, alpha: 1)
        googleMapCircle.strokeWidth = 1
        googleMapCircle.map = self.googleMapView
    }
    
    private func downrouteSettingView(animate: Bool = true) {
        if animate == true {
            UIView.animate(withDuration: 0.3) {
                self.routeSettingBottomView.frame.origin.y = self.viewHeight - 50
                self.toolLoactionBtn.frame.origin.y        = self.viewHeight - 110
                self.toolFilterBtn.frame.origin.y          = self.viewHeight - 170
                self.view.layoutIfNeeded()
            }
        } else {
            self.routeSettingBottomView.frame.origin.y = self.viewHeight - 50
            self.toolLoactionBtn.frame.origin.y        = self.viewHeight - 110
            self.toolFilterBtn.frame.origin.y          = self.viewHeight - 170
            self.view.layoutIfNeeded()
        }
    }
    
    private func willDisplayStationDetail(display: Bool) {
        if display == true {
            UIView.animate(withDuration: 0.3) {
                self.detailViewBottom.constant = 0
                self.view.layoutIfNeeded()
            }
        } else {
            UIView.animate(withDuration: 0.3) {
                self.detailViewBottom.constant = -600
                self.view.layoutIfNeeded()
            }
        }
    }
    
    private func addStationToList() {
        guard let selectPin = self.selectPin else { return }
        if let tempPosition = temporaryMarker?.position {
            if let prePosition = previousMarker?.position { /// 確認有沒有存過
                if (tempPosition.latitude == prePosition.latitude && tempPosition.longitude == prePosition.longitude) {
                    /// 重複站點
                    return
                }
            }
        }
        if (self.selectCustomPinArray.count < 20) { /// 上線量
            previousMarker = temporaryMarker /// 存起來確認
            // 添加
            guard let data = selectPin.data else { return }
            self.selectCustomPinArray.append(data)
            self.stationCoodinateArray.append(selectPin.coordinate)
            
        } else {
            /// 超過上限
        }
        print("add \(selectCustomPinArray.count)")
        nextBtn.backgroundColor = selectCustomPinArray.count > 0 ? ThemeManager.shared.colorPalettes.redBackgroundColor : ThemeManager.shared.colorPalettes.garyBackgroundColor
        nextBtn.isEnabled = selectCustomPinArray.count > 0 ? true : false
    }
}


// MARK: API
extension RouteSettingViewController {
    func callAPIRequestForStationList(queryString: String?, queryType: KJLRequest.RequestQueryType , carType: String) {
        KJLRequest.requestForStationList(queryType: queryType, queryString: queryString, type: carType) { (response) in
            guard let response = response as? StationListClass else {
                return
            }
            if queryString == nil {
                if let lists = response.datas {
                    /// 避免重塞
                    self.clusterManager.clearItems()
                    self.googleMapView.clear()
                    self.showStationPins(stationListDatas: lists)
                } else {
                    self.showStationPins(stationListDatas: [])
                }
            }
        }
    }
    
    // 設定站點大頭針並顯示
    func showStationPins(stationListDatas: [StationListClass.Data]) {
        showMarkerArray.removeAll()
        pinArray.removeAll()
        var tempDatas = [StationListClass.Data]()
        var index = 0
        for aData in stationListDatas {
            if titlerSwitch.isOn == true {
                if aData.stationCategory?.lowercased() == "porsche" {
                    if acSwitch.isOn == true && dcFilter.isOn == true && availableSwitch.isOn == true {
                        if aData.status == "01" || aData.status == "02" {
                            tempDatas.append(aData)
                        }
                    } else if acSwitch.isOn == true && dcFilter.isOn == true {
                        tempDatas.append(aData)
                    } else if acSwitch.isOn == true && availableSwitch.isOn == true {
                        if aData.hasAC == true {
                            if aData.status == "01" || aData.status == "02" {
                                tempDatas.append(aData)
                            }
                        }
                    } else if dcFilter.isOn == true && availableSwitch.isOn == true {
                        if aData.hasDC == true {
                            if aData.status == "01" || aData.status == "02" {
                                tempDatas.append(aData)
                            }
                        }
                    } else if acSwitch.isOn == true {
                        if aData.hasAC == true {
                            tempDatas.append(aData)
                        }
                    } else if dcFilter.isOn == true {
                        if aData.hasDC == true {
                            tempDatas.append(aData)
                        }
                    }
                }
            } else {
                if acSwitch.isOn == true && dcFilter.isOn == true && availableSwitch.isOn == true {
                    if aData.status == "01" || aData.status == "02" {
                        tempDatas.append(aData)
                    }
                } else if acSwitch.isOn == true && availableSwitch.isOn == true {
                    if aData.hasAC == true {
                        if aData.status == "01" || aData.status == "02" {
                            tempDatas.append(aData)
                        }
                    }
                } else if dcFilter.isOn == true && availableSwitch.isOn == true {
                    if aData.hasDC == true {
                        if aData.status == "01" || aData.status == "02" {
                            tempDatas.append(aData)
                        }
                    }
                } else if acSwitch.isOn == true && dcFilter.isOn == true {
                    tempDatas.append(aData)
                } else if acSwitch.isOn == true {
                    if aData.hasAC == true {
                        tempDatas.append(aData)
                    }
                } else if dcFilter.isOn == true {
                    if aData.hasDC == true {
                        tempDatas.append(aData)
                    }
                } else {
                    if aData.stationCategory?.lowercased() == "porsche" {
                        if aData.hasAC == true && aData.hasDC == true && aData.status == "01" && aData.status == "02" {
                            tempDatas.append(aData)
                        }
                    }
                }
            }
        }
        
        for (i,tempData) in tempDatas.enumerated() {
            let location = CLLocationCoordinate2D(latitude: Double(tempData.latitude ?? "0") ?? 0, longitude: Double(tempData.longitude ?? "0") ?? 0)
            let pin = CustomPin(coordinate: location)
            pin.title = tempData.stationName
            pin.pk = tempData.stationId ?? ""
            pin.num = "\(i + 1)"
            if i + 1 > 19 {
                pin.num = "0"
            }
            // 站點狀態：01: 可使用 , 02: 使用中 , 03: 未連線 , 04: 未營運
            var imgName: String = AnnotationType.NONE.rawValue
            let stationCarGory = tempData.stationCategory?.lowercased()

            if tempData.hasDC == true {
                if stationCarGory == "porsche"{
                    imgName = MapManger.dcPorscheICON(tempData)
                } else {
                    imgName = MapManger.dcICON(tempData)
                }
            } else {
                if stationCarGory == "porsche"{
                    imgName = MapManger.acPorscheICON(tempData)
                } else{
                    imgName = MapManger.acICON(tempData)
                }
            }
            
            pin.imageName = imgName
            pin.data = tempData
             
            let marker = POIItem(position: CLLocationCoordinate2DMake(location.latitude, location.longitude), name: pin.title!)
            
            /// check empty total
            var emptyToString = ""
            if tempData.connection == false {
                var total = ""
                if tempData.hasDC == true {
                    total = "\(tempData.dcTotal ?? 0)"
                } else {
                    total = "\(tempData.acTotal ?? 0)"
                }
                emptyToString = total
            } else {
                if tempData.hasDC == true {
                    let empty = "\(tempData.dcEmpty ?? 0)"
                    let total = "\(tempData.dcTotal ?? 0)"
                    emptyToString = empty + "/" + total
                } else {
                    if stationCarGory == "porsche"{
                        let total = "\(tempData.acTotal ?? 0)"
                        emptyToString = total
                    } else {
                        let empty = "\(tempData.acEmpty ?? 0)"
                        let total = "\(tempData.acTotal ?? 0)"
                        emptyToString = empty + "/" + total
                    }
                }
            }
            
            if tempData.isBussinessTime?.lowercased() == "n" {
                marker.iconImage = UIImage(named: imgName)
            } else {
                let toImage = ImageManager.share.textToImage(drawText: emptyToString, inImage: UIImage(named: pin.imageName) ?? UIImage())
                marker.iconImage = toImage
            }
            
            marker.index = index
            index += 1
            self.showMarkerArray.append(marker)
            self.pinArray.append(pin)
            clusterManager.add(marker)
        }
        clusterManager.cluster()
    }
}


// MARK: - TextField Delegate
extension RouteSettingViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if (textField == self.startTextField) {
//            self.focusType = .Start
            textField.resignFirstResponder()
            let vc = RoutePlaningSearchVC(nibName: "RoutePlaningSearchVC", bundle: nil)
            vc.delegate = self
            vc.whereTo  = .Start
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle   = .crossDissolve
            present(vc, animated: true, completion: nil) 
        } else if (textField == self.endTextField){
//            self.focusType = .End
            textField.resignFirstResponder()
            let vc = RoutePlaningSearchVC(nibName: "RoutePlaningSearchVC", bundle: nil)
            vc.delegate = self
            vc.whereTo  = .End
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle   = .crossDissolve
            present(vc, animated: true, completion: nil)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
}

extension RouteSettingViewController: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
//        if (self.focusType == .Start) {
//            self.startTextField.text = place.formattedAddress
//        } else if (self.focusType == .End) {
//            self.endTextField.text = place.formattedAddress
//        }
//        self.dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("AutocompleteError: \(error.localizedDescription)")
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        
    }
}

// MARK: - CLLocationManager Delegate
extension RouteSettingViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.authorizedWhenInUse {
            googleMapView.isMyLocationEnabled = true
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Get Location Error: \(error)")
    }
}

extension RouteSettingViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        guard let poiItem = marker.userData as? POIItem , poiItem.isNotFound == false else{
          return false
        }
        routeSettingBottomView.isHidden = true
        willDisplayStationDetail(display: true)
        downrouteSettingView(animate: false)
        let pin = pinArray[poiItem.index]
        selectPin = pin
        temporaryMarker = marker
        self.selectDistance = "\(poiItem.distance) km"
        if let data = pin.data {
            stationName.text = data.stationName ?? ""
            stationAddress.text = data.stationAddress ?? ""
            
            let dcTotal = data.dcTotal ?? 0
            let dcConut = data.dcEmpty ?? 0
            let acTotal = data.acTotal ?? 0
            let acConut = data.acEmpty ?? 0
            
            dcAvailable.text =  LocalizeUtils.localized(key: "StationAvailable") + ":\(dcConut)" + " / " + LocalizeUtils.localized(key: "StationInstalled") + ":\(dcTotal)"
            acAvailable.text = LocalizeUtils.localized(key: "StationAvailable") + ":\(acConut)" + " / " + LocalizeUtils.localized(key: "StationInstalled") + ":\(acTotal)"
            
            dcStackView.isHidden = dcTotal == 0 ? true : false
            acStackView.isHidden = acTotal == 0 ? true : false
            
            // 站點狀態：01: 可使用 , 02: 使用中 , 03: 未使用 , 04: 未營運
            if data.status ==  "04" || data.status ==  "05" || data.connection == false  {
                dcAvailable.text = LocalizeUtils.localized(key: "StationInstalled") + ":\(dcTotal)"
                acAvailable.text = LocalizeUtils.localized(key: "StationInstalled") + ":\(acTotal)"
            }
             
            let lat = Float(data.latitude ?? "0") ?? 0
            let long = Float(data.longitude ?? "0") ?? 0
            LocationManagers.updateLocation { (_, _) in }
            
            let myLocation = LocationManager.instanceShare.myLocation.coordinate
            if myLocation.latitude != 0.0 && myLocation.longitude != 0.0 {
                let current = CLLocation(latitude: (myLocation.latitude), longitude: (myLocation.longitude))
                let location = CLLocation(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(long))
                let distance = current.distance(from: location)
                let meter = "\(distance)"
                let km = String(format: "%.2f", (Float(meter) ?? 0)/1000)
                kmLabel.text = km + "km"
            }
        }
        nextBtn.isHidden = false
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
    }
    
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        
    }
}

extension RouteSettingViewController: GoogleAPIRequestManagerDelegate {
    
    func googleGeocodingResultWith(geocodingData: GoogleGeocodingDataClass) {
//        self.removeMarker()
        if let status = geocodingData.status  {
            if status == "OK" {
                if let formattedAddress = geocodingData.formattedAddress,let resultLatitude = geocodingData.lat,let resultLongitude = geocodingData.lng {
                    if (geocodingData.locationType == .Start) {
                        self.startCoodinate = CLLocationCoordinate2DMake(resultLatitude, resultLongitude)
                        self.addMarkerOnGoogleMapWith(title: formattedAddress, coodinate: self.startCoodinate!, geocodingLocationType: .Start)
                        self.stationCoodinateArray.append(self.startCoodinate!)
                    } else if geocodingData.locationType == .End {
                        self.endCoodinate = CLLocationCoordinate2DMake(resultLatitude, resultLongitude)
                        self.addMarkerOnGoogleMapWith(title: formattedAddress, coodinate: self.endCoodinate!, geocodingLocationType: .End)
                    } else {
                        self.startTextField.text = formattedAddress
                    }
                    
                    if let startCoodinate = self.startCoodinate ,let endCoodinate = self.endCoodinate {
                        googleMapView.animate(with: GMSCameraUpdate.coordinateBounds(startCoordinate: startCoodinate, endCoordinate: endCoodinate))
                    }
                }
            } else {
                
            }
        } else {
            
        }
    }
    // 計算距離與時間
    func googleDistanceMatrixResultWith(distanceDurationData: GoogleDistanceMatrixClass) {
        // 檢視已添加站點清單 
        guard let status = distanceDurationData.status else { return }
        if (status == "OK") {
            // 站點篩選
            self.saveGoogleDistanceWith(distanceData: distanceDurationData)
            pinIndex += 1
            if (pinIndex < self.stationCoodinateArray.count - 1) {
                self.calculateDistance()
            } else {
                self.saveRouteStationFileWith(routeSaveName: self.routeSaveName)
                if (self.routeSaveName == self.temporaryFileName) {
                    SVProgressHUD.dismiss()
                    
                    let vc = RouteSettingDetailVC(nibName: "RouteSettingDetailVC", bundle: nil)
                    vc.dataName = self.temporaryFileName
                    self.navigationController?.pushViewController(vc, animated: true)
                    
//                    guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "RouteDetailViewController") as? RouteDetailViewController else { return }
//                    vc.setupVCWith(title: "檢視已添加站點清單", dataName: self.temporaryFileName , isShowNaviBtn: false)
//                    self.navigationController?.show(vc, sender: self)
                }
            }
        }
        
    }
    // 導航
    func googleDirectionResultWith(directionData: GoogleDirectionDataClass) {
        
    }
}


//MARK: Save
extension RouteSettingViewController {
    // 刪除站點清單的暫存檔
    private func deleteTemporaryStationListFileWith(name: String) {
        let fileManager = FileManager.default
        let file = NSHomeDirectory() + "/Documents/" + name + ".data"
        do {
            try fileManager.removeItem(atPath: file)
        } catch {
            print("檢視清單暫存檔刪除失敗")
        }
    }
    
    // 路線清單詳細資訊 - 清單資訊處理
    private func stationInfoListDataActionWith(routeSaveName: String) {
        // 處理檔案
        self.stationCoodinateArray.append(self.endCoodinate!)
        self.calculateDistance()
        SVProgressHUD.show()
    }
    
    // 計算已存的所有站點間的距離
    private func calculateDistance() {
        let start = self.stationCoodinateArray[pinIndex]
        let end = self.stationCoodinateArray[pinIndex + 1]
        self.callGoogleDistanceMatrixAPIWith(startCoodinate: start, endCoodinate: end, avoidType: self.avoidType, resultType: .Filter)
    }
    
    // 路線清單詳細資訊 - 修改、存檔
    private func saveRouteStationFileWith(routeSaveName: String) {
        let startStationObj = RouteStationInfoObjClass(stationName: self.startLocation, distance: self.stationDistanceArray[0], stationId: "" , latitude: "\(self.stationCoodinateArray[0].latitude)" , longitude: "\(self.stationCoodinateArray[0].longitude)", address: self.startLocation, stationCategory: nil, hasDC: nil, hasAC: nil, connection: nil)
        self.stationInfoArray.append(startStationObj)
        print("Coodinate Count: \(self.stationCoodinateArray.count)")
        print("CustomPin Count: \(self.selectCustomPinArray.count)")
        print("Distance Count: \(self.stationDistanceArray.count)")
        for i in 0..<self.selectCustomPinArray.count {
            let pin = self.selectCustomPinArray[i]
            let distance = self.stationDistanceArray[i + 1]
            
            let stationInfoObj = RouteStationInfoObjClass(stationName: pin.stationName ?? "", distance: distance, stationId: pin.stationId ?? "" , latitude: "\(pin.latitude ?? "")" , longitude: "\(pin.longitude ?? "")", address: pin.stationAddress ?? "", stationCategory: nil, hasDC: pin.hasDC ?? false, hasAC: pin.hasAC ?? false, connection: pin.connection ?? false)
            self.stationInfoArray.append(stationInfoObj)
        }
        
        
        let endStationObj = RouteStationInfoObjClass(stationName: self.endLocation, distance: "", stationId: "" , latitude: "\((self.stationCoodinateArray.last?.latitude ?? 0))" , longitude: "\((self.stationCoodinateArray.last?.longitude ?? 0))", address: self.endLocation, stationCategory: nil, hasDC: nil, hasAC: nil, connection: nil)
        self.stationInfoArray.append(endStationObj)
        
        
        // 存檔
        let routeDetailDataFile = NSHomeDirectory() + "/Documents/" + routeSaveName + ".data"
        let routeStationInfoListManager = RouteStationInfoListManager(stationInfoArray: self.stationInfoArray)
        if NSKeyedArchiver.archiveRootObject(routeStationInfoListManager, toFile: routeDetailDataFile) == false {
            SVProgressHUD.dismiss()
            print("路線規劃 - 路線詳細資訊 - 存檔 - 失敗")
        } else {
            SVProgressHUD.dismiss()
            print("路線規劃 - 路線詳細資訊 - 存檔 - 成功")
        }
    }
    
    func setupInitValue() {
        self.stationInfoArray = []
        // 為了避免檢視完已添加站點清單後，資料不對，所以看完清單回到頁面要把最後添加的目的地座標移除掉，觀看站點資訊回來時，不需要刪除最後一筆資料
        self.stationDistanceArray = []
        self.pinIndex = 0
    }
}


// MARK: - Google Maps API
extension RouteSettingViewController {
    /// Google 地址座標轉換 API
    private func callGoogleGeocodingAPIWith(addressString: String , geocodingLocationType : GeocodingLocationType, completion: @escaping ()-> Void) {
        googleAPIManager.callGoogleAddressConvertToCoodinateWith(addressString: addressString , geocodingLocationType: geocodingLocationType)
        completion()
    }
    
    /// Google 計算距離＆時間
    private func callGoogleDistanceMatrixAPIWith(startCoodinate: CLLocationCoordinate2D , endCoodinate: CLLocationCoordinate2D , avoidType: GoogleRouteAvoidType , resultType: DistanceMatrixType) {
        googleAPIManager.callGoogleDistanceMatrixAPIWith(startCoodinate: startCoodinate, endCoodinate: endCoodinate, avoidType: avoidType , resultType: resultType)
    }
    
    /// Google 導航＆計算方位
    private func callGoogleDirectionAPIWith(startCoodinate: CLLocationCoordinate2D , endCoodinate: CLLocationCoordinate2D , avoidType: GoogleRouteAvoidType, lineColor: UIColor) {
        googleAPIManager.callGoogleDirectionAPIWith(startCoodinate: startCoodinate, endCoodinate: endCoodinate, avoidType: avoidType, googleMapView: self.googleMapView, lineColor: lineColor)
    }
    
    // 在 Google Map 加大頭針
    private func addMarkerOnGoogleMapWith(title: String , coodinate: CLLocationCoordinate2D , geocodingLocationType: GeocodingLocationType) {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: coodinate.latitude, longitude: coodinate.longitude)
        marker.title = title
        switch geocodingLocationType {
        case .Start: marker.icon = UIImage(named: ToolPhoto.points_Start.rawValue)
        case .End:   marker.icon = UIImage(named: ToolPhoto.points_End.rawValue)
        default: break
        }
        marker.map = self.googleMapView
    }
    
    // 儲存 Google Api 回來的距離
    private func saveGoogleDistanceWith(distanceData: GoogleDistanceMatrixClass) {
        if let distance = distanceData.distanceText {
            self.stationDistanceArray.append(distance)
            
        }
    }
}


//MARK: GMUClusterRendererDelegate
extension RouteSettingViewController: GMUClusterRendererDelegate {
    func renderer(_ renderer: GMUClusterRenderer, willRenderMarker marker: GMSMarker) {
        marker.groundAnchor = CGPoint(x: 0.5, y: 1)
        if let markerData = (marker.userData as? POIItem) {
           let icon = markerData.iconImage
            marker.icon = icon
        }
    }
}


//MARK: GMUClusterManagerDelegate
extension RouteSettingViewController :GMUClusterManagerDelegate {
    func clusterManager(_ clusterManager: GMUClusterManager, didTap clusterItem: GMUClusterItem) -> Bool {
        return false
    }
    
    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool {
        let newCamera = GMSCameraPosition.camera(withTarget: cluster.position,
          zoom: googleMapView.camera.zoom + 1)
        let update = GMSCameraUpdate.setCamera(newCamera)
        googleMapView.animate(with: update)
        return true
    }
}

 
//NARK: Route Planing Search
extension RouteSettingViewController: RoutePlaningSearchDelegate {
    func getAddress(text: String, whereTo: GeocodingLocationType) {
        if whereTo == .Start {
            startTextField.text = text
        } else if whereTo == .End {
            endTextField.text = text
        }
    }
}


extension RouteSettingViewController: ShowCheckMessageDelegate {
    func popMessage(content: String) {
        let vc = ShowCheckMessageVC()
        vc.toContent = content
        vc.toTitle   = ""
        vc.delegate  = self
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        present(vc, animated: true, completion: nil)
    }
    
    func tapConfirm(tagCount: Int) {
        addStationToList()
        self.dismiss(animated: true, completion: nil)
    }
    
    func tapCancel() {
        self.dismiss(animated: true, completion: nil)
    }
}
