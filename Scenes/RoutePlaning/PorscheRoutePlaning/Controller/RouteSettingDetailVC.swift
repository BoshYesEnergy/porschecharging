//
//  RouteSettingDetailVC.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/18.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit
import SVProgressHUD

class RouteSettingDetailVC: BaseViewController {

    @IBOutlet weak var listTable: UITableView!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var navBtn: UIButton!
    
    private var routeListArray: [RoutePlanObjClass] = []
    private var dataArray: [RouteStationInfoObjClass] = []
    private var saveDataArray: [RouteStationInfoObjClass] = []
    var dataName = ""
    var dontSave: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setHeaderView("Route Planning", action: .POP)
        hiddenNavigationBar(isHidden: true)
        setToolButton(nil, UIImage(named: "button_close_default"), UIImage(named: "button_close_click")) {
            self.dismiss(animated: true, completion: nil)
        }
        listTable.register(UINib(nibName: "RouteSettingDetailCell", bundle: nil), forCellReuseIdentifier: "RouteSettingDetailCell")
        loadDataWith(dataName: dataName)
        saveBtn.isHidden = !dontSave
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) 
    }
    
    // 路線清單詳細資訊 - 修改、存檔
    private func saveRouteStationFileWith(routeSaveName: String, completion: @escaping ()-> Void ) {
        saveDataArray.removeAll()
        let startStationObj = RouteStationInfoObjClass(stationName: "Starting Point", distance: self.dataArray[0].distance ?? "", stationId: "" , latitude: "\(self.dataArray[0].latitude ?? "")" , longitude: "\(self.dataArray[0].longitude ?? "")", address: self.dataArray[0].address ?? "", stationCategory: nil, hasDC: nil, hasAC: nil, connection: nil)
        self.saveDataArray.append(startStationObj)
        for i in 0..<self.dataArray.count {
            let pin = self.dataArray[i]
//            let distance = self.dataArray[i + 1].distance
            if i != 0 || i != dataArray.count - 1 {
                let distance = self.dataArray[i].distance
                
                let stationInfoObj = RouteStationInfoObjClass(stationName: pin.stationName ?? "", distance: distance, stationId: pin.stationId ?? "" , latitude: "\(pin.latitude ?? "")" , longitude: "\(pin.longitude ?? "")", address: pin.address ?? "", stationCategory: nil, hasDC: pin.hasDC ?? false, hasAC: pin.hasAC ?? false, connection: pin.connection ?? false)
                self.saveDataArray.append(stationInfoObj)
            }
        }
        
        
        let endStationObj = RouteStationInfoObjClass(stationName: "Destination", distance: "", stationId: "" , latitude: "\((self.dataArray.last?.latitude ?? ""))" , longitude: "\((self.dataArray.last?.longitude ?? ""))", address: dataArray.last?.address ?? "", stationCategory: nil, hasDC: nil, hasAC: nil, connection: nil)
        self.saveDataArray.append(endStationObj)
        
        
        // 存檔
        let routeDetailDataFile = NSHomeDirectory() + "/Documents/" + routeSaveName + ".data"
        let routeStationInfoListManager = RouteStationInfoListManager(stationInfoArray: self.saveDataArray)
        if NSKeyedArchiver.archiveRootObject(routeStationInfoListManager, toFile: routeDetailDataFile) == false {
            SVProgressHUD.dismiss()
            print("路線規劃 - 路線詳細資訊 - 存檔 - 失敗")
        } else {
            SVProgressHUD.dismiss()
            print("路線規劃 - 路線詳細資訊 - 存檔 - 成功")
        }
        completion()
    }
    
    func loadDataWith(dataName: String) {
        let routeListDataFile = NSHomeDirectory() + "/Documents/" + dataName + ".data"
        if let tmp = NSKeyedUnarchiver.unarchiveObject(withFile: routeListDataFile) as? RouteStationInfoListManager {
            print("路線規劃清單 - 讀檔 - 成功")
            self.dataArray.removeAll()
            self.dataArray = tmp.stationInfoArray!
            self.listTable.reloadData()
        } else {
            print("路線規劃清單 - 讀檔 - 失敗")
        }
    }
    
    // 路線清單 - 讀取、修改、修改
    private func routeListDataActionWith(routeSaveName: String) {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm"
        let createTime = formatter.string(from: date)
        // 讀檔
        let routeListDataFile = NSHomeDirectory() + "/Documents/RouteListData.data"
        if let tmp = NSKeyedUnarchiver.unarchiveObject(withFile: routeListDataFile) as? RoutePlanListManager {
            self.routeListArray = tmp.planListArray!
            print("路線規劃 - 路線清單 - 讀檔 - 成功")
        } else {
            print("路線規劃 - 路線清單 - 讀檔 - 失敗")
        }
        
        // 更新檔案
        let routePlanObj = RoutePlanObjClass(routeName: routeSaveName, createTime: createTime, stationCount: "\(self.dataArray.count)", dataName: routeSaveName)
        self.routeListArray.append(routePlanObj)
        
        // 存檔
        let routeListManager = RoutePlanListManager(planListArray: self.routeListArray)
        if NSKeyedArchiver.archiveRootObject(routeListManager, toFile: routeListDataFile) == false {
            print("路線規劃 - 路線清單 - 存檔 - 失敗")
//            ShowMessageView.showMessage(title: nil, message: "儲存失敗", completion: { (isOK) in
//                
//            })
        } else {
            print("路線規劃 - 路線清單 - 存檔 - 成功")
            self.deleteTemporaryStationListFileWith(name: self.dataName)
//            ShowMessageView.showMessage(title: nil, message: "儲存完成", completion: { (isOK) in
//                if (isOK == true) {
//
//                }
//            })
        }
    }
    
    // 刪除站點清單的暫存檔
    private func deleteTemporaryStationListFileWith(name: String) {
        let fileManager = FileManager.default
        let file = NSHomeDirectory() + "/Documents/" + name + ".data"
        do {
            try fileManager.removeItem(atPath: file)
        } catch {
            print("檢視清單暫存檔刪除失敗")
        }
    }
    
    @IBAction func save(_ sender: UIButton) {
        popMessage()
    }
    
    @IBAction func navigate(_ sender: UIButton) {
        let data = dataArray.last
        if let latitude = data?.latitude {
            if let longitude = data?.longitude {
                
                var coodinateArray : [CLLocationCoordinate2D] = []
                for data in dataArray {
                    if let latitude = data.latitude {
                        if let longitude = data.longitude {
                            let coodinate = CLLocationCoordinate2D(latitude: Double(latitude) ?? 0, longitude: Double(longitude) ?? 0)
                            coodinateArray.append(coodinate)
                        }
                    }
                }
                KJLCommon.openMultiStationMapGoogle(dataArray: coodinateArray)
            }
        }
    }
}


extension RouteSettingDetailVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "RouteSettingDetailCell", for: indexPath) as? RouteSettingDetailCell {
            let data = dataArray[indexPath.row]
            cell.index = indexPath.row
            cell.data = data
            if indexPath.row == 0 {
                cell.titleName.text     = "Starting Point"
                cell.otherImage.image   = UIImage(named: "icon_location_default")
                cell.iconImage.image    = UIImage(named: "")
                cell.garyLine.isHidden  = true
                cell.deleteBtn.isHidden = true
                
            } else if indexPath.row + 1 == dataArray.count {
                cell.titleName.text     = "Destination"
                cell.otherImage.image   = UIImage(named: "icon_destination_default")
                cell.iconImage.image    = UIImage(named: "")
                cell.titleKm.text       = ""
                cell.garyLine.isHidden  = true
                cell.deleteBtn.isHidden = true
            } else {
                cell.otherImage.image   = UIImage(named: "")
                cell.titleName.text     = data.stationName ?? ""
                cell.titleKm.text       = data.distance ?? ""
                cell.garyLine.isHidden  = false
                cell.deleteBtn.isHidden = false
                cell.delegate           = self
                cell.titleAddress.text  = dataArray[indexPath.row].address ?? ""
            }
            if dontSave == false {
                cell.deleteBtn.isHidden = true
            }
            
            return cell
        }
        return UITableViewCell()
    }
}


extension RouteSettingDetailVC: RouteSettingDetailDelegate {
    func deleteStation(index: Int) {
        dataArray.remove(at: index)
        listTable.reloadData()
        saveRouteStationFileWith(routeSaveName: dataName) { }
    }
}


extension RouteSettingDetailVC: ShowTextFieldInformationDelegate {
    private func popMessage() {
        let vc = ShowTextFieldInformationVC()
        vc.titlName = "Save the route plan"
        vc.other    = "Route"
        vc.placeholder = "Enter the name"
        vc.delegate = self
        vc.modalPresentationStyle = .overFullScreen
        present(vc, animated: true, completion: nil)
    }
    
    func tapConfirm(text: String, delegateTag: Int) {
        dataName = text
        routeListDataActionWith(routeSaveName: text)
        saveRouteStationFileWith(routeSaveName: text) {
            self.dismiss(animated: true) {
                self.navigationController?.popToRootViewController(animated: true)
                NotificationCenter.default.post(name: .updateRouteList, object: nil)
            }
        }
    }
    
    func tapCancel() {
        self.dismiss(animated: true, completion: nil)
    }
}
 
