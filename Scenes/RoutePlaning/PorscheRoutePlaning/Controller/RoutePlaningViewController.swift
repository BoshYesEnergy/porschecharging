//
//  RoutePlaningViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class RoutePlaningViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var hintLabel: UILabel!
    
    private var routeListArray: [RoutePlanObjClass] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setHeaderView("Route Planning", action: .DISMISS)
        hiddenNavigationBar(isHidden: true)
        setTable()
        setToolButton(nil, UIImage(named: "button_close_default"), UIImage(named: "button_close_click")) {
            self.dismiss(animated: true, completion: nil)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(getRouteList), name: .updateRouteList, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getRouteList()
    }
    
    private func setTable() {
        tableView.register(UINib(nibName: "SetRouteCell", bundle: nil), forCellReuseIdentifier: "SetRouteCell")
    }
    
    @objc func getRouteList() {
        // 路線清單
        let routeListDataFile = NSHomeDirectory() + "/Documents/RouteListData.data"
        if let tmp = NSKeyedUnarchiver.unarchiveObject(withFile: routeListDataFile) as? RoutePlanListManager {
            print("路線規劃清單 - 讀檔 - 成功")
            self.routeListArray = tmp.planListArray!
            self.tableView.reloadData()
            
        } else {
            print("路線規劃清單 - 讀檔 - 失敗")
        }
    }
    
    // 刪除點擊後要帶入跳轉頁面的檔案
    private func deleteStationDetailDataWith(dataName: String) {
        let fileManager = FileManager.default
        let fileName = NSHomeDirectory() + "/Documents/" + dataName + ".data"
        do {
            try fileManager.removeItem(atPath: fileName)
        } catch {
            print("刪除失敗")
        }
    }
}

// MARK: - TableView Data Source
extension RoutePlaningViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return routeListArray.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == routeListArray.count {
            if let setRouteCell = tableView.dequeueReusableCell(withIdentifier: "SetRouteCell", for: indexPath) as? SetRouteCell {
                setRouteCell.delegate = self
                setRouteCell.content = "You don’t have any charging route, please add a new one."
                setRouteCell.hiddenButton = routeListArray.count == 0
                return setRouteCell
            } else {
                return UITableViewCell()
            }
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "RoutePlaningCell", for: indexPath) as? RoutePlaningCell {
                let data = routeListArray[indexPath.row]
                cell.setupCellWith(name: data.routeName ?? "", time: data.createTime ?? "" )
                return cell
            } else {
                return UITableViewCell()
            }
        }
    }
}

// MARK: - TableView Delegate
extension RoutePlaningViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 80
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == routeListArray.count {
            
        } else {
            if let vc = RouteSettingDetailVC(nibName: "RouteSettingDetailVC", bundle: nil) as? RouteSettingDetailVC {
                let data = routeListArray[indexPath.row]
                vc.dataName = data.dataName ?? ""
                vc.dontSave = false
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
            
//            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "RouteDetailViewController" ) as? RouteDetailViewController {
//                let data = routeListArray[indexPath.row]
//                vc.setupVCWith(title: data.routeName ?? "", dataName: data.dataName ?? "", isShowNaviBtn: true)
//                self.navigationController?.show(vc, sender: self)
//            }
        } 
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "刪除") { (delete, indexPath) in
            ShowMessageView.showQuestion(title: nil, message: "確定是否刪除？", completion: { (isOK) in
                if (isOK == true) {
                    
                    let routeListData = self.routeListArray[indexPath.row]
                    self.deleteStationDetailDataWith(dataName: routeListData.dataName ?? "")
                    
                    self.routeListArray.remove(at: indexPath.row)
                    self.tableView.deleteRows(at: [indexPath], with: .automatic)
                    
//                    if (self.routeListArray.count > 0) {
//                        self.hintLabel.isHidden = true
//                        self.tableView.isHidden = false
//                    } else {
//                        self.hintLabel.isHidden = false
//                        self.tableView.isHidden = true
//                    }
                    
                    // 路線清單 - 存檔
                    let routeListDataFile = NSHomeDirectory() + "/Documents/RouteListData.data"
                    let routeListManager = RoutePlanListManager(planListArray: self.routeListArray)
                    if NSKeyedArchiver.archiveRootObject(routeListManager, toFile: routeListDataFile) == false {
                        print("刪除後 - 存檔 - 失敗")
                    } else {
                        print("刪除後 - 存檔 - 成功")
                    }
                }
            })
        }
        return [deleteAction]
    }
}


//MARK: Protocol
extension RoutePlaningViewController: SetRouteDelegate {
    func tapLeft() {
        // SetRouteCell
        // Edit 
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RoutePlaningEditVC") as! RoutePlaningEditVC
        vc.hiddenNavigationBar(isHidden: true)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func tapRight() {
        // SetRouteCell
        // Add RoutePlaning
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RouteSettingViewController") as! RouteSettingViewController
        vc.hiddenNavigationBar(isHidden: true)
        navigationController?.pushViewController(vc, animated: true)
    }
}
