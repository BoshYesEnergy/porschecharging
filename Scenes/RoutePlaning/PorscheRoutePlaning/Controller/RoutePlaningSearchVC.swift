//
//  RoutePlaningSearchVC.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/16.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

protocol RoutePlaningSearchDelegate: class {
    func getAddress(text: String, whereTo: GeocodingLocationType)
}

class RoutePlaningSearchVC: UIViewController {
    
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchTable: UITableView!
    @IBOutlet weak var tableHeightConstraint: NSLayoutConstraint!
    
    var whereTo: GeocodingLocationType = .None
    weak var delegate: RoutePlaningSearchDelegate?
    var searchText: String = ""
    var searchDataArray: [StationSearchModel.Data] = []
    private var googleSearchArray: [Dictionary<String,AnyObject>] = Array()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        settingView()
        setTable()
        settingKeyboard()
        NotificationCenter.default.addObserver(self, selector: #selector(searchChange(obj:)), name: .UITextFieldTextDidChange, object: searchTextField)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.onIQKeyboardManager()
    }
    
    fileprivate func settingView() {
        borderView.layer.cornerRadius = 5
        borderView.layer.borderColor  = #colorLiteral(red: 0.7882352941, green: 0.7921568627, blue: 0.7960784314, alpha: 1)
        borderView.layer.borderWidth  = 1
        
        searchTable.layer.cornerRadius = 5
        searchTextField.clearButtonMode = UITextFieldViewMode.whileEditing
        searchTextField.text = searchText
        callStationSerch("") { }
    }
    
    fileprivate func setTable() {
        searchTable.register(UINib(nibName: "NormalCell", bundle: nil), forCellReuseIdentifier: "NormalCell")
    }
    
    fileprivate func settingKeyboard() {
        self.offIQKeyboardManager()
        searchTextField.becomeFirstResponder()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardSohw(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide(notification:)), name: .UIKeyboardWillHide, object: nil)
    }
}


//MARK: Action
extension RoutePlaningSearchVC {
    @IBAction func back(_ sender: UIButton) {
        delegate?.getAddress(text: searchTextField.text ?? "", whereTo: whereTo)
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func keyboardSohw(notification: Notification) {
        if let userInfo = notification.userInfo,
           let keyboardRectangle = userInfo[UIKeyboardFrameEndUserInfoKey] as? CGRect {
            print("keyboardRectangle height: \(keyboardRectangle.height)")
            tableHeightConstraint.constant = self.view.frame.height - keyboardRectangle.height - 150
            searchTable.layoutIfNeeded()
        }
    }
    
    @objc private func keyboardHide(notification: Notification) {
        
    }
    
    @objc func searchChange(obj: Notification) {
        let pattern = "^[\\u4E00-\\u9FA5A-Za-z0-9_]+$"
        let matcher = MyRegex(pattern)
        let chinese = searchTextField.text ?? ""
        self.googleSearchArray.removeAll()
        if matcher.match(input: chinese) { /// 收尋內容符合
            printLog("OO")
            if searchText != searchTextField.text {
                searchText = searchTextField.text ?? ""
                if searchText.count > 0 {
                    callStationSerch(searchText) {
                        //導入Google Map
                        self.searchPlaceFromGoogle(place: self.searchText) {
                            if self.googleSearchArray.count < 3 {
                                //                                self.searchGeocodeFromGoogle(place: self.searchText)
                            }
                        }
                    }
                } else {
                    self.searchDataArray = []
                    self.googleSearchArray = []
                    searchTable.reloadData()
                }
            }
        } else { /// 收尋內容不符合
            searchText = ""
            if (searchTextField.text?.count ?? 0) <= 0 {
                self.searchDataArray = []
                searchTable.reloadData()
            }
            printLog("XX")
        }
    }
    
    private func searchPlaceFromGoogle(place: String, completion: @escaping ()-> Void) {
        /* Google Text Search Requests */
        // HTTP Url : https://maps.googleapis.com/maps/api/place/textsearch/output?parameters
        var strGoogleApi = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=\(place)+in+Taiwan&key=\(KJLRequest.googleKey)"
        //AIzaSyDrsdFkWJEsCElvhfQr7i5RXYevNwNgBqM
        strGoogleApi = strGoogleApi.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        var urlRequest = URLRequest(url: URL(string: strGoogleApi)!)
        urlRequest.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if error == nil {
                if data != nil {
                    let jsonDic = try? JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
                    //                    print(jsonDic)
                    if let dics = jsonDic as? Dictionary<String, AnyObject> {
                        if let results = dics["results"] as? [Dictionary<String,AnyObject>] {
                            print("json: \(results)")
                            for i in 0..<results.count {
                                if i < 3 {
                                    self.googleSearchArray.append(results[i])
                                }
                            }
                            DispatchQueue.main.async {
                                self.searchTable.reloadData()
                            }
                            completion()
                        }
                    }
                }
            }
        }
        task.resume()
    }
    
    private func searchGeocodeFromGoogle(place: String) {
        var geoCodeURL = "https://maps.googleapis.com/maps/api/geocode/json?address=\(place)+in+Taiwan&key=\(KJLRequest.googleKey)"
        geoCodeURL = geoCodeURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        var urlRequest = URLRequest(url: URL(string: geoCodeURL)!)
        urlRequest.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            guard data != nil else { return }
            let jsonDic = try? JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
            if let dics = jsonDic as? Dictionary<String, AnyObject> {
                guard let results = dics["results"] as? [Dictionary<String,AnyObject>]
                else { return }
                //                print("GeoCode json: \(results)")
                if self.googleSearchArray.count < 3 {
                    for i in 0..<results.count {
                        if self.googleSearchArray.count < 3 {
                            self.googleSearchArray.insert(results[i], at: 0)
                        }
                    }
                    DispatchQueue.main.async {
                        self.searchTable.reloadData()
                    }
                }
            }
        }
        task.resume()
    }
}


//MARK: Api
extension RoutePlaningSearchVC {
    /// search bar
    fileprivate func callStationSerch(_ queryString: String, completion: @escaping ()-> Void ) {
        KJLRequest.requestForStationSearch(queryString) {[weak self] (response) in
            guard let response = response as? StationSearchModel else { return }
            if let data = response.datas {
                self?.searchDataArray = data
                self?.searchTable.reloadData()
            } else {
                self?.searchDataArray = []
                self?.searchTable.reloadData()
            }
            completion()
        }
    }
}


//MARK: Tableview delegate & datasource
extension RoutePlaningSearchVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchDataArray.count + googleSearchArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "NormalCell", for: indexPath) as? NormalCell {
            if (indexPath.row <= searchDataArray.count - 1) {
                let data = searchDataArray[indexPath.row]
                cell.list = data
            } else {
                let data = googleSearchArray[indexPath.row - searchDataArray.count]
                if data["name"] != nil {
                    cell.titleCell.text      = data["name"] as? String ?? ""
                    cell.stationAddress.text = data["formatted_address"] as? String ?? ""
                    cell.stationICON.image   = UIImage(named: "icon_search_location_default")
                } else {
                    cell.stationAddress.text = data["formatted_address"] as? String ?? ""
                    cell.stationICON.image   = UIImage(named: "icon_search_location_default")
                }
            }
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return searchDataArray.count == 0 ? 80 : UITableViewAutomaticDimension 
    }
}


//MARK: Table view did select
extension RoutePlaningSearchVC {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if searchDataArray.count != 0  {
            searchTextField.text = searchDataArray[indexPath.row].address ?? ""
            print(searchDataArray[indexPath.row].address ?? "")
        } 
    }
}
