//
//  RoutePlaningEditVC.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/18.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class RoutePlaningEditVC: BaseViewController {

    @IBOutlet weak var routeTable: UITableView!
    
    private var routeListArray: [RoutePlanObjClass] = []
    var deleteArray: [Int] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setHeaderView("Route Planning", action: .POP)
        hiddenNavigationBar(isHidden: true)
        setToolButton(nil, UIImage(named: "button_close_default"), UIImage(named: "button_close_click")) {
            self.dismiss(animated: true, completion: nil)
        }
        routeTable.register(UINib(nibName: "RemoveEditCreditCell", bundle: nil), forCellReuseIdentifier: "RemoveEditCreditCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let routeListDataFile = NSHomeDirectory() + "/Documents/RouteListData.data"
        if let tmp = NSKeyedUnarchiver.unarchiveObject(withFile: routeListDataFile) as? RoutePlanListManager {
            print("路線規劃清單 - 讀檔 - 成功")
            self.routeListArray = tmp.planListArray!
        } else {
            print("路線規劃清單 - 讀檔 - 失敗")
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.post(name: .updateRouteList, object: nil)
    }
    
    // 刪除點擊後要帶入跳轉頁面的檔案
    private func deleteStationDetailDataWith(dataName: String) {
        let fileManager = FileManager.default
        let fileName = NSHomeDirectory() + "/Documents/" + dataName + ".data"
        do {
            try fileManager.removeItem(atPath: fileName)
        } catch {
            print("刪除失敗")
        }
    }
     
}


extension RoutePlaningEditVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return routeListArray.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == routeListArray.count {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "RemoveEditCreditCell", for: indexPath) as? RemoveEditCreditCell {
                cell.delegate = self
                cell.tapOff = true
                return cell
            } else {
                return UITableViewCell()
            }
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "RoutePlaningEditCell", for: indexPath) as? RoutePlaningEditCell {
                let data = routeListArray[indexPath.row]
                cell.index = indexPath.row
                cell.data = data
                cell.delegate = self
                return cell
            } else {
                return UITableViewCell()
            }
        }
    }
}


extension RoutePlaningEditVC: RoutePlaningEditDelegate {
    func check(index: Int) {
        if deleteArray.contains(index) == false {
            deleteArray.append(index)
            routeTable.reloadData()
            print("deleteArray: \(deleteArray)")
        } else {
            let newList = deleteArray.filter{ $0 != index }
            deleteArray.removeAll()
            deleteArray = newList
            routeTable.reloadData()
            print("deleteArray: \(deleteArray)")
        }
    }
}


extension RoutePlaningEditVC: RemoveEditCreditProtocol {
    func checkRemove() {
        for i in deleteArray {
            let routeListData = self.routeListArray[i]
            self.deleteStationDetailDataWith(dataName: routeListData.dataName ?? "") 
            self.routeListArray.remove(at: i)
            self.routeTable.reloadData()
            // 路線清單 - 存檔
            let routeListDataFile = NSHomeDirectory() + "/Documents/RouteListData.data"
            let routeListManager = RoutePlanListManager(planListArray: self.routeListArray)
            if NSKeyedArchiver.archiveRootObject(routeListManager, toFile: routeListDataFile) == false {
                print("刪除後 - 存檔 - 失敗")
            } else {
                print("刪除後 - 存檔 - 成功")
            }
        }
    }
}
