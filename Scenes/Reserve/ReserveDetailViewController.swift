//
//  ReserveDetailViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class ReserveDetailViewController: KJLNavView {

    //MARK: - IBOutlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var stationNameLabel: UILabel!
    @IBOutlet weak var chargingHeadImage: UIImageView!
    @IBOutlet weak var chargingNumLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var longCancelBtn: UIButton!
    @IBOutlet weak var routeNavButton: UIButton!
    @IBOutlet weak var readyChargeButton: UIButton!
    @IBOutlet weak var oneBtnView: UIView!
    @IBOutlet weak var twoBtnView: UIView!
    
    //MARK: - Property
    private var titleArray: [String] = ["預約結果：","地址：","預約建立時間：","預約時段：","預約費："]
    private var infoArray: [String] = []
    private var reserveDetailData: ReserveDetailClass.Data? = nil
    private var reserveNo: Int = 0
    private var isFromReserveRecordVC = false
    
    //MARK: -  生命週期
    override func viewDidLoad() {
        super.viewDidLoad() 
        self.setupVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - IBAction
    @IBAction func cancelAction(_ sender: UIButton) {
        // 取消預約
        ShowMessageView.showQuestion(title: nil, message: "確定取消預約？") { (isOK) in
            self.callAPIForRequestReserveCancel()
        }
    }
    // 導航
    @IBAction func routeNavAction(_ sender: UIButton) {
//        KJLCommon.openNavMap(lat: Double(reserveDetailData?.latitude ?? ""), long: Double(reserveDetailData?.longitude ?? ""), subControl: self)
    }
    
    // 準備充電
    @IBAction func readyChargeAction(_ sender: UIButton) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "QRCodeScanViewController") as? QRCodeScanViewController {
            self.navigationController?.show(vc, sender: self)
        }
    }
    
    //MARK: - 自訂 func
    override func backAction() {
        if (isFromReserveRecordVC == true) {
            self.navigationController?.popViewController(animated: true)
        } else {
            var vc: UIViewController? = self
            while vc?.presentingViewController != nil {
                print(vc ?? "")
                vc = vc?.presentingViewController
                print(self.presentingViewController ?? "")
            }
            vc?.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func setupDataWith(reserveNo: Int , isFromReserveRecordVC: Bool) {
        self.reserveNo = reserveNo
        self.isFromReserveRecordVC = isFromReserveRecordVC
    }
    
    private func setupVC() {
        self.stationNameLabel.text = ""
        self.chargingNumLabel.text = ""
        
        createNavViewForRightBtn(rightName: "", rightImage: "", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: UIColor.black)
        titleLab?.text = "預約充電明細"
        
        cancelButton.setEmptyWhiteShortImage()
        readyChargeButton.setFullCyanShortImage()
        longCancelBtn.setEmptyWhiteSpecialLongImage()
        
//        if (isFromReserveRecordVC == true) {
//            self.oneBtnView.isHidden = false
//            self.twoBtnView.isHidden = true
//        } else {
            self.oneBtnView.isHidden = true
            self.twoBtnView.isHidden = false
//        }
        
        self.callAPIRequestForReserveDetail()
    }
    
    //MARK: - API
    /// 803 預約取消
    private func callAPIForRequestReserveCancel() {
//        KJLRequest.requestForReserveCancel(reserveNo: self.reserveNo) { (response) in
//            if response != nil {
//                if (self.isFromReserveRecordVC == true) {
//                    self.navigationController?.popViewController(animated: true)
//                } else {
//                    var vc: UIViewController? = self
//                    while vc?.presentingViewController != nil {
//                        print(vc ?? "")
//                        vc = vc?.presentingViewController
//                        print(self.presentingViewController ?? "")
//                    }
//                    vc?.navigationController?.popToRootViewController(animated: true)
//                }
//            }
//        }
    }
    
    /// 805: 預約明細
    private func callAPIRequestForReserveDetail() {
        KJLRequest.requestForReserveDetail(reserveNo: self.reserveNo) { (response) in
            guard let response = response as? ReserveDetailClass else {
                return
            }
            
            guard let data = response.datas else {
                return
            }
            
            self.reserveDetailData = data
//            self.stationNameLabel.text = data.stationName ?? ""
//            self.chargingNumLabel.text = "充電槍代碼：\(data.qrNo ?? "")"
//            let image = data.socketType?.replacingOccurrences(of: "/", with: ":")
//            self.chargingHeadImage.image = UIImage(named: image ?? "")
            
//            if (self.isFromReserveRecordVC == true && data.reserveStatus != "預約中") {
//                self.twoBtnView.isHidden = true
//                self.oneBtnView.isHidden = true
//            }
            
//            self.infoArray.append(data.reserveStatus ?? "")
//            self.infoArray.append(data.stationAddr ?? "")
//            self.infoArray.append(data.reserveDate ?? "")
//            self.infoArray.append(data.reserveTime ?? "")
//            if (data.reserveAmount == 0) {
//                self.infoArray.append("$0")
//            } else {
//                self.infoArray.append("$\(data.reserveAmount ?? 0)")
//            }
//            self.tableView.reloadData()
        }
    }
}

//MARK: - TableView Datasource
extension ReserveDetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return infoArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "ReserveDetailCell", for: indexPath)
        if let cell = cell as? ReserveDetailCell {
            cell.setupCellWith(indexPath: indexPath, titleString: titleArray[indexPath.row], infoString: infoArray[indexPath.row])
        }
        return cell
    }
}

//MARK: - TableView Delegate
extension ReserveDetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 35
    }
}
