//
//  ReserveChargingViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class ReserveChargingViewController: KJLNavView {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var stationNameLabel: UILabel!
    @IBOutlet weak var chargingHeadImage: UIImageView!
    @IBOutlet weak var chargingNumLabel: UILabel!
    @IBOutlet weak var noticeLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    
    //MARK: - Property
    private var stationId: String = ""
    private var reservePrepareData: ReservePrepareClass.Data? = nil
    private var cardListArray: [ReservePrepareClass.Data.CardList] = []
    private var cardNo: Int = 0
    private var cardNumString = ""
    private var titleArray: [String] = []
    private var infoArray: [String] = []
    
    //MARK: -  生命週期
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createNavViewForRightBtn(rightName: "", rightImage: "", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: UIColor.black)
        titleLab?.text = "預約充電"
        
        cancelButton.setEmptyWhiteShortImage()
        confirmButton.setFullCyanShortImage()
        
        self.setupVCWithReservePrepareData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - IBAction
    @IBAction func cancelAction(_ sender: UIButton) {
        self.callAPIForRequestReserveCancel()
    }
    
    @IBAction func confirmAction(_ sender: UIButton) {
        guard (self.cardListArray.count > 0) else {
            ShowMessageView.showMessage(title: nil, message: "請先設定付款方式")
            return
        }
        ShowMessageView.showQuestion(title: nil, message: "確定預約？") { (isOK) in
            self.callAPIForRequestReserveStart()
        }
    }
    
    //MARK: - 自訂 func
    override func backAction() {
        self.callAPIForRequestReserveCancel()
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupStationId(stationId: String , reservePrepareData: ReservePrepareClass.Data) {
        self.stationId = stationId
        self.reservePrepareData = reservePrepareData
    }
    
    private func setupVCWithReservePrepareData() {
        self.stationNameLabel.text = ""
        self.chargingNumLabel.text = ""
        self.noticeLabel.text = ""
        
        let image = reservePrepareData?.socketType?.replacingOccurrences(of: "/", with: ":")
        self.chargingHeadImage.image = UIImage(named: image ?? "")
        
        self.cardListArray = reservePrepareData?.cardList ?? []
        if (self.cardListArray.count != 0) {
            for temp in self.cardListArray {
                if temp.isDefault == true {
                    self.cardNo = temp.cardNo ?? 0
                    self.cardNumString = temp.cardNum ?? ""
                    break
                }
            }
        } else {
            ShowMessageView.showMessage(title: nil, message: "請先設定付款方式")
        }
        
        self.stationNameLabel.text = reservePrepareData?.stationName
        self.chargingNumLabel.text = "充電槍代碼：\(reservePrepareData?.qrNo ?? "")"
        self.infoArray.append(reservePrepareData?.stationAddr ?? "")
        self.infoArray.append(reservePrepareData?.reserveDate ?? "")
        self.infoArray.append(reservePrepareData?.reserveTime ?? "")
        if (reservePrepareData?.reserveAmount == 0) {
            self.infoArray.append("$0")
        } else {
            self.infoArray.append("$\(reservePrepareData?.reserveAmount ?? 0)")
        }
        
        
        
        if (self.reservePrepareData?.vehicleNum?.count == 0) {
            if (self.reservePrepareData?.taxidName?.count == 0 && self.reservePrepareData?.taxidNum?.count == 0) {
                // YES 載具
                self.titleArray = ["地址：","預約建立時間：","預約時段：","預約費："," "," ","預設使用支付方式為"]
                self.infoArray.append("")
                self.infoArray.append("")
            } else {
                // 營業人
                self.titleArray = ["地址：","預約建立時間：","預約時段：","預約費：","營業人統一編號","營業人名稱","預設使用支付方式為"]
                self.infoArray.append(reservePrepareData?.taxidNum ?? "")
                self.infoArray.append(reservePrepareData?.taxidName ?? "")
            }
        } else {
            // 手機條碼
            self.titleArray = ["地址：","預約建立時間：","預約時段：","預約費：","手機條碼","預設使用支付方式為"]
            self.infoArray.append(reservePrepareData?.vehicleNum ?? "")
        }
        
        self.setupNoticeLabel()
    }
    
    private func setupNoticeLabel() {
        /* 預約成功且您完成充電，將會從您的信用卡收取一筆預約費及充電費，若預約成功，卻未至場域端完成充電，將會從您的信用卡收取一筆預約費。請盡速前往預約站進行充電，逾時請重新預約。 */
        let blackAttribute = [NSAttributedStringKey.foregroundColor:#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)]
        let cyanAttribute = [NSAttributedStringKey.foregroundColor:#colorLiteral(red: 0, green: 0.768627451, blue: 0.7960784314, alpha: 1)]
        let attrString1 = NSMutableAttributedString(string: "預約成功且您", attributes: blackAttribute)
        let attrString2 = NSMutableAttributedString(string: "完成充電", attributes: cyanAttribute)
        let attrString3 = NSMutableAttributedString(string: "，將會從您的信用卡收取一\n筆", attributes: blackAttribute)
        let attrString4 = NSMutableAttributedString(string: "預約費及充電費", attributes: cyanAttribute)
        let attrString5 = NSMutableAttributedString(string: "，若預約成功，卻未至場域端完\n成充電，將會從您的信用卡收取一筆預約費。請盡\n速前往預約站進行充電，", attributes: blackAttribute)
        let attrString6 = NSMutableAttributedString(string: "逾時請重新預約", attributes: cyanAttribute)
        let attrString7 = NSMutableAttributedString(string: "。", attributes: blackAttribute)
        
        let combinString = NSMutableAttributedString()
        combinString.append(attrString1)
        combinString.append(attrString2)
        combinString.append(attrString3)
        combinString.append(attrString4)
        combinString.append(attrString5)
        combinString.append(attrString6)
        combinString.append(attrString7)
        
        self.noticeLabel.attributedText = combinString
    }
    
    //MARK: - API
    /// 802 預約成功
    private func callAPIForRequestReserveStart() {
//        if let reserveNo = reservePrepareData?.reserveNo  {
//            KJLRequest.requestForReserveStart(cardNo: self.cardNo , reserveNo: reserveNo) { (response) in
//                    if response != nil {
//                        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReserveDetailViewController") as? ReserveDetailViewController {
//                            vc.setupDataWith(reserveNo: reserveNo , isFromReserveRecordVC: false)
//                            self.navigationController?.show(vc, sender: self)
//                        }
//                    }
//                }
//        }
    }
    /// 803 預約取消
    private func callAPIForRequestReserveCancel() {
//        if let reserveNo = reservePrepareData?.reserveNo {
//            KJLRequest.requestForReserveCancel(reserveNo: reserveNo) { (response) in
//                if response != nil {
//                    self.navigationController?.popViewController(animated: true)
//                }
//            }
//        }
    }
}

//MARK: - TableView Datasource
extension ReserveChargingViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (titleArray[indexPath.row] == "預設使用支付方式為") {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "ReservePayCell", for: indexPath)
            if let cell = cell as? ReservePayCell {
                cell.setupCellWith(titleString: titleArray[indexPath.row], cardNumString: self.cardNumString)
                return cell
            }
        } else {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "ReserveChargingCell", for: indexPath)
            if let cell = cell as? ReserveDetailCell {
                cell.setupCellWith(indexPath: indexPath, titleString: titleArray[indexPath.row], infoString: infoArray[indexPath.row])
            }
            return cell
        }
        return UITableViewCell(style: .default, reuseIdentifier: "")
    }
}

//MARK: - TableView Delegate
extension ReserveChargingViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         if (titleArray[indexPath.row] == "預設使用支付方式為") {
            guard self.cardListArray.count > 0 else {
                ShowMessageView.showMessage(title: nil, message: "請先設定付款方式")
                return
            }
            
            let control = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            for temp in self.cardListArray {
                let action = UIAlertAction(title: temp.cardName ?? "", style: .default, handler: { (alertAction) in
                    self.cardNo = temp.cardNo ?? 0
                    self.cardNumString = temp.cardNum ?? ""
                    self.tableView.reloadData()
                })
                control.addAction(action)
            }
            
            let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
            control.addAction(cancel)
            
            if control.actions.count > 1 {
                present(control, animated: true, completion: nil)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (titleArray[indexPath.row] == "預設使用支付方式為") {
            return 85
        } else {
            return 35
        }
    }
}
