//
//  ReservePayCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class ReservePayCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cardNumLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    func setupCellWith(titleString:String , cardNumString: String) {
        self.titleLabel.text = titleString
        self.cardNumLabel.text = cardNumString
    }
}
