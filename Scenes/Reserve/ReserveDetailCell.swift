//
//  ReserveDetailCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class ReserveDetailCell: UITableViewCell {

    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.titleLabel.text = ""
        self.infoLabel.text = ""
    }
    
    func setupCellWith(indexPath: IndexPath , titleString: String , infoString: String) {
        self.titleLabel.text = titleString
        self.infoLabel.text = infoString
        
        if (titleString == "預約結果：" && infoString == "預約中") {
            self.infoLabel.textColor = #colorLiteral(red: 0, green: 0.768627451, blue: 0.7960784314, alpha: 1)
        } else if (titleString == "預約結果：" && infoString != "預約中") {
            self.infoLabel.textColor = #colorLiteral(red: 0.4588235294, green: 0.4470588235, blue: 0.5215686275, alpha: 1)
        }
        
        if (titleString == "預約費：") {
            self.infoLabel.textColor = #colorLiteral(red: 0, green: 0.768627451, blue: 0.7960784314, alpha: 1)
        }
    }
}
