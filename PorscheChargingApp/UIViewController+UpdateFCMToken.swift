//
//  UIViewController+UpdateFCMToken.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/8/1.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

extension UIViewController {
    class func updateFCMToken(normalClass: NormalClass) {
        let deviceId = normalClass.datas?.deviceId ?? ""
        guard let memberNo = normalClass.datas?.memberNo
            else { return }
        self.callApiUpdateFCMToken(deviceId: deviceId, memberNo: memberNo)
    }
    
    class func updateFCMToken(appLoginClass:AppLoginClass) {
        let deviceId = appLoginClass.datas?.deviceId ?? ""
        guard let memberNo = appLoginClass.datas?.memberNo
            else { return }
        self.callApiUpdateFCMToken(deviceId: deviceId, memberNo: memberNo)
    }
    
    ///
    /// if device = "" is Register, if device != "" is update
    ///
    class func callApiUpdateFCMToken(deviceId:String,memberNo:String) {
        UserDefaults.standard.set(deviceId, forKey: "deviceId")
        UserDefaults.standard.set(memberNo, forKey: "memberNo")
        
        if let fcmToken = UserDefaults.standard.string(forKey: "fcmToken") {
            KJLRequest.requestForUpdateFcmToken(deviceId: deviceId, fcmToken: fcmToken, memberNo: memberNo) { (response) in
                guard let response = response as? NormalClass else {
                    return
                }
                
                guard let datas = response.datas else {
                    return
                }
                
                if let updateDeviceId = datas.deviceId {
                    UserDefaults.standard.set(updateDeviceId, forKey: "deviceId")
                    KJLRequest.requestForDeviceUpload(devicdId: updateDeviceId) { (response) in
                        
                    }
                    
                    UserDefaults.standard.set(true, forKey: "userLogout")
                    UserDefaults.standard.synchronize()
                }
            }
        }
    }
    
    class func pushLogOut() {
        guard let deviceId = UserDefaults.standard.string(forKey: "deviceId"),let memberNo = UserDefaults.standard.string(forKey: "memberNo") else {
            return
        }
        
        if deviceId == "" {
            return
        }
        
        KJLRequest.requestForPushLogOut(deviceId: deviceId, memberNo: memberNo) { (response) in
            guard let _ = response as? NormalClass else {
                return
            }
            
            print("移除本地端deviceId")
            UserDefaults.standard.removeObject(forKey: "deviceId")
            UserDefaults.standard.removeObject(forKey: "chargeQRno") 
            UserDefaults.standard.synchronize()
        }
    } 
}
