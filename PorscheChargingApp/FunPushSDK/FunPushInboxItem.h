//
//  FunPushInboxItem.h
//  FunPushSDK_V1
//
//  Created by Wendy Chen on 2014/11/13.
//  Copyright (c) 2014年 jamzooiha. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface FunPushInboxItem : NSObject
{
    NSString        *m_pstrAccount;
    NSString        *m_pstrId;
    NSString        *m_pstrReceiveStatus;
    NSString        *m_pstrReadStatus;
    NSString        *m_pstrDeleteStatus;
    NSString        *m_pstrType;
    NSString        *m_pstrSubject;
    NSString        *m_pstrContent;
    NSString        *m_pstrUrl;
    NSString        *m_pstrLinktype;
    NSString        *m_pstrAct;
    NSString        *m_pstrCalEid;
    NSString        *m_pstrCalStart;
    NSString        *m_pstrCalEnd;
    NSString        *m_pstrCalRepeat;
    NSString        *m_pstrCalRemind;
    NSString        *m_pstrTimestamp;
    NSString        *m_pstrCustom;
    NSString        *m_pstrEvent_action;
    NSString        *m_pstrEvent_id;
    NSMutableArray  *m_parrAxes;
}

///帳號
@property ( nonatomic, copy )   NSString        *m_pstrAccount;

///Push編號
@property ( nonatomic, copy )   NSString        *m_pstrId;

///Push收到狀態
@property ( nonatomic, copy )   NSString        *m_pstrReceiveStatus;

///Push讀取狀態
@property ( nonatomic, copy )   NSString        *m_pstrReadStatus;

///Push刪除狀態
@property ( nonatomic, copy )   NSString        *m_pstrDeleteStatus;

///Push類別(8種推播)
@property ( nonatomic, copy )   NSString        *m_pstrType;

///訊息標題
@property ( nonatomic, copy )   NSString        *m_pstrSubject;

///訊息內容
@property ( nonatomic, copy )   NSString        *m_pstrContent;

///連結網址
@property ( nonatomic, copy )   NSString        *m_pstrUrl;

///連結類型,用於識別檔案位置
@property ( nonatomic, copy )   NSString        *m_pstrLinktype;

///下載後動作
@property ( nonatomic, copy )   NSString        *m_pstrAct;

///Event Id:日曆事件代號
@property ( nonatomic, copy )   NSString        *m_pstrCalEid;

///開始時間(行事曆)
@property ( nonatomic, copy )   NSString        *m_pstrCalStart;

///結束時間(行事曆)
@property ( nonatomic, copy )   NSString        *m_pstrCalEnd;

///重複行程(行事曆)
@property ( nonatomic, copy )   NSString        *m_pstrCalRepeat;

///行程開始前幾分鐘要發出提醒(行事曆)
@property ( nonatomic, copy )   NSString        *m_pstrCalRemind;

///此訊息發送時間
@property ( nonatomic, copy )   NSString        *m_pstrTimestamp;

///客戶於發送時自行定義欄位內容
@property ( nonatomic, copy )   NSString        *m_pstrCustom;

///觸發事件推播需要完成的動作
@property ( nonatomic, copy )   NSString        *m_pstrEvent_action;

///完成動作所觸發的事件代號
@property ( nonatomic, copy )   NSString        *m_pstrEvent_id;

///座標的陣列集合
@property ( nonatomic, copy )   NSMutableArray  *m_parrAxes;

@end
