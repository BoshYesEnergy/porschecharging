//
//  FunPushBaseData.h
//  FunPushSDK_V1
//
//  Created by Wendy Chen on 2014/11/7.
//  Copyright (c) 2014年 jamzooiha. All rights reserved.
//

#import <Foundation/Foundation.h>

/* Local端Notification Key Name設定 */
#define kInboxSyncFinished              @"InobxSyncFinished"                //收件夾同步完成
#define kInboxPushStatusUpdateFinished  @"InboxPushStatusUpdateFinished"    //推播收件夾狀態更新完成
#define kDeviceLocationUpdateFinished   @"DeviceLocationUpdateFinished"     //手機裝置的位置更新完成
#define kDeleteMultiDataInInboxFinished @"DeleteMultiDataInInboxFinished"   //移除收件夾裡面的多筆資料

typedef enum
{
    FP_Push_Status_Received         = 0,
    FP_Push_Status_Read             = 1,
    FP_Push_Status_Delete           = 2,
}enumFP_Push_Status;

@interface FunPushBaseData : NSObject

///deviceToken是否已經註冊
@property BOOL m_bIsRegisterDeviceToken;
///device是否已更新完畢
@property BOOL m_bIsUpdateDeviceFinished;
///app是否已更新完畢
@property BOOL m_bIsUpdateAppFinished;

///裝置編號 DXID
@property ( nonatomic, retain ) NSString        *m_pstrTempDXID;
//@property ( nonatomic, retain ) NSString        *m_pstrAppKey;
//@property ( nonatomic, retain ) NSString        *m_pstrToken;

#pragma mark - Class methods
/**
 * @brief sharedFunPushBaseData，初始化"g_pFunPushBaseData"這個靜態變數，此變數宣告在m檔
 * @return FunPushBaseData value.
 */
+ ( FunPushBaseData * )sharedFunPushBaseData;

#pragma mark - DXID 相關
/**
 * @brief 取得暫存的的DXID(裝置編號)，若DXID存取到本地端失敗時，暫存的DXID
 * @return Return NSString value.
 */
- ( NSString * )getSavedDXID;

#pragma mark - AppKey 相關
/**
 * @brief 取得AppKey，從info.plist取得使用者自行設定的AppKey，如果值是null就會自動設定為空字串(此AppKey將會在FunPush後台的app-->開發設定-->App設定，該頁將會有顯示)
 * @return Return NSString value.
 */
- ( NSString * )getAppKey;

/**
 * @brief 取得儲存DXID(裝置編號)的檔案路徑
 * @return Return NSString value.
 */
- ( NSString * )getFunPushDxidFilePath;

#pragma mark - DeviceToken 相關

//- ( UIPasteboard * )getDeviceTokenPasteboard;

/**
 * @brief 將DeviceToken寫入剪貼簿，剪貼簿是持久化(persistent = TRUE)
 * @param pstrDeviceToken NSString value
 */
- ( void )writeDeviceTokenToPasteBoard:(NSString *) pstrDeviceToken;

/**
 * @brief 從剪貼簿讀取DeviceToken資料
 * @return Return NSString value.
 */
- ( NSString * )getSavedDeviceToken;

#pragma mark - 是否支援裝置收件匣或帳號收件匣

/**
 * @brief 是否支援裝置收件匣
 * @return Return BOOL value.
 */
- ( BOOL )getEnableDeviceInbox;

/**
 * @brief 是否支援帳號收件匣
 * @return Return BOOL value.
 */
- ( BOOL )getEnableAccountInbox;

#pragma mark - 是否支援行事曆推播

/**
 * @brief 裝置收件匣推播是否支援行事曆推播
 * @return Return BOOL value.
 */
- ( BOOL )getEnableCalendar;

/**
 * @brief 帳號收件匣推播是否支援行事曆推播
 * @return Return BOOL value.
 */
- ( BOOL )getAccountEnableCalendar;

#pragma mark - API 基本設定相關

/**
 * @brief 串接Gateway傳入參數
 * @return Return NSMutableDictionary value.
 */
- ( NSMutableDictionary * )setGatewayBaseParameter;

/**
 * @brief 帳號收件匣推播是否支援行事曆推播
 * @param pstrURL NSString value domain url
 * @param pParameter NSMutableDictionary value 參數
 */
- ( void ) printURL:(NSString *)pstrURL withParameter:(NSMutableDictionary *)pParameter;


@end
