/**
 Theme: AppleDoc
 IDE: Xcode 7.3.1
 Language: Objective C
 Date: 2012/01/10
 Author: Xavier
 */
//
//  Utilities.h
//  JamPush
//
//  Created by Xavier on 2012/01/10.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum
{
    JP_ASYNC_CONTROL_GPS = 0
}JP_ASYNC_CONTROL_TYPE;

/**這是測試Utilities
 解釋你如何使用AppleDoc😀
 */
@interface Utilities : NSObject

// Class methods

/**
 * @brief 取得裝置的機型
 * @return Return NSString value.
 */
+ ( NSString * )getDevicePlatform;

/**
 * @brief 給予網卡介面號碼(例如：en0) 跟 MAC address中間的符號( : ) 最後就會回傳MAC address
 * @param interface NSString value
 * @param delimiter NSString value
 * @return Return NSString value.
 */
+ ( NSString * )getMacAddress:( NSString * )interface withDelimiter:( NSString * )delimiter;

/**
 * @brief 檢查是否為iPad裝置
 * @return Return BOOL value.
 */
+ ( BOOL )isPad;

/**
 * @warning (Deprecated in iOS 5)
 * @brief 取得UUID
 * @param value 無
 * @return Return NSString value.
 */
+ ( NSString * )uniqueIdentifier;

/**
 * @brief 自行設定圖片大小
 * @param image UIImage value
 * @param size CGSize value
 * @return Return UIImage value.
 */
+ ( UIImage * )resizeImage:( UIImage * )image withSize:( CGSize )size;

/**
 * @brief 是否要顯示導航條上的標題
 * @param withTitle BOOL value
 * @return Return UIImage value.
 */
+ ( UIImage * )navigationBarImage:( BOOL )withTitle;

/**
 * @brief 取得Document資料夾的路徑
 * @return Return NSString value.
 */
+ ( NSString * )getDocumentPath;

/**
 * @brief 取得temp資料夾的路徑
 * @return Return NSString value.
 */
+ ( NSString * )getTempPath;

/**
 * @brief 取得目前是哪個機種 e.g. @"iPhone", @"iPod touch"
 * @return Return NSString value.
 */
+ ( NSString * )deviceModel;

/**
 * @brief 取得iOS版本
 * @return Return NSString value.
 */
+ ( NSString * )iosVersion;

/**
 * @brief 取得螢幕的寬高
 * @return Return CGSize value.
 */
+ ( CGSize )screenSize;

/**
 * @brief sha1驗證
 * @param input NSString value
 * @return Return NSString value.
 */
+ ( NSString * )sha1:( NSString * )input;

/**
 * @brief MD5驗證
 * @param input NSString value
 * @return Return NSString value.
 */
+ ( NSString * )MD5:( NSString * )input;

/**
 * @brief 取得app版號
 * @return Return NSString value.
 */
+ ( NSString * )appVersion;

/**
 * @brief 取得bundle Identifier
 * @return Return NSString value.
 */
+ ( NSString * )appID;

/**
 * @warning (這功能沒有寫完)
 * @brief xxx
 * @param pstrTimeStamp NSString value
 * @return Return NSString value.
 */
+ ( NSString * )timeStampToLocalTime:( NSString * )pstrTimeStamp;

/**
 * @brief 秒數轉成NSDate
 * @param pstrTimestamp NSString value
 * @return Return NSDate value.
 */
+ ( NSDate * )getDateFromTimestamp:( NSString * )pstrTimestamp;

/**
 * @brief 是否連線
 * @return Return BOOL value.
 */
+ ( BOOL )isConnectedToInternet;


/**
 * @brief 透過兩個點的經緯度計算距離
 * @param lat1 double value 緯度1
 * @param lon1 double value 經度1
 * @param lat2 double value 緯度2
 * @param lon2 double value 經度2
 * @return Return double value.
 */
+ ( double )approxDistance:( double )lat1
					  Lon1:( double )lon1
					  Lat2:( double )lat2
					  Lon2:( double )lon2;

#pragma mark - 避免空值
/**
 * @brief 如果是Null就把他設定成空字串
 * @param string NSString value
 * @return Return NSString value.
 */
+ (NSString *) avoidNull:(NSString *)string;

@end

@interface NSData(Encription)
/**
 * @brief 加密
 * @param key NSString value
 * @return Return NSData value.
 */
- (NSData *)AES256EncryptWithKey:(NSString *)key;

/**
 * @brief 解密
 * @param key NSString value
 * @return Return NSData value.
 */
- (NSData *)AES256DecryptWithKey:(NSString *)key;

/**
 * @brief 追加64编码
 * @return Return NSString value.
 */
- ( NSString * )newStringInBase64FromData;

/**
 * @brief 同上64编码
 * @param str NSString value
 * @return Return NSString value.
 */
+ ( NSString * )base64encode:( NSString * )str;

@end