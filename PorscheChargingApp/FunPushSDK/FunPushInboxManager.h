//
//  FunPushInboxManager.h
//  FunPushSDK_V1
//
//  Created by Wendy Chen on 2014/11/12.
//  Copyright (c) 2014年 jamzooiha. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "FunPushBaseData.h"
#import "FunPushInboxItem.h"

typedef enum
{
    FP_INBOX_ORDERBY_DATE = 0,
    FP_INBOX_ORDERBY_TITLE
}enumFP_INBOX_ORDERBY_FILTER;

typedef enum
{
    FP_PushLog_ActType_Play     = 0,
    FP_PushLog_ActType_Stop     = 1
}enumFP_PushLog_ActType;

typedef enum
{
    FP_InboxType_Device         = 0,
    FP_InboxType_Account        = 1
}enumFP_InboxType;

/** The `FunPushInboxManagerDelegate` will notify the controller about anything that happens inside `FunPushInboxManager`
 */
@protocol FunPushInboxManagerDelegate <NSObject>

@optional

/**
 * @brief 裝置收件夾更新完成
 * @param pStatusDic NSMutableDictionary value
 */
- ( void )funPushDeviceInboxUpdateFinished:( NSMutableDictionary * )pStatusDic;

/**
 * @brief 帳號收件夾更新完成
 * @param pStatusDic NSMutableDictionary value
 */
- ( void )funPushAccountInboxUpdateFinished:( NSMutableDictionary * )pStatusDic;

/**
 * @brief 裝置收件夾的詳細頁更新完成
 * @param pStatusDic NSMutableDictionary value
 */
- ( void )funPushDeviceInboxDetailFinished:( NSMutableDictionary * )pStatusDic;

/**
 * @brief 帳號收件夾的詳細頁更新完成
 * @param pStatusDic NSMutableDictionary value
 */
- ( void )funPushAccountInboxDetailFinished:( NSMutableDictionary * )pStatusDic;

/**
 * @brief 裝置收件夾的詳細頁面更新完成
 * @param pStatusDic NSMutableDictionary value 資料來自後台
 */
- ( void )funPushDeviceInboxPushUnreadFinished:( NSMutableDictionary * )pStatusDic;

/**
 * @brief 帳號收件夾
 * @param pStatusDic NSMutableDictionary value
 */
- ( void )funPushAccountInboxPushUnreadFinished:( NSMutableDictionary * )pStatusDic;

/**
 * @brief 裝置收件夾
 * @param pStatusDic NSMutableDictionary value
 */
- ( void )funPushDeviceInboxPushStatusUpdateFinished:( NSMutableDictionary * )pStatusDic;

/**
 * @brief 帳號收件夾
 * @param pStatusDic NSMutableDictionary value
 */
- ( void )funPushAccountInboxPushStatusUpdateFinished:( NSMutableDictionary * )pStatusDic;

/**
 * @brief 裝置收件夾
 * @param pStatusDic NSMutableDictionary value
 * @param eFP_InboxType enumFP_InboxType value 裝置 = 0, 帳號 = 1
 */
- ( void )funPushDeleteMultiDataInInboxFinished:( NSMutableDictionary * )pStatusDic andInboxType:( enumFP_InboxType )eFP_InboxType;

@end

@interface FunPushInboxManager : NSObject
{
    ///收件夾的路徑
    NSString        *m_pstrInboxPath;
}

///FunPushInboxManagerDelegate
@property ( nonatomic, assign ) id< FunPushInboxManagerDelegate >    delegate;

#pragma mark - Class Methods
/**
 * @brief sharedFunPushInboxManager
 */
+ ( FunPushInboxManager * )sharedFunPushInboxManager;

#pragma mark - DB相關
/**
 * @brief 建立收件匣DB
 */
- ( void )createFunPushInboxDB;

#pragma mark - DB : 裝置收件匣 - 行事曆(EvenLog)相關
/**
 * @brief 新增行事曆事件
 * @param pstrPID NSString value 推播代號
 * @param pstrEID NSString value 行事曆EventId
 * @param pstrUUID NSString value eventIdentifier
 */
- ( void )insertCalendarEventLog:( NSString * )pstrPID EID:( NSString * )pstrEID EventUUID:( NSString * )pstrUUID;

/**
 * @brief 刪除行事曆事件
 * @param pstrEID NSString value 行事曆EventId
 */
- ( void )deleteCalendarEventLog:( NSString * )pstrEID;

/**
 * @brief 取得行事曆UUID
 * @param pstrEID NSString value 行事曆EventId
 * @return NSString value
 */
- ( NSString * )getCalendarEventUUID:( NSString * )pstrEID;

#pragma mark - 呼叫FunPushInbox相關API
/**
 * @brief 同步裝置收件匣
 */
- ( void )funPushDeviceInboxSync;

/**
 * @brief 同步帳號收件匣
 * @param pstrAccount NSString value 帳號
 */
- ( void )funPushAccountInboxSync:( NSString * )pstrAccount;

/**
 * @brief 取得裝置收件匣詳細資料
 * @param pstrPushId NSString value 推播編號
 */
- ( void )funPushDeviceInboxDetail:( NSString * )pstrPushId;

/**
 * @brief 取得帳號收件匣詳細資料
 * @param pstrAccount NSString value 帳號
 * @param pstrPushId NSString value 推播編號
 */
- ( void )funPushAccountInboxDetail:( NSString * )pstrAccount andPushId:( NSString * )pstrPushId;

/**
 * @brief 同步裝置收件匣狀態
 */
- ( void )funPushDeviceInBoxStatus;

/**
 * @brief 同步帳號收件匣狀態
 * @param pstrAccount NSString value 帳號
 */
- ( void )funPushAccountInBoxStatus:( NSString * )pstrAccount;

/**
 * @brief 取得裝置收件匣未讀數
 */
- ( void )funPushDeviceInboxUnreadCount;

/**
 * @brief 取得帳號收件匣未讀數
 * @param pstrAccount NSString value 帳號
 */
- ( void )funPushAccountInboxUnreadCount:( NSString * )pstrAccount;

/**
 * @brief 設定每則推播狀態(已收/已讀/刪除)
 * @param pPushInboxItem FunPushInboxItem value 每筆資料的object
 * @param penumAP_Push_Status enumFP_Push_Status value 每筆資料的( FP_Push_Status_Received = 0, FP_Push_Status_Read = 1, FP_Push_Status_Delete = 2 )
 */
- ( void )funPushPushStatus:( FunPushInboxItem * )pPushInboxItem andStatus:( enumFP_Push_Status )penumAP_Push_Status;

/**
 * @brief 記錄Push詳細資料行為
 * @param pFunPushInboxItem FunPushInboxItem value 每筆資料的object
 * @param pstrActType enumFP_PushLog_ActType value 每筆資料的( FP_PushLog_ActType_Play = 0, FP_PushLog_ActType_Stop = 1 ) (未知)
 */
- ( void )funPushInboxContentLog:( FunPushInboxItem * )pFunPushInboxItem andActionId:( enumFP_PushLog_ActType )pstrActType;

/**
 * @brief 上傳StatusLog到Server上
 */
- ( void )funPushSyncStatusLogToServer;

#pragma mark - 帳號相關
/**
 * @brief 新增帳號
 * @param pstrAccount NSString value 帳號
 * @param pstrCustom NSString value 自定義欄位內容
 */
- ( void )funPushCreateAccount:( NSString * )pstrAccount andCustom:( NSString * )pstrCustom;

/**
 * @brief 刪除帳號
 * @param pstrAccount NSString value 帳號
 */
- ( void )funPushRemoveAccount:( NSString * )pstrAccount;

/**
 * @brief 取得所有帳號
 * @return Return NSMutableArray value
 */
- ( NSMutableArray * )funPushGetAllAcounts;

#pragma mark - 收件匣信件動作相關
/**
 * @brief 刪除推播資料，單筆刪除跟多筆刪除(多筆刪除狀態，先將刪除狀態記到本地端DB，待之後再一起同步上傳到server)
 * @param parrDeleteData NSMutableArray value 
 * @param eFP_InboxType enumFP_InboxType value 資料夾類別 ( FP_InboxType_Device = 0, FP_InboxType_Account = 1 )
 */
- ( void )funPushDeleteInboxPush:( NSMutableArray * )parrDeleteData andInboxType:( enumFP_InboxType )eFP_InboxType;

#pragma mark - 從本地端DB取得Inbox相關訊息
/**
 * @brief 依照推播Id，取得該裝置收件匣訊息詳細資料
 * @param pstrId NSString value 推播Id
 * @return FunPushInboxItem value
 */
- ( FunPushInboxItem * )getDeviceInboxItemWithId:( NSString * )pstrId;

/**
 * @brief 取得裝置收件匣所有訊息
 * @param enumOrderFilter enumFP_INBOX_ORDERBY_FILTER value 日期排序, 標題排序( FP_INBOX_ORDERBY_DATE = 0, FP_INBOX_ORDERBY_TITLE )
 * @param bASC BOOL value 開啟遞增
 * @return NSMutableArray value
 */
- ( NSMutableArray * )getDeviceInboxAllPushes:( enumFP_INBOX_ORDERBY_FILTER )enumOrderFilter ASC:( BOOL )bASC;

/**
 * @brief 取得裝置收件匣未讀訊息(取出未讀取的資料，不含刪除的資料)，從local的DB取得資料，如果沒資料就會回傳nil，此功能通常用於 "applicationDidEnterBackground" 主要是來計算AppIcon上面的badge數.
 * @param nOffset NSInteger value (未知)
 * @param nLimit NSInteger value (未知)
 * @param enumOrderFilter enumFP_INBOX_ORDERBY_FILTER value 日期排序, 標題排序( FP_INBOX_ORDERBY_DATE = 0, FP_INBOX_ORDERBY_TITLE )
 * @param bASC BOOL value
 * @return NSMutableArray value
 */
- ( NSMutableArray * )getDeviceInboxUnReadPushes:( NSInteger )nOffset Limit:( NSInteger )nLimit ORDERBY:( enumFP_INBOX_ORDERBY_FILTER )enumOrderFilter ASC:( BOOL )bASC;

/**
 * @brief 取得裝置收件匣已讀訊息(取出已讀的資料，但是不含刪除的資料)，從local的DB取得資料，如果沒資料就會回傳nil
 * @param nOffset NSInteger value (未知)
 * @param nLimit NSInteger value (未知)
 * @param enumOrderFilter enumFP_INBOX_ORDERBY_FILTER value 日期排序, 標題排序( FP_INBOX_ORDERBY_DATE = 0, FP_INBOX_ORDERBY_TITLE )
 * @param bASC BOOL value (未知)
 * @return NSMutableArray value
 */
- ( NSMutableArray * )getDeviceInboxReadPushes:( NSInteger )nOffset Limit:( NSInteger )nLimit ORDERBY:( enumFP_INBOX_ORDERBY_FILTER )enumOrderFilter ASC:( BOOL )bASC;

/**
 * @brief 取得裝置收件匣已刪除的推播，從local的DB取得資料，如果沒資料就會回傳nil
 * @param nOffset NSInteger value (未知)
 * @param nLimit NSInteger value (未知)
 * @param enumOrderFilter enumFP_INBOX_ORDERBY_FILTER value 日期排序, 標題排序( FP_INBOX_ORDERBY_DATE = 0, FP_INBOX_ORDERBY_TITLE )
 * @param bASC BOOL value (未知)
 * @return NSMutableArray value
 */
- ( NSArray * )getDeviceInboxDeletePushes:( NSInteger )nOffset Limit:( NSInteger )nLimit ORDERBY:( enumFP_INBOX_ORDERBY_FILTER )enumOrderFilter ASC:( BOOL )bASC;

#pragma mark - 從本地端DB取得AccountInbox相關訊息
/**
 * @brief 依照推播Id，取得該帳號收件匣指定訊息的詳細資料，從local的DB取得資料
 * @param pstrId NSString value 推播Id
 * @param pstrAccount NSString value 帳號
 * @return FunPushInboxItem value
 */
- ( FunPushInboxItem * )getAccountInboxItemWithId:( NSString * )pstrId andAccount:( NSString * )pstrAccount;

/**
 * @brief 取得指定帳號的收件匣所有訊息，若帳號傳空則取得所有帳號的資料，從local的DB取得資料
 * @param enumOrderFilter enumFP_INBOX_ORDERBY_FILTER value 日期排序, 標題排序( FP_INBOX_ORDERBY_DATE = 0, FP_INBOX_ORDERBY_TITLE )
 * @param bASC BOOL value (未知)
 * @param pstrAccount NSString value 帳號
 * @return NSMutableArray value
 */
- ( NSMutableArray * )getAccountInboxAllPushes:( enumFP_INBOX_ORDERBY_FILTER )enumOrderFilter ASC:( BOOL )bASC  andAccount:( NSString * )pstrAccount;

/**
 * @brief 取得指定帳號收件匣中的未讀訊息，若帳號傳空則取得所有帳號的資料，從local的DB取得資料，如果沒資料就會回傳nil
 * @param nOffset NSInteger value (未知)
 * @param nLimit NSInteger value (未知)
 * @param enumOrderFilter enumFP_INBOX_ORDERBY_FILTER value 日期排序, 標題排序( FP_INBOX_ORDERBY_DATE = 0, FP_INBOX_ORDERBY_TITLE )
 * @param bASC BOOL value (未知)
 * @param pstrAccount NSString value 帳號
 * @return NSMutableArray value
 */
- ( NSMutableArray * )getAccountInboxUnReadPushes:( NSInteger )nOffset Limit:( NSInteger )nLimit ORDERBY:( enumFP_INBOX_ORDERBY_FILTER )enumOrderFilter ASC:( BOOL )bASC andAccount:( NSString * )pstrAccount;

/**
 * @brief 取得該帳號的收件匣中已讀的訊息，若帳號傳空則取得所有帳號的資料，從local的DB取得資料
 * @param nOffset NSInteger value (未知)
 * @param nLimit NSInteger value (未知)
 * @param enumOrderFilter enumFP_INBOX_ORDERBY_FILTER value 日期排序, 標題排序( FP_INBOX_ORDERBY_DATE = 0, FP_INBOX_ORDERBY_TITLE )
 * @param bASC BOOL value (未知)
 * @param pstrAccount NSString value 帳號
 * @return NSMutableArray value
 */
- ( NSMutableArray * )getAccountInboxReadPushes:( NSInteger )nOffset Limit:( NSInteger )nLimit ORDERBY:( enumFP_INBOX_ORDERBY_FILTER )enumOrderFilter ASC:( BOOL )bASC andAccount:( NSString * )pstrAccount;

/**
 * @brief 取得該帳號的收件匣中已刪除的推播，若帳號傳空則取得所有帳號的資料，從local的DB取得資料
 * @param nOffset NSInteger value (未知)
 * @param nLimit NSInteger value (未知)
 * @param enumOrderFilter enumFP_INBOX_ORDERBY_FILTER value 日期排序, 標題排序( FP_INBOX_ORDERBY_DATE = 0, FP_INBOX_ORDERBY_TITLE )
 * @param bASC BOOL value (未知)
 * @param pstrAccount NSString value 帳號
 * @return NSArray value
 */
- ( NSArray * )getAccountInboxDeletePushes:( NSInteger )nOffset Limit:( NSInteger )nLimit ORDERBY:( enumFP_INBOX_ORDERBY_FILTER )enumOrderFilter ASC:( BOOL )bASC andAccount:( NSString * )pstrAccount;

@end
