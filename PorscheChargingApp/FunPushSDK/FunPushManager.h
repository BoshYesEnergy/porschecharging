//
//  FunPushManager.h
//  FunPushSDK_V1
//
//  Created by Wendy Chen on 2014/11/7.
//  Copyright (c) 2014年 jamzooiha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

/** The `FunPushManagerDelegate` will notify the controller about anything that happens inside `FunPushManager`
 */

static NSString *const RLKeychainUserName = @"JamDXID";
static NSString *const RLKeychainServiceName = @"Jamzoo";

@protocol FunPushManagerDelegate <NSObject>

@optional

/**
 * @brief 裝置收件夾更新完成
 * @param pStatusDic NSMutableDictionary value
 */
- ( void )funPushDeviceUpdateFinished:( NSDictionary * )pStatusDic;

/**
 * @brief App收件夾更新完成
 * @param pStatusDic NSMutableDictionary value
 */
- ( void )funPushAppUpdateFinished:( NSDictionary * )pStatusDic;

/**
 * @brief App最後上線日更新完成
 * @param pStatusDic NSMutableDictionary value
 */
- ( void )funPushAppOnlineFinished:( NSDictionary * )pStatusDic;

/**
 * @brief App國家更新完成
 * @param pStatusDic NSMutableDictionary value
 */
- ( void )funPushAppCountryFinished:( NSDictionary * )pStatusDic;

/**
 * @brief App的Badge更新完成
 * @param pStatusDic NSMutableDictionary value
 */
- ( void )funPushAppBadgeFinished:( NSDictionary * )pStatusDic;

/**
 * @brief 裝置位置更新完成
 * @param pStatusDic NSMutableDictionary value
 */
- ( void )funPushUpdateDeviceLocationFinished:( NSDictionary * )pStatusDic;

/**
 * @brief 分群名稱tag更新完成
 * @param pStatusDic NSMutableDictionary value
 */
- ( void )funPushUpdateGroupNameTagFinished:( NSMutableDictionary * )pStatusDic;

/**
 * @brief 取得分群名稱tag更新完成
 * @param pStatusDic NSMutableDictionary value
 */
- ( void )funPushGetGroupNameTagFinished:( NSMutableDictionary * )pStatusDic;

/**
 * @brief 裝置的安靜時間更新完成
 * @param pStatusDic NSMutableDictionary value
 */
- ( void )funPushUpdateQuietTimeIndexFinished:( NSMutableDictionary * )pStatusDic;

/**
 * @brief 取得裝置的安靜時間完成
 * @param pStatusDic NSMutableDictionary value
 */
- ( void )funPushGetQuietTimeIndexFinished:( NSMutableDictionary * )pStatusDic;

/**
 * @brief 檢查Token更新完成
 * @param pStatusDic NSMutableDictionary value
 */
- ( void )funPushTokenCheckerFinished:( NSMutableDictionary * )pStatusDic;

@end

@interface FunPushManager : NSObject

///FunPushManagerDelegate
@property ( nonatomic, assign ) id< FunPushManagerDelegate >    delegate;

#pragma mark - Class Methods

+ ( FunPushManager * )sharedFunPushManager;

#pragma mark - 啟動FunPush相關API
///更新裝置硬體資訊
- ( void )funPushUpdateDevice;

///更新裝置軟體資訊
- ( void )funPushUpdateApp;

///裝置最後上線日 (v2.1新增)
- ( void )funPushAppOnline;

/**
 * @brief 回傳使用者的國家、縣市 (v2.1新增)
 * @param pCurrentLocation CLLocationCoordinate2D value 一組經緯度( CLLocationDegrees latitude, CLLocationDegrees longitude; )
 */
- ( void )funpushAppCountry:( CLLocationCoordinate2D )pCurrentLocation;

/**
 * @brief 回傳BadgeNumber (v2.1新增)
 * @param unreadCount NSInteger value 未讀的總數
 */
- ( void )funpushAppBadge:( NSInteger )unreadCount;

/**
 * @brief 更新裝置位置
 * @param pCurrentLocation CLLocationCoordinate2D value 一組經緯度( CLLocationDegrees latitude, CLLocationDegrees longitude; )
 */
- ( void )funPushUpdateDeviceLocation:( CLLocationCoordinate2D )pCurrentLocation;

/**
 * @brief 更新分群名稱
 * @param parrGroupName NSArray value 分群名稱組
 */
- ( void )funPushUpdateGroupNameTag:( NSArray * )parrGroupName;

/**
 * @brief 取得分群名稱
 */
- ( void )funPushGetGroupNameTag;

/**
 * @brief 更新該裝置的安靜時間
 * @param parrUpdateQuitTime NSMutableArray value
 */
- ( void )funPushUpdateDeviceQuietTime:( NSMutableArray * )parrUpdateQuitTime;

/**
 * @brief 取得該裝置的安靜時間
 */
- ( void )funPushGetDeviceQuietTime;

/**
 * @brief 記錄App行為
 * @param pstrPageId NSString value 
 * @param pstrActionId NSString value
 */
- ( void )funPushAppLog:( NSString * )pstrPageId andActionId:( NSString * )pstrActionId;

/**
 * @brief 觸發事件推播
 * @param pstrEventId NSString value
 */
- ( void )funPushEventTrigger:( NSString * )pstrEventId;

/**
 * @brief 錯誤TOKEN移除 (v2.1新增)
 * @param errorDXID NSString value 錯誤的DXID(裝置編號)
 */
- ( void )funPushTokenChecker:( NSString * )errorDXID;

#pragma mark - 變數相關
/**
 * @brief 註冊TOKEN，每天至少更新一次APP軟體資訊給Server
 * @param pData NSData value
 */
- ( void )funPushRegisterToken:( NSData * )pData;


#pragma mark - FunPushGatewayManager Delegate
/**
 * @brief 裝置更新的callBack
 * @param pDic NSMutableDictionary value
 */
- ( void ) callBackDeviceUpdate:(NSMutableDictionary *)pDic;

/**
 * @brief App更新的callBack
 * @param pDic NSMutableDictionary value
 */
- ( void ) callBackAppUpdate:(NSMutableDictionary *)pDic;

@end
