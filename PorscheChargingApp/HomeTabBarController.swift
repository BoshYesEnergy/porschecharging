//
//  MainTabBarController.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/8/1.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

var TabBar:HomeTabBarController?

//MARK: life cycle
class HomeTabBarController: UITabBarController {
    var scanButton:UIButton!
    var chargingListArray: [ChargingListClass.ChargingList] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        KJLRequest.gotToken()
        self.setupScanButton()
        self.setupViews()
        tabBar.barTintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        tabBar.unselectedItemTintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        KJLRequest.shareInstance().kJLRequestDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) 
        changeStatus()
        NotificationCenter.default.addObserver(self, selector: #selector(changeStatus), name: .checkChargingStatus, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(changeScanTabImage), name: .changeScanTabImage, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(callCharging), name: .gotoCharging, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(gotoProfile), name: .popToProfile, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
}


//MARK: setup
extension HomeTabBarController {
    func setupScanButton() {
        var width = (UIScreen.main.bounds.width / 5) * 1.1
        width = (width > 90) ? 90 : width
        let cornerRadius = (width / 2)
        let scanButton = UIButton.init(frame: CGRect.init(x: 0, y: -cornerRadius + 20, width: width, height: width))
        scanButton.layer.cornerRadius = cornerRadius
        scanButton.setImage(UIImage(named: TabImage.green_scan_normal), for: .normal)
        scanButton.imageView?.contentMode = .scaleToFill
        scanButton.center.x = self.tabBar.center.x
        scanButton.backgroundColor = UIColor.clear
        scanButton.addTarget(self, action: #selector(scanAction), for: .touchUpInside)
        self.scanButton = scanButton
        //        self.tabBar.addSubview(scanButton)
    }
    
    func setupViews() {
        /// tab bar background color
        //        let numberOftabs = CGFloat((tabBar.items?.count)!)
        //        let tabSize = CGSize(width: tabBar.frame.width / numberOftabs, height: tabBar.frame.height)
        //        self.tabBar.selectionIndicatorImage = UIImage.imageWithColor(color: #colorLiteral(red: 0.1467642486, green: 0.735709846, blue: 0.7560048699, alpha: 1), size: tabSize)
        self.tabBar.tintColor = #colorLiteral(red: 0.8352941176, green: 0, blue: 0.1098039216, alpha: 1)
        
        self.delegate = self
        TabBar = self
        /// Map
        let mainViewController:UIViewController = self.viewControllers![0]
        mainViewController.tabBarItem = UITabBarItem.init(title: LocalizeUtils.localized(key: Language.Map.rawValue), image: UIImage(named: TabImage.home_Map)!.withRenderingMode(.alwaysOriginal), selectedImage: UIImage(named: TabImage.home_Map_hover)!.withRenderingMode(.alwaysOriginal))
        mainViewController.tabBarItem.setTitleTextAttributes([.font : UIFont.systemFont(ofSize: 14)], for: .normal)
        /// Route Planing
//        let chargingListViewController:UIViewController = self.viewControllers![1]
//        chargingListViewController.tabBarItem = UITabBarItem.init(title: LocalizeUtils.localized(key: Language.Route.rawValue), image: UIImage(named: TabImage.charging_list)!.withRenderingMode(.alwaysOriginal), selectedImage: UIImage(named: TabImage.charging_list_hover)!.withRenderingMode(.alwaysOriginal))
//        chargingListViewController.tabBarItem.setTitleTextAttributes([.font : UIFont.systemFont(ofSize: 14)], for: .normal)
        /// Scan
        let viewController:UIViewController = self.viewControllers![1]
        viewController.tabBarItem = UITabBarItem.init(title: LocalizeUtils.localized(key: Language.Scan.rawValue), image: UIImage(named: TabImage.green_scan_normal)!.withRenderingMode(.alwaysOriginal), selectedImage: UIImage(named: TabImage.green_scan_press)!.withRenderingMode(.alwaysOriginal))
        viewController.tabBarItem.tag = 3
        viewController.tabBarItem.setTitleTextAttributes([.font : UIFont.systemFont(ofSize: 14)], for: .normal)
        /// Profile
        let paymentSelectSettingViewController:UIViewController = self.viewControllers![2]
        paymentSelectSettingViewController.tabBarItem = UITabBarItem.init(title: LocalizeUtils.localized(key: Language.Profile.rawValue), image: UIImage(named: TabImage.profile)!.withRenderingMode(.alwaysOriginal), selectedImage: UIImage(named: TabImage.profile_hover)!.withRenderingMode(.alwaysOriginal))
        paymentSelectSettingViewController.tabBarItem.setTitleTextAttributes([.font : UIFont.systemFont(ofSize: 14)], for: .normal)
    }
}


//MARK:- TabBar Controller Delegate
extension HomeTabBarController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if let vcs = tabBarController.viewControllers { /// 關掉跳頁功能
            if viewController == vcs[2] {
                gotoProfile()
                return false
            } else if viewController == vcs[1] {
                scanAction()
                return false
//            } else if viewController == vcs[1] {
//                routePlaning()
//                return false
            } else if viewController == vcs[0] {
                NotificationCenter.default.post(name: .mapAllUpdate, object: nil) 
                return false
            } else {
                return false
            }
        }
        return true
    }
}


//MARK:- Action
extension HomeTabBarController {
    @objc func gotoProfile() {
        guard KJLRequest.isLogin() == true else {
            gotoLogin()
            return
        }
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController")
        let nav:UINavigationController = UINavigationController.init(rootViewController: vc)
        nav.isNavigationBarHidden = true
        nav.modalPresentationStyle = .overFullScreen
        nav.modalTransitionStyle   = .crossDissolve
        self.present(nav, animated: true, completion: nil)
    }
    
    //掃qrcode
    @objc func scanAction() {
        guard KJLRequest.isLogin() == true else {
            gotoLogin()
            return }
        
        callProfile {
            let profile = KJLRequest.shareInstance().profile?.datas
            KJLRequest.requestForChargingData { (response) in
                guard let response = response as? ChargingDataModel,
                      let data = response.datas
                else {
                    NotificationCenter.default.post(name: .hiddenChargingStatusAlert, object: nil)
                    NotificationCenter.default.post(name: .sendChargingTranNo, object: self, userInfo: ["tranNo" : 0])
                    
                    if profile?.subscription == true
                        || profile?.partnerMails?.count ?? 0 >= 1
                        || profile?.porscheMan == true {
                        let vc = ScanQRcodeViewController()
                        vc.status      = .Scan
                        vc.content     = LocalizeUtils.localized(key: "ScanCharging")
                        self.present(vc, animated: true, completion: nil)
                    } else {
                        if profile?.voucher == true {
                            if profile?.cardNum ?? "" != "" {
                                let vc = ScanQRcodeViewController()
                                vc.status  = .Scan
                                vc.content = LocalizeUtils.localized(key: "ScanCharging")
                                self.present(vc, animated: true, completion: nil)
                            } else {
                                self.popErrorMessage(reMsg: LocalizeUtils.localized(key: "popMessageToPayment"))
                            }
                        } else {
                            ///Scan partner
                            let vc     = ScanQRcodeViewController()
                            vc.status  = .ScanPartner
                            vc.content = LocalizeUtils.localized(key: "ScanPartner")
                            self.present(vc, animated: true, completion: nil)
                        }
                    }
                    return }
                
                let charging = data.charging ?? false
                switch charging {
                case true:
                    let tranNo = data.tranNo ?? 0
                    UserDefaults.standard.set(tranNo, forKey: "chargeTranNo")
                    NotificationCenter.default.post(name: .sendChargingTranNo, object: self, userInfo: ["tranNo" : tranNo ])
                    NotificationCenter.default.post(name: .showChargingStatusAlert, object: nil)
                    
                case false:
                    NotificationCenter.default.post(name: .hiddenChargingStatusAlert, object: nil)
                    NotificationCenter.default.post(name: .sendChargingTranNo, object: self, userInfo: ["tranNo" : 0])
                    
                    if profile?.subscription == true
                        || profile?.partnerMails?.count ?? 0 >= 1
                        || profile?.porscheMan == true {
                        let vc = ScanQRcodeViewController()
                        vc.status      = .Scan
                        vc.content     = LocalizeUtils.localized(key: "ScanCharging")
                        self.present(vc, animated: true, completion: nil)
                    } else {
                        if profile?.voucher == true {
                            if profile?.cardNum ?? "" != "" {
                                let vc = ScanQRcodeViewController()
                                vc.status  = .Scan
                                vc.content = LocalizeUtils.localized(key: "ScanCharging")
                                self.present(vc, animated: true, completion: nil)
                            } else {
                                self.popErrorMessage(reMsg: LocalizeUtils.localized(key: "popMessageToPayment"))
                            }
                        } else {
                            ///Scan partner
                            let vc     = ScanQRcodeViewController()
                            vc.status  = .ScanPartner
                            vc.content = LocalizeUtils.localized(key: "ScanPartner")
                            self.present(vc, animated: true, completion: nil)
                        }
                    }
                }
            }
            
        }
    }
    
    func routePlaning() {
        guard KJLRequest.isLogin() == true else {
            gotoLogin()
            return }
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RoutePlaningViewController") as! RoutePlaningViewController
        let nav = UINavigationController(rootViewController: vc)
        nav.modalPresentationStyle = .overFullScreen
        present(nav, animated: true, completion: nil)
    }
    
    @objc func changeStatus() {
        if KJLRequest.isLogin() == false {
            return
        }
        KJLRequest.requestForChargingData { (response) in
            guard let response = response as? ChargingDataModel,
                  let data = response.datas
            else {
                NotificationCenter.default.post(name: .hiddenChargingStatusAlert, object: nil)
                NotificationCenter.default.post(name: .sendChargingTranNo, object: self, userInfo: ["tranNo" : 0])
                return }
            let charging = data.charging ?? false
            switch charging {
            case true:
                let tranNo = data.tranNo ?? 0
                UserDefaults.standard.set(tranNo, forKey: "chargeTranNo")
                NotificationCenter.default.post(name: .sendChargingTranNo, object: self, userInfo: ["tranNo" : tranNo])
                NotificationCenter.default.post(name: .showChargingStatusAlert, object: nil)
                break
            case false:
                NotificationCenter.default.post(name: .hiddenChargingStatusAlert, object: nil)
                NotificationCenter.default.post(name: .sendChargingTranNo, object: self, userInfo: ["tranNo" : 0])
            }
        }
    }
    
    @objc func callReserveCheck() {
        KJLRequest.requestForReserveCheck { (response) in
            guard let response = response as? ReserveCheckModel else { return }
            if response.resCode == "0803" {
                let vc = BookingStationDeatilVC(nibName: "BookingStationDeatilVC", bundle: nil)
                vc.stationName  = response.datas?.stationName ?? ""
                vc.parkingSpace = response.datas?.parkingSpace ?? 0
                vc.endTime      = response.datas?.reserveEndTime ?? ""
                UserDefaults.standard.set(response.datas?.parkingSpace ?? 0, forKey: "BookingParkingSpace")
                let nav = UINavigationController(rootViewController: vc)
                nav.modalPresentationStyle = .overFullScreen
                nav.isNavigationBarHidden  = true
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    @objc func changeScanTabImage() {
        
    }
    
    fileprivate func chargingData(_ response: ChargingListClass) {
        let trans = response.datas?.trans ?? []
        let viewController:UIViewController = self.viewControllers![2]
        if trans.count > 0 { /// 欠款
            
        } else { /// 無欠款
            if response.datas?.charging == true { //使用者充電中:掃描btn顯示
                
            } else {
                
            }
        }
    }
}


//MARK: Api
extension HomeTabBarController {
    func callProfile(completion: @escaping ()-> Void ) {
        KJLRequest.requestForProfile { (response) in
            guard let data = response as? ProfileClass else { return }
            KJLRequest.shareInstance().profile = data
            completion()
        }
    }
    
    fileprivate func gotoLogin() {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SelectLoginViewController") as! SelectLoginViewController
        let nav:UINavigationController = UINavigationController.init(rootViewController: vc)
        nav.isNavigationBarHidden = true
        nav.modalPresentationStyle = .overFullScreen
        nav.modalTransitionStyle   = .crossDissolve
        self.present(nav, animated: true, completion: nil)
    }
    
    @objc func callCharging() {
        let qrcode = UserDefaults.standard.string(forKey: "chargeQRno") ?? ""
        gotoCharging(qrcode)
    }
    
    fileprivate func gotoCharging(_ qrNo: String) {
        let vc = ReadyChargingVC(nibName: "ReadyChargingVC", bundle: nil)
        vc.hiddenNavigationBar(isHidden: true)
        vc.chargingId = qrNo
        let nav = UINavigationController(rootViewController: vc)
        nav.modalPresentationStyle = .overFullScreen
        nav.navigationBar.isHidden = true
        self.present(nav, animated: true, completion: nil)
    }
}


//MARK: Protocol
extension HomeTabBarController: KJLRequestDelegate {
    func apiFailResultWith(resCode: String, message: String) {
        present(.confirmAlert("", message, "OK", {
            
        }, "", { }), animated: true, completion: nil)
    }
}


//MARK: Message Delegate
extension HomeTabBarController: ShowMessageAlertDelegate {
    func popErrorMessage(reMsg: String) {
        let vc = ShowMessageAlertVC()
        vc.stringToContent = reMsg
        vc.titleToBtn = LocalizeUtils.localized(key: Language.Confirm.rawValue)
        vc.delegate = self
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        present(vc, animated: true) { }
    }
    
    func messageTapAction() {
        self.dismiss(animated: true, completion: nil)
    }
}
