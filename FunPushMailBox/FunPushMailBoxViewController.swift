//
//  FunPushMailBoxViewController.swift
//  NissanCar
//
//  Created by Una Lee on 2019/5/2.
//  Copyright © 2019 s5346. All rights reserved.
//

import UIKit

class FunPushMailBoxViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let manager = CLLocationManager()
    var selectMode = String()
    var intoType = String()
    var dataArray = [FunPushInboxItem]()
    var selectInfo = [String: Bool]()
    var selectStatus = Bool()
    var m_parrDeleteData = [FunPushInboxItem]()
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        manager.requestWhenInUseAuthorization()
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.delegate = self
        selectMode = UserDefaults.standard.object(forKey: "SelectMode") as! String
        intoType = UserDefaults.standard.object(forKey: "GoFavortyListPageType") as! String
        
        
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }

}
