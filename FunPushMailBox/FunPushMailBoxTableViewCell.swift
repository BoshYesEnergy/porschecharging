//
//  FunPushMailBoxTableViewCell.swift
//  NissanCar
//
//  Created by Una Lee on 2019/5/2.
//  Copyright © 2019 s5346. All rights reserved.
//

import UIKit

class FunPushMailBoxTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var messageDate: UILabel!
    @IBOutlet weak var messageInfo: UILabel!
    @IBOutlet weak var messageTitle: UILabel!
    @IBOutlet weak var selectStatusImage: UIImageView!
    @IBOutlet weak var unReadImage: UIImageView!
    @IBOutlet weak var unReadImageConstraint: NSLayoutConstraint!
    @IBOutlet weak var messageLabelConstraint: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
