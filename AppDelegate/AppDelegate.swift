//
//  AppDelegate.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//APP-Key: d82787e8-0037-4c14-8166-a81624896278

import UIKit
import GoogleSignIn
import FacebookCore
import UserNotifications 
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import Crashlytics
import Fabric

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var allowNotification = Bool()
    var parrFunPushDeviceInboxItems = NSMutableArray()                 //裝置收件匣訊息陣列
    var iUnreadDevicePushCount = 0                                     //裝置未讀數
    var m_bSkipFirstActive = Bool()
    var isFromWillPresent = Bool()
    var kPushInfo = NSDictionary()
    var paths: String = ""
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        checkApiServer()
        #warning("暫時強制登出")
        //強制登出 下一版要拿掉
        if UserDefaults.standard.string(forKey: "userLogout") == nil {
            KJLRequest.logout()
        } 
        
        // Language
        LocalizeUtils.shared.settingUserLanguageCode()
        // 檢查推播權限
        setupRemoteNotification()
        // keyboard
        configureKeyboard()
        // Google Map
        GMSServices.provideAPIKey(KJLRequest.googleKey)
        // Google Place
        GMSPlacesClient.provideAPIKey(KJLRequest.googleKey) 
        // facebook
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        // Initialize sign-in
        GIDSignIn.sharedInstance().clientID = "503522067777-d5ust72eilarp7vpiisqo8587sptdqhk.apps.googleusercontent.com"  
        if KJLRequest.isLogin() == false  {
            UIApplication.shared.applicationIconBadgeNumber = 0
        }
        UserDefaults.standard.set( "", forKey: "shareQRcode")
        UserDefaults.standard.setValue("", forKey: "chargeQRno")
        
        print("language: \(KJLRequest.shareInstance().language ?? "")")
        print("PHONE: \(UIDevice.current.type)")
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) { }
    
    func applicationDidEnterBackground(_ application: UIApplication) { }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                self.allowNotification = true;
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
        if KJLRequest.isLogin() == false  {
            UIApplication.shared.applicationIconBadgeNumber = 0
        }
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        AppEvents.activateApp()
        NotificationCenter.default.post(name: .mainReloadMarkerList, object: nil)
        NotificationCenter.default.post(name: .chargingStatus, object: nil)
        NotificationCenter.default.post(name: .startChargingAnimate, object: nil)
        NotificationCenter.default.post(name: .carChargingStatus, object: nil)
        NotificationCenter.default.post(name: .mainVersionCheck, object: nil)
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        print("url.absoluteURL.absoluteString = \(url.absoluteURL.absoluteString)")
        /// Porsche login
        if url.absoluteURL.absoluteString.lowercased().contains("porscheapp") {
            let toString = url.absoluteString
            let stringArray = toString.components(separatedBy: "code=")
            UserDefaults.standard.set(stringArray[1], forKey: "porscheappIDcode")
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                NotificationCenter.default.post(name: .porscheLogin, object: nil)
            } 
            print(stringArray[1])
        }
        
        if (url.scheme == "yescharge") {
            if(url.absoluteURL.absoluteString.contains("LinePay?status=true")){
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    NotificationCenter.default.post(name: .linePayUpdate, object: nil, userInfo: nil)
                }
            } else if (url.absoluteURL.absoluteString.contains("EasyPay")){
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    NotificationCenter.default.post(name: .easyPayUpdate, object: nil, userInfo: nil)
                }
            } else {
                let urlString = url.absoluteString
                if (urlString.hasSuffix("success")) {
                    ShowMessageView.showMessage(title: nil, message: "驗證成功")
                } else if (urlString.hasSuffix("fail")){
                    ShowMessageView.showMessage(title: nil, message: "驗證失敗")
                }
            }
            return true
        } else {
            let appId = Settings.appID!
            if url.scheme != nil && url.scheme!.hasPrefix("fb\(appId)") && url.host ==  "authorize" { // facebook
                return ApplicationDelegate.shared.application(app, open: url, options: options)
            } else {
                return GIDSignIn.sharedInstance()?.handle(url) != nil 
            }
        }
    }
    
    //背景收到推播後點擊進入APP
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        isFromWillPresent = false
        let userInfo = response.notification.request.content.userInfo
        kPushInfo = userInfo as NSDictionary
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        isFromWillPresent = true
        let userInfo = notification.request.content.userInfo
        kPushInfo = userInfo as NSDictionary
    }
}

extension AppDelegate { 
    @available(iOS 8.0, *)
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        let handled = DynamicLinks.dynamicLinks().handleUniversalLink(userActivity.webpageURL!) { (dynamiclink, error) in
            if error != nil { return }
            if let url = dynamiclink?.url {
                if (url.absoluteString.contains("qrcode")) {
                    self.openScanAction()
                }
            } else {
                if userActivity.webpageURL!.absoluteString == "https://yescharge.page.link/home" {
                    self.openScanAction()
                }
            }
        }
        return handled
    }
    
    func openScanAction() {
        self.removeShowMessageView()
        if TabBar != nil {
            let viewController = TabBar!
            if viewController.presentingViewController != nil {
                viewController.presentingViewController?.dismiss(animated: false, completion: nil)
            }
            TabBar?.scanAction()
        }
    }
    
    func removeShowMessageView() {
        if UIApplication.shared.keyWindow != nil {
            for subview in UIApplication.shared.keyWindow!.subviews {
                if subview is ShowMessageView {
                    let view:ShowMessageView = subview as! ShowMessageView
                    view.removeView()
                }
            }
        }
    }
}


//MARK: MessagingDelegate
extension AppDelegate: MessagingDelegate {
    /// 註冊 取得 TOKEN
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("deviceTokendeviceTokendeviceToken")
        let token: String = deviceToken.map{ String(format: "%02.2hhx", arguments: [$0]) }.joined(separator: "")
        print(token)
        Messaging.messaging().apnsToken = deviceToken
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        m_bSkipFirstActive = false
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        guard let userInfo = userInfo as? [String : Any] else { return }
        print("userInfo: \(userInfo)")
        let state = application.applicationState
        if state == .active || state == .inactive {
            guard let badges = userInfo["iOSBadge"] as? Int else { return }
            UIApplication.shared.applicationIconBadgeNumber = badges
            UserDefaults.standard.set(badges, forKey: "UnReadCount")
        }
    }
    
    func setupRemoteNotification() { // 設定 推播
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: {_, _ in })
        DispatchQueue.main.async {
            UIApplication.shared.registerForRemoteNotifications()
            let bundle = Bundle.main
            if Bundle.main.bundleIdentifier!.contains(".dev") { 
                self.paths = "GoogleServiceDev-Info"
            } else {
                self.paths = "GoogleService-Info"
            }
            guard
                let path = bundle.path(forResource: self.paths, ofType: "plist"),
                let fireOptione = FirebaseOptions(contentsOfFile: path)
                else { return }
            FirebaseApp.configure(options: fireOptione)
            Messaging.messaging().isAutoInitEnabled = true 
            Messaging.messaging().delegate = self
            Fabric.with([Crashlytics.self])
        }
    }
    
    // 收到 fcmToken
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        //更新 token
        let pasteBoard = UIPasteboard.general
        pasteBoard.string = fcmToken
        print("fcmTokenfcmToken: \(fcmToken)")
        UserDefaults.standard.set(fcmToken, forKey: "fcmToken")
        UserDefaults.standard.synchronize()
        guard
            let deviceId = UserDefaults.standard.string(forKey: "deviceId"),
            let memberNo = UserDefaults.standard.string(forKey: "memberNo")
            else { return }
        if deviceId == "" { return }
        KJLRequest.requestForUpdateFcmToken(deviceId: deviceId, fcmToken: fcmToken, memberNo: memberNo) { (response) in
            guard
                let response = response as? NormalClass,
                let datas = response.datas
                else { return }
            if let updateDeviceId = datas.deviceId {
                KJLRequest.requestForDeviceUpload(devicdId: updateDeviceId) { (response) in }
                UserDefaults.standard.set(updateDeviceId, forKey: "deviceId")
                UserDefaults.standard.synchronize()
            }
        }
    } 
}
