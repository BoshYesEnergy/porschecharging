//
//  AppDelegate + Extension.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/5/18.
//  Copyright © 2020 s5346. All rights reserved.
//

import Foundation
import IQKeyboardManagerSwift

extension AppDelegate {
    #warning("檢查domain")
    internal func checkApiServer() { 
        if Bundle.main.bundleIdentifier!.contains(".dev") { /// test sever
            KJLRequest.domain = "http://18.182.99.126:8089/api/"
            KJLRequest.pushDomain = "http://18.182.99.126:8079/api/"
            KJLRequest.googleKey = "AIzaSyA1DzPupNDnFBWu0fNLaFJJr3Cy86LdoW8"
        } else {
            KJLRequest.domain = "https://porscheapi.yes-energy.com.tw/api/"
            KJLRequest.pushDomain = "https://appserver.yulon-energy.com:8079/api/"
            KJLRequest.googleKey = "AIzaSyA1DzPupNDnFBWu0fNLaFJJr3Cy86LdoW8"
        }
    }
    
    // 採用IQKeyboardManager
    // 詳細設定請參閱： https://github.com/hackiftekhar/IQKeyboardManager
    internal func configureKeyboard() {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
    }
}
