//
//  MaskDetailModel.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/3/20.
//  Copyright © 2020 s5346. All rights reserved.
//

import Foundation

struct MaskDetailModel: Codable {
    let datas : [Data]?
    let resCode : String?
    let resMsg : String?
     
    enum CodingKeys: String, CodingKey {
        case datas = "datas"
        case resCode = "resCode"
        case resMsg = "resMsg"
    }
    
    struct Data: Codable {
        let pharmacyPhone: String?
        let kidMaskCounts: String?
        let pharmacyAddress: String?
        let adultMaskCounts: String?
        let pharmacyNo: String?
        let latitude: String?
        let pharmacyName: String?
        let longitude: String?
        let update_date: String?
        
        enum CodingKeys: String, CodingKey {
            case pharmacyPhone = "PharmacyPhone"
            case kidMaskCounts = "KidMaskCounts"
            case pharmacyAddress = "PharmacyAddress"
            case adultMaskCounts = "AdultMaskCounts"
            case pharmacyNo = "PharmacyNo"
            case latitude = "Latitude"
            case pharmacyName = "PharmacyName"
            case longitude = "Longitude"
            case update_date = "update_date"
        } 
    }
}
