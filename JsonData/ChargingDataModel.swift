//
//  ChargingDataModel.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/22.
//  Copyright © 2020 s5346. All rights reserved.
//

import Foundation

struct ChargingDataModel: Codable {
    let datas            : Data?
    let resCode          : String?
    let resMsg           : String?
    
    struct Data: Codable {
        let tranNo          : Int?
        let stationName     : String?
        let chargespotId    : String?
        let startTime       : String?
        let charging        : Bool?
        let process         : String? // start = ReadyChargingVC , charging = StartChargingVC
    }
}
