//
//  ChargeStatisticsClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import Foundation

struct ChargeStatisticsClass : Codable {
    
    let datas : Data?
    let resCode : String?
    let resMsg : String?
    
    
    enum CodingKeys: String, CodingKey {
        case datas
        case resCode = "resCode"
        case resMsg = "resMsg"
    }
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        datas = try values.decodeIfPresent(Data.self, forKey: .datas)
//        resCode = try values.decodeIfPresent(String.self, forKey: .resCode)
//        resMsg = try values.decodeIfPresent(String.self, forKey: .resMsg)
//    }
    
    struct Data : Codable {
        
        let picUrl : String?
        let totalAmount : Int64?
        let totalCount : Int?
        let totalKwh : String?
        let totalTime : Int64?
        
        
        enum CodingKeys: String, CodingKey {
            case picUrl = "picUrl"
            case totalAmount = "totalAmount"
            case totalCount = "totalCount"
            case totalKwh = "totalKwh"
            case totalTime = "totalTime"
        }
//        init(from decoder: Decoder) throws {
//            let values = try decoder.container(keyedBy: CodingKeys.self)
//            picUrl = try values.decodeIfPresent(String.self, forKey: .picUrl)
//            totalAmount = try values.decodeIfPresent(Int64.self, forKey: .totalAmount)
//            totalCount = try values.decodeIfPresent(Int.self, forKey: .totalCount)
//            totalKwh = try values.decodeIfPresent(String.self, forKey: .totalKwh)
//            totalTime = try values.decodeIfPresent(Int64.self, forKey: .totalTime)
//        }
        
        
    }
}
