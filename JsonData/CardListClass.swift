//
//  CardListClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import Foundation

struct CardListClass : Codable {
    
    let datas : [Data]?
    let resCode : String?
    let resMsg : String?
    
    
//    enum CodingKeys: String, CodingKey {
//        case datas = "datas"
//        case resCode = "resCode"
//        case resMsg = "resMsg"
//    }
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        datas = try values.decodeIfPresent([Data].self, forKey: .datas)
//        resCode = try values.decodeIfPresent(String.self, forKey: .resCode)
//        resMsg = try values.decodeIfPresent(String.self, forKey: .resMsg)
//    }
    
    struct Data : Codable {
        
        let addedDate : String?
        let cardName : String?
        let cardNo : Int?
        let cardNum : String?
        let cardType : String?
        var `default` : Bool?
        /// 卡片過期
        var expire : Bool?
        var expireSoon : Bool?
        var expiry : String?
        var linepayError : Bool?
         
//        enum CodingKeys: String, CodingKey {
//            case addedDate = "addedDate"
//            case cardName = "cardName"
//            case cardNo = "cardNo"
//            case cardNum = "cardNum"
//            case cardType = "cardType"
//            case defaultField = "default"
//            /// 卡片過期
//            case expire = "expire"
//            case expireSoon = "expireSoon"
//            case expiry = "expiry"
//            case linepayError = "linepayError"
//        }
//        init(from decoder: Decoder) throws {
//            let values = try decoder.container(keyedBy: CodingKeys.self)
//            addedDate = try values.decodeIfPresent(String.self, forKey: .addedDate)
//            cardName = try values.decodeIfPresent(String.self, forKey: .cardName)
//            cardNo = try values.decodeIfPresent(Int.self, forKey: .cardNo)
//            cardNum = try values.decodeIfPresent(String.self, forKey: .cardNum)
//            cardType = try values.decodeIfPresent(String.self, forKey: .cardType)
//            defaultField = try values.decodeIfPresent(Bool.self, forKey: .defaultField)
//            /// 卡片過期
//            expire = try values.decode(Bool.self, forKey: .expire)
//            expireSoon = try values.decode(Bool.self, forKey: .expireSoon)
//            expiry = try values.decode(String.self, forKey: .expiry)
//            linepayError = try values.decode(Bool.self, forKey: .linepayError)
//        }
        
        
    }
}
