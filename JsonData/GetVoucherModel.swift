//
//  GetVoucherModel.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/22.
//  Copyright © 2020 s5346. All rights reserved.
//

import Foundation

struct GetVoucherModel: Codable {
    let datas            : Data?
    let resCode          : String?
    let resMsg           : String?
    
    struct Data: Codable {
        var companyName     : String?
        var issueStime      : String?
        var issueEtime      : String?
        var useTime         : String?
        var voucherStatus   : String?
        var unlimit         : Bool?
    }
}
