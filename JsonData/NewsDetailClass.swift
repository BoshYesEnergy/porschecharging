//
//  NewsDetailClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import Foundation

struct NewsDetailClass : Codable {
    
    let datas : Data?
    let resCode : String?
    let resMsg : String?
    
    
    enum CodingKeys: String, CodingKey {
        case datas = "datas"
        case resCode = "resCode"
        case resMsg = "resMsg"
    }
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        datas = try values.decodeIfPresent(Data.self, forKey: .datas)
//        resCode = try values.decodeIfPresent(String.self, forKey: .resCode)
//        resMsg = try values.decodeIfPresent(String.self, forKey: .resMsg)
//    }
    
    struct Data : Codable {
        
        let newsNo : Int?
        let outline : String?
        let picUrl : String?
        let text : String?
        let title : String?
        let datetime: String?
        
        enum CodingKeys: String, CodingKey {
            case newsNo = "newsNo"
            case outline = "outline"
            case picUrl = "picUrl"
            case text = "text"
            case title = "title"
            case datetime = "datetime"
        }
//        init(from decoder: Decoder) throws {
//            let values = try decoder.container(keyedBy: CodingKeys.self)
//            newsNo = try values.decodeIfPresent(Int.self, forKey: .newsNo)
//            outline = try values.decodeIfPresent(String.self, forKey: .outline)
//            picUrl = try values.decodeIfPresent(String.self, forKey: .picUrl)
//            text = try values.decodeIfPresent(String.self, forKey: .text)
//            title = try values.decodeIfPresent(String.self, forKey: .title)
//            datetime = try values.decodeIfPresent(String.self, forKey: .datetime)
//        }
        
        
    }
}
