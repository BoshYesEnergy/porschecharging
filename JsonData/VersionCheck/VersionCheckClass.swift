//
//  VersionCheck.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import Foundation

struct VersionCheckClass : Codable {
    
    let datas : DataClass?
    let resCode : String?
    let resMsg : String?
    
    
    enum CodingKeys: String, CodingKey {
        case datas = "datas"
        case resCode = "resCode"
        case resMsg = "resMsg"
    }
    
    struct DataClass : Codable {
        
        let appUrl : String?
        let newVersion : Bool?
        let required : Bool?
        
        
        enum CodingKeys: String, CodingKey {
            case appUrl = "appUrl"
            case newVersion = "newVersion"
            case required = "required"
        }  
    }
}


