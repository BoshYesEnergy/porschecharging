//
//  SystemVerbClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import Foundation

struct SystemVerbClass : Codable {
        
        let datas : Data?
        let resCode : String?
        let resMsg : String?
        
        
        enum CodingKeys: String, CodingKey {
            case datas
            case resCode = "resCode"
            case resMsg = "resMsg"
        }
        struct Data : Codable {
            
            let locData : LocData?
            let typeData : TypeData?
            
            
            enum CodingKeys: String, CodingKey {
                case locData
                case typeData
            }
        }
        
        struct TypeData : Codable {
            
            let typeList : [TypeList]?
            let typeListVer : String?
            
            
            enum CodingKeys: String, CodingKey {
                case typeList = "typeList"
                case typeListVer = "typeListVer"
            }
        }
        
        struct TypeList : Codable {
            
            let carName : String?
            let carNo : Int?
            
            
            enum CodingKeys: String, CodingKey {
                case carName = "carName"
                case carNo = "carNo"
            }
            
        }
        
        struct LocData : Codable {
            
            let locList : [LocList]?
            let locVer : String?
            
            
            enum CodingKeys: String, CodingKey {
                case locList = "locList"
                case locVer = "locVer"
            }
        }
        
        struct LocList : Codable {
            
            let secDatas : [SecData]?
            let secName : String?
            
            
            enum CodingKeys: String, CodingKey {
                case secDatas = "secDatas"
                case secName = "secName"
            }
        }
        
        struct SecData : Codable {
            
            let locName : String?
            
            
            enum CodingKeys: String, CodingKey {
                case locName = "locName"
            } 
        }
}
