//
//  ChargerPairClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import Foundation

struct ChargerPairClass : Codable {
    
    let datas : Data?
    let resCode : String?
    let resMsg : String?
    
//    enum CodingKeys: String, CodingKey {
//        case datas
//        case resCode = "resCode"
//        case resMsg = "resMsg"
//    }
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        datas = try values.decodeIfPresent(Data.self, forKey: .datas)
//        resCode = try values.decodeIfPresent(String.self, forKey: .resCode)
//        resMsg = try values.decodeIfPresent(String.self, forKey: .resMsg)
//    }
    
    struct Data : Codable {
        
        let cardList : [CardList]?
        let chargingFee : String?
        let current : String?
        let kwh : String?
        let powerType : String?
        let qrNo : String?
        let scoketType : String?
        let stationName : String?
        let volt : String?
        let tranNo : Int64?
        let vehicleNum :  String?
        let taxidName: String?
        let taxidNum: String?
        let lock: Bool?
        let preAuthAmount: String?
        let type: String?
        let chargingPoint:String?
        let parkingFee:String?
        let lockStatus: String?
        
//        enum CodingKeys: String, CodingKey {
//            case cardList = "cardList"
//            case chargingFee = "chargingFee"
//            case current = "current"
//            case kwh = "kwh"
//            case powerType = "powerType"
//            case qrNo = "qrNo"
//            case scoketType = "scoketType"
//            case stationName = "stationName"
//            case volt = "volt"
//            case tranNo = "tranNo"
//            case vehicleNum = "vehicleNum"
//            case taxidName = "taxidName"
//            case taxidNum = "taxidNum"
//            //是否有地鎖
//            case lock = "lock"
//            case preAuthAmount = "preAuthAmount"
//            case carType = "type"
//            case chargingPoint = "chargingPoint"
//            //地鎖狀態 0= 已降下 1= 已升起
//            case lockStatus = "lockStatus"
//            case parkingFee = "parkingFee"
//        }
//        init(from decoder: Decoder) throws {
//            let values = try decoder.container(keyedBy: CodingKeys.self)
//            cardList = try values.decodeIfPresent([CardList].self, forKey: .cardList)
//            chargingFee = try values.decodeIfPresent(String.self, forKey: .chargingFee)
//            current = try values.decodeIfPresent(String.self, forKey: .current)
//            kwh = try values.decodeIfPresent(String.self, forKey: .kwh)
//            powerType = try values.decodeIfPresent(String.self, forKey: .powerType)
//            qrNo = try values.decodeIfPresent(String.self, forKey: .qrNo)
//            scoketType = try values.decodeIfPresent(String.self, forKey: .scoketType)
//            stationName = try values.decodeIfPresent(String.self, forKey: .stationName)
//            volt = try values.decodeIfPresent(String.self, forKey: .volt)
//            tranNo = try values.decodeIfPresent(Int64.self, forKey: .tranNo)
//
//            vehicleNum = try values.decodeIfPresent(String.self, forKey: .vehicleNum)
//            taxidName = try values.decodeIfPresent(String.self, forKey: .taxidName)
//            taxidNum = try values.decodeIfPresent(String.self, forKey: .taxidNum)
//            lock = try values.decodeIfPresent(Bool.self, forKey: .lock)
//            lockStatus = try values.decodeIfPresent(String.self, forKey: .lockStatus)
//            preAuthAmount = try values.decodeIfPresent(String.self, forKey: .preAuthAmount)
//            carType = try values.decodeIfPresent(String.self, forKey: .carType)
//            chargingPoint = try values.decodeIfPresent(String.self, forKey: .chargingPoint)
//            parkingFee = try values.decodeIfPresent(String.self, forKey: .parkingFee)
//        }
        
        struct CardList : Codable {
            
            let addedDate : String?
            let cardName : String?
            let cardNo : Int?
            let cardNum : String?
            let cardType : String?
            let `default` : Bool?
            let expire: Bool?
            let expireSoon: Bool?
            let expiry: String?
            let linepayError: Bool?
            
//            enum CodingKeys: String, CodingKey {
//                case addedDate = "addedDate"
//                case cardName = "cardName"
//                case cardNo = "cardNo"
//                case cardNum = "cardNum"
//                case cardType = "cardType"
//                case defaultField = "default"
//                case expire = "expire"
//                case expireSoon = "expireSoon"
//                case expiry = "expiry"
//                case linepayError = "linepayError"
//            }
//            init(from decoder: Decoder) throws {
//                let values = try decoder.container(keyedBy: CodingKeys.self)
//                addedDate = try values.decodeIfPresent(String.self, forKey: .addedDate)
//                cardName = try values.decodeIfPresent(String.self, forKey: .cardName)
//                cardNo = try values.decodeIfPresent(Int.self, forKey: .cardNo)
//                cardNum = try values.decodeIfPresent(String.self, forKey: .cardNum)
//                cardType = try values.decodeIfPresent(String.self, forKey: .cardType)
//                defaultField = try values.decodeIfPresent(Bool.self, forKey: .defaultField)
//                expire = try values.decode(Bool.self, forKey: .expire)
//                expireSoon = try values.decode(Bool.self, forKey: .expireSoon)
//                expiry = try values.decode(String.self, forKey: .expiry)
//                linepayError = try values.decode(Bool.self, forKey: .linepayError)
//            }
        }
    }
}
