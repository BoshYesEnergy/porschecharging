//
//  ReserveDetailClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import Foundation

struct ReserveDetailClass : Codable {
    
    let datas : Data?
    let resCode : String?
    let resMsg : String?
    
    struct Data : Codable {
    // 暫時沒使用預約充電
//        let latitude: String?
//        let longitude: String?
//        let qrNo : String?
//        let reserveAmount : Int?
//        let reserveDate : String?
//        let reserveNo : Int?
//        let reserveStatus: String?
//        let reserveTime : String?
//        let socketType : String?
//        let stationAddr : String?
//        let stationName : String?
//        let finishCode_en:String?
        
        var vehicleType : String?
        let servicePhone:String?
        let finishCode_ch:String?
        let transIsEnd : String? //交易是否結束 (Y / N)
        let gunIsEnd : String? //槍是否掛回 (Y / N)
        var bufferFlag : String? // 是否尚在緩衝 (Y / N)
        let evEndTime : String?
        let nowTime : String?
        let overTime : String? //緩衝時間 (分鐘)
        var overTimeFee : String? // 緩衝費率 (5元 / 1分鐘)
        var overTimeAmt : String?
        var lock : Bool?
        var lockStatus : String?
        var chargeIsStart : String?
        var chargeIsEnd : String?
        var isOvertime : String?
    }
}
