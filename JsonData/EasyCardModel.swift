//
//  EasyCardModel.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import Foundation

struct EsayCardModel: Codable {
    let datas   : Data?
    let resCode : String?
    let resMsg  : String?
    
    struct Data : Codable {
        let linkUrl          : String?
        let expirationTime   : String?
        let intentId         : String?
    }
}


