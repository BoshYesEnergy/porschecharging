//
//  AppLoginClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import Foundation

struct AppLoginClass : Codable {
    
    let datas : Data?
    let resCode : String?
    let resMsg : String?
    
    
    enum CodingKeys: String, CodingKey {
        case datas
        case resCode = "resCode"
        case resMsg = "resMsg"
    }
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        datas = try values.decodeIfPresent(Data.self, forKey: .datas)
//        resCode = try values.decodeIfPresent(String.self, forKey: .resCode)
//        resMsg = try values.decodeIfPresent(String.self, forKey: .resMsg)
//    }
    
    struct Data : Codable {
        
        let authToken : String?
        let showChangePass : Bool?
        var memberNo : String?
        var deviceId : String?
        
        enum CodingKeys: String, CodingKey {
            case authToken = "authToken"
            case showChangePass = "showChangePass"
            case memberNo = "memberNo"
            case deviceId = "deviceId"
            
        }
//        init(from decoder: Decoder) throws {
//            let values = try decoder.container(keyedBy: CodingKeys.self)
//            authToken = try values.decodeIfPresent(String.self, forKey: .authToken)
//            showChangePass = try values.decodeIfPresent(Bool.self, forKey: .showChangePass)
//            deviceId = try values.decodeIfPresent(String.self, forKey: .deviceId)
//            if deviceId == nil {
//                deviceId = ""
//            }
//            memberNo = try values.decodeIfPresent(String.self, forKey: .memberNo)
//        }
        
        
    }
}
