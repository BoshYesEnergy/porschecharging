//
//  ReservePrepareClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import Foundation

struct ReservePrepareClass : Codable {
    
    let datas : Data?
    let resCode : String?
    let resMsg : String?
    
    
    enum CodingKeys: String, CodingKey {
        case datas
        case resCode = "resCode"
        case resMsg = "resMsg"
    }
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        datas = try values.decodeIfPresent(Data.self, forKey: .datas)
//        resCode = try values.decodeIfPresent(String.self, forKey: .resCode)
//        resMsg = try values.decodeIfPresent(String.self, forKey: .resMsg)
//    }
    
    struct Data : Codable {
        let cardList : [CardList]?
        let qrNo : String?
        let reserveAmount : Int?
        let reserveDate : String?
        let reserveNo : Int?
        let reserveTime : String?
        let socketType : String?
        let stationAddr : String?
        let stationName : String?
        let taxidName : String?
        let taxidNum : String?
        let vehicleNum : String?
        
        enum CodingKeys: String, CodingKey {
            case cardList = "cardList"
            case qrNo = "qrNo"
            case reserveAmount = "reserveAmount"
            case reserveDate = "reserveDate"
            case reserveNo = "reserveNo"
            case reserveTime = "reserveTime"
            case socketType = "socketType"
            case stationAddr = "stationAddr"
            case stationName = "stationName"
            case taxidName = "taxidName"
            case taxidNum = "taxidNum"
            case vehicleNum = "vehicleNum"
        }
        
//        init(from decoder: Decoder) throws {
//            let values = try decoder.container(keyedBy: CodingKeys.self)
//            cardList = try values.decodeIfPresent([CardList].self, forKey: .cardList)
//            qrNo = try values.decodeIfPresent(String.self, forKey: .qrNo)
//            reserveAmount = try values.decodeIfPresent(Int.self, forKey: .reserveAmount)
//            reserveDate = try values.decodeIfPresent(String.self, forKey: .reserveDate)
//            reserveNo = try values.decodeIfPresent(Int.self, forKey: .reserveNo)
//            reserveTime = try values.decodeIfPresent(String.self, forKey: .reserveTime)
//            socketType = try values.decodeIfPresent(String.self, forKey: .socketType)
//            stationAddr = try values.decodeIfPresent(String.self, forKey: .stationAddr)
//            stationName = try values.decodeIfPresent(String.self, forKey: .stationName)
//            taxidName = try values.decodeIfPresent(String.self, forKey: .taxidName)
//            taxidNum = try values.decodeIfPresent(String.self, forKey: .taxidNum)
//            vehicleNum = try values.decodeIfPresent(String.self, forKey: .vehicleNum)
//        }
        
        struct CardList : Codable {
            
            let cardName: String?
            let cardNo : Int?
            let cardNum: String?
            let isDefault: Bool?
            
            
            enum CodingKeys: String, CodingKey {
                case cardName = "cardName"
                case cardNo = "cardNo"
                case cardNum = "cardNum"
                case isDefault = "default"
            }
            
//            init(from decoder: Decoder) throws {
//                let values = try decoder.container(keyedBy: CodingKeys.self)
//                cardName = try values.decodeIfPresent(String.self, forKey: .cardName)
//                cardNo = try values.decodeIfPresent(Int.self, forKey: .cardNo)
//                cardNum = try values.decodeIfPresent(String.self, forKey: .cardNum)
//                isDefault = try values.decodeIfPresent(Bool.self, forKey: .isDefault)
//            }
        }
    }
}
