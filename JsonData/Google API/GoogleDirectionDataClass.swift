//
//  GoogleDirectionDataClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit
import GoogleMaps

class GoogleDirectionDataClass: NSObject {
    /* Google -  導航和計算方位 */
    
    // MARK: - Google Property
    var status: String?
//    var routes: [Dictionary<String, AnyObject>]?
    var overviewPolyline: Dictionary<String, AnyObject>?
    var points: String?
    var googleMapsView: GMSMapView!
    var routePolyline: GMSPolyline!
    
    
    // MARK: - Initialize
    public override init() {
        super.init()
    }
    
    // MARK: - Func
    func setDirectionDataWith(jsonDic: AnyObject , googleMapsView: GMSMapView) {
        
        /// 28/5/2020 -- 取消使用 -- 
//        if (jsonDic["status"] as? String) != nil {
//
//            let status = jsonDic["status"] as? String
//            self.status = status
//        }
//
//        if (jsonDic["routes"]) as? [Dictionary<String, AnyObject>] != nil {
//            let routes = jsonDic["routes"] as? [Dictionary<String, AnyObject>]
////            self.routes = routes
////
//            for route in routes! {
//                if let routeOverviewPolyline = route["overview_polyline"] as? Dictionary<String, AnyObject> {
//                    if let points = routeOverviewPolyline["points"] as? String {
//                        let path = GMSPath.init(fromEncodedPath: points)
//                        let polyline = GMSPolyline.init(path: path)
//                        polyline.strokeColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
//                        polyline.strokeWidth = 4
//                        polyline.map = googleMapsView
//                        routePolyline = polyline
//                        self.googleMapsView = googleMapsView
//                    }
//                }
//
//            }
//        }
    }
    
    func setDirectionApiData(_ points: String, googleMapsView: GMSMapView, color: UIColor) {
            status = "OK"
            let path = GMSPath.init(fromEncodedPath: points)
            let polyline = GMSPolyline.init(path: path)
        
            polyline.strokeColor = color
            polyline.strokeWidth = 4
            polyline.map = googleMapsView
            routePolyline = polyline
            self.googleMapsView = googleMapsView
    }
    
}

/* 範例
 
 
 {
    "geocoded_waypoints" : [
        {
             "geocoder_status" : "OK",
             "place_id" : "ChIJRVY_etDX3IARGYLVpoq7f68",
             "types" : [
                 "bus_station",
                 "transit_station",
                 "point_of_interest",
                 "establishment"
             ]
         },
         {
             "geocoder_status" : "OK",
             "partial_match" : true,
             "place_id" : "ChIJp2Mn4E2-woARQS2FILlxUzk",
             "types" : [ "route" ]
         }
    ],
    "routes" : [
         {
         "bounds" : {
             "northeast" : {
             "lat" : 34.1330949,
             "lng" : -117.9143879
            },
             "southwest" : {
             "lat" : 33.8068768,
             "lng" : -118.3527671
            }
         },
         "copyrights" : "Map data ©2016 Google",
         "legs" : [
            {
                 "distance" : {
                 "text" : "35.9 mi",
                 "value" : 57824
                 },
                 "duration" : {
                 "text" : "51 mins",
                 "value" : 3062
                 },
                 "end_address" : "Universal Studios Blvd, Los Angeles, CA 90068, USA",
                 "end_location" : {
                 "lat" : 34.1330949,
                 "lng" : -118.3524442
                },
                 "start_address" : "Disneyland (Harbor Blvd.), S Harbor Blvd, Anaheim, CA 92802, USA",
                 "start_location" : {
                 "lat" : 33.8098177,
                 "lng" : -117.9154353
                },
 
 ... Additional results truncated in this example[] ...
 
 
            "overview_polyline" : {
            "points" : "knjmEnjunUbKCfEA?_@]@kMBeE@qIIoF@wH@eFFk@WOUI_@?u@j@k@`@EXLTZHh@Y`AgApAaCrCUd@cDpDuAtAoApA{YlZiBdBaIhGkFrDeCtBuFxFmIdJmOjPaChDeBlDiAdD}ApGcDxU}@hEmAxD}[tt@yNb\\yBdEqFnJqB~DeFxMgK~VsMr[uKzVoCxEsEtG}BzCkHhKWh@]t@{AxEcClLkCjLi@`CwBfHaEzJuBdEyEhIaBnCiF|K_Oz\\
                {MdZwAbDaKbUiB|CgCnDkDbEiE|FqBlDsLdXqQra@kX|m@aF|KcHtLm@pAaE~JcTxh@w\\`v@gQv`@}F`MqK`PeGzIyGfJiG~GeLhLgIpIcE~FsDrHcFfLqDzH{CxEwAbBgC|B}F|DiQzKsbBdeA{k@~\\oc@bWoKjGaEzCoEzEwDxFsUh^wJfOySx[uBnCgCbCoFlDmDvAiCr@eRzDuNxC_EvAiFpCaC|AqGpEwHzFoQnQoTrTqBlCyDnGmCfEmDpDyGzGsIzHuZzYwBpBsC`CqBlAsBbAqCxAoBrAqDdDcNfMgHbHiPtReBtCkD|GqAhBwBzBsG~FoAhAaCbDeBvD_BlEyM``@uBvKiA~DmAlCkA|B}@lBcChHoJnXcB`GoAnIS~CIjFDd]A|QMlD{@jH[vAk@`CoGxRgPzf@aBbHoB~HeMx^eDtJ}BnG{DhJU`@mBzCoCjDaAx@mAnAgCnBmAp@uAj@{Cr@wBPkB@kBSsEW{GV}BEeCWyAWwHs@qH?
                    cIHkDXuDn@mCt@mE`BsH|CyAp@}AdAaAtAy@lBg@pCa@jE]fEcBhRq@pJKlCk@hLFrB@lD_@xCeA`DoBxDaHvM_FzImDzFeCpDeC|CkExDiJrHcBtAkDpDwObVuCpFeCdHoIl\\uBjIuClJsEvMyDbMqAhEoDlJ{C|J}FlZuBfLyDlXwB~QkArG_AnDiAxC{G|OgEdLaE`LkBbEwG~KgHnLoEjGgDxCaC`BuJdFkFtCgCnBuClD_HdMqEzHcBpB_C|BuEzCmPlIuE|B_EtDeBhCgAdCw@rCi@|DSfECrCAdCS~Di@jDYhA_AlC{AxCcL`U{GvM_DjFkBzBsB`BqDhBaEfAsTvEmEr@iCr@qDrAiFnCcEzCaE~D_@JmFdGQDwBvCeErEoD|BcFjC}DbEuD~D`@Zr@h@?d@Wr@}@vAgCbEaHfMqA`Cy@dAg@bAO`@gCi@w@W"
            },
             "summary" : "I-5 N and US-101 N",
             "warnings" : [],
             "waypoint_order" : []
        }
    ],
    "status" : "OK"
 }
 
 
 */
