//
//  GoogleGeocodingDataClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class GoogleGeocodingDataClass: NSObject {
    /* Google - 地址 轉成 座標  API*/
    
    // MARK: - Google Property
    var status: String?
    var formattedAddress: String?
    var lat: Double?
    var lng: Double?
    private var results: [Dictionary<String, AnyObject>]?
    private var geometry: Dictionary<String, AnyObject>?
    private var location: Dictionary<String, AnyObject>?
   
//    private var addressComponents: [Dictionary<String, AnyObject>]?
//    private var longName: String?
//    private var shorName: String?
//    private var types: [String]?
//    private var locationType: String?
//    private var viewport: Dictionary<String, AnyObject>?
//    private var northeast: Dictionary<String, AnyObject>?
//    private var southwest: Dictionary<String, AnyObject>?
//    private var placeId: String?
    
    var locationType: GeocodingLocationType = .None

    // MARK: - Initialize
    public override init() {
        super.init()
    }
    
    // MARK: - Func
    func setGeocodingDataWith(jsonDic: AnyObject , geocodingLocationType: GeocodingLocationType) {
        
        self.locationType = geocodingLocationType
        
        if (jsonDic["status"] as? String) != nil {
            
            let status = jsonDic["status"] as? String
            self.status = status
        }
            
        if (jsonDic["results"]) as? [Dictionary<String, AnyObject>] != nil {
            let results = jsonDic["results"] as? [Dictionary<String, AnyObject>]
            self.results = results
            
            for result in results! {
                
                if let formattedAddress = result["formatted_address"] as? String {
                    self.formattedAddress = formattedAddress
                }
                
                if let geometry = result["geometry"] as? Dictionary<String, AnyObject> {
                    
                    self.geometry = geometry
                    
                    if let location = geometry["location"] as? Dictionary<String, AnyObject>
                    {
                        self.location = location
                        
                        if let lat = location["lat"] as? Double {
                            self.lat = lat
                        }
                        
                        if let lng = location["lng"] as? Double {
                            self.lng = lng
                        }
                    }
                }
            }
        }
    }
    
    func setCoodinateToAddressDataWith(jsonDic: AnyObject , geocodingLocationType: GeocodingLocationType) {
        
        self.locationType = geocodingLocationType
        
        if (jsonDic["status"] as? String) != nil {
            
            let status = jsonDic["status"] as? String
            self.status = status
        }
        
        if (jsonDic["results"]) as? [Dictionary<String, AnyObject>] != nil {
            let results = jsonDic["results"] as? [Dictionary<String, AnyObject>]
            self.results = results
            
            for result in results! {
                
                if let formattedAddress = result["formatted_address"] as? String {
                    self.formattedAddress = formattedAddress
                }
                
                if let geometry = result["geometry"] as? Dictionary<String, AnyObject> {
                    
                    self.geometry = geometry
                    
                    if let location = geometry["location"] as? Dictionary<String, AnyObject>
                    {
                        self.location = location
                        
                        if let lat = location["lat"] as? Double {
                            self.lat = lat
                        }
                        
                        if let lng = location["lng"] as? Double {
                            self.lng = lng
                        }
                    }
                }
            }
        }
    }
}

/*  座標轉地址 - 範例
 {
     "results" : [
         {
         "address_components" : [
             {
             "long_name" : "277",
             "short_name" : "277",
             "types" : [ "street_number" ]
             },
             {
             "long_name" : "Bedford Avenue",
             "short_name" : "Bedford Ave",
             "types" : [ "route" ]
             },
             {
             "long_name" : "Williamsburg",
             "short_name" : "Williamsburg",
             "types" : [ "neighborhood", "political" ]
             },
             {
             "long_name" : "Brooklyn",
             "short_name" : "Brooklyn",
             "types" : [ "sublocality", "political" ]
             },
             {
             "long_name" : "Kings",
             "short_name" : "Kings",
             "types" : [ "administrative_area_level_2", "political" ]
             },
             {
             "long_name" : "New York",
             "short_name" : "NY",
             "types" : [ "administrative_area_level_1", "political" ]
             },
             {
             "long_name" : "United States",
             "short_name" : "US",
             "types" : [ "country", "political" ]
             },
             {
             "long_name" : "11211",
             "short_name" : "11211",
             "types" : [ "postal_code" ]
             }
         ],
         "formatted_address" : "277 Bedford Avenue, Brooklyn, NY 11211, USA",
         "geometry" : {
             "location" : {
                 "lat" : 40.714232,
                 "lng" : -73.9612889
             },
             "location_type" : "ROOFTOP",
             "viewport" : {
                 "northeast" : {
                     "lat" : 40.7155809802915,
                     "lng" : -73.9599399197085
                 },
                 "southwest" : {
                     "lat" : 40.7128830197085,
                     "lng" : -73.96263788029151
                 }
             }
         },
         "place_id" : "ChIJd8BlQ2BZwokRAFUEcm_qrcA",
         "types" : [ "street_address" ]
 },
 
 ... Additional results[] ...
 */



    /*  地址轉座標 - 範例
    {
    "results" : [
        {
            "formatted_address" : "1600 Amphitheatre Parkway, Mountain View, CA 94043, USA",
            "geometry" : {
                    "location" : {
                        "lat" : 37.4224764,
                        "lng" : -122.0842499
                    },
                    "location_type" : "ROOFTOP",
                    "viewport" : {
                        "northeast" : {
                                "lat" : 37.4238253802915,
                                "lng" : -122.0829009197085
                        },
                        "southwest" : {
                                "lat" : 37.4211274197085,
                                "lng" : -122.0855988802915
                        }
                    }
                },
            "place_id" : "ChIJ2eUgeAK6j4ARbn5u_wAGqWA",
            "types" : [ "street_address" ]
        }
    ],
    "status" : "OK"
    } */
    

