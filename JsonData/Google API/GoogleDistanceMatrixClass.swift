//
//  GoogleDistanceMatrixClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import Foundation
import UIKit


class GoogleDistanceMatrixClass: NSObject {
    /* Google - 計算距離與時間 */
    
    // MARK: - Property - Google Data
    var status: String?
    var durationValue: Int?
    var durationText: String?
    var distanceValue: Int?
    var distanceText: String?
    private var originAddresses: [Dictionary<String, AnyObject>]?
    private var destinationAddresses: [Dictionary<String, AnyObject>]?
    private var rows: [Dictionary<String, AnyObject>]?
    private var elements: [Dictionary<String, AnyObject>]?
    private var duration: Dictionary<String, AnyObject>?
    private var distance: Dictionary<String, AnyObject>?
    
    var avoidType: GoogleRouteAvoidType = .None
    var resultType: DistanceMatrixType = .None
    
    public override init() {
        super.init()
    }
    
    func setDataWith(jsonDic: AnyObject , avoidType: GoogleRouteAvoidType , resultType: DistanceMatrixType) {
        
        self.avoidType = avoidType
        self.resultType = resultType
        
        if (jsonDic["status"] as? String) != nil {
            
            let status = jsonDic["status"] as? String
            self.status = status
        }
        
        if (jsonDic["origin_addresses"] as? [Dictionary<String, AnyObject>]) != nil {
            self.originAddresses = jsonDic["origin_addresses"] as? [Dictionary<String, AnyObject>]
        }
        
        if (jsonDic["destinationAddresses"] as? [Dictionary<String, AnyObject>]) != nil {
            self.destinationAddresses = jsonDic["destinationAddresses"] as? [Dictionary<String, AnyObject>]
        }
        
        if (jsonDic["rows"] as? [Dictionary<String, AnyObject>] != nil) {
            
            let rows = jsonDic["rows"] as? [Dictionary<String, AnyObject>]
            self.rows = rows
            
            for row in rows! {
                if let elements = row["elements"] as? [Dictionary<String, AnyObject>] {
                    
                    self.elements = elements
                    
                    for element in elements {
                        if let distance = element["distance"] as? Dictionary<String, AnyObject> {
                            self.distance = distance
                            
                            if let value = distance["value"] as? Int {
                                self.distanceValue = value
                            }
                            
                            if let text = distance["text"] as? String {
                                self.distanceText = text
                            }
                        }
                        
                        if let duration = element["duration"] as? Dictionary<String, AnyObject> {
                            self.duration = duration
                            
                            if let value = duration["value"] as? Int {
                                self.durationValue = value
                            }
                            
                            if let text = duration["text"] as? String {
                                self.durationText = text
                            }
                        }
                    }
                }
            }
        }
    }
}

/* 範例
 
 {
     "status": "OK",
         "origin_addresses": [ "Vancouver, BC, Canada", "Seattle, État de Washington, États-Unis" ],
         "destination_addresses": [ "San Francisco, Californie, États-Unis", "Victoria, BC, Canada" ],
         "rows": [ {
         "elements": [ {
             "status": "OK",
             "duration": {
                 "value": 340110,
                 "text": "3 jours 22 heures"
             },
             "distance": {
                 "value": 1734542,
                 "text": "1 735 km"
             }
             }, {
             "status": "OK",
             "duration": {
                 "value": 24487,
                 "text": "6 heures 48 minutes"
             },
             "distance": {
                 "value": 129324,
                 "text": "129 km"
             }
         } ]
     }, {
     "elements": [ {
             "status": "OK",
             "duration": {
                 "value": 288834,
                 "text": "3 jours 8 heures"
             },
             "distance": {
                 "value": 1489604,
                 "text": "1 490 km"
             }
             }, {
             "status": "OK",
             "duration": {
                 "value": 14388,
                 "text": "4 heures 0 minutes"
             },
             "distance": {
                 "value": 135822,
                 "text": "136 km"
             }
         } ]
     } ]
 }
 */
