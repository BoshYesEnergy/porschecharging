//
//  GoogleAPIRequestManager.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

protocol GoogleAPIRequestManagerDelegate: class {
    func googleGeocodingResultWith(geocodingData: GoogleGeocodingDataClass )
    func googleDistanceMatrixResultWith(distanceDurationData: GoogleDistanceMatrixClass)
    func googleDirectionResultWith(directionData: GoogleDirectionDataClass)
}

class GoogleAPIRequestManager: NSObject {
   
    weak var delegate: GoogleAPIRequestManagerDelegate?
    
    /// Google - 地址 轉成 座標
    func callGoogleAddressConvertToCoodinateWith(addressString: String , geocodingLocationType: GeocodingLocationType) {
        var strGoogleApi = "https://maps.googleapis.com/maps/api/geocode/json?address=\(addressString)&key=\(KJLRequest.googleKey)"
        
        strGoogleApi = strGoogleApi.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        var urlRequest = URLRequest(url: URL(string: strGoogleApi)!)
        urlRequest.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if error == nil {
                if data != nil {
                    let jsonDic = try? JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
//                    print(jsonDic)
                    let googleGeocoding = GoogleGeocodingDataClass.init()
                    googleGeocoding.setGeocodingDataWith(jsonDic: jsonDic as AnyObject , geocodingLocationType: geocodingLocationType)
                    DispatchQueue.main.async {
                        self.delegate?.googleGeocodingResultWith(geocodingData: googleGeocoding)
                    }
                }
            } else {
                
            }
        }
        task.resume()
    }
    
    
    func callGoogleCoodinateConvertToAddressWith(latitude: CLLocationDegrees , longitude: CLLocationDegrees,  geocodingLocationType: GeocodingLocationType) {
        var strGoogleApi = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(latitude),\(longitude)&location_type=ROOFTOP&result_type=street_address&key=\(KJLRequest.googleKey)"
        
        strGoogleApi = strGoogleApi.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        var urlRequest = URLRequest(url: URL(string: strGoogleApi)!)
        urlRequest.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if error == nil {
                if data != nil {
                    let jsonDic = try? JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
//                    print(jsonDic)
                    let googleGeocoding = GoogleGeocodingDataClass.init()
                    googleGeocoding.setCoodinateToAddressDataWith(jsonDic: jsonDic as AnyObject , geocodingLocationType: geocodingLocationType)
                    DispatchQueue.main.async {
                        self.delegate?.googleGeocodingResultWith(geocodingData: googleGeocoding)
                    }
                }
            } else {
                
            }
        }
        task.resume()
    }
    
    /// Google - 計算距離與時間
    func callGoogleDistanceMatrixAPIWith(startCoodinate : CLLocationCoordinate2D , endCoodinate: CLLocationCoordinate2D , avoidType: GoogleRouteAvoidType , resultType: DistanceMatrixType) {
       
        let origin = "\(startCoodinate.latitude),\(startCoodinate.longitude)"
        let destination = "\(endCoodinate.latitude),\(endCoodinate.longitude)"
    
        var strGoogleApi = "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=\(origin)&destinations=\(destination)&avoid=\(avoidType.rawValue)&mode=driving&key=\(KJLRequest.googleKey)"
    
        strGoogleApi = strGoogleApi.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        var urlRequest = URLRequest(url: URL(string: strGoogleApi)!)
        urlRequest.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if error == nil {
                if data != nil {
                    let jsonDic = try? JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
//                  print(jsonDic)
                    let googleDistanceMatrix = GoogleDistanceMatrixClass.init()
                    googleDistanceMatrix.setDataWith(jsonDic: jsonDic as AnyObject , avoidType: avoidType , resultType: resultType)
                    DispatchQueue.main.async {
                        self.delegate?.googleDistanceMatrixResultWith(distanceDurationData: googleDistanceMatrix)
                    }
                }
            } else {
                
            }
        }
        task.resume()
    }
    
    /// Google - 導航和計算方位
    func callGoogleDirectionAPIWith(startCoodinate: CLLocationCoordinate2D , endCoodinate: CLLocationCoordinate2D , avoidType: GoogleRouteAvoidType , googleMapView: GMSMapView, lineColor: UIColor) {
        KJLRequest.requestForGoogleMapRoute("\(startCoodinate.latitude)", "\(startCoodinate.longitude)", "\(endCoodinate.latitude)", "\(endCoodinate.longitude)", avoid: avoidType.rawValue) {[weak self] (response) in
            
            guard let data = response as? GoogleMapRouteModel,
                let points = data.datas?.points
                else { return }
            let googleDirection = GoogleDirectionDataClass()
            print(points)
            googleDirection.setDirectionApiData(points, googleMapsView: googleMapView, color: lineColor)
            DispatchQueue.main.async {
                self?.delegate?.googleDirectionResultWith(directionData: googleDirection)
            }
        }
        
        /// 28/5/2020 -- 取消使用 --
//        let origin = "\(startCoodinate.latitude),\(startCoodinate.longitude)"
//        let destination = "\(endCoodinate.latitude),\(endCoodinate.longitude)"
//        var strGoogleApi = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&avoid=\(avoidType.rawValue)&mode=driving&key=\(KJLRequest.googleKey)"
//
//        strGoogleApi = strGoogleApi.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
//        var urlRequest = URLRequest(url: URL(string: strGoogleApi)!)
//        urlRequest.httpMethod = "GET"
//        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
//            if error == nil {
//                if data != nil {
//                    let jsonDic = try? JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
////                    print(jsonDic)
//                    let googleDirection = GoogleDirectionDataClass()
//                    googleDirection.setDirectionDataWith(jsonDic: jsonDic as AnyObject, googleMapsView: googleMapView)
//                    DispatchQueue.main.async {
//                        self.delegate?.googleDirectionResultWith(directionData: googleDirection)
//                    }
//                }
//            } else {
//
//            }
//        }
//        task.resume()
    }
}
