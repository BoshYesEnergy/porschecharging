//
//  ChargerPairingModel.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/6/30.
//  Copyright © 2020 s5346. All rights reserved.
//

import Foundation

struct ChargerPairingModel : Codable {
    
    let datas   : Data?
    let resCode : String?
    let resMsg  : String?
    
    struct Data : Codable {
        
        let cardList        : [CardList]?
        let rateList        : [RateList]?
        let chargingFee     : String?
        let socketType      : String?
        let stationName     : String?
        let lock            : Bool?
        let type            : String?
        let chargingPoint   : String?
        let parkingFee      : String?
        let lockStatus      : String?
        let overtimeFee     : String?
        let overtimeBuffer  : Int?
        let discount        : Bool?
        let discountDesc    : String?
        let discountDate    : String?
        let noneQrcode      : Bool?    /// 免掃 = true
 
        struct CardList : Codable {
            let cardNo      : Int?
            let paymentType : String?
            let cardType    : String?
            let cardNum     : String?
            let cardName    : String?
            let addedDate   : String?
            let expiry      : String?
            let warn        : String?
            let `default`   : Bool?
            let expireSoon  : Bool?
            let disable     : Bool?
        }
        
        struct RateList: Codable {
             let brand       : String?
             let model       : String?
             let list        : [List]?
        }
        
        struct List: Codable {
            let frequency   : Int64?
            let type        : String?   //分鐘
            let amount      : Int?
            let fee         : String?
            let pointFee    : Bool?
        }
    }
}
