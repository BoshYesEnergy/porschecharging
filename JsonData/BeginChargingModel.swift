//
//  StartChargingClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import Foundation

struct BeginChargingModel : Codable {
    
    let datas : Data?
    let resCode : String?
    let resMsg : String?
     
    struct Data : Codable {
        let startTime : String?
        let tranNo    : Int?
    }
}
