//
//  MajiStationDetailClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import Foundation

struct MajiStationDetailClass : Codable {
    
    let data : [Data]?
    
    
    enum CodingKeys: String, CodingKey {
        case data
    }
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        data = try values.decodeIfPresent([Data].self, forKey: .data)
//    }
    
    struct Data : Codable {
        
        let store_id : String? 
        let vendor_id : String?
        let store_type : String?
        let lat : Double?
        let lng : Double?
        let name : String?
        let cooperation_state : String?
        let telephone : String?
        let address : String?
        let current_business_hours : String?
        let full_business_hours : String?
        let share_link : String?
        let opening_time_status : String?
        let opening_time_all_day_open : Bool?
        let extra_info : ExtraInfo?
        var note1:String?
        enum CodingKeys: String, CodingKey {
            case store_id = "store_id"
            case vendor_id = "vendor_id"
            case store_type = "store_type"
            case lat = "lat"
            case lng = "lng"
            case name = "name"
            case cooperation_state = "cooperation_state"
            case telephone = "telephone"
            case address = "address"
            case current_business_hours = "current_business_hours"
            case full_business_hours = "full_business_hours"
            case share_link = "share_link"
            case opening_time_status = "opening_time_status"
            case opening_time_all_day_open = "opening_time_all_day_open"
            case extra_info = "extra_info"
            case note1 = "note1"
        }
        
//        init(from decoder: Decoder) throws {
//            let values = try decoder.container(keyedBy: CodingKeys.self)
//            store_id = try values.decodeIfPresent(String.self, forKey: .store_id)
//            vendor_id = try values.decodeIfPresent(String.self, forKey: .vendor_id)
//            store_type = try values.decodeIfPresent(String.self, forKey: .store_type)
//            lat = try values.decodeIfPresent(Double.self, forKey: .lat)
//            lng = try values.decodeIfPresent(Double.self, forKey: .lng)
//            name = try values.decodeIfPresent(String.self, forKey: .name)
//            cooperation_state = try values.decodeIfPresent(String.self, forKey: .cooperation_state)
//            telephone = try values.decodeIfPresent(String.self, forKey: .telephone)
//            address = try values.decodeIfPresent(String.self, forKey: .address)
//            current_business_hours = try values.decodeIfPresent(String.self, forKey: .current_business_hours)
//            full_business_hours = try values.decodeIfPresent(String.self, forKey: .full_business_hours )
//            share_link = try values.decodeIfPresent(String.self, forKey: .share_link)
//            opening_time_status = try values.decodeIfPresent(String.self, forKey: .opening_time_status)
//            opening_time_all_day_open = try values.decodeIfPresent(Bool.self, forKey: .opening_time_all_day_open)
//            extra_info = try values.decodeIfPresent(ExtraInfo.self, forKey: .extra_info)
//            note1 = try values.decodeIfPresent(String.self, forKey: .note1)
//            if note1 == nil {
//                note1 = ""
//            }
//        }
        
        struct ExtraInfo : Codable {
            
            let current_parking_price: String?
            let current_first_hour_price: CurrentFirstHourPrice?
            let full_parking_price: String?
            let full_parking_price_array: [String]?
            let building: String?
            let autopass_issue: String?
            let total_lots: Int?
            let available_lots: Int?
            let gas_price: GasPrice?
            
            enum CodingKeys: String, CodingKey {
                case current_parking_price = "current_parking_price"
                case current_first_hour_price = "current_first_hour_price"
                case full_parking_price = "full_parking_price"
                case full_parking_price_array = "full_parking_price_array"
                case building = "building"
                case autopass_issue = "autopass_issue"
                case total_lots = "total_lots"
                case available_lots = "available_lots"
                case gas_price = "gas_price"
            }
            
//            init(from decoder: Decoder) throws {
//                let values = try decoder.container(keyedBy: CodingKeys.self)
//                current_parking_price = try values.decodeIfPresent(String.self, forKey: .current_parking_price)
//                current_first_hour_price = try values.decodeIfPresent(CurrentFirstHourPrice.self, forKey: .current_first_hour_price)
//                full_parking_price = try values.decodeIfPresent(String.self, forKey: .full_parking_price)
//                full_parking_price_array = try values.decodeIfPresent([String].self, forKey: .full_parking_price_array)
//                building = try values.decodeIfPresent(String.self, forKey: .building)
//                autopass_issue = try values.decodeIfPresent(String.self, forKey: .autopass_issue)
//                total_lots = try values.decodeIfPresent(Int.self, forKey: .total_lots)
//                available_lots = try values.decodeIfPresent(Int.self, forKey: .available_lots)
//                gas_price = try values.decodeIfPresent(GasPrice.self, forKey: .gas_price)
//            }
            
            
            struct CurrentFirstHourPrice : Codable {
                
                let price: String?
                let charge_mode: String?
                let fixed_rate_duration: Int?
                
                enum CodingKeys: String, CodingKey {
                    case price = "price"
                    case charge_mode = "charge_mode"
                    case fixed_rate_duration = "fixed_rate_duration"
                }
                
//                init(from decoder: Decoder) throws {
//                    let values = try decoder.container(keyedBy: CodingKeys.self)
//                    price = try values.decodeIfPresent(String.self, forKey: .price)
//                    charge_mode = try values.decodeIfPresent(String.self, forKey: .charge_mode)
//                    fixed_rate_duration = try values.decodeIfPresent(Int.self, forKey: .fixed_rate_duration)
//                }
            }
            
            struct GasPrice : Codable {
                
                let gas_92: String?
                let gas_95: String?
                let gas_98: String?
                let diesel: String?
                
                enum CodingKeys: String, CodingKey {
                    case gas_92 = "gas_92"
                    case gas_95 = "gas_95"
                    case gas_98 = "gas_98"
                    case diesel = "diesel"
                }
                
//                init(from decoder: Decoder) throws {
//                    let values = try decoder.container(keyedBy: CodingKeys.self)
//                    gas_92 = try values.decodeIfPresent(String.self, forKey: .gas_92)
//                    gas_95 = try values.decodeIfPresent(String.self, forKey: .gas_95)
//                    gas_98 = try values.decodeIfPresent(String.self, forKey: .gas_98)
//                    diesel = try values.decodeIfPresent(String.self, forKey: .diesel)
//                }
            }
        }
    }
}
