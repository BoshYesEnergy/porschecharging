//
//  PorscheLoginModel.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/19.
//  Copyright © 2020 s5346. All rights reserved.
//

import Foundation

struct PorscheLoginModel: Codable {
    let datas            : Data?
    let resCode          : String?
    let resMsg           : String?
    
    struct Data: Codable {
        let authToken: String?
        let showChangePass: Bool?
        let memberNo: String?
        let deviceId: String?
        let checkTerms: Bool?
    }
}
