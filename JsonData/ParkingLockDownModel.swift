//
//  ParkingLockOpenClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import Foundation

struct ParkingLockDownModel : Codable {
    
    let datas : Data?
    let resCode : String?
    let resMsg : String?
    
    struct Data : Codable {
        let openResult : Bool?
        let lockStatus : Int?
        let tranNo     : String?
        let startTime  : String?
        let result     : Bool?
    }
}
