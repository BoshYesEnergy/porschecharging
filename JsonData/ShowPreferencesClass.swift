//
//  ShowPreferencesClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import Foundation

struct ShowPreferencesClass : Codable {
    
    let datas : Data?
    let resCode : String?
    let resMsg : String?
    
    
    enum CodingKeys: String, CodingKey {
        case datas
        case resCode = "resCode"
        case resMsg = "resMsg"
    }
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        datas = try values.decodeIfPresent(Data.self, forKey: .datas)
//        resCode = try values.decodeIfPresent(String.self, forKey: .resCode)
//        resMsg = try values.decodeIfPresent(String.self, forKey: .resMsg)
//    }
    
    struct Data : Codable {
        
        let preferences : Preference?
        let useDefault : Bool?
        
        
        enum CodingKeys: String, CodingKey {
            case preferences
            case useDefault = "useDefault"
        }
//        init(from decoder: Decoder) throws {
//            let values = try decoder.container(keyedBy: CodingKeys.self)
//            preferences = try values.decodeIfPresent(Preference.self, forKey: .preferences)
//            useDefault = try values.decodeIfPresent(Bool.self, forKey: .useDefault)
//        }
        
        
    }
    
    struct Preference : Codable {
        
        let chargeTypes : String?
        let currentTypes : String?
        let interfaceTypesAC : String?
        let interfaceTypesDC : String?
        let mapDetail : String?
        let showFavourite : Bool?
        
        let businessTime: Bool?
        let chargePayType: String?
        let parkingCoupon: String?
        let socketBrand: String?
        let stationOpenType: String?
        let stationReserve: Bool?
        let stationStatus: Bool?
        
        let type : String?
        
        enum CodingKeys: String, CodingKey {
            case chargeTypes = "chargeTypes"
            case currentTypes = "currentTypes"
            case interfaceTypesAC = "interfaceTypesAC"
            case interfaceTypesDC = "interfaceTypesDC"
            case mapDetail = "mapDetail"
            case showFavourite = "showFavourite"
            
            case businessTime = "businessTime"
            case chargePayType = "chargePayType"
            case parkingCoupon = "parkingCoupon"
            case socketBrand = "socketBrand"
            case stationOpenType = "stationOpenType"
            case stationReserve = "stationReserve"
            case stationStatus = "stationStatus"
            
            case type = "type"
        }
//        init(from decoder: Decoder) throws {
//            let values = try decoder.container(keyedBy: CodingKeys.self)
//            chargeTypes = try values.decodeIfPresent(String.self, forKey: .chargeTypes)
//            currentTypes = try values.decodeIfPresent(String.self, forKey: .currentTypes)
//            interfaceTypesAC = try values.decodeIfPresent(String.self, forKey: .interfaceTypesAC)
//            interfaceTypesDC = try values.decodeIfPresent(String.self, forKey: .interfaceTypesDC)
//            mapDetail = try values.decodeIfPresent(String.self, forKey: .mapDetail)
//            showFavourite = try values.decodeIfPresent(Bool.self, forKey: .showFavourite)
//            type = try values.decodeIfPresent(String.self, forKey: .type)
//            
//            businessTime = try values.decodeIfPresent(Bool.self, forKey: .businessTime)
//            chargePayType = try values.decodeIfPresent(String.self, forKey: .chargePayType)
//            parkingCoupon = try values.decodeIfPresent(String.self, forKey: .parkingCoupon)
//            socketBrand = try values.decodeIfPresent(String.self, forKey: .socketBrand)
//            stationOpenType = try values.decodeIfPresent(String.self, forKey: .stationOpenType)
//            stationReserve = try values.decodeIfPresent(Bool.self, forKey: .stationReserve)
//            stationStatus = try values.decodeIfPresent(Bool.self, forKey: .stationStatus)
//        }
    }
}
