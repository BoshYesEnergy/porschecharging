//
//  CheckPartnerModel.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/1.
//  Copyright © 2020 s5346. All rights reserved.
//

import Foundation

struct CheckPartnerModel: Codable {
    let datas            : Datas?
    let resCode          : String?
    let resMsg           : String?
    
    struct Datas: Codable {
        var nickName: String?
        var email   : String?
        var partner : Bool? 
    }
}
