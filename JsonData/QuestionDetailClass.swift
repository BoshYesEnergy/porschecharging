//
//  QuestionDetailClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import Foundation

struct QuestionDetailClass : Codable {
    
    let datas : DataClass?
    let resCode : String?
    let resMsg : String?
    
    
    enum CodingKeys: String, CodingKey {
        case datas
        case resCode = "resCode"
        case resMsg = "resMsg"
    }
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        datas = try values.decodeIfPresent(DataClass.self, forKey: .datas)
//        resCode = try values.decodeIfPresent(String.self, forKey: .resCode)
//        resMsg = try values.decodeIfPresent(String.self, forKey: .resMsg)
//    }
    
    struct DataClass : Codable {
        
        let picUrl : String?
        let questionNo : Int?
        let text : String?
        let title : String?
        
        
        enum CodingKeys: String, CodingKey {
            case picUrl = "picUrl"
            case questionNo = "questionNo"
            case text = "text"
            case title = "title"
        }
//        init(from decoder: Decoder) throws {
//            let values = try decoder.container(keyedBy: CodingKeys.self)
//            picUrl = try values.decodeIfPresent(String.self, forKey: .picUrl)
//            questionNo = try values.decodeIfPresent(Int.self, forKey: .questionNo)
//            text = try values.decodeIfPresent(String.self, forKey: .text)
//            title = try values.decodeIfPresent(String.self, forKey: .title)
//        }
        
        
    }
}
