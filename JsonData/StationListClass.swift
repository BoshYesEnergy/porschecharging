//
//  StationListClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import Foundation

struct StationListClass : Codable {
    
    let datas : [Data]?
    let resCode : String?
    let resMsg : String?
    let distance: Int?
    
    struct Data : Codable {
        var myDistance: String?
        
        let stationId : String?
        let stationName : String?
        let stationScore : String?
        let stationAbrv : String?
        var stationAddress: String?
        let latitude : String?
        let longitude : String?
        let hasAC : Bool?
        let hasDC : Bool?
        let acEmpty : Int?
        let acTotal : Int?
        let dcEmpty : Int?
        let dcTotal : Int?
        let favourite : Bool?
        let status : String?
        let businessHours: String?
        let type : String? // 1:汽車, 2:機車
        let noneQrcode: Bool?
        let stationCategory: String?
        let connection   : Bool?
        let isBussinessTime : String? // NA = no detail, Y = working, N = close ,
    }
}
