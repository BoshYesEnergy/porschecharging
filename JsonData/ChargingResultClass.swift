//
//  ChargingResultClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import Foundation

struct ChargingResultClass : Codable {
    
    let datas : Data?
    let resCode : String?
    let resMsg : String?
    
    
    enum CodingKeys: String, CodingKey {
        case datas
        case resCode = "resCode"
        case resMsg = "resMsg"
    }
    
    struct Data : Codable {
        let isAICharge: String?
        let amount : Int?
        let cardNum : String?
        let chargeStatus : String?
        let endTime : String?
        let paymentStatus : String?
        let paymentError : String?
        let qrNo : String?
        let startTime : String?
        let stationName : String?
        let totalKwh : String?
        let totalTime : Int?
        let socketType: String?
        
        let finishCode_en:String?
        let finishCode_ch:String?
        let useFee: String?
        let thisFee: String?
        let deductFee: String?
        let remainPoint: String?
        let reserveFlag: String?
        let reserveStart: String?
        let reserveAmt: String?
        let overtimeFlag: String?
        let overtimeStart: String?
        let overtimeEnd: String?
        let overtimes: Int?
        let overtimeAmt: String?
        //充電累計費用
        let chargeamount: Int?
        
        //開始充電時間
        let chargestartTime:String?

        //充電結束時間
        let chargeendTime:String?
        
        //充電累計時間 單位秒
        let chargetotalTime:Int?
    
        //付款總金額
        let paymentamount:Int?
        
        //停車累計費用
        let parkamount:Int?
        
        //停車費率
        let parkRate:String?
        
        //開始停車時間
        let parkstartTime: String?
        
        //停車結束時間
        let parkendTime: String?
        
        //停車累計時間 單位秒
        let parktotalTime: Int?
        
        //充電費率
        let chargeRate: String?
        
        //交易單號
        let tran_no: String?
        
        //地鎖編號
        let lockNo: String?

        //是否有優惠
        var isDiscount:String?
        
        //優惠費率
        var discountFee:String?
        
        //優惠扣除餘額
        var discountAmt:String?
        //是否有地鎖
        var lock : Bool?
        var transactionNo:String?
        enum CodingKeys: String, CodingKey {
            case lock = "lock"
            case transactionNo = "transactionNo"
            case discountFee = "discountFee"
            case discountAmt = "discountAmt"
            case amount = "amount"
            case cardNum = "cardNum"
            case chargeStatus = "chargeStatus"
            case endTime = "endTime"
            case paymentStatus = "paymentStatus"
            case paymentError = "paymentError"
            case qrNo = "qrNo"
            case startTime = "startTime"
            case stationName = "stationName"
            case totalKwh = "totalKwh"
            case totalTime = "totalTime"
            case socketType = "socketType"
            case isAICharge = "isAICharge"
            case useFee = "useFee"
            case thisFee = "thisFee"
            case deductFee = "deductFee"
            case remainPoint = "remainPoint"
            case reserveFlag = "reserveFlag"
            case reserveStart = "reserveStart"
            case reserveAmt = "reserveAmt"
            case overtimeFlag = "overtimeFlag"
            case overtimeStart = "overtimeStart"
            case overtimeEnd = "overtimeEnd"
            case overtimes = "overtimes"
            case overtimeAmt = "overtimeAmt"
            case finishCode_en = "finishCode_en"
            case finishCode_ch = "finishCode_ch"
            case chargeamount = "chargeamount"
            case chargestartTime = "chargestartTime"
            case chargeendTime = "chargeendTime"
            case chargetotalTime = "chargetotalTime"
            case paymentamount = "paymentamount"
            case parkamount = "parkamount"
            case parkRate = "parkRate"
            
            //開始停車時間
            case parkstartTime = "parkstartTime"
                   
                   //停車結束時間
            case parkendTime = "parkendTime"
                   
                   //停車累計時間 單位秒
            case parktotalTime = "parktotalTime"
                   
                   //充電費率
            case chargeRate = "chargeRate"
                   
                   //交易單號
            case tran_no = "tran_no"
                   
                   //地鎖編號
            case lockNo = "lockNo"
            case isDiscount = "isDiscount"
            //            // == 適用費率 (中華點數 / 一般費率) ==
            //            private String useFee;
            //            // == 本次費率 ( 10元 / 1分鐘) ==
            //            private String thisFee;
            //            // == 扣款金額 ==
            //            private String deductFee;
            //            // == 剩餘點數 ==
            //            private String remainPoint;
            //
            //            // == 是否預約 (Y/N)==
            //            private String reserveFlag;
            //            // == 預約開始 ==
            //            private String reserveStart;
            //            // == 預約金額 ==
            //            private String reserveAmt;
            //
            //            // == 是否逾時 (Y/N)==
            //            private String overtimeFlag;
            //            // == 是否逾時開始 ==
            //            private String overtimeStart;
            //            // == 是否逾時結束 ==
            //            private String overtimeEnd;
            //            // == 是否逾時時間 ==
            //            private Int overtimes;
            //            // == 是否逾時金額 ==
            //            private String overtimeAmt;
        }
    }
}
