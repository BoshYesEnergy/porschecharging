//
//  ChargingReportListClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import Foundation

struct ChargingReportListClass : Codable {
    
    let datas : [Data]?
    let resCode : String?
    let resMsg : String?
    
    enum CodingKeys: String, CodingKey {
        case datas
        case resCode = "resCode"
        case resMsg = "resMsg"
    }
    
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        datas = try values.decodeIfPresent([Data].self, forKey: .datas)
//        resCode = try values.decodeIfPresent(String.self, forKey: .resCode)
//        resMsg = try values.decodeIfPresent(String.self, forKey: .resMsg)
//    }
    
    struct Data : Codable {
        
        let nickname : String?
        let picUrl : String?
        let reportType : String?
        let comment : String?
        let reportTime : String?
        
        enum CodingKeys: String, CodingKey {
            case nickname = "nickname"
            case picUrl = "picUrl"
            case reportType = "reportType"
            case comment = "comment"
            case reportTime = "reportTime"
        }
        
//        init(from decoder: Decoder) throws {
//            let values = try decoder.container(keyedBy: CodingKeys.self)
//            
//            nickname = try values.decodeIfPresent(String.self, forKey: .nickname)
//            picUrl = try values.decodeIfPresent(String.self, forKey: .picUrl)
//            reportType = try values.decodeIfPresent(String.self, forKey: .reportType)
//            comment = try values.decodeIfPresent(String.self, forKey: .comment)
//            reportTime = try values.decodeIfPresent(String.self, forKey: .reportTime)
//        }
    }
}
