//
//  PartnerModel.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/26.
//  Copyright © 2020 s5346. All rights reserved.
//

import Foundation

struct PartnerModel: Codable {
    let datas            : Datas?
    let resCode          : String?
    let resMsg           : String?
    
    struct Datas: Codable {
        var infos: [Infos]?
        var porscheMan: Bool? // true = shared qrcode code
    }
    
    struct Infos: Codable {
        var memberNo: Int?
        var nickname: String?
        var email   : String?
        var licensePlate: String?
        var startTime : String?
        var endTime: String?
        var status: Bool?
        var carType : String?
    }
}
