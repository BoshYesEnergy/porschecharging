//
//  StationSearchModel.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/16.
//  Copyright © 2020 s5346. All rights reserved.
//

import Foundation

struct StationSearchModel: Codable {
    let datas            : [Data]?
    let resCode          : String?
    let resMsg           : String?
    
    struct Data: Codable {
        let position : String?
        let address  : String?
        let longitude: String?
        let latitude : String?
        let type     : String? /// m = me, h = hot, "" = google
        let stationId: String?
    }
}
