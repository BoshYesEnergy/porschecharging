//
//  LinePaymentClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import Foundation

// MARK: - Temperatures
struct LinePaymentClass: Codable {
    let returnCode, returnMessage: String
    let info: LinePaymentInfo
}

// MARK: - Info
struct LinePaymentInfo: Codable {
    let paymentURL: PaymentURL
    let transactionID: Int
    let paymentAccessToken: String
    
    enum CodingKeys: String, CodingKey {
        case paymentURL = "paymentUrl"
        case transactionID = "transactionId"
        case paymentAccessToken
    }
}

// MARK: - PaymentURL
struct PaymentURL: Codable {
    let web: String
    let app: String
}
