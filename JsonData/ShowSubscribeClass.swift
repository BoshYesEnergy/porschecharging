//
//  ShowSubscribeClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import Foundation

struct ShowSubscribeClass : Codable {
    
    let datas : Data?
    let resCode : String?
    let resMsg : String?
    
    
    enum CodingKeys: String, CodingKey {
        case datas
        case resCode = "resCode"
        case resMsg = "resMsg"
    }
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        datas = try values.decodeIfPresent(Data.self, forKey: .datas)
//        resCode = try values.decodeIfPresent(String.self, forKey: .resCode)
//        resMsg = try values.decodeIfPresent(String.self, forKey: .resMsg)
//    }
    
    struct Data : Codable {
        
        let preferences : Preference?
        let useDefault : Bool?
        
        
        enum CodingKeys: String, CodingKey {
            case preferences
            case useDefault = "useDefault"
        }
//        init(from decoder: Decoder) throws {
//            let values = try decoder.container(keyedBy: CodingKeys.self)
//            preferences = try values.decodeIfPresent(Preference.self, forKey: .preferences)
//            useDefault = try values.decodeIfPresent(Bool.self, forKey: .useDefault)
//        }
        
        
    }
    
    struct Preference : Codable {
        
        let locList : [LocList]?
        let type : String?
        
        
        enum CodingKeys: String, CodingKey {
            case locList = "locList"
            case type = "type"
        }
//        init(from decoder: Decoder) throws {
//            let values = try decoder.container(keyedBy: CodingKeys.self)
//            locList = try values.decodeIfPresent([LocList].self, forKey: .locList)
//            type = try values.decodeIfPresent(String.self, forKey: .type)
//        }
        
        
    }
    
    struct LocList : Codable {
        
        let secDatas : [SecData]?
        let secName : String?
        
        
        enum CodingKeys: String, CodingKey {
            case secDatas = "secDatas"
            case secName = "secName"
        }
//        init(from decoder: Decoder) throws {
//            let values = try decoder.container(keyedBy: CodingKeys.self)
//            secDatas = try values.decodeIfPresent([SecData].self, forKey: .secDatas)
//            secName = try values.decodeIfPresent(String.self, forKey: .secName)
//        }
        
        
    }
    
    struct SecData : Codable {
        
        let locName : String?
        
        
        enum CodingKeys: String, CodingKey {
            case locName = "locName"
        }
//        init(from decoder: Decoder) throws {
//            let values = try decoder.container(keyedBy: CodingKeys.self)
//            locName = try values.decodeIfPresent(String.self, forKey: .locName)
//        }
        
        
    }
}
