//
//  MbCodeListClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import Foundation

struct MbCodeListClass : Codable {
    
    let datas : [Data]?
    let resCode : String?
    let resMsg : String?
    
    
    enum CodingKeys: String, CodingKey {
        case datas = "datas"
        case resCode = "resCode"
        case resMsg = "resMsg"
    }
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        datas = try values.decodeIfPresent([Data].self, forKey: .datas)
//        resCode = try values.decodeIfPresent(String.self, forKey: .resCode)
//        resMsg = try values.decodeIfPresent(String.self, forKey: .resMsg)
//    }
    
    struct Data : Codable {
        
        let code : String?
        let defaultField : Bool?
        let name : String?
        let no : Int?
        
        
        enum CodingKeys: String, CodingKey {
            case code = "code"
            case defaultField = "default"
            case name = "name"
            case no = "no"
        }
//        init(from decoder: Decoder) throws {
//            let values = try decoder.container(keyedBy: CodingKeys.self)
//            code = try values.decodeIfPresent(String.self, forKey: .code)
//            defaultField = try values.decodeIfPresent(Bool.self, forKey: .defaultField)
//            name = try values.decodeIfPresent(String.self, forKey: .name)
//            no = try values.decodeIfPresent(Int.self, forKey: .no)
//        }
        
        
    }
}
