//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct NormalClass : Codable {

	let datas : Data?
	let resCode : String?
	let resMsg : String?

    struct Data : Codable { 
        let authToken : String?
        let webLink:String?
        let appLink:String?
        let transactionId:String?
        var deviceId:String?
        var memberNo:String?
        var points: String?
    }
}
