//
//  ChargingHistoryModel.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/12.
//  Copyright © 2020 s5346. All rights reserved.
//

import Foundation

struct ChargingHistoryModel: Codable {
    let datas            : [Datas]?
    let resCode          : String?
    let resMsg           : String?
    
    struct Datas: Codable {
        let tranNo      : Int?
        let status      : String?
        let stationName : String?
        let chargingTime: String?
        let soc         : String?
        let startTime   : String?
    }
}
