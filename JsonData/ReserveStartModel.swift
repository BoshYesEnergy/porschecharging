//
//  ReserveStartModel.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/21.
//  Copyright © 2020 s5346. All rights reserved.
//

import Foundation

struct ReserveStartModel: Codable {
    let datas            : Data?
    let resCode          : String?
    let resMsg           : String?
    
    struct Data: Codable {
        let reserveNo: Int?
        let longitude: String?
        let latitude: String?
        let reserveEndTime: String? 
    }
}
