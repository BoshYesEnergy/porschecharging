//
//  GoogleMapRouteModel.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/5/28.
//  Copyright © 2020 s5346. All rights reserved.
//

import Foundation

struct GoogleMapRouteModel: Codable {
    let datas : Data?
    let resCode : String?
    let resMsg : String?
    
    struct Data: Codable {
        var points: String?
    }
}
