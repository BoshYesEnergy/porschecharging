//
//  CarChargingStatusModel.swift
//  NissanCar
//
//  Created by Heng.Wang on 2020/7/22.
//  Copyright © 2020 s5346. All rights reserved.
//

import Foundation

struct ChargingStatusModel : Codable {
    
    let datas : Data?
    let resCode : String?
    let resMsg : String?
    
    struct Data : Codable {
        let status:         String?
        let current:        String?
        let voltage:        String?
        let kwh:            String?
        let chargeAmount:   Int?
        let totalTime:      Int?
        let parkAmount:     Int?
        let parkTotalTime:  Int?
        let chargeRate:     String?
        let parkRate:       String?
        let type:           String?
        let amountType:     String?
        let cardNoInfo:     String?
        let overtimeFee:    String?
        let pointEndTime:   String?
        let socketId:       String?
        let chargeIsStart:  String?
        let chargeIsEnd:    String?
    }
}

