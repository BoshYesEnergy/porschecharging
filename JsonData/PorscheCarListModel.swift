//
//  PorscheCarList.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/23.
//  Copyright © 2020 s5346. All rights reserved.
//

import Foundation

struct PorscheCarListModel: Codable {
    let datas            : Data?
    let resCode          : String?
    let resMsg           : String?
    
    struct Data: Codable {
        var infos        : [Infos]? 
    }
     
    struct Infos: Codable {
        var cardNum      : String?
        var licensePlate : String?
        var startTime    : String?
        var endTime      : String?
        var status       : Bool?
        var carType      : String?
        let porscheEmail : String?
        let vin          : String?
    }
}
