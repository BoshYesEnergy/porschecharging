//
//  apiForQuestionListClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import Foundation

struct QuestionListClass : Codable {
    
    let datas : [Data]?
    let resCode : String?
    let resMsg : String?
    
    
    enum CodingKeys: String, CodingKey {
        case datas = "datas"
        case resCode = "resCode"
        case resMsg = "resMsg"
    }
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        datas = try values.decodeIfPresent([Data].self, forKey: .datas)
//        resCode = try values.decodeIfPresent(String.self, forKey: .resCode)
//        resMsg = try values.decodeIfPresent(String.self, forKey: .resMsg)
//    }
    
    struct Data : Codable {
        
        let questionNo : Int?
        let title : String?
        
        
        enum CodingKeys: String, CodingKey {
            case questionNo = "questionNo"
            case title = "title"
        }
//        init(from decoder: Decoder) throws {
//            let values = try decoder.container(keyedBy: CodingKeys.self)
//            questionNo = try values.decodeIfPresent(Int.self, forKey: .questionNo)
//            title = try values.decodeIfPresent(String.self, forKey: .title)
//        }
        
        
    }
}
