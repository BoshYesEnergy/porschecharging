//
//  TaxIDListClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import Foundation

struct TaxIDListClass : Codable {
    
    let datas : [Data]?
    let resCode : String?
    let resMsg : String?
    
    
    enum CodingKeys: String, CodingKey {
        case datas = "datas"
        case resCode = "resCode"
        case resMsg = "resMsg"
    }
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        datas = try values.decodeIfPresent([Data].self, forKey: .datas)
//        resCode = try values.decodeIfPresent(String.self, forKey: .resCode)
//        resMsg = try values.decodeIfPresent(String.self, forKey: .resMsg)
//    }
    
    struct Data : Codable {
        
        let defaultField : Bool?
        let taxId : String?
        let taxIdName : String?
        let taxIdNo : Int?
        
        
        enum CodingKeys: String, CodingKey {
            case defaultField = "default"
            case taxId = "taxId"
            case taxIdName = "taxIdName"
            case taxIdNo = "taxIdNo"
        }
//        init(from decoder: Decoder) throws {
//            let values = try decoder.container(keyedBy: CodingKeys.self)
//            defaultField = try values.decodeIfPresent(Bool.self, forKey: .defaultField)
//            taxId = try values.decodeIfPresent(String.self, forKey: .taxId)
//            taxIdName = try values.decodeIfPresent(String.self, forKey: .taxIdName)
//            taxIdNo = try values.decodeIfPresent(Int.self, forKey: .taxIdNo)
//        }
        
        
    }
}
