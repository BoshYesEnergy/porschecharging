//
//  ChargeListClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import Foundation

struct ChargeListClass : Codable {
    
    let datas : [Data]?
    let resCode : String?
    let resMsg : String?
    
    
    enum CodingKeys: String, CodingKey {
        case datas = "datas"
        case resCode = "resCode"
        case resMsg = "resMsg"
    }
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        datas = try values.decodeIfPresent([Data].self, forKey: .datas)
//        resCode = try values.decodeIfPresent(String.self, forKey: .resCode)
//        resMsg = try values.decodeIfPresent(String.self, forKey: .resMsg)
//    }
    
    struct Data : Codable {
        
        let chargingFee : String?
        let descriptionField : String?
        let endTime : String?
        let payment : String?
        let result : String?
        let startTime : String?
        let stationId : String?
        let stationName : String?
        let tranNo : Int64?
        let chargeStatus : String?
        let amount : Int64?
        let paymentStatus : String?
        let type: String?
        let paymentStatusColor : String?
        let usefee : String?
        enum CodingKeys: String, CodingKey {
            case chargingFee = "chargingFee"
            case descriptionField = "description"
            case endTime = "endTime"
            case payment = "payment"
            case result = "result"
            case startTime = "startTime"
            case stationId = "stationId"
            case stationName = "stationName"
            case tranNo = "tranNo"
            case chargeStatus = "chargeStatus"
            case amount = "amount"
            case paymentStatus = "paymentStatus"
            case type = "type"
            case paymentStatusColor = "paymentStatusColor"
            case usefee = "usefee"
        }
//        init(from decoder: Decoder) throws {
//            let values = try decoder.container(keyedBy: CodingKeys.self)
//            chargingFee = try values.decodeIfPresent(String.self, forKey: .chargingFee)
//            type = try values.decodeIfPresent(String.self, forKey: .type)
//            descriptionField = try values.decodeIfPresent(String.self, forKey: .descriptionField)
//            endTime = try values.decodeIfPresent(String.self, forKey: .endTime)
//            payment = try values.decodeIfPresent(String.self, forKey: .payment)
//            result = try values.decodeIfPresent(String.self, forKey: .result)
//            startTime = try values.decodeIfPresent(String.self, forKey: .startTime)
//            stationId = try values.decodeIfPresent(String.self, forKey: .stationId)
//            stationName = try values.decodeIfPresent(String.self, forKey: .stationName)
//            tranNo = try values.decodeIfPresent(Int64.self, forKey: .tranNo)
//            chargeStatus = try values.decodeIfPresent(String.self, forKey: .chargeStatus)
//            amount = try values.decodeIfPresent(Int64.self, forKey: .amount)
//            paymentStatus = try values.decodeIfPresent(String.self, forKey: .paymentStatus)
//        }
        
        
    }
}
