//
//  NewTypeCountModel.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/5/25.
//  Copyright © 2020 s5346. All rights reserved.
//

import Foundation

struct NewTypeCountModel: Codable {
    let datas   : Data?
    let resCode : String?
    let resMsg  : String?
    
    struct Data: Codable {
        var newestCount:   Int?
        var activeCount:   Int?
        var warningCount : Int?
        var pushCount:     Int?
    }
}


