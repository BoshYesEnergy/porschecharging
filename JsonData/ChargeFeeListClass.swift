//
//  ChargeFeeListClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import Foundation

struct ChargeFeeListClass : Codable {
    
    let datas : [Data]?
    let resCode : String?
    let resMsg : String?
    
    
    enum CodingKeys: String, CodingKey {
        case datas
        case resCode = "resCode"
        case resMsg = "resMsg"
    }
    
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        datas = try values.decodeIfPresent([Data].self, forKey: .datas)
//        resCode = try values.decodeIfPresent(String.self, forKey: .resCode)
//        resMsg = try values.decodeIfPresent(String.self, forKey: .resMsg)
//    }
    
    struct Data : Codable {
        
        let chargeFee : String?
        let chargeFeeName : String?
       
        
        enum CodingKeys: String, CodingKey {
            
            case chargeFee = "chargeFee"
            case chargeFeeName = "chargeFeeName"
        }
        
//        init(from decoder: Decoder) throws {
//            let values = try decoder.container(keyedBy: CodingKeys.self)
//            chargeFee = try values.decodeIfPresent(String.self, forKey: .chargeFee)
//            chargeFeeName = try values.decodeIfPresent(String.self, forKey: .chargeFeeName)
//        }
    }
}
