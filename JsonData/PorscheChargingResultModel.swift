//
//  PorscheChargingResultModel.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/12.
//  Copyright © 2020 s5346. All rights reserved.
//

import Foundation

struct PorscheChargingResultModel: Codable {
    let datas            : Datas?
    let resCode          : String?
    let resMsg           : String?
    
    struct Datas: Codable {
        let stationName        : String?
        let batteryInfo        : BatteryInfo?
        let chargingInfo       : ChargingInfo?
        let overStayTimeInfo   :  String?
        let paymentStatusInfo  : PaymentStatusInfo?
    }
    
    struct BatteryInfo: Codable {
        let start : String?
        let end   : String?
        let status: String?
        let kwh   : String?
    }
    
    struct ChargingInfo: Codable {
        let startTime: String?
        let endTime  : String?
        let total    : String?
    }
    
    struct PaymentStatusInfo: Codable {
        let status   : String?
        let method   : String?
        let transactionId : String?
    }
}
