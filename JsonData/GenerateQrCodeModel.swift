//
//  GenerateQrCodeModel.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/22.
//  Copyright © 2020 s5346. All rights reserved.
//

import Foundation

struct GenerateQrCodeModel: Codable {
    let datas            : Data?
    let resCode          : String?
    let resMsg           : String?
    
    struct Data: Codable {
        var qrcode       : String?
    }
}
