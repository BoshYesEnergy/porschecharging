//
//  InvoiceSettingListModel.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/28.
//  Copyright © 2020 s5346. All rights reserved.
//

import Foundation

struct InvoiceSettingListModel: Codable {
    let datas            : Datas?
    let resCode          : String?
    let resMsg           : String?
    
    struct Datas: Codable {
        var accountEmail: String?
        var backupEmail: String?
        var useBackup: Bool?
        var invoiceType: String?
        var vehicleType: String?
        var vehicleNum: String?
        var taxidName: String?
        var taxidNum: String?
        
    }
}
