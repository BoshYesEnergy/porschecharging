//
//  ReserveCheckModel.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/22.
//  Copyright © 2020 s5346. All rights reserved.
//

import Foundation

struct ReserveCheckModel: Codable {
    let datas            : Data?
    let resCode          : String?
    let resMsg           : String?
    
    struct Data: Codable {
        let parkingSpace    : Int?
        let stationName     : String?
        let stationAddress  : String?
        let longitude       : String?
        let latitude        : Bool?
        let reserveEndTime  : String?
    }
}
