//
//  QueryLicensePlateModel.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/8/20.
//  Copyright © 2020 s5346. All rights reserved.
//

import Foundation

struct QueryLicensePlateModel: Codable {
    let datas            : Data?
    let resCode          : String?
    let resMsg           : String?
    
    struct Data: Codable {
        var paymentVerifi: Bool?
        var warn         : String?
        var list         : [Lists]?
        
        struct Lists: Codable {
            var registerPhone: String?
            var licensePlate : String?
            var remark       : String?
            var bindDate     : String?
            var chargeLimit  : Int?
        }
    } 
}
