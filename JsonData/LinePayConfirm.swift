//
//  LinePayConfirm.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

import Foundation

// MARK: - Temperatures
struct LinePayConfirm: Codable {
    let returnCode, returnMessage: String
    let info: Info
}

// MARK: - Info
struct Info: Codable {
    let orderID: String
    let payInfo: [PayInfo]
    let regKey: String
    
    enum CodingKeys: String, CodingKey {
        case orderID = "orderId"
        case payInfo, regKey
    }
}

// MARK: - PayInfo
struct PayInfo: Codable {
    let method: String
    let amount: Int
    let creditCardNickname, creditCardBrand, maskedCreditCardNumber: String
}
