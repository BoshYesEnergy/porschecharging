//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct ProfileClass : Codable {

	let datas : Data?
	let resCode : String?
	let resMsg : String?

    struct Data : Codable {
        
        let carType         : String?
        let mails           : [Mail]?
        let nickName        : String?
        let phoneNumber     : String?
        let phoneVerified   : Bool?
        let sex             : String? //M: 男性 / F: 女性
        let picUrl          : String?
        let carNo           : Int?
        let hasPassword     : Bool?
        let memberType      : String?  /// 1: 一般會員 2: CMC會員 3: 企業用戶
        let unRead          : Int?
        let paymentIssues   : Bool?
        let bind            : Bool?
        let subscriptionMail: String?
        let subscription    : Bool?
        let subscriptionEndDate: String?
        let cardType        : String?
        let cardNo          : Int?
        let cardNum         : String?
//        let partner         : Bool?
        let voucher         : Bool?
        var porscheMan      : Bool? /// porsche company
        var partnerMails    : [PartnerMails]?
        var unread          : Int?
        
        struct Mail : Codable {
            let `default`   : Bool?
            let email       : String?
            let source      : String?
            let verified    : Bool?
             
        }
        
        struct PartnerMails : Codable {
            let email: String?
        }
    }
}
