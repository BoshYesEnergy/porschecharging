//
//  ChargingListClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import Foundation

struct ChargingListClass : Codable {
    
    let datas   : ChargingList?
    let resCode : String?
    let resMsg  : String?
    
    struct ChargingList : Codable {
        
        let tranNo      : Int64?
        let startTime   : String?
        let stationName : String?
        let qrNo        : String?
        let charging    : Bool?             // 充電狀態 
        let trans       : [BackPayment]?
        
        struct BackPayment   : Codable {
            let tranNo       : Int64?
            let stationId    : String?
            let stationName  : String?
            let paymentAmount: Int64?
            let type         : String?
        } 
    }
}
