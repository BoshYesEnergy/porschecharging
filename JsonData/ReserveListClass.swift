//
//  ReserveListClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import Foundation

struct ReserveListClass : Codable {
    
    let datas : [Data]?
    let resCode : String?
    let resMsg : String?
    
    enum CodingKeys: String, CodingKey {
        case datas
        case resCode = "resCode"
        case resMsg = "resMsg"
    }
    
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        datas = try values.decodeIfPresent([Data].self, forKey: .datas)
//        resCode = try values.decodeIfPresent(String.self, forKey: .resCode)
//        resMsg = try values.decodeIfPresent(String.self, forKey: .resMsg)
//    }
    
    struct Data : Codable {
        let reserveAmount: Int?
        let reserveNo: Int?
        let reserveStatus: String?
        let reserveTime: String?
        let stationName: String?
        
        enum CodingKeys: String, CodingKey {
            case reserveAmount = "reserveAmount"
            case reserveNo = "reserveNo"
            case reserveStatus = "reserveStatus"
            case reserveTime = "reserveTime"
            case stationName = "stationName"
        }
        
//        init(from decoder: Decoder) throws {
//            let values = try decoder.container(keyedBy: CodingKeys.self)
//            
//            reserveAmount = try values.decodeIfPresent(Int.self, forKey: .reserveAmount)
//            reserveNo = try values.decodeIfPresent(Int.self, forKey: .reserveNo)
//            reserveStatus = try values.decodeIfPresent(String.self, forKey: .reserveStatus)
//            reserveTime = try values.decodeIfPresent(String.self, forKey: .reserveTime)
//            stationName = try values.decodeIfPresent(String.self, forKey: .stationName)
//        }
    }
}
