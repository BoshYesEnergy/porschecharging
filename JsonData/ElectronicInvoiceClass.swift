//
//  ElectronicInvoiceClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import Foundation

struct ElectronicInvoiceClass : Codable {
    
    let datas : Data?
    let resCode : String?
    let resMsg : String?
    
    enum CodingKeys: String, CodingKey {
        case datas
        case resCode = "resCode"
        case resMsg = "resMsg"
    }
    
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        datas = try values.decodeIfPresent(Data.self, forKey: .datas)
//        resCode = try values.decodeIfPresent(String.self, forKey: .resCode)
//        resMsg = try values.decodeIfPresent(String.self, forKey: .resMsg)
//    }
    
    struct Data : Codable {
        
        let invoiceType: String?
        let taxidName: String?
        let taxidNum: String?
        let vehicleNum: String?
        let vehicleType: String?
        
        enum CodingKeys: String, CodingKey {
            case invoiceType = "invoiceType"
            case taxidName = "taxidName"
            case taxidNum = "taxidNum"
            case vehicleNum = "vehicleNum"
            case vehicleType = "vehicleType"
        }
        
//        init(from decoder: Decoder) throws {
//            let values = try decoder.container(keyedBy: CodingKeys.self)
//            
//            invoiceType = try values.decodeIfPresent(String.self, forKey: .invoiceType)
//            taxidName = try values.decodeIfPresent(String.self, forKey: .taxidName)
//            taxidNum = try values.decodeIfPresent(String.self, forKey: .taxidNum)
//            vehicleNum = try values.decodeIfPresent(String.self, forKey: .vehicleNum)
//            vehicleType = try values.decodeIfPresent(String.self, forKey: .vehicleType)
//        }
    }
}
