//
//  ScoketListClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import Foundation

struct SocketListClass : Codable {
    
    let datas : [Data]?
    let resCode : String?
    let resMsg : String?
     
    struct Data : Codable {
        let kwh : String?
        let powerType : String?
        let qrNo : String?
        let socketType : String?
        let stationAbrv : String?
        let status : String? 
    }
}
