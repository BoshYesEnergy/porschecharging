//
//  NewsListClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import Foundation

struct NewsListClass : Codable {
    
    let datas : [Data]?
    let resCode : String?
    let resMsg : String?
    
    struct Data : Codable {
        let newsNo : Int?
        let outline : String?
        let title : String?
        let datetime: String?
        let newsType: String?
        let content:String?
        let createDate:String?
        let detailId:String?
        let readDate:String?
        let subject:String?
        let taskId:Int?
        let read: Bool? 
    }
}
