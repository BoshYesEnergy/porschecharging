//
//  StationDetailClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import Foundation

struct StationDetailClass : Codable {
    
    let datas : Data?
    let resCode : String?
    let resMsg : String?
    
    struct Data : Codable {
        
        let acEmpty : Int?
        let acTotal : Int?
        let dcEmpty : Int?
        let dcTotal : Int?
        let businessHours : String?
        let full_business_hours : String?
        let opening_time_status : String?
        let chargingFee : String?
        let coverPicNo : Int? 
        var favourite : Bool?
        let hasAC : Bool?
        let hasDC : Bool?
        let interface_AC : String?
        let interface_DC : String?
        let latitude : String?
        let longitude : String?
        let parkingFee : String?
        let picArray : [PicArray]?
        let stationAddress : String?
        let stationId : String?
        let stationName : String?
//        let stationNo : Int?
        let stationScore : String?
        let stationAbrv : String?
        let status : String?
        let carSpace : String?
        let motorSpace : String?
        let ocppType : String?
        let reserveFee : String?
        let spotFloor : String?
        let stationPhone : String?
        let reserve : Bool?
        let majiId : String?
        let majiToken : String?
        var note1:String?
        var lock: Bool?
          
        var connection: Bool?
        var parkingSpace: Int? // 0 = 無車位 , 1 = 左邊 , 2 = 右邊
        
        struct PicArray : Codable {
            
            let picNo : Int?
            let picPath : String?
            
        }
    }
}
