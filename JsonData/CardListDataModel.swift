//
//  CardListDataModel.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/6/12.
//  Copyright © 2020 s5346. All rights reserved.
//

import Foundation

struct CardListDataModel : Codable {
    
    let datas : [Data]?
    let resCode : String?
    let resMsg : String?
    
    struct Data : Codable { 
        let cardNo      : Int?
        let paymentType : String?
        let cardType    : String?
        let cardNum     : String?
        let cardName    : String?
        let addedDate   : String?
        let expiry      : String?
        var warn        : String?
         
        var `default`   : Bool?
        var disable     : Bool?
        var expireSoon  : Bool?
    }
}
