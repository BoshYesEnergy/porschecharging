//
//  CheckPhoneClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import Foundation

struct CheckPhoneClass : Codable {
    
    let datas : Data?
    let resCode : String?
    let resMsg : String?
    
    
    enum CodingKeys: String, CodingKey {
        case datas
        case resCode = "resCode"
        case resMsg = "resMsg"
    }
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        datas = try values.decodeIfPresent(Data.self, forKey: .datas)
//        resCode = try values.decodeIfPresent(String.self, forKey: .resCode)
//        resMsg = try values.decodeIfPresent(String.self, forKey: .resMsg)
//    }
    
    struct Data : Codable {
        
        let email : String?
        let phoneStatus : Bool?
        
        
        enum CodingKeys: String, CodingKey {
            case email = "email"
            case phoneStatus = "phoneStatus"
        }
//        init(from decoder: Decoder) throws {
//            let values = try decoder.container(keyedBy: CodingKeys.self)
//            email = try values.decodeIfPresent(String.self, forKey: .email)
//            phoneStatus = try values.decodeIfPresent(Bool.self, forKey: .phoneStatus)
//        }
        
        
    }
}
