//
//  SubscriptionCarModel.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/23.
//  Copyright © 2020 s5346. All rights reserved.
//

import Foundation

struct SubscriptionCarModel: Codable {
    let datas            : Data?
    let resCode          : String?
    let resMsg           : String?
    
    struct Data: Codable {
        var vin         : String?
        var model       : String?
        var validity    : String?
    }
}
