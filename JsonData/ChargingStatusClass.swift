//
//  ChargingStatusClass.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import Foundation

struct ChargingStatusClass : Codable {
    
    let datas : Data?
    let resCode : String?
    let resMsg : String?
    
    struct Data : Codable {
                
        let status           : String?
        let stationName      : String?
        let chargesportId    : String?
        let kwh              : String?
        let soc              : String?
        let chargingStartTime: String?
        let lock             : Bool?
    }
}
