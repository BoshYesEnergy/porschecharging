//
//  NotificationCenter+Name.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/8/1.
//  Copyright © 2020 s5346. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let mainVersionCheck       = Notification.Name("mainVersionCheck")
    static let mainReloadMarkerList   = Notification.Name("mainReloadMarkerList")
    static let linePayUpdate          = Notification.Name("LinePayUpdate")
    static let easyPayUpdate          = Notification.Name("EasyPayUpdate")
    static let getDxidFinished        = Notification.Name("GetDxidFinished")
    static let inobxSyncFinished      = Notification.Name("InobxSyncFinished")
    static let tableViewInit          = Notification.Name("TableViewInit")
    
    static let carChargingStatus      = Notification.Name("CarChargingStatus")
    static let startChargError        = Notification.Name("startChargError")
    static let repayLoadView          = Notification.Name("repayLoadView")
    static let rePayCreditCard        = Notification.Name("RePayCreditCard")
    static let presentRePayCreditCard = Notification.Name("presentRePayCreditCard")
    static let reloadCarCompleteCharging = Notification.Name("reloadCarCompleteCharging") 
    static let changeScanTabImage     = Notification.Name("changeScanTabImage")
    static let queryLicensePlate      = Notification.Name("queryLicensePlate")
    static let startChargingTimer     = Notification.Name("startChargingTimer")
    
    static let popToProfile           = Notification.Name("popToProfile")
    static let apiDeleteCard          = Notification.Name("apiDeleteCard")
    
    static let apiPorscheCarList      = Notification.Name("apiPorscheCarList")
    static let apiUpdatePartner       = Notification.Name("apiUpdatePartner")
    static let apiDeletePartner       = Notification.Name("apiDeletePartner")
    static let checkPartnerRequest    = Notification.Name("checkPartnerRequest")
    static let confirmPartner         = Notification.Name("confirmPartner")
    static let cancelPartner          = Notification.Name("cancelPartner")
    
    static let gotoCharging           = Notification.Name("gotoCharging")
    static let mapAllUpdate           = Notification.Name("mapAllUpdate")
    static let reloadSubscription     = Notification.Name("reloadSubscription")
    static let reloaAddSubscriptionView = Notification.Name("reloaAddSubscriptionView")
    
    static let updateRouteList = Notification.Name("updateRouteList")
    static let porscheLogin = Notification.Name("porscheLogin")
    
    static let showChargingStatusAlert = Notification.Name("showChargingStatusAlert")
    static let hiddenChargingStatusAlert = Notification.Name("hiddenChargingStatusAlert")
    // send data
    static let sendChargingTranNo = Notification.Name("sendChargingTranNo")
    static let checkChargingStatus = Notification.Name("checkChargingStatus")
    static let chargingStatus      = Notification.Name("chargingStatus")
    static let startChargingAnimate = Notification.Name("startChargingAnimate")
    
}
