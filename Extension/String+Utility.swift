//
//  String+Utility.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/8/1.
//  Copyright © 2020 s5346. All rights reserved.
//

import Foundation

extension String {
    // 是否含有注音
    func isContainsPhoneticCharacters() -> Bool {
        for scalar in self.unicodeScalars {
            if (scalar.value >= 12549 && scalar.value <= 12582) || (scalar.value == 12584 || scalar.value == 12585 || scalar.value == 19968) {
                return true
            }
        }
        return false
    }
    
    // 是否含有中文字元
    func isContainsChineseCharacters() -> Bool {
        for scalar in self.unicodeScalars {
            if scalar.value >= 19968 && scalar.value <= 171941 {
                return true
            }
        }
        return false
    }
    
    // 是否含有空白字元
    func isContainsSpaceCharacters() -> Bool {
        for scalar in self.unicodeScalars {
            if scalar.value == 32 {
                return true
            }
        }
        return false
    }
    
    func isEnglishWithNumbers() -> String {
        let pattern = "[^A-Za-z0-9]+"
        let result = self.replacingOccurrences(of: pattern, with: "", options: [.regularExpression])
        return result
    }
    
    func hiddenCardCarNumber() -> String {
        let number = self
        let start = number.index(number.endIndex, offsetBy: -4)
        let end = number.index(number.endIndex, offsetBy: 0)
        let cardcarNumber = "****-" + String(number[start..<end])
        return cardcarNumber
    }
    
    func replace(target: String, withString: String) -> String {
        return self.replacingOccurrences(of: target, with: withString, options: .literal, range: nil)
    }
    
    func clearWhiteSpaces() -> String{
        return self.trimmingCharacters(in: .whitespaces)
    }
    
    var parameterValue: Dictionary<String, String> {
        return ["newsNo": self]
    }
}
