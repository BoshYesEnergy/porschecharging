//
//  UIViewController.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/5/25.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

// MARK:- UIPopoverPresentationControllerDelegate
extension UIViewController : UIPopoverPresentationControllerDelegate {
    public func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    public func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool{
        // popoverPresentationController.presentedViewController 被推出來的 viewController
        return true
    }
    
    func popover(pop:UIViewController,size:CGSize,arch:UIView,Direction:UIPopoverArrowDirection){
        pop.modalPresentationStyle = .popover
        pop.popoverPresentationController?.delegate = self
        pop.popoverPresentationController?.sourceView = arch
        pop.popoverPresentationController?.sourceRect = arch.bounds
        pop.preferredContentSize = size
        pop.popoverPresentationController?.permittedArrowDirections = Direction
        self.present(pop, animated: true, completion: nil)
    }
    
    func onIQKeyboardManager() {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
    }
    
    func offIQKeyboardManager() {
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.shouldResignOnTouchOutside = false
    }
}


// MARK:- Alert
extension UIViewController {
    static func confirmAlert(_ title: String, _ message: String, _ handlerTitle: String, _ handler: @escaping() -> Void, _ cancel: String = "", _ cancelHandler: @escaping() -> Void) -> UIAlertController {
        let controller = UIAlertController(title: title, message: message, preferredStyle: .alert)
        ///
        ///-- change alert view background color
        ///
//        let subview = controller.view.subviews.first!.subviews.first! as UIView
//        let view    = subview.subviews.first! as UIView
//        ///-- bottom view color
//        subview.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
//        ///-- alert view color,
//        view.backgroundColor    = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        let action = UIAlertAction(title: handlerTitle, style: .default) { (_) in
            handler()
        }
//        action.setValue(UIColor.red, forKey: "titleTextColor")
        if cancel != "" {
            let cancelAction = UIAlertAction(title: cancel, style: .default) { (_) in
                cancelHandler()
            }
//            cancelAction.setValue(UIColor.red, forKey: "titleTextColor")
            controller.addAction(cancelAction)
        }
        controller.addAction(action)
        return controller
    }
}
