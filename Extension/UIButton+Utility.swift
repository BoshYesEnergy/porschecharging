//
//  UIButton+Utility.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/15.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

extension UIButton {
    func setBackgroundColor(color: UIColor, titleColor: UIColor, forState: UIControl.State) {
        self.clipsToBounds = true  // add this to maintain corner radius
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        if let context = UIGraphicsGetCurrentContext() {
            context.setFillColor(color.cgColor)
            context.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
            let colorImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            self.setBackgroundImage(colorImage, for: forState)
            self.setTitleColor(titleColor, for: forState)
        }
    }
}
