//
//  ImageView+Utility.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/2.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit
import SDWebImage

extension UIImageView {
    
    func setImageWithUrlPath(_ path: String, completed: SDExternalCompletionBlock? = nil) {
        SDImageCache.shared.removeImage(forKey: path) {
            self.sd_setImage(with: URL(string: path), completed: completed)
        }
    }
    
    func setLocalWebpImage(url: URL) {
        SDWebImageManager.shared.loadImage(with: url, options: [], progress: nil) { (image, data, error, cacheType, finished, url) in
            if let error = error {
                print(error)
            } else {
                self.image = image
            }
        }
    }
    
}
