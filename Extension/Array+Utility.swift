//
//  Array+Utility.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/27.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

extension Array where Element: Hashable {
  func removingDuplicates() -> [Element] {
      var addedDict = [Element: Bool]()
      return filter {
        addedDict.updateValue(true, forKey: $0) == nil
      }
   }
   mutating func removeDuplicates() {
      self = self.removingDuplicates()
   }
}
