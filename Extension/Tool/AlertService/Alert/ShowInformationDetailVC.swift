//
//  ShowInformationDetailVC.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/6.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

protocol ShowInformationDetailDelegate: class {
    func tapLeftWithAction()
    func tapRightWithAction()
}

class ShowInformationDetailVC: UIViewController { 
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var infoTitle: UILabel!
    
    @IBOutlet weak var projectStackView: UIStackView!
    @IBOutlet weak var projectTitle: UILabel!
    @IBOutlet weak var projectValue: UILabel!
    
    @IBOutlet weak var modelStackView: UIStackView!
    @IBOutlet weak var modelTitle: UILabel!
    @IBOutlet weak var modelValue: UILabel!
    
    @IBOutlet weak var deatilStackView: UIStackView!
    @IBOutlet weak var detailTitle: UILabel!
    @IBOutlet weak var detailValue: UILabel!
    
    @IBOutlet weak var contentLabel: UILabel!
    
    @IBOutlet weak var buttonStackView: UIStackView!
    @IBOutlet weak var leftBtn: UIButton!
    @IBOutlet weak var rightBtn: UIButton!
     
    weak var delegate: ShowInformationDetailDelegate?
    
    var projectToString = ""
    var modelToString   = ""
    var detailToString  = ""
    var contentToString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        leftBtn.layer.borderWidth = 1
        leftBtn.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        contentLabel.text  = contentToString
        projectValue.text  = projectToString
        modelValue.text    = modelToString
        detailValue.text   = detailToString
        setLanguage()
    }
    
    func setLanguage() {
        leftBtn.setTitle(LocalizeUtils.localized(key: Language.Cancel.rawValue), for: .normal)
        rightBtn.setTitle(LocalizeUtils.localized(key: Language.Confirm.rawValue), for: .normal)
        projectTitle.text = LocalizeUtils.localized(key: "SubscriptionVin")
        modelTitle.text = LocalizeUtils.localized(key: "SubscriptionModel")
        detailTitle.text = LocalizeUtils.localized(key: "SubscriptionValidity")
        infoTitle.text = LocalizeUtils.localized(key: "PaymentInfo")
        contentLabel.text = LocalizeUtils.localized(key: "SubscriptionAlert")
    }
 
    @IBAction func leftAction(_ sender: UIButton) {
        delegate?.tapLeftWithAction()
    }
    
    @IBAction func rightAction(_ sender: UIButton) {
        delegate?.tapRightWithAction()
    }
}
