//
//  ShowCheckMessageVC.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/10.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

protocol ShowCheckMessageDelegate: class {
    func tapConfirm(tagCount: Int)
    func tapCancel()
}

enum MessageCheckBtn { 
    case one
    case two
}

class ShowCheckMessageVC: UIViewController {
 
    @IBOutlet weak var infoTitle: UILabel!
    @IBOutlet weak var infoContent: UILabel!
    
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    
    weak var delegate: ShowCheckMessageDelegate?
    
    var toTitle   = ""
    var toContent = "" 
    var tagCount  = 0
    var custom: MessageCheckBtn = .two
        
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    func setUI() {
        cancelBtn.layer.borderWidth = 1
        cancelBtn.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        infoTitle.text   = ""
        infoContent.text = ""
        if toTitle != "" {
            infoTitle.text = toTitle
        }
        
        if toContent != "" {
            infoContent.text = toContent
        }
        
        if custom == .one {
            cancelBtn.isHidden = true
        } else {
            cancelBtn.isHidden = false
        }
        
        confirmBtn.setTitle(LocalizeUtils.localized(key: Language.Confirm.rawValue), for: .normal)
        cancelBtn.setTitle(LocalizeUtils.localized(key: Language.Cancel.rawValue), for: .normal)
    }
    
    @IBAction func confirm(_ sender: UIButton) {
        delegate?.tapConfirm(tagCount: tagCount)
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        delegate?.tapCancel()
    }
}
