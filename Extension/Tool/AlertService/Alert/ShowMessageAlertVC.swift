//
//  ShowMessageAlertVC.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/8.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

protocol ShowMessageAlertDelegate: class {
    func messageTapAction()
}

class ShowMessageAlertVC: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var tapBtn: UIButton!
    
    var stringToTitle   = ""
    var stringToContent = ""
    var titleToBtn      = ""

    weak var delegate: ShowMessageAlertDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    func setUI() {
        titleLabel.text   = ""
        contentLabel.text = ""
        if stringToTitle != "" {
            titleLabel.text = stringToTitle
        }
        
        if stringToContent != "" {
            contentLabel.text = stringToContent
        }
        
        if titleToBtn != "" {
            tapBtn.setTitle(titleToBtn, for: .normal)
        }
    }
 
    @IBAction func tap(_ sender: UIButton) {
        delegate?.messageTapAction()
    }
}
