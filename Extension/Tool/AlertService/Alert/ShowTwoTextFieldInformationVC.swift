//
//  ShowTwoTextFieldInformationVC.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/9.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

protocol TwoTextFieldDelegate: class {
    func confirm(first: String, second: String)
    func cancel()
}

class ShowTwoTextFieldInformationVC: UIViewController {

    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var numbetTitle: UILabel!
    @IBOutlet weak var firstTextfield: UITextField!
    @IBOutlet weak var secondTextfield: UITextField!
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var nameBorderView: UIView!
    @IBOutlet weak var numberBorder: UIView!
    
    var stringToTitle  = ""
    var stringToFirst  = ""
    var stringToSecond = ""
    var constraintText: Int = 8
    
    weak var delegate: TwoTextFieldDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        firstTextfield.delegate = self
        secondTextfield.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(searchChange(obj:)), name: .UITextFieldTextDidChange, object: firstTextfield)
        NotificationCenter.default.addObserver(self, selector: #selector(searchChange(obj:)), name: .UITextFieldTextDidChange, object: secondTextfield)
    }
    
    @objc func searchChange(obj: Notification) {
        if firstTextfield.text?.count ?? 0 >= 1 {
            confirmBtn.backgroundColor = ThemeManager.shared.colorPalettes.redBorder
            confirmBtn.isEnabled = true
        } else {
            confirmBtn.backgroundColor = ThemeManager.shared.colorPalettes.garyBackgroundColor
            confirmBtn.isEnabled = false
        }
    }

    
    func setUI() {
        cancelBtn.layer.borderWidth = 1
        cancelBtn.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        nameBorderView.layer.borderWidth = 1
        nameBorderView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        numberBorder.layer.borderWidth = 1
        numberBorder.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        confirmBtn.setTitle(LocalizeUtils.localized(key: Language.Confirm.rawValue), for: .normal)
        cancelBtn.setTitle(LocalizeUtils.localized(key: Language.Cancel.rawValue), for: .normal)
        
        numbetTitle.text = LocalizeUtils.localized(key: "TaxIDnumber")
        firstTextfield.placeholder = LocalizeUtils.localized(key: "EnterCloudinvoiceCode")
        secondTextfield.placeholder = LocalizeUtils.localized(key: "TaxNumber")
        
        titleLabel.text      = stringToTitle
        firstTextfield.text  = stringToFirst
        secondTextfield.text = stringToSecond
    }

    @IBAction func confirm(_ sender: UIButton) {
        delegate?.confirm(first: firstTextfield.text ?? "", second:  secondTextfield.text ?? "")
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        delegate?.cancel()
    }
    
}


extension ShowTwoTextFieldInformationVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let count = text.count + string.count - range.length
         
        if textField == secondTextfield {
            if count > constraintText {
                return false
            }
        }
         
        return true
    }
}
