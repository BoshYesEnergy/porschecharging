//
//  ShowInformationVC.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/25.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

enum InfoStatus {
    case NONE
    case ONE
    case TWO
    case YesAndNo
}

enum InfoActionStatus {
    case None
    case Back 
}

enum WhereTo {
    case None
    case Partenr
}

class ShowInformationVC: UIViewController {
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton! 
    
    var titleString    = ""
    var contentString  = ""
    var cancelIsHidden = true
    var oneBtnTitle    = "OK"
    var custom: InfoStatus = .NONE
    var actionStataus: InfoActionStatus = .None
    var whereTo: WhereTo = .None
    
    override func viewDidLoad() {
        super.viewDidLoad()
        confirmBtn.setTitle(LocalizeUtils.localized(key: Language.Confirm.rawValue), for: .normal)
        cancelBtn.setTitle(LocalizeUtils.localized(key: Language.Cancel.rawValue), for: .normal)
        titleLabel.text = LocalizeUtils.localized(key: "PaymentInfo")
        if titleString != "" {
            titleLabel.text = titleString
        }
        
        if contentString != "" {
            contentLabel.text = contentString
        }
        cancelBtn.layer.borderWidth = 1
        cancelBtn.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        if custom == .NONE {
            
        } else if custom == .ONE {
            setOneButton()
        } else if custom == .YesAndNo {
            setTwoButton()
        } 
        cancelBtn.isHidden = cancelIsHidden == true ? true : false 
    }
    
    func setOneButton() {
        cancelIsHidden = true
        confirmBtn.setTitle(oneBtnTitle, for: .normal)
    }
    
    // Yee or No btn
    func setTwoButton() {
        cancelIsHidden = false
        
        confirmBtn.setTitle(LocalizeUtils.localized(key: "Yes"), for: .normal)
        confirmBtn.layer.borderWidth = 0
        confirmBtn.backgroundColor    = #colorLiteral(red: 0.8352941176, green: 0, blue: 0.1098039216, alpha: 1)
        confirmBtn.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        
        cancelBtn.setTitle(LocalizeUtils.localized(key: "No"), for: .normal) 
        cancelBtn.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        cancelBtn.layer.borderWidth = 1
        cancelBtn.backgroundColor   = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        cancelBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
    }
    
    @IBAction func confirm(_ sender: UIButton) {
        self.dismiss(animated: true) {
            if self.whereTo == .Partenr {
                NotificationCenter.default.post(name: .confirmPartner, object: nil)
                return
            }
            
            if self.actionStataus == .Back {
                NotificationCenter.default.post(name: .popToProfile, object: nil)
            }
            
            NotificationCenter.default.post(name: .apiDeleteCard, object: nil) 
            NotificationCenter.default.post(name: .apiDeletePartner, object: nil)
        }
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        dismiss(animated: true) {
            NotificationCenter.default.post(name: .cancelPartner, object: nil)
        }
    }
}
