//
//  ShowTextFieldInformationVC.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/5.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

protocol ShowTextFieldInformationDelegate: class {
    func tapConfirm(text: String, delegateTag: Int)
    func tapCancel()
}

class ShowTextFieldInformationVC: UIViewController {

    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textBorderView: UIView!
    @IBOutlet weak var returnTextfield: UITextField!
    @IBOutlet weak var otherTitle: UILabel!
    
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    
    var delegate : ShowTextFieldInformationDelegate?
    var titlName            = LocalizeUtils.localized(key: Language.AlertNickname.rawValue)
    var content             = ""
    var placeholder         = LocalizeUtils.localized(key: Language.placeholderName.rawValue)
    var other               = ""
    var constraintText: Int = 13
    var delegateTag         = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        NotificationCenter.default.addObserver(self, selector: #selector(searchChange(obj:)), name: .UITextFieldTextDidChange, object: returnTextfield)
    }
    
    func setUI() {
        textBorderView.layer.borderWidth = 1
        textBorderView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        cancelBtn.layer.borderWidth      = 1
        cancelBtn.layer.borderColor      = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        returnTextfield.text             = content
        returnTextfield.placeholder      = placeholder
        titleLabel.text                  = titlName
        otherTitle.text                  = other
        returnTextfield.delegate         = self
        confirmBtn.setTitle(LocalizeUtils.localized(key: Language.Confirm.rawValue), for: .normal)
        cancelBtn.setTitle(LocalizeUtils.localized(key: Language.Cancel.rawValue), for: .normal)
    }
    
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    @objc func searchChange(obj: Notification) {
        if delegateTag == 1 {
            if isValidEmail(returnTextfield.text ?? "") == true {
                textBorderView.layer.borderColor = ThemeManager.shared.colorPalettes.blackBorder.cgColor
                confirmBtn.backgroundColor = ThemeManager.shared.colorPalettes.redBorder
                confirmBtn.isEnabled = true
            } else {
                textBorderView.layer.borderColor = ThemeManager.shared.colorPalettes.redBorder.cgColor
                confirmBtn.backgroundColor = ThemeManager.shared.colorPalettes.garyBackgroundColor
                confirmBtn.isEnabled = false
            }
        } else if delegateTag == 2 {
            if returnTextfield.text?.count ?? 0 == 8 {
                textBorderView.layer.borderColor = ThemeManager.shared.colorPalettes.blackBorder.cgColor
                confirmBtn.backgroundColor = ThemeManager.shared.colorPalettes.redBorder
                confirmBtn.isEnabled = true
            } else {
                textBorderView.layer.borderColor = ThemeManager.shared.colorPalettes.redBorder.cgColor
                confirmBtn.backgroundColor = ThemeManager.shared.colorPalettes.garyBackgroundColor
                confirmBtn.isEnabled = false
            }
        }
    }
    
    @IBAction func confirm(_ sender: UIButton) {
        let text = returnTextfield.text ?? ""
        delegate?.tapConfirm(text: text, delegateTag: delegateTag)
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        delegate?.tapCancel()
    }
}


extension ShowTextFieldInformationVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let count = text.count + string.count - range.length
         
        if count > constraintText {
            return false
        }
        
        return true
    }
}
