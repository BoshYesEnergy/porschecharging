//
//  IServiceViewController.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/6.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class IServiceViewController: UIViewController {
    @IBOutlet weak var infoTitle: UILabel!
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var tutorialBtn: UIButton!
    @IBOutlet weak var faqBtn: UIButton!
    @IBOutlet weak var suportBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLanguage()
    }
    
    func setLanguage() {
        infoTitle.text = LocalizeUtils.localized(key: "InformationCenter")
        faqBtn.setTitle(LocalizeUtils.localized(key: "FAQ"), for: .normal)
        tutorialBtn.setTitle(LocalizeUtils.localized(key: "Toturial"), for: .normal)
        suportBtn.setTitle(LocalizeUtils.localized(key: "Support"), for: .normal)
    }

    @IBAction func close(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func tutoral(_ sender: UIButton) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AppGuideViewController")
        let nav = UINavigationController(rootViewController: vc)
        nav.isNavigationBarHidden  = true
        nav.modalPresentationStyle = .overFullScreen
        present(nav, animated: true, completion: nil)
    }
    
    @IBAction func faq(_ sender: UIButton) {
        let vc       = DisplayWebViewController(nibName: "DisplayWebViewController", bundle: nil)
        if KJLRequest.shareInstance().language == "en" {
            vc.status    = .FAQen
            vc.titleName = LocalizeUtils.localized(key: "FAQ")
        } else {
            vc.status    = .FAQ
            vc.titleName = LocalizeUtils.localized(key: "FAQ")
        } 
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func suport(_ sender: UIButton) {
        // 電話號碼不可包含空白字串，會閃退
//        number = number.replacingOccurrences(of: " ", with: "-")
        let phoneUrl = URL(string: "tel://0800718911")
        UIApplication.shared.open(phoneUrl!, options: [:], completionHandler: nil)
    }
}
