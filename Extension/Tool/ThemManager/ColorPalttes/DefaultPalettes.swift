//
//  DefaultPalettes.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/17.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

struct DefaultPalettes: ColorPalettesProtocol {
    var garyToText: UIColor = #colorLiteral(red: 0.5882352941, green: 0.5960784314, blue: 0.6039215686, alpha: 1) //RGB 150 152 154
    
    var blackToText: UIColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    
    var whiteToText: UIColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
    
    var redBackgroundColor  : UIColor  = #colorLiteral(red: 0.8352941176, green: 0, blue: 0.1098039216, alpha: 1) // RGB 213 0 28
    
    var garyBackgroundColor : UIColor  = #colorLiteral(red: 0.7882352941, green: 0.7921568627, blue: 0.7960784314, alpha: 1) //RGB 201 202 203
    
    var garyLine            : UIColor  = #colorLiteral(red: 0.7882352941, green: 0.7921568627, blue: 0.7960784314, alpha: 1) //RGB 201 202 203

    var blackBorder         : UIColor  = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    
    var whiteBorder         : UIColor  = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
    
    var redBorder           : UIColor  = #colorLiteral(red: 0.8352941176, green: 0, blue: 0.1098039216, alpha: 1) // RGB 213 0 28
    
    var greenPrompt         : UIColor  = #colorLiteral(red: 0.003921568627, green: 0.5411764706, blue: 0.007843137255, alpha: 1)
}
