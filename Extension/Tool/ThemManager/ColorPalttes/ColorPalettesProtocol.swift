//
//  ColorPalettesProtocol.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/17.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

protocol ColorPalettesProtocol {
    var garyToText          : UIColor { get }
    var blackToText         : UIColor { get }
    var whiteToText         : UIColor { get }
      
    var redBackgroundColor  : UIColor { get }
    var garyBackgroundColor : UIColor { get }
    
    var garyLine            : UIColor { get }
    
    var blackBorder         : UIColor { get }
    var whiteBorder         : UIColor { get }
    var redBorder           : UIColor { get } 
    var greenPrompt         : UIColor { get }
}

enum ColorPalettesType: CaseIterable {
    case Default
    
    var theme: ColorPalettesProtocol {
        get {
            switch self {
            case .Default:
                return DefaultPalettes()
            }
        }
    }
}
