//
//  ThemeManager.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/17.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

class ThemeManager {
    static let shared = ThemeManager()
    
    var colorPalettes: ColorPalettesProtocol = ColorPalettesType.Default.theme
    
    func switchTheme(_ type: ColorPalettesType) {
        self.colorPalettes = type.theme
        
    }
}
