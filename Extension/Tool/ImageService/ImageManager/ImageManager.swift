//
//  ImageManager.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/11/7.
//  Copyright © 2020 s5346. All rights reserved.
//

import Foundation

class ImageManager {
    
    static let share = ImageManager()
    
    /// add text
    func textToImage(drawText text: String, inImage image: UIImage) -> UIImage {
        let textColor = UIColor.black
        let textFont = UIFont(name: "Helvetica Bold", size: 12)!

        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(image.size, false, scale)
        
        let style = NSMutableParagraphStyle()
        style.alignment = NSTextAlignment.center
        
        let textFontAttributes = [
            NSAttributedStringKey.font: textFont,
            NSAttributedStringKey.foregroundColor: textColor,
            .paragraphStyle: style,
            ] as [NSAttributedStringKey : Any]
        image.draw(in: CGRect(origin: CGPoint.zero, size: image.size))

        let rect = CGRect(x: 0, y: image.size.height - 15, width: image.size.width - 5, height: 15)
        text.draw(in: rect, withAttributes: textFontAttributes)

        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage!
     }
    
    /// change size
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
}
