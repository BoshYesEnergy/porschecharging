//
//  ToolImage.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/10/8.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

enum TabImage {
    /// viewcontroller [0]
    static let home_Map                    = "icon_bar_map_default"
    static let home_Map_hover              = "icon_bar_map_click"
    /// viewControllers [1]
    static let charging_list               = "icon_bar_route_default"
    static let charging_list_hover         = "icon_bar_route_default_click"
    /// viewcontroller [2]
    static let green_scan_normal           = "icon_bar_scan_default"
    static let green_scan_press            = "icon_bar_scan_click"
    /// viewcontroller [3]
    static let profile                     = "icon_bar_profile_default"
    static let profile_hover               = "icon_bar_profile_default_click"
}

enum ToolImage {
    /// AddEInvoiceChildCell
    static let radio_disable = "icon_radio_button_uncheck_disable"
    static let radio_default = "icon_radio_button_check_default"
    static let radio_click   = "icon_radio_button_uncheck_click"
    /// AddEInvoiceChildCell
    static let edit_default  = "icon_edit_default"
    static let edit_disable  = "icon_edit_disable"
     
    static let location_default = "button_floating_locate_default"
    static let location_click   = "button_floating_locate_click"
    
    static let filter_default = "button_floating_filter_default"
    static let filter_click   = "button_floating_filter_click"
    
    static let information_default = "button_floating_information_default"
    static let information_click   = "button_floating_information_click"
}

enum ToolPhoto: String {
    case inbox_default = "icon_mail_close_default"
    case inbox_click   = "icon_mail_close_click"
    case inbox_disable = "icon_mail_close_disable"
    
    case inbox_open_default = "icon_mail_open_default"
    case inbox_open_click   = "icon_mail_open_click"
    case inbox_open_disable = "icon_mail_open_disable"
    
    case close_default = "button_close_default"
    case close_check   = "button_check_default"
    case close_disable = "button_close_disable"
    
    case next         = "icon_next_default"
    case next_click   = "icon_next_click"
    case next_disable = "icon_next_disable"
    
    case check = "icon_check_default"
    case check_none = "icon_uncheck_default"
    
    case icon_success = "icon_notification_success"
    case icon_error   = "icon_notification_error"
    case icon_warning = "icon_notification_warning"
    
    case booking_car  = "photo_car_default"
    case booking_car_disable = "photo_car_disable"
    
    case points_Start = "thumbnail_map_destination_location"
    case points_End   = "thumbnail_map_current_location"
}


// AppGuide
enum GuideImage: String {
    case en01 = "tutorial_eng_01"
    case en02 = "tutorial_eng_02"
    case en03 = "tutorial_eng_03"
    case en04 = "tutorial_eng_04"
    case en05 = "tutorial_eng_05"
    case en06 = "tutorial_eng_06"
    case en07 = "tutorial_eng_07"
    
    case zh01 = "tutorial_tw_01"
    case zh02 = "tutorial_tw_02"
    case zh03 = "tutorial_tw_03"
    case zh04 = "tutorial_tw_04"
    case zh05 = "tutorial_tw_05"
    case zh06 = "tutorial_tw_06"
    case zh07 = "tutorial_tw_07"
}
