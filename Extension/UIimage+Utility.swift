//
//  UIimage+Utility.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/8/1.
//  Copyright © 2020 s5346. All rights reserved.
//

import Foundation

extension UIImage {
    /// tap tab bar item under background color
    class func imageWithColor(color: UIColor, size: CGSize) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}


extension UIImage {
    enum HomeID: String {
        case id = ""
        
    }
    
    convenience init!(homeID: HomeID) {
        self.init(named: homeID.rawValue)
    }
}
