//
//  UiView+Utility.swift
//  PorscheChargingApp
//
//  Created by Heng.Wang on 2020/8/1.
//  Copyright © 2020 s5346. All rights reserved.
//

import UIKit

extension UIView {
    func setupMerryChristmas() {
        // init EmitterCell with image
        //https://ios.devdon.com/archives/1046
        let emitterCell = CAEmitterCell()
        emitterCell.contents = UIImage(named: "snow.png")?.cgImage
        
        //– 粒子的創建速率
        emitterCell.birthRate = 30
        
        //每個粒子的存活時間
        emitterCell.lifetime = 30
        
        //粒子存活時間容許的容差範圍
        emitterCell.lifetimeRange = 2
        
        //Z 軸方向的發射角度
        emitterCell.yAcceleration = 0
        
        //XY 平面的發射角度
        emitterCell.xAcceleration = 0
        
        
        emitterCell.velocity = 30.0
        emitterCell.velocityRange = 200.0
        
        emitterCell.emissionLongitude = .pi * 0.5
        emitterCell.emissionRange = .pi * 0.5
        
        //        emitterCell.scale = 0.8
        //        emitterCell.scaleRange = 0.8
        //        emitterCell.scaleSpeed = -0.01
        
        emitterCell.alphaRange = 0.75
        emitterCell.alphaSpeed = -0.05
        
        // init emitter
        let rect = CGRect(x: 0.0, y: -60, width: UIScreen.main.bounds.width, height: 20)
        let emitter = CAEmitterLayer()
        emitter.borderWidth = 0
        emitter.emitterCells = [emitterCell]
        emitter.frame = rect
        
        // appear way
        emitter.emitterShape = "rectangle"
        emitter.emitterMode = "surface"
        emitter.emitterPosition = CGPoint(x: rect.width/2, y: rect.height/2)
        emitter.emitterSize = rect.size
        self.layer.addSublayer(emitter)
    }
}


extension UIView {
    func addFillConstraint() {
        guard let superview = self.superview else {
            print("Error! `superview` was nil – call `addSubview(view: UIView)` before calling `bindFrameToSuperviewBounds()` to fix this.")
            return
        }
        
        self.translatesAutoresizingMaskIntoConstraints = false
        superview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[subview]-0-|", options: .directionLeadingToTrailing, metrics: nil, views: ["subview": self]))
        superview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[subview]-0-|", options: .directionLeadingToTrailing, metrics: nil, views: ["subview": self]))
    }
}

