//
//  SelectPhotoCollectionCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit

class SelectPhotoCollectionCell: UICollectionViewCell {
    
    
    @IBOutlet weak var photoImage: UIImageView!
    @IBOutlet weak var indexLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
        self.layer.borderColor = #colorLiteral(red: 0.5921568627, green: 0.5921568627, blue: 0.5921568627, alpha: 1)
        self.layer.borderWidth = 1
        self.indexLabel.text = ""
    }
    
    func setupCellWith(image: UIImage , isShowLabel: Bool , indexString: String) {
        self.photoImage.image = image
        self.indexLabel.text = ""
        if (isShowLabel == true) {
            self.indexLabel.isHidden = false
            self.indexLabel.text = indexString
            self.layer.borderWidth = 3
            self.layer.borderColor = #colorLiteral(red: 0, green: 0.768627451, blue: 0.7960784314, alpha: 1)
        } else {
            self.indexLabel.isHidden = true
            self.backgroundColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
            self.layer.borderColor = #colorLiteral(red: 0.5921568627, green: 0.5921568627, blue: 0.5921568627, alpha: 1)
            self.layer.borderWidth = 1
        }
        
    }
}
