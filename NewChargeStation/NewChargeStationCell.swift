//
//  NewChargeStationCell.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit


class NewChargeStationCell: UITableViewCell {

    // MARK: - IBOutlet
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var tipBtn: UIButton!
    @IBOutlet weak var grayView: UIView!
    
    // MARK: - Property
    private var indexPath: IndexPath?
    
    // MARK: - 生命週期
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        inputTextField.text = ""
        grayView.layer.cornerRadius = grayView.bounds.size.height / 2
        grayView.layer.masksToBounds = true
    }

    // MARK: - IBAction
    @IBAction func tipAction(_ sender: UIButton) {
    }
    
    // MARK: - 自訂 Func
    func setupCellWith(indexPath: IndexPath , isShowAbsoluteImage: Bool , titleString: String) {
        self.indexPath = indexPath
        self.titleLabel.text = titleString
        
        // 設定按鈕圖案
        if (indexPath.row == 2 || indexPath.row == 3 || indexPath.row == 4) {
            tipBtn.setImage(UIImage(named: "new_ic_site"), for: .normal)
        } else if (indexPath.row == 8) {
            tipBtn.setImage(UIImage(named: "sign_in_ic_more"), for: .normal)
        }
        else {
            tipBtn.setImage(UIImage(named: "sign_in_ic_edit"), for: .normal)
        }
        
        // 設定鍵盤種類
        if (indexPath.row == 1 ) {
            inputTextField.keyboardType = .numberPad
        } else {
            inputTextField.keyboardType = .default
        }
        
        // 充電站地址、經度、緯度 自動帶值，所以設定為無法編輯
        if (indexPath.row == 2 || indexPath.row == 3 || indexPath.row == 4) {
            inputTextField.isEnabled = false
        } else {
            inputTextField.isEnabled = true
        }
    }
}
