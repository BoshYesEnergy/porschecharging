//
//  NewPlaceSearchViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import GoogleMaps
import GooglePlaces


class NewPlaceSearchViewController: KJLNavView {
    
    // MARK: - IBOutlet
//    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var googleMapView: GMSMapView!
    @IBOutlet weak var searchTextField: UITextField!
    
    // MARK: - Property
    private var locationManager = CLLocationManager()
    private var address: String = ""
    private var resultLatitude: String = ""
    private var resultLongitude: String = ""
    private var googleAPIManager =  GoogleAPIRequestManager.init()
    private var resultMarker = GMSMarker()
    private var isFirstSearch = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        createNavViewForRightBtn(rightName: "繼續", rightImage: "", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), isShadow: false, titleColor: UIColor.black)

        titleLab?.text = "充電站位置"
        rightBtn?.addTarget(self, action: #selector(continueAction(_:)), for: .touchUpInside)
        
        searchTextField.delegate = self
        googleMapView.delegate = self
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        setupMyLocation()
        self.googleAPIManager.delegate = self
        changeContinueBtnStatus()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) 
    }
    
    @IBAction func deleteAction(_ sender: UIButton) {
        self.searchTextField.text = ""
        changeContinueBtnStatus()
    }
    
    // MARK: - 自訂 Func
    @objc private func continueAction(_ sender: UIButton) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewChargeStationViewController") as? NewChargeStationViewController {
            vc.address = self.address
            vc.latitude = self.resultLatitude
            vc.longitude = self.resultLongitude
            self.navigationController?.show(vc, sender: self)
        }
    }
    
    private func setupMyLocation() {
        guard CLLocationManager.authorizationStatus() != .denied else {
            let a = URL(string: UIApplicationOpenSettingsURLString)
            if UIApplication.shared.canOpenURL(a!) {
                UIApplication.shared.open(a!, options: [:], completionHandler: nil)
            }
            return
        }
        
        if CLLocationManager.authorizationStatus() == .notDetermined || CLLocationManager.authorizationStatus() == .denied {
            self.zoomInToTheLocationWith(latitude: 25.0329893, longitude: 121.5326985, zoomNumber: 15)
        } else {
            if let myLocation = MainViewController.mainController?.mapView.camera.target {
                self.zoomInToTheLocationWith(latitude: myLocation.latitude, longitude: myLocation.longitude, zoomNumber: 15)
                googleAPIManager.callGoogleCoodinateConvertToAddressWith(latitude: myLocation.latitude, longitude: myLocation.longitude, geocodingLocationType: .Coodinate)
            }
        }
    }
    
    private func zoomInToTheLocationWith(latitude: CLLocationDegrees , longitude: CLLocationDegrees , zoomNumber: Float) {
        let camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: zoomNumber)
        self.googleMapView.camera = camera
    }
    
    private func clearAllMarker() {
        self.resultMarker.map = nil
    }
    
    private func changeContinueBtnStatus() {
        guard self.searchTextField.text!.count == 0 else {
            rightBtn?.isEnabled = true
            rightBtn?.setTitleColor(#colorLiteral(red: 0, green: 0.768627451, blue: 0.7960784314, alpha: 1), for: .normal)
            return
        }
        rightBtn?.isEnabled = false
        rightBtn?.setTitleColor(#colorLiteral(red: 0.5921568627, green: 0.5921568627, blue: 0.5921568627, alpha: 1), for: .normal)
    }
    
    private func addMarkerOnGoogleMapWith(title: String , latitude: CLLocationDegrees , longitude: CLLocationDegrees , geocodingLocationType: GeocodingLocationType) {
        
        resultMarker = GMSMarker()
        resultMarker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        resultMarker.title = title
        resultMarker.icon = UIImage(named: "home_ic_pin")
        resultMarker.map = googleMapView
//        resultMarker.isDraggable = true
    }
}

// MARK: - GoogleMap View Delegate {
extension NewPlaceSearchViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        return false
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
//        googleAPIManager.callGoogleCoodinateConvertToAddressWith(latitude: coordinate.latitude, longitude: coordinate.longitude, geocodingLocationType: .Coodinate)
//        resultMarker.isDraggable = true
    }
    
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        print("")
//        resultMarker.isDraggable = true
    }

    func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker) {
        //        print("start drag: \(marker.position.latitude)")
    }

    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        //         print("end drag: \(marker.position.latitude)")
        
//        googleAPIManager.callGoogleCoodinateConvertToAddressWith(latitude: marker.position.latitude, longitude: marker.position.longitude, geocodingLocationType: .Coodinate)
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        googleAPIManager.callGoogleCoodinateConvertToAddressWith(latitude: position.target.latitude, longitude: position.target.longitude, geocodingLocationType: .Coodinate)
    }
}

// MARK: - CLLocationManager Delegate
extension NewPlaceSearchViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.authorizedWhenInUse {
            googleMapView.isMyLocationEnabled = true
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Get Location Error: \(error)")
    }
}

// MARK: - TextField Delegate
extension NewPlaceSearchViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let autoVC = GMSAutocompleteViewController()
        autoVC.delegate = self
        let filter = GMSAutocompleteFilter()
        //        filter.type = .establishment
        filter.country = "TW"
        autoVC.autocompleteFilter = filter
        autoVC.modalPresentationStyle = .fullScreen
        self.present(autoVC, animated: true, completion: nil)
    }
}

// MARK: - GMSAutocompleteVC Delegate
extension NewPlaceSearchViewController: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.searchTextField.text = place.formattedAddress
         googleAPIManager.callGoogleAddressConvertToCoodinateWith(addressString: place.formattedAddress ?? "" , geocodingLocationType: .None)
        self.dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Autocomplete Error: \(error.localizedDescription)")
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK: - GoogleRequesetAPIManagerDelegate
extension NewPlaceSearchViewController: GoogleAPIRequestManagerDelegate {
    
    func googleGeocodingResultWith(geocodingData: GoogleGeocodingDataClass) {
        
        if let status = geocodingData.status  {
            if status == "OK" {
                if let formattedAddress = geocodingData.formattedAddress {
                    self.address = formattedAddress
                    
                    if let resultLatitude = geocodingData.lat {
                        self.resultLatitude = "\(resultLatitude)"
                        
                        if let  resultLongitude = geocodingData.lng {
                            self.resultLongitude = "\(resultLongitude)"
                            
                            self.clearAllMarker()   // 清掉前一個大頭針

                            if (geocodingData.locationType == .None) {
                                // 使用者輸入地址搜尋
//                                self.addMarkerOnGoogleMapWith(title: formattedAddress, latitude: resultLatitude, longitude: resultLongitude, geocodingLocationType: .None)
                                self.zoomInToTheLocationWith(latitude: resultLatitude, longitude: resultLongitude, zoomNumber: 15)
                            
                            } else if (geocodingData.locationType == .Coodinate){
                                // 點擊地圖 座標轉地址
                                self.searchTextField.text = formattedAddress
//                                self.addMarkerOnGoogleMapWith(title: formattedAddress, latitude: resultLatitude, longitude: resultLongitude, geocodingLocationType: .None)

                            }
                            self.isFirstSearch = false
                            self.changeContinueBtnStatus()
                        }
                    }
                }
            } else {
//                if (isFirstSearch == true) {
//                    ShowMessageView.showMessage(title: nil, message: "找不到您的目前所在位置，請改用手動輸入", completion: { (isOK) in
//                        self.searchTextField.text = ""
//                        self.changeContinueBtnStatus()
//                    })
//                } else {
//                    ShowMessageView.showMessage(title: nil, message: "找不到您搜尋的位置，請重新輸入", completion: { (isOK) in
//                        self.searchTextField.text = ""
//                        self.changeContinueBtnStatus()
//                    })
//                }
            }
        } else {
//            if (isFirstSearch == true) {
//                ShowMessageView.showMessage(title: nil, message: "找不到您的目前所在位置，請改用手動輸入", completion: { (isOK) in
//                    self.searchTextField.text = ""
//                    self.changeContinueBtnStatus()
//                })
//            } else {
//                ShowMessageView.showMessage(title: nil, message: "找不到您搜尋的位置，請重新輸入", completion: { (isOK) in
//                    self.searchTextField.text = ""
//                    self.changeContinueBtnStatus()
//                })
//            }
        }
        
    }
    // 計算距離與時間
    func googleDistanceMatrixResultWith(distanceDurationData: GoogleDistanceMatrixClass) {
        
    }
    // 導航
    func googleDirectionResultWith(directionData: GoogleDirectionDataClass) {
        
    }
}
