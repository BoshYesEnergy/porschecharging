//
//  SelectPhotoViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit
import Photos
import SVProgressHUD

protocol SelectPhotoViewControllerDelegate: class {
    func didFinishSelectPhotoWith(imageArray: [UIImage])
}

class SelectPhotoViewController: KJLNavView {

    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: - Property
    private var albumImageArray: [UIImage] = []
    private var selectIndexArray: [Int] = []
    weak var delegte: SelectPhotoViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        createNavViewForRightBtn(rightName: "完成", rightImage: "", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: UIColor.black)
        titleLab?.text = "選取照片"
        
        rightBtn?.addTarget(self, action: #selector(rightBtnAction(_ :)), for: .touchUpInside)
        
        self.albumImageArray = self.fetchAllPhotos()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        SVProgressHUD.dismiss()
    }
    
    // MARK: - 自訂 Func
    @objc func rightBtnAction(_ sender: UIButton) {
        var imageArray: [UIImage] = []
        
        for index in selectIndexArray {
            imageArray.append(albumImageArray[index])
        }
        
        self.delegte?.didFinishSelectPhotoWith(imageArray: imageArray)
        
        ShowMessageView.showMessage(title: nil, message: "完成") { (isOK) in
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    // 抓取手機內所有照片
    func fetchAllPhotos() -> [UIImage]  {
        var images = [UIImage]()
        
        // 從裝置中取得所有類型為圖片的asset
        let fetchResult = PHAsset.fetchAssets(with: .image, options: nil)
        let requestOptions = PHImageRequestOptions()
        requestOptions.isSynchronous = true
        requestOptions.deliveryMode = .highQualityFormat
        
        for n in 0 ..< fetchResult.count {
            autoreleasepool {   // <== 加上這個
                let imageAsset = fetchResult.object(at: n)
                let size = CGSize(width: imageAsset.pixelWidth, height: imageAsset.pixelHeight)
                
                PHImageManager.default().requestImage(
                    for: imageAsset,
                    targetSize: size,
                    contentMode: .default,
                    options: requestOptions,
                    resultHandler: { (image, nil) in
                        // 參數 image 即為所取得的圖片
                        images.append(image!)
                })
            } // <== autoreleasepool 結束
        }
        
        return images
    }
}

extension SelectPhotoViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return albumImageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectPhotoCollectionCell", for: indexPath) as? SelectPhotoCollectionCell {
            
            var isShowLabel = false
            var index = 0
            if (selectIndexArray.contains(indexPath.item)) {
                isShowLabel = true
            }
            
            if (isShowLabel == true) {
                index = selectIndexArray.index(of: indexPath.item)!
            }
            
            cell.setupCellWith(image: albumImageArray[indexPath.row] , isShowLabel: isShowLabel , indexString: "\(index + 1)")
            
            return cell
        }
        return UICollectionViewCell()
    }
}

extension SelectPhotoViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
  
        if let index = selectIndexArray.index(of: indexPath.item) {
            selectIndexArray.remove(at: index)
        } else {
            if (selectIndexArray.count < 3) {
                selectIndexArray.append(indexPath.item)
            } else {
                ShowMessageView.showMessage(title: nil, message: "照片選擇上限 ：3 張", completion: { (isOK) in
                    
                })
            }
        }
        
        self.collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(12, 17, 12, 17)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenWidth = UIScreen.main.bounds.size.width
        let cellWith = (screenWidth - (12 * 4)) / 3
        return CGSize(width: cellWith, height: cellWith)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 6
    }
}
