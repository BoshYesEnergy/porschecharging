//
//  NewChargeStationViewController.swift
//  PorscheChargingApp
//
//  Created by Daisuke on 2020/8/1.
//  Copyright © 2020 Heng.Wang. All rights reserved.
//

import UIKit
import SVProgressHUD 

class NewChargeStationViewController: KJLNavView {
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var stationImage: UIImageView!
    @IBOutlet weak var imageBtn: UIButton!
    
    @IBOutlet weak var stationImage1: UIImageView!
    @IBOutlet weak var imageBtn1: UIButton!
    
    @IBOutlet weak var stationImage2: UIImageView!
    @IBOutlet weak var imageBtn2: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var bottomViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tableViewTopConstraint: NSLayoutConstraint!
    
    // MARK: - Property
    var address: String = ""
    var latitude: String = ""
    var longitude: String = ""
    private var cellHeight: CGFloat = 60
    private var titleArray: [String] = ["＊ 充電站名稱", // v
                                        "＊ 充電站電話",
                                        "＊ 充電站地址", // v
                                        "＊ 經度", // v
                                        "＊ 緯度", // v
                                        "＊ 充電樁樓層", // v
                                        "＊ 充電車位號碼", // v
                                        "＊ 充電費率",
                                        "＊ 充電槍規格", // v
                                        "＊ 充電樁品牌",
                                        "＊ 備註"]
    private var keyArray: [String] = ["stationName",
                                      "stationPhone",
                                      "stationAddress",
                                      "stationLongitude",
                                      "stationLatitude",
                                      "spotFloor",
                                      "spotSpace",
                                      "chargeFee",
                                      "spotFormat",
                                      "spotVendor",
                                      "note"]
    private var inputDictionary = ["stationName": "", // 0
                                   "stationPhone": "", // 1
                                   "stationAddress": "", // 2
                                   "stationLongitude": "", // 3
                                   "stationLatitude": "", // 4
                                   "spotFloor": "", // 5
                                   "spotSpace": "", // 6
                                   "chargeFee": "", // 7
                                   "spotFormat": "", // 8
                                   "spotVendor": "", // 9
                                   "note": ""] // 10
    private var pickerDataArray = [String]()
    private var stationPicArray: [String] = []
    private var currentButton : UIButton?
    private var hintView: UIView?
    private var spotPatternArray: [String] = []
    
    // MARK: - 生命週期
    override func viewDidLoad() {
        super.viewDidLoad() 
        createNavViewForRightBtn(rightName: "", rightImage: "", leftName: "", leftImage: "sign_in_ic_back", groundColor: #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1), isShadow: true, titleColor: UIColor.black)
        titleLab?.text = "回報充電站站點"
        
        self.inputDictionary["stationAddress"] = self.address
        self.inputDictionary["stationLatitude"] = self.latitude
        self.inputDictionary["stationLongitude"] = self.longitude
        
        //
        imageBtn.tag = 0
        imageBtn1.tag = 1
        imageBtn2.tag = 2
        #warning("02/26/2020--callChargeFeeList 用不到隱藏起來--")
//        callChargeFeeList()
        callSpotFormatList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.offIQKeyboardManager()
        self.tableViewHeight.constant = cellHeight * CGFloat(titleArray.count)
        self.bottomViewHeight.constant = CGFloat(200) + self.tableViewHeight.constant + CGFloat(120)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.onIQKeyboardManager()
    }
    
    // MARK: - IBAction
    @IBAction func addImageAction(_ sender: UIButton) {
        
        currentButton = sender
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        
        let cameraAction = UIAlertAction(title: "相機", style: .default) { (cameraAction) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.sourceType = .camera
                imagePicker.delegate = self
                self.show(imagePicker, sender: self)
            }
        }
        
        let albumAction = UIAlertAction(title: "從相簿選取", style: .default) { (albumAction) in
            /* 改用 ios 內建相簿，僅單選一張 - By Scott 2018.12.15 */
            
//            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectPhotoViewController") as? SelectPhotoViewController {
//                vc.delegte = self
//                self.navigationController?.show(vc, sender: self)
//            }
            /* 改用 ios 內建相簿，僅單選一張 - By Scott 2018.12.15 */
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.delegate = self
            self.show(imagePicker, sender: self)
        }
        
        alert.addAction(albumAction)
        alert.addAction(cameraAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func sureAction(_ sender: UIButton) {

        if ((inputDictionary[keyArray[0]]?.count == 0) ||
            (inputDictionary[keyArray[2]]?.count == 0) ||
            (inputDictionary[keyArray[3]]?.count == 0) ||
            (inputDictionary[keyArray[4]]?.count == 0) ||
            (inputDictionary[keyArray[5]]?.count == 0) ||
            (inputDictionary[keyArray[6]]?.count == 0) ||
            (inputDictionary[keyArray[8]]?.count == 0))
        {
            //
            ShowMessageView.showMessage(title: nil, message: "＊ 資料不得為空", completion: { (isOK) in
                //
            })
        } else {
            
            if (self.stationImage.image != nil || self.stationImage1.image != nil || self.stationImage2.image != nil) {
               self.showHintView()
            }
           
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                if (self.imageBtn.isSelected == true) {
                    let newImage = self.compressImage(image: self.stationImage.image ?? UIImage())
                    let imgString = KJLCommon.convertImageToBase64(image: newImage)
                    self.stationPicArray.append(imgString)

                }
                
                if (self.imageBtn1.isSelected == true) {
                    let newImage = self.compressImage(image: self.stationImage1.image ?? UIImage())
                    let imgString = KJLCommon.convertImageToBase64(image: newImage)
                    self.stationPicArray.append(imgString)

                }
                
                if (self.imageBtn2.isSelected == true) {
                    let newImage = self.compressImage(image: self.stationImage2.image ?? UIImage())
                    let imgString = KJLCommon.convertImageToBase64(image: newImage)
                    self.stationPicArray.append(imgString)

                }
                
                self.callStationCreateWith(inputDicArray: self.inputDictionary, stationPic: self.stationPicArray)
            }
        }
        
    }
    // MARK: - 自訂 Func
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
    
    // MARK: - call api 311
    func callChargeFeeList ()
    {
        KJLRequest.requestForChargeFeeList() { (response) in
            guard let response = response as? ChargeFeeListClass else {
                return
            }
            
            guard let data = response.datas else {
                return
            }
            
            for i in 0..<data.count
            {
                if let chargeFeeName = data[i].chargeFeeName {
                    if let chargeFee = data[i].chargeFee {
                        let aString = chargeFeeName + ":" + chargeFee
                        self.pickerDataArray.append(aString)
                    }
                }
            }
            
            print("")
        }
    }
    
    /// API-312 充電樁規格列表
    private func callSpotFormatList ()
    {
        KJLRequest.requestForSpotFormatList() { (response) in
            guard let response = response as? SpotFormatListClass else {
                return
            }
            
            guard let data = response.datas else {
                return
            }
            
            for i in 0..<data.count {
                if let spotPattern = data[i].spotPattern {
                    self.spotPatternArray.append(spotPattern)
                }
            }
            
            print("")
        }
    }
    
    /// API-309 充電站建立
    func callStationCreateWith(inputDicArray: [String: String], stationPic: [String]) {
        
        KJLRequest.requestForStationCreate(inputDicArray: inputDicArray, stationPic: stationPic) { (response) in
            
            if let hintView = self.hintView {
                hintView.removeFromSuperview()
            }
            
            guard response != nil else {
                
                return
            }
            
            ShowMessageView.showMessage(title: nil, message: "您已成功回報站點，謝謝！", completion: { (isOK) in
                if (isOK == true) {
                    var vc: UIViewController? = self
                    while vc?.presentingViewController != nil {
                        print(vc ?? "")
                        vc = vc?.presentingViewController
                        print(self.presentingViewController ?? "")
                    }
                    vc?.navigationController?.popToRootViewController(animated: true)
                }
            })
        }
    }
    
    private func convertImageToBase64(image: UIImage) -> String {
        
        let str = KJLCommon.convertImageToBase64(image: image)
        
        return str
    }
    
    private func showHintView() {
        
        self.hintView = UIView(frame: CGRect(x: 20, y: UIScreen.main.bounds.size.height / 2 - 50, width: UIScreen.main.bounds.size.width - 40, height: 100))
        if let hintView = self.hintView {
            hintView.backgroundColor = UIColor.clear
            hintView.layer.borderColor = #colorLiteral(red: 0.8196078431, green: 0.8196078431, blue: 0.8196078431, alpha: 1)
            hintView.layer.borderWidth = 1
            hintView.layer.shadowColor = #colorLiteral(red: 0.4, green: 0.4, blue: 0.4, alpha: 1)
            hintView.layer.shadowOffset = CGSize(width: 0 , height: 3)
            hintView.layer.shadowRadius = 3
            hintView.layer.shadowOpacity = 1
            hintView.layer.cornerRadius = 9
            
            let cornerView = UIView(frame: CGRect(x: 0, y: 0, width: hintView.bounds.size.width, height: hintView.bounds.size.height))
            cornerView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            hintView.addSubview(cornerView)
            cornerView.layer.cornerRadius = 9
            cornerView.layer.masksToBounds = true
            
            let showLabel = UILabel(frame: CGRect(x: 10, y: 10, width: hintView.bounds.size.width - 20, height: hintView.bounds.size.height - 20))
            showLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            showLabel.textAlignment = .center
            showLabel.font = UIFont.systemFont(ofSize: 16)
            showLabel.numberOfLines = 2
            showLabel.text = "照片正在上傳中，請耐心等候..."
            cornerView.addSubview(showLabel)
            self.view.addSubview(hintView)
        }
    }
    
    // 壓縮照片大小
    private func compressImage(image: UIImage) -> UIImage {
        let width = image.size.width
        let height = image.size.height
        
        var scale: CGFloat = 1
        if (width > 1000 || height > 1000) {
            if (width > height) {
                scale = 1000 / width
            }
            else
            {
                scale = 1000 / height
            }
        }
        let newSize = CGSize(width: width * scale, height: height * scale)
        
        UIGraphicsBeginImageContext(newSize)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        // 壓縮
        if let data = UIImageJPEGRepresentation(newImage ?? UIImage(), 0.1) {
            return UIImage(data: data)!
        }
        return UIImage()
    }
}

// MARK: - TableView DataSource
extension NewChargeStationViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "NewChargeStationCell", for: indexPath) as? NewChargeStationCell {
            var isShowAbsoluteImage = true
            if (indexPath.row == 1 || indexPath.row == 7 || indexPath.row == 9 || indexPath.row == 10 ) {
                isShowAbsoluteImage = false
                // 不顯示 ＊
                let title = titleArray[indexPath.row]
                // 字顏色
                let myAttribute = [NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.5176470588, green: 0.5176470588, blue: 0.5176470588, alpha: 1)]
                let myString = NSMutableAttributedString(string: title, attributes: myAttribute )
                // ＊顏色
                let anotherAttribute = [NSAttributedStringKey.foregroundColor: UIColor.clear]
                let myRange = NSRange(location: 0, length: 1)
                myString.addAttributes(anotherAttribute, range: myRange)
                // 設定
                cell.titleLabel.attributedText = myString
            } else {
                // 顯示 ＊
                let title = titleArray[indexPath.row]
                // 字顏色
                let myAttribute = [NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.5176470588, green: 0.5176470588, blue: 0.5176470588, alpha: 1)]
                let myString = NSMutableAttributedString(string: title, attributes: myAttribute )
                // ＊顏色
                let anotherAttribute = [NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0, green: 0.768627451, blue: 0.7960784314, alpha: 1)]
                let myRange = NSRange(location: 0, length: 1)
                myString.addAttributes(anotherAttribute, range: myRange)
                // 設定
                cell.titleLabel.attributedText = myString
            }
            cell.setupCellWith(indexPath: indexPath, isShowAbsoluteImage: isShowAbsoluteImage, titleString: titleArray[indexPath.row])
            cell.inputTextField.delegate = self as? UITextFieldDelegate
            cell.inputTextField.tag = indexPath.row
            cell.inputTextField.addTarget(self, action:#selector(self.textFieldChange(_:)), for: .editingChanged)
            
            cell.inputTextField.text = inputDictionary[keyArray[indexPath.row]]
            
            return cell
        }
        return UITableViewCell(style: .default, reuseIdentifier: "")
    }
    
    @objc func textFieldChange(_ textField: UITextField)
    {
        let keyName = keyArray[textField.tag]
        
        if keyName.count > 0 {
            inputDictionary[keyName] = textField.text
        }
        
        print("")
    }
}

// MARK: - TableView Delegate
extension NewChargeStationViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.cellHeight
    }
}

// MARK: - 增加照片
extension NewChargeStationViewController: UINavigationControllerDelegate , UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        self.stationPicArray.removeAll()
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        switch (currentButton?.tag) {
        case 0?:
            self.stationImage.image = image
            self.imageBtn.isSelected = true
            break
        case 1?:
            self.stationImage1.image = image
            self.imageBtn1.isSelected = true

            break
        case 2?:
            self.stationImage2.image = image
            self.imageBtn2.isSelected = true
            break
        default:
            break
        }
        
        dismiss(animated: true, completion: nil)
    }
}

// MARK: - TextField Delegate
extension NewChargeStationViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.tableViewTopConstraint.constant = 0
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
//        if (textField.tag == 7)
//        {
//            textField.resignFirstResponder()
//
//            let picker = Bundle.main.loadNibNamed("KJLPickerOneForArray", owner: self, options: nil)?[0] as? KJLPickerOneForArray
//            picker?.changeData(t: pickerDataArray)
//            picker?.show()
//            picker?.pickerCompletion = { (data) in
//                if let data = data {
//
//                    print(data)
//                    self.inputDictionary[self.keyArray[7]] = data
//                    textField.text = data
//
//                }
//            }
//        } else    /* 充電費率改為由使用者自行填入 */
            if (textField.tag == 8) {
            textField.resignFirstResponder()
            
            let picker = Bundle.main.loadNibNamed("KJLPickerOneForArray", owner: self, options: nil)?[0] as? KJLPickerOneForArray
            picker?.changeData(t: spotPatternArray)
            picker?.show()
            picker?.pickerCompletion = { (data) in
                if let data = data {
                    
                    print(data)
                    self.inputDictionary[self.keyArray[8]] = data
                    textField.text = data
                    
                }
            }
        } else if (textField.tag > 8) {
            self.tableViewTopConstraint.constant = -150
        }
        print("")
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        self.tableViewTopConstraint.constant = 0
        print("")
    }
}

extension NewChargeStationViewController: SelectPhotoViewControllerDelegate {
    func didFinishSelectPhotoWith(imageArray: [UIImage]) {
        for i in 0..<imageArray.count {
            if (i == 0) {
                self.stationImage.image = imageArray[i]
                self.imageBtn.isSelected = true
            }
            
            if (i == 1) {
                self.stationImage1.image = imageArray[i]
                self.imageBtn1.isSelected = true
            }
            
            if (i == 2) {
                self.stationImage2.image = imageArray[i]
                self.imageBtn2.isSelected = true
            }
        }
    }
}

